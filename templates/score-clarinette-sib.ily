\score {
  \new Staff \notemode <<
    \transpose sib do' <<
      \keepWithTag #(*tag-global*) \global
      \keepWithTag #(*tag-notes*) \includeNotes #(*note-filename*)
    >>
    \clef #(*clef*)
    $(make-music 'ContextSpeccedMusic
      'context-type 'Staff
      'element (make-music 'PropertySet
      'value (if (*instrument-name*)
                 (make-large-markup (*instrument-name*))
                 #{ \markup\center-column { Clarinette en si♭ } #})
      'symbol 'instrumentName))
    $(or (*score-extra-music*) (make-music 'Music))
  >>
  \layout {
    system-count = #(*system-count*)
    indent = \largeindent
    ragged-last = #(*score-ragged*)
  }
}