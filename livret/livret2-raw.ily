\livretAct ACTE II
\livretDescAtt\wordwrap-center {
  Le Théâtre représente un appartement du Palais d’Horace.
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center {
  \smallCaps { Camille, Curiace, } le jeune \smallCaps Horace.
}
\livretPers Camille
\livretRef #'BAAcacujh
%# Ainsi le Ciel pour jamais nous rassemble!
\livretPers Le jeune Horace
%# Sur le bonheur public le nôtre est affermi.
\livretPers Curiace
%# Que mon sort est heureux! Ce jour me rend ensemble
%# Et ma maîtresse & mon ami.
\livretPers Horace
%# Il unira d'une chaine éternelle
%# Nos familles & nos états.
\livretPers Curiace
%# Quel Dieu propice, après tant de débats,
%# A pu former une union si belle?
\livretPers Horace
%# Que ce Dieu bienfaisant en bénisse le cours!
\livretPers Camille
\livretRef #'BABcacujh
%# Qui l'eût dit, que ce jour, marqué par tant d'alarmes,
%# Seroit le plus beau de mes jours!
\livretPers Ensemble
%# Douce paix, transports pleins de charmes,
%# Ah! puissiez-vous durer toujours!
\livretPersDidas Chœur derrière le Théatre
\livretRef #'BBAchoeur
%# Vive à jamais le nom d'Horace
\livretPers Horace
%#- Qu'entends-je?
\livretScene SCÈNE II
\livretDescAtt\wordwrap-center {
  Le vieil \smallCaps Horace, Chevaliers Romains, les Précédens.
}
\livretPers Le vieil Horace
%#= Viens, mon fils, que ton père t'embrasse.
\livretPers Camille
%# Ah! Rome l'a choisi pour notre défenseur!
\livretPers Curiace & les romains
%# Rome a rendu justice à sa haute valeur.
\livretPers Le vieil Horace
%# Tes deux frères & toi partagent cet honneur.
\livretPers Horace
%# A ce suprême *honneur eussions-nous pu prétendre?
\livretPers Les chevaliers
%# Les Dieux ont fait ce choix: seuls ils l'ont pu dicter.
\livretPers Le vieil Horace
%# Moins nous devions l'attendre,
%# Plus il faudra le mériter.
\livretPers Horace
%# Mon père! et je le jure à vous, à ma patrie
%# Ou Rome sera libre, ou je ne serai plus.
\livretPers Le vieil Horace
%# Mourant pour son pa=ys, on meurt digne d'envie.
\livretPers Les chevaliers
%# Nos vœux ne seront point déçus.
\livretPers Horace
\livretRef #'BBBhoracesChoeur
%# Dieux, protecteurs du Tibre!
%# Tranchez mes jours & sauvez mon pa=ys!
%# Je reçois de ma vie =un assez digne prix,
%# Si Rome par ma mort est tri=omphante & libre.
\livretPers Le vieil Horace
%# O de Rome *heureux défenseur,
%# Cours, vole où la gloire t'appelle!
%# Tes frères, pleins du même zèle,
%# T'attendent aux champs de l'*honneur.
\livretPers Les chevaliers
%# O de Rome *heureux défenseur,
%# Cours, vole où la gloire t'appelle!
\livretPers Le vieil Horace
%# Quel Romain n'envieroit cet immortel honneur!
%# Tous brigueroient en foule une mort aussi belle.
\livretPers Les chevaliers
%# O de Rome *heureux défenseur,
%# Cours, vole où la gloire t'appelle!
\livretPers Horace
%# Amis, vous embrasez mon cœur!
%# Je vole où la gloire m'appelle…
%# Mais un soldat Albain s'avance ici vers nous.
\livretScene SCÈNE III
\livretDescAtt\wordwrap-center {
  Un envoyé d’Albe, les Précédens.
}
\livretPersDidas Curiace à l’Albain
\livretRef #'BCArecit
%# Quels sont les trois guerriers que le choix d’Albe *honore?
\livretPers L'Albain
%# Seigneur! l'ignorez-vous encore?
\livretPers Curiace
%#- Achevez, qui?
\livretPers L'Albain
%#= Vos deux frères & vous.
\livretPers Camille
%#- Curi=ace!
\livretPersDidas Curiace & les Romains à voix basse
%#- Grands Dieux!
\livretPers L'Albain
%#= Vous semblez vous confondre,
%#- Blâmeriez-vous ce choix?
\livretPers Curiace
%#= Il a dû me confondre.
%# Je m'estimois trop peu pour un si grand honneur.
%#- Ah! Camille!
\livretPers L'Albain
%#= Au Sénat que dirai-je, seigneur?
\livretPers Curiace
%# Que mon cœur, pénétré de cette grace insigne,
%# N'osoit pas y compter… mais qu'il s'en rendra digne.
\livretScene SCÈNE IV
\livretDescAtt\wordwrap-center {
  Les mêmes excepté l’Albain.
}
\livretPers Les chevaliers
\livretRef #'BDArecit
%# O déplorable choix! triste & funeste *honneur!
\livretPers Curiace
%# O devoir rigoureux que l'*honneur nous impose!
\livretPersDidas Camille à Horace & à Curiace
%# Ciel! & quoi votre cœur
%# Ne se révolte pas à la loi qu'on propose!
\livretPers Les chevaliers
%# O déplorable choix! triste & funeste *honneur!
\livretPers Camille
%# Ah! c'est un crime affreux qui doit vous faire *horreur.
\livretPers Horace
%# Appellez-vous forfait de servir sa patrie?
\livretPers Camille
%# Appellez-vous vertu, cet attentat impie?
\livretPers Les chevaliers
%# O déplorable choix! triste & funeste *honneur!
\livretPers Horace
%# Je conçois tout notre malheur;
%# Il peut nous étonner, mais non pas nous abattre.
%# Toi, reste, Curi=ace, & console ma sœur:
%# Je viendrai te rejoindre, & nous irons combattre.
\livretPers Le vieil Horace
%# Vertu digne de Rome! ô mon fils! mon cher fils!
%# Voilà les sentimens que mon cœur t'a transmis!
\livretDidasP\line { (A Curiace & à Horace.) }
%# Oui, mes enfans, votre infortune est grande,
%# Mon cœur, comme le vôtre, en a senti les coups:
%# Mais l'effort est digne de vous,
%# Et tout cède à l'*honneur alors qu'il nous commande.
%# Je plains Camille, & permets sa douleur;
%# Son malheur, sans doute, est extrême:
%# C'est à toi, Curi=ace, à raffermir son cœur;
%# Rends-la digne de nous & digne de toi-même.
\livretDidasP\line { (Il sort, les Chevaliers & Horace le suivent.) }
\livretScene SCÈNE V
\livretDescAtt\wordwrap-center {
  \smallCaps { Camille, Curiace. }
}
\livretPers Camille
\livretRef #'BEAcamilleCuriace
%#- Iras-tu, Curi=ace?
\livretPers Curiace
%#= Ah! dans ce jour fatal,
%# Je n'ai plus que le choix du crime;
%# Pour moi le malheur est égal,
%# Et partout, sous mes pas, le sort creuse un abîme.
\livretRef #'BEBcuriace
%# Victime de l'amour, victime de l'*honneur,
%# Il faut trahir Camille, ou trahir ma patrie.
%# L'un & l'autre est affreux, l'un & l'autre est impie…
%# Mon cœur s'en effarouche, & j'en frémis d'*horreur.
%# Je sens ma vertu qui chancelle,
%# Ne cherche point à m'attendrir;
%# C'est l'*honneur même qui m'appelle,
%# Camille, il lui faut obé=ir.
\livretPers Camille
\livretRef #'BECcamilleCuriace
%# Non, je te connois mieux; non, tu n'es point barbare.
%# L'amitié, la nature, & l'*hymen & l'amour,
%# Rappelleront ta raison qui s'égare:
%# Tu ne trahiras point tant de droits en ce jour.
\livretPers Curiace
%#- Hélas!
\livretPers Camille
%#= Et que prétends-tu faire?
%# As-tu conçu l'*horreur de cet ordre inhumain?
%# Tu viendras donc m'offrir ta main
%# Fumante du sang de mon frère?
\livretPers Curiace
%#- Ah! laissez-moi, Camille!
\livretPers Camille
%#= Eh! quoi!
%# Ton nom consacré par la gloire,
%# N'est-il donc pas fameux par plus d'une victoire?
%# Albe n'a-t-elle enfin d'autres guerriers que toi?
\livretPers Curiace
%# Ciel! que proposes-tu? tu veux que Curi=ace
%# D'un opprobre éternel se souille lâchement,
%# Qu'il déshonore & lui-même & sa race?…
%# Ah! tu l'espère vainement.
\livretAlt Livret imprimé
%# La vertu rentre dans mon ame,
%# L'*honneur doit surmonter l'amour:
%# Camille, il vaut mieux, en ce jour,
%# Mourir en te perdant, que de vivre en infâme.
\livretPers Camille
%# Eh bien! je ne te retiens plus:
%# Cours, acheter l'*honneur au prix d'un parricide,
%# Cède à la fureur qui te guide,
%# Mais que du moins avant tous nos nœuds soient rompus,
%#- Frappe, ingrat!
\livretPers Curiace
%#= Je n'entends plus rien;
%#- Je vous fuis…
\livretPersDidas Camille arrêtant Curiace & se jettant à ses pieds.
%#= A tes pieds tu veux donc que je meure?
\livretPers Curiace
%#- Camille! vous pleurez!
\livretPers Camille
%#= Hélas! il le faut bien;
%# Quand ta main m'assassine, il faut bien que je pleure!
\livretDidasP\wordwrap {
  (Curiace la relève ; il est attendri, elle continue :)
}
\livretAltEnd
\livretRef #'BEDcamilleCuriace
%# Par l'amour & par l'amitié,
%# Par ce nœud si doux qui nous lie!
%# Ne te montre point sans pitié,
%# C'est Camille en pleurs, qui te prie.
\livretPers Curiace
%# Hélas! tu déchire mon cœur,
%# Camille, ta douleur m'accable:
%# Laisse ton amant déplorable
%# Mourir, victime de l'*honneur.
\livretPers Camille
\livretRef #'BEEcamilleCuriace
%# O Ciel! quoi, ma pri=ère est vaine;
%# Je n'ai plus sur toi de pouvoir!
\livretPers Curiace
%# Tu sais qu'un barbare devoir
%# Commande à mon cœur & l'entraîne.
\livretPers Camille
%# Tu ne te souviens plus que ton cœur est à moi?
\livretPers Curiace
%# J'étois à mon pa=ys avant que d'être à toi.
\livretPers Ensemble
\livretPers Curiace
\livretRef #'BEFcamilleCuriace
%# O sort cru=el! devoir barbare
%# Hélas! faut-il vous obé=ïr?
%# Camille, il faut me fuir,
%# Le Ciel pour jamais nous sépare:
%# Oui, c'en est fait il faut partir.
\livretPers Camille
%# Cœur insensible! amant barbare!
%# Ainsi rien ne peut te fléchir!
%# Cru=el, peux-tu me fuir!
%# Je sens que ma raison s'égare,
%# C'en est fait: je me sens mourir.
\livretScene SCÈNE VI
\livretDescAtt\wordwrap-center {
  \smallCaps { Horace, Curiace, Camille. }
}
\livretPers Horace
\livretRef #'BFArecit
%# Ne tardons plus: viens, suis-moi, Curi=ace!
\livretPers Curiace
%#- Marchons!
\livretPers Camille
%#= Non, demeurez; je ne vous quitte pas.
\livretPers Horace
%# Ma sœur, quelle est donc cette audace?
%#- Ah! marchons…
\livretPers Camille
%#= Non, cru=els, je m'attache à vos pas.
%# Vous ne commettrez pas ce crime abominable.
\livretPers Curiace
%# Horace! ah! retenez ces horribles éclats.
\livretPers Horace
%# Cet excès de foiblesse, ô Ciel! est-il croy=able.
\livretPers Camille
%# Je veux intéresser Albe & Rome à mes cris.
%# Voy=ez quelle rage est la vôtre!
%# O Ciel! deux frères, deux amis
%# Brûlent de se baigner dans le sang l'un de l'autre.
\livretScene SCÈNE VII
\livretDescAtt\wordwrap-center {
  \smallCaps { Le vieil Horace, } les Précédens.
}
\livretPers Horace
\livretRef #'BGArecit
%#- Mon père!…
\livretPers Le vieil Horace
%#= Mes enfans, il est temps de partir.
\livretAlt Livret imprimé
\livretPers Horace
%# Par ses vaines clameurs, ma sœur nous désespère.
\livretPers Curiace
%# Seigneur, daignez la retenir.
\livretPers Le vieil Horace
%#- Allons, rentrez Camille.
\livretPers Camille
%#= Et vous aussi, mon père!
\livretAltB Partition
\livretPers Le vieil Horace
%#- Vous, Camille, rentrez.
\livretPers Camille
%#= Mon père!
\livretAltEnd
%# Quoi! sur vous la nature a si peu de pouvoir?
\livretPers Le vieil Horace
%# La nature se taît où parle le devoir.
\livretPers Ensemble
\livretPersVerse Le vieil Horace \line {
  \hspace#7 \column {
    \livretVerse#8 { Oui mes enfans partez sur l’heure. }
    \livretVerse#8 { Allez remplir votre devoir }
    \livretVerse#10 { Laissez Camille, & son vain désespoir. }
  }
}
\livretPersVerse Curiace & Horace \line {
  \hspace#7 \column {
    \livretVerse#8 { Allons, ami, partons sur l’heure }
    \livretVerse#8 { Allons remplir notre devoir : }
  }
}
\livretPersVerse Horace \line {
  \hspace#7 \livretVerse#10 { Laissons Camille, & son vain désespoir. }
}
\livretPersVerse Curiace \line {
  \hspace#7 \livretVerse#10 { Allons éteindre un amour sans espoir. }
}
\livretPersVerse Camille \line {
  \hspace#7 \column {
    \livretVerse#8 { Quoi c’est donc en vain que je pleure ! }
    \livretVerse#8 { Quoi ? rien ne peut vous émouvoir : }
    \livretVerse#8 { On méprise mon désespoir. }
  }
}
\livretPersDidas Curiace au vieil Horace
%# Seigneur, en ce moment funeste,
%#- Puis-je encor?…
\livretPers Le vieil Horace
%#= Je t'entends: ne viens point m'attendrir
%# Va: remplis ton devoir… les Dieux feront le reste.
\livretPers Camille
%# Tigres, allez combattre, & moi je vais mourir.
\livretPers Le vieil Horace
%# Ma fille, allons, rentrez & laissez-les partir,
\livretPers Ensemble
\livretPersVerse Le vieil Horace \line {
  \hspace#7 \livretVerse#8 { Oui mes enfans, &c. }
}
\livretPersVerse Horace & Curiace \line {
  \hspace#7 \livretVerse#8 { Allons, ami, partons, &c. }
}
\livretPersVerse Camille \line {
  \hspace#7 \livretVerse#8 { Quoi c’est donc, &c. }
}
\livretFinAct Fin du second acte
\sep
\livretAct SECOND INTERMÈDE
\livretDescAtt\column {
  \justify {
    Le Théâtre représente une campagne des environs de Rome
    où les armées de Rome & d’Albe sont en présence.
    Les trois Horaces sont auprès du roi de Rome, & les
    trois Curiaces auprès du dictateur d’Albe.
  }
  \justify {
    Un autel est placé au milieu des deux armées. Il est
    censé situé sur la ligne qui sépare les territoires
    des deux différens états.
  }
  \justify {
    Un Grand-Sacrificateur, & plusieurs Prêtres inférieurs
    entourent l’Autel.
  }
}
\livretPers Le grand sacrificateur
\livretRef #'BHAsacrificateur
%# Romains, Albains! ce jour prévient votre ruine.
%# Ce pa=ys inculte & désert,
%# De vos longs différens a trop long-temps souffert;
%# Le Ciel pour jamais les termine.
%# Six guerriers choisis parmi vous,
%# Vont décider du sort de l'un & l'autre empire:
%# Ce jour même la guerre expire,
%# Et ce dernier combat nous ré=unira tous.
\livretRef #'BHBchoeur
%# Jurez au nom des Dieux, par l'*honneur & la gloire,
%# D'étouffer tout esprit de vengeance & d'aigreur:
%# Qu'Albe ou Rome en ce jour obtienne la victoire,
%# Jurez tous d'obé=ir au parti du vainqueur.
\livretPers Les chefs des deux armées
%# Nous jurons tous aux Dieux, par l'*honneur & la gloire
%# D'étouffer tout esprit de vengeance & d'aigreur.
%# Qu'Albe ou Rome en ce jour emporte la victoire,
%# Nous jurons d'obé=ir au parti du vainqueur.
\livretPers Tous
%# Nous jurons d'obé=ir au parti du vainqueur.
\livretAlt Partition
%# Les Horaces, des freres.
%# Les Curiaces, des amis.
\livretAltEnd
\null
\livretDidasP\justify {
  Après le serment, on donne le signal du combat dans les
  deux camps. Les six Champions sont conduits en présence.
  Dès qu'ils paroissent, les deux armées s’écrient en même temps :
}
%#- Ciel!
\livretDidasP\justify {
  Le signal recommence.
}
\livretPers Les deux armées
%#= O crime! ô honte éternelle!
%# La guerre même étoit moins criminelle
%# Que cet horrible choix.
\livretPers Les Horaces & les Curiaces
%# Allons, volons où l'*honneur nous appelle.
\livretPers Les deux armées
%# De la terre & du Ciel c'est outrager les loix.
\livretPers Les Horaces & les Curiaces
%# Allons, volons où l'*honneur nous appelle.
\livretPers Les deux armées
%# Nous ne souffrirons pas ce combat plein d'*horreur.
\livretPers Les Horaces & les Curiaces
%#- Avançons…
\livretPers Les deux armées
%#- Arrêtez!
\livretDidasP\justify {
  Une foule de soldats des deux Armées quittent ses rangs.
  Ils se précipitent malgré les efforts des chefs pour les
  retenir. Ils veulent séparer les Champions qui s’obstinent
  au combat.
}
\livretDidasP\line {
  Pendant cette Pantommime.
}
\livretPers Les chefs de l'armée
%#= Révolte punissable!
\livretPers Les Horaces & les Curiaces
%#- Laissez-nous.
\livretPers Les chefs de l'armée
%#= De vos chefs reconnoissez la voix.
\livretPers Les deux armées
%# Qu'Albe fasse un autre choix!
%# Que Rome fasse un autre choix!
\livretPers Les Horaces & les Curiaces
%#- Laissez-nous.
\livretPers Les chefs de l'armée
%#= De vos chefs reconnoissez la voix.
\livretPers Les deux armées
%# Nous ne souffrirons point ce meurtre abominable
\livretAlt Partition
%# De la terre & du Ciel c'est outrager les loix.
\livretAltEnd
%# Oui, la guerre étoit moins coupable
%# Que cet horrible choix.
\livretDidasP\justify {
  La guerre est prête à s'allumer ; les deux rois s’approchent
  du Grand-Prêtre, ainsi que les principaux chefs de l’armée :
  ils délibèrent un moment ; & le grand Prêtre s’écrie :
}
%# Héros d’Albe & de Rome, & vous chefs & soldats,
%# Ecoutez!… =& cessez d'inutiles débats.
%# Si le choix du Sénat vous blesse,
%# Allez tous consulter vos Dieux:
%# Qu'ils soit ou non désaprouvé par eux;
%# Quel profane osera condamner leur sagesse!
\livretPers Les deux armées
\livretRef #'BHCchoeur
%# Oui, que les Dieux décident entre-nous!
\livretAlt Livret imprimé
%# Quoi qu'il puisse arriver, nous obé=irons tous.
\livretAltB Partiton
%# À leur choix quel qu'il soit, nous nous soumettons tous.
\livretAltEnd
\livretDidasP\justify {
  On sépare les six Champions. On les emmène & l’intermède finit.
}
\livretFinAct Fin de l’intermède
\sep
