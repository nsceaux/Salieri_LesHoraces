\livretAct ACTE PREMIER
\livretDescAtt\wordwrap-center {
  Le Théâtre représente l’extérieur du Temple d’Egérie,
  au milieu de l’enceinte qui lui est consacrée.
}
\livretScene SCÈNE PREMIÈRE
\livretDescAtt\wordwrap-center {
  \smallCaps Camille suivie de ses femmes, jeunes filles
  qui portent des offrandes : elles restent au fond du Théatre,
  Camille s’avance avec ses femmes.
}
\livretPers Une des femmes
\livretRef #'AABcamille
%# D’où naît le trouble qui vous presse?
%# Vous tremblez à l’aspect de ces augustes lieux!
%# Un mot peut dissiper cette sombre tristesse,
%# Osez sur vos destins interroger les Dieux.
\livretPers Camille
%# J’ai déja prévu leur réponse,
%# Un noir pressentiment d’avance me l’annonce:
%# Un mot peut me donner la mort,
%# Hélas! fuy=ons plutôt sans connoître mon sort,
%#- Fuy=ons.
\livretPers Une des femmes
%#- Non, demeurez.
\livretPers Camille
%#= Que faut-il que j’espère?
\livretPers Une des femmes
%# Qu’Albe ou Rome tri=omphe, en ce moment fatal,
%#- Votre père pourrait…
\livretPers Camille
%#= Que tu le connois mal!
%# Ces noms si doux & de fille & de père
%# Dans son cœur tout Romain sont des noms sans pouvoir.
%# Et sur quoi fonder mon espoir?
%# Pensez-vous que pour gendre il accep=tât un homme
%# Que seroit ou l’esclave ou le maître de Rome?
\livretPers Une des femmes
%# Lui-même enfin, lui-même avoit formé ces nœuds.
\livretPers Camille
%# Ah! ce jour à la fois heureux & malheureux
%# Fit naître & détruisit ma plus chère espérance.
%# Mon père me donnoit à l’objet de mes vœux,
%# Albe et Rome approuvoient cette illustre alli=ance.
%# Soudain, le sort jaloux
%# Des deux états détruit l’intelligence,
%# La guerre en un moment brise des nœuds si doux.
%# Un même jour me donne & m’ôte à ce que j’aime,
%# Le malheur naît pour moi du sein du bonheur même.
\null
\livretRef #'AACcamille
%# Pour Albe, *hélas! quels vœux me sont permis?
%# Je ne puis séparer sa cause de la nôtre:
%# Mon cœur, entre les deux, flotte encore indécis,
%# Mes frères sont pour l'une & mon amant pour l'autre;
%# D'un & d'autre côté je ne vois que malheurs;
%# N'espérez pas que j'y survive.
%# Ah! je devrai, quoi qu'il arrive,
%# Mes larmes aux vaincus & ma haine aux vainqueurs.
\livretDidasP\wordwrap {
  (Le fond du Théatre s’ouvre & laisse voir la statue d’Egérie.)
}
\livretPers Une des femmes
\livretRef #'AADcamille
%# Déja le sanctu=aire s'ouvre,
%# D'Egérie =à nos yeux l'image se découvre:
%#- Avancez.
\livretPers Camille
%#= Je frémis, vous, soutenez mes pas,
%# Allons, je vais chercher la vie =ou le trépas.
\livretRef #'AAEsinfonia
\livretDidasPPage\wordwrap {
  (Les jeunes filles portant les offrandes, précedent & suivent
  Camille sur une marche religieuse. Elles déposent leurs dons au pied
  de l’autel.)
}
\livretRef #'AAFcamilleChoeur
%# Dé=esse secourable,
%# Je t'invoque en tremblant;
%# Du doute qui m'accable
%# Fais cesser le tourment.
%# Faut-il que je renonce
%# À la plus tendre ardeur?
%# Hélas! tout mon bonheur
%# Dépend de ta réponse.
\livretPers Chœur
%# Faut-il qu'elle renonce
%# À la plus tendre ardeur?
%# Hélas! tout son bonheur
%# Dépend de ta réponse.
\livretPers L'oracle
\livretRef #'AAGoracle
%# La guerre entre Albe & Rome aujourd'hui doit finir:
%# Ce jour à ton amant va pour jamais t'unir.
\livretDidasP\wordwrap {
  (La statue se recouvre.)
}
\livretPers Camille
\livretRef #'AAHcamille
%# Ce jour à ton amant va pour jamais t'unir;
%# Pour jamais… mon cher Curi=ace!
%# Mais où m'emporte un espoir trop flatteur!
%# Quand de tous ces flé=aux la guerre nous menace,
%# Malheureuse! est-ce à moi d'oser croire au bonheur?…
%# Mais quoi! l'oracle est la voix des Dieux mêmes.
%# Je l'ai bien entendu, ce n'est point une erreur,
%# Quand le Ciel a parlé, le doute est un blasphême.
\null
\livretRef #'AAIcamille
%# Oui, mon bonheur est assuré
%# Je ne puis en douter sans crime.
%# Mon cœur, de plaisir enivré,
%# Cède au doux espoir qui l'anime.
%# Oui, tous nos malheurs sont passés,
%# Reviens, ô mon cher Curi=ace,
%# Reviens, & que ta main efface
%# Les pleurs cru=els que j'ai versés.
\livretScene SCÈNE II
\livretDescAtt\wordwrap-center {
  Le peuple en foule inonde les portiques du temple :
  il doit être composé de femmes, d’enfans & de vieillards.
}
\livretPers Chœur
\livretRef #'ABAcamilleChoeur
%# Secourez-nous, ô puissante Egérie,
%# Protègez de Numa le peuple infortuné.
\livretPers Camille
%#- Ciel! & quoi?
\livretPers Un coriphée
%#= Du combat le signal est donné.
\livretPers Camille
%#- Je meurs!
\livretPers Une partie du peuple
%#= Malheureuse patrie!
\livretPers Tous
%# Secoure-nous, ô puissante Egérie,
%# Protège de Numa le peuple infortuné.
\livretPers Camille
%# Et voilà donc la foi que l'on doit aux oracles!
%# Pourquoi d'un faux bonheur m'annoncer les appas,
%# Dieux vains! tremblante, hélas! devant vos tabernacles,
%# J'aurois cru faire un crime en ne vous croy=ant pas.
%# Voilà donc le bonheur dont je m'étois flattée!
%#- Hélas!
\livretPers Le Chœur
%#= Dé=esse redoutée,
%# Dans ce moment peut-être on est aux mains
%# Veille sur nous, combats pour les Romains.
\livretScene SCÈNE III
\livretDescAtt\wordwrap-center {
  Le vieil \smallCaps { Horace, Horace, Curiace, }
  Chevaliers d’Albe & de Rome, les Précédents.
}
\livretPers Le vieil Horace
\livretRef #'ACAhccChoeur
%# Peuples, dissipez vos alarmes,
%# Les Dieux nous ont donné la paix.
\livretPers Camille et le peuple
%#- Ciel!
\livretPers Le vieil Horace
%#= Albe & Rome ont déposé leurs armes.
\livretPersDidas Camille avec transport
%# Grands Dieux! Ah! pardonnez mes transports indiscrets!
\livretPersDidas Curiace à Camille
%# Chere Camille, enfin je puis revoir vos charmes.
\livretPers Camille
%# Ah! tu m'as coûté bien des larmes!
\livretPers Le peuple
%# Quel miracle a produit ces étonnans effets?
\livretPers Le vieil Horace
\livretRef #'ACBhoraceChoeur
%# Un Dieux, qui parle aux cœurs. Déja les deux armées
%# D'une égale fureur paroissoient animées;
%# On alloit en venir aux mains…
%# Entre les deux partis soudain Tulle s'avance:
%# On s'arrête, on l'entoure, on l'écoute en silence:
%# Albains, dit-il, & vous, écoutez-moi Romains!
\null
%# Dieux! quelles fureurs sont les nôtres.
%# Je vois à notre aspect la nature frémir!
%# Vos fils sont nos neveux, nos filles sont les vôtres,
%# Le sang de mille nœuds a voulu nous unir.
%# D'un sang si préci=eux pourquoi souiller la terre?
%# Qu'entre vous, qu'entre nous trois guerriers soient choisis!
%# Et que notre intérêt, entre leurs mains remis,
%# Fasse un simple combat d'une effroy=able guerre.
\livretPers Le peuple
%# O roi, le modèle des rois!
%# Oui, les Dieux t'inspiroient, ils parloient par ta voix!
\livretPers Le vieil Horace
%# Vous eussiez vu soudain dans l'une & l'autre armée
%# La joie =& la concorde enflammer tous les cœurs.
%# D'un égal intérêt l'une & l'autre animée
%# Ne songe plus qu'au choix de ses trois défenseurs.
%# Romains, quelle gloire à prétendre!
%# Trop heureux les héros qui sauront nous défendre!
\livretPers Le jeune Horace
%#- Que je leur porte envie!
\livretPers Le vieil Horace
%#= O noble & cher transport!
%# Les Dieux veillent sur ma patrie,
%# Et dans des dignes mains ils remettront son sort.
\livretDidasP\line { (à Curiace.) }
%# Toi dont Albe se glorifie,
%# Par tes hautes vertus dignes d'être Romain
%# Deviens mon fils en recevant sa main.
\livretPers Camille & Curiace
%#- Mon père!
\livretPersDidas Le vieil Horace à Camille
%#= Tu le peux, sans hasarder ma gloire
%# Soit que Rome tri=omphe ou qu'Albe ait la victoire
%# Le vaincu du vainqueur reconnoîtra les loix,
%# Sans honte, sans tributs servils;
%# Et nos états, unis par choix,
%# Ne seront qu'un empire & qu'un peuple en deux villes.
\livretPers Le peuple
\livretRef #'ACCquatuorChoeur
%# O du sort trop heureux retour?
\livretPers Camille & Curiace
%# Que nous devons de graces à l'amour.
\livretPers Camille
%#- Curi=ace!
\livretPers Curiace
%#- Camille!
\livretPers Camille
%#- O mon père!
\livretPers Le vieil Horace
%#= Ma fille!
\livretPers Curiace & Camille
%# Que nous devons de graces à l'amour!
\livretPers Le vieil Horace
%# O Ciel! sur Rome & ma famille
%# Verse tes bienfaits en ce jour!
\livretPers Curiace & Camille
%# Du sein des plus rudes alarmes
%# Ainsi le bonheur naît pour nous.
\livretPers Le vieil Horace
%# Il doit en paroître plus doux:
%# Le malheur lui prête des charmes.
\livretPers Le peuple
%# O du sort trop heureux retour!
\livretPers Camille & Curiace
%# Que nous devons de graces à l'amour!
\livretPers Le peuple, Curiace, & Camille
%# O Ciel sur Horace & sa fille
%# Verse tes bienfaits en ce jour.
\livretPers Le vieil Horace, le jeune Horace
%# O Ciel sur Rome & ma famille
%# Verse tes bienfaits en ce jour.
\livretDescAtt\wordwrap-center{ DIVERTISSEMENT GÉNÉRAL }
\livretFinAct Fin du premier acte
\sep
\livretAct PREMIER INTERMEDE
\livretDescAtt\column {
  \justify {
    Le Théâtre représente le Temple de Jupiter-Capitolin.
    On voit dans le fond, l’Autel & la Statue de ce Dieu.
    Le Roi, les principaux Chefs de l’Armée, & le Sénat Romain
    occupent le Sanctuaire. Le peuple est sur
    la partie extérieure.
  }
  \justify {
    Les Prêtres entrent sur une marche noble & imposante.
  }
}
\livretPers Le grand prêtre
\livretRef #'ADApretre
%# Le Sénat, rassemblé sous ces voutes sacrées,
%# Va choisir trois Héros pour être nos vengeurs:
%# Puissent nos voix, par les Dieux inspirées,
%# Nommer à cet état de dignes défenseurs!
%# Vous, Romains, à nos vœux unissez vos pri=ères,
%# Le salut de l'Etat dépend de ce grand choix:
%# Pri=ez les Dieux protecteurs de nos loix,
%# De verser sur nous leurs lumières.
\livretPers Le grand prêtre
\livretRef #'ADBpretreChoeur
%# Puissant moteur de l'univers,
%# O toi dont l'essence suprême
%# Assujettit le Destin même,
%# Que sur nous, tes yeux soient ouverts.
\livretPers Le grand prêtre
%# Foibles jou=ets des destinées,
%# Que pouvons-nous sans ton secours?
%# C'est lui seul qui de nos années
%# Arrête ou prolonge le cours.
\livretPers Le peuple
%#8 Puissant moteur, &c.
\livretPers Le grand prêtre
%# Les jours tristes, les jours sereins,
%# La douce paix, l'affreuse guerre,
%# Et la rosée =& le tonnerre,
%# Tout part de tes puissantes mains.
\livretPers Le peuple
%#8 Puissant moteur, &c.
\livretAlt Livret imprimé
\livretPers Le grand prêtre
%# Hélas! devant ton trône auguste
%# Que sont tous les foibles humains?
%# Mais ta voix règle leurs destins,
%# Et c'est l'espérance du juste.
\livretPers Le peuple
%#8 Puissant moteur, &c.
\livretAltEnd
\null
\livretRef #'ADCmarche
\livretDidasPPage\justify {
  Après l’Hymne, on brûle l’encens, on fait les libations, &c.
  Les cérémonies finies avec la pompe convenable, le Grand-Prêtre
  s’avance du côté du peuple & chante l’air suivant.
}
\livretPers Le grand prêtre
\livretRef #'ADDpretreChoeur
%# O Rome! ô ma patrie!
%# Choisis, on te présente ou le sceptre, ou des fers.
%# Eveille ton puissant génie,
%# Souviens-toi que les Dieux t'ont promis l'univers,
%# L'espoir de tes enfans sur leurs décrets se fonde,
%# Jupiter même a réglé ton destin:
%# Tes ennemis t'attaqueront en vain,
%# Rome doit être un jour la maîtresse du monde.
\livretPers Le peuple
%# Ses ennemis l'attaqueront en vain,
%# Rome doit être un jour la maîtresse du monde.
\null
\livretDidasP\justify {
  Le Grand-Prêtre revient à l’Autel, finit les sacrifices,
  puis se retournant du côté du sénat, il dit :
}
\livretPers Le grand prêtre
%# Roi, Pontifes, Sénat, ré=unissez-vous tous:
%# Que nos trois défenseurs soient nommés ce jour même !
%# Sur le grand choix que Rome attend de vous,
%# Je vous promets des Dieux l'assistance suprême.
\null
\livretRef #'ADEsinfonia
\livretDidasPPage\justify {
  Le Roi, les Prêtres, les Chefs de l’armée, le Sénat
  se retirent sur une marche regulière & sont supposés
  aller dans la Salle du Capitole, destinée aux assemblées
  du Sénat.
}
\livretPers Le peuple
\livretRef #'ADFchoeur
%# O Dieux, défenseurs de nos loix
%# Inspirez le Sénat & parlez par sa voix!
\livretFinAct Fin de l’intermède
\sep
