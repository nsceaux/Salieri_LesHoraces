\notesSection "Livret"
\markuplist\abs-fontsize-lines#8 \page-columns-title \act\line { LIVRET } {
\livretAct ACTE PREMIER
\livretDescAtt\wordwrap-center {
  Le Théâtre représente l’extérieur du Temple d’Egérie,
  au milieu de l’enceinte qui lui est consacrée.
}
\livretScene SCÈNE PREMIÈRE
\livretDescAtt\wordwrap-center {
  \smallCaps Camille suivie de ses femmes, jeunes filles
  qui portent des offrandes : elles restent au fond du Théatre,
  Camille s’avance avec ses femmes.
}
\livretPers Une des femmes
\livretRef #'AABcamille
\livretVerse#8 { D’où naît le trouble qui vous presse ? }
\livretVerse#12 { Vous tremblez à l’aspect de ces augustes lieux ! }
\livretVerse#12 { Un mot peut dissiper cette sombre tristesse, }
\livretVerse#12 { Osez sur vos destins interroger les Dieux. }
\livretPers Camille
\livretVerse#8 { J’ai déja prévu leur réponse, }
\livretVerse#12 { Un noir pressentiment d’avance me l’annonce : }
\livretVerse#8 { Un mot peut me donner la mort, }
\livretVerse#12 { Hélas ! fuyons plutôt sans connoître mon sort, }
\livretVerse#12 { Fuyons. }
\livretPers Une des femmes
\livretVerse#12 { \transparent { Fuyons. } Non, demeurez. }
\livretPers Camille
\livretVerse#12 { \transparent { Fuyons. Non, demeurez. } Que faut-il que j’espère ? }
\livretPers Une des femmes
\livretVerse#12 { Qu’Albe ou Rome triomphe, en ce moment fatal, }
\livretVerse#12 { Votre père pourrait… }
\livretPers Camille
\livretVerse#12 { \transparent { Votre père pourrait… } Que tu le connois mal ! }
\livretVerse#10 { Ces noms si doux & de fille & de père }
\livretVerse#12 { Dans son cœur tout Romain sont des noms sans pouvoir. }
\livretVerse#8 { Et sur quoi fonder mon espoir ? }
\livretVerse#12 { Pensez-vous que pour gendre il acceptât un homme }
\livretVerse#12 { Que seroit ou l’esclave ou le maître de Rome ? }
\livretPers Une des femmes
\livretVerse#12 { Lui-même enfin, lui-même avoit formé ces nœuds. }
\livretPers Camille
\livretVerse#12 { Ah ! ce jour à la fois heureux & malheureux }
\livretVerse#12 { Fit naître & détruisit ma plus chère espérance. }
\livretVerse#12 { Mon père me donnoit à l’objet de mes vœux, }
\livretVerse#12 { Albe et Rome approuvoient cette illustre alliance. }
\livretVerse#6 { Soudain, le sort jaloux }
\livretVerse#10 { Des deux états détruit l’intelligence, }
\livretVerse#12 { La guerre en un moment brise des nœuds si doux. }
\livretVerse#12 { Un même jour me donne & m’ôte à ce que j’aime, }
\livretVerse#12 { Le malheur naît pour moi du sein du bonheur même. }
\null
\livretRef #'AACcamille
\livretVerse#10 { Pour Albe, hélas ! quels vœux me sont permis ? }
\livretVerse#12 { Je ne puis séparer sa cause de la nôtre : }
\livretVerse#12 { Mon cœur, entre les deux, flotte encore indécis, }
\livretVerse#12 { Mes frères sont pour l’une & mon amant pour l’autre ; }
\livretVerse#12 { D’un & d’autre côté je ne vois que malheurs ; }
\livretVerse#8 { N’espérez pas que j’y survive. }
\livretVerse#8 { Ah ! je devrai, quoi qu’il arrive, }
\livretVerse#12 { Mes larmes aux vaincus & ma haine aux vainqueurs. }
\livretDidasP\wordwrap {
  (Le fond du Théatre s’ouvre & laisse voir la statue d’Egérie.)
}
\livretPers Une des femmes
\livretRef #'AADcamille
\livretVerse#8 { Déja le sanctuaire s’ouvre, }
\livretVerse#12 { D’Egérie à nos yeux l’image se découvre : }
\livretVerse#12 { Avancez. }
\livretPers Camille
\livretVerse#12 { \transparent { Avancez. } Je frémis, vous, soutenez mes pas, }
\livretVerse#12 { Allons, je vais chercher la vie ou le trépas. }
\livretRef #'AAEsinfonia
\livretDidasPPage\wordwrap {
  (Les jeunes filles portant les offrandes, précedent & suivent
  Camille sur une marche religieuse. Elles déposent leurs dons au pied
  de l’autel.)
}
\livretRef #'AAFcamilleChoeur
\livretVerse#6 { Déesse secourable, }
\livretVerse#6 { Je t’invoque en tremblant ; }
\livretVerse#6 { Du doute qui m’accable }
\livretVerse#6 { Fais cesser le tourment. }
\livretVerse#6 { Faut-il que je renonce }
\livretVerse#6 { À la plus tendre ardeur ? }
\livretVerse#6 { Hélas ! tout mon bonheur }
\livretVerse#6 { Dépend de ta réponse. }
\livretPers Chœur
\livretVerse#6 { Faut-il qu’elle renonce }
\livretVerse#6 { À la plus tendre ardeur ? }
\livretVerse#6 { Hélas ! tout son bonheur }
\livretVerse#6 { Dépend de ta réponse. }
\livretPers L'oracle
\livretRef #'AAGoracle
\livretVerse#12 { La guerre entre Albe & Rome aujourd’hui doit finir : }
\livretVerse#12 { Ce jour à ton amant va pour jamais t’unir. }
\livretDidasP\wordwrap {
  (La statue se recouvre.)
}
\livretPers Camille
\livretRef #'AAHcamille
\livretVerse#12 { Ce jour à ton amant va pour jamais t’unir ; }
\livretVerse#8 { Pour jamais… mon cher Curiace ! }
\livretVerse#10 { Mais où m’emporte un espoir trop flatteur ! }
\livretVerse#12 { Quand de tous ces fléaux la guerre nous menace, }
\livretVerse#12 { Malheureuse ! est-ce à moi d’oser croire au bonheur ?… }
\livretVerse#10 { Mais quoi ! l’oracle est la voix des Dieux mêmes. }
\livretVerse#12 { Je l’ai bien entendu, ce n’est point une erreur, }
\livretVerse#12 { Quand le Ciel a parlé, le doute est un blasphême. }
\null
\livretRef #'AAIcamille
\livretVerse#8 { Oui, mon bonheur est assuré }
\livretVerse#8 { Je ne puis en douter sans crime. }
\livretVerse#8 { Mon cœur, de plaisir enivré, }
\livretVerse#8 { Cède au doux espoir qui l’anime. }
\livretVerse#8 { Oui, tous nos malheurs sont passés, }
\livretVerse#8 { Reviens, ô mon cher Curiace, }
\livretVerse#8 { Reviens, & que ta main efface }
\livretVerse#8 { Les pleurs cruels que j’ai versés. }
\livretScene SCÈNE II
\livretDescAtt\wordwrap-center {
  Le peuple en foule inonde les portiques du temple :
  il doit être composé de femmes, d’enfans & de vieillards.
}
\livretPers Chœur
\livretRef #'ABAcamilleChoeur
\livretVerse#10 { Secourez-nous, ô puissante Egérie, }
\livretVerse#12 { Protègez de Numa le peuple infortuné. }
\livretPers Camille
\livretVerse#12 { Ciel ! & quoi ? }
\livretPers Un coriphée
\livretVerse#12 { \transparent { Ciel ! & quoi ? } Du combat le signal est donné. }
\livretPers Camille
\livretVerse#8 { Je meurs ! }
\livretPers Une partie du peuple
\livretVerse#8 { \transparent { Je meurs ! } Malheureuse patrie ! }
\livretPers Tous
\livretVerse#10 { Secoure-nous, ô puissante Egérie, }
\livretVerse#12 { Protège de Numa le peuple infortuné. }
\livretPers Camille
\livretVerse#12 { Et voilà donc la foi que l’on doit aux oracles ! }
\livretVerse#12 { Pourquoi d’un faux bonheur m’annoncer les appas, }
\livretVerse#13 { Dieux vains ! tremblante, hélas ! devant vos tabernacles, }
\livretVerse#12 { J’aurois cru faire un crime en ne vous croyant pas. }
\livretVerse#12 { Voilà donc le bonheur dont je m’étois flattée ! }
\livretVerse#8 { Hélas ! }
\livretPers Le Chœur
\livretVerse#8 { \transparent { Hélas ! } Déesse redoutée, }
\livretVerse#10 { Dans ce moment peut-être on est aux mains }
\livretVerse#10 { Veille sur nous, combats pour les Romains. }
\livretScene SCÈNE III
\livretDescAtt\wordwrap-center {
  Le vieil \smallCaps { Horace, Horace, Curiace, }
  Chevaliers d’Albe & de Rome, les Précédents.
}
\livretPers Le vieil Horace
\livretRef #'ACAhccChoeur
\livretVerse#8 { Peuples, dissipez vos alarmes, }
\livretVerse#8 { Les Dieux nous ont donné la paix. }
\livretPers Camille et le peuple
\livretVerse#10 { Ciel ! }
\livretPers Le vieil Horace
\livretVerse#10 { \transparent { Ciel ! } Albe & Rome ont déposé leurs armes. }
\livretPersDidas Camille avec transport
\livretVerse#12 { Grands Dieux ! Ah ! pardonnez mes transports indiscrets ! }
\livretPersDidas Curiace à Camille
\livretVerse#12 { Chere Camille, enfin je puis revoir vos charmes. }
\livretPers Camille
\livretVerse#8 { Ah ! tu m’as coûté bien des larmes ! }
\livretPers Le peuple
\livretVerse#12 { Quel miracle a produit ces étonnans effets ? }
\livretPers Le vieil Horace
\livretRef #'ACBhoraceChoeur
\livretVerse#12 { Un Dieux, qui parle aux cœurs. Déja les deux armées }
\livretVerse#12 { D’une égale fureur paroissoient animées ; }
\livretVerse#8 { On alloit en venir aux mains… }
\livretVerse#12 { Entre les deux partis soudain Tulle s’avance : }
\livretVerse#12 { On s’arrête, on l’entoure, on l’écoute en silence : }
\livretVerse#12 { Albains, dit-il, & vous, écoutez-moi Romains ! }
\null
\livretVerse#8 { Dieux ! quelles fureurs sont les nôtres. }
\livretVerse#12 { Je vois à notre aspect la nature frémir ! }
\livretVerse#12 { Vos fils sont nos neveux, nos filles sont les vôtres, }
\livretVerse#12 { Le sang de mille nœuds a voulu nous unir. }
\livretVerse#12 { D’un sang si précieux pourquoi souiller la terre ? }
\livretVerse#12 { Qu’entre vous, qu’entre nous trois guerriers soient choisis ! }
\livretVerse#12 { Et que notre intérêt, entre leurs mains remis, }
\livretVerse#12 { Fasse un simple combat d’une effroyable guerre. }
\livretPers Le peuple
\livretVerse#8 { O roi, le modèle des rois ! }
\livretVerse#12 { Oui, les Dieux t’inspiroient, ils parloient par ta voix ! }
\livretPers Le vieil Horace
\livretVerse#12 { Vous eussiez vu soudain dans l’une & l’autre armée }
\livretVerse#12 { La joie & la concorde enflammer tous les cœurs. }
\livretVerse#12 { D’un égal intérêt l’une & l’autre animée }
\livretVerse#12 { Ne songe plus qu’au choix de ses trois défenseurs. }
\livretVerse#8 { Romains, quelle gloire à prétendre ! }
\livretVerse#12 { Trop heureux les héros qui sauront nous défendre ! }
\livretPers Le jeune Horace
\livretVerse#12 { Que je leur porte envie ! }
\livretPers Le vieil Horace
\livretVerse#12 { \transparent { Que je leur porte envie ! } O noble & cher transport ! }
\livretVerse#8 { Les Dieux veillent sur ma patrie, }
\livretVerse#12 { Et dans des dignes mains ils remettront son sort. }
\livretDidasP\line { (à Curiace.) }
\livretVerse#8 { Toi dont Albe se glorifie, }
\livretVerse#12 { Par tes hautes vertus dignes d’être Romain }
\livretVerse#10 { Deviens mon fils en recevant sa main. }
\livretPers Camille & Curiace
\livretVerse#11 { Mon père ! }
\livretPersDidas Le vieil Horace à Camille
\livretVerse#11 { \transparent { Mon père ! } Tu le peux, sans hasarder ma gloire }
\livretVerse#12 { Soit que Rome triomphe ou qu’Albe ait la victoire }
\livretVerse#12 { Le vaincu du vainqueur reconnoîtra les loix, }
\livretVerse#8 { Sans honte, sans tributs servils ; }
\livretVerse#8 { Et nos états, unis par choix, }
\livretVerse#12 { Ne seront qu’un empire & qu’un peuple en deux villes. }
\livretPers Le peuple
\livretRef #'ACCquatuorChoeur
\livretVerse#8 { O du sort trop heureux retour ? }
\livretPers Camille & Curiace
\livretVerse#10 { Que nous devons de graces à l’amour. }
\livretPers Camille
\livretVerse#10 { Curiace ! }
\livretPers Curiace
\livretVerse#10 { \transparent { Curiace ! } Camille ! }
\livretPers Camille
\livretVerse#10 { \transparent { Curiace ! Camille ! } O mon père ! }
\livretPers Le vieil Horace
\livretVerse#10 { \transparent { Curiace ! Camille ! O mon père ! } Ma fille ! }
\livretPers Curiace & Camille
\livretVerse#10 { Que nous devons de graces à l’amour ! }
\livretPers Le vieil Horace
\livretVerse#8 { O Ciel ! sur Rome & ma famille }
\livretVerse#8 { Verse tes bienfaits en ce jour ! }
\livretPers Curiace & Camille
\livretVerse#8 { Du sein des plus rudes alarmes }
\livretVerse#8 { Ainsi le bonheur naît pour nous. }
\livretPers Le vieil Horace
\livretVerse#8 { Il doit en paroître plus doux : }
\livretVerse#8 { Le malheur lui prête des charmes. }
\livretPers Le peuple
\livretVerse#8 { O du sort trop heureux retour ! }
\livretPers Camille & Curiace
\livretVerse#10 { Que nous devons de graces à l’amour ! }
\livretPers Le peuple, Curiace, & Camille
\livretVerse#8 { O Ciel sur Horace & sa fille }
\livretVerse#8 { Verse tes bienfaits en ce jour. }
\livretPers Le vieil Horace, le jeune Horace
\livretVerse#8 { O Ciel sur Rome & ma famille }
\livretVerse#8 { Verse tes bienfaits en ce jour. }
\livretDescAtt\wordwrap-center{ DIVERTISSEMENT GÉNÉRAL }
\livretFinAct Fin du premier acte
\sep
\livretAct PREMIER INTERMEDE
\livretDescAtt\column {
  \justify {
    Le Théâtre représente le Temple de Jupiter-Capitolin.
    On voit dans le fond, l’Autel & la Statue de ce Dieu.
    Le Roi, les principaux Chefs de l’Armée, & le Sénat Romain
    occupent le Sanctuaire. Le peuple est sur
    la partie extérieure.
  }
  \justify {
    Les Prêtres entrent sur une marche noble & imposante.
  }
}
\livretPers Le grand prêtre
\livretRef #'ADApretre
\livretVerse#12 { Le Sénat, rassemblé sous ces voutes sacrées, }
\livretVerse#12 { Va choisir trois Héros pour être nos vengeurs : }
\livretVerse#10 { Puissent nos voix, par les Dieux inspirées, }
\livretVerse#12 { Nommer à cet état de dignes défenseurs ! }
\livretVerse#12 { Vous, Romains, à nos vœux unissez vos prières, }
\livretVerse#12 { Le salut de l’Etat dépend de ce grand choix : }
\livretVerse#10 { Priez les Dieux protecteurs de nos loix, }
\livretVerse#8 { De verser sur nous leurs lumières. }
\livretPers Le grand prêtre
\livretRef #'ADBpretreChoeur
\livretVerse#8 { Puissant moteur de l’univers, }
\livretVerse#8 { O toi dont l’essence suprême }
\livretVerse#8 { Assujettit le Destin même, }
\livretVerse#8 { Que sur nous, tes yeux soient ouverts. }
\livretPers Le grand prêtre
\livretVerse#8 { Foibles jouets des destinées, }
\livretVerse#8 { Que pouvons-nous sans ton secours ? }
\livretVerse#8 { C’est lui seul qui de nos années }
\livretVerse#8 { Arrête ou prolonge le cours. }
\livretPers Le peuple
\livretVerse#8 { Puissant moteur, &c. }
\livretPers Le grand prêtre
\livretVerse#8 { Les jours tristes, les jours sereins, }
\livretVerse#8 { La douce paix, l’affreuse guerre, }
\livretVerse#8 { Et la rosée & le tonnerre, }
\livretVerse#8 { Tout part de tes puissantes mains. }
\livretPers Le peuple
\livretVerse#8 { Puissant moteur, &c. }
\livretAlt Livret imprimé
\livretPers Le grand prêtre
\livretVerse#8 { Hélas ! devant ton trône auguste }
\livretVerse#8 { Que sont tous les foibles humains ? }
\livretVerse#8 { Mais ta voix règle leurs destins, }
\livretVerse#8 { Et c’est l’espérance du juste. }
\livretPers Le peuple
\livretVerse#8 { Puissant moteur, &c. }
\livretAltEnd
\null
\livretRef #'ADCmarche
\livretDidasPPage\justify {
  Après l’Hymne, on brûle l’encens, on fait les libations, &c.
  Les cérémonies finies avec la pompe convenable, le Grand-Prêtre
  s’avance du côté du peuple & chante l’air suivant.
}
\livretPers Le grand prêtre
\livretRef #'ADDpretreChoeur
\livretVerse#6 { O Rome ! ô ma patrie ! }
\livretVerse#12 { Choisis, on te présente ou le sceptre, ou des fers. }
\livretVerse#8 { Eveille ton puissant génie, }
\livretVerse#12 { Souviens-toi que les Dieux t’ont promis l’univers, }
\livretVerse#12 { L’espoir de tes enfans sur leurs décrets se fonde, }
\livretVerse#10 { Jupiter même a réglé ton destin : }
\livretVerse#10 { Tes ennemis t’attaqueront en vain, }
\livretVerse#12 { Rome doit être un jour la maîtresse du monde. }
\livretPers Le peuple
\livretVerse#10 { Ses ennemis l’attaqueront en vain, }
\livretVerse#12 { Rome doit être un jour la maîtresse du monde. }
\null
\livretDidasP\justify {
  Le Grand-Prêtre revient à l’Autel, finit les sacrifices,
  puis se retournant du côté du sénat, il dit :
}
\livretPers Le grand prêtre
\livretVerse#12 { Roi, Pontifes, Sénat, réunissez-vous tous : }
\livretVerse#12 { Que nos trois défenseurs soient nommés ce jour même ! }
\livretVerse#10 { Sur le grand choix que Rome attend de vous, }
\livretVerse#12 { Je vous promets des Dieux l’assistance suprême. }
\null
\livretRef #'ADEsinfonia
\livretDidasPPage\justify {
  Le Roi, les Prêtres, les Chefs de l’armée, le Sénat
  se retirent sur une marche regulière & sont supposés
  aller dans la Salle du Capitole, destinée aux assemblées
  du Sénat.
}
\livretPers Le peuple
\livretRef #'ADFchoeur
\livretVerse#8 { O Dieux, défenseurs de nos loix }
\livretVerse#12 { Inspirez le Sénat & parlez par sa voix ! }
\livretFinAct Fin de l’intermède
\sep
\livretAct ACTE II
\livretDescAtt\wordwrap-center {
  Le Théâtre représente un appartement du Palais d’Horace.
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center {
  \smallCaps { Camille, Curiace, } le jeune \smallCaps Horace.
}
\livretPers Camille
\livretRef #'BAAcacujh
\livretVerse#10 { Ainsi le Ciel pour jamais nous rassemble ! }
\livretPers Le jeune Horace
\livretVerse#12 { Sur le bonheur public le nôtre est affermi. }
\livretPers Curiace
\livretVerse#12 { Que mon sort est heureux ! Ce jour me rend ensemble }
\livretVerse#8 { Et ma maîtresse & mon ami. }
\livretPers Horace
\livretVerse#10 { Il unira d’une chaine éternelle }
\livretVerse#8 { Nos familles & nos états. }
\livretPers Curiace
\livretVerse#10 { Quel Dieu propice, après tant de débats, }
\livretVerse#9 { A pu former une union si belle ? }
\livretPers Horace
\livretVerse#12 { Que ce Dieu bienfaisant en bénisse le cours ! }
\livretPers Camille
\livretRef #'BABcacujh
\livretVerse#12 { Qui l’eût dit, que ce jour, marqué par tant d’alarmes, }
\livretVerse#8 { Seroit le plus beau de mes jours ! }
\livretPers Ensemble
\livretVerse#8 { Douce paix, transports pleins de charmes, }
\livretVerse#8 { Ah ! puissiez-vous durer toujours ! }
\livretPersDidas Chœur derrière le Théatre
\livretRef #'BBAchoeur
\livretVerse#8 { Vive à jamais le nom d’Horace }
\livretPers Horace
\livretVerse#11 { Qu’entends-je ? }
\livretScene SCÈNE II
\livretDescAtt\wordwrap-center {
  Le vieil \smallCaps Horace, Chevaliers Romains, les Précédens.
}
\livretPers Le vieil Horace
\livretVerse#11 { \transparent { Qu’entends-je ? } Viens, mon fils, que ton père t’embrasse. }
\livretPers Camille
\livretVerse#12 { Ah ! Rome l’a choisi pour notre défenseur ! }
\livretPers Curiace & les romains
\livretVerse#12 { Rome a rendu justice à sa haute valeur. }
\livretPers Le vieil Horace
\livretVerse#12 { Tes deux frères & toi partagent cet honneur. }
\livretPers Horace
\livretVerse#12 { A ce suprême honneur eussions-nous pu prétendre ? }
\livretPers Les chevaliers
\livretVerse#12 { Les Dieux ont fait ce choix : seuls ils l’ont pu dicter. }
\livretPers Le vieil Horace
\livretVerse#6 { Moins nous devions l’attendre, }
\livretVerse#8 { Plus il faudra le mériter. }
\livretPers Horace
\livretVerse#12 { Mon père ! et je le jure à vous, à ma patrie }
\livretVerse#12 { Ou Rome sera libre, ou je ne serai plus. }
\livretPers Le vieil Horace
\livretVerse#12 { Mourant pour son pays, on meurt digne d’envie. }
\livretPers Les chevaliers
\livretVerse#8 { Nos vœux ne seront point déçus. }
\livretPers Horace
\livretRef #'BBBhoracesChoeur
\livretVerse#6 { Dieux, protecteurs du Tibre ! }
\livretVerse#10 { Tranchez mes jours & sauvez mon pays ! }
\livretVerse#12 { Je reçois de ma vie un assez digne prix, }
\livretVerse#12 { Si Rome par ma mort est triomphante & libre. }
\livretPers Le vieil Horace
\livretVerse#8 { O de Rome heureux défenseur, }
\livretVerse#8 { Cours, vole où la gloire t’appelle ! }
\livretVerse#8 { Tes frères, pleins du même zèle, }
\livretVerse#8 { T’attendent aux champs de l’honneur. }
\livretPers Les chevaliers
\livretVerse#8 { O de Rome heureux défenseur, }
\livretVerse#8 { Cours, vole où la gloire t’appelle ! }
\livretPers Le vieil Horace
\livretVerse#12 { Quel Romain n’envieroit cet immortel honneur ! }
\livretVerse#12 { Tous brigueroient en foule une mort aussi belle. }
\livretPers Les chevaliers
\livretVerse#8 { O de Rome heureux défenseur, }
\livretVerse#8 { Cours, vole où la gloire t’appelle ! }
\livretPers Horace
\livretVerse#8 { Amis, vous embrasez mon cœur ! }
\livretVerse#9 { Je vole où la gloire m’appelle… }
\livretVerse#12 { Mais un soldat Albain s’avance ici vers nous. }
\livretScene SCÈNE III
\livretDescAtt\wordwrap-center {
  Un envoyé d’Albe, les Précédens.
}
\livretPersDidas Curiace à l’Albain
\livretRef #'BCArecit
\livretVerse#12 { Quels sont les trois guerriers que le choix d’Albe honore ? }
\livretPers L'Albain
\livretVerse#8 { Seigneur ! l’ignorez-vous encore ? }
\livretPers Curiace
\livretVerse#10 { Achevez, qui ? }
\livretPers L'Albain
\livretVerse#10 { \transparent { Achevez, qui ? } Vos deux frères & vous. }
\livretPers Camille
\livretVerse#11 { Curiace ! }
\livretPersDidas Curiace & les Romains à voix basse
\livretVerse#11 { \transparent { Curiace ! } Grands Dieux ! }
\livretPers L'Albain
\livretVerse#11 { \transparent { Curiace ! Grands Dieux ! } Vous semblez vous confondre, }
\livretVerse#12 { Blâmeriez-vous ce choix ? }
\livretPers Curiace
\livretVerse#12 { \transparent { Blâmeriez-vous ce choix ? } Il a dû me confondre. }
\livretVerse#12 { Je m’estimois trop peu pour un si grand honneur. }
\livretVerse#12 { Ah ! Camille ! }
\livretPers L'Albain
\livretVerse#12 { \transparent { Ah ! Camille ! } Au Sénat que dirai-je, seigneur ? }
\livretPers Curiace
\livretVerse#12 { Que mon cœur, pénétré de cette grace insigne, }
\livretVerse#12 { N’osoit pas y compter… mais qu’il s’en rendra digne. }
\livretScene SCÈNE IV
\livretDescAtt\wordwrap-center {
  Les mêmes excepté l’Albain.
}
\livretPers Les chevaliers
\livretRef #'BDArecit
\livretVerse#12 { O déplorable choix ! triste & funeste honneur ! }
\livretPers Curiace
\livretVerse#12 { O devoir rigoureux que l’honneur nous impose ! }
\livretPersDidas Camille à Horace & à Curiace
\livretVerse#6 { Ciel ! & quoi votre cœur }
\livretVerse#12 { Ne se révolte pas à la loi qu’on propose ! }
\livretPers Les chevaliers
\livretVerse#12 { O déplorable choix ! triste & funeste honneur ! }
\livretPers Camille
\livretVerse#12 { Ah ! c’est un crime affreux qui doit vous faire horreur. }
\livretPers Horace
\livretVerse#12 { Appellez-vous forfait de servir sa patrie ? }
\livretPers Camille
\livretVerse#12 { Appellez-vous vertu, cet attentat impie ? }
\livretPers Les chevaliers
\livretVerse#12 { O déplorable choix ! triste & funeste honneur ! }
\livretPers Horace
\livretVerse#8 { Je conçois tout notre malheur ; }
\livretVerse#12 { Il peut nous étonner, mais non pas nous abattre. }
\livretVerse#12 { Toi, reste, Curiace, & console ma sœur : }
\livretVerse#12 { Je viendrai te rejoindre, & nous irons combattre. }
\livretPers Le vieil Horace
\livretVerse#12 { Vertu digne de Rome ! ô mon fils ! mon cher fils ! }
\livretVerse#12 { Voilà les sentimens que mon cœur t’a transmis ! }
\livretDidasP\line { (A Curiace & à Horace.) }
\livretVerse#10 { Oui, mes enfans, votre infortune est grande, }
\livretVerse#12 { Mon cœur, comme le vôtre, en a senti les coups : }
\livretVerse#8 { Mais l’effort est digne de vous, }
\livretVerse#12 { Et tout cède à l’honneur alors qu’il nous commande. }
\livretVerse#10 { Je plains Camille, & permets sa douleur ; }
\livretVerse#8 { Son malheur, sans doute, est extrême : }
\livretVerse#12 { C’est à toi, Curiace, à raffermir son cœur ; }
\livretVerse#12 { Rends-la digne de nous & digne de toi-même. }
\livretDidasP\line { (Il sort, les Chevaliers & Horace le suivent.) }
\livretScene SCÈNE V
\livretDescAtt\wordwrap-center {
  \smallCaps { Camille, Curiace. }
}
\livretPers Camille
\livretRef #'BEAcamilleCuriace
\livretVerse#12 { Iras-tu, Curiace ? }
\livretPers Curiace
\livretVerse#12 { \transparent { Iras-tu, Curiace ? } Ah ! dans ce jour fatal, }
\livretVerse#8 { Je n’ai plus que le choix du crime ; }
\livretVerse#8 { Pour moi le malheur est égal, }
\livretVerse#12 { Et partout, sous mes pas, le sort creuse un abîme. }
\livretRef #'BEBcuriace
\livretVerse#12 { Victime de l’amour, victime de l’honneur, }
\livretVerse#12 { Il faut trahir Camille, ou trahir ma patrie. }
\livretVerse#12 { L’un & l’autre est affreux, l’un & l’autre est impie… }
\livretVerse#12 { Mon cœur s’en effarouche, & j’en frémis d’horreur. }
\livretVerse#8 { Je sens ma vertu qui chancelle, }
\livretVerse#8 { Ne cherche point à m’attendrir ; }
\livretVerse#8 { C’est l’honneur même qui m’appelle, }
\livretVerse#8 { Camille, il lui faut obéir. }
\livretPers Camille
\livretRef #'BECcamilleCuriace
\livretVerse#12 { Non, je te connois mieux ; non, tu n’es point barbare. }
\livretVerse#12 { L’amitié, la nature, & l’hymen & l’amour, }
\livretVerse#10 { Rappelleront ta raison qui s’égare : }
\livretVerse#12 { Tu ne trahiras point tant de droits en ce jour. }
\livretPers Curiace
\livretVerse#8 { Hélas ! }
\livretPers Camille
\livretVerse#8 { \transparent { Hélas ! } Et que prétends-tu faire ? }
\livretVerse#12 { As-tu conçu l’horreur de cet ordre inhumain ? }
\livretVerse#8 { Tu viendras donc m’offrir ta main }
\livretVerse#8 { Fumante du sang de mon frère ? }
\livretPers Curiace
\livretVerse#8 { Ah ! laissez-moi, Camille ! }
\livretPers Camille
\livretVerse#8 { \transparent { Ah ! laissez-moi, Camille ! } Eh ! quoi ! }
\livretVerse#8 { Ton nom consacré par la gloire, }
\livretVerse#12 { N’est-il donc pas fameux par plus d’une victoire ? }
\livretVerse#12 { Albe n’a-t-elle enfin d’autres guerriers que toi ? }
\livretPers Curiace
\livretVerse#12 { Ciel ! que proposes-tu ? tu veux que Curiace }
\livretVerse#12 { D’un opprobre éternel se souille lâchement, }
\livretVerse#11 { Qu’il déshonore & lui-même & sa race ?… }
\livretVerse#8 { Ah ! tu l’espère vainement. }
\livretAlt Livret imprimé
\livretVerse#8 { La vertu rentre dans mon ame, }
\livretVerse#8 { L’honneur doit surmonter l’amour : }
\livretVerse#8 { Camille, il vaut mieux, en ce jour, }
\livretVerse#12 { Mourir en te perdant, que de vivre en infâme. }
\livretPers Camille
\livretVerse#8 { Eh bien ! je ne te retiens plus : }
\livretVerse#12 { Cours, acheter l’honneur au prix d’un parricide, }
\livretVerse#8 { Cède à la fureur qui te guide, }
\livretVerse#12 { Mais que du moins avant tous nos nœuds soient rompus, }
\livretVerse#8 { Frappe, ingrat ! }
\livretPers Curiace
\livretVerse#8 { \transparent { Frappe, ingrat ! } Je n’entends plus rien ; }
\livretVerse#12 { Je vous fuis… }
\livretPersDidas Camille arrêtant Curiace & se jettant à ses pieds.
\livretVerse#12 { \transparent { Je vous fuis… } A tes pieds tu veux donc que je meure ? }
\livretPers Curiace
\livretVerse#12 { Camille ! vous pleurez ! }
\livretPers Camille
\livretVerse#12 { \transparent { Camille ! vous pleurez ! } Hélas ! il le faut bien ; }
\livretVerse#12 { Quand ta main m’assassine, il faut bien que je pleure ! }
\livretDidasP\wordwrap {
  (Curiace la relève ; il est attendri, elle continue :)
}
\livretAltEnd
\livretRef #'BEDcamilleCuriace
\livretVerse#8 { Par l’amour & par l’amitié, }
\livretVerse#8 { Par ce nœud si doux qui nous lie ! }
\livretVerse#8 { Ne te montre point sans pitié, }
\livretVerse#8 { C’est Camille en pleurs, qui te prie. }
\livretPers Curiace
\livretVerse#8 { Hélas ! tu déchire mon cœur, }
\livretVerse#8 { Camille, ta douleur m’accable : }
\livretVerse#8 { Laisse ton amant déplorable }
\livretVerse#8 { Mourir, victime de l’honneur. }
\livretPers Camille
\livretRef #'BEEcamilleCuriace
\livretVerse#8 { O Ciel ! quoi, ma prière est vaine ; }
\livretVerse#8 { Je n’ai plus sur toi de pouvoir ! }
\livretPers Curiace
\livretVerse#8 { Tu sais qu’un barbare devoir }
\livretVerse#8 { Commande à mon cœur & l’entraîne. }
\livretPers Camille
\livretVerse#12 { Tu ne te souviens plus que ton cœur est à moi ? }
\livretPers Curiace
\livretVerse#12 { J’étois à mon pays avant que d’être à toi. }
\livretPers Ensemble
\livretPers Curiace
\livretRef #'BEFcamilleCuriace
\livretVerse#8 { O sort cruel ! devoir barbare }
\livretVerse#8 { Hélas ! faut-il vous obéïr ? }
\livretVerse#6 { Camille, il faut me fuir, }
\livretVerse#8 { Le Ciel pour jamais nous sépare : }
\livretVerse#8 { Oui, c’en est fait il faut partir. }
\livretPers Camille
\livretVerse#8 { Cœur insensible ! amant barbare ! }
\livretVerse#8 { Ainsi rien ne peut te fléchir ! }
\livretVerse#6 { Cruel, peux-tu me fuir ! }
\livretVerse#8 { Je sens que ma raison s’égare, }
\livretVerse#8 { C’en est fait : je me sens mourir. }
\livretScene SCÈNE VI
\livretDescAtt\wordwrap-center {
  \smallCaps { Horace, Curiace, Camille. }
}
\livretPers Horace
\livretRef #'BFArecit
\livretVerse#10 { Ne tardons plus : viens, suis-moi, Curiace ! }
\livretPers Curiace
\livretVerse#12 { Marchons ! }
\livretPers Camille
\livretVerse#12 { \transparent { Marchons ! } Non, demeurez ; je ne vous quitte pas. }
\livretPers Horace
\livretVerse#8 { Ma sœur, quelle est donc cette audace ? }
\livretVerse#12 { Ah ! marchons… }
\livretPers Camille
\livretVerse#12 { \transparent { Ah ! marchons… } Non, cruels, je m’attache à vos pas. }
\livretVerse#12 { Vous ne commettrez pas ce crime abominable. }
\livretPers Curiace
\livretVerse#12 { Horace ! ah ! retenez ces horribles éclats. }
\livretPers Horace
\livretVerse#12 { Cet excès de foiblesse, ô Ciel ! est-il croyable. }
\livretPers Camille
\livretVerse#12 { Je veux intéresser Albe & Rome à mes cris. }
\livretVerse#8 { Voyez quelle rage est la vôtre ! }
\livretVerse#8 { O Ciel ! deux frères, deux amis }
\livretVerse#12 { Brûlent de se baigner dans le sang l’un de l’autre. }
\livretScene SCÈNE VII
\livretDescAtt\wordwrap-center {
  \smallCaps { Le vieil Horace, } les Précédens.
}
\livretPers Horace
\livretRef #'BGArecit
\livretVerse#12 { Mon père !… }
\livretPers Le vieil Horace
\livretVerse#12 { \transparent { Mon père !… } Mes enfans, il est temps de partir. }
\livretAlt Livret imprimé
\livretPers Horace
\livretVerse#12 { Par ses vaines clameurs, ma sœur nous désespère. }
\livretPers Curiace
\livretVerse#8 { Seigneur, daignez la retenir. }
\livretPers Le vieil Horace
\livretVerse#12 { Allons, rentrez Camille. }
\livretPers Camille
\livretVerse#12 { \transparent { Allons, rentrez Camille. } Et vous aussi, mon père ! }
\livretAltB Partition
\livretPers Le vieil Horace
\livretVerse#8 { Vous, Camille, rentrez. }
\livretPers Camille
\livretVerse#8 { \transparent { Vous, Camille, rentrez. } Mon père ! }
\livretAltEnd
\livretVerse#12 { Quoi ! sur vous la nature a si peu de pouvoir ? }
\livretPers Le vieil Horace
\livretVerse#12 { La nature se taît où parle le devoir. }
\livretPers Ensemble
\livretPersVerse Le vieil Horace \line {
  \hspace#7 \column {
    \livretVerse#8 { Oui mes enfans partez sur l’heure. }
    \livretVerse#8 { Allez remplir votre devoir }
    \livretVerse#10 { Laissez Camille, & son vain désespoir. }
  }
}
\livretPersVerse Curiace & Horace \line {
  \hspace#7 \column {
    \livretVerse#8 { Allons, ami, partons sur l’heure }
    \livretVerse#8 { Allons remplir notre devoir : }
  }
}
\livretPersVerse Horace \line {
  \hspace#7 \livretVerse#10 { Laissons Camille, & son vain désespoir. }
}
\livretPersVerse Curiace \line {
  \hspace#7 \livretVerse#10 { Allons éteindre un amour sans espoir. }
}
\livretPersVerse Camille \line {
  \hspace#7 \column {
    \livretVerse#8 { Quoi c’est donc en vain que je pleure ! }
    \livretVerse#8 { Quoi ? rien ne peut vous émouvoir : }
    \livretVerse#8 { On méprise mon désespoir. }
  }
}
\livretPersDidas Curiace au vieil Horace
\livretVerse#8 { Seigneur, en ce moment funeste, }
\livretVerse#12 { Puis-je encor ?… }
\livretPers Le vieil Horace
\livretVerse#12 { \transparent { Puis-je encor ?… } Je t’entends : ne viens point m’attendrir }
\livretVerse#12 { Va : remplis ton devoir… les Dieux feront le reste. }
\livretPers Camille
\livretVerse#12 { Tigres, allez combattre, & moi je vais mourir. }
\livretPers Le vieil Horace
\livretVerse#12 { Ma fille, allons, rentrez & laissez-les partir, }
\livretPers Ensemble
\livretPersVerse Le vieil Horace \line {
  \hspace#7 \livretVerse#8 { Oui mes enfans, &c. }
}
\livretPersVerse Horace & Curiace \line {
  \hspace#7 \livretVerse#8 { Allons, ami, partons, &c. }
}
\livretPersVerse Camille \line {
  \hspace#7 \livretVerse#8 { Quoi c’est donc, &c. }
}
\livretFinAct Fin du second acte
\sep
\livretAct SECOND INTERMÈDE
\livretDescAtt\column {
  \justify {
    Le Théâtre représente une campagne des environs de Rome
    où les armées de Rome & d’Albe sont en présence.
    Les trois Horaces sont auprès du roi de Rome, & les
    trois Curiaces auprès du dictateur d’Albe.
  }
  \justify {
    Un autel est placé au milieu des deux armées. Il est
    censé situé sur la ligne qui sépare les territoires
    des deux différens états.
  }
  \justify {
    Un Grand-Sacrificateur, & plusieurs Prêtres inférieurs
    entourent l’Autel.
  }
}
\livretPers Le grand sacrificateur
\livretRef #'BHAsacrificateur
\livretVerse#11 { Romains, Albains ! ce jour prévient votre ruine. }
\livretVerse#8 { Ce pays inculte & désert, }
\livretVerse#12 { De vos longs différens a trop long-temps souffert ; }
\livretVerse#8 { Le Ciel pour jamais les termine. }
\livretVerse#8 { Six guerriers choisis parmi vous, }
\livretVerse#12 { Vont décider du sort de l’un & l’autre empire : }
\livretVerse#8 { Ce jour même la guerre expire, }
\livretVerse#12 { Et ce dernier combat nous réunira tous. }
\livretRef #'BHBchoeur
\livretVerse#12 { Jurez au nom des Dieux, par l’honneur & la gloire, }
\livretVerse#12 { D’étouffer tout esprit de vengeance & d’aigreur : }
\livretVerse#12 { Qu’Albe ou Rome en ce jour obtienne la victoire, }
\livretVerse#12 { Jurez tous d’obéir au parti du vainqueur. }
\livretPers Les chefs des deux armées
\livretVerse#12 { Nous jurons tous aux Dieux, par l’honneur & la gloire }
\livretVerse#12 { D’étouffer tout esprit de vengeance & d’aigreur. }
\livretVerse#12 { Qu’Albe ou Rome en ce jour emporte la victoire, }
\livretVerse#12 { Nous jurons d’obéir au parti du vainqueur. }
\livretPers Tous
\livretVerse#12 { Nous jurons d’obéir au parti du vainqueur. }
\livretAlt Partition
\livretVerse#6 { Les Horaces, des freres. }
\livretVerse#7 { Les Curiaces, des amis. }
\livretAltEnd
\null
\livretDidasP\justify {
  Après le serment, on donne le signal du combat dans les
  deux camps. Les six Champions sont conduits en présence.
  Dès qu'ils paroissent, les deux armées s’écrient en même temps :
}
\livretVerse#8 { Ciel ! }
\livretDidasP\justify {
  Le signal recommence.
}
\livretPers Les deux armées
\livretVerse#8 { \transparent { Ciel ! } O crime ! ô honte éternelle ! }
\livretVerse#10 { La guerre même étoit moins criminelle }
\livretVerse#6 { Que cet horrible choix. }
\livretPers Les Horaces & les Curiaces
\livretVerse#10 { Allons, volons où l’honneur nous appelle. }
\livretPers Les deux armées
\livretVerse#12 { De la terre & du Ciel c’est outrager les loix. }
\livretPers Les Horaces & les Curiaces
\livretVerse#10 { Allons, volons où l’honneur nous appelle. }
\livretPers Les deux armées
\livretVerse#12 { Nous ne souffrirons pas ce combat plein d’horreur. }
\livretPers Les Horaces & les Curiaces
\livretVerse#12 { Avançons… }
\livretPers Les deux armées
\livretVerse#12 { \transparent { Avançons… } Arrêtez ! }
\livretDidasP\justify {
  Une foule de soldats des deux Armées quittent ses rangs.
  Ils se précipitent malgré les efforts des chefs pour les
  retenir. Ils veulent séparer les Champions qui s’obstinent
  au combat.
}
\livretDidasP\line {
  Pendant cette Pantommime.
}
\livretPers Les chefs de l'armée
\livretVerse#12 { \transparent { Avançons… Arrêtez ! } Révolte punissable ! }
\livretPers Les Horaces & les Curiaces
\livretVerse#12 { Laissez-nous. }
\livretPers Les chefs de l'armée
\livretVerse#12 { \transparent { Laissez-nous. } De vos chefs reconnoissez la voix. }
\livretPers Les deux armées
\livretVerse#7 { Qu’Albe fasse un autre choix ! }
\livretVerse#8 { Que Rome fasse un autre choix ! }
\livretPers Les Horaces & les Curiaces
\livretVerse#12 { Laissez-nous. }
\livretPers Les chefs de l'armée
\livretVerse#12 { \transparent { Laissez-nous. } De vos chefs reconnoissez la voix. }
\livretPers Les deux armées
\livretVerse#12 { Nous ne souffrirons point ce meurtre abominable }
\livretAlt Partition
\livretVerse#12 { De la terre & du Ciel c’est outrager les loix. }
\livretAltEnd
\livretVerse#8 { Oui, la guerre étoit moins coupable }
\livretVerse#6 { Que cet horrible choix. }
\livretDidasP\justify {
  La guerre est prête à s'allumer ; les deux rois s’approchent
  du Grand-Prêtre, ainsi que les principaux chefs de l’armée :
  ils délibèrent un moment ; & le grand Prêtre s’écrie :
}
\livretVerse#12 { Héros d’Albe & de Rome, & vous chefs & soldats, }
\livretVerse#12 { Ecoutez !… & cessez d’inutiles débats. }
\livretVerse#8 { Si le choix du Sénat vous blesse, }
\livretVerse#8 { Allez tous consulter vos Dieux : }
\livretVerse#10 { Qu’ils soit ou non désaprouvé par eux ; }
\livretVerse#12 { Quel profane osera condamner leur sagesse ! }
\livretPers Les deux armées
\livretRef #'BHCchoeur
\livretVerse#10 { Oui, que les Dieux décident entre-nous ! }
\livretAlt Livret imprimé
\livretVerse#12 { Quoi qu’il puisse arriver, nous obéirons tous. }
\livretAltB Partiton
\livretVerse#12 { À leur choix quel qu’il soit, nous nous soumettons tous. }
\livretAltEnd
\livretDidasP\justify {
  On sépare les six Champions. On les emmène & l’intermède finit.
}
\livretFinAct Fin de l’intermède
\sep
\livretAct ACTE III
\livretDescAtt\wordwrap-center {
  Le Théâtre représente la cour du Palais d’Horace.
}
\livretScene SCÈNE PREMIERE
\livretPersDidas Camille seule
\livretRef #'CAAcamille
\livretVerse#12 { Que je vous dois d’encens, ô mes Dieux tutélaires ! }
\livretVerse#8 { Unique appui des malheureux, }
\livretVerse#8 { Vous avez exaucé mes vœux, }
\livretVerse#12 { Je retrouve par vous mon amant & mes frères. }
\livretVerse#8 { Sans espoir sous les coups du sort, }
\livretVerse#8 { J’étois restée anéantie. }
\livretVerse#8 { Vous m’avez fait trouver la vie }
\livretVerse#8 { Dans le sein même de la mort. }
\livretScene SCÈNE II
\livretDescAtt\wordwrap-center {
  Le vieil \smallCaps Horace, Chevaliers Romains,
  \smallCaps Camille.
}
\livretPers Camille
\livretRef #'CBArecit
\livretVerse#12 { Mon père, ah ! prenez part à la publique joie, }
\livretVerse#12 { Et souffrez qu’à vos yeux la mienne se déploie. }
\livretPers Le vieil Horace
\livretVerse#12 { Ma fille, devant vous je dois en convenir, }
\livretVerse#12 { Je chéris Curiace & j’estime ses frères : }
\livretVerse#8 { Mon cœur n’a pu sans déplaisir }
\livretVerse#12 { Voir combattre aujourd’hui des personnes si chères ; }
\livretVerse#12 { Mais Rome commandoit, il falloit obéir. }
\livretPers Camille
\livretVerse#8 { Ah ! que son choix m’a fait souffrir ! }
\livretPers Le vieil Horace
\livretVerse#12 { Le Ciel va prononcer sur la cause commune, }
\livretVerse#12 { Il peut ou réprouver ou confirmer ce choix. }
\livretPers Camille
\livretVerse#10 { Ah ! loin de moi cette idée importune : }
\livretVerse#12 { Non, les Dieux ne sauraient dicter d’injustes loix. }
\livretVerse#12 { Ils inspiroient le peuple, ils parloient par sa voix. }
\livretScene SCÈNE III
\livretDescAtt\wordwrap-center {
  Un Romain, les Précédens.
}
\livretPers Le Romain
\livretRef #'CCArecit
\livretVerse#12 { Vos trois fils sont aux mains, seigneur, & les Dieux mêmes }
\livretVerse#10 { D’Albe & de Rome ont confirmé le choix. }
\livretPers Camille
\livretVerse#7 { Qu’entends-je ? }
\livretPers Le vieil Horace
\livretVerse#7 { \transparent { Qu’entends-je ? } Adorons leurs loix. }
\livretVerse#12 { Soumettons-nous, Romains, à leurs décrêts suprêmes. }
\livretPers Les Romains
\livretRef #'CCBcamilleHoraceChoeur
\livretVerse#8 { Pour ces illustres défenseurs }
\livretVerse#8 { Adressons aux Dieux nos prieres. }
\livretPersDidas Camille à part
\livretVerse#8 { Comment leur dérober mes pleurs ! }
\livretPers Le vieil Horace
\livretVerse#8 { Grands Dieux ramenez-les vainqueurs. }
\livretPers Les Romains
\livretVerse#8 { Exaucez-nous Dieux tutélaires ! }
\livretPersDidas Camille à part
\livretVerse#8 { Ciel ! confonds leurs vœux sanguinaires ! }
\livretAlt Livret imprimé
\livretPersDidas Le vieil Horace à Camille
\livretVerse#8 { Je vois tes pleurs prêts à couler ; }
\livretVerse#12 { Quand les Dieux ont parlé toute plainte est coupable. }
\livretPers Camille
\livretVerse#12 { Les Dieux n’ont point dicté cet ordre abominable }
\livretVerse#8 { Vos prêtres les ont fait parler. }
\livretPers Le vieil Horace
\livretVerse#8 { Malheureuse ! qu’ose-tu dire ? }
\livretPers Camille
\livretVerse#8 { Hélas ! mon ame se déchire, }
\livretVerse#8 { La frayeur trouble ma raison. }
\livretPers Le vieil Horace
\livretVerse#8 { Songe à l’honneur de ma maison. }
\livretPers Les Romains
\livretVerse#8 { Songez que vous êtes Romaine. }
\livretPers Camille
\livretVerse#8 { Hélas ! pour mériter ce nom }
\livretVerse#8 { Faut-il donc cesser d’être humaine ? }
\livretPers Le vieil Horace
\livretVerse#8 { Cache-moi ces indignes pleurs ; }
\livretVerse#8 { Sois Romaine, imite tes frères. }
\livretPers Les Romains
\livretVerse#8 { Pour ces illustres défenseurs }
\livretVerse#8 { Adressons aux Dieux nos prières. }
\livretPers Camille
\livretVerse#8 { Eh ! comment retenir mes pleurs ? }
\livretPers Le vieil Horace
\livretVerse#8 { Grands Dieux ! ramenez-les vainqueurs ! }
\livretAltEnd
\livretPersDidas Ensemble avec les Chevaliers
\livretVerse#8 { Exaucez-nous Dieux tutélaires ! }
\livretPersDidas Camille à part
\livretVerse#8 { Ciel ! confonds leurs vœux sanguinaires ! }
\livretPersDidas Femmes derrière le Théatre
\livretVerse#8 { O sort cruel ! destins contraires ! }
\livretPers Le vieil Horace
\livretVerse#8 { D’où viennent ces tristes clameurs ? }
\livretPers Ensemble
\livretVerse#8 { Veillez sur nous, Dieux tutélaires ! }

\livretScene SCÈNE IV
\livretDescAtt\wordwrap-center {
  Plusieurs femmes entrant effrayées sur la scène, les Précédens.
}
\livretPers Les femmes
\livretRef #'CDAhoraceChoeur
\livretVerse#8 { O sort cruel ! destins contraires ! }
\livretPers Le vieil Horace & les Romains
\livretVerse#8 { Ciel ! que présagent ces douleurs ? }
\livretPers Les femmes
\livretVerse#10 { Albe triomphe & Rome est asservie. }
\livretPers Tous
\livretVerse#12 { Ô funeste combat, malheureuse patrie ! }
\livretPers Le vieil Horace
\livretRef #'CDBrecit
\livretVerse#13 { Mes fils ne sont donc plus ? }
\livretPers Une femme
\livretVerse#13 { \transparent { Mes fils ne sont donc plus ? } Un seul vous reste, hélas ! }
\livretVerse#12 { Ses frères, à nos yeux, ont reçu le trépas. }
\livretPers Le vieil Horace
\livretVerse#12 { Quoi ? lorsqu’Albe triomphe un de mes fils respire ! }
\livretVerse#8 { On vous a fait un faux rapport, }
\livretVerse#12 { Si Rome a succombé, mon dernier fils est mort. }
\livretPers Une femme
\livretVerse#10 { Ainsi que moi, tous pourront vous le dire, }
\livretVerse#12 { Long-temps, avec courage, il avoit combattu ; }
\livretVerse#10 { Mais resté seul contre trois adversaires, }
\livretVerse#12 { Et n’espérant plus rien de sa haute vertu, }
\livretVerse#12 { Sa fuite l’a sauvé du destin de ses frères. }
\livretPers Le vieil Horace
\livretVerse#12 { O crime, dont la honte en rejaillit sur nous ! }
\livretVerse#12 { O fils lâche & perfide ; opprobre de ma vie ! }
\livretPers Camille
\livretVerse#11 { Mes frères ! }
\livretPers Le vieil Horace
\livretVerse#11 { \transparent { Mes frères ! } Arrêtez, ne les pleurez pas tous. }
\livretVerse#12 { Deux jouissent d’un sort trop digne qu’on l’envie. }
\livretRef #'CDChorace
\livretVerse#12 { Que des plus nobles fleurs leurs tombeaux soient couverts ! }
\livretVerse#12 { Que Rome leur consacre un éternel hommage ! }
\livretVerse#12 { Que leur noms, révérés en cent climats divers, }
\livretVerse#12 { A nos neveux surpris soient cités d’âge en âge ! }
\livretVerse#12 { Mais leur indigne frère, après sa lâcheté, }
\livretVerse#12 { Qu’il traîne, avec mépris, sa honte & sa misère ! }
\livretVerse#12 { Qu’il soit partout errant, & partout rebuté, }
\livretVerse#12 { Maudit du monde entier comme il l’est de son père ! }
\livretPers Camille
\livretRef #'CDDrecit
\livretVerse#12 { Daignez prendre pour lui de plus doux sentimens }
\livretVerse#13 { Voulez-vous rendre, hélas ! notre sort plus funeste ? }
\livretPers Un Romain
\livretVerse#12 { Craignez de vous livrer à vos ressentimens }
\livretVerse#12 { C’est votre fils enfin, c’est le seul qui vous reste. }
\livretPers Le vieil Horace
\livretVerse#8 { Mon fils ! jamais il ne le fut. }
\livretVerse#12 { S’il eût été mon sang il l’eût mieux fait connoître. }
\livretPers Un Romain
\livretVerse#8 { A tort, vous l’accusez peut-être }
\livretVerse#12 { Que vouliez-vous qu’il fit contre trois ? }
\livretPers Le vieil Horace
\livretVerse#12 { \transparent { Que vouliez-vous qu’il fit contre trois ? } Qu’il mourût. }
\livretVerse#12 { Vous cherchez vainement à pallier son crime : }
\livretVerse#12 { J’en atteste le Ciel & l’honneur qui m’anime ; }
\livretVerse#12 { Avant la fin du jour, ces mains, ces propres mains, }
\livretVerse#12 { Laveront dans son sang la honte des Romains. }
\livretPersDidas Chœur derrière le théatre
\livretRef #'CDEchoeur
\livretVerse#8 { Du vainqueur, célébrons la gloire ; }
\livretVerse#8 { Portons son nom jusques aux Cieux. }
\livretPers Ensemble
\livretPersVerse Le vieil Horace \line {
  \hspace#13 \smaller\livretVerse#12 {
    De quels cris d’allégresse ont retentit ces lieux ?
  }
}
\livretPersVerse Les Romains \line {
  \hspace#13
  \smaller\livretVerse#10 { L’air retentit des chants de la victoire ! }
}
\livretScene SCENE V
\livretDescAtt\wordwrap-center {
  \smallCaps Valere, Suite, les Précédens.
}
\livretPersDidas Valere au vieil Horace
\livretRef #'CEArecit
\livretVerse#8 { Tandis qu’un fils victorieux }
\livretVerse#12 { D’un triomphe si beau va rendre grace aux Dieux, }
\livretVerse#12 { Souffrez qu’au nom du Roi, qu’au nom de Rome entière. }
\livretPers Le vieil Horace
\livretVerse#12 { Que prétendez-vous dire ? Expliquez-vous, Valère. }
\livretVerse#12 { Quoi ! lorsqu’Albe à ses loix nous soumet aujourd’hui, }
\livretVerse#12 { Quand mon fils !… }
\livretPers Valere
\livretVerse#12 { \transparent { Quand mon fils !… } De l’état c’est le Dieu tutélaire ! }
\livretVerse#12 { Il nous a sauvé tous, nous triomphons par lui. }
\livretPers Le vieil Horace
\livretVerse#11 { Eh, quoi ? sa fuite ?… }
\livretPers Valere
\livretVerse#11 { \transparent { Eh, quoi ? sa fuite ?… } A fait notre victoire. }
\livretVerse#12 { Ignorez-vous encor la moitié du combat. }
\livretPersDidas Camille à part
\livretVerse#12 { Je tremble. }
\livretPers Valere
\livretVerse#12 { \transparent { Je tremble. } Apprenez donc le bonheur de l’état ; }
\livretVerse#12 { Et d’un fils immortel le courage à la gloire. }
\livretRef #'CEBrecit
\livretVerse#12 { Resté seul, n’osant plus compter sur sa valeur, }
\livretVerse#8 { Il feint de fuir ; les Curiaces }
\livretVerse#8 { Le poursuivent avec fureur, }
\livretVerse#12 { Mais d’un pas inégal chacun d’eux suit ses traces : }
\livretVerse#12 { Il les voit divisés, se retourne, & d’abord }
\livretVerse#8 { Sous ses coups votre gendre expire. }
\livretDidasP\justify {
  (Camille jette un cri & s’évanouit. Ses femmes l’emmènent.)
}
\livretPers Camille
\livretVerse#10 { Ciel ! }
\livretPers Valere
\livretAlt Livret imprimé
\livretVerse#10 { \transparent { Ciel ! } Le second éprouve un même sort, }
\livretAltB Partition
\livretVerse#10 { \transparent { Ciel ! } Le second accourt, double d'effort, }
\livretVerse#8 { Son courage n’y peut suffire, }
\livretVerse#8 { Il éprouve le même sort, }
\livretAltEnd
\livretVerse#12 { Et la mort du troisième assure notre empire. }
\livretPers Le vieil Horace
\livretRef #'CEChoraceChoeur
\livretVerse#8 { O noble appui de ton pays ! }
\livretVerse#8 { Gloire éternelle de ta race ! }
\livretVerse#8 { O mon fils, ô mon cher Horace ! }
\livretVerse#8 { Reviens, que ma tendresse efface }
\livretVerse#12 { La honte du soupçon qui trompa nos esprits ! }
\livretVerse#8 { Les honneurs que Rome t’apprête }
\livretVerse#12 { De ta haute valeur seront le digne prix : }
\livretVerse#8 { Nous allons couronner ta tête }
\livretVerse#12 { Des lauriers immortels que ta main a conquis. }
\livretPersDidas Les Romains reprennent
\livretVerse#8 { Nous allons couronner sa tête }
\livretVerse#12 { Des lauriers immortels, &c. }
\livretScene SCENE VI
\livretDescAtt\column {
  \wordwrap-center { \smallCaps Horace, Peuple, les Précédens. }
  \wordwrap-center { \smallCaps Horace est porté en triomphe. }
}
\livretPers Chœurs du peuple
\livretRef #'CFAhoracesChoeur
\livretVerse#8 { Du vainqueur célébrons la gloire, }
\livretVerse#8 { Portons son nom jusques aux Cieux. }
\livretPers Le vieil Horace
\livretVerse#8 { Mon cher fils ! }
\livretPers Horace
\livretVerse#8 { \transparent { Mon cher fils ! } Heureuse victoire ! }
\livretVerse#12 { J’en reçois dans vos bras un prix bien glorieux. }
\livretPers Le chœur
\livretVerse#8 { Du vainqueur célébrons la gloire, }
\livretVerse#8 { Portons son nom jusques aux Cieux. }
\livretPersDidas Horace à son père
\livretVerse#12 { Peu content des honneurs dont sa bonté m’accable }
\livretVerse#10 { Le roi, chez vous, veut lui-même venir ; }
\livretVerse#12 { Et peut-être aujourd’hui… }
\livretPers Le vieil Horace
\livretVerse#12 { \transparent { Et peut-être aujourd’hui… } Je cours le prévenir : }
\livretVerse#12 { Jamais à ses sujets un roi n’est redevable ; }
\livretVerse#12 { Et l’on est trop payé d’avoir pu le servir. }
\livretDidasP\justify { (Le vieil Horace sort. On danse.) }
\livretRef #'CFBdivertissement
\livretDescAtt\wordwrap-center { DIVERTISSEMENT }
\livretPers Chœur
\livretRef #'CFDchoeur
\livretVerse#12 { Les Dieux, de l’univers, nous ont promis l’empire : }
\livretVerse#10 { Le bras d’Horace accomplit leurs décrets ! }
\livretVerse#12 { Sa gloire est leur ouvrage ; ils ont daigné l’élire : }
\livretVerse#8 { Nous devons tout à ses succès. }
\livretDidasP\justify { (On danse.) }

\livretScene SCENE VII
\livretDescAtt\wordwrap-center {
  CAMILLE, les Précédens.
}
\livretRef #'CGArecit
\livretDidasPPage\justify { (Elle entre avec beaucoup de fureur.) }
\livretVerse#8 { Ou suis-je ? & quel transport coupable ! }
\livretVerse#12 { Quoi ? Rome au fratricide élève des autels ! }
\livretDidasP\justify {
  (La fête s’interrompt : les personnages qui la composent s’écartent
  avec surprise.)
}
\livretPers Une partie du chœur
\livretVerse#8 { C’est Camille ! }
\livretPersDidas Une autre cherchant à l’éloigner
\livretVerse#8 { \transparent { C’est Camille ! } Arrêtez ! }
\livretPersDidas Camille les repoussant
\livretVerse#8 { \transparent { C’est Camille ! Arrêtez ! } Cruels ! }
\livretVerse#12 { Laissez-moi contempler cette fête exécrable. }
\livretPers Horace
\livretVerse#12 { Qu’on l’éloigne, Romains ! }
\livretPersDidas Camille \line { les repoussant avec plus de force, & s'approchant du char de triomphe }
\livretVerse#12 { \transparent { Qu’on l’éloigne, Romains ! } Perfides laissez-moi. }
\livretPers Horace
\livretVerse#12 { O, d’une indigne sœur, insuportable audace ! }
\livretPersDidas Camille \line { appercevant les dépouilles de Curiace, qui ornent le char de triomphe }
\livretVerse#6 { Ciel ! qu’est-ce que je voi ? }
\livretDidasP\justify {
  (Elle les arrache & les embrasse avec transports.)
}
\livretVerse#12 { O dépouilles sacrées ! ô mon cher Curiace ! }
\livretVerse#12 { Voilà donc aujourd’hui ce qui reste de toi ! }
\livretDidasP\justify {
  (Elle pleure sur les dépouilles.)
}
\livretPers Horace
\livretVerse#12 { O honte de mon sang ! ô coupable insolence ! }
\livretDidasP\line { (A Camille.) }
\livretVerse#12 { Bannis d’un lâche amour le honteux souvenir, }
\livretVerse#8 { Et sois digne de ta naissance. }
\livretPersDidas Camille toujours pleurant
\livretVerse#8 { Hélas ! }
\livretPers Horace
\livretVerse#8 { \transparent { Hélas ! } Ne nous fais plus rougir ; }
\livretVerse#12 { Et préfère du moins à l’amour d’un seul homme }
\livretVerse#12 { La gloire de ton sang & l’intérêt de Rome. }
\livretPers Camille
\livretVerse#12 { Rome ! Je la déteste, ainsi que ta valeur ; }
\livretVerse#12 { Plus tu blâmes mes pleurs, plus j’y trouve de charmes ! }
\livretVerse#12 { Rome élève un trophée au succès de tes armes, }
\livretVerse#12 { Plus elle t’applaudit, plus tu me fais horreur. }
\livretVerse#10 { Puissent les Dieux, lançant sur vous la foudre, }
\livretVerse#12 { Et sur elle & sur toi, me venger aujourd’hui ! }
\livretVerse#8 { Puissé-je voir réduire en cendre }
\livretVerse#12 { Ces féroces Romains dont ton bras fut l’appui ! }
\livretVerse#12 { Qu’à vos justes tourmens l’univers applaudisse ! }
\livretVerse#12 { Qu’on oublie à jamais Rome & son défenseur ! }
\livretVerse#12 { Qu’enfin de tant de maux seule je sois l’auteur }
\livretVerse#12 { Pour accroître à la fois ma joie & ton supplice ! }
\livretAlt Livret imprimé
\livretPersDidas Horace \line { mettant l’epée à la main & s’avançant pour frapper Camille. }
\livretVerse#10 { C’est trop souffrir un mortel déshonneur }
\livretVerse#10 { Et tout ton sang… }
\livretPersDidas Camille saisissant un poignard
\livretVerse#10 { \transparent { Et tout ton sang… } Ose frapper ta sœur : }
\livretVerse#10 { C’est un exploit digne de ton grand cœur. }
\livretDidasP\line { (Elle se frappe.) }
\livretVerse#12 { Je te l’épargne. }
\livretPers Les Romains
\livretVerse#12 { \transparent { Je te l’épargne. } Ciel ! }
\livretPers Horace
\livretVerse#12 { \transparent { Je te l’épargne. Ciel ! } Elle s’est fait justice… }
\livretVerse#12 { Pourquoi faut-il, hélas ! que mon cœur en gémisse ? }
\livretPers Les Romains
\livretVerse#12 { Détournez vos regards de ce spectacle affreux… }
\livretDidasP\line { (On l’emmène.) }
\livretPers Horace
\livretVerse#12 { Rome est libre, il suffit, rendons graces aux Dieux. }
\livretAltB Partition
\livretPers Horace
\livretVerse#10 { C’est trop souffrir un mortel déshonneur ! }
\livretPers Valere
\livretVerse#12 { Ah seigneur, d’une amante excusés la faiblesse, }
\livretVerse#9 { N’écoutés point ses transports furieux, }
\livretVerse#12 { Que rien de ce grand jour ne trouble l’allégrese. }
\livretVerse#12 { Rome est libre. }
\livretPers Horace
\livretVerse#12 { \transparent { Rome est libre. } Il suffit, rendons graces aux Dieux. }
\livretPers Les Romains
\livretRef#'CGBchoeur
\livretVerse#12 { Rome est libre, il suffit, rendons graces aux Dieux. }
\livretAltEnd
\livretFinAct FIN
}
