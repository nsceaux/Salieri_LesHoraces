\livretAct ACTE III
\livretDescAtt\wordwrap-center {
  Le Théâtre représente la cour du Palais d’Horace.
}
\livretScene SCÈNE PREMIERE
\livretPersDidas Camille seule
\livretRef #'CAAcamille
%# Que je vous dois d'encens, ô mes Dieux tutélaires!
%# Unique appui des malheureux,
%# Vous avez exaucé mes vœux,
%# Je retrouve par vous mon amant & mes frères.
%# Sans espoir sous les coups du sort,
%# J'étois restée =ané=antie.
%# Vous m'avez fait trouver la vie
%# Dans le sein même de la mort.
\livretScene SCÈNE II
\livretDescAtt\wordwrap-center {
  Le vieil \smallCaps Horace, Chevaliers Romains,
  \smallCaps Camille.
}
\livretPers Camille
\livretRef #'CBArecit
%# Mon père, ah! prenez part à la publique joie,
%# Et souffrez qu'à vos yeux la mienne se déploie.
\livretPers Le vieil Horace
%# Ma fille, devant vous je dois en convenir,
%# Je chéris Curi=ace & j'estime ses frères:
%# Mon cœur n'a pu sans déplaisir
%# Voir combattre aujourd'hui des personnes si chères;
%# Mais Rome commandoit, il falloit obé=ir.
\livretPers Camille
%# Ah! que son choix m'a fait souffrir!
\livretPers Le vieil Horace
%# Le Ciel va prononcer sur la cause commune,
%# Il peut ou réprouver ou confirmer ce choix.
\livretPers Camille
%# Ah! loin de moi cette idée =importune:
%# Non, les Dieux ne sauraient dicter d'injustes loix.
%# Ils inspiroient le peuple, ils parloient par sa voix.
\livretScene SCÈNE III
\livretDescAtt\wordwrap-center {
  Un Romain, les Précédens.
}
\livretPers Le Romain
\livretRef #'CCArecit
%# Vos trois fils sont aux mains, seigneur, & les Dieux mêmes
%# D'Albe & de Rome ont confirmé le choix.
\livretPers Camille
%#- Qu'entends-je?
\livretPers Le vieil Horace
%#= Adorons leurs loix.
%# Soumettons-nous, Romains, à leurs décrêts suprêmes.
\livretPers Les Romains
\livretRef #'CCBcamilleHoraceChoeur
%# Pour ces illustres défenseurs
%# Adressons aux Dieux nos pri=eres.
\livretPersDidas Camille à part
%# Comment leur dérober mes pleurs!
\livretPers Le vieil Horace
%# Grands Dieux ramenez-les vainqueurs.
\livretPers Les Romains
%# Exaucez-nous Dieux tutélaires!
\livretPersDidas Camille à part
%# Ciel! confonds leurs vœux sanguinaires!
\livretAlt Livret imprimé
\livretPersDidas Le vieil Horace à Camille
%# Je vois tes pleurs prêts à couler;
%# Quand les Dieux ont parlé toute plainte est coupable.
\livretPers Camille
%# Les Dieux n'ont point dicté cet ordre abominable
%# Vos prêtres les ont fait parler.
\livretPers Le vieil Horace
%# Malheureuse! qu'ose-tu dire?
\livretPers Camille
%# Hélas! mon ame se déchire,
%# La fray=eur trouble ma raison.
\livretPers Le vieil Horace
%# Songe à l'*honneur de ma maison.
\livretPers Les Romains
%# Songez que vous êtes Romaine.
\livretPers Camille
%# Hélas! pour mériter ce nom
%# Faut-il donc cesser d'être *humaine?
\livretPers Le vieil Horace
%# Cache-moi ces indignes pleurs;
%# Sois Romaine, imite tes frères.
\livretPers Les Romains
%# Pour ces illustres défenseurs
%# Adressons aux Dieux nos pri=ères.
\livretPers Camille
%# Eh! comment retenir mes pleurs?
\livretPers Le vieil Horace
%# Grands Dieux! ramenez-les vainqueurs!
\livretAltEnd
\livretPersDidas Ensemble avec les Chevaliers
%# Exaucez-nous Dieux tutélaires!
\livretPersDidas Camille à part
%# Ciel! confonds leurs vœux sanguinaires!
\livretPersDidas Femmes derrière le Théatre
%# O sort cru=el! destins contraires!
\livretPers Le vieil Horace
%# D'où viennent ces tristes clameurs?
\livretPers Ensemble
%# Veillez sur nous, Dieux tutélaires!

\livretScene SCÈNE IV
\livretDescAtt\wordwrap-center {
  Plusieurs femmes entrant effrayées sur la scène, les Précédens.
}
\livretPers Les femmes
\livretRef #'CDAhoraceChoeur
%# O sort cru=el! destins contraires!
\livretPers Le vieil Horace & les Romains
%# Ciel! que présagent ces douleurs?
\livretPers Les femmes
%# Albe tri=omphe & Rome est asservie.
\livretPers Tous
%# Ô funeste combat, malheureuse patrie!
\livretPers Le vieil Horace
\livretRef #'CDBrecit
%#- Mes fils ne sont donc plus?
\livretPers Une femme
%#= Un seul vous reste, hélas!
%# Ses frères, à nos yeux, ont reçu le trépas.
\livretPers Le vieil Horace
%# Quoi? lorsqu'Albe tri=omphe un de mes fils respire!
%# On vous a fait un faux rapport,
%# Si Rome a succombé, mon dernier fils est mort.
\livretPers Une femme
%# Ainsi que moi, tous pourront vous le dire,
%# Long-temps, avec courage, il avoit combattu;
%# Mais resté seul contre trois adversaires,
%# Et n'espérant plus rien de sa haute vertu,
%# Sa fuite l'a sauvé du destin de ses frères.
\livretPers Le vieil Horace
%# O crime, dont la honte en rejaillit sur nous!
%# O fils lâche & perfide; opprobre de ma vie!
\livretPers Camille
%#- Mes frères!
\livretPers Le vieil Horace
%#= Arrêtez, ne les pleurez pas tous.
%# Deux jou=issent d'un sort trop digne qu'on l'envie.
\livretRef #'CDChorace
%# Que des plus nobles fleurs leurs tombeaux soient couverts!
%# Que Rome leur consacre un éternel hommage!
%# Que leur noms, révérés en cent climats divers,
%# A nos neveux surpris soient cités d'âge en âge!
%# Mais leur indigne frère, après sa lâcheté,
%# Qu'il traîne, avec mépris, sa honte & sa misère!
%# Qu'il soit partout errant, & partout rebuté,
%# Maudit du monde entier comme il l'est de son père!
\livretPers Camille
\livretRef #'CDDrecit
%# Daignez prendre pour lui de plus doux sentimens
%# Voulez-vous rendre, hélas! notre sort plus funeste?
\livretPers Un Romain
%# Craignez de vous livrer à vos ressentimens
%# C'est votre fils enfin, c'est le seul qui vous reste.
\livretPers Le vieil Horace
%# Mon fils! jamais il ne le fut.
%# S'il eût été mon sang il l'eût mieux fait connoître.
\livretPers Un Romain
%# A tort, vous l'accusez peut-être
%#- Que vouliez-vous qu'il fit contre trois?
\livretPers Le vieil Horace
%#= Qu'il mourût.
%# Vous cherchez vainement à palli=er son crime:
%# J'en atteste le Ciel & l'*honneur qui m'anime;
%# Avant la fin du jour, ces mains, ces propres mains,
%# Laveront dans son sang la honte des Romains.
\livretPersDidas Chœur derrière le théatre
\livretRef #'CDEchoeur
%# Du vainqueur, célébrons la gloire;
%# Portons son nom jusques aux Cieux.
\livretPers Ensemble
\livretPersVerse Le vieil Horace \line {
  \hspace#13 \smaller\livretVerse#12 {
    De quels cris d’allégresse ont retentit ces lieux ?
  }
}
\livretPersVerse Les Romains \line {
  \hspace#13
  \smaller\livretVerse#10 { L’air retentit des chants de la victoire ! }
}
\livretScene SCENE V
\livretDescAtt\wordwrap-center {
  \smallCaps Valere, Suite, les Précédens.
}
\livretPersDidas Valere au vieil Horace
\livretRef #'CEArecit
%# Tandis qu'un fils victori=eux
%# D'un tri=omphe si beau va rendre grace aux Dieux,
%# Souffrez qu'au nom du Roi, qu'au nom de Rome entière.
\livretPers Le vieil Horace
%# Que prétendez-vous dire? Expliquez-vous, Valère.
%# Quoi! lorsqu'Albe à ses loix nous soumet aujourd'hui,
%#- Quand mon fils!…
\livretPers Valere
%#= De l'état c'est le Dieu tutélaire!
%# Il nous a sauvé tous, nous tri=omphons par lui.
\livretPers Le vieil Horace
%#- Eh, quoi? sa fuite?…
\livretPers Valere
%#= A fait notre victoire.
%# Ignorez-vous encor la moitié du combat.
\livretPersDidas Camille à part
%#- Je tremble.
\livretPers Valere
%#= Apprenez donc le bonheur de l'état;
%# Et d'un fils immortel le courage et la gloire.
\livretRef #'CEBrecit
%# Resté seul, n'osant plus compter sur sa valeur,
%# Il feint de fuir; les Curi=aces
%# Le poursuivent avec fureur,
%# Mais d'un pas inégal chacun d'eux suit ses traces:
%# Il les voit divisés, se retourne, & d'abord
%# Sous ses coups votre gendre expire.
\livretDidasP\justify {
  (Camille jette un cri & s’évanouit. Ses femmes l’emmènent.)
}
\livretPers Camille
%#- Ciel!
\livretPers Valere
\livretAlt Livret imprimé
%#= Le second éprouve un même sort,
\livretAltB Partition
\livretVerse#10 { \transparent { Ciel ! } Le second accourt, double d'effort, }
%# Son courage n'y peut suffire,
%# Il éprouve le même sort,
\livretAltEnd
%# Et la mort du troisième assure notre empire.
\livretPers Le vieil Horace
\livretRef #'CEChoraceChoeur
%# O noble appui de ton pa=ys!
%# Gloire éternelle de ta race!
%# O mon fils, ô mon cher Horace!
%# Reviens, que ma tendresse efface
%# La honte du soupçon qui trompa nos esprits!
%# Les honneurs que Rome t'apprête
%# De ta haute valeur seront le digne prix:
%# Nous allons couronner ta tête
%# Des lauriers immortels que ta main a conquis.
\livretPersDidas Les Romains reprennent
%# Nous allons couronner sa tête
%#12 Des lauriers immortels, &c.
\livretScene SCENE VI
\livretDescAtt\column {
  \wordwrap-center { \smallCaps Horace, Peuple, les Précédens. }
  \wordwrap-center { \smallCaps Horace est porté en triomphe. }
}
\livretPers Chœurs du peuple
\livretRef #'CFAhoracesChoeur
%# Du vainqueur célébrons la gloire,
%# Portons son nom jusques aux Cieux.
\livretPers Le vieil Horace
%#- Mon cher fils!
\livretPers Horace
%#= Heureuse victoire!
%# J'en reçois dans vos bras un prix bien glori=eux.
\livretPers Le chœur
%# Du vainqueur célébrons la gloire,
%# Portons son nom jusques aux Cieux.
\livretPersDidas Horace à son père
%# Peu content des honneurs dont sa bonté m'accable
%# Le roi, chez vous, veut lui-même venir;
%#- Et peut-être aujourd'hui…
\livretPers Le vieil Horace
%#= Je cours le prévenir:
%# Jamais à ses sujets un roi n'est redevable;
%# Et l'on est trop pay=é d'avoir pu le servir.
\livretDidasP\justify { (Le vieil Horace sort. On danse.) }
\livretRef #'CFBdivertissement
\livretDescAtt\wordwrap-center { DIVERTISSEMENT }
\livretPers Chœur
\livretRef #'CFDchoeur
%# Les Dieux, de l'univers, nous ont promis l'empire:
%# Le bras d'Horace accomplit leurs décrets!
%# Sa gloire est leur ouvrage; ils ont daigné l'élire:
%# Nous devons tout à ses succès.
\livretDidasP\justify { (On danse.) }

\livretScene SCENE VII
\livretDescAtt\wordwrap-center {
  CAMILLE, les Précédens.
}
\livretRef #'CGArecit
\livretDidasPPage\justify { (Elle entre avec beaucoup de fureur.) }
%# Ou suis-je? & quel transport coupable!
%# Quoi? Rome au fratricide élève des autels!
\livretDidasP\justify {
  (La fête s’interrompt : les personnages qui la composent s’écartent
  avec surprise.)
}
\livretPers Une partie du chœur
%#- C'est Camille!
\livretPersDidas Une autre cherchant à l’éloigner
%#- Arrêtez!
\livretPersDidas Camille les repoussant
%#= Cru=els!
%# Laissez-moi contempler cette fête exécrable.
\livretPers Horace
%#- Qu'on l'éloigne, Romains!
\livretPersDidas Camille \line { les repoussant avec plus de force, & s'approchant du char de triomphe }
%#= Perfides laissez-moi.
\livretPers Horace
%# O, d'une indigne sœur, insuportable audace!
\livretPersDidas Camille \line { appercevant les dépouilles de Curiace, qui ornent le char de triomphe }
%# Ciel! qu'est-ce que je voi?
\livretDidasP\justify {
  (Elle les arrache & les embrasse avec transports.)
}
%# O dépouilles sacrées! =ô mon cher Curi=ace!
%# Voilà donc aujourd'hui ce qui reste de toi!
\livretDidasP\justify {
  (Elle pleure sur les dépouilles.)
}
\livretPers Horace
%# O honte de mon sang! ô coupable insolence!
\livretDidasP\line { (A Camille.) }
%# Bannis d'un lâche amour le honteux souvenir,
%# Et sois digne de ta naissance.
\livretPersDidas Camille toujours pleurant
%#- Hélas!
\livretPers Horace
%#= Ne nous fais plus rougir;
%# Et préfère du moins à l'amour d'un seul homme
%# La gloire de ton sang & l'intérêt de Rome.
\livretPers Camille
%# Rome! Je la déteste, ainsi que ta valeur;
%# Plus tu blâmes mes pleurs, plus j'y trouve de charmes!
%# Rome élève un trophée =au succès de tes armes,
%# Plus elle t'applaudit, plus tu me fais horreur.
%# Puissent les Dieux, lançant sur vous la foudre,
%# Et sur elle & sur toi, me venger aujourd'hui!
%# Puissé-je voir réduire en cendre
%# Ces féroces Romains dont ton bras fut l'appui!
%# Qu'à vos justes tourmens l'univers applaudisse!
%# Qu'on oublie =à jamais Rome & son défenseur!
%# Qu'enfin de tant de maux seule je sois l'auteur
%# Pour accroître à la fois ma joie =& ton supplice!
\livretAlt Livret imprimé
\livretPersDidas Horace \line { mettant l’epée à la main & s’avançant pour frapper Camille. }
%# C'est trop souffrir un mortel déshonneur
%#- Et tout ton sang…
\livretPersDidas Camille saisissant un poignard
%#= Ose frapper ta sœur:
%# C'est un exploit digne de ton grand cœur.
\livretDidasP\line { (Elle se frappe.) }
%#- Je te l'épargne.
\livretPers Les Romains
%#- Ciel!
\livretPers Horace
%#= Elle s'est fait justice…
%# Pourquoi faut-il, hélas! que mon cœur en gémisse?
\livretPers Les Romains
%# Détournez vos regards de ce spectacle affreux…
\livretDidasP\line { (On l’emmène.) }
\livretPers Horace
%# Rome est libre, il suffit, rendons graces aux Dieux.
\livretAltB Partition
\livretPers Horace
%# C'est trop souffrir un mortel déshonneur!
\livretPers Valere
%# Ah seigneur, d'une amante excusés la faiblesse,
%# N'écoutés point ses transports furieux,
%# Que rien de ce grand jour ne trouble l'allégrese.
%#- Rome est libre.
\livretPers Horace
%#= Il suffit, rendons graces aux Dieux.
\livretPers Les Romains
\livretRef#'CGBchoeur
%# Rome est libre, il suffit, rendons graces aux Dieux.
\livretAltEnd
\livretFinAct FIN
