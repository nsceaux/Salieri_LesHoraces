OUTPUT_DIR=out
DELIVERY_DIR=delivery
NENUVAR_LIB_PATH=$$(pwd)/../nenuvar-lib
LILYPOND_CMD=lilypond -I$$(pwd) -I$(NENUVAR_LIB_PATH) \
  --loglevel=WARN -ddelete-intermediate-files \
  -dno-protected-scheme-parsing
PROJECT=Salieri_LesHoraces

# Partition complète
conducteur:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT) main.ly
.PHONY: $(PROJECT)
# Oboe I
oboe1:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-oboe1 -dpart=oboe1 part.ly
.PHONY: oboe1
# Oboe II
oboe2:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-oboe2 -dpart=oboe2 part.ly
.PHONY: oboe2
# Flauto I
flauto1:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-flauto1 -dpart=flauto1 part.ly
.PHONY: flauto1
# Flauto II
flauto2:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-flauto2 -dpart=flauto2 part.ly
.PHONY: flauto2
# Clarinetto I
clarinetto1:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-clarinetto1 -dpart=clarinetto1 part.ly
.PHONY: clarinetto1
# Clarinetto II
clarinetto2:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-clarinetto2 -dpart=clarinetto2 part.ly
.PHONY: clarinetto2
# Fagotto I
fagotto1:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-fagotto1 -dpart=fagotto1 part.ly
.PHONY: fagotto1
# Fagotto II
fagotto2:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-fagotto2 -dpart=fagotto2 part.ly
.PHONY: fagotto2
# Violino I
violino1:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-violino1 -dpart=violino1 part.ly
.PHONY: violino1
# Violino II
violino2:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-violino2 -dpart=violino2 part.ly
.PHONY: violino2
# Alto
alto:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-alto -dpart=alto part.ly
.PHONY: alto
# Bassi
basso:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-basso -dpart=basso part.ly
.PHONY: basso
# Corno I
corno1:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-corno1 -dpart=corno1 part.ly
.PHONY: corno1
# Corno II
corno2:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-corno2 -dpart=corno2 part.ly
.PHONY: corno2
# Tromba I
tromba1:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-tromba1 -dpart=tromba1 part.ly
.PHONY: tromba1
# Tromba II
tromba2:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-tromba2 -dpart=tromba2 part.ly
.PHONY: tromba2
# Timpani
timpani:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-timpani -dpart=timpani part.ly
.PHONY: timpani

delivery:
	@mkdir -p $(DELIVERY_DIR)/
	@if [ -e $(OUTPUT_DIR)/$(PROJECT).pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT).pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-oboe1.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-oboe1.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-oboe2.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-oboe2.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-flauto1.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-flauto1.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-flauto2.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-flauto2.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-clarinetto1.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-clarinetto1.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-clarinetto2.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-clarinetto2.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-fagotto1.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-fagotto1.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-fagotto2.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-fagotto2.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-violino1.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-violino1.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-violino2.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-violino2.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-alto.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-alto.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-basso.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-basso.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-corno1.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-corno1.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-corno2.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-corno2.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-tromba1.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-tromba1.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-tromba2.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-tromba2.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-timpani.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-timpani.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-1.midi ]; then tar zcf $(DELIVERY_DIR)/$(PROJECT)-midi.tar.gz $(OUTPUT_DIR)/$(PROJECT).midi $(OUTPUT_DIR)/$(PROJECT)-[0-9]*.midi; fi
	git archive --prefix=$(PROJECT)/ HEAD . | gzip > $(DELIVERY_DIR)/$(PROJECT).tar.gz
.PHONY: delivery

clean:
	@rm -f $(OUTPUT_DIR)/$(PROJECT)-* $(OUTPUT_DIR)/$(PROJECT).*
.PHONY: clean

parts: oboe1 oboe2 flauto1 flauto2 clarinetto1 clarinetto2 \
	fagotto1 fagotto2 \
	violino1 violino2 alto basso \
	corno1 corno2 tromba1 tromba2 timpani \
	delivery clean
.PHONY: parts

all: check conducteur parts
.PHONY: all

check:
	@if [ ! -d $(NENUVAR_LIB_PATH) ]; then \
	  echo "Please install nenuvar-lib in parent directory:"; \
	  echo " cd .. && git clone https://github.com/nsceaux/nenuvar-lib.git"; \
	  false; \
	fi
.PHONY: check
