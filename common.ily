\header {
  copyrightYear = "2014"
  composer = "Antonio Salieri"
  poet = "Nicolas-François Guillard"
}

%% LilyPond options:
%%  urtext  if true, then print urtext score
%%  part    if a symbol, then print the separate part score
#(ly:set-option 'original-layout (eqv? #t (ly:get-option 'urtext)))
#(ly:set-option 'apply-vertical-tweaks
                (and (not (eqv? #t (ly:get-option 'urtext)))
                     (not (symbol? (ly:get-option 'part)))))
#(ly:set-option 'print-footnotes #f)
%(not (symbol? (ly:get-option 'part))))

%% Use rehearsal numbers
#(ly:set-option 'use-rehearsal-numbers #t)

%% Staff size
#(set-global-staff-size
  (cond ((not (symbol? (ly:get-option 'part))) 16)
        (else 18)))

%% Line/page breaking algorithm
%%  optimal   for lead sheets
%%  page-turn for instruments and vocal parts
\paper {
  #(define page-breaking
     (if (symbol? (ly:get-option 'part))
         ly:page-turn-breaking
         ly:optimal-breaking))
}

\layout {
  reference-incipit-width = #(* 1/2 mm)
}

\language "italiano"
\include "nenuvar-lib.ily"
\setPath "ly"
\opusPartSpecs
#`((tromba1 "Tromba I" ()
            (#:notes "trombe" #:tag-notes tromba1))
   (tromba2 "Tromba II" ()
            (#:notes "trombe" #:tag-notes tromba2))
   (corno1 "Corno I" ()
            (#:notes "corni" #:tag-notes corno1))
   (corno2 "Corno II" ()
            (#:notes "corni" #:tag-notes corno2))
   (flauto1 "Flauto I" ()
            (#:notes "flauti" #:tag-notes flauto1))
   (flauto2 "Flauto II" ()
            (#:notes "flauti" #:tag-notes flauto2))
   (oboe1 "Oboe I" ()
          (#:notes "oboi" #:tag-notes oboe1))
   (oboe2 "Oboe II" ()
          (#:notes "oboi" #:tag-notes oboe2))
   (clarinetto1 "Clarinetto I" ()
                (#:notes "clarinetti" #:tag-notes clarinetto1))
   (clarinetto2 "Clarinetto II" ()
                (#:notes "clarinetti" #:tag-notes clarinetto2))
   (fagotto1 "Fagotto I" ()
             (#:clef "bass" #:notes "bassi" #:tag-notes fagotto1))
   (fagotto2 "Fagotto II" ()
             (#:clef "bass" #:notes "bassi" #:tag-notes fagotto2))

   (violino1 "Violino I" ()
             (#:notes "violini" #:tag-notes violino1))
   (violino2 "Violino II" ()
             (#:notes "violini" #:tag-notes violino2))
   (alto "Alto" () (#:notes "alto" #:clef "alto"))
   (basso "Basso, Contrabasso" ()
          (#:notes "bassi" #:clef "bass" #:tag-notes basso
           #:instrument , #{ \markup\center-column { Basso Contrabasso } #}))

   (timpani "Timpani" ()
            (#:notes "timpani" #:clef "bass")))
\opusTitle "Les Horaces"

\paper {
  % de la place en bas de page pour les annotations
  last-bottom-spacing.padding = #3
}

\layout {
  indent = #(if (symbol? (ly:get-option 'part))
                smallindent
                largeindent)
  short-indent = #(if (symbol? (ly:get-option 'part))
                      0
                      (* 8 mm))
  ragged-last = ##f
}

\layout {
  \context {
    \Voice \override Script.avoid-slur = #'inside
  }
}

\header {
  maintainer = \markup {
    Nicolas Sceaux,
    \with-url #"http://www.lestalenslyriques.com" \line {
      Les Talens Lyriques – Christophe Rousset,
    }
    \with-url #"http://www.cmbv.fr" CMBV
  }
  license = "Creative Commons Attribution-ShareAlike 4.0 License"
  tagline = \markup\sans\abs-fontsize #8
  \override #'(baseline-skip . 0) \vcenter {
    \right-column\bold {
      \with-url #"http://nicolas.sceaux.free.fr" {
        \concat { Éditions \tagline-vspacer }
        \concat { Nicolas \tagline-vspacer }
        \concat { Sceaux \tagline-vspacer }
      }
    }
    \abs-fontsize #9 \with-color #(x11-color 'grey40) \raise #-0.7 \musicglyph #"clefs.petrucci.f"
    \column {
      \line { \tagline-vspacer \copyright }
      \line {
        \tagline-vspacer Partition éditée grâce au soutien
        de la Fondation d’Entreprise Philippine de Rothschild.
      }
      \smaller\line {
        \tagline-vspacer
        Sheet music from
        \with-url #"http://nicolas.sceaux.free.fr"
        http://nicolas.sceaux.free.fr
        typeset using \with-url #"http://lilypond.org" LilyPond
        on \concat { \today . }
      }
      \smaller\line {
        \tagline-vspacer \license
        — free to download, distribute, modify and perform.
      }
    }
  }

}

corniInstr = \with {
  instrumentName = "Corni"
  shortInstrumentName = "Cor."
}
trombeInstr = \with {
  instrumentName = "Trombe"
  shortInstrumentName = "Tr."
}
flautiInstr = \with {
  instrumentName = "Flauti"
  shortInstrumentName = "Fl."
}
oboiInstr = \with {
  instrumentName = "Oboi"
  shortInstrumentName = "Ob."
}
clarinettiInstr = \with {
  instrumentName = "Clarinetti"
  shortInstrumentName = "Cl."
}
fagottiInstr = \with {
  instrumentName = "Fagotti"
  shortInstrumentName = "Fg."
}

violiniInstr = \with {
  instrumentName = "VViolini"
  shortInstrumentName = "Vn."
}
altoInstr = \with {
  instrumentName = "Alto"
  shortInstrumentName = "Vla."
}
violaInstr = \with {
  instrumentName = "Viola"
  shortInstrumentName = "Vla."
}
bassoInstr = \with {
  instrumentName = "Basso"
  shortInstrumentName = "B."
}
violoncelliInstr = \with {
  instrumentName = "Violoncelli"
  shortInstrumentName = "Vc."
}
cbInstr = \with {
  instrumentName = "Contrabasso"
  shortInstrumentName = "Cb."
}
bcbInstr = \with {
  instrumentName = \markup\center-column {
    Basso Contrabasso
  }
  shortInstrumentName = \markup\center-column { B. Cb. }
}
vccbInstr = \with {
  instrumentName = \markup\center-column {
    Violoncelli Contrabasso
  }
  shortInstrumentName = \markup\center-column { Vc. Cb. }
}
timpaniInstr = \with {
  instrumentName = "Timpani"
  shortInstrumentName = "Timp."
}
choeurInstr = \with {
  instrumentName = \markup\smallCaps Chœur
  shortInstrumentName = \markup\smallCaps Ch.
}
camilleInstr = \with {
  instrumentName = \markup\smallCaps Camille
  shortInstrumentName = \markup\smallCaps Ca.
}
curiaceInstr = \with {
  instrumentName = \markup\smallCaps Curiace
  shortInstrumentName = \markup\smallCaps Cu.
}
vieilHoraceInstr = \with {
  instrumentName = \markup\center-column\smallCaps { Le vieil Horace }
  shortInstrumentName = \markup\smallCaps VH.
}
jeuneHoraceInstr = \with {
  instrumentName = \markup\center-column\smallCaps { Le jeune Horace }
  shortInstrumentName = \markup\smallCaps JH.
}
pretreInstr = \with {
  instrumentName = \markup\center-column\smallCaps { Le Grand Prêtre }
  shortInstrumentName = \markup\smallCaps GP.
}
valereInstr = \with {
  instrumentName = \markup\smallCaps Valere
  shortInstrumentName = \markup\smallCaps Va.
}

footnoteHere =
#(define-music-function (parser this-location offset note)
     (number-pair? markup?)
   (with-location #f
   (if (ly:get-option 'print-footnotes)
       #{ <>-\tweak footnote-music #(make-footnote-here-music offset note)
          ^\markup\transparent\box "1" #}
       (make-music 'Music 'void #t))))


intermede =
#(define-music-function (parser location title) (string?)
  (add-toc-item parser 'tocActMarkup title)
  (add-even-page-header-text parser (string-upper-case (*opus-title*)) #f)
  (*act-title* title)
  (add-odd-page-header-text
    parser
    (format #f "~a" (string-upper-case (*act-title*)))
    #f)
  (add-toplevel-markup parser
    (markup #:act (string-upper-case title)))
  (add-no-page-break parser)
  (make-music 'Music 'void #t))

ouverture =
#(define-music-function (parser location title) (string?)
   (let ((rehearsal (rehearsal-number)))
    (if (not (symbol? (*part*)))
        (add-toc-item parser 'tocActMarkup #{ \markup\sep #}))
    (add-toc-item parser 'tocPieceMarkup title rehearsal)
    (add-even-page-header-text parser (string-upper-case (*opus-title*)) #f)
    (add-odd-page-header-text parser (string-upper-case title) #f)
    (add-toplevel-markup parser (markup #:act (string-upper-case title)))
    (add-no-page-break parser)
    (if (eqv? #t (ly:get-option 'use-rehearsal-numbers))
        (begin
         (add-toplevel-markup parser (markup #:rehearsal-number rehearsal))
         (add-no-page-break parser))))
  (make-music 'Music 'void #t))
