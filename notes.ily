\notesSection "Notes"
\markuplist\with-line-width-ratio #0.7 \column-lines {
  \livretAct NOTES
  \livretParagraph {
    Cette édition de l'opéra \italic { Les Horaces } d’Antonio Salieri,
    réalisée pour Les Talens Lyriques – Christophe Rousset,
    est basée sur les sources suivantes :
  }
  \null
  %% manuscrit
  \indented-lines#5 {
    \line\bold { [Manuscrit] }
    \line {
      \italic { Les Horaces, }
      Salieri, Antonio (1750-1825).
    }
    \line { Partition manuscrite complète, 1786 }
    \wordwrap-lines {
      Bibliothèque Nationale de France, bibliothèque-musée de l'opéra, côte A-316.
      \with-url #"http://catalogue.bnf.fr/ark:/12148/cb39691327m/PUBLIC"
      \smaller\typewriter "http://catalogue.bnf.fr/ark:/12148/cb39691327m/PUBLIC"
    }
  }
  \null
  %% livret
  \indented-lines#5 {
    \line\bold { [Livret imprimé] }
    \italic\wordwrap-lines {
      Les Horaces, tragédie-lyrique en 3 actes, mêlée d'intermèdes,
      représentée devant Leurs Majestés, à Fontainebleau,
      le 2 novembre 1786.
    }
    \line { Guillard, Nicolas-François (1752-1814). }
    \line { Monographie imprimée. }
    \wordwrap-lines {
      Bibliothèque nationale de France, département Littérature et art, 8-Yth-8734
      \with-url #"http://gallica.bnf.fr/ark:/12148/bpt6k76123s"
      \smaller\typewriter "http://gallica.bnf.fr/ark:/12148/bpt6k76123s"
    }
  }
  \null
  \fill-line {
    \general-align #Y #CENTER \epsfile #Y #60
    #"images/LesHoraces-manuscrit.eps"
    
    \general-align #Y #CENTER \epsfile #Y #60
    #"images/LesHoraces-livret.eps"
  }
  \null
  \paragraph {
    Le matériel d'orchestre est constitué des parties
    séparées suivantes :
    flûte I, flûte II, hautbois I, hautbois II,
    clarinette I, clarinette II, basson I, basson II,
    cor I, cor II, trompette I, trompette II,
    violon I, violon II, alto, basses, timbales.
  }
  \null
  \paragraph {
    Cette édition est distribuée selon les termes de la licence
    Creative Commons Attribution-ShareAlike 4.0.  Il est donc permis,
    et encouragé, de jouer cette partition, la distribuer,
    l’imprimer.
  }
}