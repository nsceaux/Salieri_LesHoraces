\clef "treble" fa'8-\sug\f |
fa'2~ fa'8 fa'-\sug\p fa' fa' |
mib'4 fa' solb' fa' |
mib'2 r4 r8 fa'-\sug\f |
fa'2~ fa'8 fa' fa' fa' |
fa'4 sib'? sib' la' |
sib'2 r |
