\clef "bass" reb8-\sug\f |
reb2~ reb8 reb-\sug\p reb reb |
do4( reb mib reb) |
<<
  \tag #'fagotto1 { do2 }
  \tag #'fagotto2 { lab,2 }
>> r4 r8 reb-\sug\f |
reb2~ reb8 <<
  \tag #'fagotto1 { mib'?8 mib' mib' | reb'4 solb' fa' mib' | reb'2 }
  \tag #'fagotto2 { do8 do do | sib,4 solb8 mib fa4 fa, | sib,2 }
>> r2 |
