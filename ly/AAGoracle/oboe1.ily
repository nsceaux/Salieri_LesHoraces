\clef "treble" lab'8\f |
lab'2~ lab'8 lab'\p lab' lab' |
lab'4 lab' lab' lab' |
lab'2 r4 r8 lab'\f |
lab'2~ lab'8 la' la' la' |
sib'4 mib'' reb'' do'' |
sib'2 r |
