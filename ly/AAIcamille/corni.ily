\clef "treble" \transposition sib
<>-\sug\f
\twoVoices #'(corno1 corno2 corni) <<
  { mi''2 }
  { do'' }
>> r2 |
R1*2 |
r4 \twoVoices #'(corno1 corno2 corni) <<
  { mi''8. mi''16 mi''2 | }
  { sol'8. sol'16 sol'2 | }
  { <>-\sug\ff }
>>
R1 |
r4 \twoVoices #'(corno1 corno2 corni) <<
  { re''2 mi''4 | re''4 }
  { sol'2 do''4 | sol' }
>> sol'8.-\sug\f sol'16 sol'4 sol' |
sol' r r2 |
R1*6 |
r4 re''2\p re''4 |
re''1~ |
re''~ |
re'' |
sol'4 r r2 |
R1*6 |
r4 re''8.-\sug\p re''16 re''4 re'' |
\twoVoices #'(corno1 corno2 corni) <<
  { sol''1~ | sol'' | sol''4 re''2 mi''4 | }
  { sol'1~ | sol' | sol'4 sol'2 do''4 | }
  { s1*2-\sug\cresc | s4\!-\sug\f s-\sug\p }
>>
re''1 |
sol'2 r\fermata |
R1*3 |
r4 <>-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { mi''8. mi''16 mi''2 | }
  { sol'8. sol'16 sol'2 | }
>>
R1 |
r4 \twoVoices #'(corno1 corno2 corni) <<
  { re''2 mi''4 | re''4 re'' re'' re'' | re'' }
  { sol'2 do''4 | sol' sol' sol' sol' | sol' }
  { s2.-\sug\p | s4 s2.-\sug\f }
>> sol'8. sol'16 sol'4 r |
R1*17 |
r4 do''2-\sug\sf do''4 |
do''2-\sug\mf re''-\sug\cresc |
mi''4 do''2 do''4~ |
do'' do''2 do''4~ |
do''2-\sug\f r |
<<
  \tag #'(corno1 corni) \new Voice {
    \tag #'corni \voiceOne
    mi''2 re'' |
    do''2 r4 re'' |
    mi''2. re''4 |
    mi''2. re''4 |
    mi''4
  }
  \tag #'(corno2 corni) \new Voice {
    \tag #'corni \voiceTwo
    do''2 sol' |
    do''2 r4 sol' |
    do''2. sol'4 |
    do''2. sol'4 |
    do''4
  }
>> r4 r2 |
R1*5 |
r4 do''2-\sug\sf do''4 |
do''2-\sug\mf re''-\sug\cresc |
mi''4\!-\sug\f do''2 do''4~ |
do'' do''2 do''4 |
do''2 r |
R1 |
do''1-\sug\f |
<<
  \tag #'(corno1 corni) \new Voice {
    \tag #'corni \voiceOne mi''2 re'' | do''
  }
  \tag #'(corno2 corni) \new Voice {
    \tag #'corni \voiceTwo do''2 sol' | mi'
  }
>> r2 |
R1*3 |
