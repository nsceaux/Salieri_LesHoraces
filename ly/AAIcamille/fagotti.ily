\clef "bass" \tag #'fagotti <>^"[a 2]"
sib1-\sug\f |
mib4-\sug\p-\sug\cresc mib mib mib\!-\sug\f |
mib2 mib4 fa |
sib,2 r4 re'-\sug\p |
mib'2 re' |
do'4 la2 sib4 |
fa fa,-\sug\f la, do |
fa, r r2 |
<<
  \tag #'(fagotto1 fagotti) {
    R1 |
    \tag #'fagotti \voiceOne
    \clef "tenor" fa' |
    mi' |
    fa' | \tag #'fagotti \oneVoice
    R1*2 |
  }
  \tag #'fagotti \new Voice { s1 \voiceTwo R1*3 }
  \tag #'fagotto2 R1*6
>>
<<
  \tag #'(fagotto1 fagotti) \new Voice {
    \tag #'fagotti \voiceOne
    r4 sol^\p( do' mi') |
    sol' mi' do' sib |
    la do' fa' la' |
    sol' mi' do' sib |
    la \clef "bass"
  }
  \tag #'(fagotto2 fagotti) \new Voice {
    \tag #'fagotti \voiceTwo
    do1-\sug\fp~ | do~ | do~ | do | fa4
  }
>> fa,4-\sug\f la, do |
la, fa,-\sug\p la, do |
fa r r2 |
r4 sib-\sug\cresc la sol |
fa2\!-\sug\f r |
R1 |
\clef "tenor" r4 fa-\sug\p la do' |
fa'8 do' fa' do' mi' do' mi' do' |
fa'4 fa'-\sug\cresc do' la |
fa \clef "bass" fa do la, |
fa,\!-\sug\f la2-\sug\p sib4 |
do'1 |
fa2 r\fermata |
sib1\p |
mib4 mib mib mib |
mib2 mib4 fa |
sib,2. re'4-\sug\p |
mib'2 re' |
do'4 la2 sib4 |
fa4 fa2-\sug\f fa4 |
fa fa fa r |
<<
  \tag #'(fagotto1 fagotti) {
    R1*3 |
    \tag #'fagotti \voiceOne
    \clef "tenor" sol'2-\sug\f fa' |
    mib'1~ |
    mib' |
    \tag #'fagotti \oneVoice R1*2 |
    \tag #'fagotti \voiceOne
    r4 si( re' sol') |
    fa'2 mib' |
    si2. do'4 |
    si4 \tag #'fagotti \oneVoice \clef "bass"
  }
  \tag #'fagotti \new Voice {
    \voiceTwo s1*3 R1*3 s1*2 R1*3 r4
  }
  \tag #'fagotto2 { R1*11 | r4 }
>> sol4 sol, sol, |
sol,4 r r2 |
R1 |
\clef "tenor" r4 mi'8\sf( fa') fa'2~ |
fa'4 mi'8( fa') fa'2~ |
fa'4 mi'8 fa' fa'2~ |
fa'4 sib,-\sug\sf( do re) |
mib2-\sug\mf mib'-\sug\cresc |
re'4 mib' re' do' |
sib mib' re' do' |
sib2-\sug\f mib' |
fa'1 |
r4 mi'8-\sug\sf( fa') fa'2~ |
fa'4 \once\slurDashed mi'8(-\sug\f fa') fa'2~ |
fa'4 \once\slurDashed mi'8(-\sug\f fa') fa'2~ |
fa'4 fa'4\sf( re' do') |
si1-\sug\p |
do'2. re'4 |
mib'2. do'4 |
re'8 r sib!4-\sug\sf( fa' re') |
si \once\slurDashed do'-\sug\sf( sol' mib') |
re' \clef "bass" sib,-\sug\sf( do re) |
mib2-\sug\mf mib'-\sug\cresc |
re'4\!-\sug\f mib' re' do' |
sib4 mib' re' do' |
sib2 mib'-\sug\mf |
fa'1 |
sib2-\sug\f mib' |
fa'1 |
sib2 r |
R1*3 |
