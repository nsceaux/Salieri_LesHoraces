\clef "treble"
<<
  \tag #'oboe1 {
    fa''2-\sug\f re''4.-\sug\p sib'8 |
    la'4 sol'2 mib''8.-\sug\f fa''16 |
    sol''4. sib'8 mib''-\sug\p do'' sib' la' |
  }
  \tag #'oboe2 {
    re'2-\sug\f fa'4.-\sug\p re'8 |
    mib'2 r |
    sib'4.-\sug\f sol'8 sol'4-\sug\p fa'8 la' |
  }
>>
sib'4 sib''-\sug\ff~ sib''8 fa''-\sug\p re'' sib' |
la'!2 sib' |
<<
  \tag #'oboe1 {
    mib''2. re''4 |
    do'' r r2 |
    <>^"Solo" fa''2 \grace sol''8 fa''4 mi''8 re'' |
    do''( re'' mi'' fa'' sol'' mi'' do'' sib') |
    la'1 |
    sol' |
    la' |
    R1*2 |
    <>^"Solo" r4 sol'\p( do'' mi'') |
    sol'' mi'' do'' sib' |
    la' do'' fa'' la'' |
    sol'' mi'' do'' sib' |
    la'
  }
  \tag #'oboe2 {
    do''2. sib'4 |
    la' r r2 |
    R1*11 |
    r4
  }
>> fa'4-\sug\f la' do'' |
la' fa'-\sug\p la' do'' |
<<
  \tag #'oboe1 {
    << { fa''1~ | fa'' | } { s2 s-\sug\cresc } >>
    fa''4\!-\sug\f r r2 |
    r4 do'''2-\sug\p \grace fa''8 mi'' re''16 mi'' |
    fa''4 r r2 |
    fa''2 sol'' |
    la''1-\sug\cresc ~ |
    la''~ |
    la''4\!-\sug\f r r sib''-\sug\p |
    la''2. sol''4 |
    fa''2 r\fermata |
    R1*3 |
    r4 sib''-\sug\f~ sib''8
  }
  \tag #'oboe2 {
    fa'4 re'' do''-\sug\cresc sib' |
    la' re'' do'' sib' |
    la'-\sug\f r r2 |
    r4 do''2-\sug\p sib'!4 |
    la' r r2 |
    la'2 do'' |
    fa''1-\sug\cresc~ |
    fa''~ |
    fa''4\!-\sug\f r r sol'-\sug\p |
    fa'4 fa''2 mi''4 |
    fa''2 r\fermata |
    R1*3 |
    r4 sib'-\sug\f~ sib'8
  }
>> fa''8-\sug\p re'' sib' |
la'!2 sib' |
<<
  \tag #'oboe1 {
    mib''2. re''4 |
    do''4 do''2-\sug\f do''4 |
    do''
  }
  \tag #'oboe2 {
    do''2. sib'4 |
    la'4 la'2-\sug\f la'4 |
    la'4
  }
>> fa'8. fa'16 fa'4 r |
<<
  \tag #'oboe1 {
    R1*3 |
    <>^"Solo" r4 sib'2-\sug\f sib'4 |
    sib'( mib''2 re''4) |
    \grace re''4 do''2 r |
    R1*5 |
    r4 sol'' sol'' sol'' |
    sol''1~ |
    sol''2. do'''4 |
    sib'' la'' r mib''( |
    re''2.) mib''4( |
    re''2.) mib''4( |
    re''4) \once\slurDashed re''-\sug\sf( mib'' fa'') |
    sol''2-\sug\mf la''-\sug\cresc |
    sib''1~ |
    sib'' |
    sib''2-\sug\f sol'' |
    fa'' la' |
    sib'2 r4 do'''( |
    sib''2.) do'''4( |
    sib''2.) do'''4( |
    sib''4) r r2 |
    <>^"Solo" r4 lab''( sol'' fa'') |
    mib''2.( re''4 |
    do''2. la'!4) |
    sib' r r2 |
    R1 |
    r4 \once\slurDashed re''(-\sug\sf mib'' fa'') |
    sol''2-\sug\mf la''-\sug\cresc |
    sib''1\!-\sug\f~ |
    sib'' |
    sib''2 sol''-\sug\mf |
    fa'' la'' |
    sib''1-\sug\f |
    sib''2 la'' |
    sib'' r |
  }
  \tag #'oboe2 {
    R1*11 |
    r4 si' si' si' |
    si' r r2 |
    R1 |
    r2 r4 do''( |
    sib'2.) do''4( |
    sib'2.) do''4( |
    sib'4) sib'2-\sug\sf sib'4 |
    sib'2-\sug\mf mib''-\sug\cresc |
    re''4 sol'' fa'' mib'' |
    re'' sol'' fa'' mib'' |
    re''2-\sug\f mib'' |
    re'' do'' |
    sib'2 r4 mib''( |
    re''2.) mib''4( |
    re''2.) mib''4( |
    re''4) r r2 |
    R1*5 |
    r4 sib'2-\sug\sf sib'4 |
    sib'2-\sug\mf mib''-\sug\cresc |
    re''4\!-\sug\f sol'' fa'' mib'' |
    re''4 sol'' fa'' mib'' |
    re''2 mib''-\sug\mf |
    re'' do'' |
    re''-\sug\f sol'' |
    re''2 do'' |
    sib' r |
  }
>>
R1*3 |

