\clef "alto" re'8-\sug\f fa' re' fa' re'-\sug\p fa' re' fa' |
mib'-\sug\cresc sib mib' sib mib' sib sol'-\!-\sug\f fa' |
mib' sib sol' mib' sol'-\sug\p mib' re' do' |
sib re' sib-\sug\ff re' sib re'-\sug\p sib re' |
do' mib' do' mib' sib re' sib re' |
la do' la do' la do' sib re' |
fa'4 fa-\sug\f la do' |
fa'8 la'\p fa' la' fa' la' fa' la' |
mi' sol' mi' sol' mi' sol' mi' sol' |
fa'8 la' fa' la' fa' la' fa' la' |
mi' sol' mi' sol' mi' sol' mi' sol' |
fa'\f la' fa' la' fa' la' fa' la' |
<>-\sug\p\< \rt#4 re'8 re'8 re' do' do'\! |
<>-\sug\f \rt#4 <si re'>8 \rt#4 q |
<do' mi'>8 mi'4-\sug\p mi' mi' mi'8~ |
mi' mi'4 mi' mi' mi'8 |
fa' fa'4 fa' fa' fa'8 |
mi'8 mi'4 mi' mi' mi'8 |
fa'4 fa-\sug\f la do' |
la fa-\sug\p la do' |
fa' sib'8. sib'16 la'8.-\sug\cresc la'16 sol'8. sol'16 |
fa'8. fa'16 sib'8. sib'16 la'8. la'16 sol'8. sol'16\! |
fa'2.-\sug\f sib4-\sug\p |
\rt#4 do'8 \rt#4 do' |
do'4 r r2 |
R1 |
r4 fa'8.-\sug\cresc fa'16 do'8. do'16 la8. la16 |
fa8. fa16 fa'8. fa'16 do'8. do'16 la8. la16\! |
<< { s4-\sug\f <>-\sug\p } { \rt#4 fa8 fa fa sib sib } >> |
\rt#4 do'8 \rt#4 do' |
fa2 r\fermata |
<>\p re'8 fa' re' fa' re' fa' re' fa' |
mib' sib mib' sib mib' sib sol'-\sug\f fa' |
mib' sib sol' mib' sol' mib' re' do' |
sib8 re'-\sug\f sib re' sib re'-\sug\p sib re' |
do' mib' do' mib' sib re' sib re' |
la do' la do' la do' sib re' |
<< { s4 <>-\sug\f } { \rt#4 fa'8 \rt#4 fa' } >> |
fa'4 fa8. fa16 fa4 r |
sib'2(-\sug\p la'4 sol') |
fad'8 fad'4 fad' fad' fad'8 |
sol' sol'4 sol'-\sug\cresc sol' sol'8 |
sol'4-\!-\sug\f re'2 re'4 |
mib'8 mib'4 mib' mib'-\sug\p mib'8 |
mib' lab'4 lab' lab' lab'8 |
fa' lab' fa' lab' fa' lab' mib' sol' |
si sol' si sol' do' sol' do' sol' |
sol2. r4 |
re'2 do' |
si4 si2 do'4 |
sol4 sol' sol sol |
sol r si r |
do' r mib' r |
fa' r r do''( |
sib'2.) do''4( |
sib'2.) do''4( |
sib') sib-\sug\sf( do' re') |
mib'2-\sug\mf do'-\sug\cresc |
sib4 mib'8. mib'16 re'8. re'16 do'8. do'16 |
sib8. sib16 mib'8. mib'16 re'8. re'16 do'8. do'16 |
<>-\!-\sug\f \rt#4 sib8 \rt#4 mib' |
\rt#4 fa' \rt#4 fa |
sib4 r r do''( |
sib'2.) do''4( |
sib'2.) do''4( |
sib'4) r r2 |
r4 fa'-\sug\p( mib' re') |
do' r si r |
do' r fa' r |
r sib-\sug\sf( fa' re') |
si do'-\sug\sf( sol' mib') |
re' \once\slurDashed sib-\sug\sf( do' re') |
mib'2-\sug\mf do'-\sug\cresc |
sib4\!-\sug\f mib'8. mib'16 re'8. re'16 do'8. do'16 |
sib8. sib16 mib'8. mib'16 re'8. re'16 do'8. do'16 |
\rt#4 sib8 <>-\sug\mf \rt#4 mib' |
\rt#4 fa' \rt#4 fa |
<>-\sug\f \rt#4 sib \rt#4 mib' |
\rt#4 fa' \rt#4 fa' |
\rt#4 sib \rt#4 sib |
<>\ff \rt#4 sib sib8 sib sib do' |
re' mib' re' dod' re' mib' re' dod' |
re' mib' re' do'! sib do' sib la |
