\clef "bass" sib,4\f sib, sib, sib,\p |
mib mib\cresc mib mib\!\f |
mib\f mib mib\p fa |
sib, sib\ff sib sib\p |
do' do' sib sib |
la la la sib |
fa fa,\f la, do |
fa, r r2 |
R1*4 |
<>\p\< \rt#4 re8 re re do do\! |
<>\f \rt#4 si, \rt#4 si, |
<>\fp \rt#4 do \rt#4 do |
\rt#4 do \rt#4 do |
\rt#4 do \rt#4 do |
\rt#4 do \rt#4 do |
fa4 fa\f la do' |
la fa\p la do' |
fa' sib la\cresc sol |
fa sib la sol |
<>\!\f \rt#4 fa8 fa8\p fa sib, sib, |
\rt#4 do8 \rt#4 do |
fa4 \clef "tenor" <>^"[Violoncelli]"_"[C.b. tacet]" fa4 la do' fa'8 do' fa' do' mi' do' mi' do' |
fa'4 fa'8. fa'16\cresc do'8. do'16 la8. la16 |
fa4 \clef "bass" <>^"[tutti?]" fa do la, |
fa,8\!\f fa[ fa fa] fa\p fa sib, sib, |
\rt#4 do \rt#4 do |
fa2 r\fermata |
sib,4\p sib, sib, sib, |
mib mib mib mib |
mib2 mib4 fa |
sib,4 sib\f sib\p sib |
do' do' sib sib |
la la la sib |
fa8[ fa] fa[-\sug\f fa] \rt#4 fa |
fa4 fa, fa r |
sib2\p( la4 sol) |
fad fad fad fad |
sol sol\cresc sol sol |
sol\!\f sol fa fa |
mib mib mib\p mib |
lab lab lab lab |
fa8\p lab fa lab fa lab mib sol |
si, sol si, sol do sol do sol |
sol, sol sol sol \rt#4 sol |
\rt#4 sol \rt#4 sol |
\rt#4 sol \rt#4 sol |
sol4 sol, sol sol |
sol r si, r |
do r mib r |
fa r r la( |
sib2.) la4( |
sib2.) la4( |
sib) sib,\sf( do re) |
mib\mf mib do\cresc do |
sib,4 mib re do |
sib, mib re do |
<>\!\f \rt#4 sib,8 \rt#4 mib |
\rt#4 fa \rt#4 fa, |
sib,2 r4 la |
sib2. la4 |
sib2. la4 |
sib2 r |
r4 fa\pp( mib re) |
do r4 si, r |
do r fa r |
sib,2 r |
la2 r |
sib4 sib,\sf( do re) |
mib\mf mib do\cresc do |
sib,\!\f mib re do |
sib, mib re do |
\rt#4 sib,8 <>\mf \rt#4 mib |
\rt#4 fa \rt#4 fa, |
<>\f \rt#4 sib, \rt#4 mib |
\rt#4 fa \rt#4 fa |
\rt#4 sib, \rt#4 sib, |
<>\ff \rt#4 sib,8 sib, sib, sib, do |
re mib re dod re mib re dod |
re mib re do! sib, do sib, la, |
