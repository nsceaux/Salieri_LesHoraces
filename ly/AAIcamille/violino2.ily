\clef "treble" re'8-\sug\f fa' re' fa' re'-\sug\p fa' re' fa' |
mib'\cresc sib mib' sib mib' sib sol'\!-\sug\f fa' |
mib' sib sol' mib' sol'-\sug\p mib' re' do' |
re' fa' re'-\sug\ff fa' re' fa'-\sug\p re' fa' |
mib' fa' mib' fa' re' fa' re' fa' |
do' fa' do' fa' do' fa' sib fa' |
la4\! fa'8.\f fa'16 la'8. la'16 do''8. do''16 |
fa'8 la'\p fa' la' fa' la' fa' la' |
mi' sol' mi' sol' mi' sol' mi' sol' |
fa'8 la' fa' la' fa' la' fa' la' |
mi' sol' mi' sol' mi' sol' mi' sol' |
fa'\f la' fa' la' fa' la' fa' la' |
<>\p\< \rt#4 fa'8 fa'8 fa' mi' mi'\! |
<>-\sug\f \rt#4 <si re'>8 \rt#4 q |
<do' mi'>8 sol'4-\sug\p sol' sol' sol'8~ |
sol' sol'4 sol' sol' sol'8 |
la'8 la'4 la' la' la'8 |
sol'8 sol'4 sol' sol' sol'8 |
fa'4 fa'8.\f fa'16 la'8. la'16 do''8. do''16 |
la'4 fa'-\sug\p la' do'' |
fa'' re''8. re''16 do''8.-\sug\cresc do''16 sib'8. sib'16 |
la'8. la'16 re''8. re''16 do''8. do''16 sib'8. sib'16\! |
la'2.-\sug\f sib'8(-\sug\p sol') |
fa' do' fa' do' fa' do' mi' mi' |
fa'4 fa' la' do'' |
la'2 do'' |
fa''8 fa''4-\sug\cresc fa'' fa'' fa''8~ |
fa'' fa''4 fa'' fa'' fa''8-\! |
fa''4-\sug\f la'2-\sug\p sib'8( sol') |
fa'8 do' fa' do' fa' do' mi' do' |
<la fa'>2 r\fermata |
re'8\p fa' re' fa' re' fa' re' fa' |
mib' sib mib' sib mib' sib sol'\f fa' |
sol' sib sol' mib' sol' mib' re' do' |
re' fa'\f re' fa' re' fa'\p re' fa' |
mib' fa' mib' fa' re' fa' re' fa' |
do' fa' do' fa' do' fa' sib fa' |
la4 <>-\sug\f \rt#8 <la' fa''>16 \rt#4 q |
<< fa''4 \\ la' >> fa'8. fa'16 fa'4 r |
r re''(-\sug\p do'' sib') |
la'8 la'4 la' la' la'8 |
re' re'4 re'-\sug\cresc re' re'8 |
re'8 <sib sol'>4-\!-\sug\f q8 <sib lab'>8 q4 q8 |
<sib sol'>8 q4 q q-\sug\p q8 |
lab'8 mib'4 mib' mib' mib'8 |
re'2.-\sug\p( do'4) |
sol'2. do''4 |
si'2. r4 |
fa'2 mib' |
re'4 re'2 do'4 |
si4 <re' si' sol''>4 q q |
q sol' sol' sol' |
sol' sol' r sol' |
do' r r mib''( |
re''2.) mib''4( |
re''2.) mib''4( |
re'') re'(-\sug\sf mib' fa') |
sol'8 sol'4-\sug\mf sol'8 la'8-\sug\cresc la'4 la'8 |
sib'4 sol''8. sol''16 fa''8. fa''16 mib''8. mib''16 |
re''8. re''16 sol''8. sol''16 fa''8. fa''16 mib''8. mib''16 |
<>-\sug\f \rt#4 re''8 \rt#4 mib'' |
\rt#4 re'' \rt#4 do'' |
sib4 r r mib''( |
re''2.) mib''4( |
re''2.) \once\slurDashed mib''4( |
re'') r r2 |
r4 lab'(\p sol' fa') |
mib' mib' r sol' |
r sol' r la' |
r sib-\sug\sf( fa' re') |
si do'-\sug\sf( sol' mib') |
re' re'-\sug\sf( mib' fa') |
sol'8-\sug\sf sol'4 sol'8 la'-\sug\cresc la'4 la'8 |
sib'4\!-\sug\f sol''8. sol''16 fa''8. fa''16 mib''8. mib''16 |
re''8. re''16 sol''8. sol''16 fa''8. fa''16 mib''8. mib''16 |
\rt#4 re''8 <>-\sug\mf \rt#4 mib'' |
\rt#4 re'' \rt#4 do'' |
<>-\sug\f \rt#4 re'' sib' mib'' sol'' mib'' |
<< { \rt#4 sib'' \rt#4 la'' | sib''8 }
  \\ { \rt#4 re'' \rt#4 do'' | sib'8 }
>> sib8\ff sib sib \rt#4 sib |
\rt#4 sib sib8 sib sib do' |
re' mib' re' dod' re' mib' re' dod' |
re' mib' re' do'! sib do' sib la |
