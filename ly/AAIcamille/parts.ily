\piecePartSpecs
#`((corno1 #:tag-global () #:instrument "Corno I en si♭")
   (corno2 #:tag-global () #:instrument "Corno II en si♭")
   (oboe1)
   (oboe2)
   (fagotto1 #:notes "fagotti")
   (fagotto2 #:notes "fagotti")
   (violino1 #:notes "violino1")
   (violino2 #:notes "violino2")
   (alto)
   (basso
    #:instrument , #{ \markup\center-column { Violoncelli Contrabasso } #})
   (silence #:on-the-fly-markup , #{ \markup\tacet#83 #}))
