\clef "soprano/treble" fa''2 re''4. sib'8 |
\grace la'4 sol'2 mib''4. fa''8 |
sol''4. do''8 mib''[ do''] sib'[ la'] |
sib'2 r4 re''8 sib' |
la'2 sib'4. sib'8 |
mib''2. re''4 |
do''4 fa' r2 |
R1 |
r2 r4 do'' |
fa''2 \grace sol''8 fa''4 mi''8[ re''] |
do''2 do''4. sib'8 |
la'2 r |
si'4. do''8 re''4 mi'' |
fa''2 re''4. sol''8 |
mi''4 mi'' r do'' |
sol''( mi'') do'' sib' |
la'( do'') fa'' la'' |
sol''( mi'') do'' sib' |
la'2 r |
la'4 fa' la' do'' |
fa''1~ |
fa''~ |
fa''4( la'') mi''8[ fa''] re''[ sib'] |
la'2.( sol'4) |
la' fa' la' do'' |
fa''2\melisma sol'' |
la''1~ |
la''~ |
la''4 sol''8[ fa'']\melismaEnd mi''[ fa''] re''[ sib'] |
la'2.( sol'4) |
fa'1\fermata |
fa''2 re''4. sib'8 |
\grace la'4 sol'2. mib''8. fa''16 |
sol''4. do''8 mib''[ do''] sib'[ la'] |
sib'2 r4 re''8 sib' |
la'2 sib'4. sib'8 |
mib''2. re''4 |
do''4 fa' r2 |
r r4 fa'' |
re''2 re''4. re''8 |
mib''2. \grace re''4 do'' |
sib' sib' r2 |
r4 sib' sib' sib' |
sib'( mib''2) re''4 |
do'' do'' r do'' |
si'2. do''4 |
fa'' re'' mib'' do'' |
si'2 r4 sol'' |
si'2 do''4. do''8 |
fa''2. mib''8 mib'' |
re''4 re'' r2 |
r4 re'' re'' re'' |
mib''2. do''4 |
la' la' r mib'' |
re''2. mib''4 |
re''2 r |
r4 re'' mib'' fa'' |
sol''2( la'' |
sib''1)~ |
sib''~ |
sib''2 sol'' |
fa'' la' |
sib' r4 mib'' |
re''2. mib''4 |
re''2. mib''4 |
re'' re'' r2 |
r4 si' si' si' |
do''2. re''4 |
mib''2. do''4 |
re''2 fa''4( re'') |
si'( do'') sol''( mib'') |
re'' re'' mib'' fa'' |
sol''2\melisma la'' |
sib''1~ |
sib''~ |
sib''2\melismaEnd sol'' |
fa'' la'' |
sib'' sol'' |
fa''2. la'4 |
sib'2 r2 |
<>^\markup\italic\column {
  \line { Le peuple en foule inonde les portiques du temple : }
  \line { il doit être composé de femmes, d’enfans & de vieillards. }
}
R1*3 |
