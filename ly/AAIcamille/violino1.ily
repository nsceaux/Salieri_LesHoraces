\clef "treble" <re' sib' fa''>2\f re''4.\p sib'8 |
la'4 sol'2 mib''8.\f fa''16 |
sol''4.\f do''8 mib''\p do'' sib' la' |
sib'4 sib''\sf~ sib''8 fa''-.\p re''-. sib'-. |
la'2 sib'4. sib'8 |
mib''2. re''4 |
do''4 fa'8.\f fa'16 la'8. la'16 do''8. do''16 |
fa''4 r r2 |
R1 |
fa''2 \grace sol''8 fa''4 mi''8 re'' |
do''( re'' mi'' fa'' sol'' mi'' do'' sib') |
la'4. la''8\f mi''8([ fa'' sol'' fa'']) |
si'4.\p\< do''8 re''4 mi''\! |
fa''2.\f re''4 |
mi''8 r sol'4\p( do'' mi'') |
sol''( mi'' do'' sib') |
la'( do'' fa'' la'') |
sol''( mi'' do'' sib') |
la' fa'8.\f fa'16 la'8. la'16 do''8. do''16 |
la'8. la'16 fa'8.\p fa'16 la'8. la'16 do''8. do''16 |
fa''8 fa''4 fa'' fa''\cresc fa''8~ |
fa'' fa''4 fa'' fa'' fa''8\! |
fa''4\f( la'') mi''8([\p fa'']) re''([ sib']) |
la'2. sol'4 |
la'4 fa' la' do'' |
fa''2 sol'' |
la''8 la''4\cresc la'' la'' la''8~ |
la'' la''4 la'' la'' la''8\! |
la''4\f sol''8(\p fa'') mi''([ fa'']) re''([ sib']) |
la'2. sol'4 |
<fa' la>2 r\fermata |
fa''2\p re''4. sib'8 |
la'4 sol'2 mib''8.\f fa''16 |
sol''4. do''8 mib'' do'' sib' la' |
sib' r sib''4\f~ sib''8 fa''\p re'' sib' |
la'2 sib'4. sib'8 |
mib''2. re''4 |
do''4 <>\f \rt#4 la''16 \rt#4 do''' \rt#4 la'' |
fa''4 fa'8. fa'16 fa'4 r |
r re''2\sf re''4 |
mib''2. re''8 do'' |
sib'8 sib'4 sib'\cresc sib' \once\tieDashed sib'8~ |
sib'8 sib'4\!\f sib' sib' sib'8 |
sib'4( mib''2) re''4\p |
do''8 do''4 do'' do'' do''8 |
si'2.\p( do''4) |
fa''4 re''( mib'' do'') |
si'2. sol''4 |
si'2 do'' |
fa''2. mib''4 |
re''4 <re' si' sol''> q q |
q re'' re'' re'' |
mib'' mib'' r do'' |
r mi''8\sf( fa'') fa''2~ |
fa''4 mi''8\sf( fa'') fa''2~ |
fa''4 mi''8\sf( fa'') fa''2~ |
fa''4 re''\sf( mib'' fa'') |
sol''8 sol''4\mf sol''8 la''\cresc la''4 la''8 |
<<
  \new Voice {
    \voiceOne sib''8 sib''4 sib'' sib'' sib''8~ |
    sib'' sib''4 sib'' sib'' sib''8 |
    sib''8 sib'' sib'' sib''
  }
  { \voiceTwo sib'8 sib'4 sib' sib' sib'8~ |
    sib' sib'4 sib' sib' sib'8 |
    sib'\!\f sib' sib' sib' \oneVoice }
>> \rt#4 sol''8 |
\rt#4 fa'' \rt#4 la' |
sib'4 mi''8\sf( fa'') fa''2~ |
fa''4 mi''8\sf( fa'') fa''2~ |
fa''4 mi''8\sf( fa'') fa''2~ |
fa''4 fa''(\sf re'' do'') |
si'1-\sug\p |
do''4 do'' r re'' |
r mib'' r do'' |
r4 sib'(\sf fa'' re'') |
si' do''(\sf sol'' mib'') |
re'' re''(\sf mib'' fa'') |
sol''8\sf sol''4 sol''8 la''8\cresc la''4 la''8 |
<< { \voiceOne sib''8\!_\f sib''4 sib'' sib'' sib''8~ |
    sib''8 sib''4 sib'' sib'' sib''8 |
    \rt#4 sib''8 \oneVoice }
  \new Voice {
    \voiceTwo sib'8 sib'4 sib' sib' sib'8~ |
    sib'8 sib'4 sib' sib' sib'8 |
    \rt#4 sib'8 }
>> <>_\markup\center-align\concat { \italic\bold poco \dynamic f } \rt#4 sol''8 |
\rt#4 fa'' \rt#4 la'' |
<>\f << { \rt#4 sib''8 \rt#4 sib'' | \rt#4 sib'' \rt#4 la'' | sib''8 }
  \\ { \rt#4 sib' \rt#4 sib' | \rt#4 re'' \rt#4 do'' | sib'8 }
>> sib8\ff sib sib \rt#4 sib |
\rt#4 sib sib8 sib sib do' |
re' mib' re' dod' re' mib' re' dod' |
re' mib' re' do'! sib do' sib la |
