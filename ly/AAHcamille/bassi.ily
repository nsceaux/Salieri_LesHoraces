\clef "bass" r8 |
r2 mi!4\p r |
r2 fa4 r |
r2 fa,8\f la,16 do fa8 sol |
la4 la, r2 |
r fa,8\ff la,16 do fa8 sol |
\repeat unfold 2 \rt#8 la16 |
sib8. sib,16 sib,8. sib,16 sib,8. sib,16 sib,8. sib,16 |
sib,4 r r2 |
sol1\p |
r8. fa16\f fa2 r4 |
R1*2 |
sold1\fp~ |
sold2 la4 r\fermata |
mi4 r r2 |
r2 fa4 r |
R1 |
la,4 r sib, r |
R1 |
r2 r4 do\f |
fa2 r |
