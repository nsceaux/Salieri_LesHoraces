\clef "treble" r8 |
r2 << \tag #'violino1 do''4\p \tag #'violino2 sol'4-\sug\p >> r4 |
r2 << \tag #'violino1 la'4 \tag #'violino2 do' >> r4 |
r2 <fa' do'' la''!>4.\f \grace do'''16 sib''16 la''32 sib'' |
do'''8. la''16 fa''4 r2 |
r2 <fa' do'' la''>4.\ff \grace do'''16 sib''16 la''32 sib'' |
do'''8( la'') fa'' fa'' fa''16 la'' do''' sib'' la'' sol'' fa'' mib'' |
<<
  \tag #'violino1 {
    re''8. re'''16 re'''8. sib''16 sib''8. fa''16 fa''8. re''16 |
    re''4 r r2 |
    dod''1\p |
    r8. re'16\f re'2 r4 |
  }
  \tag #'violino2 {
    << { s8. fa''16 fa''8. fa''16 fa''8. re''16 re''8. re''16 | re''4 }
      \\ { re''8. re''16 re''8. re''16 re''8. fa'16 fa'8. fa'16 | fa'4 }
    >> r4 r2 |
    la'1\p |
    r8. la16\f la2 r4 |
  }
>>
R1*2 |
<<
  \tag #'violino1 {
    re''1\fp~ |
    re''2 do''4 r\fermata |
    sol''4 r r2 |
    r2 la''4 r |
    R1 |
    mib''4 r re'' r |
  }
  \tag #'violino2 {
    si'1\fp~ |
    si'2 la'4 r\fermata |
    do''4 r r2 |
    r2 do''4 r |
    R1 |
    do''4 r sib' r |
  }
>>
R1 |
r2 r4 << mi''!4 \\ <sib' sol'>\f >> |
<< fa''2 \\ <la' fa'> >> r |
