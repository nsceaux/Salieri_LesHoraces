\clef "alto" r8 |
r2 do'4-\sug\p r |
r2 fa'4 r |
r2 fa8\f la16 do' fa'8 sol' |
la'4 la r2 |
r fa8\ff la16 do' fa'8 sol' |
la'16 do'' do'' do'' \repeat unfold 3 \rt#4 do''16 |
\repeat unfold 4 \rt#4 <fa' re''>16 |
<fa' re''>4 r r2 |
mi'!1-\sug\p |
r8. fa16\f fa2 r4 |
R1*2 |
mi'1-\sug\fp~ |
mi'2~ mi'4 r\fermata |
sol'4 r r2 |
r2 fa'4 r |
R1 |
fa'4 r fa' r |
R1 |
r2 r4 do'-\sug\f |
fa'2 r |
