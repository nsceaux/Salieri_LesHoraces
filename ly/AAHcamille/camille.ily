\clef "soprano/treble" fa'8 |
sib'8 sib' do'' reb'' do''4.^! do''8 |
do'' do'' do'' sol' la'!4^! r8 la'16 sib' |
do''4 r r2 |
r4 r8 fa'' fa''4. do''16 do'' |
la'4 la' r2 |
R1*2 |
r4 re''2 re''8 mi''! |
dod''8^! dod'' r dod''16 re'' mi''4 dod''8 la' |
re''4^! r r r8 la'16 la' |
la'4 la'8 la' re''4. re''8 |
fa''8 fa'' mi'' fa'' re'' re'' r re''16 re'' |
si'8^! si' r si'16 si' mi''4 si'8 do'' |
re''4 mi''8 si' do''4^! r8\fermata do''8 |
sol'4^! r8 sol' do'' do'' r sol'16 la' |
sib'4 sib'8 do'' la'^! la' r do''16 do'' |
la'4 sib'8 do'' fa'4 do''8 re'' |
mib''4^! mib''8 fa'' re''4^! r8 re''16 re'' |
re''4 do''8 re'' sib'4 r8 re'' |
sib'8 sib' sib' la' fa' fa' r4 |
R1 |
