Ce jour à ton a -- mant va pour ja -- mais t’u -- nir ;
Pour ja -- mais… mon cher Cu -- ri -- a -- ce !
Mais où m’em -- por -- te un es -- poir trop flat -- teur !
Quand de tous ces flé -- aux la guer -- re nous me -- na -- ce,
Mal -- heu -- reu -- se ! est-ce à moi d’o -- ser croire au bon -- heur ?…
Mais quoi ! l’o -- rac -- le est la voix des Dieux mê -- mes.
Je l’ai bien en -- ten -- du, ce n’est point une er -- reur,
Quand le Ciel a par -- lé, le doute est un blas -- phê -- me.
