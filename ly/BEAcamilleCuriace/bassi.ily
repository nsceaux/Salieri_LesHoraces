\clef "bass" la,4.\mf r8 r8. la16 la8. la16 |
fad2 r |
sol4 r r2 |
sol1\fp~ |
sol2 sol\fp~ |
sol fa\fp |
mi1\fp |
mib\fp~ |
mib |
r4 fa\f sib, r |
