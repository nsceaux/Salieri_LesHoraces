\clef "tenor" R1*3 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sib1~ |
    sib~ |
    sib2 la2 |
    sib1 |
    la~ |
    la | }
  { sol1~ |
    sol~ |
    sol2 fa2 |
    sol1 |
    fa~ |
    fa | }
  { s1*2\fp s2 s-\sug\fp s1-\sug\fp s-\sug\fp }
>>
r4 fa-\sug\f sib, r |
