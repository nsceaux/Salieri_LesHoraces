\clef "alto/G_8" R1*3 |
re'2^! re'8 re' do' re' |
sib4 r8 sib16 sol dod'2^!~ |
dod'8 dod'16 dod' dod'8 re' la8^! la r re' |
do'4^! r8 sol16 la sib4 sib8 do' |
la4^! r8 do'16 do' mib'4 do'8 sib |
la4 r8 do' la4 la8 la16 sib |
fa8^! fa r4 r2 |
