\tag #'camille {
  I -- ras- tu, Cu -- ri -- a -- ce ?
}
\tag #'curiace {
  Ah ! dans ce jour fa -- tal,
  Je n’ai plus __ que le choix du cri -- me ;
  Pour moi le mal -- heur est é -- gal,
  Et par -- tout, sous mes pas, le sort creuse un a -- bî -- me.
}
