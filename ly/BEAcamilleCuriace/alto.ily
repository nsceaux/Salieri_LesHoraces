\clef "alto" la4.-\sug\mf r8 r8. la16 la8. la16 |
la2 r |
sol4 r r2 |
sol'1-\sug\fp~ |
sol'2 mi'!-\sug\fp~ |
mi' re'-\sug\fp |
sib'1-\sug\fp |
la'-\sug\fp~ |
la' |
r4 fa'-\sug\f sib r |
