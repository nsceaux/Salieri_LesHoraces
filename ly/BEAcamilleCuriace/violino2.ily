\clef "treble" do'4.\mf r8 r8. do'16 do'8. do'16 |
do'2 r |
sib4 r r2 |
sib'1\fp~ |
sib'2 sib'\fp~ |
sib' la'\fp |
sol''1\fp |
la''\fp~ |
la'' |
r4 <fa' do'' la''>\f <fa' re'' sib''> r |
