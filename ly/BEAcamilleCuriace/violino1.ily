\clef "treble" mib'4.\mf r8 r8. mib'16 mib'8. mib'16 |
re'2 r |
re'4 r r2 |
re''1\fp~ |
re''2 dod''\fp~ |
dod'' re''\fp |
do''1\fp |
do''\fp~ |
do'' |
r4 <fa' do'' la''>\f <fa' re'' sib''> r |
