\clef "treble" R1*3 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { re'''1~ |
    re'''2 dod'''~ |
    dod''' re''' |
    sib''1 |
    do'''~ |
    do''' |
    s4 la'' sib'' s | }
  { sib''1~ |
    sib''2 sib''~ |
    sib'' la'' |
    sol''1 |
    la''~ |
    la'' |
    s4 mib'' re'' s | }
  { s1\fp s2 s\fp s s\fp s1\fp s\fp s r4 s2\f r4 }
>>
