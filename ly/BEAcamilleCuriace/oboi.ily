\clef "treble" R1*3 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''1~ |
    re''2 dod''~ |
    dod'' re'' |
    sol''1 |
    fa''~ |
    fa'' |
    s4 la'' sib'' s | }
  { sib'1~ |
    sib'2 sib'~ |
    sib' la' |
    do''1 |
    la'~ |
    la' |
    s4 mib'' re'' s | }
  { s1\fp s2 s\fp s s-\sug\fp
    s1-\sug\fp s-\sug\fp s r4 s2-\sug\f r4 }
>>
