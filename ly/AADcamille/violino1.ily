\clef "treble" R1*2 |
r2 sib'~ |
sib' do''~ |
do''1~ |
do''4 do''2\rinf do''4 |
do''1~ |
do''8.[ do''16\f do''8. do''16] si'!2~ |
si'1~ |
si'2 r4 <si' re' sol>4 |
<sol mi'! do''>2 r |
