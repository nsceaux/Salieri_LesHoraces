\clef "soprano/treble" R1*4 |
r2 r4 r8 do''16 do'' |
lab'4^! r r2 |
do''2 sol'8 lab' sib' do'' |
lab'4^! r r r8 re'' |
si'!4 r8 re'' re'' re'' fa'' re'' |
si' si' r16 si' si' do'' sol'4^! r4 |
R1 |
