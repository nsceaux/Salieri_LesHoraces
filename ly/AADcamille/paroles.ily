\tag #'suivante {
  Dé -- ja le sanc -- tu -- ai -- re s’ou -- vre,
  D’E -- gé -- rie à nos yeux l’i -- ma -- ge se dé -- cou -- vre :
  A -- van -- cez.
}
\tag #'camille {
  Je fré -- mis, vous, sou -- te -- nez mes pas,
  Al -- lons, je vais cher -- cher la vi -- e ou le tré -- pas.
}
