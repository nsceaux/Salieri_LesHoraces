\clef "bass" R1*2 |
r2 sol~ |
sol lab~ |
lab1~ |
lab4 lab\rinf( sol fa) |
mi!1 |
fa8.[ fa16\f fa8. fa16] sol2~ |
sol1~ |
sol2 r4 sol |
do2 r |
