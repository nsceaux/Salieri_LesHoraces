\clef "alto" R1*2 |
r2 sol~ |
sol lab~ |
lab1~ |
lab4 mib'-\sug\sf( mi' fa') |
do'1 |
do'8.[ fa'16-\sug\f fa'8. fa'16] fa'2~ |
fa'1~ |
fa'2 r4 fa' |
mi'!2 r |

