\clef "treble" R1*2 |
r2 mib'~ |
mib'1~ |
mib'~ |
mib'4 do''(\sf sib' lab') |
sol'1 |
lab'8.[ lab'16-\sug\f lab'8. lab'16] re'2~ |
re'1~ |
re'2 r4 <sol re' si'>4 |
<do'' mi' sol>2 r |
