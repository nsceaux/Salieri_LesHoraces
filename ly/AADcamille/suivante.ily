\clef "soprano/treble" r4 r8 sol' do''4 r8 do'' |
do'' do'' do'' mib'' do'' do'' r8 do''16 do'' |
do''4 re''8 mib'' sib'4-! r8 sib' |
reb'' reb'' reb'' do'' lab'-! lab' r mib''16 mib'' |
do''4 r r2 |
R1*6 |
