\piecePartSpecs
#`((tromba1 #:tag-global () #:instrument "Tromba I en ré"
            #:music , #{ s2.*54 <>^\markup\italic {
  Ne cherche point à m’attendrir }#})
   (tromba2 #:tag-global () #:instrument "Tromba II en ré"
            #:music , #{ s2.*54 <>^\markup\italic {
  Ne cherche point à m’attendrir }#})
   (corno1 #:tag-global () #:instrument "Corno I en mi♭")
   (corno2 #:tag-global () #:instrument "Corno II en mi♭")
   (flauto1 #:instrument "Flauto solo")
   (clarinetto1 #:score-template "score-clarinette-sib")
   (clarinetto2 #:score-template "score-clarinette-sib")
   (fagotto1 #:clef "tenor")
   (fagotto2 #:clef "tenor")
   (violino1)
   (violino2)
   (alto)
   (basso #:instrument "Basso")
   (silence #:on-the-fly-markup , #{ \markup\tacet#66 #}))
