\clef "tenor"
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sib8^"Solo" |
    mib'4~ mib'8. re'16 do'8. si16 |
    si4( do') r8. do'16 |
    do'4~ do'8. re'16 mib'8. fa'16 |
    fa'4( lab) r |
    R2. |
    r4 r \grace fa'16 mib'8 re'16 do' |
    sib4~ sib16( mib' sol sib sib lab fa re) |
    mib8. sib,16 mib4 r |
    R2.*4 |
    r8 sib( mib' reb' do' sol) |
    lab sib16 do' do'4 lab, |
    sib,2. |
    mib8. mib16 mib4 r |
    sol'4.\fp fa'8 mib' re' |
    \grace re'4 do' do' }
  { r8 |
    R2.*16 |
    mib'4. re'8 do' sib |
    \grace sib4 lab lab }
>> lab,4 |
sib,2. |
mib8
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sol4 sol8 sol sol |
    lab lab4 lab lab8 |
    sol sol4 sol8 sol sol |
    lab lab4 lab lab8 |
    sol2 sol16 lab sib do' |
    reb'2. |
    do' |
    sib |
    lab4. do'8 do' do' |
    do'2 re'4 |
    re'
  }
  { mib4 mib8 mib mib |
    fa fa4 fa fa8 |
    mib mib4 mib8 mib mib |
    fa fa4 fa fa8 |
    mib2. |
    sol,4 sol2 |
    lab2. |
    sol |
    fa4. lab8 lab lab |
    la2. |
    sib4
  }
>>
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  {
  }
  {
  }
>>
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  {
  }
  {
  }
>>
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  {
  }
  {
  }
>>
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  {
  }
  {
  }
>>