\clef "treble" r8 |
R2.*50 |
re'''4. si''8 sol'' fa'' |
re'' mib''4( sol''8 do''' mib''' |
re'''4. si''8 sol'' fa'') |
mib''4 r r |
r4 r8 re'''( do''' sib'' |
la'' sib'' do''' la'' sib'' sol'') |
fad''8\f re''4 re'''8( do''' sib'' |
la'' sib'' do''' la'' sib'' sol'') |
fad''2\fermata r8 r |
R2.*7 |
