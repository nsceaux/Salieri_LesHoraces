\clef "alto" r8 |
r8. <sol mib'>16-\sug\p q2 |
r8. <lab mib'>16 q2 |
r8. <lab fa'>16-\sug\f q2 |
r8. <sib fa'>16 q2 |
r8 sib(-\sug\p mib' reb' do' sol) |
lab sib16 do' do'4 lab8-\sug\f lab |
sib sib4 sib8 sib sib |
mib'8. sib16 sol4 r |
r8. <sol mib'>16\p q2 |
r8. <lab mib'>16 q2 |
r8. <lab fa'>16\f q2 |
r8. <sib fa'>16 q2 |
R2. |
r4 r lab8.-\sug\mf lab16 |
sib8 sib16 mib' sol'( mib' sib' sol' re' fa' sib' re') |
mib'8.-\sug\f sib16 mib'4 r |
R2. |
r4 r lab-\sug\mf |
sib8. mib'16 sol'( mib' sol' mib') re'( fa' re' fa') |
mib'8-\sug\f <sol mib>4 q8 q q |
<<
  { lab8 lab4 lab lab8 |
    sol sol4 sol8 sol sol |
    lab lab4 lab lab8 |
    \rt#4 sol16 \rt#4 sol \rt#4 sol | } \\
  { fa8 fa4-\sug\p fa fa8 |
    mib mib4 mib8-\sug\f mib mib |
    fa fa4-\sug\p fa fa8 |
    <>-\sug\cresc \rt#4 mib16 \rt#4 mib16 \rt#4 mib16 <>\!-\sug\fp | }
>>
\rt#12 <sol mib'>16 |
<>-\sug\f \rt#12 <lab mib'>16 |
<>-\sug\mf <sol' sib'>32 q q q q q q q <>-\sug\cresc \rt#16 q |
<>\!-\sug\f lab'16 lab lab lab \rt#8 lab |
<>-\sug\fp \rt#12 la |
<>-\sug\fp sib32 re' re' re' re' re' re' re' \rt#16 re' |
<>-\sug\fp mib' mib'' mib'' mib'' mib'' mib'' mib'' mib'' \rt#8 mib'' re''8. re'16 |
mib'8-\sug\ff mib mib16 fa32 sol lab sib do' re' mib'8 dod'' |
\rt#24 re''32 |
sol'4 r\fermata r |
r8. mib'16\p mib'2 |
r8. lab16 lab2 |
r8. fa'16\f fa'2 |
r8. sib16 sib2 |
R2. |
r4 r lab8.\mf lab16 |
sib8. mib'16 sol' mib' sib' sol' re' fa' sib' re' |
mib'8. sib16 mib'4 r |
R2. |
r4 r lab-\sug\mf |
sib8 sib16 mib' sol' mib' sol' mib' re' fa' re' fa' |
mib8( sol sib sol do' mib) |
re fa sib fa re sib |
mib sol sib sol do' mib |
re fa sib fa re sib |
mib4 r r |
re''4. si'8 sol' fa' |
re' mib'4 sol'8 do'' mib'' |
re''4. si'8 sol' fa' |
mib'4 do'8.\f dod'16 dod'8.\trill si32 dod' |
re'8( re''\p do'' sib' la' sol') |
fad'( sol' la' fad' sol' sib') |
re'\f re''( do'' sib' la' sol') |
fad'( sol' la' fad' sol' sib') |
re''2\fermata r8.\fermata mib'16\f |
mib'2\fermata sib8.-\sug\p sib16 |
do'8. do'16 do'8. do'16 sib8. sib16 |
sol2 r8\fermata r |
mib'2-\sug\f\fermata sib8.-\sug\p sib16 |
do'2-\sug\cresc sib8.\!-\sug\f sib16 |
sib8. <sib sol'>16 q8 q q <sol mib'> |
q8. sib16 q4 r |
