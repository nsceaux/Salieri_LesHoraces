\clef "treble" \transposition re
r8 |
R2.*54 | <>-\markup { \dynamic f \italic sempre } ^\markup { en ré }
\twoVoices #'(tromba1 tromba2 trombe) <<
  { do''4~ do''16. do''32 do''16. do''32 do''16. do''32 do''16. do''32 |
    do''4~ do''16. do''32 do''16. do''32 do''16. do''32 do''16. do''32 |
    do''4~ do''16. do''32 do''16. do''32 do''16. do''32 do''16. do''32 |
    do''4~ do''16. do''32 do''16. do''32 do''16. do''32 do''16. do''32 |
    do''2\fermata }
  { do'4~ do'16. do'32 do'16. do'32 do'16. do'32 do'16. do'32 |
    do'4~ do'16. do'32 do'16. do'32 do'16. do'32 do'16. do'32 |
    do'4~ do'16. do'32 do'16. do'32 do'16. do'32 do'16. do'32 |
    do'4~ do'16. do'32 do'16. do'32 do'16. do'32 do'16. do'32 |
    do'2\fermata }
>> r8\fermata r |
R2.*7 |
