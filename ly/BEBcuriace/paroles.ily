Vic -- ti -- me de l’a -- mour, vic -- ti -- me de l’hon -- neur,
Il faut tra -- hir Ca -- mil -- le, ou tra -- hir ma pa -- tri -- e.
Il faut tra -- hir Ca -- mil -- le, ou tra -- hir ma pa -- tri -- e.
L’un & l’autre est af -- freux, l’un & l’autre est im -- pi -- e…
Mon cœur s’en ef -- fa -- rou -- che, & j’en fré -- mis d’hor -- reur.
Mon cœur s’en ef -- fa -- rou -- che, & j’en fré -- mis d’hor -- reur,
& j’en fré -- mis d’hor -- reur.
Vic -- ti -- me de l’a -- mour, vic -- ti -- me de l’hon -- neur,
Il faut tra -- hir Ca -- mil -- le, ou tra -- hir ma pa -- tri -- e.
Il faut tra -- hir Ca -- mil -- le, ou tra -- hir ma pa -- tri -- e.


Je sens ma ver -- tu qui chan -- cel -- le,
Ne cher -- che point à m’at -- ten -- drir ;
C’est l’hon -- neur mê -- me qui m’ap -- pel -- le,
C’est l’hon -- neur mê -- me,
Ca -- mil -- le, il lui faut o -- bé -- ir.
Ca -- mil -- le, il lui faut o -- bé -- ir.
