\clef "alto/G_8" r8 |
R2.*7 |
r4 r r8 sib |
mib'4. re'8 do' si |
si4( do') r8 do' |
do'4. re'8 mib' fa' |
\grace fa'4 lab2 r8 fa |
sol4. sol8 lab sib |
do'[ re'16 mib'] mib'4 mib'8. do'16 |
sib2 sib8. lab16 |
lab8 sol4. r8 sib |
sol'4. fa'8 mib' re' |
\grace re'4 do' do' mib'8. do'16 |
sib2 sib8. lab16 |
sol8 mib r4 r |
r r8 sib16 sib sib8 sib16 sib |
sol4 r r |
r r8 sib16 sib sib8 sib16 sib |
mib'8 mib r4 r8 mib |
reb'4. mib8 mib8. reb'16 |
do'8 do' r4 r |
mi'4 mi'8. mi'16 mi'8. mi'16 |
fa'4 r r8 fa |
mib'!4. mib'8 mib'8. do'16 |
sib8 sib r4 r |
la4 la8. la16 re'8. re'16 |
mib'4. sol'8 sol'8. dod'16 |
re'2~ re'8. re'16 |
sol4 r\fermata r8 sib |
mib'4. re'8 do'8. si16 |
\grace si4 do'2 r8 do' |
do'4. re'8 mib'8. fa'16 |
\appoggiatura fa'8 lab2 r8 fa |
sol4. sol8 lab sib |
do'8[ re'16 mib'] mib'4 mib'8. do'16 |
sib2 sib8. lab16 |
lab8 sol4. r8 sib |
sol'4. fa'8 mib'8. re'16 |
\grace re'8 do'4 do' mib'8. do'16 |
sib2 sib8. lab16 |
lab8 sol4. r4 |
R2. |
r8 sol sib4 sol8 do' |
sib2 sib8 lab |
sol mib r4 r |
R2. |
r4 r8 sol do' mib' |
re'4. re'8 sol'8. fa'16 |
mib'4 r r |
r4 r8 re' re' re' |
re'4. re'8 re' sol' |
fad' re' r4 r |
r r8 re' re' sol' |
fad'4 re'\fermata r8 mib' |
mib'4 mib\fermata mib'8. mib'16 |
lab2 sib8. sib16 |
do'2 r8\fermata sib8 |
sol'4 mib'\fermata mib'8. mib'16 |
lab2 sib8. sib16 |
mib4 r r |
R2. |
