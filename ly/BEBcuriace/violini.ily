\clef "treble"
<<
  \tag #'violino1 {
    sib'8\p |
    mib''4~ mib''8.( re''16 do''8. si'16) |
    si'4( do'') r8. do''16\f |
    do''4~ do''8.( re''16 mib''8. fa''16) |
    fa''4( lab'4) r8 lab'\p |
    sol'4.( sol'8 lab' sib') |
    do'' re''16 mib'' mib''4 \grace fa''16 mib''8\f re''16 do'' |
    sib'4~ sib'16 mib''( sol' sib' sib' lab' fa' re') |
    mib'8. sib16 <sol mib'>4 r8 sib'\p |
    mib''4~ mib''8.( re''16 do''8. si'16) |
    si'4( do'') r8 do''\f |
    do''4~ do''8.( re''16 mib''8. fa''16) |
    fa''4( lab') r4 |
    R2. |
    r4 r mib''8.\mf do''16 |
    sib'8 sib4 sib' lab'8 |
    lab'8.\f sol'16 sol'4 r |
    R2. |
    r4 r mib''8.\mf do''16 |
    sib'8 sib4 sib'8 sib'8. lab'16 |
    sol'8\f mib'~ mib'16 mib' re' mib' \grace lab'16 sol'8 fa'16 mib' |
    do'' sib' sib'8\p~ sib'16 sib( re' fa' sib' fa' re' sib) |
    mib'4~ mib'16 mib'\f( re' mib' \grace lab'16 sol'8 fa'16 mib') |
    do'' sib' sib'8\p~ sib'( sib'16 do'' \grace mib''16 re''8 do''16 sib') |
    mib''8 mib'4\cresc mib' mib''8 |
    reb''8.\fp reb'16 reb'8. reb'16 reb'8. reb'16 |
    do'8 do'''4\f lab''8 mib'' do'' |
    <<
      { mi''32 mi'' mi'' mi'' mi'' mi'' mi'' mi'' \rt#16 mi'' } \\
      { sib'32\mf sib' sib' sib' sib' sib' sib' sib' <>\cresc \rt#16 sib' <>\! }
    >>
    fa''8\f fa'~ fa'16 sol'32 lab' sib' do'' re''! mi'' fa''8. fa''16 |
    mib''!16.\fp <mib' do'>32 q16. q32 q8. mib''16 re''8. do''16 |
    <<
      { s32 sol'' sol'' sol'' sol'' sol'' sol'' sol'' \rt#16 sol'' 
        \rt#16 sol'' \rt#8 fad'' |
        \rt#24 sol'' |
        \rt#16 sol'' \rt#8 fad'' |
        sol''4 } \\
      { sib'32\fp sib' sib' sib' sib' sib' sib' sib' \rt#16 sib' |
        <>\fp \rt#16 la' \rt#8 la' |
        <>\ff \rt#24 sib' |
        \rt#16 sib' \rt#8 la' |
        sib'4 }
    >> r4\fermata r |
    r8. <sol' sib>16\p q2 |
    r8. <lab' do'>16 q2 |
    r8. q16\f q2 |
    r8. <re' fa'>16 q2 |
    R2. |
    r4 r mib''8.\mf do''16 |
    sib'8 sib4 sib' lab'8 |
    lab'8. sol'16 sol'4 r |
    R2. |
    r4 r mib''8.\mf do''16 |
    sib'8 sib4 sib'8 sib'8. lab'16 |
  }
  \tag #'violino2 {
    r8 |
    r8. <sol' sib>16-\sug\p q2 |
    r8. <do' lab'>16 q2 |
    r8. q16-\sug\f q2 |
    r8. <re' fa'>16 q2 |
    r8 sib(-\sug\p mib' reb' do' sol) |
    lab8 sib16 do' do'4~ do'16 lab'(-\sug\f do'' lab') |
    sol'( mib' sib mib') sol'( sib' mib' sol') re'( fa' lab sib) |
    sol8. sib16 <sol mib'>4 r |
    r8. <sol' sib>16\p q2 |
    r8. <do' lab'>16 q2 |
    r8. q16\f q2 |
    r8. <re' fa'>16 q2 |
    R2. |
    r4 r do''8.-\sug\mf lab'16 |
    sol'16( mib' sib mib') sol'( mib' sib' sol') re'( fa' sib' re') |
    mib'8.-\sug\f sib16 sib4 r |
    R2. |
    r4 r do''8.-\sug\mf lab'16 |
    sol'16( mib' sib mib') sol'( mib' sol' mib') re'( fa' re' fa') |
    mib'8-\sug\f sib4 sib8 sib sib |
    sib sib4-\sug\p sib sib8 |
    sib sib4 sib8-\sug\f sib sib |
    sib sib4-\sug\p sib sib8 |
    <>-\sug\cresc \rt#4 sib16 \rt#4 sib16 \rt#4 sib16 |
    <>\!-\sug\fp \rt#12 sib16 |
    do'-\sug\f mib' mib' mib' \rt#8 mib' |
    r8 reb'''4-\sug\mf sib8-\sug\cresc reb'8. sib16 |
    <>\!-\sug\f lab do' do' do' \rt#8 do' |
    <>-\sug\fp \rt#8 do' \rt#4 re' |
    <>-\sug\f re'4 r8. sol32-\sug\f la sib8. sol16 |
    do'4 r8 la16 sib32 do' re'8. re'16 |
    mib'4-\sug\ff~ mib'16 fa'32 sol' lab' sib' do'' re'' mib''8. mib'16 |
    re'32 <sib' sol''> q q q q q q \rt#8 q \rt#8 <fad'' la'> |
    << sol''4 \\ sib' >> r\fermata r |
    r8. <mib' sol>16-\sug\p q2 |
    r8. <mib' lab>16 q2 |
    r8. <fa' lab>16-\sug\f q2 |
    r8. <fa' sib>16 q2 |
    R2. |
    r4 r do''8.-\sug\mf lab'16 |
    sol'( mib' sib mib' sol' mib' sib' sol' re' fa' sib' re') |
    mib'8. sib16 sib4 r |
    R2. |
    r4 r do''8.-\sug\mf lab'16 |
    sol'16 mib' sib mib' sol' mib' sol' mib' re' fa' re' fa' |
  }
>>
<>^\markup\italic { Serrer un peu }
sol'16 mib'8 sol' sib' sol' do'' mib'16 |
re' re'8 fa' sib' fa' re' sib16~ |
sib mib'8 sol' sib' sol' do'' mib'16 |
re' re'8 fa' sib' fa' re' sib16~ |
sib mib'8 sib mib' sol' sib' mib'16 |
si16 si8 re' sol' re' si sol16 |
sol do'8 sol' do'' sol' mib' do'16 |
si si8 re' sol' re' si sol16 |
do'4 do'8.\f dod'16 dod'8.\trill si32 dod' |
<<
  \tag #'violino1 {
    re'4. re'''8\p( do''' sib'' |
    la'' sib'' do''' la'' sib'' sol'') |
    fad''8\f re''4 re'''8( do''' sib'' |
    la'' sib'' do''' la'' sib'' sol'') |
    fad''2\fermata
  }
  \tag #'violino2 {
    re'16. re'32 re'16. re'32 re'16.\p re'32 re'16. re'32 re'16. re'32 re'16. re'32 |
    re'16. re'32 re'16. re'32 re'16. re'32 re'16. re'32 re'16. re'32 re'16. re'32 |
    re'16.\f re'32 re'16. re'32 re'16. re'32 re'16. re'32 re'16. re'32 re'16. re'32 |
    re'16. re'32 re'16. re'32 re'16. re'32 re'16. re'32 re'16. re'32 re'16. re'32 |
    re'2\fermata
  }
>> r8.\fermata <mib' sol>16\f |
q2\fermata <<
  \tag #'violino1 {
    mib''8.\p mib''16 |
    mib''8. mib''16 mib''8. mib''16 re''8. re''16 |
    do''2
  }
  \tag #'violino2 {
    sol'8.-\sug\p sol'16 |
    fa'8. fa'16 fa'8. fa'16 fa'8. fa'16 |
    mib'2
  }
>> r8\fermata r |
<mib' sib' sol''>2\fermata\f <<
  \tag #'violino1 {
    sol'8.\p sol'16 |
    <>\cresc \rt#16 fa'32 <>\!\f \rt#8 fa' |
  }
  \tag #'violino2 {
    mib'8.-\sug\p mib'16 |
    <>-\sug\cresc \rt#16 mib'32 <>\!-\sug\f \rt#8 re' |
  }
>>
<sol mib'>8. sol''16\trill sib'' sol'' mib'' sol'' sib' mib'' sol' sib' |
mib'8. sib16 <sol mib'>4 r |
