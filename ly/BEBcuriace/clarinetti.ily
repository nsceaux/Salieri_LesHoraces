\clef "treble"
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { sib'8-\sug\p ^\markup\whiteout Solo |
    mib''4~ mib''8.( re''16 do''8. si'16) |
    si'4( do'') r8. do''16-\sug\f |
    do''4~ do''8.( re''16 mib''8. fa''16) |
    fa''4( lab') r8 lab'-\sug\p |
    sol'4.( sol'8 lab' sib') |
    do'' re''16 mib'' mib''4 \grace fa''16 mib''8-\sug\f re''16 do'' |
    sib'4~ sib'8 mib'' re''8. re''16 |
    mib''8. sib'16 mib''4 r |
    R2.*3 |
    r4 r r8 fa' |
    sol'4.( sol'8 lab' sib') |
    do'' re''16 mib'' mib''4 mib''8.-\sug\mf do''16 |
    sib'2 sib'8. re''16 |
    re''8.-\sug\f mib''16 mib''4 r |
    sol''4.\fp fa''8 mib'' re'' |
    \grace re''4 do'' do'' mib''8.-\sug\mf do''16 |
    sib'8 sib''4 sib'' lab''8 |
    sol''-\sug\f mib''~ mib''16 mib'' re'' mib'' \grace lab'' sol''8 fa''16 mib'' |
    do'''16 sib'' sib''4-\sug\p sib' sib'8 |
    sib'4~ sib'16 mib''(-\sug\f re'' mib'' \grace lab''16 sol''8 fa''16 mib'') |
    do''' sib'' sib''4-\sug\p sib'16( do'' \grace mib''16 re''8 do''16 sib') |
    mib''2.-\sug\cresc |
    mib''2.\!-\sug\fp |
    mib''-\sug\f |
    mi''!-\sug\mf -\sug\cresc |
    fa''\!-\sug\f |
    mib''!2-\sug\fp re''8. do''16 |
    sib'8-\sug\f sol''4 sol'' sol''8 |
    sol''2 fad''4 |
    sol''2.-\sug\ff |
    sol''2 fad''4 |
    sol'' r\fermata r |
    mib''4.\p re''8 do''8. si'16 |
    si'4 do'' r8 do''-\sug\f |
    do''4. re''8 mib''8. fa''16 |
    fa''4( lab') r8 fa' |
    sol'4. sol'8 lab' sib' |
    do''8 re''16 mib'' mib''4 r |
    sib'8-\sug\mf sib'4 sib' re''8 |
    re''8. mib''16 mib''4 r8 sib' |
    sol''4.\fp fa''8 mib'' re'' |
    re'' do'' do''4 r |
    r8 sib''4-\sug\mf sib'' lab''8 |
    sol''4 r r |
    r8 fa'' r fa'' r fa'' |
    r sol'' r4 r |
    r8 fa'' r fa'' r fa'' |
    r sol''4 sol''8 sol'' sol'' | }
  { r8 |
    R2.*5 |
    r4 r do''8-\sug\f lab' |
    sol'4~ sol'4 lab'8. lab'16 |
    sol'4 sol' r |
    R2.*5 |
    r4 r do''8.-\sug\mf lab'16 |
    sol'2 sol'8. fa'16 |
    fa'8.-\sug\f sol'16 sol'4 r |
    mib''4.\fp re''8 do'' sib' |
    \grace sib'4 lab' lab' do''8.-\sug\mf lab'16 |
    sol'8 sol''4 sol''8 fa'' fa'' |
    mib''8 sol'4 sol'8 mib' sol' |
    lab' lab'4-\sug\p lab' lab'8 |
    sol' sol'4 sol'8-\sug\f mib' sol' |
    lab' lab'4-\sug\p lab' lab'8 |
    sol'2.-\sug\cresc |
    reb''2.\!-\sug\fp |
    do''-\sug\f |
    sib'-\sug\mf -\sug\cresc |
    lab'\!-\sug\f |
    la'!-\sug\fp |
    sol'8-\sug\f sib'4 sib' sib'8 |
    la'2 la'4 |
    sol'2-\sug\ff~ sol'8 sol' |
    sib'2 la'4 |
    sib'4 r\fermata r |
    R2.*7 |
    r4 r r8 sib' |
    mib''4.-\sug\fp re''8 do'' sib' |
    sib' lab' lab'4 r |
    r8 sol''4-\sug\mf mib''8 re'' fa'' |
    mib''4 r r |
    r8 sib' r sib' r sib' |
    r8 mib'' r4 r |
    r8 sib' r sib' r sib' |
    r mib''4 mib''8 mib'' mib'' | }
>>
R2.*8 |
r4 r\fermata r8.\fermata <>-\sug\f
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { mib''16 |
    mib''2\fermata mib''8.-\sug\p mib''16 |
    mib''8. mib''16 mib''8. mib''16 re''8. re''16 |
    do''2 r8\fermata r |
    sol''2-\sug\f\fermata sol''8.-\sug\p sol''16 |
    fa''8.-\sug\cresc fa''16 fa''8. fa''16 fa''8.-\sug\f fa''16 |
    mib''4 mib''8 mib'' mib'' mib'' |
    mib''8. sib'16 mib''4 r | }
  { sol'16 |
    sol'2\fermata sol'8.-\sug\p sol'16 |
    fa'8. fa'16 fa'8. fa'16 fa'8. fa'16 |
    mib'2 r8\fermata r |
    sib'2-\sug\f\fermata mib''8.-\sug\p mib''16 |
    mib''8.-\sug\cresc mib''16 mib''8. mib''16 re''8.-\sug\f re''16 |
    mib''4 sol'8 sol' sol' sol' |
    sol'8. sib'16 sol'4 r | }
>>
