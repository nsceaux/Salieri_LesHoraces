<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    \clef "tenor"
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { sib8-\sug\p ^\markup\whiteout Solo |
        mib'4~ mib'8.( re'16 do'8. si16) |
        si4( do') r8. do'16-\sug\f |
        do'4~ do'8.( re'16 mib'8. fa'16) |
        fa'4( lab) r |
        R2. |
        r4 r \grace fa'16 mib'8-\sug\f re'16 do' |
        sib4~ sib16( mib' sol sib sib lab fa re) |
        mib8. sib,16 mib4 r |
        R2.*4 |
        r8 sib( mib' reb' do' sol) |
        lab sib16 do' do'4 lab,-\sug\mf |
        sib,2. |
        mib8.-\sug\f mib16 mib4 r |
        sol'4.\fp fa'8 mib' re' |
        \grace re'4 do' do'-\sug\mf }
      { r8 |
        R2.*16 |
        mib'4.\fp re'8 do' sib |
        \grace sib4 lab lab-\sug\mf }
    >> lab,4 |
    sib,2. |
    mib8
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { sol4 sol8 sol sol |
        lab lab4 lab lab8 |
        sol sol4 sol8-\sug\f sol sol |
        lab lab4-\sug\p lab lab8 |
        sol2-\sug\cresc sol16 lab sib do' |
        reb'2.\!-\sug\fp |
        do'-\sug\f |
        sib-\sug\mf -\sug\cresc |
        lab4.\!-\sug\f do'8 do' do' |
        do'2-\sug\fp re'4 |
        re' }
      { mib4 mib8 mib mib |
        fa fa4 fa fa8 |
        mib mib4 mib8-\sug\f mib mib |
        fa fa4-\sug\p fa fa8 |
        mib2.-\sug\cresc |
        sol,4\!-\sug\fp sol2 |
        lab2.-\sug\f |
        sol-\sug\mf -\sug\cresc |
        fa4.\!-\sug\f lab8 lab lab |
        la2.-\sug\fp |
        sib4 }
    >> \clef "bass"
  }
  \tag #'basso {
    \clef "bass" r8 |
    r8. mib16\p mib2 |
    r8. lab,16 lab,2 |
    r8. fa16\f fa2 |
    r8. sib,16 sib,2 |
    R2. |
    r4 r lab,8\f lab, |
    \rt#6 sib,8 |
    mib8. sib,16 mib4 r |
    r8. mib16\p mib2 |
    r8. lab,16 lab,2 |
    r8. fa16\f fa2 |
    r8. sib,16 sib,2 |
    R2. |
    r4 r lab,8.\mf lab,16 |
    \rt#6 sib,8 |
    mib8.\f mib16 mib4 r |
    R2. |
    r4 r lab,8\mf lab, |
    \rt#6 sib,8 |
    <>\f \rt#6 mib |
    re re <>\p \rt#4 re |
    mib mib <>\f \rt#4 mib |
    re re <>\p \rt#4 re |
    <>\cresc \rt#6 mib |
    <>\!\fp \rt#6 sol, |
    <>\f \rt#6 lab, |
    sol,\mf sol <>\cresc \rt#4 sol |
    <>\f \rt#6 fa |
    <>\fp \rt#6 fad |
    sol4\f
  }
>> r8. sol,32\f lab, sib,8. sol,16 |
do4 r8 la,16 sib,32 do re8. re16 |
mib4\ff~ mib16 fa32 sol lab sib do' re' mib'8. mib16 |
\rt#6 re8 |
sol,4 r\fermata r4 |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    \clef "tenor"
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { mib'4.\p re'8 do'8. si16 |
        \grace si4 do'2 r8 do'-\sug\f |
        do'4. re'8( mib'8. fa'16) |
        fa'4( lab) r |
        r8 sib( mib' reb' do' sol) |
        lab8 sib16 do' do'4 lab, |
        sib,2. |
        mib4 r r |
        sol'4.\fp fa'8 mib' re' |
        re' do' do'4
      }
      { R2.*8 |
        mib'4.-\sug\fp re'8 do' sib |
        sib lab lab4 }
    >> lab,4-\sug\mf |
    sib,2. |
    mib4 r r |
    R2.*3 |
    mib'2. |
    si |
    do' |
    si |
    do'4
  }
  \tag #'basso {
    r8. mib16\p mib2 |
    r8. lab,16 lab,2 |
    r8. fa16\f fa2 |
    r8. sib,16 sib,2 |
    R2. |
    r4 r lab,8.\mf lab,16 |
    \rt#6 sib,8 |
    mib8. mib16 mib4 r |
    R2. |
    r4 r lab,8\mf lab, |
    \rt#6 sib, |
    mib8(\p sol sib sol do' mib) |
    re( fa sib fa re sib,) |
    mib sol sib sol do' mib |
    re fa sib fa re sib, |
    mib sib, mib sol sib mib |
    si, re sol re si, sol, |
    do sol do' sol mib do |
    si, re sol re si, sol, |
    do4
  }
>> do8.\f dod16 dod8.\trill si,32 dod |
re8( re'\p do' sib la sol) |
fad( sol la fad sol sib) |
re\f( re' do' sib la sol) |
fad( sol la fad sol sib) |
re'2\fermata r8.\fermata mib16\f |
mib2\fermata <<
  \tag #'(fagotto1 fagotto2 fagotti) {
    sol,4-\sug\p |
    lab,2 sib,4 |
    do2 r8\fermata r |
    mib2-\sug\f\fermata
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { sol4-\sug\p |
        fa2-\sug\cresc fa4-\sug\f |
        mib8. sib16 sib8 sib sib sib |
        sib4 sib r | }
      { mib4-\sug\p |
        mib2-\sug\cresc re4-\sug\f |
        mib8. sol16 sol8 sol sol sol |
        sol4 sol r | }
    >>
  }
  \tag #'basso {
    sol,8.\p sol,16 |
    lab,4~ lab,8. lab,16 sib,8. sib,16 |
    do2 r8\fermata r |
    mib2\f\fermata mib8.\p mib16 |
    lab,2\cresc sib,8.\!\f sib,16 |
    mib4 mib8 mib mib mib |
    mib4 mib r |
  }
>>
