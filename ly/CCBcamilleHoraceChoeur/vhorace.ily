\clef "vbasse" r16 |
R1*4 |
r2 r4 mib |
mib'4 mib'8 mib' mib'4 do'8 do' |
la2 r |
R1 |
r2 r8 sib sib sib |
mib'2 mib'4 do'8 do' |
la2 la4 r |
R1*2 |
r4 sib8[ do'] reb'4. reb'8 |
do'4 r r2 |
R1 |
mib'2 do'4. lab8 |
sib1 |
sib4 sib8[ do'] reb'4. reb'8 |
do'4 r r2 |
R1 |
mib'2 do'4. lab8 |
sib1 |
