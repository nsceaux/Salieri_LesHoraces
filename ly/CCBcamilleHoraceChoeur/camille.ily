\clef "vbas-dessus" r16 |
R1*4 |
r4 r8 sib' sol'2 |
sol'4 sol'8 sol' sol'4. mib''8 |
\grace re''4 do''2 r |
R1 |
r2 fa''4. sol''8 |
sol'4. sol'8 sol'4 mib''8 mib'' |
do''2 do''4 r |
R1 |
r4 mib''2 mib''4 |
mib''1~ |
mib''4 lab'' lab'' lab'' |
lab''2 r4 sib' |
sib''2 lab''4. lab''8 |
sol''2.( fa''4) |
mib''4 mib''2 mib''4 |
mib'' lab'' lab'' lab'' |
lab''2 r4 sib' |
sib''2 lab''4. lab''8 |
sol''2.( fa''4) |
