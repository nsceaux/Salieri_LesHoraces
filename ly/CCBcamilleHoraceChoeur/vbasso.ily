\clef "vbasse" r16 |
r2 r8 sib sib sib |
mib4. mib8 mib4. mib8 |
lab2 r4 lab8 lab |
fa4. fa8 fa4 fa8 fa |
mib2 mib4 r |
R1 |
r2 r8 do' do' do' |
la2 la4 la8 la |
sib2 sib4 r |
R1 |
r2 r4 do' |
la la8 la la4 la8 la |
sib4 r r2 |
R1 |
r4 r8 do' lab4. lab8 |
sib2 r |
mib'2 do'4. lab8 |
sib1 |
mib4 r r2 |
r4 r8 do' lab4. lab8 |
sib2 r |
mib'2 do'4. lab8 |
sib1 |
