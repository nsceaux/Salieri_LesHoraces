\clef "treble"
\twoVoices #'(oboe1 oboe2 oboi) <<
  { mib''16 | mib''2~ mib''4 }
  { sol'16 | sol'2~ sol'4 }
  { s16-\sug\f }
>> r4 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sib'1 | do'' | re'' | mib'' | }
  { sol'1 | lab' | fa' | mib' | }
  { s1-\sug\mf | s1*2 | s1-\sug\p }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { s4 sol''2 mib''4 |
    do'' do''2 do''4 |
    la''2. do'''4 |
    sib''1 |
    s4 sol''2 mib''4 |
    do''4 do''2 do''4 |
    la''2. do'''4 |
    sib''4 }
  { s4 mib''2 do''4 |
    la' la'2 la'4 |
    do''2. mib''4 |
    re''1 |
    s4 mib''2 do''4 |
    la'4 la'2 la'4 |
    do''2. mib''4 |
    re''4 }
  { r4 s2. | s4 s2.-\sug\f | s1*2 | r4 s2.-\sug\p | s4 s2.-\sug\f }
>> r4 r2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { s4 sib'8( do'' reb''4.) reb''8 |
    do''4 do''2 fa''4 |
    re'' s sol''2~ |
    sol'' lab''4. lab''8 |
    sol''2. fa''4 |
    mib''8 s sib'8( do'' reb''2) |
    do''4 do''2( fa''4) |
    re''4 s sol''2~ |
    sol'' lab'' |
    sol''2. fa''4 | }
  { s4 sol'8( lab' sib'4.) sib'8 |
    lab'4 lab'2 lab'4 |
    fa' s mib''2~ |
    mib'' mib''4. fa''8 |
    mib''2. re''4 |
    mib''8 s sol'8( lab' sib'2) |
    lab'4 lab'2( sib'4) |
    fa' s mib''2~ |
    mib''~ mib''4. fa''8 |
    mib''2. re''4 | }
  { r4 s2. | s1 | s4 r s2\f | s1*2 | s8 r s2.-\sug\p |
    s4 s2.-\sug\mf | s4 r s2-\sug\ff | s1*2 }
>>
