\clef "alto" <mib sib>16-\sug\f |
q2~ q8 sib[-\sug\mf sib sib] |
sol sol4 sib mib' sol'8 |
lab' mib'4 do' lab do'8 |
fa lab4 re' fa' fa'8 |
mib' sol'4-\sug\p mib' sib sol8 |
sol8 sol4 sol sol do'8 |
la8 la4-\sug\f do' fa' la'8 |
la' do'4 la do' do'8 |
sib re'4-\sug\p re' re' re'8 |
mib' do'4 sol sol sol8 |
fa la4-\sug\f do' fa' la'8~ |
la' do'4 la do' do'8 |
sib4 r r2 |
r4 sol8( lab sib4.) sib8 |
lab4 lab2-\sug\mf lab4 |
fa2 r |
sol8-\sug\ff sib mib' sol lab mib' do' lab |
\rt#4 sib8 \rt#4 sib |
mib'8 r sol8(-\sug\p lab sib4.) sib8 |
lab4 lab2-\sug\mf fa4 |
fa2 r |
sol8-\sug\ff sib mib' sol lab do' lab' lab |
\rt#4 sib \rt#4 sib |
