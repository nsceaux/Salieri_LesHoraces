\clef "vhaute-contre" r16 |
r2 r8 sib sib sib |
mib'4. mib'8 sol'4. mib'8 |
do'2 r4 do'8 mib' |
re'4. fa'8 lab'4 fa'8 re' |
mib'2 mib'4 r |
R1 |
r2 r8 do' do' do' |
fa'2 fa'4 sol'8 mib' |
re'2 re'4 r |
R1 |
r2 r4 do' |
fa' fa'8 fa' fa'4 sol'8 mib' |
re'4 r r2 |
R1 |
r4 r8 do' fa'4. fa'8 |
fa'2 sol'~ |
sol' lab'4. fa'8 |
mib'2.( re'4) |
mib'4 r r2 |
r4 r8 do' fa'4. fa'8 |
fa'2 sol'~ |
sol' lab'4. fa'8 |
mib'2.( re'4) |
