\clef "treble" \transposition mib
\twoVoices #'(corno1 corno2 corni) <<
  { do''16 | do''2~ do''4 }
  { mi'16 | mi'2~ mi'4 }
  { s16-\sug\f }
>> r4 |
\twoVoices #'(corno1 corno2 corni) <<
  { do''1~ | do'' | sol' | do''~ | do''4 }
  { do'1~ | do' | sol | do'~ | do'4 }
  { s1-\sug\mf | s1*2 | s1-\sug\p }
>> r4 r2 |
r4 re''2-\sug\f re''4 |
re''1 |
\twoVoices #'(corno1 corno2 corni) <<
  { sol''1 }
  { sol' }
>>
R1 |
r4 re''2-\sug\f re''4 |
re''1 |
sol'4 r r2 |
R1*3 |
\twoVoices #'(corno1 corno2 corni) <<
  { sol''2 fa''4. fa''8 | mi''2. re''4 | do''8 }
  { mi''2 do''4. do''8 | do''2. sol'4 | do'8 }
  { s1-\sug\f }
>> r8 r4 r2 |
R1*2 |
\twoVoices #'(corno1 corno2 corni) <<
  { sol''2 fa''4. fa''8 | mi''2. re''4 | }
  { mi''2 do''4. do''8 | do''2. sol'4 | }
  { s1-\sug\ff }
>>
