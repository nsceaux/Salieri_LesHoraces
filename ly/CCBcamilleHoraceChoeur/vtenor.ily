\clef "vtaille" r16 |
r2 r8 sib sib sib |
sol4. sol8 sib4. sol8 |
lab2 r4 lab8 lab |
lab4. lab8 lab4 lab8 lab |
sol2 sol4 r |
R1 |
r2 r8 do' do' do' |
do'2 do'4 do'8 do' |
sib2 sib4 r |
R1 |
r2 r4 do' |
do' do'8 do' do'4 do'8 do' |
sib4 r r2 |
R1 |
r4 r8 do' do'4. do'8 |
re'2 mib'~ |
mib' mib'4. do'8 |
sol2.( lab4) |
sol4 r r2 |
r4 r8 do' do'4. do'8 |
re'2 mib'~ |
mib' mib'4. do'8 |
sol2.( lab4) |
