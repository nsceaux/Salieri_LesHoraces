\piecePartSpecs
#`((corno1 #:instrument "Corno I mi♭" #:tag-global ())
   (corno2 #:instrument "Corno II mi♭" #:tag-global ())
   (oboe1)
   (oboe2)
   (clarinetto1 #:score-template "score-clarinette-sib"
                #:notes "oboi" #:tag-notes oboe1)
   (clarinetto2 #:score-template "score-clarinette-sib"
                #:notes "oboi" #:tag-notes oboe2)
   (fagotto1 #:clef "tenor")
   (fagotto2 #:clef "tenor")
   (violino1)
   (violino2)
   (alto)
   (basso #:instrument "Basso")
   (silence #:on-the-fly-markup , #{ \markup\tacet#23 #}))
