<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    \clef "tenor" mib16-\sug\f |
    mib2~ mib4 r |
    mib'1-\sug\mf~ |
    mib'2. lab4 |
    lab1 |
    sol-\sug\p |
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { s4 sol'2 mib'4 | do' do'2 do'4 | do'1 | re' |
        s4 sol'2 mib'4 | do'4 do'2 do'4 | do'1 | re'4 }
      { s4 mib'2 do'4 | la la2 la4 | la1 | sib |
        s4 mib'2 do'4 | la4 la2 la4 | la1 | sib4 }
      { r4 s2. | s4 s2.-\sug\f | s1*2 | r4 s2.-\sug\p | s4 s2.-\sug\f }
    >> r4 r2 |
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { s4 sib8( do' reb'2) | do'2. fa'4 | re' s sol'2~ | sol' }
      { s4 sol8( lab sib2) | lab2. lab4 | fa s mib'2~ | mib' }
      { r4 s2. | s1 | s4 r s2-\sug\f | }
    >> mib'4. do'8 |
    sol2. lab4 |
    sol8 r \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { sib8( do' reb'2) | do'2.( fa'4) | re'4 s sol'2~ | sol' }
      { sol8( lab sib2) | lab2. lab4 | fa s mib'2~ | mib'2 }
      { s2.-\sug\p | s4 s2.-\sug\mf | s4 r s2-\sug\ff | }
    >> mib'4. do'8 |
    sol2. lab4 |
  }
  \tag #'basso {
    \clef "bass" r16 |
    mib2\f~ mib4 r |
    mib\mf mib mib mib |
    lab, lab, lab, lab, |
    sib, sib, sib, sib, |
    mib mib\p mib mib |
    mib mib mib mib |
    fa\f fa fa fa |
    fa fa fa fa |
    sib, sib, sib,\p sib, |
    do do do do |
    fa\f fa fa fa |
    fa fa fa fa |
    sib, r r2 |
    r mib\p |
    lab4 r fa2\mf |
    sib r |
    <>\ff \rt#4 mib8 do do lab, lab, |
    \rt#4 sib, \rt#4 sib, |
    mib8 r r4 mib2\p |
    lab,4 r fa2\mf |
    sib, r |
    <>\ff \rt#4 mib8 do do lab, lab, |
    \rt#4 sib, \rt#4 sib, |
  }
>>
