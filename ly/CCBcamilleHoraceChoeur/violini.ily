\clef "treble" <sol mib'>16\f |
q2~ q8 <<
  \tag #'violino1 {
    sib'[\mf sib' sib'] |
    mib''4. mib''8 sol''4. mib''8 |
    re'' do'' do''2 do''8 mib'' |
    re''4. fa''8 lab''4 fa''8 re'' |
    mib'' mib''4\p mib'' mib'' mib''8 |
    mib'2.( sol4) |
    la8 fa'4\f la' do'' fa''8 |
    fa'' la''4 do''' la' la'8 |
    sib' sib'4\p sib' sib' sib'8 |
    sol' sol'4 sol' mib' mib'8 |
    do' fa'4-\sug\f la' do'' fa''8~ |
    fa'' la''4 do''' la' la'8 |
    sib'4 <mib' mib''>2\sf q4 |
    q1~ |
    q4 r r2 |
    r r4 sib'\ff |
    <mib' sib' sib''>2 <<
      { lab''4. lab''8 | sol''8 sol''4 sol'' sol'' fa''8 | } \\
      { do''4. do''8 | sib'1 | }
    >>
    mib''8 r <mib' mib''>2\sf q4 |
    q r r2 |
    r r4 sib'\ff |
    <mib' sib' sib''>2 lab'8 do'' mib'' lab'' |
    <<
      { \rt#4 sol''8 sol'' sol'' fa'' fa'' | } \\
      { \rt#4 sib'8 \rt#4 sib' | }
    >>
  }
  \tag #'violino2 {
    sib8[-\sug\mf sib sib] |
    sib sib4 mib' sol' sib'8 |
    do'' lab'4 mib' do' lab8~ |
    lab re'4 fa' lab' lab'8 |
    sol' sib'4\p sol' mib' sib8 |
    sib sib4 sib mib' mib'8 |
    do' do'4-\sug\f fa' la' do''8 |
    do'' fa''4 la'' mib' mib'8 |
    re' fa'4-\sug\p fa' fa' fa'8 |
    mib' mib'4 mib' do' do'8 |
    la8 do'4-\sug\f fa' la' do''8~ |
    do'' fa''4 la'' mib' mib'8 |
    re'4 r r2 |
    r4 sib8( do' reb'4.) reb'8 |
    do'4 do'2\mf fa'4 |
    re'2 r4 sib'-\sug\ff |
    <mib' sib' sol''>4 mib''2 do''8 fa'' |
    mib''8 mib''4 mib'' mib'' re''8 |
    mib'' r sib8\p( do' reb'4.) reb'8 |
    do'4 do'2\mf fa'4 |
    re'2 r4 sib'-\sug\ff |
    <mib' sib' sol''>4 mib''2 do''8 fa'' |
    \rt#4 mib''8 mib'' mib'' re'' re'' |
  }
>>