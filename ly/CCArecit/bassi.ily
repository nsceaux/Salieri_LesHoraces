\clef "bass" re8.\ff fad32 la re'16 re' re' re' re' la fad re la,8 la |
re4 r r2 |
r sol4 r |
R1*2 |
r8. mi16 mi8. mib16 mib2~ |
mib1~ |
mib~ |
mib2 r4 fa |
sib,2 r |
