\tag #'romain {
  Vos trois fils sont aux mains, sei -- gneur, & les Dieux mê -- mes
  D’Albe & de Ro -- me ont con -- fir -- mé le choix.
}
\tag #'camille {
  Qu’en -- tends- je ?
}
\tag #'vhorace {
  A -- do -- rons leurs loix.
  Sou -- met -- tons- nous, Ro -- mains, à leurs dé -- crêts su -- prê -- mes.
}
