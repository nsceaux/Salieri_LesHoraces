\clef "alto" re'8.\ff fad'32 la' re''16 re'' re'' re'' re'' la' fad' re' la8 la' |
re'4 r r2 |
r sol'4 r |
R1*2 |
r8. mi'16 mi'8. mib'16 mib'2~ |
mib'1~ |
mib'2 fa~ |
fa r4 fa' |
sib2 r |
