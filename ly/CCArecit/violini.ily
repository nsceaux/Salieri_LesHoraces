\clef "treble"
re'8.\ff fad'32 la' re''16 re'' re'' re'' re'' la' fad' re' la8 <mi' dod'' la''> |
<fad'' la' re'>4 r r2 |
r <re' si' sol''>4 r |
R1*2 |
<<
  \tag #'violino1 {
    r8. do''16 do''8. sol''16 sol''2~ |
    sol''1~ |
    sol''2 la''~ |
    la''
  }
  \tag #'violino2 {
    r8. sol'16 sol'8. do''16 do''2~ |
    do''1~ |
    do''~ |
    do''2
  }
>> r4 <fa' do'' la''>4 |
<fa' re'' sib''>2 r |
