\clef "vbasse" R1 |
r4 la8 la re'4 la8 si |
do'4. re'8 si4^! r16 sol sol la |
si8 si r4 re' re'8 re' |
si si r re' si si si do' |
sol4^! r4 r2 |
R1*4 |
