\clef "treble" do''2 r4 do''8. la'16 |
sold'4. la'8 si'4. sold'8 |
la'2. do''4 |
la'2. fa'4 |
re'2 r\fermata |
sol'2(-\sug\p re') |

do'4.( mi'8 fa'4 sol') |
la'2. sol'4 |
fa'2. mi'4 |
re'2. re''4 |
do''1 |
reb'' |
do''~ |
do'' |
si'4. re''8-\sug\f re''4. mi''8 |
mi''4. do''8 do''4. si'8 |
si'4. fa'8 fa'4. mi'8 |
mi'4 r r2 |
R1 |
r4 do''2-\sug\p si'4 |

do''4. mi'8-\sug\f mi'4 mi' |
mi'1 |
