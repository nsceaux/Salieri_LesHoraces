\clef "vbasse" la2 r4 la8. do'16 |
mi'4. mi8 mi mi mi mi |
la2 r4 la8 la |
re2 re'4 re' |
sol2 r\fermata |
sol4^\p sol8 sol sol4 sol |
do2 r |
R1*15
