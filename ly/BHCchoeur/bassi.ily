\clef "bass" la,2 r4 la,8. do16 |
mi2 mi, |
la,4 la la, la |
re2 re4 re |
sol2 r\fermata |
sol\p sol, |

do4.( do8 re4 mi) |
fa4. fa,8 fa4 mi |
re2. do4 |
si,2. sol,4 |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    \clef "tenor"
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { mib'1~ | mib' | mib'~ | mib' |
        re'4. re'8 re'4. mi'!8 | mi'4. fad'8 fad'4. sol'8 |
        sol'4. fa'8 fa'4. mi'8 | mi'2 re'4 mi' |
        fa'4. fad'8 fad'4. sol'8 | sol'4 mi' re'2 | }
      { do'1 | reb' | do'~ | do' |
        si4. si8 si4. do'8 | do'4. do'8 do'4. si8 |
        si4. si8 si4. do'8 | do'2. do'4 |
        do'2~ do'4. do'8 | do'2. si4 }
      { s1*4 | s4. s8-\sug\f s2 | s1*2 | s2 s-\sug\p | }
    >>
  }
  \tag #'basso {
    lab,4( do mib lab) | sib( sol mib? sol) |
    lab( do' lab sol) | fad( sol lab fad) |
    sol4. sol,8\f sol,4. sol,8 | sol,4. sol,8 sol,4. sol,8 |
    sol,4. sol,8 sol,4. la,!8 | la,4 la\p fa sol |
    la4. lab8 lab4. sol8 | sol2 sol, |
  }
>>
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { do'4. do'8 do'4 do' | do'1 | }
      { do'4. mi8 mi4 mi | mi1 | }
      { s4. s8\f }
    >>
  }
  \tag #'basso {
    do4. do8\f do4 do |
    do1 |
  }
>>
