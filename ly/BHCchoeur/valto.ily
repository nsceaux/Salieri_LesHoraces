\clef "vhaute-contre" la'2 r4 la'8. la'16 |
sold'4. la'8 si' si' si' sold' |
la'2 r4 la'8 la' |
la'2 la'4 la' |
si'2 r\fermata |
sol'4^\p sol'8 sol' re'4 re' |
mi'2 r |
R1*15
