\clef "vtaille" mi'2 r4 mi'8. mi'16 |
mi'4. mi'8 mi' mi' mi' mi' |
mi'2 r4 la8 la |
fa'2 fa'4 fa' |
re'2 r\fermata |
sol4^\p sol8 sol sol4 sol |
sol2 r |
R1*15
