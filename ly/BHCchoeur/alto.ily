\clef "alto" mi'2. mi'4 |
mi'4. mi'8 mi' <mi' si> q q |
<< mi'4 \\ do' >> la' la la |
re'2 re'4 re' |
re'2 r\fermata |
sol'\p sol |

do'4.( do'8 re'4 mi') |
fa'4. fa8 fa'4 mi' |
re'2. do'4 |
si2. sol4 |
lab4( do' mib' lab) |
sib( sol mib sol) |
lab( do' lab sol) |
fad( sol lab fad) |
sol4. sol8-\sug\f sol4. sol8 |
sol4. sol8 sol4. sol8 |
sol4. sol8 sol4. la8 |
la4 la-\sug\p fa sol |
la4. lab8 lab4. sol8 |
sol2 sol4 sol |

mi4. mi8\f mi4 mi |
mi1 |
