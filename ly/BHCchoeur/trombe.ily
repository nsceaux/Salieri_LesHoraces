\clef "treble"
\twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''2 s4 mi''8. mi''16 | mi''1~ | mi''4 s s mi'' | s1 | re''2 }
  { do''2 s4 mi'8. mi'16 | mi'1~ | mi'4 s s do'' | s1 | sol'2 }
  { s2 r4 s | s1 | s4 r r s | re''1 | }
>> r2\fermata |
sol'1\p |

do'1 |
R1*7 |
r4 r8 sol'-\sug\f sol'4. sol'8 |
sol'4. sol'8 sol'4. sol'8 |
sol'4. sol'8 sol'4. sol'8 |
do''4 r r2 |
R1 |
r4 sol'8.\p sol'16 sol'4 sol' |

do'4. do'8\f do'4 do' |
do'1 |
