\clef "vbas-dessus" mi''2 r4 mi''8. mi''16 |
si'4. do''8 re'' re'' re'' mi'' |
do''2 r4 mi''8 mi'' |
fa''2 fa''4 re'' |
si'2 r\fermata |
sol'4^\p sol'8 sol' si'4 si' |
do''2 r |
R1*15

