\clef "treble" <la mi' do'' mi''>2. <<
  \tag #'violino1 {
    mi''8. mi''16 |
    si'4. do''8 re'' re' re' mi' |
    do'4
  }
  \tag #'violino2 {
    do''8. la'16 |
    sold'4. la'8 si' sold sold sold |
    la4
  }
>> <do' mi' la'>4 <la mi' do''> <la' do'' mi''> |
<re' la' fa''>2 q4 <re'' fa'> |
<re' si'>2 r\fermata |
<<
  \tag #'violino1 { sol'2\p( si') | }
  \tag #'violino2 { sol'2-\sug\p( re') | }
>>
<<
  \tag #'violino1 {
    do''1~ |
    do''~ |
    do''4 si'8. si'16 si'4 do'' |
    re''4 fa'8. fa'16 fa'4 fa' |
    mib'8 mib'4 mib' mib' mib'8~ |
    mib' mib'4 mib' mib' mib'8~ |
    mib' mib'4 mib' mib' mib'8~ |
    mib' mib'4 mib' mib' mib'8 |
    re'4. si''!8\f si''4. do'''8 |
    do'''4. fad''8 fad''4. sol''8 |
    sol''4. si'8 si'4. do''8 |
    do''4 mi'8.\p mi'16 re'4 mi' |
    fa'4. fad'8 fad'4. sol'8 |
    sol'4 sol8. sol16 sol4 sol |
  }
  \tag #'violino2 {
    mi'4.( mi'8 fa'4 sol') |
    la'2. sol'4 |
    fa'2. mi'4 |
    re'4 re'8. re'16 re'4 re' |
    do'8 do'4 do' do' do'8 |
    reb' reb'4 reb' reb' reb'8 |
    do' do'4 do' do' do'8~ |
    do' do'4 do' do' do'8 |
    si!4. re''8-\sug\f re''4. mi''!8 |
    mi''4. do''8 do''4. <si' re'>8 |
    <re' si'>4. <fa' re'>8 q4. <mi' do'>8 |
    q4 do'8.-\sug\p do'16 do'4~ do'~ |
    do'4. <<
      { mib'8 mib'4. mi'8 |
        mi'4 mi'8. mi'16 re'4 re' | } \\
      { do'8 do'4. do'8 |
        do'4 do'8. do'16 do'4 si | }
    >>
  }
>>
<<
  \tag #'violino1 {
    sol4. do'8\f do'4 do' |
    do'1 |
  }
  \tag #'violino2 {
    do'4. sol8-\sug\f sol4 sol |
    sol1 |
  }
>>
