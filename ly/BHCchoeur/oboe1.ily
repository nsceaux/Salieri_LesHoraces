\clef "treble" mi''2 r4 mi''8. mi''16 |
si'4. do''8 re'' re'' re'' mi'' |
do''2. mi''4 |
fa''2 fa''4 re'' |
si'2 r\fermata |
sol'(\p si') |

do''1~ |
do''~ |
do''4 si'8. si'16 si'4 do'' |
re''4 fa'2 fa''4 |
mib''1~ |
mib''~ |
mib''~ |
mib'' |
re''4. si''!8\f si''4. do'''8 |
do'''4. fad''8 fad''4. sol''8 |
sol''4. si'8 si'4. do''8 |
do''4 r r2 |
R1 |
r4 mi''\p re''2 |

do''4. do''8\f do''4 do'' |
do''1 |
