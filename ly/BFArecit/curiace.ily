\clef "vhaute-contre" R1*2 |
r4 r8 re' sol4 r |
R1*7 |
r2 r4 r8 do' |
la la r4 do' do'8 do' |
fa'4 do'8 re' mib'4 mib'8 fa' |
re'4^! r r2 |
R1*8 |
