\clef "bass" fad4 r r2 |
R1 |
sol4 r r2 |
sol1\f~ |
sol2 do4 r |
R1 |
fa4 r r2 |
re4 r sold2~ |
sold la~ |
la sol~ |
sol fa~ |
fa1~ |
fa |
sib,4 r r2 |
r8. lab,16 lab,4 r2 |
r8. sol16 sol4~ sol2~ |
sol1 |
r8. lab16 lab8. lab16 sol2~ |
sol fa~ |
fa sol~ |
sol1~ |
sol |
