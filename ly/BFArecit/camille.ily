\clef "vbas-dessus" R1*3 |
sol''4^! re''8 re'' si'4 r8 re'' |
re'' mi'' fa'' sol'' mi''4^! r |
R1*2 |
r4 fa''8 r16 fa'' mi''4^! r8 mi''16 mi'' |
mi''4 re''8 mi'' do''4^! r8 do'' |
do'' do'' do'' re'' mi''4^! r8 sol'' |
mi'' mi'' mi'' fa'' do''^! do'' r4 |
R1*4 |
r4 r8 sib' sib' sib' sib' sib' |
mib''4 sib'8 do'' reb''4 reb''8 mib'' |
do''4^! r8 do'' mi''!4 mi''8 sol'' |
sol''4 sib'8 do'' lab'^! lab' r8 do'' |
fa''4^! r8 re''! si'!^! si' si' do'' |
re''4 r sol''8 re''16 re'' re''8 re'' |
si'!4 re''8 mib'' fa''4 re''8 mib'' |
