\tag #'jhorace {
  Ne tar -- dons plus : viens, suis- moi, Cu -- ri -- a -- ce !
}
\tag #'curiace {
  Mar -- chons !
}
\tag #'camille {
  Non, de -- meu -- rez ; je ne vous quit -- te pas.
}
\tag #'jhorace {
  Ma sœur, quelle est donc cette au -- da -- ce ?
  Ah ! mar -- chons…
}
\tag #'camille {
  Non, cru -- els, je m’at -- tache à vos pas.
  Vous ne com -- met -- trez pas ce crime a -- bo -- mi -- na -- ble.
}
\tag #'curiace {
  Ho -- ra -- ce ! ah ! re -- te -- nez ces hor -- ri -- bles é -- clats.
}
\tag #'jhorace {
  Cet ex -- cès de foi -- bles -- se, ô Ciel ! est- il croy -- a -- ble.
}
\tag #'camille {
  Je veux in -- té -- res -- ser Albe & Rome à mes cris.
  Voy -- ez quel -- le rage est la vô -- tre !
  O Ciel ! deux frè -- res, deux a -- mis
  Brû -- lent de se bai -- gner dans le sang l’un de % l’au -- tre.
}
