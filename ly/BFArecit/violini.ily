\clef "treble" <la fad' re''>4 r r2 |
R1 |
<si'! re'>4 r r2 |
<<
  \tag #'violino1 {
    \appoggiatura { re'16[ si'] } sol''1\f~ |
    sol''2 mi''4 r |
    R1 |
    fa''4 r r2 |
    <re' la' fa''>4 r mi''2~ |
    mi''1~ |
    mi''~ |
    mi''2 fa''~ |
    fa''1~ |
    fa''1 |
    fa''4 r r2 |
    r8. << { sib''16 sib''4 } \\ { re''16 re''4 } >> r2 |
    r8. << { sib''16 sib''4~ sib''2~ | sib''1 | }
      \\ { sib'16 sib'4~ sib'2~ | sib'1 | } >>
    r8. do'''16 do'''8. do'''16 mi''!2~ |
    mi'' fa''~ |
    fa'' << { fa''~ | fa''1~ | fa'' | }
      \\ { re''2~ | re''1~ | re'' | } >>
  }
  \tag #'violino2 {
    <>-\sug\f <re' si'>1~ |
    q2 <mi' do''>4 r |
    R1 |
    la'4 r r2 |
    <re' re''>4 r si''!2~ |
    si'' do'''~ |
    do''' sib''~ |
    sib'' la''~ |
    la''1~ |
    la'' |
    sib''4 r r2 |
    r8. << { fa''16 fa''4 } \\ { sib'16 sib'4 } >> r2 |
    r8. << { mib''16 mib''4~ mib''2~ | mib''1 | }
      \\ { mib'16 mib'4~ mib'2~ | mib'1 | } >>
    r8. mib''16 mib''8. mib''16 sib'2~ |
    sib'2 lab' |
    lab'' sol''~ |
    sol''1~ |
    sol''1*15/16~ \hideNotes sol''16 |
  }
>>
