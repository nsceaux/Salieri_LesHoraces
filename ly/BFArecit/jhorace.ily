\clef "vtaille" r4 r16 la la la re'4 r |
re'4. la8 do'4. re'16 re' |
si!8^! si r4 r2 |
R1 |
r2 r4 r8 sol |
do'4 r8 sol16 sol do'4 do'8 sol |
la^! la r4 la4. la8 |
re'4^! r r2 |
R1*5 |
r4 r8 sib16 sib sib4 sib8 do' |
re' re' r fa' re' re' do' sib |
mib'^! mib' r4 r2 |
R1*6 |
