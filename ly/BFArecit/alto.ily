\clef "alto" fad'4 r r2 |
R1 |
sol'4 r r2 |
<>-\sug\f sol'1~ |
sol'2 sol'4 r |
R1 |
fa'4 r r2 |
re'4 r mi'2~ |
mi'1~ |
mi'~ |
mi'2 do''~ |
do''1~ |
do'' |
re''4 r r2 |
r8. lab16 lab4 r2 |
r8. sol'16 sol'4~ sol'2~ |
sol'1 |
r8. lab'16 lab'8. lab'16 reb''2~ |
reb'' do''~ |
do'' si'!~ |
si'1~ |
si' |
