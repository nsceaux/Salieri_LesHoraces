\clef "alto" r4 |
la'8-\sug\f la' la' la' la' la' mi' mi' |
\rt#4 fad' \rt#4 fad' |
\rt#4 fad' \rt#4 re' |
\rt#4 mi' \rt#4 re' |
dod'8 mi' dod' la sold sold' sold' sold' |
la' la si dod' re' fad' mi' re' |
\rt#4 dod' \rt#4 re' |
\rt#4 mi' \rt#4 mi' |
la'4 la la r |
<>\p \rt#4 la8 \rt#4 mi |
\rt#4 fad \rt#4 fad |
\rt#4 fad \rt#4 re |
\rt#4 mi \rt#4 mi |
\rt#4 fad \rt#4 fad |
<>-\sug\mf \rt#4 sold \rt#4 sold |
la4-\sug\p fad8 re \rt#4 mi |
\rt#4 la la la-\sug\cresc([ sold sol]) |
fad2 fa |
mi4 re!4.-\sug\f\fermata fad'8-\sug\p( mi' re') |
\grace re'4 dod' la8( dod' mi'2) |
r8 dod' dod' dod' r dod' dod' dod' |
r re' r re' r si r si |
r dod' dod' dod' r dod' dod' dod' |
<>-\sug\cresc \rt#4 fad' \rt#4 red' |
mi'4.-\sug\f mi'8 mi'8. mi'16 mi'8. mi'16 |
mi'4 mi'8. mi'16 mi'4 r |
la8 la4 la la la8 |
sold! sold4 sold sold-\sug\p sold8 |
la la4 la la la8 |
sold sold4 sold sold sold8 |
la1 |
re2 red |
mi r |
%%
do'!8\f do'[ do' do'] \rt#4 do' |
\rt#4 re' \rt#4 re' |
<>\p \rt#4 sol' \rt#4 sol |
\rt#4 do' \rt#4 do' |
\rt#4 mi' \rt#4 mi' |
\rt#4 fa' \rt#4 fa' |
mi'8\cresc do' mi' do' fa' re' fa' re' |
<>\f \rt#4 mi' mi' fa'\p fa' fa' |
\rt#4 sol' \rt#4 sol |
do'8 do'4 do' do' do'8 |
mi mi4 mi mi mi8 |
fa fa4 fa8 mi mi4 mi8 |
red red4 red red red8 |
mi' mi' r mi'-\sug\cresc r mi' r sold' |
la'-\sug\f fa'! do' la la la la la |
la1\fermata\p |
mi2 si'-\sug\sf |
la'( sold') |
la'4( sold') r2\fermata |
<>\p \rt#4 la'8 \rt#4 mi' |
\rt#4 fad' \rt#4 fad' |
\rt#4 fad' \rt#4 re' |
\rt#4 mi' \rt#4 mi' |
\rt#4 fad' \rt#4 fad' |
<>\mf \rt#4 sold' \rt#4 sold' |
la'8 la' fad' re' \rt#4 mi' |
\rt#4 la la la-\sug\cresc( sold sol) |
\rt#4 fad \rt#4 fa |
mi4 re2-\sug\f\fermata re'4-\sug\p |
mi'1 |
la8\f la([ si dod']) re' fad'( mi' re') |
<>\p \rt#4 dod'8 \rt#4 re' |
<>\cresc \rt#4 mi' \rt#4 mi' |
<>\f \rt#4 la8 mi' mi' re' re' |
dod'8 mi' dod' la sold sold' sold' sold' |
la' la si dod' re' fad' mi' re' |
\rt#4 dod' \rt#4 re' |
\rt#4 mi' \rt#4 mi' |
la4 r8. la16 la8. la16 la8. la'16 |
