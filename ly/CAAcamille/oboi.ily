\clef "treble"
\twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''8. re''16 |
    \grace re''8 dod''4 dod''2 si'4 |
    \grace si'8 la'4 la'8. dod''16 la'4 }
  { dod''8. si'16 \grace si'8 la'4 la'2 sold'4 |
    \grace sold'8 la'4 fad'8. la'16 fad'4 }
  { s4-\sug\f }
>> r4 |
R1 |
r2 \twoVoices #'(oboe1 oboe2 oboi) <<
  { sold''4. la''16 si'' | la''2 si'' |
    dod'''8 la''4 la'' la''8 sold'' fad'' |
    mi''2 fad''4. sold''16 la'' |
    la''4 dod''4. mi''8 \grace mi''8 re''8 dod''16 si' |
    la'4 }
  { si'2 | mi'' mi'' |
    mi''8 dod''[ re'' mi''] fad'' la'4 la'8 | la'1 |
    la'4 la'2 sold'4 | la' }
>> r4 r2 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { dod''2 si'4 |
    \grace si'8*2 la'4 dod''8. fad''16 dod''4 }
  { la'2 sold'4 | \grace sold'8*2 la'4 la' la' }
  { s2-\sug\p }
>> r4 |
R1*2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { s8 la''4 la'' la'' la''8 | s si''4 si'' si'' si''8 | }
  { s8 red''4 red'' red'' red''8 | s re''!4 re'' re'' re''8 }
  { r8 s2. s8 | r s2.-\sug\mf s8-\sug\p }
>>
R1 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { dod''8. mi''16 dod''4 }
  { la'8. dod''16 la'4 }
>> r4 |
R1 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''4.\fermata }
  { la'4.\fermata }
  { s4.-\sug\f }
>> r8 r4 |
R1 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { s8 mi'' mi'' mi'' s mi'' mi'' mi'' |
    fad''2 sold'' |
    la''8 mi''[ mi'' mi''] s mi'' mi'' mi'' |
    red''2 fad'' |
    si'4. sold''8 sold''8. si''16 si''8. sold''16 |
    mi''4 }
  { s8 dod'' dod'' dod'' s dod'' dod'' dod'' |
    re''2 si' |
    dod''8 dod''[ dod'' dod''] s dod'' dod'' dod'' |
    la'1 |
    sold'4. si'8 si'8. sold'16 sold'8. si'16 |
    sold'4 }
  { r8 s4.-\sug\p r8 s4. | s1 | s2 r8 s4. | s1-\sug\cresc | s\f }
>> mi'8. mi'16 mi'4 r |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''1 | mi''~ | mi''2 do'' | mi''1 |
    mi''4 re''4. re''8( mi'' fa'') | si'4 si''2 la''4 | sold''2 }
  { la'1 | si' | do''2 la' | si'1 |
    la' | si' | si'2 }
  { s1*2 | s1-\sug\p }
>> r2 |
%%
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''!2 mi''4. sol''8 |
    sol''8 fa'' fa''4. mi''8 re'' do'' |
    si'4. la''8 sol''4. fa''8 |
    mi''4 mi''8. sol''16 mi''4 }
  { mi'2 do''4. mi''8 |
    si'4 si'2 fa'4 |
    re'2 si' |
    do''4 do''8. mi''16 do''4 }
  { s1\f s1 s1-\sug\p }
>> r4 |
R1*2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''1~ | sol''2 }
  { sol'2 si' | do'' }
  { s1-\sug\cresc | s2-\sug\f }
>> r |
R1 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''2. mi''8 re'' | dod''1 | re''2 do''! | si'1 |
    si'2. sold''4 | la''1~ |
    la''2.\fermata red''4 | s2 \once\slurDashed sold''4.( la''16 si'') |
    la''2 si''4.-\sug\sf do'''16 re''' | do'''4 si'' }
  { mi'2. sol'4 | sol'1 | la' | la' |
    sold'2. si'4 | la'1~ | la'\fermata | s2 re'' |
    do'' sold' | la'4 sold' }
  { s1-\sug\p | s1*3 | s4 s2.-\sug\cresc | s1-\sug\f |
    s1-\sug\p | r2 s-\sug\sf | s1 | s2-\sug\f }
>> r4\fermata r |
%%
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { dod''2 si'4 | \grace si'4 la' la'8. dod''16 la'4 }
  { la'2 sold'4 | \grace sold' la' fad'8. la'16 fad'4 }
  { s2.-\sug\p }
>> r4 |
R1*2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { s8 la''4 la'' la'' la''8 | s si''4 si'' si'' si''8 | }
  { s8 red''4 red'' red'' red''8 | s re''!4 re'' re'' re''8 }
  { r8 s2. s8 | r8 s2.-\sug\mf s8 }
>>
R1 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { dod''8. mi''16 dod''4 }
  { la'8. dod''16 la'4 }
>> r4 |
R1 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''4. }
  { la'4. }
  { s4.-\sug\f\fermata }
>> r8 r4 |
R1 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { la''2~ la''8 la''( sold'' fad'') |
    mi''2 fad''4 sold''8 la'' |
    la''4 dod''4. mi''8 \grace mi''8 re'' dod''16 si' |
    la'2 sold'' |
    la''2 si'' |
    dod'''8 la''4 la'' la''8 sold'' fad'' |
    mi''2 fad''4. sold''16 la'' |
    la''4 dod''4. mi''8 \grace mi'' re'' dod''16 si' |
    la'4 }
  { r8 dod''( re'' mi'') fad'' la'4 la'8 |
    la'1 |
    la'4 la'2 sold'4 |
    la'2 si' |
    mi''1 |
    mi''8 dod''[ re'' mi''] fad'' la'4 la'8 |
    la'1 |
    la'4 la'2 sold'4 |
    la' }
  { s1-\sug\f | s-\sug\p | s-\sug\cresc | s-\sug\f }
>> r4 r2 |
