\clef "vbas-dessus" r4 |
R1*8 |
r2 r4 mi''8 re'' |
dod''4 dod''2 si'4 |
\grace si'4 la'2 r4 la'8. la'16 |
re''2 fad''4. re''16[ si'] |
la'4 sold' r8 si' si' mi'' |
red''4. red''8 red''4. mi''8 |
\grace mi''4 re''!2 r4 re''8 re'' |
dod''[ mi''] re'' si' la'4. si'8 |
\grace si'4 dod''2 r8 dod'' si' dod'' |
re''4. re''8 red''4. red''8 |
mi''4( fad''4.)\fermata la''8[ sold''] fad'' |
\grace fad''4 mi'' dod''8 mi'' \grace mi''4 re'' dod''8[ si'] |
la'2 r4 dod''8 mi'' |
\grace mi''4 re'' re''8 dod'' si'[ dod''] re'' mi'' |
dod''2 r4 la'8 mi'' |
red''2 fad''4. la'8 |
sold'4 sold' r2 |
R1*2 |
r2 r4 mi''8 mi'' |
mi''[ do''] do'' do'' do''4. do''8 |
si'2 r8 si' si' mi'' |
\grace mi''4 re''2~ re''8 re'' mi'' fa'' |
si'2.( la'4) |
sold'2 r8 mi'' mi'' re'' |
do''!4. do''8 mi''4. sol''8 |
sol''[ fa''] fa''4. mi''8 re'' do'' |
si'4. la'8 sol'4. fa'8 |
mi'2 r8 sol' mi'' re'' |
do''4. do''8 do''4. dod''8 |
dod''[ re''] re''4. re''8 mi'' fa'' |
sol''1~ |
sol''2~ sol''8[ la''] fa''[ re''] |
do''2. si'4 |
do''2 r4 mi''8 mi'' |
dod''4 dod''8 dod'' dod''4. dod''8 |
re''2 r8 do''! do'' do'' |
si'2~ si'8 do'' si' la' |
sold'4 sold' r8 mi'' mi'' mi'' |
la''1~ |
la''4\fermata fa'' mi'' red'' |
mi''1~ |
mi''~ |
mi''4 mi''2\fermata mi''8 re'' |
dod''4 dod''2 si'4 |
la'2. la'8 la' |
re''2 \grace re''4 fad''4. re''16[ si'] |
sold'4 sold' r8 si' si' mi'' |
red''4. red''8 red''4. mi''8 |
\grace mi''4 re''!2 r4 re''8 re'' |
dod''[ mi''] re'' si' la'4. si'8 |
\grace si'4 dod''2 r8 dod'' si' dod'' |
re''4. re''8 red''4. red''8 |
mi''4( fad''4.)\fermata la''8[ sold''] fad'' |
mi''4 dod''8 mi'' \grace mi''4 re'' dod''8[ si'] |
la''2~ la''8 la''[ sold''] fad'' |
mi''2 fad''4 sold''8[ la''] |
la''4( dod''2) re''8[ si'] |
la'2 r |
R1*5 |
