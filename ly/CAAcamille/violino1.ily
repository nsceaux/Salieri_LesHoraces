\clef "treble" mi''8.\f re''16 |
\grace re''8 dod''4 dod''2 si'4 |
\grace si'8 la'2 r4 la'8 la' |
re''2 \acciaccatura re''8 fad''4. re''16 si' |
\grace la'4 sold'2 <mi' si' sold''>4. la''16 si'' |
\grace si''16 la''8 sold''16 fad'' mi''4 si''4. dod'''16 re''' |
dod'''8 la''4 la'' la''8 sold'' fad'' |
mi''2 fad''4. sold''16 la'' |
la''4 dod''4. mi''8 \grace mi''8 re'' dod''16 si' |
la'4 mi'8. fad'16 mi'4 mi''8.\p re''16 |
dod''4 dod''2 si'4 |
\grace si'4 la'2 r4 la' |
re''2 \grace re''8 fad''4. re''16 si' |
\grace la'4 sold'2~ sold'8 si' si'( mi'') |
red''8 red''4 red'' red'' mi''8 |
r re''!4\mf re'' re'' re''8\p |
dod''8( mi'' re'' si') la'4. si'8 |
\grace si'4 dod''2 r8 dod''(\cresc si' dod'') |
re''8 re''4 re'' red'' red''8 |
mi''4 fad''4.\f\fermata la''8(\p sold'' fad'') |
\grace fad''4 mi'' dod''8 mi'' \grace mi''4 re'' dod''8 si' |
\grace si'16 la'( sold' la' si' dod'' la' sold' la') mi'( la' sold' la' dod'' la' dod'' la') |
fad' la' re'' re'' fad' la' re'' re'' sold' si' mi'' mi'' sold' si' mi'' mi'' |
\grace si'16 la'( sold' la' si' dod'' la' sold' la') mi'( la' sold' la' dod'' la' sold' la') |
la'8\cresc red''4 fad''8 fad''4. la'8 |
sold'4.\f <<
  { sold''8 sold''8. si''16 si''8. sold''16 | mi''4 } \\
  { si'8 si'8. si'16 si'8. si'16 | si'4 }
>> mi'8. mi'16 mi'4 r |
mi''8\sf( do'') r do'' r do''( fa''\sf mi'') |
mi''( si') r si' r si'(\p mi'' re'') |
do''\sf mi'' r do'' r do''( fa''\sf mi'') |
mi'' si' r si' r si' si' mi'' |
mi''4 re''4. re''8( mi'' fa'') |
si'4 si'2 la'4 |
sold'2 r |
%%
<sol mi' do''!>4.\f do''8 mi''4. sol''8 |
\times 2/3 { sol''8 mi'' fa'' } fa''4. mi''8 re'' do'' |
si'4.\p la'8 sol'4. fa'8 |
mi'2~ mi'8 mi'' mi'' re'' |
do''( sol' mi' sol' do'' sol'' mi'' dod'') |
dod''8 re'' re''4. re''8( mi'' fa'') |
sol''8\cresc sol''4 sol'' sol'' sol''8 |
sol''2\f~ sol''8 la''(\p fa'' re'') |
do''4 do''2 si'4 |
do''8\sf do'' r do'' r sol'(\sf mi'' re'') |
dod'' dod'' r dod'' r dod''(\sf sib'' dod'') |
re'' re'' r re'' do''! do'' r do'' |
si'!4 si''2 si4 |
si8 si' r si'\cresc r si' r mi'' |
la''4\f la''2 la''4~ |
la''2.\fermata red'4\p |
mi'2 sold''4.\sf( la''16 si'') |
la''2 si''4.\sf do'''16 re''' |
do'''4\f si'' r\fermata mi''8.\p re''16 |
dod''4 dod''2 si'4 |
\grace si'4 la'2 r4 la' |
re''2 \grace re''8*2 fad''4. re''16 si' |
\grace la'4 sold'2~ sold'8 si' si' mi'' |
red'' red''4 red'' red'' red''8 |
r8 re''!4\mf re'' re'' re''8 |
dod'' mi'' re'' si' la'4. si'8 |
\grace si'4 dod''2 r8 dod''(\cresc si' dod'') |
re''8 re''4 re''8 red'' red''4 red''8 |
mi''4 fad''4.\f\fermata la''8(\p sold'' fad'') |
\grace fad''4 mi'' dod''8 mi'' \grace mi''4 re'' dod''8 si' |
la''8\f la''4 la'' la''8( sold'' fad'') |
mi''8\p mi''4 mi''8 fad''8-. fad''( sold'' la'') |
la''4\cresc dod''4. mi''8 \grace mi'' re'' dod''16 si' |
la'2\f <mi' si' sold''>4. la''16 si'' |
\grace si''8 la''8 sold''16 fad'' mi''4 si''4. dod'''16 re''' |
dod'''8 la''4 la'' la''8 sold'' fad'' |
mi''2 fad''4. sold''16 la'' |
la''4 dod''4. mi''8 \grace mi''8 re'' dod''16 si' |
la'4 r8. mi'16^\markup\italic serré mi'8. la'16 la'8. dod''16 |
