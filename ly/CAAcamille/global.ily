\tag #'all \key la \major \midiTempo#112
\tempo "Allegro maestoso"
\time 4/4 \partial 4 s4 s1*33 \bar "||"
\beginMark "Minore"
\tag #'all \key la \minor s1*19 \bar "||"
\beginMark "Majore"
\tag #'all \key la \major s1*19 s4
\tempo "Piu allegro" s2. \bar "|."
