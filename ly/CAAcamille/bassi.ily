\clef "bass" r4 |
la8\f la la la la la mi mi |
\rt#4 fad \rt#4 fad |
\rt#4 fad \rt#4 re |
\rt#4 mi \rt#4 re |
dod8 mi dod la, sold, sold sold sold |
la la, si, dod re fad mi re |
\rt#4 dod \rt#4 re |
\rt#4 mi \rt#4 mi |
la4 la, la, r |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    R1 | \clef "tenor"
    \grace s4 r4 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { dod' dod' }
      { la la }
    >> r |
    R1*2 |
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { la1 | si | }
      { fad | sold | }
      { s1 | s-\sug\mf | }
    >>
    la4-\sug\p fad'8 re' dod'4. sold8 |
    r4 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { dod'8. mi'16 dod'4 }
      { la8. dod'16 la4 }
    >> r4 |
    r4 fad4-\sug\cresc fa2 |
    mi4 re!2-\sug\f\fermata r4 |
    r4 la8(-\sug\p dod' mi'2) |
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { s8 mi' mi' mi' s mi' mi' mi' |
        s fad' s fad' s re' s re' |
        s mi' mi' mi' s mi' mi' mi' | }
      { s dod' dod' dod' s dod' dod' dod' |
        s re' s re' s si s si |
        s dod' dod' dod' s dod' dod' dod' | }
      { r8 s4. r8 s4. | r8 s r s r s r s | r s4. r8 s4. }
    >>
    fad'2-\sug\cresc red' |
    mi'4-\sug\f r8 mi mi4 mi |
    mi mi8. mi16 mi4 r |
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { do'1 | mi'~ | mi'2 do' | mi'1~ | mi'4 re'2 re'4 | }
      { la1 | si | do'2 la | si1~ | si | }
      { s1*2 | s1-\sug\p }
    >>
    fa'2 fad' |
    mi'2 r |
  }
  \tag #'basso {
    <>\p \rt#4 la8 \rt#4 mi |
    \rt#4 fad \rt#4 fad |
    \rt#4 fad \rt#4 re |
    \rt#4 mi \rt#4 mi |
    \rt#4 fad \rt#4 fad |
    <>\mf \rt#4 sold \rt#4 sold |
    la\p la fad re \rt#4 mi |
    la, la la la la la([\cresc sold! sol]) |
    \rt#4 fad8 \rt#4 fa |
    mi4 re!2\fermata\f re4\p |
    \rt#4 mi8 \rt#4 mi |
    \rt#4 la \rt#4 la |
    \rt#4 la \rt#4 la |
    \rt#4 la \rt#4 la |
    <>\cresc \rt#4 fad \rt#4 red |
    mi4.\!-\sug\f mi8 mi8. mi16 mi8. mi16 |
    mi4 mi8. mi16 mi4 r |
    la8\sf r do' r fa' r la\sf r |
    sold! r mi r sold r mi\p r |
    la\sf r do' r fa' r la\sf r |
    sold r mi r sold r mi r |
    fa fa fa fa \rt#4 fa |
    re2 red |
    mi2 r |
  }
>>
%%
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    R1*6 |
    mi8-\sug\cresc do mi do fa re fa re |
    mi2-\sug\f r |
    R1 |
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { do'2. mi'8 re' | dod'1 | re'2 do'! | si1 | si1 | }
      { mi2. sol4 | sol1 | la | la | sold | }
      { s1-\sug\p | s1*3 | s4 s2.-\sug\cresc | <>-\sug\f }
    >>
    la1~ | la-\sug\p\fermata |
    sold2 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { re'2 | do' si | do'4 si }
      { si2 | la sold | la4 sold }
      { s2-\sug\sf | s1 | s2-\sug\f }
    >> r2\fermata |
  }
  \tag #'basso {
    do!8\f do[ do do] \rt#4 do |
    \rt#4 re \rt#4 re |
    <>\p \rt#4 sol \rt#4 sol, |
    \rt#4 do \rt#4 do |
    \rt#4 mi \rt#4 mi |
    \rt#4 fa \rt#4 fa |
    mi8\cresc do mi do fa re fa re |
    <>\f \rt#4 mi mi8 fa\p fa fa |
    \rt#4 sol \rt#4 sol, |
    do\sf r mi r sol r do'\sf r |
    la, r sib r sol r mi\sf r |
    fa r la r mi r la r |
    red r fad r red r si, r |
    mi r sold\cresc r si r mi r |
    <>\f \rt#4 fa \rt#4 fa |
    <>\p\fermata \rt#4 fa \rt#4 fa |
     mi1\p~ |
     mi~ |
     mi2 r4\fermata r | \clef "bass"
  }
>>
%%
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    R1 |
    r4 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { la8. dod'16 la4 }
      { fad8. la16 fad4 }
      { s4-\sug\p }
    >> r4 |
    R1*2 |
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { la1 | si | }
      { fad1 | sold | }
      { s1 | s-\sug\mf | }
    >>
    r4 fad'8 re' dod'4. sold8 |
    r4 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { dod'8. mi'16 dod'4 }
      { la8. dod'16 la4 }
    >> r4 |
    R1 |
    r4 re2-\sug\f\fermata r4 |
    r la8-\sug\p( dod' mi'2) |
    \clef "bass"
  }
  \tag #'basso {
    <>\p \rt#4 la8 \rt#4 mi |
    \rt#4 fad \rt#4 fad |
    \rt#4 fad \rt#4 re |
    \rt#4 mi \rt#4 mi |
    \rt#4 fad \rt#4 fad |
    <>\mf \rt#4 sold \rt#4 sold |
    la8 la fad re \rt#4 mi |
    la, la la la la la(\cresc sold sol) |
    \rt#4 fad8 \rt#4 fa |
    mi4 re2\f\fermata re4\p |
    \rt#4 mi8 \rt#4 mi |
  }
>>
la,8\f la,([ si, dod]) re fad( mi re) |
<>\p \rt#4 dod8 \rt#4 re |
<>\cresc mi2 mi |
<>\!\f \rt#4 la,8 mi mi re re |
dod mi dod la, sold, sold sold sold |
la la, si, dod re fad mi re |
\rt#4 dod \rt#4 re |
\rt#4 mi \rt#4 mi |
la,4 <<
  \tag #'(fagotto1 fagotto2 fagotti) { r4 r2 | }
  \tag #'basso { r8. la16 la8. la16 la8. la16 | }
>>
