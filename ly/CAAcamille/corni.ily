\clef "treble" \transposition la
\twoVoices #'(corno1 corno2 corni) <<
  { sol''8. fa''16 |
    mi''4 mi''2 re''4 |
    \grace re''8 do''4 do''8. mi''16 do''4 }
  { mi''8. re''16 do''4 do''2 sol'4 |
    mi'4 mi'8. mi'16 mi'4 }
  { s4-\sug\f }
>> r4 |
R1 |
r2 \twoVoices #'(corno1 corno2 corni) <<
  { re''2 | do'' re'' | mi''8 }
  { sol'2 | do'' sol' | do''8 }
>> do''4 do''8 do'' do''4 do''8 |
do''1~ |
do''4 \twoVoices #'(corno1 corno2 corni) <<
  { mi''2 re''4 | do'' sol' sol' s | s mi''2 re''4 |
    \grace re''8*2 do''4 do''8. mi''16 do''4 }
  { do''2 sol'4 | mi' mi' mi' s |
    s do''2 sol'4 | mi'4 mi'8. mi'16 mi'4 }
  { s2. | s2. r4 | r s2.\p }
>> r4 |
R1*4 |
\twoVoices #'(corno1 corno2 corni) <<
  { do''4. fa''8 mi''4. re''8 |
    \grace re''4 mi'' mi''8. sol''16 mi''4 }
  { do''4. re''8 do''4. sol'8 |
    \grace sol'4 do'' do''8. mi''16 do''4 }
  { s4.-\sug\p }
>> r4 |
R1 |
r4 do''4.-\sug\f\fermata r8 r4 |
R1 |
do'1-\sug\p~ |
do'~ |
do' |
do''-\sug\cresc |
sol'4.-\sug\f sol'8 sol'8. sol'16 sol'8. sol'16 |
sol'4 sol'8. sol'16 sol'4 r |
R1*5 |
r4 re''2-\sug\p re''4 |
re''2 r |
R1*18 |
R1^\fermataMarkup |
r4 \twoVoices #'(corno1 corno2 corni) <<
  { mi''2 re''4 | re''8 do'' do''8. mi''16 do''4 }
  { do''2 sol'4 | mi' mi' mi' }
  { s2.\p }
>> r4 |
R1*5 |
\twoVoices #'(corno1 corno2 corni) <<
  { re''4 mi''8. sol''16 mi''4 }
  { sol'4 do''8. mi''16 do''4 }
>> r4 |
R1 |
r4 do''4.-\sug\f\fermata r8 r4 |
R1 |
do''8-\sug\f do''4 do''8 do''2 |
do''1-\sug\p |
\twoVoices #'(corno1 corno2 corni) <<
  { do''4 mi''2 re''4 | do''2 sol'4 re''8. re''16 | }
  { do''4 do''2 sol'4 | mi'2 sol'4 sol'8. sol'16 | }
  { s1-\sug\cresc | s-\sug\f }
>>
do''4 sol' sol' \twoVoices #'(corno1 corno2 corni) <<
  { re''8. re''16 | mi''4 }
  { sol'8. sol'16 | do''4 }
>> do''2 do''4~ |
do''1~ |
do''4 \twoVoices #'(corno1 corno2 corni) <<
  { mi''2 re''4 | do''4 }
  { do''2 sol'4 | mi'4 }
>> r r2 |
