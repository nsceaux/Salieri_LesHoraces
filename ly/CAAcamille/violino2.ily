\clef "treble" dod''8.\f si'16 |
la'( mi' dod' mi') la'( mi' dod' mi') la'( mi' dod' mi') sold'( mi' sold' mi') |
dod'( la dod' fad') la'( fad' dod' fad') la'( fad' dod' fad') la'( fad' dod' fad') |
la( re' fad' la') la'( fad' re' fad') la'( fad' re' fad') la'( fad' re' fad') |
si( mi' sold' mi') si( mi' sold' mi') si( mi' sold' mi') si( mi' sold' mi') |
la( dod' mi' la') la'( mi' dod' mi') mi'( sold' si' mi'') mi''( si' sold' mi') |
mi'8 dod'16 dod' re' re' mi' mi' fad' la' la' la' \rt#4 la' |
la'( mi' dod' la) la( dod' mi' la') la'( fad' re' la) la( re' fad' la') |
la'( mi' dod' mi') la'( mi' dod' mi') la'( mi' dod' mi') si( mi' sold' mi') |
dod'4 dod'8. re'16 dod'4 dod''8.-\sug\p si'16 |
la'16( mi' dod' mi') la'( mi' dod' mi') la'( mi' dod' mi') sold'( mi' sold' mi') |
dod'( la dod' fad') la'( fad' dod' fad') la'( fad' dod' fad') la'( fad' dod' fad') |
la( re' fad' la') la'( fad' re' fad') la'( fad' re' fad') la'( fad' re' fad') |
si( mi' sold' si') si'( sold' mi' sold') si'( sold' mi' sold') si'( sold' mi' sold') |
la'8 la4 la la la8 |
r si4-\sug\mf si si si8-\sug\p |
la la4 re'8 dod'( mi' dod' sold) |
la16 dod' mi' la' la' mi' dod' mi' la'8 mi'4-\sug\cresc mi'8 |
la8 la'4 la' la' la'8 |
sold'4 la'4.-\sug\f\fermata fad'8-\sug\p( mi' re') |
\grace re'4 dod' la'8 dod'' \grace dod''4 si' la'8 sold' |
la' mi' mi' mi' r mi' mi' mi' |
r fad' r fad' r re' r re' |
r mi' mi' mi' r mi' mi' mi' |
la'16-\sug\cresc( fad' la' la') la' fad' la' la' la' fad' la' la' la' fad' red' si |
si4.-\sug\f <<
  { sold''8 sold''8. sold''16 sold''8. si'16 | si'4 } \\
  { si'8 si'8. si'16 si'8. sold'16 | sold'4 }
>> mi'8. mi'16 mi'4 r |
mi'8(\sf do') r do' r do'( fa'\sf mi') |
mi'( si) r si r si\p( mi' re') |
do'\sf mi' r do' r do'( fa'\sf mi') |
mi' si r si r si si mi' |
la'1 |
fa'2 fad' |
si r |
%%
<< { mi'16( sol' mi' sol' mi' sol' mi' sol') } \\ sol2-\sug\f >>
do''16 sol' mi' sol' do'' sol' mi'' do'' |
si'( do'' re'' do'' si' do'' re'' do'') si'8 fa'4 fa'8 |
re'2(\p si) |
do'16( mi' sol' mi' sol' mi' sol' mi') sol'( mi' sol' mi' sol' mi' sol' mi') |
sol' do'' sol' mi' do' mi' sol' mi' do' mi' sol' mi' do'' sol' mi' sol' |
la'( fa' re' la la re' fa' la') la'8 la'4 la'8 |
sol'-\sug\cresc mi' sol' mi' si'16 do'' re'' do'' si' do'' re'' si' |
do''8-\sug\f do''4 do'' la'-\sug\p fa'8 |
mi'16 sol' mi' sol' do'' sol' mi' sol' do'' sol' mi' sol' re' mi' fa' re' |
<sol mi'>8 q4 q q sol'8 |
sol' sol' r sol' r sib'4 sib'8 |
la' la' r la' la' la' r la' |
la' la'4 la' la' la'8 |
sold' sold' r sold'-\sug\cresc r sold' r si' |
la'-\sug\f fa'! do' la la la la la |
la1\fermata\p |
sold2 re''\sf |
do''( si') |
do''4 si' r\fermata mi'8.\p sold'16 |
la'( mi' dod' mi') la' mi' dod' mi' la' mi' dod' mi' sold' mi' sold' mi' |
dod' la dod' fad' la' fad' dod' fad' la' fad' dod' fad' la' fad' dod' fad' |
la re' fad' la' la' fad' re' fad' la' fad' re' fad' la' fad' re' fad' |
si mi' sold' si' si' sold' mi' sold' si' sold' mi' sold' si' sold' mi' sold' |
la'8 la4 la la la8 |
r8 si4-\sug\mf si si si8 |
la la4 re'8 dod' mi' dod' sold |
la16( dod' mi' la' la' mi' dod' mi') la'8 mi'4-\sug\cresc mi'8 |
la8 la'4 la' do''8 si' la' |
sold'4 la'4.-\sug\f\fermata fad'8-\sug\p( mi' re') |
\grace re'4 dod' la'8 dod'' \grace dod''4 si' la'8 sold' |
la'-\sug\f dod'16[ dod'] re' re' mi' mi' fad' la' la' la' \rt#4 la' |
la'-\sug\p mi' dod' la la dod' mi' la' la' fad' re' la la re' fad' la' |
la'-\sug\cresc mi' dod' mi' la' mi' dod' mi' la' mi' dod' mi' si mi' sold' mi' |
la'-\sug\f mi' dod' mi' la dod' mi' dod' si mi' sold' mi' si mi' sold' mi' |
la dod' mi' la' la' mi' dod' mi' mi' sold' si' mi'' mi'' si' sold' mi' |
mi'8 dod'16 dod' re' re' mi' mi' fad' la' la' la' \rt#4 la' |
la' la' sold' la' si' la' sold' la' re'' la' sold' la' si' la' sold' la' |
la' mi' dod' mi' la' mi' dod' mi' la' mi' dod' mi' si mi' sold' mi' |
dod'4 r8. dod'16 dod'8. <dod' mi'>16 q8. mi'16 |

    