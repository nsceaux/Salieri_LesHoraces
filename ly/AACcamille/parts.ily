\piecePartSpecs
#`((corno1 #:instrument "Corno I en do"
           #:system-count 3)
   (corno2 #:instrument "Corno II en do"
           #:system-count 3)
   (clarinetto1 #:notes "clarinetto1")
   (clarinetto2 #:notes "clarinetto2")
   (fagotto1 #:notes "fagotti")
   (fagotto2 #:notes "fagotti")
   (violino1 #:notes "violino1")
   (violino2 #:notes "violino2")
   (alto)
   (basso #:instrument , #{ \markup\center-column { Basso Contrabasso } #})
   (silence #:on-the-fly-markup , #{ \markup\tacet#54 #}))
