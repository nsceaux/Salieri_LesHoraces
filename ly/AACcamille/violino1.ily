\clef "treble" << <mi'' si'>4 \\ sol' >> r8. sol''16\f mi''8. si'16 |
si'4( do''8 re'' mi'' do'')\p |
si'4( la'8 si' do''\cresc dod'') |
re''( mi'' fa'' sol'' \tuplet 3/2 { la'' fa'' re'')\! } |
do''4( si') mi''8.\p re''16 |
re''4( do''8) sol'[ sol' sol'] |
la'8 la'4 la'8 la' la' |
la'4( sol'8.) sol''16\f mi''8. si'16 |
si'4( do''8 re'' mi'' do''\p) |
si'4( la'8 si' do''\cresc dod'') |
re''( mi'' fa'' sol'') \tuplet 3/2 { la''8( fa'' re'')\! } |
do''4( si'8.) <re' si>16[\ff q8. q16] |
<mib' do'>4.. <fad' la>16[ q8. q16] |
<sol' si>4 r8 si'\p si' si' |
si'( la') la' la' la' la' |
la' la'4 la'8 la' la' |
la'4 r8 r16 la16\ff[ la8. la16] |
sib4 r8 r16 dod'16[ dod'8. dod'16] |
re' re''-.\p[ la'-. re''-.] r16 re''-. la'-. re''-. r re''-. la'-. re''-. |
r dod'' dod'' dod'' r dod'' dod'' dod'' r dod'' dod'' dod'' |
r do''! do'' do'' r do'' do'' do'' r do'' do'' do'' |
r do'' do'' do'' r do'' do'' do'' r do'' do'' do'' |
si'4 r8. <re' si>16\f[ <do' mi'>8. q16] |
<re' si>4 r8. q16[ <re' fa'!>8. <re' fa'>16] |
<mi' sol>8 do''([ mi'' re'' do'' si']) |
<< la'2. \\ fa'\fp >> |
sol'8 sib''\f([ la'' sol'' fa'' la'']) |
do''2\p re''4 |
do''16( sol' mi' sol') do''( sol' mi' sol') do''(\cresc sol' do'' re'') |
mi''( do'' mi'' fa'') sol''8\!\f sol''4 sib'8\p |
la'4 r8. do'16[\f re'8. re'16] |
do'4 r8. <fa' la'>16 <sol' sib'>8. q16 |
<fa' la'>8 do''\p[ do'' do'' do'' do''] |
\rt#4 re''8 re''8 mi'' |
fa'' la([\f do' fa' la' do'']) |
fa''2\f re''8.\p do''16 |
si'!8 re'' r re''( mib''8. do''16) |
si'8\f sol'([ lab' si' re'' si']) |
do''2\p do''8.\p mib''16 |
re''8 re''4\cresc re''8[ fa''8. sol''16]\! |
lab''8\f lab''4 lab'' si'!8\p |
do''16 sol''([ fa'' mib''] re'' do'' si' do'' mib'' do'' mib'' do'') |
r16 sol''( fa'' mib'' re'' do'' si' do'' mib'' do'' mib'' do'') |
r16 do'''( sib'' lab'' sol'' fa'' mi''! fa'' fa'' re'' fa'' re'') |
r16 sol''( mib'' do'') r sol''( mib'' do'') r re''( si'! sol') |
sol'\p do'' mib'' mib'' <>\cresc \rt#4 mib''16 fa''( fa'' sol'' lab'') |
sol''16\!\f do'''8 do''' do''' do'''16 si''16 si''8 si''16 |
do'''8\ff do'''4 do'''8 mib''' do''' |
do'''2( si''4) |
re'''4. si''8( sol'' fa'') |
mib''8 fa''16 re'' do''4 re''8. re''16 |
mib''4. mib''8 fa'' sol''16 lab'' |
sol''4\f~ sol''8. sol''16 <si' re'>8. q16 |
<do'' mib' sol>8. do'16 do'4 r |
