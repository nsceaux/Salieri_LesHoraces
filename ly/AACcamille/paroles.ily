Pour Albe, hé -- las ! __ quels vœux me sont per -- mis ?
quels vœux, quels vœux me sont per -- mis ?
Je ne puis sé -- pa -- rer __ sa cau -- se de la nô -- tre :
Mon cœur, en -- tre les deux, flotte en -- core in -- dé -- cis,
Mes frè -- res sont pour l’u -- ne & mon a -- mant pour l’au -- tre ;
D’un & d’au -- tre cô -- té je ne vois que mal -- heurs ;
N’es -- pé -- rez pas que j’y sur -- vi -- ve.
Ah ! je de -- vrai, quoi qu’il ar -- ri -- ve,
Mes lar -- mes aux vain -- cus & ma haine aux vain -- queurs,
Mes lar -- mes aux vain -- cus & ma haine aux vain -- queurs,
& ma haine aux vain -- queurs.