\clef "bass" mi4 r r8. re16\f |
re4( do2) |
fa4\p fa, r |
fa4.\f( mi8 re8. fa16) |
sol4 sol, r |
r do\p do |
do do do |
do2 r8. re16\f |
re4( do2) |
fa4-\sug\p fa, r |
fa4. mi8\cresc re8 fa\! |
sol4 sol, r |
r8. sol,16\ff sol,2 |
r8. sol,16\ff sol,4. sol,8\p |
do4 do do |
do do do |
re4 r r |
r8. re16\ff re4~ re8. re16 |
re8\p r re r re r |
mi r mi r mi r |
fad r fad r fad r |
re r re r re r |
sol4 r r |
r8. sol,16\ff sol,4~ sol,8. sol16 |
do4 r r |
R2. |
r8 sol\f( fa mi re fa) |
<>-\sug\p \rt#4 sol8 sol, sol, |
<< \rt#6 do8 { s2 s4\cresc } >> |
<< \rt#6 do8 { s4 s\! s\p } >> |
fa4 r sib,8.\f re16 |
fa8. fa,16 fa8. fa16 mi8. do16 |
fa4\p fa fa |
fa fa fa |
fa r r |
re2\f( fa4)\p |
sol sol fad |
sol2( fa!4)\sf |
mib8\p mib mib mib mib mib |
<< \rt#6 fa8 { s8 s\cresc s2 <>\! } >> |
<>\f \rt#6 fa8 |
<>\p \rt#6 mib8 |
\rt#6 mib |
\rt#6 fa8 |
\rt#6 sol |
do8[\p do'] do'[\cresc sib] lab[ fa]\! |
<>\f \rt#4 sol sol, sol, |
<>\ff \rt#6 do8 |
\rt#6 re |
\rt#6 si,! |
do8 fa sol sol sol, sol, |
do8 do'4 sib8 lab[ fa] |
<>\f \rt#4 sol8 sol,8 sol, |
do8. do16 do4 r |
