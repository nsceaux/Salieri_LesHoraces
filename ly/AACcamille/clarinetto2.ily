\clef "treble"
r4 r8. sol'16-\sug\f sol'8. fa'16 |
fa'4 mi' do'-\sug\p~ |
do'2 r4 |
la'4.-\sug\f( sol'8 fa'8. la'16) |
mi'4( re') sol'8.-\sug\p fa'16 |
fa'4( mi'8) mi'[ mi' mi'] |
fa'8 fa'4 fa'8 fa' fa' |
fa'4( mi'8) r8 r8. fa'16-\sug\f |
fa'4( mi') do'-\sug\p~ |
do'2 r4 |
la'4. sol'8 fa'8. la'16 |
mi'4 re' r |
R2.*4 |
r4 r8. fad'16-\sug\ff[ fad'8. fad'16] |
sol'4~ sol'8.[ sol'16 sol'8. sol'16] |
fad'2.-\sug\p |
sol' |
la'~ |
la' |
sol'4 r r |
R2.*2 |
r8 la''-\sug\mf( sol'' fa'' mi'' re'') |
dod''8 r r4 r |
do''!2-\sug\p si'4 |
mi'2~ mi'8.-\sug\cresc re'16 |
do'8 do''16 re'' mi''8.\! fa''16 sol''4-\sug\p |
fa''4 r r |
R2. |
r8 la'4-\sug\p la'8 la' la' |
sib'2~ sib'8 sol' |
fa'4 r r |
la'2-\sug\f la'4-\sug\p |
re' si' la' |
re' r r |
do''2.-\sug\p |
<< do''2. { s8 s-\sug\cresc s2 <>\! } >> |
si'2.-\sug\f |
do''-\sug\p |
do'' |
lab' |
do''2 si'!4 |
<< do''2 { s4-\sug\p s-\sug\cresc } >> do''4 |
mib''8\!-\sug\f mib''4 mib''8 re''[ re''] |
mib''2.-\sug\ff |
fa'' |
fa'' |
mib''8 re'' do''4 si' |
do''2. |
mib''2 re''4 |
do''4 r r |
