\clef "soprano/treble" R2.*4 |
r4 r8 sol' mi''8. re''16 |
re''4( do'') r8 do'' |
la'4. si'8 do'' si'16[ la'] |
\grace la'4 sol'2 r4 |
r4 r r8 do'' |
si'4( la') r8 dod'' |
re''4. mi''8 fa''8. re''16 |
\grace do''4 si'2 r4 |
R2. |
r4 r8 si'16 do'' re''8 do''16 si' |
\grace si'4 la'2~ la'8. do''16 |
mi''4. do''8 la'8. sol'16 |
fad'8 fad' r4 r |
R2. |
r4 r r8 re'' |
dod''2 dod''8 dod''16 re'' |
do''!2. |
do''8 la' re'4 re''8. do''16 |
si'4 r r |
R2. |
r8 do'' mi'' re'' do'' si' |
la'4 la' r |
r8 dod'' re'' mi'' fa'' re'' |
do''!2( re''4) |
do''4 r8 sol'16 sol' do''8 do''16 re'' |
mi''8 mi''16 fa'' sol''4 sib'8. sib'16 |
la'2 r4 |
R2. |
r4 r8 do'' do'' fa'' |
re''4. fa''8 re'' sib' |
la'4 la' r |
fa''2 re''8. do''16 |
si'!4. re''8 mib''8. do''16 |
si'8 sol' r4 r8 sol' |
do''4. do''8 do'' mib'' |
re''2 fa''8. sol''16 |
lab''2 si'!8. si'16 |
do''2 r8 do'' |
sol''4. sol''8 mib'' do'' |
lab'2 fa''8. re''16 |
do''2 re''8. re''16 |
mib''2 fa''8 lab'' |
sol''2 si'8. si'16 |
do''2 r4 |
R2.*6 |
