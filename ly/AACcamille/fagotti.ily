\clef "bass" mi4 r r8. re16-\sug\f |
re4 do2 |
fa4-\sug\p fa, r |
fa4.-\sug\f( mi8 re8. fa16) |
sol4 sol, r |
do2.\p~ |
do~ |
do2 r8. re16-\sug\f |
re4 do2 |
fa4-\sug\p fa, r |
fa4. mi8 re8.[ fa16] |
sol4 sol, r4 |
r8. sol,16-\sug\ff sol,2 |
r8. sol,16 sol,2 |
R2.*2 |
r4 r8. <>-\sug\ff
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { la16[ la8. la16] | sib4 s8. dod'16[ dod'8. dod'16] | re'2. | }
  { fad16[ fad8. fad16] | sol4 s8. sol16[ sol8. sol16] | fad2. | }
  { s16 s4 | s4 r8. s16 s4 | <>-\sug\p }
>>
\clef "tenor" mi'2. |
re' |
fad |
sol4 r r |
\clef "bass" r8. sol,16-\sug\ff sol,2 |
R2. |
\clef "tenor" fa'!2.-\sug\fp |
mi'8 dod'-\sug\f[ re' mi' fa' re'] |
sol'2.-\sug\p |
do'2.~ |
do' |
do'4 r8. \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { do'16[ re'8. re'16] |
    do'8 s16 fa fa4 mi8. do16 |
    fa8 do'4 do'8 do' do' |
    re'2~ re'8 mi' |
    fa'4 }
  { la16[ sib8. sib16] |
    la8 s16 fa fa4 mi8. do16 |
    fa8 la4 la8 la la |
    sib2. |
    la4 }
  { s16-\sug\f s4 | s8 r16 s s2 | s8 <>-\sug\p }
>> r4 r |
\clef "bass" re2-\sug\f( fa4-\sug\p) |
sol2( fad4) |
sol4 r r |
\clef "tenor"
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sol'2. | lab' | lab' | sol'~ | sol' | }
  { mib'2. | fa' | fa' | mib'~ | mib' | }
  { s2.-\sug\p | s8 s-\sug\cresc s2 | s2.\!-\sug\f | s2.-\sug\p }
>>
fa'2. |
sol' |
\clef "bass" do8[\p do'] do'[\cresc sib] lab[ fa] |
<>\!\f \rt#4 sol8 sol,8 sol, |
do'2.-\sug\ff |
re' |
si! |
do'8 fa sol2 |
do8 do'4 sib8 lab[ fa] |
\rt#4 sol8 sol, sol, |
do8. do16 do4 r |
