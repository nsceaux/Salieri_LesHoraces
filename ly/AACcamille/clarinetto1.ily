\clef "treble"
r4 r8. sol''16\f mi''8. si'16 |
si'4( do''8 re'' mi'' do'')\p |
si'4( la'8 si' do''\cresc dod'') |
re''8( mi'' fa'' sol'' \tuplet 3/2 { la''8 fa'' re'')\! } |
do''4( si') mi''8.\p re''16 |
re''4( do''8) sol'[ sol' sol'] |
la'8 la'4 la'8 la' la' |
la'4( sol'8.) sol''16\f mi''8. si'16 |
si'4( do''8 re'' mi'' do''\p) |
si'4( la'8 si' do''\cresc dod'') |
re''( mi'' fa'' sol'') \tuplet 3/2 { la''8( fa'' re'')\! } |
do''4 si' r |
R2.*4 |
r4 r8. la'16-\sug\ff[ la'8. la'16] |
sib'4~ sib'8.[ dod''16 dod''8. dod''16] |
re''2.-\sug\p |
dod'' |
do''!~ |
do'' |
si'4 r r |
R2. |
r8 do''-\sug\f( mi'' re'' do'' si') |
la' r r4 r |
r8 sib''-\sug\f( la'' sol'' fa'' la'') |
mi''2-\sug\p re''4 |
do''2~ do''8.-\sug\cresc re''16 |
mi''8. fa''16 sol''8.\! la''16 sib''4-\sug\p |
la'' r r |
R2. |
r8 do''4-\sug\p do''8 do'' do'' |
re''2~ re''8 mi'' |
fa''4 r r |
fa''2-\sug\f re''8.-\sug\p do''16 |
si'!8 re'' re''4 mib''8. do''16 |
si'4 r r |
do''2.-\sug\p |
<< re''2. { s8 s-\sug\cresc s2 <>\! } >> |
re''2.-\sug\f |
do''-\sug\p |
sol''~ |
sol''4 fa''2 |
mib'' re''4 |
<< mib''2 { s4-\sug\p s-\sug\cresc } >> fa''8 lab'' |
sol''\!-\sug\f do'''4 do'''8 si''[ si''] |
do'''2.-\sug\ff |
lab'' |
sol'' |
sol''8 fa'' mib''4 re'' |
mib''2 fa''8 sol''16 lab'' |
sol''2 si''4 |
do''' r r |
