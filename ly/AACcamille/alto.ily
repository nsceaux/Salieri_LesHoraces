\clef "alto" mi'4 r r8. re'16-\sug\f |
re'4( do'2) |
fa'4-\sug\p fa r |
fa'4.-\sug\f( mi'8 re'8. fa'16) |
sol'4 sol r |
r8 do'-\sug\p do' do' do' do' |
do' do'4 do'8 do' do' |
do'2 r8. re'16-\sug\f |
re'4( do'2) |
fa'4-\sug\p fa r |
fa'4. mi'8 re'8. fa'16 |
sol'4 sol r4 |
r8. sol16-\sug\ff sol2 |
r8. sol16 sol8 re'-\sug\p[ re' re'] |
mi' mi'4 mi' mi'8 |
mi' mi'4 mi'8 mi' mi' |
re'4 r8 r16 fad16-\sug\ff[ fad8. fad16] |
sol4 r8 r16 sol[ sol8. sol16] |
fad16 re'[\p re' re'] r re' re' re' r re' re' re' |
r mi' mi' mi' r mi' mi' mi' r mi' mi' mi' |
r fad' fad' fad' r fad' fad' fad' r fad' fad' fad' |
r re' re' re' r re' re' re' r re' re' re' |
re'4 r r |
r8. sol16-\sug\ff sol4~ sol8. sol16 |
do'4 r r |
fa'2.-\sug\fp |
mi'8 sol-\sug\f[ fa mi re fa] |
<>-\sug\p \rt#6 sol8 |
do'16( mi' do' mi') do'( mi' do' mi') do'(-\sug\cresc mi' do' sol) |
do'( sol do' re') mi'\!( sol' mi' sol' mi'-\sug\p sol' mi' sol') |
fa'4 r8. fa16-\sug\f[ fa8. fa16] |
fa4 r8. do'16 do'8. do'16 |
do'8 fa'\p[ fa' fa' fa' fa'] |
\rt#6 fa' |
fa'4 r r |
re'2-\sug\f( fa'4-\sug\p) |
sol'4 sol fad |
sol r lab'-\sug\sf |
sol'8-\sug\p sol'4 mib'8 do'[ do''] |
do'' do''4-\sug\cresc do'' do''8 |
si'\!-\sug\f si'4 si' re''8-\sug\p |
sol' sol'4 sol' sol'8 |
sol' sol'4 sol' sol'8 |
lab'8 lab'4 lab' lab'8 |
sol' sol'4 sol' sol'8 |
<< \rt#4 sol'8 { s4-\sug\p s-\sug\cresc } >> lab'8 fa' |
sol'16\!-\sug\f sol'8 sol' sol' sol' sol' sol'16 |
sol'8-\sug\ff sol'4 sol' sol'8 |
fa'8 fa'~ fa'16( mi'! fa' sol' lab' fa' lab' fa') |
re'8 re'4 re' re'8 |
sol8 fa'16 lab' sol'8 sol4 sol8 |
do8[ do'] do'[ sib] lab[ fa] |
sol16 sol'8 sol' sol' sol' sol' sol'16 |
sol'8. do16 do4 r |
