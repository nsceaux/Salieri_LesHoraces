\clef "treble"
<< <mi'' si'>4 \\ sol' >> r r8. fa'16-\sug\f |
fa'4 mi' do'-\sug\p~ |
do'2 r4 |
la'4.-\sug\f( sol'8 fa'8. la'16) |
mi'4( re') sol'8.-\sug\p fa'16 |
fa'4( mi'8) mi'[ mi' mi'] |
fa'8 fa'4 fa'8 fa' fa' |
fa'4( mi'8) r8 r8. fa'16-\sug\f |
fa'4( mi') do'-\sug\p~ |
do'2 r4 |
la'4. sol'8 fa'8. la'16 |
mi'4( re'8.) <re' si>16[\ff q8. q16] |
<do' mib'>4.. do'16[ do'8. do'16] |
si4 r8 sol'-\sug\p sol' sol'
\rt#6 sol'8 |
sol' sol'4 sol'8 sol' sol' |
fad'4 r8 r16 la\ff[ la8. la16] |
sib4 r8 r16 sib[ sib8. sib16] |
la fad'\p[ fad' fad'] r fad' fad' fad' r fad' fad' fad' |
r sol' sol' sol' r sol' sol' sol' r sol' sol' sol' |
r <la' re'> q q r q q q r q q q |
r <fad' la'> q q r q q q r q q q |
si'4 r8. <re' si>16\f[ <do' mi'>8. q16] |
<re' si>4 r8. q16[ <re' fa'!>8. <re' fa'>16] |
do'4 r r |
r8 la''(\mf sol'' fa'' mi'' re'') |
dod'' dod'-\sug\f[ re' mi' fa' re'] |
mi'-\sug\p( sol' mi' sol' fa' sol') |
mi'16( sol' mi' sol') mi'( sol' mi' sol') mi'-\sug\cresc( sol' mi' si) |
do'( sol do' re') mi'\!( sol' mi' sol' mi'-\sug\p sol' mi' sol') |
fa'4 r8. la16-\sug\f[ sib8. sib16] |
la4 r8. <fa' la'>16 <sol' sib'>8. q16 |
<fa' la'>8 la'\p[ la' la' la' la'] |
\rt#6 sib'8 |
la'8 la([-\sug\f do' fa' la' do'']) |
<< la'2 \\ fa'-\sug\f >> la'4-\sug\p |
re'8( si!) r si' la'4 |
re'8\f sol'([ lab' si' re'' si']) |
do''-\sug\p( sol' mib' sol' sol' sol') |
<< \rt#6 lab'8 { s8 s-\sug\cresc s2 <>\! } >> |
re'8-\sug\f fa' lab' fa' re' re'-\sug\p |
do'16 sol'([ fa' mib'] re' do' si do' mib' do' mib' do') |
r16 sol'( fa' mib' re' do' si do' mib' do' mib' do') |
r16 do''( sib' lab' sol' fa' mi'! fa' lab' fa' lab' fa') |
r mib'( sol' mib') r mib'( sol' mib') r si!( re' si) |
mib'-\sug\p sol'( do'' do'') <>-\sug\cresc \rt#4 do''16 \rt#4 do'' |
mib''16\!-\sug\f mib''8 mib'' mib'' mib''16 re''16 re''8 re''16 |
mib''8-\sug\ff <mib' sol>4 mib'' mib''8 |
fa'' fa''~ fa''16 mi''!( fa'' sol'' lab'' fa'' lab'' fa'') |
re''8 sol'4 sol'8( si' re'') |
do'' lab'16 fa' mib' sol' mib' sol' si! re' si re' |
do' mib' sol' do'' \rt#4 do'' \rt#4 do'' |
<do'' mib'>16[ q8 q q q16] <si' re'>16[ q8 q16] |
<do'' mib'>8. do'16 do'4 r |
