\clef "tenor/G_8" la2. re'4 |
re'2 si4. la8 |
sol2. sol4 |
fad2 r |
R1*20 |
r2 r4 fad' |
mi'1~ |
mi'4 mi' mi' mi' |
mi'2 mi' |
mi'4. dod'8 sold4 la |
fad2 fad'4. fad'8 |
fad'2 fad'4. fad'8 |
si2 sold4 sold |
la2 mi'4. dod'8 |
si2 r |
R1*15 |
mi'2. mi'4 |
dod'( mi') dod' la |
sold( si) mi' mi' |
dod'2 la4.^\f la8 |
<< la1~ { s2 s^\p} >> |
la2 la |
la la4 fad |
mi2\melisma mi'~ |
mi'1\melismaEnd |
mi'2 la4.^\f la8 |
la2 la |
la la4 fad |
mi2\melisma mi'~ |
mi'~ mi'\melismaEnd |
dod' r\fermata |
la2.^\p re'4 |
re'2 si4. la8 |
sol2. sol4 |
fad2 r |
R1*5 |
r2 r4 re' |
re'2. re'4 |
do'4 si do' la |
si2 si |
la mi' |
re'1~ |
re'~ |
re'4 re' re' si |
la2 la4. la8 |
la2 r |
R1*7 |
R1^\fermataMarkup |
R1 |
r2 r4 si^\p |
la1 |
R1*2 |
r2 r4 la^\p |
re'2.\fermata la4 |
re'2. re'4 |
dod'( si) la( sol) |
fad2 fad |
R1 |
fad'1^\f~ |
fad'~ |
fad'4 re' re' si |
la2 la4. la8 |
fad2 r |
R1*6 |
r2 r4 la |
re'2. re'4 |
dod' si la sol |
fad2 fad |
R1 |
fad'1^\f~ |
fad'~ |
fad'4 re' re' si |
la2 la4. la8 |
fad1^\p |
si2 si |
si1 |
la2. la4 |
fad'1^\ff |
re'2 re' |
re'1 |
dod'2. dod'4 |
re'2 r |
R1*20 |
