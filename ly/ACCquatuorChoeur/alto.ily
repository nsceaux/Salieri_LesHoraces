\clef "alto" la2.-\sug\p re'4 |
re'2 si4. la8 |
sol4 sol'2 sol'4 |
fad'2 r |
R1*4 |
<la fad'>1 |
<la sol'>1~ |
q |
<< { sol'4 fad' fad'2~ | fad'1 | sol'~ | sol' | sol'4 fad'~ fad'4 } \\
  { la2 la~ | la1 | la~ | la | la2 la4 }
>> r4 |
R1*7 |
r2 r4 la8\f si16 dod' |
re'4 la fad re |
mi8 mi' mi' mi' <>-\sug\p \rt#4 mi' |
\rt#4 mi' \rt#4 mi' |
\rt#4 mi' \rt#4 mi' |
mi'4 la-\sug\f( si dod') |
<>-\sug\p \rt#4 re'8 \rt#4 re' |
\rt#4 re' \rt#4 re' |
<< \rt#4 mi' { s4 s-\sug\cresc } >> \rt#4 mi'8 |
\rt#4 mi' \rt#4 mi' |
mi'4\!-\sug\f mi8. mi16 mi4 mi |
mi2~ mi4 r |
R1*7 |
dod'8( mi' dod' mi' dod' mi' dod' mi') |
re'( mi' re' mi' re' mi' re' mi') |
dod' mi' dod' mi' dod' mi' dod' mi' |
si mi' si mi' si mi' si mi' |
la( dod' mi' dod') la( dod' mi' dod') |
fad'2 re' |
mi'2 r |
r sold4.-\sug\p sold8 |
la1 |
si4. la16 si sold4 sold |
la8-.-\sug\f la-. si-. dod'-. re'-. mi'-. fad'-. sold'-. |
la' sold' fad' mi' re'-\sug\p dod' si la |
\rt#4 re' \rt#4 re' |
\rt#4 re' \rt#4 re' |
\rt#4 mi' \rt#4 mi' |
\rt#4 mi' \rt#4 mi' |
dod'8 mi' dod'-\sug\cresc mi' dod' mi' dod' mi' |
re' fad' la' fad' re' fad' la' fad' |
re' fad' la' fad' re' fad' si' si'\! |
<>-\sug\f \rt#4 mi' \rt#4 mi' |
\rt#4 mi' \rt#4 mi' |
la2 r\fermata |
la2.-\sug\p re'4 |
re'2 si~ |
si2. la4 |
la2 r |
R1*3 |
r2 r4 la8\f si16 dod' |
re'2. re'4 |
do'4 si do' << { la'4 | si' } \\ { fad' | sol' } >>
re'4 si sol |
la sol la fad |
\rt#4 sol8 \rt#4 sol |
\rt#4 la \rt#4 la |
\grace { la16[ fad'] } re''1~ |
re''4 re''2 re''4~ |
re'' fad'( sol' sol') |
\rt#4 la8 \rt#4 la |
re1 |
<re' fad'>1 |
<mi' sol'>1 |
<dod' mi'>1 |
<re' fad'>1~ |
q |
<mi' sol'>1 |
<la mi'> |
<si re'>\fermata |
mi1\p |
si |
la2 sol'\<~ |
sol'4( fad'\! mi' re' |
dod'1)~ |
dod'-\sug\p |
re'2.\fermata la4\p |
si2. si4 |
la( sol fad mi) |
re re'2 re'4 |
dod'( si la sol) |
fad-\sug\f re fad la |
re' la fad la |
re re sol sol |
\rt#4 la'8 \rt#4 la |
re'2. red'4 |
mi'2. fad'8 re' |
dod'4 la'( sold' fad') |
mi'2. re'4 |
dod'2 r |
R1*2 |
r2 r4 la-\sug\f |
si2. si4 |
la sol fad mi |
re-\sug\f re'2 re'4 |
dod' si la sol |
fad re fad la |
re' la fad la |
re re' sol sol |
la la la la |
re'\p dod' si la |
si la sol fad |
sol mi fad sol |
la si dod' la |
re'-.\ff dod'-. si-. la-. |
si la sol fad |
sol mi fad sol |
la mi dod la |
re'8-\sug\ff <<
  { fad'8 fad' fad' \rt#4 fad' |
    \rt#4 fad' \rt#4 fad' |
    \rt#4 sol' \rt#4 sol' |
    \rt#4 mi' \rt#4 mi' |
    \rt#4 fad' \rt#4 fad' |
    \rt#4 fad' \rt#4 fad' | } \\
  { la8 la la \rt#4 la |
    \rt#4 la \rt#4 la |
    \rt#4 la \rt#4 la |
    \rt#4 la \rt#4 la |
    \rt#4 la \rt#4 la |
    \rt#4 la \rt#4 la | }
>>
sol'4 sol si re' |
sol' si' re'' si' |
sol'8 re'' dod'' re'' dod'' si' la' sol' |
\rt#4 la'8 \rt#4 la |
re''4-.\p dod''-. si'-. la'-. |
si'-. la'-. sol'-. fad'-. |
sol'-. mi'-. fad'-. sol'-. |
la'-. si'-. dod''-. la'-. |
re''-.\ff dod''-. si'-. la'-. |
si' la' sol' fad' |
sol' mi' fad' sol' |
la' mi' dod' la |
re'2 r4 r8 re' |
re'2 re' |
re'1 |
