\clef "treble" fad'2.\p fad'4 |
sol'2 si'~ |
si'2. la'4 |
la'2 r |
R1*3 |
r2 r4 fad''8 sol'' |
la''2.( fad''8 re'') |
re'' dod'' dod''2 dod''8 re'' |
mi''4( dod'' la' mi'') |
mi'' fad'' re'' fad''8 sol'' |
la''2.( fad''8 re'') |
re''( dod'' dod''2 dod''8 re'' |
mi''4 dod'' la' mi'') |
mi''( fad'' re'') r |
R1*7 |
r2 r4 la'8\f si'16 dod'' |
re''4 la' fad' re' |
mi'8 <<
  { si'8 si' si' \rt#4 si' | \rt#4 si' \rt#4 si' |
    \rt#4 dod'' \rt#4 dod'' | dod''4 } \\
  { sold'8 sold' sold' <>-\sug\p \rt#4 sold' | \rt#4 sold' \rt#4 sold' |
    \rt#4 la' \rt#4 la' | la'4 }
>> dod''4-\sug\f( sold' la') |
<>-\sug\p fad'1~ |
fad'8 fad' fad' fad' fad' re' fad' re' |
si si' si'-\sug\cresc si' sold' si' mi'' si' |
la' dod'' mi'' dod'' la' dod'' mi'' dod''\! |
si'4-\sug\f mi'8. mi'16 mi'4 mi' |
mi'2~ mi'4 r |
R1*7 |
dod'8( mi' dod' mi' dod' mi' dod' mi') |
re'( mi' re' mi' re' mi' re' mi') |
dod' mi' dod' mi' dod' mi' dod' mi' |
si mi' si mi' si mi' si mi' |
la( dod' mi' dod') la( dod' mi' dod') |
la'2 fad' |
si2 r | <>\fp
<mi'' mi'>1~ |
q~ |
q~ |
q4 <mi' dod'>2-\sug\f q4~ |
q q2 q4\p |
re'8 re'[ fad' la'] re'' la' fad' la' |
re'' la' fad' la' re'' la' fad' si' |
la' mi' la' mi' la' mi' la' mi' |
la' mi' la' mi' sold' mi' re' mi' |
dod' mi' dod'-\sug\cresc mi' dod' mi' dod' mi' |
fad' la' re'' la' fad' la' re'' la' |
fad' la' re'' la' re''( la' fad' si')\! |
mi'8-\sug\f <dod'' la''> q q \rt#4 q |
\rt#4 q \rt#4 <si' sold''> |
<la'' la'>2 r\fermata |
fad'1-\sug\p |
sol'2 si'4. la'8 |
sol'2. sol'4 |
fad'2 r |
R1*3 |
r2 r4 la'8\f si'16 dod'' |
\rt#4 <re' re''>8 \rt#4 q |
\rt#4 q \rt#4 q |
\rt#4 q \rt#4 q |
\rt#4 q \rt#4 q |
re''4 re''( si' sol') |
mi'8 mi' la' mi' dod'! mi' dod' dod' |
\grace { re'16[ fad'] } la'1~ |
la'4 la'2 la'4 |
la' la'( si' sol') |
fad'2 mi' |
re'2. fad''8 sol'' |
la''2. fad''8 re'' |
re''8 dod'' dod''2 dod''8 re'' |
mi''4( dod'' la' mi'') |
mi'' fad'' re'' fad''8 sol'' |
la''2. fad''8 re'' |
re''8 dod'' dod''2 dod''8 re'' |
mi''4( dod'' la' dod') |
re'1\fermata |
mi'1\p |
sold' |
<< { la'1~ | la'2 } { s2 s\< s4 s\!} >> sol'4 fad' |
<mi' dod'>1~ |
q-\sug\p |
re'2.\fermata r4 |
R1 |
r2 r4 la'-\sug\p |
fad''2. fad''4 |
mi''( re'' dod'' si') |
la'2 r |
<la' fad''>1-\sug\f~ |
q4 la' si'4. sol'8 |
fad' la' fad' la' mi' la' mi' mi' |
re' fad' fad' fad' fad' fad' fad' fad' |
sold\p si mi' si sold si mi' si |
la dod' mi' dod' la dod' mi' dod' |
sold si mi' si sold si mi' sold |
la2 r |
R1*4 |
r2 r4 la' |
<re' la' fad''>2.\f fad''4 |
mi''8 fad'' re'' mi'' dod'' re'' si' dod'' |
la'2 r |
<< { fad''4 fad''2 fad''4~ | fad''4 }
  \\ { la'4 la'2 la'4~ | la'4 }
>> la'4 si'8 sol' si' sol' |
fad' la' fad' la' mi' la' mi' la' |
re''4-.\p dod''-. si'-. la'-. |
si'8 dod'' la' si' sol' la' fad' la' |
sol'4 mi' fad' sol' |
la' \grace dod''8 si' la'16 si' dod''4 la' |
<<
  { s8 re'''[ re''' re'''] \rt#4 re''' |
    \rt#4 re''' \rt#4 re''' |
    \rt#4 re''' \rt#4 re''' |
    \rt#4 dod''' \rt#4 dod''' | } \\
  { fad''8\ff fad'' fad'' fad'' \rt#4 fad'' |
    \rt#8 fad'' |
    \rt#8 mi'' |
    \rt#8 mi'' | }
>>
re'''8\ff <<
  { la'8[ la' la'] \rt#4 la' |
    \rt#4 la' \rt#4 la' |
    \rt#4 la' \rt#4 la' |
    \rt#4 la' \rt#4 la' |
    \rt#4 la' \rt#4 la' |
    \rt#4 la' \rt#4 la' |
    si'8 sol''4 sol'' sol'' sol''8~ |
    sol'' sol''4 sol'' sol'' sol''8~ |
    sol'' sol''4 sol''
  } \\
  { fad'8[ fad' fad'] \rt#4 fad' |
    \rt#4 fad' \rt#4 fad' |
    \rt#4 sol' \rt#4 sol' |
    \rt#4 sol' \rt#4 sol' |
    \rt#4 fad' \rt#4 fad' |
    \rt#4 fad' \rt#4 fad' |
    sol'8 si'4 si' si' si'8 |
    si'1 |
    si'8 si'4 si'
  }
>> re''4 re''8 |
re'' la' re'' la' dod'' la' dod'' la' |
re''4-.\p dod''-. si'-. la'-. |
si'8-. dod''-. la'-. si'-. sol'-. la'-. fad'-. la'-. |
sol'4-. mi'-. fad'-. sol'-. |
la'4 \grace dod''8 si' la'16 si' dod''4 la' |
<<
  { s8 re''' re''' re''' \rt#4 re''' |
    \rt#4 re''' \rt#4 re''' |
    \rt#4 re''' \rt#4 re''' |
    \rt#4 dod''' \rt#4 dod''' |
    re'''2 } \\
  { fad''8\ff fad'' fad'' fad'' \rt#4 fad'' |
    \rt#4 fad'' \rt#4 fad'' |
    \rt#4 mi'' \rt#4 mi'' |
    \rt#4 mi'' \rt#4 mi'' |
    re''2 }
>> r4 r8 re' |
re'2 re' |
re'1 |
