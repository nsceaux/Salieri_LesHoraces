\clef "tenor" <>-\sug\p
<<
  \tag #'(fagotto1 fagotti) \new Voice {
    \tag #'fagotti \voiceOne
    la2. re'4~ | re'2 si4. la8 | sol1 | fad2
  }
  \tag #'(fagotto2 fagotti) \new Voice {
    \tag #'fagotti \voiceTwo
    fad1 | sol2 sol4. fad8 | mi1 | re2
  }
>> r4 fad' |
fad' sol'2 sol'4 |
fad' fad' mi' re' |
dod'2. re'4 |
la2 r |
R1 |
r2 r4 dod'8 re' |
mi'4( dod' la mi') |
mi'( fad' re'2) |
R1 |
r2 r4 dod'8 re' |
mi'4 dod' la mi' |
mi' fad' re' <<
  \tag #'(fagotto1 fagotti) \new Voice {
    \tag #'fagotti \voiceOne
    re'4~ | re'4 dod' si2~ | si4 la sol si |
  }
  \tag #'(fagotto2 fagotti) \new Voice {
    \tag #'fagotti \voiceTwo
    fad4 | sol1~ | sol4 fad mi sol |
  }
>>
la2 la, |
re~ re4 r |
r4 re2( mi8 fad) |
sol2. sol4 |
la2 la, |
re2 r4 la8-\sug\f si16 dod' |
re'4 la fad re |
mi4
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { re'2 re'4~ | re'1 | dod' | }
  { si2 si4~ | si1 | la | }
  { s2 <>-\sug\p }
>>
r4 la,-\sug\f si, dod |
re1-\sug\p~ |
re4 re'2 re'4 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { re'1 | dod' | si4 }
  { si1 | la | sold4 }
  { <>-\sug\cresc s1*2 | <>\!-\sug\f }
>> mi8. mi16 mi4 mi |
mi2~ mi4 r | <>\p
<<
  \tag #'(fagotto1 fagotti) \new Voice {
    \tag #'fagotti \voiceOne
    dod'2 la4. si8 | dod'2 re'4 dod' | dod'( si) si2 | r4 mi' mi' mi' |
    mi'2 mi'4. re'8 | dod'1 | re2. mi4 | la1 |
    si4 sold fad mi | la2. si8 dod' | si2 si4 si | dod'2. mi'4 |
    re' dod' si la | sold2 mi |
  }
  \tag #'(fagotto2 fagotti) \new Voice {
    \tag #'fagotti \voiceTwo
    la1~ | la | si, | sold |
    la | la | re2. mi4 | dod1 |
    re | dod | mi | mi |
    la2 fad | mi mi |
  }
>>
r2 sold-\sug\p |
la1 |
si2 sold |
la1-\sug\f~ |
<< la1~ { s2 <>-\sug\p } >> |
la4 re' re' re' |
re'1 |
mi'~ |
mi'~ |
mi'4 dod'-\sug\cresc( la) la4~ |
la re' re' re' |
re'1 |
mi'1\!-\sug\f~ |
mi' |
la2 r\fermata |
<>-\sug\p \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { la2. re'4 | re'2 si4. la8 | sol2. sol4 | fad2. }
  { fad1 | sol2 sol4. fad8 | mi2. mi4 | re2. }
>> fad'4 |
fad' sol'2 sol'4 |
fad' fad' mi' re' |
dod'2. re'4 |
la2 r4 la,8\f si,16 dod |
re2. re'4 |
do' si do' la |
si re' si sol |
la sol la fad |
sol sol, sol si |
la1~ |
la4 re fad la |
re' la fad la |
re fad( sol sol) |
la2 la, |
re r |
R1 |
r2 r4 dod'8 re' |
mi'4( dod' la mi') |
mi' fad' re'2 |
R1 |
r2 r4 dod'8 re' |
mi'4( dod' la sol) |
fad1\fermata |
mi1-\sug\p~ |
mi |
la,2 sol'\< ~ |
sol'4 fad'\! mi' re' |
dod'1~ |
dod'-\sug\p |
re'2.\fermata la4-\sug\p |
si2. si4 |
la( sol fad mi) |
re
<<
  \tag #'(fagotto1 fagotti) \new Voice {
    \tag #'fagotti \voiceOne
    fad'2 fad'4 | mi'( re' dod' si) | la2
  }
  \tag #'(fagotto2 fagotti) \new Voice {
    \tag #'fagotti \voiceTwo
    re'2 re'4 | dod'( si la sol) | fad2
  }
>> r2 |
la1-\sug\f~ |
la4 re re re |
la, la2 la4 |
si1 |
R1*3 |
r4 la( si dod') |
re' <<
  \tag #'(fagotto1 fagotti) \new Voice {
    \tag #'fagotti \voiceOne
    re'2 re'4 | si si2 si4 | la2
  }
  \tag #'(fagotto2 fagotti) \new Voice {
    \tag #'fagotti \voiceTwo
    re4( mi fad) | sol sol( fad mi) | la2
  }
>> la,2 |
si,2. si4 |
la sol fad mi |
re-\sug\f re'2 re'4 |
dod' si la sol |
fad re fad la |
re1~ |
re4 re sol, sol, |
la,2 la |
re'4-\sug\p <<
  \tag #'(fagotto1 fagotti) \new Voice {
    \tag #'fagotti \voiceOne
    fad'2 fad'4~ |
    fad' fad'2 fad'4 |
    mi' mi'2 mi'4~ |
    mi' mi'2 mi'4 |
    
  }
  \tag #'(fagotto2 fagotti) \new Voice {
    \tag #'fagotti \voiceTwo
    re'2 re'4~ |
    re' re'2 re'4 |
    re' re'2 re'4 |
    dod' dod'2 dod'4 |
  }
>>
\clef "bass" re'4-.\ff dod'-. si-. la-. |
si la sol fad |
sol mi fad sol |
la mi dod la, |
re1~ |
re4 re'2 re'4 |
mi'1 |
dod' |
re'~ |
re' |
sol4 sol, si, re |
sol si re' si |
sol1 |
la |
\clef "tenor" r4 <<
  { <>-\sug\p s2. s1*3 <>-\sug\ff }
  \tag #'(fagotto1 fagotti) \new Voice {
    \tag #'fagotti \voiceOne
    fad'2 fad'4~ |
    fad' fad'2 fad'4 |
    mi' mi'2 mi'4 |
    mi' mi'2 mi'4 |
    fad' fad'2 fad'4~ |
    fad' fad'2 fad'4 |
    mi' mi'2 mi'4 |
    mi' mi'2 mi'4 |
    re'2
  }
  \tag #'(fagotto2 fagotti) \new Voice {
    \tag #'fagotti \voiceTwo
    re'2 re'4 |
    re' re'2 re'4 |
    re' re'2 re'4 |
    dod' dod'2 dod'4 |
    re' re'2 re'4~ |
    re' re'2 re'4 |
    re' re'2 re'4 |
    dod' dod'2 dod'4 |
    re'2
  }
>> r4 r8 re |
re2 re |
re1 |
