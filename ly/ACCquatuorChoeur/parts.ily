\piecePartSpecs
#`((corno1 #:tag-global () #:instrument "Corno I en ré")
   (corno2 #:tag-global () #:instrument "Corno II en ré")
   (oboe1 #:notes "oboe1")
   (oboe2 #:notes "oboe2")
   (clarinetto1 #:notes "oboe1")
   (clarinetto2 #:notes "oboe2")
   (fagotto1 #:notes "fagotti" #:clef "tenor")
   (fagotto2 #:notes "fagotti" #:clef "tenor")
   (violino1 #:notes "violino1")
   (violino2 #:notes "violino2")
   (alto)
   (basso #:instrument "Basso")
   (silence #:on-the-fly-markup , #{ \markup\tacet#152 #}))
