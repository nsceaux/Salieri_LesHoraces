\clef "bass" re1\p |
sol2 sol4. fad8 |
mi2. la4 |
re2 r |
R1*4 |
re1 |
mi |
dod |
re~ |
re |
mi |
dod |
re2~ re4 r |
R1*7 |
r2 r4 la8\f si16 dod' |
re'4 la fad re |
\rt#4 mi8 <>\p \rt#4 mi |
\rt#4 mi \rt#4 mi |
\rt#4 la \rt#4 la |
la4 la,\f( si, dod) |
<>\p \rt#4 re8 \rt#4 re |
\rt#4 re \rt#4 re |
<< \rt#4 mi { s4 s\cresc } >> \rt#4 mi8 |
\rt#4 mi \rt#4 mi |
mi4\f\! mi8. mi16 mi4 mi |
mi2~ mi4 r |
R1*7 |
la,1~ |
la,~ |
la, |
sold, |
la, |
fad2 re |
mi2 r |
r sold\p |
la2. la4 |
si2 sold4 sold |
la4\f la la la |
la4 la la,\p la, |
re re re re |
re re re re |
mi mi mi mi |
mi mi mi mi |
\rt#4 la8 <>\cresc \rt#4 la, |
\rt#4 re \rt#4 re |
\rt#4 re \rt#4 re |
<>\f \rt#4 mi \rt#4 mi |
\rt#4 mi \rt#4 mi |
la2 r\fermata |
re1\p |
sol2 sol4. fad8 |
mi2. la4 |
re2 r |
R1*3 |
r2 r4 la8\ff si16 dod' |
re'2. re'4 |
do'4 si do' do\f |
si, re si, sol, |
la, sol, la, fad, |
sol,8 sol sol sol \rt#4 sol |
\rt#4 la \rt#4 la |
re4 re fad la |
re' la fad la |
re fad sol sol |
\rt#4 la8 \rt#4 la, |
re1 |
\clef "tenor" <>^"Violoncelle" re'1 |
mi' |
dod' |
re'~ |
re' |
mi' |
\clef "bass" la2 la, |
si,1\fermata |
<>^"Tutti" mi4\p mi mi mi |
mi mi mi mi |
la la, la,\< la, |
la, la,\! la, la, |
la, la, la, la, |
la,\p la, la, la, |
re1\fermata~ |
re~ |
re~ |
re4 re'2 re'4 |
dod'( si la sol) |
fad4\f re-! fad-! la-! |
re' la fad la |
re2 sol4. sol8 |
\rt#4 la \rt#4 la, |
si,1 |
sold\p |
la |
sold |
la2 r |
R1*2 |
r2 r4 la,\f |
si,4 si2 si4 |
la sol fad mi |
re\f re'2 re'4 |
dod' si la sol |
fad\ff re fad la |
re' la fad la |
re re sol sol |
la la la, la, |
re'-.\p dod'-. si-. la-. |
si la sol fad |
sol mi fad sol |
la si dod' la |
re'-.\ff dod'-. si-. la-. |
si la sol fad |
sol mi fad sol |
la mi dod la, |
<>\ff \rt#4 re8 \rt#4 re |
\rt#4 re \rt#4 re |
\rt#4 mi \rt#4 mi |
\rt#4 dod \rt#4 dod |
\rt#4 re \rt#4 re |
\rt#4 re \rt#4 re |
sol4 sol, si, re |
sol si re' si |
\rt#4 sol8 \rt#4 sol |
\rt#4 la \rt#4 la, |
re'4-.\p dod'-. si-. la-. |
si-. la-. sol-. fad-. |
sol-. mi-. fad-. sol-. |
la-. si-. dod'-. la-. |
re'-.\ff dod'-. si-. la-. |
si la sol fad |
sol mi fad sol |
la mi dod la, |
re2 r4 r8 re |
re2 re |
re1 |
