\clef "bass" re2. re4 |
sol2 sol4. fad8 |
mi2. la4 |
re2 r |
R1*10 |
r2 r4 dod' |
re'2 re4 r |
R1*6 |
r2 r4 la |
re'1~ |
re'~ |
re'2. si4 |
sold mi si re' |
dod'2 dod' |
mi'1~ |
mi'4 fad re' dod' |
si2 si4. la8 |
sold2 si4 re' |
dod'( mi') dod' la |
mi2 r |
R1*7 |
r2 r4 mi |
la2 la4. la8 |
si4( sold) fad mi |
la2 r |
r si4. si8 |
dod'2. mi'4 |
re'( dod') si la |
sold2 mi |
r sold |
la la4 la |
si2 si4 sold |
la2 r |
r2 la4 la |
re'2 re' |
re' re'4 re' |
mi'1~ |
mi'2\melisma mi\melismaEnd |
la la4 la |
re'2 re' |
re' re'4 re' |
mi'1~ |
mi' |
la2 r\fermata |
re2.^\p re4 |
sol2 sol4. fad8 |
mi2. la4 |
re2 r |
R1*3 |
r2 r4 la |
re'2. re'4 |
do' si do' la |
si2 sol |
la2. fad4 |
sol2 sol |
la la |
re'1~ |
re'~ |
re'4 fad sol sol |
la2 la4. la8 |
re2 r |
R1*6 |
r2 r4 dod' |
re'2 re\fermata |
R1 |
r2 r4 sold^\p |
la1 |
R1*2 |
r2 r4 dod'^\p |
re'2.\fermata \grace sol8 fad4 |
si2. si4 |
la( sol) fad( mi) |
re2 re |
R1*2 |
re'1^\f~ |
re'4 re' sol sol |
la2 la4. la8 |
re2 r |
R1*6 |
r2 r4 la |
si2. si4 |
la sol fad mi |
re2 re |
R1*2 |
re'1^\f~ |
re'4 re sol sol |
la2 la4. la8 |
re'1^\p |
si4( la) sol( fad) |
sol1 |
la2. la4 |
re'1^\ff |
si4( la) sol( fad) |
sol1 |
la2. la4 |
re2 r |
R1*20 |
