\clef "treble" R1*3 |
r2 r4 la' |
la'1~ |
la'~ |
la'4 mi''2 re''4 |
\grace re''8 dod''2 r |
R1*9 |
si'2. sol'4 |
fad' la'2 dod''4 |
dod''2( re''4) r |
r4 fad'2 sol'8 la' |
si'4 sol'( la' si') |
fad'2. sol'4 |
fad'1\f~ |
fad' |
sold'2. sold'4-\sug\p |
re''1 |
dod'' |
mi''-\sug\f~ |
mi''4 fad'-\sug\p re'' dod'' |
si' fad'2 fad''4 |
si'1-\sug\cresc |
dod'' |
si'4\!-\sug\f mi'8. mi'16 mi'4 mi' |
mi'2 r |
la'1\p~ |
la' |
fad' |
r4 si' si' si' |
la'2 la'4. si'8 |
dod''1 |
re''2. si'4 |
la'2 r |
R1*6 |
r2 si'-\sug\p |
dod''1 |
re''4.\trill dod''16 re'' si'4 si' |
dod''1-\sug\f~ |
<< dod''1 { s2 <>-\sug\p } >> |
re''1~ |
re''4 fad''( re'' si') |
la'1~ |
la'2 sold' |
la' dod''-\sug\cresc |
re''1~ |
re''4 fad''( re'' si') |
la'2\!-\sug\f la''~ |
la'' sold'' |
la'' r\fermata |
R1*3 |
r2 r4 la''-\sug\p |
la''1~ |
la''~ |
la''~ |
la''2 r |
R1 |
r2 r4 re''8-\sug\f mi''16 fad'' |
sol''2. si''4 |
fad''4 sol'' fad'' re''~ |
re'' re'' si' sol' |
la'2 r |
la'1~ |
la'~ |
la'2 si'4( sol') |
fad'2 mi' |
re' r |
R1*7 |
R1^\fermataMarkup |
R1*3 |
r2 dod''4 re'' |
mi''1~ |
mi''-\sug\p |
re''2.\fermata r4 |
R1*4 |
fad''1-\sug\f~ |
fad''~ |
fad''4 la' si' sol' |
fad'2 mi' |
fad' r |
R1*3 |
r4 dod''( re'' mi'') |
fad'' fad'( sol' la') |
si' si'( la' sol') |
fad'2. mi'4 |
re'2 r |
r r4 la' |
fad''2.-\sug\f fad''4 |
mi'' re'' dod'' si' |
la'1 |
fad''~ |
fad''4 la' si' sol' |
fad'2 mi' |
fad'4-\sug\p re''2 re''4~ |
re'' re''2 re''4 |
re'' re''2 re''4 |
dod'' dod''2 dod''4 |
re''1-\sug\ff |
re'' |
re'' |
dod'' |
re'' |
fad' |
sol' |
sol' |
fad'~ |
fad'2. fad''4 |
sol''8 re''4 re'' re'' re''8~ |
re'' re''4 re'' re'' re''8~ |
re''1 |
re''2 dod'' |
re''4-\sug\p re''2 re''4~ |
re'' re''2 re''4 |
re'' re''2 re''4 |
dod'' dod''2 dod''4 |
re''-\sug\ff re''2 re''4~ |
re'' re''2 re''4~ |
re'' re''2 re''4 |
dod'' dod''2 dod''4 |
re''2 r4 r8 re' |
re'2 re' |
re'1 |
