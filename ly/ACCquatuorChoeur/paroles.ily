O du sort trop heu -- reux re -- tour ?
\tag #'(camille curiace) {
  Que nous de -- vons de gra -- ces à l’a -- mour.
}
\tag #'camille { Cu -- ri -- a -- ce ! }
\tag #'curiace { Ca -- mil -- le ! }
\tag #'camille { O mon pè -- re ! }
\tag #'vhorace { Ma fil -- le ! }
\tag #'(camille curiace) {
  Que nous de -- vons de gra -- ces à l’a -- mour !
  Que nous de -- vons de gra -- ces à l’a -- mour !
}
\tag #'(vhorace jhorace) {
  O Ciel ! __ sur \tag #'vhorace { Rome & } ma fa -- mil -- le
  Ver -- se \tag #'jhorace { tes bien -- faits }
  tes bien -- faits en ce jour,
  tes bien -- faits en ce jour !
}
\tag #'(camille curiace) {
  Du sein des plus ru -- des a -- lar -- mes
  Ain -- si le bon -- heur naît pour nous.
  Oui le bon -- heur naît pour nous.
  Du sein des plus ru -- des a -- lar -- mes
  Du sein des plus ru -- des a -- lar -- mes.
}
\tag #'vhorace {
  Il doit en pa -- roî -- tre plus doux :
  Le mal -- heur lui prê -- te des char -- mes.
  Il doit en pa -- roî -- tre plus doux :
}
\tag #'jhorace {
  Oui il doit en pa -- roî -- tre plus doux :
}
\tag #'(vhorace jhorace) {
  \tag #'jhorace { Le mal -- heur __ }
  \tag #'vhorace { Le mal -- heur }
  lui prê -- te des char -- mes.
  Le mal -- heur lui prê -- te des char -- mes.
}

O du sort trop heu -- reux re -- tour !
\tag #'(camille curiace) {
  Que nous de -- vons de gra -- ces à l’a -- mour.
}
\tag #'vhorace {
  O Ciel sur Rome & ma fa -- mil -- le ma fa -- mil -- le
}
\tag #'curiace {
  O Ciel sur Ho -- race & sa fil -- le
}
\tag #'(camille jhorace) {
  O Ciel sur Rome & ma fa -- mil -- le
}
\tag #'(vsoprano valto vtenor vbasso) {
  O Ciel sur Rome & sa fa -- mil -- le
}
Ver -- se ver -- se tes bien -- faits en ce jour.

\tag #'camille { Cu -- ri -- a -- ce ! }
\tag #'curiace { Ca -- mil -- le ! }
\tag #'camille { O mon pè -- re ! }
\tag #'vhorace { Ma fil -- le ! }
O Ciel O Ciel
\tag #'(jhorace vhorace) { sur Rome & ma fa -- mil -- le }
\tag #'(vsoprano vbasso) { Sur Rome & sa fa -- mil -- le }
Ver -- se tes bien -- faits en ce jour.
\tag #'camille {
  Que nous de -- vons de gra -- ces à l’a -- mour.
}
\tag #'(camille curiace) {
  Que nous de -- vons de gra -- ces à l’a -- mour.
}
\tag #'(jhorace vhorace) {
  O Ciel sur Rome & ma fa -- mil -- le
}
\tag #'(vsoprano vbasso) { O Ciel sur Rome & sa fa -- mil -- le }
Ver -- se tes bien -- faits en ce jour,
tes bien -- faits en ce jour,
tes bien -- faits en ce jour.
