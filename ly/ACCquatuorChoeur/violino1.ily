\clef "treble" re''2.\p re''4 |
dod''( si' mi''4. fad''8) |
sol''2. dod''4 |
re''2 r |
R1*4 |
fad'8( la' fad' la' fad' la' fad' la') |
sol'( la' sol' la' sol' la' sol' la') |
mi'( sol' la' sol') mi'( sol' la' sol') |
sol' la' fad' la' fad' la' fad' la' |
fad'8 la' fad' la' fad' la' fad' la' |
sol' la' sol' la' sol' la' sol' la' |
mi' sol' la' sol' mi' sol' la' sol' |
sol' la' fad' la' fad'4 r |
R1*7 |
r2 r4 la'8\f si'16 dod'' |
re''4 la' fad' re' |
mi'4 re'''2 si''4\p |
sold'' mi'' si'' re''' |
dod''' la''(\f dod''' mi''') |
mi'''1~ |
mi'''4 fad''(\p re''' dod''') |
si''2 si''4. la''8 |
<< sold''2 { s4 s\cresc } >> si''4. re'''8 |
dod'''4 mi''' dod''' la''\! |
sold''4\f mi'8. mi'16 mi'4 mi' |
mi'2~ mi'4 r |
R1*6 |
r2 r4 mi' |
la' la'' la'' la'' |
si''( sold'' fad'' mi'') |
la''4 la''2 si''8 dod''' |
si'' r mi''4( si'') si'' |
dod'''2. mi'''4 |
re''' dod''' si'' la'' |
sold''4 mi'' mi'' mi'' |
mi''2\f si'4.\p si'8 |
la'( dod'' mi'' dod'' mi'' dod'' mi'' dod'') |
re''4.\prall dod''16 re'' si'4 si' |
dod''8-.\f la'-. si'-. dod''-. re''-. mi''-. fad''-. sold''-. |
la''-. sold''-. fad''-. mi''-. re''-.\p dod''-. si'-. la'-. |
fad''4 fad''2 fad''4~ |
fad''4 la''( fad'' re'') |
dod''2 dod''~ |
dod''2 si' |
la'8 dod'' mi''\cresc dod'' la'' mi'' dod'' la' |
<re' la' fad''>2 fad''4. fad''8 |
fad''4 la'' \grace sold''8 fad''4 \grace mi''8 re''4\! |
<>\f \rt#4 <dod'' la''>8 \rt#4 q |
\rt#4 q \rt#4 <si' sold''> |
<la'' la'>2 r\fermata |
re''2.\p re''4 |
dod''( si' mi''4.) fad''8 |
sol''2. dod''4 |
re''2 r |
R1*5 |
r2 r4 re''8\f mi''16 fad'' |
sol''2. si''4 |
do'''4 \grace do'''8 si'' la''16 si'' do'''4 la'' |
si'' si''( sol'' mi'') |
\grace re'' dod''!4.\prall si'16 dod'' la'4 la' |
\grace { re'16[ la'] } fad''1~ |
fad''4 fad''2 fad''4~ |
fad'' la''( sol'' mi'') |
re''2 dod''4.\prall si'16 dod'' |
re''8( la' fad' la' fad' la' fad' la') |
fad'( la' fad' la' fad' la' fad' la') |
sol' la' sol' la' sol' la' sol' la' |
mi' sol' la' sol' mi' sol' la' sol' |
fad' la' fad' la' fad' la' fad' la' |
fad' la' fad' la' fad' la' fad' la' |
sol' la' sol' la' sol' la' sol' la' |
mi' sol' la' sol' mi' sol' la' sol' |
fad'1\fermata |
mi'1\p |
\grace mi''4 re''( dod''8 re'' mi''4 re'') |
<< dod''1~ { s2 s\< } >> |
dod''4( re''\! mi'' fad'' |
sol''1) |
\grace la''8 sol''4\p( fad''8 sol'' la''4 sol'') |
fad''2.\fermata la'4-\sug\p |
re''2. re''4 |
dod''( si' la' sol') |
fad'2 r |
R1 |
<la' la''>1\f~ |
q~ |
q4 fad'' mi''8( fad'' sol'' mi'') |
re''2 dod''4.\trill si'16 dod'' |
re''2. red''4 |
mi''2. fad''8 re''! |
dod''4 la''( sold'' fad'') |
mi''2. re''4 |
dod''2 r |
R1*2 |
r2 r4 la'\f |
<si fad' re''>2. re''4 |
dod''8 re'' si' dod'' la' si' sol' la' |
fad'2 r |
R1 | <>\ff
<<
  { la''4 la''2 la''4~ | la'' la''2 la''4~ | la'' } \\
  { la'4 la'2 la'4~ | la' la'2 la'4~ | la' }
>> \grace sol''8 fad'' mi''16 fad'' sol''4 mi'' |
re''2 dod''4.\trill si'16 dod'' |
re''4-.\p dod''-. si'-. la'-. |
si'8 dod'' la' si' sol' la' fad' la' |
sol'4 mi' fad' sol' |
la' \grace dod''8 si' la'16 si' dod''4 la' |
<<
  { s8 re'''[ re''' re'''] \rt#4 re''' |
    \rt#4 re''' \rt#4 re''' |
    \rt#4 re''' \rt#4 re''' |
    \rt#4 dod''' \rt#4 dod''' | } \\
  { fad''8\ff fad'' fad'' fad'' \rt#4 fad'' |
    \rt#8 fad'' |
    \rt#8 mi'' |
    \rt#8 mi'' | }
>>
re'''4\ff re''2 fad''8 sol'' |
la''2.( fad''8 re'') |
re'' dod'' dod''2 dod''8( re'' |
mi''4 dod'' la' mi'') |
mi''( fad'' re'') fad''8 sol'' |
la''4( fad'' re'' do''') |
<<
  { si''8 si''4 si'' si'' si''8~ |
    si'' si''4 si'' si'' si''8 | } \\
  { re''8 re''4 re'' re'' re''8 |
    re''1 | }
>>
si''8 re''' dod''' re''' dod''' si'' la'' sol'' |
fad''2 \grace fad''4 mi'' re''8 mi'' |
re''4-.\p dod''-. si'-. la'-. |
si'8-. dod''-. la'-. si'-. sol'-. la'-. fad'-. la'-. |
sol'4-. mi'-. fad'-. sol'-. |
la'4 \grace dod''8 si' la'16 si' dod''4 la' |
<<
  { s8 re''' re''' re''' \rt#4 re''' |
    \rt#4 re''' \rt#4 re''' |
    \rt#4 re''' \rt#4 re''' |
    \rt#4 dod''' \rt#4 dod''' |
    re'''2 } \\
  { fad''8\ff fad'' fad'' fad'' \rt#4 fad'' |
    \rt#4 fad'' \rt#4 fad'' |
    \rt#4 mi'' \rt#4 mi'' |
    \rt#4 mi'' \rt#4 mi'' |
    re''2 }
>> r4 r8 re' |
re'2 re' |
re'1 |
