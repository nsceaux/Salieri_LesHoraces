\clef "alto/G_8" fad'2. fad'4 |
sol'2 si4. si8 |
si2. la4 |
la2 r4 fad' |
fad'4( sol'2) sol'4 |
fad' fad' mi' re' |
dod'2. re'4 |
dod'2 r |
R1*2 |
r2 r4 mi' |
mi'( fad') re'2 |
R1*3 |
r2 r4 re' |
re'( dod') si2 |
si4 la sol sol' |
fad'2. dod'4 |
re'2 r4 re' |
fad'2. sol'8[ la'] |
si'4 sol' la' si' |
fad'2. mi'4 |
re'2 r |
R1*10 |
r2 r4 dod' |
dod'2 la4 si |
dod'2 re'4 dod' |
dod'( si) si2 |
r si |
la2 la4. si8 |
dod'1 |
re'2. si4 |
dod'2 r |
R1*6 |
mi'2 mi'4 mi' |
mi'1 |
mi'2. mi'4 |
mi'2 r |
r2 dod' |
re' re'4 re' |
re'( fad') re' si |
la1~ |
la2\melisma sold\melismaEnd |
la dod' |
re' re'4 re' |
re'( fad') re' si |
la1~ |
la2\melisma sold\melismaEnd |
la r\fermata |
fad'2.^\p fad'4 |
sol'2 si4. si8 |
si2. la4 |
la2 r4 fad'4 |
fad'( sol'2) sol'4 |
fad' fad' mi' re' |
dod'2. re'4 |
dod'2 r |
R1 |
r2 r4 re'4 |
re'2 re'4 re' |
do'( si) do' la |
si2 si |
mi' dod'! |
la'1~ |
la'~ |
la'4 la si sol' |
fad'2 mi'4. mi'8 |
re'2 r |
R1*2 |
r2 r4 mi' |
mi'( fad') re' r |
R1*3 |
R1^\fermataMarkup |
R1 |
r2 r4 mi'^\p |
mi'1 |
R1*2 |
r2 r4 la'^\p |
la'2.\fermata r4 |
R1*4 |
fad'1^\f~ |
fad'~ |
fad'4 re' re' sol' |
fad'2 mi'4. mi'8 |
re'2 r |
R1*3 |
r2 r4 la |
re'2. re'4 |
si si' la' sol' |
fad'2. mi'4 |
re'2 r |
R1*3 |
fad'1^\f~ |
fad'~ |
fad'4 re' si sol' |
fad'2 mi'4. mi'8 |
fad'1^\f |
fad'2 fad' |
mi'1 |
mi'2. mi'4 |
re'1^\ff |
re'2 re' |
re'1 |
dod'2. dod'4 |
re'2 r |
R1*20 |
