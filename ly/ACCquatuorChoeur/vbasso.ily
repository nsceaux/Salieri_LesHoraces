\clef "bass" re2. re4 |
sol2 sol4. fad8 |
mi2. la4 |
re2 r |
R1*60 |
re2.^\p re4 |
sol2 sol4. fad8 |
mi2. la4 |
re2 r |
R1*5 |
r2 r4 re' |
sol2. sol4 |
la4 sol la fad |
sol2 sol |
la la |
re'1~ |
re'~ |
re'4 fad sol sol |
la2 la4. la8 |
re2 r |
R1*7 |
R1^\fermataMarkup |
R1 |
r2 r4 sold^\p |
la1 |
R1*2 |
r2 r4 dod'^\p |
re'2.\fermata r4 |
R1 |
r2 r4 la^\p |
re'2. re'4 |
dod'( si) la( sol) |
fad2 re |
re'1^\f~ |
re'4 re' sol sol |
la2 la4. la8 |
re'2 r |
R1*8 |
r2 r4 la |
re'2. re'4 |
dod'4 si la sol |
fad2 fad |
re'1^\f~ |
re'4 re sol sol |
la2 la4. la8 |
re'1^\p |
si4( la) sol( fad) |
sol1 |
la2. la4 |
re'1^\ff |
si4( la) sol( fad) |
sol1 |
la2. la4 |
re2 r |
R1*20 |
