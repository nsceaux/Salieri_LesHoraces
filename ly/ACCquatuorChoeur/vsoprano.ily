\clef "vbas-dessus" re''2. re''4 |
dod''4( si') mi''4. fad''8 |
sol''2. dod''4 |
re''2 r |
R1*60 |
re''2.^\p re''4 |
dod''( si') mi''4. fad''8 |
sol''2. dod''4 |
re''2 r |
R1*5 |
r2 r4 re'' |
sol''2. re''4 |
re'' re'' re'' re'' |
re''2 re'' |
dod''!2 la' |
fad''1~ |
fad''~ |
fad''4 la'' sol'' mi'' |
re''2 dod''4. dod''8 |
re''2 r |
R1*7 |
R1^\fermataMarkup |
R1 |
r2 r4 re''^\p |
dod''1 |
R1*2 |
r2 r4 mi''^\p |
fad''2.\fermata r4 |
R1 |
r2 r4 la'^\p |
fad''2. fad''4 |
mi''( re'') dod''( si') |
la'2 fad' |
fad''1^\f~ |
fad''4 fad'' sol'' mi'' |
re''2 dod''4. dod''8 |
re''2 r |
R1*8 |
r2 r4 la' |
fad''2. fad''4 |
mi'' re'' dod'' si' |
la'2 la' |
fad''1^\f~ |
fad''4 fad'' sol'' mi'' |
re''2 dod''4. dod''8 |
re''1^\p |
fad''2 fad'' |
mi''1 |
mi''2. mi''4 |
fad''1^\ff |
fad''2 fad'' |
mi''1 |
mi''2. mi''4 |
re''2 r |
R1*20 |
