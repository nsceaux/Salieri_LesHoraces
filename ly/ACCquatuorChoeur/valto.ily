\clef "vhaute-contre" fad'2. fad'4 |
sol'2 si4. si8 |
si2. la4 |
la2 r |
R1*60 |
fad'2.^\p fad'4 |
sol'2 si4. si8 |
si2. la4 |
la2 r |
R1*5 |
r2 r4 la' |
si'2. si'4 |
fad'4 sol' re' la' |
re'2 sol' |
mi' dod' |
la'1~ |
la'~ |
la'4 la si sol' |
fad'2 mi'4. mi'8 |
re'2 r |
R1*7 |
R1^\fermataMarkup |
R1 |
r2 r4 mi'^\p |
mi'1 |
R1*2 |
r2 r4 la'^\p |
la'2.\fermata r4 |
R1*4 |
la'1^\f~ |
la'~ |
la'4 la' si' sol' |
fad'2 mi'4. mi'8 |
re'2 r |
R1*11 |
la'1^\f~ |
la'~ |
la'4 la' si' sol' |
fad'2 mi'4. mi'8 |
fad'1^\p |
re'2 re' |
re'1 |
dod'2. dod'4 |
re'1^\ff |
si'2 si' |
la'1 |
la'2. la'4 |
re'2 r |
R1*20 |
