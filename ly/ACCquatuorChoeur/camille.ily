\clef "soprano/treble" re''2. re''4 |
dod''( si') mi''4. fad''8 |
sol''2. dod''4 |
re''2 r4 re'' |
re''4( dod''2) dod''4 |
re'' re'' mi'' fad'' |
sol''2. fad''4 |
mi''2 r |
r2 r4 fad''8 re'' |
re''[ dod''] dod''2 r4 |
R1*2 |
r2 fad''4 re'' |
re''( dod'') dod''2 |
R1 |
r2 r4 re'' |
si'( dod'') re''( red'') |
mi'' fad'' sol'' mi'' |
re''2. mi''4 |
fad''2 r4 re'' |
la''2. sol''8[ fad''] |
mi''4 mi'' fad'' sol'' |
re''2. dod''4 |
re''2 r |
R1*10 |
r2 r4 mi'' |
mi''2 dod''4 re'' |
mi''2 fad''4 mi'' |
mi''( red'') red''2 |
r re''! |
dod''2 dod''4. re''8 |
mi''1 |
fad''2. sold'4 |
la'2 r |
R1*6 |
mi''2 si'4 si' |
dod''1 |
re''2. si'4 |
dod''2 r |
r2 mi'' |
fad'' fad''4 fad'' |
fad''( la'') fad'' re'' |
dod''1~ |
dod''2\melisma si'\melismaEnd |
dod'' mi'' |
fad'' fad''4 fad'' |
fad''( la'') fad'' re'' |
dod''1~ |
dod''2\melisma si'\melismaEnd |
la' r\fermata |
re''2.^\p re''4 |
dod''( si') mi''4. fad''8 |
sol''2. dod''4 |
re''2 r4 re'' |
re''( dod''2) dod''4 |
re'' re'' mi'' fad'' |
sol''2. fad''4 |
dod''2 r |
R1 |
r2 r4 re''4^\f |
sol''2. re''4 |
re'' re'' re'' re'' |
re''2 re'' |
dod''!2 la' |
fad''1~ |
fad''~ |
fad''4 la'' sol'' mi'' |
re''2 dod''4. dod''8 |
re''2 r |
r r4 fad''8 re'' |
re''4 dod'' r2 |
R1*2 |
r2 fad''4 re'' |
re''4 dod'' r2 |
R1 |
R1^\fermataMarkup |
R1 |
r2 r4 re''^\p |
dod''1 |
R1*2 |
r2 r4 mi''4^\p |
fad''2.\fermata r4 |
R1*4 |
la''1^\f~ |
la''~ |
la''4 fad'' sol'' mi'' |
re''2 dod''4. dod''8 |
re''2. red''4 |
mi''2. fad''8[ re''!] |
dod''4 la'' sold'' fad'' |
mi''2. re''4 |
dod''2 r4 la' |
fad''2. fad''4 |
fad'' mi'' fad'' sol'' |
re''2. dod''4 |
re''2 r |
R1*3 |
la''1^\f~ |
la''~ |
la''4 fad'' sol'' mi'' |
re''2 dod''4. re''8 |
re''1^\p |
fad''2 fad'' |
mi''1 |
la''2. la''4 |
fad''1^\ff |
fad''2 fad'' |
mi''1 |
la''2. la''4 |
re''2 r |
R1*20 |
