\clef "vtaille" la2. re'4 |
re'2 si4. la8 |
sol2. sol4 |
fad2 r |
R1*60 |
la2.^\p re'4 |
re'2 si4. la8 |
sol2. sol4 |
fad2 r |
R1*5 |
r2 r4 re' |
re'2. re'4 |
do'4 si do' la |
si2 si |
la mi' |
re'1~ |
re'~ |
re'4 re' re' si |
la2 la4. la8 |
la2 r |
R1*7 |
R1^\fermataMarkup |
R1 |
r2 r4 si^\p |
la1 |
R1*2 |
r2 r4 la^\p |
re'2.\fermata r4 |
R1*4 |
fad'1^\f~ |
fad'~ |
fad'4 re' re' si |
la2 la4. la8 |
fad2 r |
R1*11 |
fad'1^\f~ |
fad'~ |
fad'4 re' re' si |
la2 la4. la8 |
fad1^\p |
la2 la |
la1 |
la2. la4 |
fad'1^\ff |
re'2 re' |
re'1 |
dod'2. dod'4 |
re'2 r |
R1*20 |
