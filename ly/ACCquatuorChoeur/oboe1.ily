\clef "treble" R1*3 |
r2 r4 re'' |
re''4( dod''2) dod''4 |
re'' re''( mi'' fad'') |
sol''2. fad''4 |
\grace fad''8 mi''2 r4 fad''8( sol'' |
la''2. fad''8 re'' |
re'' dod'' dod''2 dod''8 re'' |
mi''4 dod'' la' mi'')( |
mi'' fad'' re'' fad''8 sol'' |
la''2. fad''8 re'') |
re''8( dod'' dod''2 dod''8 re'' |
mi''4 dod'' la' mi'') |
mi''( fad'' re'') re'' |
si'( dod'' re'' red'') |
mi''( fad'' sol'' mi'') |
re''2. mi''4 |
mi''2( fad''4.) re''8 |
la''2.\fp sol''8 fad'' |
mi''4 mi''( fad'' sol'') |
re''2. dod''4 |
re''1-\sug\f~ |
re''~ |
re''4( re'''2) si''4-\sug\p |
sold''1 |
la''4 la'-\sug\f( dod'' mi'') |
mi''2 sold''4 la'' |
fad''1-\sug\p~ |
fad''4 si''2 la''4 |
sold''1-\sug\cresc |
la'' |
sold''4\!-\sug\f mi'8. mi'16 mi'4 mi' |
mi'2 r4 mi''\p |
mi''2 dod''4. re''8 |
mi''2 fad''4. mi''8 |
mi''4 red'' red''2 |
r4 re''! re'' re'' |
dod''2 dod''4. re''8 |
mi''4 la''( sold'' la'') |
fad''2. sold'4 |
la'2 r |
R1*6 |
mi''1-\sug\fp~ |
mi''~ |
mi'' |
mi''-\sug\f~ |
<< mi''1 { s2 <>-\sug\p } >> |
fad''1~ |
fad''4 la''( fad'' re'') |
dod''1~ |
dod''2 si' |
dod'' mi''-\sug\cresc |
\once\slurDashed fad''1~ |
fad''4 la''( fad'' re'') |
dod''2\!-\sug\f dod'''~ |
dod''' si'' |
la'' r\fermata |
R1*3 |
r2 r4 re''-\sug\p |
re'' dod''2 dod''4 |
re'' re'' mi'' fad'' |
sol''2. fad''4 |
mi''2 r |
R1 |
r2 r4 re''8-\sug\f mi''16 fad'' |
sol''2. si''4 |
do''' si'' do''' la'' |
si'' si''( sol'' mi'') |
dod''!2 la' |
fad''1~ |
fad''~ |
fad''4 la''( sol'' mi'') |
re''2 dod'' |
re''2. fad''8 sol'' |
la''2. fad''8 re'' |
re''8 dod'' dod''2 dod''8 re'' |
mi''4 dod'' la' mi'' |
mi''( fad'' re'') fad''8 sol'' |
la''2. fad''8 re'' |
re''8 dod'' dod''2 dod''8 re'' |
mi''4( dod'' la' sol') |
fad'1\fermata |
R1*2 |
r2 dod''-\sug\p\<~ |
dod''4 re''\! mi'' fad'' |
sol''1 |
sol''4-\sug\p fad''8 sol'' la''4 sol'' |
fad''2.\fermata r4 |
R1*4 |
la''1-\sug\f~ |
la''~ |
la''4 fad'' mi''8 fad'' sol'' mi'' |
re''2 dod'' |
re''2. red''4 |
mi''2. fad''8 re''! |
dod''4 la''( sold'' fad'') |
mi''2. re''4 |
dod''4 la''2 la''4 |
la'' fad''2 fad''4~ |
fad'' mi''( fad'' sol'') |
re''2. dod''4 |
re''2 r |
R1*3 |
la''1-\sug\f |
la''~ |
la''4 fad'' sol'' mi'' |
re''2 dod'' |
re''4-\sug\p fad''2 fad''4~ |
fad'' fad''2 fad''4 |
mi'' mi''2 mi''4~ |
mi'' la''2 la''4 |
fad''1-\sug\ff~ |
fad'' |
mi''~ |
mi'' |
re''2. fad''8 sol'' |
la''2.( fad''8 re'') |
re'' dod'' dod''2 dod''8( re'' |
mi''4 dod'' la' mi'') |
mi''( fad'' re'') fad''8 sol'' |
la''4( fad'' re'' do''') |
si''8 si''4 si'' si'' \once\tieDashed si''8~ |
si'' si''4 si'' si'' si''8 |
si''4 re''' \grace dod'''8 si''4 la''8 sol'' |
fad''2 mi'' |
re''4\p fad''2 fad''4~ |
fad'' fad''2 fad''4 |
mi'' mi''2 mi''4 |
la'' la''2 la''4 |
fad''-\sug\ff fad''2 \once\tieDashed fad''4~ |
fad'' fad''2 fad''4 |
mi'' mi''2 mi''4 |
la'' la''2 la''4 |
fad''2 r4 r8 re' |
re'2 re' |
re'1 |
