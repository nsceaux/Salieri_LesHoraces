\clef "treble"
<<
  \tag #'violino1 { fa'2 }
  \tag #'violino2 { <fa' la>2 }
>> r2 |
R1 |
<re' si'!>4 r r2 |
<<
  \tag #'violino1 { do''4 }
  \tag #'violino2 { sol' }
>> r4 r2 |
R1 |
<<
  \tag #'violino1 { mi''2\f }
  \tag #'violino2 { si'-\sug\f }
>> r2 |
r8. <<
  \tag #'violino1 { dod''16\f dod''4 }
  \tag #'violino2 { mi'16\f mi'4 }
>> r2 |
r2 <<
  \tag #'violino1 {
    sold'2\fp~ | sold'1~ | sold' | dod''~ | dod''4
  }
  \tag #'violino2 {
    red'2-\sug\fp~ | red'1 | mi' | sold' | la'4
  }
>> r4 r2 |
r2 <<
  \tag #'violino1 { dod''2\f | re''8. <re'' re'''>16 q8. q16 q4 }
  \tag #'violino2 { sol'2-\sug\f | fad'8. <la' fad''>16 q8. q16 q4 }
>> r4 |
r2 <<
  \tag #'violino1 { re''2 | si'4 }
  \tag #'violino2 { <re' la'>2 | <re' si'>4 }
>> r4 r2 |
<<
  \tag #'violino1 { <re' si'>4 r do'' r | }
  \tag #'violino2 { sol'4 r sol' r | }
>>
r4 r8 sol'16 la'32 si' do''8-. mi''-. sol''-. mi''-. |
do''4 r r2 |
r2 <fa' do'' la''>4 r |
r2 <fa' re'' sib''>4 r |
r2 r4 << <sib' mi''>4 \\ sol' >> |
<< <fa'' la'> \\ fa' >> r r2 |
