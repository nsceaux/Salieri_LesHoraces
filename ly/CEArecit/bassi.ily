\clef "bass" fa2 r |
R1 |
fa4 r r2 |
mi4 r r2 |
R1 |
sold2\f r |
r8. la16 la4 r2 |
r sid,\fp~ |
sid,1 |
dod |
mid |
fad4 r r2 |
r2 mi!2\f |
re8. re'16 re'8. re'16 re'4 r |
r2 fad |
sol4 r r2 |
fa!4 r mi r |
r4 r8 sol,16 la,32 si, do8-. mi-. sol-. mi-. |
do4 r r2 |
r fa4 r |
r2 sib,4 r |
r2 r4 do |
fa, r r2 |
