\clef "vbasse" r8 la la sib do' do' sib do' |
la4 r8 do'16 do' do'4 do'8 re' |
si!4^! r16 si si do' re'4. sol8 |
do'4^! r8 sol sol sol sol sol |
do'4 r8 do' do' do' re' mi' |
si8^! si r4 r2 |
R1*3 |
r2 r4 r8 sold16 sold |
dod'4 sold8 la! si4 si8 dod' |
la^! la r dod'16 dod' la4 si8 dod' |
fad4 r dod'8^! dod'16 dod' dod'8 re' |
la4^! r r2 |
r4 r8 la re'4 la8 la16 si |
sol8^! sol r sol16 sol sol4 sol8 la |
si8^! si16 si si8 la16 sol do'4^! r4 |
R1 |
r8 sol sol sol do'4 do'8 do' |
do'4 sib8 do' la4^! r8 do'16 do' |
do'4 re'8 mib' re'4^! r8 re'16 re' |
sib4 sib8 la fa fa^! r4 |
R1 |
