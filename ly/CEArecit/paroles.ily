\tag #'valere {
  Tan -- dis qu’un fils vic -- to -- ri -- eux
  D’un tri -- om -- phe si beau va ren -- dre grace aux Dieux,
  Souf -- frez qu’au nom du Roi, qu’au nom de Rome en -- tiè -- re.
}
\tag #'vhorace {
  Que pré -- ten -- dez- vous di -- re ? Ex -- pli -- quez- vous, Va -- lè -- re.
  Quoi ! lors -- qu’Albe à ses loix nous sou -- met au -- jour -- d’hui,
  Quand mon fils !…
}
\tag #'valere {
  De l’é -- tat c’est le Dieu tu -- té -- lai -- re !
  Il nous a sau -- vé tous, nous tri -- om -- phons par lui.
}
\tag #'vhorace {
  Eh, quoi ? sa fui -- te ?…
}
\tag #'valere {
  A fait no -- tre vic -- toi -- re.
  I -- gno -- rez- vous en -- cor la moi -- tié du com -- bat.
}
\tag #'camille {
  Je trem -- ble.
}
\tag #'valere {
  Ap -- pre -- nez donc le bon -- heur de l’é -- tat ;
  Et d’un fils im -- mor -- tel le cou -- rage et la gloi -- re.
}
