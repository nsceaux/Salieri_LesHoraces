\clef "alto" do'2 r |
R1 |
sol'4 r r2 |
sol'4 r r2 |
R1 |
sold'2-\sug\p r |
r8. la'16\f la'4 r2 |
r sid-\sug\fp~ |
sid1 |
dod'~ |
dod'~ |
dod'4 r r2 |
r2 mi'!\f |
re'8. re''16 re''8. re''16 re''4 r |
r2 fad'2 |
sol'4 r r2 |
fa'!4 r mi' r |
r4 r8 sol16 la32 si do'8-. mi'-. sol'-. mi'-. |
do'4 r r2 |
r fa'4 r |
r2 sib4 r |
r2 r4 do' |
fa r4 r2 |
