\clef "vbasse" R1*5 |
r4 r8 si si si sold mi |
la^! la r16 mi mi mi la4. dod'8 |
la la r4 sold4.^! sold8 |
sold4 sold8 lad sid sid16 sid sid8 lad16 sold |
dod'4^! r8 dod'16 dod' sold4 r |
R1*3 |
r2 r8 la fad r16 fad |
la8 la r4 r2 |
R1*8 |
