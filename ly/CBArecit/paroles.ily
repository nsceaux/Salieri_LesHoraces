\tag #'camille {
  Mon pè -- re, ah ! pre -- nez part à la pu -- bli -- que joi -- e,
  Et souf -- frez qu’à vos yeux la mien -- ne se dé -- ploi -- e.
}
\tag #'vhorace {
  Ma fil -- le, de -- vant vous je dois en con -- ve -- nir,
  Je ché -- ris Cu -- ri -- a -- ce & j’es -- ti -- me ses frè -- res :
  Mon cœur n’a pu sans dé -- plai -- sir
  Voir com -- battre au -- jour -- d’hui des per -- son -- nes si chè -- res ;
  Mais Ro -- me com -- man -- doit, il fal -- loit o -- bé -- ir.
}
\tag #'camille {
  Ah ! que son choix m’a fait souf -- frir !
}
\tag #'vhorace {
  Le Ciel va pro -- non -- cer sur la cau -- se com -- mu -- ne,
  Il peut ou ré -- prou -- ver ou con -- fir -- mer ce choix.
}
\tag #'camille {
  Ah ! loin de moi cette i -- dé -- e im -- por -- tu -- ne :
  Non, les Dieux ne sau -- raient dic -- ter d’in -- jus -- tes loix.
  Ils ins -- pi -- roient le peu -- ple, ils par -- loient par sa voix.
}
