\clef "vbasse" R1*4 |
r2 r4 r8 la |
fad fad r fad16 sol la4. la8 |
do'8 do' do' re' si4^! r |
si8 si si si16 si si8 fad^! r8 fad16 sol |
la4 la8 si sol^! sol r mi |
sold4.^! sold8 si si la si |
sold4 sold8 sold sold4 la8 si |
mid4^! sold8 la si4 si8 dod' |
la^! la r4 dod' la16 la sold la |
fad4 r8 la16 fad sid4 sid8 dod'? |
sold4^! r r2 |
r2 r4 r8 sold |
sold sold sold dod' si4^! r8 fad16 sold |
la4 la8 si sold^! sold r si |
si si la si sold4 r8 sold |
sold sold la si mi4 r |
R1*6 |
