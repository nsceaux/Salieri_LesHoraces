\clef "treble" mi'1~ |
mi'~ |
mi'~ |
mi' |
re'4-\sug\f dod' fad'2~ |
fad'1 |
<< { la'2 si'~ | si' } \\ { fad' sol'~ | sol' } >> si'~ |
si'1 |
si'~ |
si' |
si' |
la'4 r r2 |
R1 |
r4 sold' sold'2~ |
sold'~ sold'4 r |
r2 fad'4 r |
r2 sold'4 r |
R1*2 |
<<
  { si'1~ | si'2 dod'' | dod''1~ | dod''2 } \\
  { sold'1~ | sold'2 la' | lad'1~ | lad'2 }
>> si'2~ |
si'1~ |
si'2 r4 si' |
