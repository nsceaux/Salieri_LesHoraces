\clef "treble" dod''1~ |
dod''~ |
dod''~ |
dod'' |
re''16\f( la' fad' la' sol' mi' dod' la) re'2~ |
re'1 |
re'~ |
re'2 fad'~ |
fad' sol' |
sold'?1~ |
sold'!~ |
sold' |
<< la'4 \\ fad' >> r4 r2 |
R1 |
r4 sid' dod''2~ |
dod''~ dod''4 r |
r2 si'4 r |
r2 si'4 r |
R1*2 |
mi''1~ |
mi'' |
fad''~ |
fad''~ |
fad'' |
mid''2 r4 mid'' |
