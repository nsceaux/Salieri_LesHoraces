\clef "alto" la'1~ |
la'~~ |
la'~ |
la'~ |
la'2-\sug\f la' |
la'1 |
re'1~ |
re'2 red'~ |
red' mi' |
mi'1~ |
mi' |
mid' |
fad'4 r r2 |
R1 |
r4 red' dod'2~ |
dod'~ dod'4 r |
r2 red'4 r |
r2 mi'4 r |
R1*2 |
mi'1~ |
mi' |
fad'~ |
fad'~ |
fad' |
sold'2 r4 sold' |
