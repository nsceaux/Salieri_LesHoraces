\clef "bass" sol!1~ |
sol~ |
sol~ |
sol |
fad4\f mi re2~ |
re1~ |
re2 sol~ |
sol red~ |
red mi |
re!1~ |
re |
dod |
fad4 r r2 |
R1 |
r4 sold mi!2~ |
mi~ mi4 r |
r2 red4 r |
r2 mi4 r |
R1*2 |
mi1~ |
mi2 la |
fad1~ |
fad2 re~ |
re1~ |
re2 r4 dod |
