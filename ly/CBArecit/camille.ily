\clef "vbas-dessus" r4 r8 la' dod'' dod'' r4 |
mi'' re''8 mi'' dod''4. dod''8 |
dod'' dod'' si' dod'' la' la' r la'16 la' |
la'8 la'16 si' dod''8 r16 mi'' dod''8 dod'' dod'' re'' |
la'^! la' r4 r2 |
R1*9 |
r2 sold'4 sold'8 sold' |
dod''8. dod''16 dod''8 mi'' dod''4^! r |
R1*4 |
mi''4^! si'8 si' sold'4 si'8 dod'' |
re'' re'' re'' mi'' dod''^! dod'' r4 |
dod''4.^! dod''8 lad'4 dod''16 dod'' dod'' re'' |
mi''8. dod''16 dod''8 re'' si'4^! r8 fad'' |
re'' re'' dod'' re'' si' si' r re''16 si' |
mid''4^! mid''8 fad'' dod''4 r |
