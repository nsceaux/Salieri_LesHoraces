\clef "vbasse" r2 do'4 sol8 sol |
mi4 r r8 sol la si |
do'4 do r8 la do' la |
fa2 sol4 sol8 sol |
do4 r r8 sol sol sol |
do'4 do'8 do' do'4 la8 sol |
fad4 r r2 |
R1*19 |
<>^\markup Récit r4 re'8 re' si4 sol8 la |
si4 si8 do' sol4^! r4 |
do'^! r8 sol sib4 sib8 do' |
la4^! r8 la re'8. sold16 sold8 la |
mi8^! mi r4 r2 |
R1*2 |
r2 r4 r8 mi |
la4. la8 do'4. la8 |
re'4 re'8 re' re'4 re'8 re' |
si2 r |
do'4 sol8 sol mi do re mi |
fa4 fa sol8 sol la si |
do'4 do r8 do' do' do' |
si4 si8 si si4 si8 si |
do'4 r r2 |
R1*2 |
r4 r8 do' la4. la8 |
sol4. sol8 fa4. fa8 |
mi4 do r2 |
R1*2 |
r2 r4 r8 sol |
do'4. do'8 la!4. la8 |
fa1 |
sol2 sol4 sol |
do'4. do'8 la4 la |
fa1 |
sol2 sol4 sol |
do r r2 |
R1*16 |
