\clef "treble" do'8\f re'16 mi' fa' sol' la' si' do''4\mf sol'8 sol' |
mi'8 \grace { re'16[ do' si] } do'8[\f re' mi'] fa' \grace { la'16[ sol' fa'] } sol'8 la' si' |
<<
  \tag #'violino1 {
    do''4 r r2 |
    r8 fa''16\mf fa'' re''8 do'' <si' re'>4 q8 q |
  }
  \tag #'violino2 {
    do''8 do''16\f do'' mi''8 do'' la'8 la'16\mf la' do''8 la' |
    fa'2 r8 sol' sol sol |
  }
>>
do'8\f re'16 mi' fa' sol' la' si' do''8 sol'16[\mf sol'] sol'8 sol' |
<<
  \tag #'violino1 {
    <do'' mi'>4 q8 q q4 la'8 sol' |
    fad'4 r8 la''\f re'''4. re'''8 |
    si''8 si''[ re''' sol''] re'''2 |
    si''4 r r2 |
    r8 do'''16 do''' la''8 sol'' << fad''4 \\ la' >>
  }
  \tag #'violino2 {
    do''8 <mi' sol>16 q q8 q q q16 q q8 <do' mi'> |
    <re' la>4 r8 la'-\sug\f re''[ \grace { sol''16[ fad'' mi''] } fad''8 sol'' la''] |
    re''4 r r8 \grace { sol''16[ fad'' mi''] } fad''8 sol'' la'' |
    re''8 sol''16[ sol''] si''8 sol'' mi''8 mi''16[ mi''] sol''8 mi'' |
    do'' mi''16[ mi''] do''8 si' la'4
  }
>> << { fad''8 fad'' } \\ { la'8 la' } >> |
sol8 la16 si do' re' mi' fad' sol'8 re''16[ re''] re''8 mi'' |
<<
  \tag #'violino1 {
    <re' la' fa''!>4 re''8 re'' re'''4 si''8 la'' |
    sold''4 r r8 si'\p( mi'' re'') |
    do''4 la'8 la' dod''2-\sug\f |
    re''8\f re'16 re' fa'8 la' re'' la'\p re'' mi'' |
    fa''4 re''8 re'' si'!4 si'8 si' |
    do''4 r r sol''8\sf( sold'' |
    la'') mi''([ fa'' dod'']) re''8\f la''4 la''8 |
    la'4 r <mib' mib''>2\sf |
    re''8\p sol''16( re'' sol'' re'' sol'' re'' sol''\cresc re'' sol'' re'' sol'' re'' sol'' re''\!) |
    mib''32\mf do''' do''' do''' do''' do''' do''' do''' <>\cresc \rt#8 do''' \rt#8 do''' \rt#8 do''' |
    <>\!\f \rt#16 do''' \rt#16 do''' |
    si''8.\ff re'''16 re'''8. si''16 si''8. sol''16 sol''8. re''16 |
    si'4
  }
  \tag #'violino2 {
    <re' la' fa''!>8 <fa' la'>16[ q] q8 q q4 fa'' |
    mi'8\ff fad'16 sold' la' si' dod'' red'' mi''4 r |
    mi''4-\sug\p la''8 la'' sib''4.\f sib'8 |
    la'8 re'16 re' fa'8 la' re'' re'-\sug\p la' sol' |
    fa'4 fa'8 la' sold'4 sold'8 sold' |
    la'\fp do''16( la' do'' la' do'' la') do''-\sug\cresc sol' do'' sol' do'' sol' mi' do' |
    do' fa' la' do'' do'' la' dod'' la' re''\f la' re'' la' re'' la' re'' la' |
    re'' la' re'' la' re''\p la' re'' la' do''! la' do'' la' do'' la' do'' la' |
    \rt#8 <re' si'>16 <>-\sug\cresc \rt#8 q |
    <>\!-\sug\mf do''8 mib''16 do'' <>-\sug\cresc mib'' do'' mib'' do'' mib'' do'' mib'' do'' mib'' do'' mib'' do'' |
    <>\!-\sug\f \rt#16 fad''32 \rt#16 fad'' |
    sol''8.-\sug\ff <re'' si''>16 q8. <si' sol''>16 q8. <re' si'>16 q8. q16 |
    q4
  }
>> <re' si' si''> <re' si' sol''> r\fermata |
<<
  \tag #'violino1 {
    mi'16\p( sol') do''-. do''-.
    mi'( sol') do''-. do''-.
    mi'( sol') do''-. do''-.
    mi'( sol') do''-. do''-. |
    fa'( la') do''-. do''-.
    fa'( la') do''-. do''-.
    fa'( la') do''-. do''-.
    fa'( la') do''-. do''-. |
    <re' si'>1\fp ~ |
    q2 <mi' do''> |
    do''1\fp~ |
    do''2 sold'\sf |
  }
  \tag #'violino2 {
    <sol mi'>1-\sug\p |
    <fa' la> |
    <sol fa'>-\sug\fp~ |
    q2 <mi' sol> |
    sol'1-\sug\fp |
    la'2 si!-\sug\sf |
  }
>>
r2 <mi' si' sold''>-\sug\f |
la16 si do' re' mi' do' si la sold4 r |
la16 si do' re' mi' re' do' si la8 <<
  \tag #'violino1 {
    la''4 do''8 |
    r8 si'\sf( re'' fa'') r8 si'\sf( si'' sold'') |
    la''4.\fp la'8
  }
  \tag #'violino2 {
    la'4 la'8 |
    r8 la'4 la'8 r8 sold'4 si'8 |
    la'4.-\sug\fp la'8
  }
>> do''8\fp do''16[ do''] do''8 la' |
re''8\f <fad' la>16 q q8 q \rt#4 q |
<sol sol'>8 sol\f sol' sol' sol'16 la' sol' fa' mi' re' do' si |
do'4 <<
  \tag #'violino1 {
    r8 sol'' do'''4. do'''8 |
    la''4 r <sol'' si' re'>4 sol''8 sol'' |
    << mi''8 \\ do'' >> do'16[ do'] do'8 do' do' <<
      { do'''8[ do''' do'''] } \\ { mi''[ mi'' mi''] } >> |
    <fa'' re'''>1 |
    << do'''4 \\ mi'' >> r4 r8 sol'\p( sol'' mib'') |
    re''8 re''4 re'' re''8[ fa'' re''] |
    r do'' do'' do'' r si' si' si' |
    do''\f
  }
  \tag #'violino2 {
    r4 r r8 do'' |
    fa''4. re''8 <si' re'>8 si'[ do'' re''] |
    <do'' mi'>8 do'16[ do'] do'8 do' do' <<
      { mi''8[ mi'' mi''] } \\ { do''[ do'' do''] } >> |
    \grace sol''8 fa'' mi''16 fa'' \grace mi''4 re''8 do''16 re'' \grace do''4 si'8 la'16 si' \grace la'4 sol'8 fa'16 sol' |
    do''8 sol'16[ sol'] sol'8 sol' <>-\sug\p \rt#4 sol' |
    lab'8 lab'4 lab' lab' fa'8 |
    r8 mib' mib' mib' r re' re' re' |
    do'-\sug\f
  }
>> do''16[ do''] do''8 do'' fa''\f do''16 do'' do''8 do'' |
mi''!\f do''16 do'' do''8 do'' la''-\sug\f do''16 do'' do''8 do'' |
sol''\f do''16 do'' do''8 do''
<<
  \tag #'violino1 {
    do''8\p( sol'') sol'' sol'' |
    sol''( fad'') fad'' fad'' r fa''! fa'' fa'' |
    r4 mib''4. re''8( fa'' re'') |
    r8 do'' do'' do'' r si' si' si' |
    do''16\fp <<
      { do'''16[ do''' do'''] \rt#4 do''' \rt#4 do''' \rt#4 do''' |
        \rt#8 do''' \rt#8 do''' |
        \rt#8 si'' \rt#8 si'' |
        \rt#8 do''' \rt#8 do''' |
        \rt#8 do''' \rt#8 do''' |
        \rt#8 si'' \rt#8 si'' |
        do'''4 } \\
      { mi''!16 mi'' mi'' \rt#4 mi'' \rt#4 mi'' \rt#4 mi'' |
        <>\fp \rt#8 re'' \rt#8 re'' |
        <>\fp \rt#8 re'' \rt#8 re'' |
        <>\fp \rt#8 mi'' \rt#8 mi'' |
        <>\ff \rt#8 re'' \rt#8 re'' |
        \rt#8 re'' \rt#8 re'' |
        do''4 }
    >> r4 r2 |
    r4 sol'\p( sol'' mib'') |
    re''8(-. re''-. re''-. re''-.) re''(-. re''-. re''-. re''-.) |
    r8 mib''-. mib''-. mib''-. r re''-. re''-. re''-. |
    r4 mib''(\< sol'' sol'')\! |
    lab''2.\f\fermata fa''8\p re'' |
    r4 do'' r si' |
    do''4\f( do'''4. sib''8 lab'' sol'') |
    sol''4 fad''2 fad''4 |
    fa''!( lab''2 sol''8 fa'') |
    fa''4( mib'') r lab''\p |
    r sol'' r si' |
    do'' r sol'2\sf( |
    mib'4) r4 mib'2\sf( |
    do'4) r r2 |
    do'4\p r do' r |
    do' r r2 |
  }
  \tag #'violino2 {
    do''4\p( sol') |
    la'2 si' |
    do''( lab'4 fa') |
    r8 mib' mib' mib' r re' re' re' |
    mi'!8-\sug\fp sol'16 do'' mi''8 mi'' mi''16 re'' do'' si' la' sol' fa' mi' |
    re'8 re''16 fa'' la''8 la'' la''16 sol'' fa'' mi'' re'' do'' si' la' |
    sol'8 sol''16 si'' re'''8 re''' re'''16 do''' si'' la'' sol'' fa'' mi'' re'' |
    mi''8 sol'16 do'' mi''8 mi'' mi''16 re'' do'' si' la' sol' fa' mi' |
    re'8-\sug\ff re''16 fa'' la''8 la'' la''16 sol'' fa'' mi'' re'' do'' si' la' |
    << { \rt#8 sol''16 \rt#8 sol'' }
      \\ { \rt#8 si' \rt#8 si' } >> |
    do''8 do'' do'' do'' do'' do'' do'' do'' |
    <>-\sug\fp \rt#4 do'' \rt#4 do'' |
    do''(-. do''-. do''-. do''-.) do''(-. do''-. do''-. do''-.) |
    r8 do''-. do''-. do''-. r si'-. si'-. si'-. |
    r4 do''\< do'' do''\! |
    do''2.-\sug\f lab'8-\sug\p fa' |
    r4 mib' r re' |
    do'-\sug\f do''2 do''4~ |
    do'' do''( re'' mib'') |
    re''1 |
    sol'2 r4 fa'-\sug\p |
    r mib' r re' |
    do' r mib'2-\sug\sf( |
    do'4) r sol2-\sug\sf~ |
    sol4 r r2 |
    sol4-\sug\p r sol r |
    sol r r2 |
  }
>>
