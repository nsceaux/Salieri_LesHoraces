\tag #'vhorace {
  Oui mes en -- fans par -- tez sur l’heu -- re.
  Al -- lez rem -- plir vo -- tre de -- voir
  Lais -- sez Ca -- mille, & son vain dé -- ses -- poir.
}
\tag #'(curiace jhorace) {
  Al -- lons, a -- mi, par -- tons sur l’heu -- re
  Al -- lons rem -- plir no -- tre de -- voir :
}
\tag #'jhorace {
  Lais -- sons Ca -- mille, & son vain dé -- ses -- poir.
}
\tag #'curiace {
  Al -- lons é -- teindre un a -- mour sans es -- poir.
  Al -- lons é -- teindre un a -- mour sans es -- poir.
}
\tag #'camille {
  Quoi c’est donc en vain que je pleu -- re !
  Quoi ? rien ne peut les é -- mou -- voir :
  On mé -- pri -- se mon dé -- ses -- poir.
}
\tag #'curiace {
  Sei -- gneur, en ce mo -- ment fu -- nes -- te,
  Puis-je en -- cor ?…
}
\tag #'vhorace {
  Je t’en -- tends : ne viens point m’at -- ten -- drir
  Va : rem -- plis ton de -- voir… les Dieux fe -- ront le res -- te.
}
\tag #'camille {
  Ti -- gres, al -- lez com -- bat -- tre, & moi je vais mou -- rir.
}
\tag #'vhorace {
  Ma fille, al -- lons, ren -- trez & lais -- sez- les par -- tir,
}
\tag #'vhorace {
  Oui mes en -- fans par -- tez sur l’heu -- re,
  Oui par -- tez sur l’heu -- re.
  Lais -- sez Ca -- mille, & son vain dé -- ses -- poir.
  Oui mes en -- fans par -- tez sur l’heu -- re.
}
\tag #'jhorace {
  Al -- lons, a -- mi, par -- tons sur l’heu -- re
  Lais -- sons Ca -- mille, & son vain dé -- ses -- poir.
  Al -- lons, a -- mi, par -- tons sur l’heu -- re.
}
\tag #'curiace {
  Al -- lons, a -- mi, par -- tons,
  Al -- lons é -- teindre un a -- mour sans es -- poir.
  Al -- lons, a -- mi, par -- tons sur l’heu -- re.
}
\tag #'camille {
  Ti -- gres, al -- lez
  On mé -- pri -- se mon dé -- ses -- poir.
  On mé -- pri -- se
  On mé -- pri -- se mon dé -- ses -- poir.
}
\tag #'vhorace {
  Al -- lez, al -- lez rem -- plir vo -- tre de -- voir
  Al -- lez rem -- plir vo -- tre de -- voir
}
\tag #'(curiace jhorace) {
  Al -- lons, al -- lons rem -- plir no -- tre de -- voir :
  Al -- lons rem -- plir no -- tre de -- voir :
}
\tag #'camille {
  Ti -- gres
  On mé -- pri -- se mon dé -- ses -- poir.
  On mé -- pri -- se mon dé -- ses -- poir.
}