\clef "bass" do8\f re16 mi fa sol la si do'4\mf sol8 sol |
mi do[\f re mi] fa sol la si |
do'8 do'[\f mi' do'] la la[\mf do' la] |
fa2 sol4 sol,8 sol, |
do\f re16 mi fa sol la si do'4 r |
r8 do16-\sug\mf do do8 do \rt#4 do |
re4 r r8 re\f mi fad |
sol4 r r8 re mi fad |
sol4 r r8 mi' sol' mi' |
do' do16[ do] do8 do \rt#4 re |
sol, la,16 si, do re mi fad sol4 r |
r8 re16 re re8 re \rt#4 re |
mi8\ff fad16 sold la si dod' red' mi'4 sold\p |
la2( sol!)\f |
fa4 r r8 fa8\p fa dod |
re4 re8 re mi4 mi, |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    \clef "tenor" r2 r4 do'-\sug\cresc~ |
    do'8 mi' fa' dod' re'4-\sug\f r |
    r2 fad'-\sug\p |
    << { sol'1 | sol' | }
      { s2 s-\sug\cresc | s1 <>-\sug\f } >>
    \clef "bass" do' |
    si4-\sug\ff sol,8. sol,16 sol8. sol16 sol8. sol16 |
    sol4 sol, sol r\fermata |
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { sol1 | la | si4 }
      { mi1 | fa | fa4 }
      { s1-\sug\p }
    >> r4 r2 |
    R1*3 |
    r2 mi-\sug\f |
    la,4 r \clef "tenor" \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { s8 fa'4 fa'8 |
        mi'4 s4. mi'4 mi'8 |
        s8 fa'4 fa'8 s mi'4 mi'8 |
        do'4 }
      { s8 re'4 re'8 |
        do'4 s4. la4 la8 |
        s8 la4 la8 s sold4 sold8 |
        la4 }
      { r8 s4. | s4 r r8 s4. | r8 s4. r8 s4. | }
    >> r4 r2 |
    R1 |
    \clef "bass" r8
  }
  \tag #'basso {
    la,8\fp la la la <>\cresc \rt#4 mi8 |
    \rt#4 fa <>\f \rt#4 fa |
    \rt#4 fa <>\p \rt#4 fad |
    <>\p \rt#4 sol <>\cresc \rt#4 sol |
    <>\mf \rt#4 sol <>\cresc \rt#4 sol |
    <>\f \rt#4 sol \rt#4 sol |
    <>\ff sol8. sol16 sol8. sol16 sol8. sol16 sol8. sol16 |
    sol4 sol, sol r\fermata |
    do4\p do do do |
    do do do do |
    do1\fp~ |
    do |
    mi\fp |
    fa2 fa\sf |
    r2 mi\f |
    la,16 si, do re mi do si, la, sold,4 r |
    la,16 si, do re mi re do si, la,4 do |
    re r mi r |
    la4.\fp la,8 do4.\fp la,8 |
    re8\f re16 re re8 re \rt#4 re |
    sol8
  }
>> sol,[\f sol sol] sol16 la sol fa! mi fa mi re |
do4 r r8 do re mi |
fa fa,16[ fa,] fa,8 fa, sol, sol[ la si] |
do' do16 do do8 do \rt#4 do |
\rt#4 do \rt#4 do |
do2 <<
  \tag #'(fagotto1 fagotto2 fagotti) {
    \clef "tenor" mib'2-\sug\p |
    fa'1 |
    sol' | \clef "bass"
  }
  \tag #'basso {
    mib2\p |
    fa4 fa fa fa |
    sol r sol, r |
  }
>>
do2\f la!\f |
sol2\f fa\f |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    mi2 mib4-\sug\p( mib') |
    re'2 \clef "tenor" sol' |
    lab' fa' |
    mib'2. re'4 \clef "bass" |
  }
  \tag #'basso {
    mi2\f( mib)\f |
    re sol |
    lab fa |
    sol sol,4.*5/6 sol,16 la, si, |
  }
>>
do8\fp do' do' do' la! la la la |
<>\fp \rt#4 fa \rt#4 fa |
<>\fp \rt#4 sol \rt#4 sol |
<>\fp \rt#4 do' \rt#4 la |
<>-\sug\ff \rt#4 fa \rt#4 fa |
sol,8 si,16 re sol8 sol sol16 fa mi re do si, la, sol, |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    \clef "tenor" do'1 | mib'-\sug\fp | fa' | r4 sol' r sol' |
    do'2-\sug\< mib'\! | fa'1-\sug\f | r4 mib'-\sug\p r re' |
    do'-\sug\f( do'8. re'16 mib'2)~ | mib'4 mib'( re' do') | si!1 |
    do'2 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { s4 lab' | s sol' s re' | do' s sol'2( |
        mib'4) s mib'2( | do'4) s2. | do'4 s do' s | do' s2. | }
      { s4 fa' | s mib' s si! | do' s mib'2( |
        do'4) s sol2( | mib4) s2. | mib4 s mib s | mib s2. | }
      { r4 s-\sug\p | r s r s | s r s2\sf |
        s4 r s2-\sug\sf | s4 r r2 | s4-\sug\p r s r | s r r2 | }
    >>
  }
  \tag #'basso {
    do1 | mib\fp | fa | sol4 r sol, r |
    do2\< mib\! | fa1\f\fermata | sol4\p r sol, r | do\f do8. re16 mib2~ |
    mib4 mib'( re' do') | si!2 si, | do fa4\p r4 | sol r sol, r |
    do r r2 | do4 r r2 | do4 r r2 | do4\p r do r |
    do r r2 |
  }
>>
