\clef "vhaute-contre" R1*6 |
r2 r8 fad' sol' la' |
re'4 r r8 fad' sol' la' |
re' re' r4 r2 |
r8 mi' mi' mi' fad fad' fad' fad' |
sol'4 r r2 |
R1 |
r2 r8 mi' mi' re' |
do'4 la8 la dod'4 dod'8 dod' |
re'2 r8 re' re' mi' |
fa'4 re'8 re' si4 mi'8 mi' |
la4 r r2 |
R1*6 |
r2 r4 r8\fermata sol8 |
do'4. do'8 mi'!8. re'16 do'8. si16 |
la4 la r la8 do' |
si4 r r2 |
R1*10 |
r2 r4 r8 do' |
fa'4. re'8 si4 r8 sol |
do'4 r r8 mi' mi' mi' |
fa'4 re'8 re' si4 sol8 sol |
do'4 r r2 |
R1*2 |
r4 r8 do' fa'4. fa'8 |
mi'4. mi'8 la'4. la'8 |
sol'4 mi' r2 |
R1*2 |
r2 r4 r8 sol |
do'4. do'8 mi'!4 mi' |
re'1 |
sol'2 sol'4 sol' |
mi'4. mi'8 mi'4 mi' |
re'1 |
sol'2 sol'4 sol' |
do'4 r r2 |
R1*16 |
