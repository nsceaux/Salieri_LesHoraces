\clef "treble" r2 do''4-\sug\mf sol'8 sol' |
mi'4 r r2 |
R1 |
r2 r4 \twoVoices #'(tromba1 tromba2 trombe) <<
  { re''8 re'' | do''4 }
  { sol'8 sol' | mi'4 }
>> r4 r2 |
R1 |
r2 r8 re''16-\sug\f re'' re''8 re'' |
sol'4 r r8 re''16 re'' re''8 re'' |
sol'4 r r2 |
r8 \twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''16 mi'' mi''8 mi'' }
  { do''16 do'' do''8 do'' }
>> re''4 re''8 re'' |
sol' sol'16 sol' sol'8 sol' sol'4 r |
re''1 |
mi''8-\sug\ff \twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''16 mi'' mi''8 mi'' mi''4 }
  { mi'16 mi' mi'8 mi' mi'4 }
>> r4 |
R1*2 |
r2 r8 mi''16\f mi'' mi''8 mi'' |
mi''4 r r2 |
r r8 re''16-\sug\f re'' re''8 re'' |
re''4 r r2 |
r8 <>\f \twoVoices #'(tromba1 tromba2 trombe) <<
  { sol'16 sol' sol'8 sol' \rt#4 sol'8 | sol'1~ | sol' | }
  { sol16 sol sol8 sol \rt#4 sol8 | sol1~ | sol | }
>>
sol'8.-\sug\ff sol'16 sol'8. sol'16 sol'8. sol'16 sol'8. sol'16 |
sol'4 sol' sol' r\fermata |
R1*6 |
r2 mi'-\sug\f |
do'4 r r2 |
R1*4 |
r8 sol'16-\sug\f sol' sol'8 sol' \rt#4 sol' |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { do''4 }
  { mi'4 }
>> sol'8 sol' mi'4 r |
r2 \twoVoices #'(tromba1 tromba2 trombe) <<
  { re''4 re''8 re'' | mi'' }
  { sol'4 sol'8 sol' | do'' }
>> do''16 do'' do''8 do'' do''
\twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''8 mi'' mi'' | re''1 | do''8 sol'16 sol' }
  { do''8 do'' do'' | sol'1 | mi'8 sol'16 sol' }
>> sol'8 sol' sol'2 |
R1*2 |
r8 do''16-\sug\f do'' do''8 do'' do'' do''16 do'' do''8 do'' |
do'' do''16 do'' do''8 do'' do'' do''16 do'' do''8 do'' |
do'' do''16 do'' do''8 do'' do''4 r |
R1*3 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { s8 mi''16 mi'' mi''8 mi'' \rt#4 mi''8 |
    s8 re''16 re'' re''8 re'' \rt#4 re'' |
    re''4 re''8. re''16 re''4 re'' |
    s8 mi''16 mi'' mi''8 mi'' \rt#4 mi''8 |
    s re''16 re'' re''8 re'' \rt#4 re''8 |
    re''4 re''8. re''16 re''4 re'' |
    do''2 }
  { s8 do''16 do'' do''8 do'' \rt#4 do''8 |
    s8 do''16 do'' do''8 do'' \rt#4 do''8 |
    sol'4 sol'8. sol'16 sol'4 sol' |
    s8 do''16 do'' do''8 do'' \rt#4 do''8 |
    s8 do''16 do'' do''8 do'' \rt#4 do''8 |
    sol'4 sol'8. sol'16 sol'4 sol' |
    do'2 }
  { r8 s s2. | r8 s s2. | s1 | r8 s s2. | r8 s s2. | }
>> r2 |
R1*16 |
