\clef "alto" do8\f re16 mi fa sol la si do'4\mf sol8 sol |
mi do[\f re mi] fa sol la si |
do' do'16-\sug\f do' mi'8 do' la8 la16-\sug\mf la do'8 la |
fa la' fa' mi' re'4 sol'8 sol |
do'8-\sug\f re'16 mi' fa' sol' la' si' do''4 r |
r8 do'16 do' do'8 do' \rt#4 do' |
re'4 r r8 re'16-\sug\f re' re'8 re' |
re'4 r r8 re'16 re' re'8 re' |
re'8 sol'16[ sol'] si'8 sol' mi' mi'16[ mi'] sol'8 mi' |
do'4 do8. do16 re2 |
sol8 la16 si do' re' mi' fad' sol'4 r |
r8 re'16 re' re'8 re' \rt#4 re' |
mi8\ff fad16 sold la si dod' red' mi'4 r |
mi'4-\sug\p do'8 do' mi'2-\sug\f |
fa'8 re16 re fa8 la re' la'-\sug\p la' la' |
la'4 la'8 fa' mi'4 mi'8 mi' |
mi'-\sug\fp mi'16( do' mi' do' mi' do') sol-\sug\cresc do' sol do' \rt#4 do' |
do'16 la do' la fa la' la' la' la'-\sug\f fa' la' fa' la' fa' la' fa' |
la' fa' la' fa' la'-\sug\p fa' la' fa' la' fad' la' fad' la' fad' la' fad' |
\rt#8 sol' <>-\sug\cresc \rt#8 sol' |
sol'8\!-\sug\mf do''16 sol' do''-\sug\cresc sol' do'' sol' do'' sol' do'' sol' do'' sol' do'' sol' |
<>\!-\sug\f mib'' do'' mib'' do'' mib'' do'' mib'' do'' mib'' do'' mib'' do'' mib'' do'' mib'' do'' |
si'8.-\sug\ff sol16 sol8. sol16 sol8. sol16 sol8. sol16 |
sol4 sol sol r\fermata |
do'1-\sug\p~ |
do'~ |
do'~ |
do'~ |
do'~ |
do'2 re'-\sug\sf |
r mi'\f |
la16 si do' re' mi' do' si la sold8 sold'4 sold8 |
la16 si do' re' mi' re' do' si la8 la'4 la'8 |
r8 re''4 re''8 r mi'4 mi'8 |
la'4.\fp la8 do'4.\fp la8 |
re'\f re'16[ re'] re'8 re' \rt#4 re' |
sol'8 sol[\f sol' sol'] sol'16 la' sol' fa'! mi' fa' mi' re' |
do'4 sol8 sol mi mi'[ fa' sol'] |
do' do'[ la' fa'] re' sol'[ sol' sol'] |
sol' <do' do>16[ q] q8 q \rt#4 q |
q1 |
q4 r mib'2-\sug\p  |
fa'1 |
sol' |
do'8-\sug\f do'16[ do'] do'8 do' la!-\sug\f do'16[ do'] do'8 do' |
sol-\sug\f do'16 do' do'8 do' fa-\sug\f do'16 do' do'8 do' |
mi-\sug\f do'16 do' do'8 do' do'2-\sug\p |
la4 re'2 re'4 |
do'2( fa'4 lab') |
sol'1 |
do'8-\sug\fp sol'16 do'' mi''8 mi'' mi''16 re'' do'' si' la' sol' fa' mi' |
re'8 re'16 fa' la'8 la' la'16 sol' fa' mi' re' do' si la |
sol8 sol'16 si' re''8 re'' re''16 do'' si' la' sol' fa' mi' re' |
mi'8 sol'16 do'' mi''8 mi'' mi''16 re'' do'' si' la' sol' fa' mi' |
re'8-\sug\ff re'16 fa' la'8 la' la'16 sol' fa' mi' re' do' si la |
sol8 si16 re' sol'8 sol' sol'16 fa' mi' re' do' si la sol |
do'1( |
sol'1)\fp |
lab'8(-. lab'-. lab'-. lab'-.) \rt#4 lab' |
r8 sol'-. sol'-. sol'-. r sol'-. sol'-. sol'-. |
r4 sol'\< sol' sol'\! |
fa'1-\sug\f |
r4 sol'-\sug\p r sol' |
do'4-\sug\f do'8.( re'16 mib'2)~ |
mib'4 mib'( re' do') |
si!1 |
do'2 fa'4-\sug\p r |
sol' r sol r |
do'4 r r2 |
do'4 r r2 |
do'4 r r2 |
do'4-\sug\p r do' r |
do' r r2 |
