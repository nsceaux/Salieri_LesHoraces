\clef "vbas-dessus" R1*15 |
r2 mi''4 r |
r8 do'' do'' do'' do''4 do''8 sold' |
la' la' r4 re'' r |
r re''8 re'' mib'' mib'' mib'' do'' |
si'4 r r re''8 re'' |
mib''2. mib''4 |
fad''2 fad''4 fad'' |
sol''4 r r2 |
R1 |
R1*7 |
mi''4 la' r8 fa'' fa'' fa'' |
mi''4 la' r r8 do'' |
si'4. fa''8 mi''4. sold'8 |
la'4 r r2 |
R1*3 |
r2 sol''4 sol''8 sol'' |
mi''4 r r2 |
R1 |
r2 r4 sol''8 mib'' |
re''2. fa''8[ re''] |
do''2 si'4. si'8 |
do''4 r r2 |
R1 |
r2 r4 sol''8 sol'' |
fad''4 fad'' r fa''!8 fa'' |
mib''2~ mib''8[ re''] fa''[ re''] |
do''2 si'4. si'8 |
do''4 r r2 |
R1*3 |
la''2 re''4 r |
R1*2 |
r2 sol''4 mib'' |
re''2. fa''8[ re''] |
do''2 re''4 re'' |
mib'' r sol'' sol'' |
lab''2.\fermata fa''8[ re''] |
do''4 r si'4. si'8 |
do''4 r r2 |
R1*9 |
