\clef "treble" r2 do''4-\sug\mf sol'8 sol' |
mi'8 do'-\sug\f[ re' mi'] fa'[ \grace { la'16 sol' fa' } sol'8 la' si'] |
do''4 r r2 |
r8 \twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''8 re'' do'' si'4 sol''8 sol'' | mi''4 }
  { la'4 la'8 re'4 si'8 si' | do''4 }
  { s8-\sug\mf }
>> r4 r2 |
R1 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { r2 r8 fad''-\sug\f\prall sol'' la'' |
    re''4 r r8 fad'' sol'' la'' |
    re''4 s2. |
    s8 do''' la'' sol'' fad''2 | }
  { r4 r8 la'-\sug\f re''4. re''8 |
    si' si'[ re'' sol'] re''4. re''8 |
    si'4 s2. |
    s8 mi'' do'' si' la'2 | }
  { s1*2 | s4 r r2 | r8 }
>>
sol'8 la'16 si' do'' re'' mi'' fad'' sol''4 r |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''!1 | mi''~ | mi''4( la'' sib''4. dod''8) | re''8 re' fa' la' re''4 }
  { la'1 | sold'4 r r2 | r2 r4 sib' | la'8 re' fa' la' re''4 }
  { s1*3 | s4-\sug\f }
>> r4 |
R1 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''2~ do''4 sol''8(\sf sold'' | la'') mi''( fa'' dod'') re''4 }
  { la'2 sol'!8 mi' do' do''~ | do'' la'4 la'8 la'4 }
  { s2-\sug\fp s2-\sug\cresc | s2 s4\!-\sug\f }
>> r4 |
r2 \twoVoices #'(oboe1 oboe2 oboi) <<
  { la''2 | re'' si'' | do'''1~ | do''' |
    si''8. re'''16 re'''8. si''16 si''8. sol''16 sol''8. re''16 |
    si'4 si'' sol'' }
  { do''2 | si' re'' | mib''1~ | mib'' |
    re''8. si'16 si'8. re''16 re''8. si'16 si'8. si'16 |
    sol'4 re'' si' }
  { s2-\sug\p | s s-\sug\cresc | s1 | s-\sug\f | s-\sug\ff }
>> r4\fermata |
R1*6 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { s2 sold''2 |
    la''4 s4. fa''4 fa''8 |
    mi''4 s4. la''4 la''8 |
    s8 la''4 la''8 s8 sold''4 sold''8 |
    la''4 }
  { s2 si' |
    do''4 s4. re''4 re''8 |
    do''4 s4. do''4 do''8 |
    s8 do''4 do''8 s8 si'4 si'8 |
    la'4 }
  { r2 s-\sug\f | s4 r r8 s4. | s4 r4 r8 s4. | r8 s4. r8 s4. | }
>> r4 r2 |
R1 |
r8 sol'16-\sug\f sol' sol'8 sol' sol'4 r |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { r4 r8 sol'' do'''2 |
    la''4 r sol'' sol''8 sol'' |
    mi''4 s4. mi''8 mi'' mi'' |
    fa''1 |
    mi''4 }
  { r2 r4 r8 do'' |
    fa''4. re''8 si'4 do''8 re'' |
    sol'4 s4. do''8 do'' do'' |
    si'1 |
    do''4 }
  { s1*2 | s4 r4 r8 }
>> r4 r2 |
R1*2 |
r2 fa''4.-\sug\f fa''8 |
mi''4. mi''8 la''4. la''8 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''1 |
    sol''4 fad''2 fa''4~ |
    fa''4 mib''4. re''8 fa'' re'' |
    s4 do''2 si'4 |
    do'''1 |
    do''' |
    si'' |
    do''' |
    do''' |
    si'' |
    do'''4 r r2 |
    r4 sol''2(-\sug\p mib''4) |
    re''1 |
    s8 mib''-. mib''-. mib''-. s re''-. re''-. re''-. |
    s4 \once\slurDashed mib''?( sol'' sol'') |
    lab''2.\fermata fa''8 re'' |
    s4 do'' s si' |
    do''4( do'''4. sib''8 lab'' sol'') |
    sol''4 fad''2 fad''4 |
    fa''!4( lab''2 sol''8 fa'') |
    fa''4( mib'') s lab'' |
    s sol'' s si' |
    do'' s sol''2( |
    mib''4) s mib''2( |
    do''4) s2. |
    do''4 s do'' s |
    do'' s2. | }
  { sol''4 sol'2 sol'4 |
    la'2 si' |
    do'' lab'?4 fa' |
    s4 sol'2 sol'4 |
    mi''!1 |
    re'' |
    re'' |
    mi'' |
    re'' |
    re'' |
    do''~ |
    do''-\sug\p~ |
    do'' |
    s8 do''-. do''-. do''-. s si'-. si'-. si'-. |
    s4 do''2 do''4 |
    do''2.\fermata lab'4 |
    s4 sol' s sol' |
    do''1~ |
    do''4 do'' re'' mib'' |
    re''1 |
    sol'2 s4 fa'' |
    s mib'' s re'' |
    do'' s mib''2( |
    do''4) s sol'2( |
    mib'4) s2. |
    mib'4 s mib' s |
    mib' s2. | }
  { s2 s-\sug\p | s1*2 | r4 s2. | s1\fp | s1-\sug\fp | s1-\sug\fp | s1-\sug\fp |
    s-\sug\ff | s1*4 | r8 s4. r8 s4. |
    r4 s2-\sug\< s4\! | s2.-\sug\f s4-\sug\p | r4 s r s | s1-\sug\f | s1*2 |
    s2 r4 s-\sug\p | r s r s | s r s2\sf | s4 r s2\sf |
    s4 r4 r2 | s4-\sug\p r s r | s r r2 | }
>>
