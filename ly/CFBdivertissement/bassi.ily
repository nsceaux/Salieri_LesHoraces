\clef "bass" do'2\ff |
sol4 mi |
do8 do16 do do8 do |
do4 r |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    la2(^"Solo" | sol2 | fa2) | mi8 <>^"[Tutti]"
  }
  \tag #'basso {
    <>^"[Violoncelli]" la16\p do' la do' la do' la do' |
    sol do' sol do' sol do' sol do' |
    fa si re' si fa si re' fa |
    mi8 <>^"[Tutti]"
  }
  \tag #'cb { R2*3 | r8 }
>> sol8[\f la si] |
do'2 |
sol4 mi |
do8 do16 do do8 do |
do2 |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    fa2(^"Solo" | mi | si,) | do4 r |
  }
  \tag #'basso {
    <>^"[Violoncelli]" fa16\p la do' la fa la do' la |
    mi sol do' sol mi sol do' sol |
    si, re sol re si, re sol si, |
    do4 r | <>^"[Tutti]"
  }
  \tag #'cb { R2*4 | }
>>
sol,4\ff r |
r8 <<
  \tag #'(fagotto1 fagotto2 fagotti) {
    sol8[ sol, sol] |
  }
  \tag #'(basso cb) {
    sol8[ sol sol] |
  }
>>
re4 r |
R2 |
re4 r |
r8 re[ re re] |
sol8. sol16 do'8. do'16 |
re'4 re |
sol,\p r |
R2 |
r8 sol[\ff sol sol] |
\rt#4 fad |
sol4 r |
R2 |
r8 sol[\f sol sol] |
fad fad fad fad |
sol4 sol,8. sol,16 |
sol,4 r |
%%%
sol2\sf |
fa!\sf |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    mi2 |
    mi8^"Solo" fad16 sold la si do' re' |
    mi'4 do'8 la |
    la sold sold la |
    si4. do'16 re' |
    \appoggiatura re'8 do'4 si8 la |
    do'4( re') |
    sol8 r r4 |
    do'4( re') |
    sol8 <>^"Tutti"
  }
  \tag #'(basso cb) {
    mi8 mi16 mi mi8 mi |
    mi2 |
    <<
      \tag #'basso {
        <>^"[Violoncelli]" la16\p do' mi' do' la do' mi' do' |
        si re' mi' re' si re' mi' re' |
        sold si mi' si sold si mi' si |
        la do' mi' do' la do' mi' do' |
        la8. si16 si8.\trill la32 si |
        do'2 |
        la8. si16 si8.\trill la32 si |
        do'8 <>^"[Tutti]"
      }
      \tag #'cb { R2*7 | r8 }
    >>
  }
>> sol8[\f la si] |
do'2 |
sol4 mi |
do8 do16 do do8 do |
do2 |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    la2(^"Solo" | sol | fa | mi4) r^"[Tutti]" |
  }
  \tag #'basso {
    <>^"[Violoncelli]" la16\p do' la do' la do' la do' |
    sol do' sol do' sol do' sol do' |
    fa si re' si fa si re' fa |
    mi4 r^"[Tutti]" |
  }
  \tag #'cb { R2*4 | }
>>
do8\ff re16 mi fa sol la si |
do'8 sol mi do |
fa sol16 la sib do' re' mi' |
fa'8 do' la fa |
re' dod'16 si la sol fad mi |
re mi fad sol la si do'! re' |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    sol2~ | sol~ | sol | sol |
  }
  \tag #'(basso cb) {
    sol8 sol, sol, sol, |
    \rt#4 sol,-\sug\p |
    \rt#4 sol, |
    \rt#4 sol, |
  }
>>
do16\ff re mi fa sol la si do' |
si la sol fa mi re do si, |
do8. mi16 fa8. la16 |
sol4 sol, |
do\p r |
R2 |
r8 do[\f do do] |
si,8 sol, sol, la,16 si, |
do\ff mi mi mi \rt#4 mi |
\rt#8 si, |
\rt#8 do |
si,8 si, sol, sol, |
do4 do |
do r |
do r8
