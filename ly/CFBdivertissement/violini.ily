\clef "treble" << do'''2 \\ do''\ff >> |
sol''4 mi'' |
do''8 do''16 do'' do''8 do'' |
do''2 |
<<
  \tag #'violino1 {
    fa''2\p( |
    mi''4. fa''8) |
    \grace mi''4 re''2 |
    do''8
  }
  \tag #'violino2 {
    la'16\p do'' la' do'' la' do'' la' do'' |
    sol' do'' sol' do'' sol' do'' sol' do'' |
    fa' si' re'' si' fa' si' re'' fa' |
    mi'8
  }
>> sol''-.[\f la''-. si''-.] |
<do''' do''>2 |
sol''4 mi'' |
do''8 do''16 do'' do''8 do'' |
do''2 |
<<
  \tag #'violino1 {
    la''2\p | sol''8( la'' si'' do''' | sol''4. fa''8) | mi''4 r |
  }
  \tag #'violino2 {
    fa'16\p la' do'' la' fa' la' do'' la' |
    mi' sol' do'' sol' mi' sol' do'' sol' |
    si re' sol' re' si re' sol' si |
    do'4 r |
  }
>>
<re' si' sol''>8\ff sol'16[ sol'] sol'8 sol' |
sol' <<
  \tag #'violino1 { sol''8[ si'' sol''] | la'' }
  \tag #'violino2 { si'8[ re'' sol''] | fad''8 }
>> re'16[ re'] re'8 re' |
re' re' re' re' |
<<
  \tag #'violino1 { <re' la' la''>8 }
  \tag #'violino2 { <re' la' fad''>8 }
>> re'16[ re'] re'8 re' |
re' <<
  \tag #'violino1 {
    la''8[ do''' la''] |
    si''8. sol''16 mi''8. la''16 |
    sol''4 fad'' |
    sol''4.\p \grace la''16 sol'' fad''32 sol'' |
    la''4. \grace si''16 la'' sol''32 la'' |
    si''8 <<
      { re'''8[ re''' re'''] | re''' re''' } \\
      { re''8[\ff re'' re''] | re'' re'' }
    >> re'''16 do''' si'' la'' |
    sol''4.\p sol''8\trill |
    la''4. la''8\trill |
    si''8 <<
      { re'''8[ re''' re'''] | re''' re''' } \\
      { re''[\f re'' re''] | re'' re'' }
    >> re'''16 do''' si'' la'' |
    sol''4
  }
  \tag #'violino2 {
    fad''8[ la'' fad''] |
    sol''8. si'16 do''8. do''16 |
    si'4 la' |
    si'16-\sug\p re'' si' re'' si' re'' si' re'' |
    fad' re'' fad' re'' fad' re'' fad' re'' |
    sol'-\sug\ff re'' si' re'' si' re'' si' re'' |
    la' re'' la' re'' la' re'' la' re'' |
    si'-\sug\p re'' si' re'' si' re'' si' re'' |
    fad' re'' fad' re'' fad' re'' fad' re'' |
    sol'-\sug\f re'' si' re'' si' re'' si' re'' |
    la' re'' la' re'' la' re'' la' re'' |
    si'4
  }
>> <sol' si>8. q16 |
q4 r |
%%%
<re' si' sol''>8 sol'16 sol' sol'8 sol' |
fa'!8 fa''16 fa'' fa''8 fa'' |
mi'' mi'16 mi' mi'8 mi' |
mi'2 |
la16\p do' mi' do' la do' mi' do' |
si re' mi' re' si re' mi' re' |
sold? si mi' si sold si mi' si |
la do' mi' do' la do' mi' do' |
la do' la do' si re' si re' |
do' mi' do' mi' do' mi' do' mi' |
la do' la do' si re' si re' |
do'8 sol''\f[ la'' si''] |
do'''2 |
sol''4 mi'' |
do''8 do''16 do'' do''8 do'' |
do''2 |
<<
  \tag #'violino1 {
    fa''2(\p |
    mi''4. fa''8 |
    \grace mi''4 re''2 |
    do''8) do''[-.\f mi''-. sol''-.] |
    sib''2 |
    sib' |
    la' |
    la'' |
    fad' |
    do''' |
    si''8 <sol' si>16[ q] q8 q |
    sol'16\p <re' si> q q \rt#4 q |
  }
  \tag #'violino2 {
    la'16\p do'' la' do'' la' do'' la' do'' |
    sol' do'' sol' do'' sol' do'' sol' do'' |
    fa' si' re'' si' fa' si' re'' fa' |
    mi'4 r |
    << mi''2 \\ sol'-\sug\ff >> |
    <mi' sol> |
    <la fa'> |
    <fa' do''> |
    la |
    << { fad'' | sol''8 } \\ { la'2 | si'8 } >>
    <re' si>16 q q8 q |
    \rt#8 q16-\sug\p |
  }
>>
\rt#8 <do' mi'>16 |
\rt#8 <si re'> |
<do' mi'>8\ff <<
  { do'''16 do''' do'''8 do''' | } \\
  { mi''16 mi'' mi''8 mi'' | }
>>
re'''16 do''' si'' la'' sol'' fa'' mi'' re'' |
<<
  \tag #'violino1 {
    mi''8. sol''16 la''8. fa''16 |
    mi''4 re'' |
    do''4.\p \grace re''16 do'' si'32 do'' |
    re''4. \grace mi''16 re'' do''32 re'' |
    mi''8 sol''[\f sol'' sol''] |
    sol'' sol'' sol'' la''16 si'' |
    do'''4.\ff \grace re'''16 do''' si''32 do''' |
    re'''4. \grace mi'''16 re''' do'''32 re''' |
    mi'''8( do''') sol'' sol'' |
    sol'' sol'' sol''16 fa'' mi'' re'' |
    do''4 << <do''' mi''> \\ sol' >> |
    << <do''' mi''> \\ sol' >> r |
    << <do''' mi''> \\ sol' >> r8
  }
  \tag #'violino2 {
    mi''8. do''16 do''8. do''16 |
    do''4 si' |
    do''16\p sol' mi' sol' mi' sol' mi' sol' |
    si sol' si sol' si sol' si sol' |
    do' sol' mi''8 sol'16-\sug\f mi'' sol' mi'' |
    sol' re'' sol' re'' sol'8 la'16 si' |
    do''-\sug\ff <sol' sol> q q \rt#4 q |
    \rt#4 q \rt#4 q |
    do' <sol' mi''> q q \rt#4 q |
    \rt#4 <sol' re''> \rt#4 <re' si'> |
    <mi' do''>4 <sol mi' do'' mi''> |
    q r |
    q r8
  }
>>
<<
  \tag #'violino1 {

  }
  \tag #'violino2 {

  }
>>