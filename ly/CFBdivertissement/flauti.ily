\clef "treble" do'''2-\sug\ff |
sol''4 mi'' |
do''8 do''16 do'' do''8 do'' |
do''2 |
R2*4 |
do'''2\f |
sol''4 mi'' |
do''8 do''16 do'' do''8 do'' |
do''2 |
R2*4 | <>-\sug\ff
\twoVoices #'(flauto1 flauto2 flauti) <<
  { sol''4 }
  { si'4 }
>> r4 |
r8 \twoVoices #'(flauto1 flauto2 flauti) <<
  { sol''8[ si'' sol''] | la''4 }
  { si'8[ re'' sol''] | fad''4 }
>> r4 |
R2 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { la''4 }
  { fad'' }
>> r |
r8 \twoVoices #'(flauto1 flauto2 flauti) <<
  { la''8[ do''' la''] | si''8. sol''16 mi''8. la''16 | sol''4 fad'' | }
  { fad''8[ la'' fad''] | sol''8. si'16 do''8. do''16 | si'4 la' | }
>>
<<
  \tag #'(flauto1 flauti) \new Voice {
    \tag #'flauti \voiceOne
    sol''4. \grace la''16 sol''^"Solo" fad''32 sol'' |
    la''4. \grace si''16 la'' sol''32 la'' |
    si''8
  }
  \tag #'(flauti flauto2) \new Voice {
    \voiceTwo sol'4 sol'\rest | R2 | sol'8\rest
  }
  \tag #'flauto2 \new CueVoice {
    \voiceOne 
    s4. \grace la''16 sol''^"Solo" fad''32 sol'' |
    la''4. \grace si''16 la'' sol''32 la'' |
    si''8
  }
>> <>^"Tutti" re'''8-\sug\ff[ re''' re'''] |
re''' re''' re'''16 do''' si'' la'' |
<<
  \tag #'(flauto1 flauti) \new Voice {
    \mergeDifferentlyDottedOn
    \tag #'flauti \voiceOne
    sol''4.^"Solo" sol''8\trill |
    la''4. la''8\prall |
    si''
  }
  \tag #'(flauto2 flauti) \new Voice {
    \voiceTwo
    sol''4 sol'\rest | R2 | sol'8\rest
  }
  \tag #'flauto2 \new CueVoice {
    \voiceOne sol''4.^"Solo" sol''8\trill |
    la''4. la''8\prall |
    si''
  }
>> <>^"[Tutti]" re'''8-\sug\f[ re''' re'''] |
re''' re''' re'''16 do''' si'' la'' |
sol''4 sol'8. sol'16 |
sol'4 r |
%%
sol''2-\sug\sf |
fa''!-\sug\sf |
mi''~ |
mi''8^"Solo" fad''16 sold'' la'' si'' do''' re''' |
mi'''4 do'''8 la'' |
la'' sold'' sold'' la'' |
si''4. do'''16 re''' |
\grace re'''8 do'''4 si''8 la'' |
R2*3 |
r8 sol''-\sug\f[ la'' si''] |
do'''2 sol''4 mi'' |
do''8 do''16 do'' do''8 do'' |
do''2 |
R2*4 |
sib''2-\sug\ff |
sib' |
la' |
la'' |
fad' |
do''' |
si''4 r |
<<
  \twoVoices #'(flauto1 flauto2 flauti) <<
    { re'''16 do''' si'' la'' sol'' fa'' mi'' re'' |
      mi'' fa'' sol'' la'' si'' do''' re''' mi''' |
      re''' do''' si'' la'' sol'' fa'' mi'' re'' |
      mi''8 }
    { \voiceTwo R2*3 | r8 \oneVoice }
  >>
  \tag #'flauto2 \new CueVoice {
    re'''16 do''' si'' la'' sol'' fa'' mi'' re'' |
    mi'' fa'' sol'' la'' si'' do''' re''' mi''' |
    re''' do''' si'' la'' sol'' fa'' mi'' re'' |
    mi''8
  }
>> do'''16-\sug\ff do''' do'''8 do''' |
re'''16 do''' si'' la'' sol'' fa'' mi'' re'' |
mi''8. sol''16 la''8. fa''16 |
mi''4 re'' |
do'' r |
R2 |
r8 sol''[-\sug\f sol'' sol''] |
sol'' sol'' sol'' la''16 si'' |
do'''4.\ff \grace re'''16 do''' si''32 do''' |
re'''4. \grace mi'''16 re''' do'''32 re''' |
mi'''8( do''') sol'' sol'' |
sol'' sol'' sol''16 fa'' mi'' re'' |
do''4 \twoVoices #'(flauto1 flauto2 flauti) <<
  { do'''4 | do''' }
  { mi''4 | mi'' }
>> r |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { do'''4 }
  { mi'' }
>> r8
