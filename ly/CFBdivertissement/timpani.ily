\clef "bass" do2\ff |
do4 do |
do8 do16 do do8 do |
do4 r |
R2*4 |
do2-\sug\f do4 do |
do8 do16 do do8 do |
do2 |
R2*4 |
sol,4-\sug\ff r |
r8 sol,[ sol, sol,] |
do4 r |
R2 |
do4 r |
r8 do[ do do] |
sol,8. sol,16 do8. do16 |
sol,4 do |
sol, r |
R2 |
r8 sol,[-\sug\ff sol, sol,] |
do8 do16 do do8 do |
sol,4 r |
R2 |
r8 sol,[-\sug\f sol, sol,] |
do do16 do do8 do |
sol,4 sol,8. sol,16 |
sol,4 r |
%%%
R2*12 |
do2\f |
do8 do16 do do8 do |
do do16 do do8 do |
do2 |
R2*4 |
\rt#8 do16\ff |
\rt#8 do |
\rt#8 do |
\rt#8 do |
\rt#8 do |
\rt#8 do |
sol,4 r |
R2*3 |
do8-\sug\ff do16 do \rt#4 do |
sol,8 sol,16 sol, sol,8 sol, |
do8. do16 do8. do16 |
do4 sol, |
do r |
R2 |
r8 do[-\sug\f do do] |
sol, sol, sol, sol,16 sol, |
do16-\sug\ff do do do \rt#4 do |
\rt#8 sol, |
do8 do16 do do8 do |
sol, sol, \rt#4 sol,16 |
do4 do |
do r |
do r8
