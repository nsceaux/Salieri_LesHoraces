\clef "treble" do''2-\sug\ff |
sol'4 mi' |
do'8 do'16 do' do'8 do' |
do'2 |
R2*4 |
do''2\f |
sol'4 mi' |
do'8 do'16 do' do'8 do' |
do'2 |
R2*4 |
r8 sol'16-\sug\ff sol' sol'8 sol' |
sol' sol'[ sol' sol'] |
re''4 r |
R2 |
r8 re''16 re'' re''8 re'' |
re'' re''[ re'' re''] |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { re''4 mi''8. mi''16 | }
  { sol'4 do''8. do''16 | }
>>
re''2~ |
re''-\sug\p~ |
re''~ |
re''8 re''-\sug\ff[ re'' re''] |
\rt#4 re'' |
re''2-\sug\p~ |
re''~ |
re''8 re''-\sug\f[ re'' re''] |
\rt#4 re'' |
sol'4 sol'8. sol'16 sol'4 r |
%%
sol'4 r |
R2 |
r8 \twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''16 mi'' mi''8 mi'' |
    mi''2~ | mi''~ | mi'' | mi''~ | mi'' | }
  { mi'16 mi' mi'8 mi' |
    mi'2~ | mi'~ | mi' | mi'~ | mi' | }
>>
R2*4 |
do''2 |
sol'4 mi' |
do'8 do'16 do' do'8 do' |
do'2 |
R2*3 |
r8 do''-\sug\f[ do'' do''] |
do''2~ |
do''8 do''16 do'' do''8 do'' |
do''2~ |
do''8 do''16 do'' do''8 do'' |
re''2~ |
re''8 re''16 re'' re''8 re'' |
sol'4 r |
R2*3 |
r8 sol'16-\sug\ff sol' sol'8 sol' |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { re''2 | mi''8. do''16 do''8. do''16 | mi''4 re'' | do'' }
  { sol'2 | do''8. do''16 do''8. do''16 | do''4 sol' | mi' }
>> r4 |
R2 |
r8 <>-\sug\f \twoVoices #'(tromba1 tromba2 trombe) <<
  { sol''8[ sol'' sol''] |
    sol'' sol' sol' sol' |
    do''4. do''8 |
    re''4. re''8 | }
  { sol'8[ sol' sol'] |
    sol' sol' sol' sol' |
    mi'4. mi'8 |
    sol'4. sol'8 | }
  { s4. | s2 | s2-\sug\ff }
>>
mi''8 do'' sol' sol' |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { sol'8 sol'' sol'' sol'' | do''4 do'' | do'' }
  { sol'8 sol' sol' sol' | mi'4 mi' | mi' }
>> r4 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { do''4 }
  { mi' }
>> r8
