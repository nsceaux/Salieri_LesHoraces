\clef "alto" do'2\ff |
do'4 do' |
do'8 do'16 do' do'8 do' |
do'4 r |
la16-\sug\p do' la do' la do' la do' |
sol do' sol do' sol do' sol do' |
fa si re' si fa si re' fa |
mi8 sol'-\sug\f [ la' si'] |
do''2 |
sol'4 mi' |
do'8 do'16 do' do'8 do' |
do'2 |
fa16\p la do' la fa la do' la |
mi sol do' sol mi sol do' sol |
si re' sol' re' si re' sol' si |
do'4 r |
<sol re' si'>4\ff r |
r8 re''[ si' sol'] |
re'8 re'16[ re'] re'8 re' |
re' re' re' re' |
re re'16[ re'] re'8 re' |
re' re'[ re' re'] |
re'4 do'8. do'16 |
re'4 re |
si'16-\sug\p re'' si' re'' si' re'' si' re'' |
fad' re'' fad' re'' fad' re'' fad' re'' |
sol'-\sug\ff re'' si' re'' si' re'' si' re'' |
la' re'' la' re'' la' re'' la' re'' |
si'-\sug\p re'' si' re'' si' re'' si' re'' |
fad' re'' fad' re'' fad' re'' fad' re'' |
sol'-\sug\f re'' si' re'' si' re'' si' re'' |
la' re'' la' re'' la' re'' la' re'' |
si'4 sol8. sol16 |
sol4 r |
%%%
sol'8 sol16 sol sol8 sol |
fa! fa'16 fa' fa'8 fa' |
mi' mi16 mi mi8 mi |
mi fad16 sold la si do' re' |
mi'4 do'8 la |
la sold sold la |
si4. do'16 re' |
\grace re'8 do'4 si8 la |
R2*3 |
r8 sol'\f[ la' si'] |
do''2 |
sol'4 mi' |
do'8 do'16 do' do'8 do' |
do'2 |
la16\p do' la do' la do' la do' |
sol do' sol do' sol do' sol do' |
fa si re' si fa si re' fa |
mi4 r |
do8-\sug\ff re16 mi fa sol la si |
do'8 sol mi do |
fa sol16 la sib do' re' mi' |
fa'8 do' la fa |
re' dod'16 si la sol fad mi |
re mi fad sol la si do'! re' |
\rt#8 sol16 |
\rt#8 sol16-\sug\p |
\rt#8 sol16 |
\rt#8 sol16 |
do'\ff re' mi' fa' sol' la' si' do'' |
si' la' sol' fa' mi' re' do' si |
do'8. mi'16 fa'8. la'16 |
sol'4 sol |
do'16\p sol' mi' sol' mi' sol' mi' sol' |
si sol' si sol' si sol' si sol' |
do' sol' mi''8 sol'16-\sug\f mi'' sol' mi'' |
sol' re'' sol' re'' sol'8 la'16 si' |
do''16-\sug\ff <sol' sol> q q \rt#4 q |
\rt#4 q \rt#4 q |
do' <sol' mi''> q q \rt#4 q |
\rt#4 <sol' re''> \rt#4 <re' si'> |
<mi' do''>4 <sol mi' do'' mi''> |
q r |
q r8
