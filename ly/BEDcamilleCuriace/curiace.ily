\clef "alto/G_8" r4 |
R2.*14 |
r4 r r8 fa' |
\grace mib'4 re'2 r4 |
fa'4. lab'8 \grace sol' fa' mib'16 re' |
mib'2 r4 |
sol'4. fa'16[ mib'] \grace re'16 do'8 sib16 la |
sib2 r8 do' |
do'4 la r |
re'8. mi'16 fa'4. re'8 |
do'[ la] fa4 r |
si4. do'8 re' mi' |
fa'2 \grace sol'8 fa'8 mi'16[ re'] |
do'4 sib!8. sib16 mi'8. fa'16 |
sol'4. mi'8 do' sib |
la2 r4 |
fa'4. mi'16[ fa'] \grace sol'8 fa'8 mi'16[ re'] |
do'2 do'8 dod' |
re'2 \grace mi'8 re'8 do'!16[ sib] |
sib8 la r fa' fa'8. la16 |
sol4. sol8 mi'8. mi'16 |
fa'4. la'8 \grace sol'16 fa'8 mi'16[ re'] |
do'8. re'16 la4. sol8 |
fa2 r4 |
sol'4. fa'8 mib' re' |
mib'2 \grace fa'8 mib' re'16[ do'] |
sib8 la r4 r |
R2. |
r4 sib8 do' re' mib' |
fa'2 \grace mib'16 re'8 do'16[ sib] |
sib8 la r4 r |
R2.*4 |
re'4. re'8 re' re' |
do'4 re'8. re'16 re'8. re'16 |
mib'4. mib'8 sol'8. mib'16 |
fa'4. fa'8 la8. la16 |
sib4 r r\fermata |
