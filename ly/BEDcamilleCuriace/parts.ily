\piecePartSpecs
#`((corno1 #:tag-global () #:instrument "Corno I en mi♭")
   (corno2 #:tag-global () #:instrument "Corno II en mi♭")
   (oboe1)
   (oboe2)
   (clarinetto1 #:score-template "score-clarinette-sib")
   (clarinetto2 #:score-template "score-clarinette-sib")
   (fagotto1 #:notes "fagotti" #:clef "tenor")
   (fagotto2 #:notes "fagotti" #:clef "tenor")
   (violino1 #:notes "violino1")
   (violino2 #:notes "violino2")
   (alto)
   (basso #:instrument "Basso")
   (silence #:on-the-fly-markup , #{ \markup\tacet#52 #}))
