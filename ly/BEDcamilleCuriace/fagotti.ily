\clef "tenor" r4 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { re'4. re'8 sol' fa' |
    mib'2.~ |
    mib'4. fa'8 sol' sol' |
    do'2. |
    do'8[ sib] }
  { sib2.~ | sib~ | sib | la | la8[ sib] }
>> sib,4 do |
re sib( do') |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { re'8 fa' fa' fa' fa' fa' |
    mib'4. sol'8 fad' sol' |
    sol'2~ sol'8 fa' |
    fa'4\f( mib') }
  { re'2. |
    mib'4. mib'8 re' mib' |
    mib'2~ mib'8 re' |
    re'4(-\tag #'fagotto2 -\sug\f mib') }
>> mib4 |
fa fa fa |
sib4. sib8 la! lab |
sol4. solb8\cresc solb solb\! |
fa8\f mib re4 mib |
\rt#6 fa8 |
sib4 r r |
R2.*4 |
r8 <>-\sug\fp \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { la4 sib8( do' dod') | re'( mi' fa'4.) mi'16 re' | do'2. }
  { fa4 sol8( la la) | sib8( do' re'4.) do'16 sib | la2. }
  { s8 s2 | s8\< s\! }
>> fa2.~ |
fa |
sol2.~ |
sol |
fa4 r r |
re'4.( dod'16 re' \grace mi'8 re'8 do'?16 sib) |
sib8 la la2 |
sib8( re' fa'4) \grace sol'8 fa'8 mi'16 re' |
re'8 do' do'8. fa'16 fa'8. fa'16 |
R2.*2 |
r4 r do |
fa8\f( sol lab2) |
sol2.~ |
sol |
fa4 la8(\< sib do' re'\!) |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { mib'!2. | re'4 }
  { do'2. | sib4 }
>> r4 r |
R2.*2 |
sol'4. sol'8 sol' sol' |
fa' fad'4.
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sol'8 sol' | re' fa' re' fa' re' la | }
  { sol'8 sol | \rt#6 fa8 | }
>>
sib4. \clef "bass" sib8\f( la lab) |
sol4\p sol solb |
fa8[\cresc mib] re[ re re re]\! |
<>\f \rt#6 mib8 |
\rt#6 fa |
sib,8. sib,16 sib,4 r\fermata |
