\clef "soprano/treble" re''8. do''16 |
sib'4 sib'8 sib' mib''8. re''16 |
\grace re''4 do''2 r4 |
do''4. re''8 mib'' mi'' |
fa''2 sol''8 mib'' |
do''[ re''] re''4 r |
r re''8 re'' \grace fa'' mib''8 re''16[ do''] |
sib'2 \grace mib''8 re'' do''16[ sib'] |
\grace la'4 sol'2 r4 |
sol'4. la'8 sib' si' |
\grace si'?4 do''2 \grace fa''16 mib''8 re''16[ do''] |
sib'2~ sib'8[ do''] |
\grace do''4 re''2 r4 |
mib''4. mi''8 mi'' mi'' |
fa''4.( fad''8) sol''16[ mib''] re''[ do''] |
sib'2( \grace re''8 do''4) |
sib'2 r4 |
R2.*18 |
r4 r mi''8. mi''16 |
fa''4 fa''8 mib''! re'' do'' |
si'2 r4 |
R2. |
r4 fa'8 sol' la' sib' |
do''2 mib''16[ do''] sib'[ la'] |
sib'4 r r |
R2.*2 |
mi''4. mi''8 mi'' mi'' |
fa''( mib''!4.) re''8 mib''16[ do''] |
sib'2~ sib'8[ do''] |
\grace do''4 re''2 r4 |
mib''!4. mib''8 mi'' mi'' |
fa''4. fad''8 fad'' fad'' |
sol''2 mib''8 do'' |
sib'2( \grace re''8 do''4) |
sib'4 r r\fermata |
