\clef "treble" r4 |
R2.*4 |
r4 re''8\f re'' \grace fa''16 mib''8 re''16 do'' |
sib'4 r r |
R2.*9 |
r8 lab'4\f lab' lab'8~ |
lab'?\p lab'4 lab' lab'8 |
sol' sol'4\cresc sol' sol'8~ |
sol' sol'4 sol'8 la'!8. la'16\! |
sib'8.\f << { sib''16 sib''4 } \\ { sib'16 sib'4 } >> r4 |
r8 la'4\fp sib'8( do'' dod'') |
re''\mf( mi'' fa''4.) mi''16 re'' |
do''8.\f la'16 fa'4 r |
si'8\p si' si' si' si' si' |
\rt#6 si'8 |
do''8 sib'! \rt#4 sib' |
sib'8\mf sol''4 mi''8 do'' sib' |
la'\f la([ do' fa' la' do'']) |
fa''4.\p( mi''16 fa'' \grace sol''8 fa''8 mi''16 re'') |
re''8 do'' do''4. dod''8 |
re''2 \grace mi''8 re''8 do''!16 sib' |
sib'8 la' la'8. fa''16\f fa''8. la'16 |
sol'8. sol'16 sol'8. sol'16 sol'8. sol'16 |
fa'8\cresc fa''4 la''8 sol''16 fa'' mi'' re''\! |
do''8.\f re''16 la'4 sol' |
fa'2.\f |
sol''8\p sol''4 fa''8 mib'' re'' |
re''8 mib''4 mib''8 \grace fa''8 mib''8 re''16 do'' |
sib'8( la') r4 r |
R2. |
r4 sib'8(\mf do'' re'' mib'') |
fa''8\f fa''4 sol''16 fa'' \grace mib''16 re''8 do''16 sib' |
la'8. << { fa''16 fa''4 } \\ { la'16 la'4 } >> r4 |
R2.*3 |
r4 r8 re''\f( do'' re'') |
mib'' mib''4\p mib''8 mi'' mi'' |
fa''\cresc fa''4 fad''8 fad'' fad''\! |
sol''16\f( sib'' la'' sol'' fad'' sol'' fad'' sol'') \grace fa''16 mib''8 re''16 do'' |
sib'16 <<
  { sib''16 sib'' sib'' \rt#4 sib'' \rt#4 la'' | sib''8. } \\
  { re''16 re'' re'' \rt#4 re'' \rt#4 do'' | sib'8. }
>> sib16 sib4 r\fermata |
