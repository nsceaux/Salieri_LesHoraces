\clef "treble" \transposition mib
r4 |
\twoVoices #'(corno1 corno2 corni) <<
  { R2.*5 | }
  { sol'2.\p~ | sol'~ | sol' | fad'2. |
    fad'?8 sol' sol'4 r | }
>>
R2.*4 |
r4 r
\twoVoices #'(corno1 corno2 corni) <<
  { mi''4 }
  { do'' }
>> re''8 re''4 re'' re''8 |
sol'4 r r |
\twoVoices #'(corno1 corno2 corni) <<
  { sol''2. |
    fad''4 sol''4. mi''8 |
    re''2. |
    sol'2.~ |
    \once\tieDashed sol'~ |
    sol'~ |
    sol'~ |
    sol'8. sol'16 sol'4 }
  { R2.*3 |
    sol2.~ |
    sol~ |
    sol~ |
    sol~ |
    sol8. sol16 sol4
  }
>> r4 |
R2.*7 |
r8 re'' re'' re'' re'' re'' |
re''2.~ |
re''~ |
re''~ |
re''2 r4 |
R2. |
re''2.~ |
re''4 r r |
R2. |
mi''2.~ |
mi'' |
re''2.~ |
re''~ |
re''~ |
re'' |
re''4 re'' r |
R2. |
r4 r r8 \twoVoices #'(corno1 corno2 corni) <<
  { mi''8 }
  { do'' }
>> |
re''2. |
sol'4. \twoVoices #'(corno1 corno2 corni) <<
  { sol''8( fad'' fa'') |
    mi''2 sol''4 |
    re''8 fad'' sol''2 |
    sol''2~ sol''8 mi'' | }
  { r8 r4 |
    R2.*2 |
    mi''2~ mi''8 do'' | }
>>
re''2. |
sol'8. sol'16 sol'4 r\fermata |
