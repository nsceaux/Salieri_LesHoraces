\tag #'camille {
  Par l’a -- mour & par l’a -- mi -- tié,
  Par ce nœud si doux qui nous li -- e !
  Ne te mon -- tre point sans pi -- tié,
  C’est Ca -- mille en pleurs, qui te pri -- e.
  C’est Ca -- mille en pleurs, qui te pri -- e.

  Par l’a -- mour & par l’a -- mi -- tié,
  Ne te mon -- tre point sans pi -- tié,
  C’est Ca -- mille en pleurs, qui te pri -- e.
  C’est Ca -- mille en pleurs, Ca -- mille en pleurs, qui te pri -- e.
}
\tag #'curiace {
  Hé -- las ! tu dé -- chi -- re mon cœur,
  tu dé -- chi -- re mon cœur,
  Ca -- mil -- le, ta dou -- leur m’ac -- ca -- ble :
  Lais -- se ton a -- mant dé -- plo -- ra -- ble
  Mou -- rir, vic -- ti -- me de l’hon -- neur.
  Lais -- se ton a -- mant ton a -- mant dé -- plo -- ra -- ble
  Mou -- rir, vic -- ti -- me de l’hon -- neur,
  Mou -- rir, vic -- ti -- me de l’hon -- neur.
  Lais -- se ton a -- mant dé -- plo -- ra -- ble
  Lais -- se ton a -- mant dé -- plo -- ra -- ble
  Lais -- se ton a -- mant
  Lais -- se ton a -- mant
  Mou -- rir, vic -- ti -- me de l’hon -- neur.
}
