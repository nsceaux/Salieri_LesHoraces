\clef "treble" r4 |
R2.*15 |
r8 \twoVoices #'(oboe1 oboe2 oboi) <<
  { lab''4 lab'' lab''8 |
    lab''?2. |
    sol'' |
    sol''2 la''!4 |
    sib''8. sib'16 sib'4 }
  { re''4 re'' re''8 |
    re''2. |
    mib'' |
    mib'' |
    re''8. re'16 re'4 }
>> r4 |
R2.*3 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''2.~ | fa'' | mi''~ | mi'' |
    fa''8 do''4 do'' do''8 | }
  { si'2.~ | si' |
    do''8 sib'! sib'2~ | sib'2. |
    la'8 la'4 la' la'8 | }
>>
R2.*3 |
r4 r \twoVoices #'(oboe1 oboe2 oboi) <<
  { la'4 |
    sol'2 do''4 |
    fa''2 fa''8 re'' |
    do''8. re''16 do''4 }
  { fa'4 |
    fa'2 mi'4 |
    la'2 la'8 sib' |
    fa'4 la' }
>> r4 |
R2.*5 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { sib'8( do'' re'' mib'') |
    fa''4. sol''16 fa'' \grace mib''16 re''8 do''16 sib' |
    la'8. fa''16 fa''4 }
  { fa'2~ |
    fa'8 fa'4 la'16 sib' fa'8 fa' |
    fa'8. la'16 la'4 }
>> r4 |
R2.*4 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { mib''2 mi''4 |
    fa''4. fad''8 fad'' fad'' |
    sol''16 sib'' la'' sol'' fad'' sol'' fad'' sol'' \grace fa''16 mib''8 re''16 do'' |
    re''2 do''4 | }
  { sib'2. |
    la'8. sib'32 do'' sib'2~ |
    sib'2 sib'8 sol' |
    sib'2 la'4 | }
>>
sib'4 r4 r\fermata |
