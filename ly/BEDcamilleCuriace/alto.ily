\clef "alto" r4 |
R2.*4 |
r4 sib-\sug\f( do' |
re'4) r r |
R2.*9 |
r8 fa'4-\sug\f fa' fa'8~ |
fa'-\sug\p fa'4 fa' fa'8 |
sol' mib'4-\sug\cresc sib sib8 |
<< \rt#6 sib8 { s2 s8. s16\! } >> |
sib8.-\sug\f sib16 sib4 r |
r8 <<
  { la4 sib8( do' dod') | re'( mi' fa'4.) mi'16 re' | do'2. | } \\
  { fa4-\sug\fp sol8( la la) | sib\mf do' re'4. do'16 sib | la2.\f | }
>>
re'8\p re' re' re' re' re' |
\rt#6 re' |
\rt#4 mi' mi'8 mi' |
mi'-\sug\mf mi'4 mi'8 mi' mi' |
fa'-\sug\f fa fa fa fa fa |
<>-\sug\p \rt#6 fa |
\rt#6 fa |
\rt#6 fa |
fa2 la4-\sug\f |
sib2-\sug\p do'4 |
re'4\cresc re' re'8 sib\! |
la8.\f sib16 do'4 do' |
fa8(-\sug\f sol lab2) |
sol8-\sug\p([ si]) re'[ re'( mib' fa')] |
fa' mib'4 mib'8 do' mib' |
\rt#6 fa'8 |
\rt#6 fa' |
r4 re'8-\sug\mf( la sib do') |
re'8-\sug\f re'4 mib'16 re' sib8 re' |
do'8. fa16 fa4 r |
R2.*3 |
r4 r8 sib-\sug\f( la lab) |
sol4-\sug\p sol solb |
fa8-\sug\cresc[ mib] re[ re' re' re']\! |
<>-\sug\f \rt#6 mib'8 \rt#6 fa' |
sib8. sib16 sib4 r\fermata |
