\clef "bass" r4 |
R2.*4 |
r4 sib,\f( do |
re) r r |
R2.*9 |
sib,8\f sib, sib, sib, sib, sib, |
<>\p \rt#6 sib, |
<< { \rt#6 sib, | \rt#6 sib, } { s8 s\cresc s2 | s2 s8. s16\! } >>
sib,8.\f sib,16 sib,4 r |
fa4 fa, r |
fa4.\mf fa8 fa fa |
fa8\f fa,[ la, do fa fa,] |
re4\p re re |
re re re |
r do do |
do\mf do do |
fa8\f fa, fa fa fa fa |
<>\p \rt#6 fa8 |
\rt#6 fa8 |
\rt#6 fa8 |
fa2 la,4\f |
sib,2\p do4 |
re\cresc re re8 sib,\! |
la,8.\f sib,16 do4 do |
fa,8\f( sol, lab,2) |
<>\p \rt#6 sol,8 |
\rt#4 do do8 mib |
<< \rt#6 fa { s4 s4.\< s8\! } >> |
\rt#6 fa |
<>\mf \rt#6 fa |
\rt#6 fa |
fa8. fa,16 fa,4 r |
R2.*3 |
r4 r8 sib\f( la lab) |
sol4\p sol solb |
fa8[\cresc mib] re[ re re re]\! |
<>\f \rt#6 mib |
\rt#6 fa |
sib,8. sib,16 sib,4 r\fermata |
