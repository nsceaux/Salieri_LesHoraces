\clef "treble" r4 |
R2.*4 |
r4 fa'-\sug\f( la' |
sib') r r |
R2.*9 |
r8 re'4-\sug\f re' re'8~ |
re'-\sug\p re'4 re' re'8 |
mib' mib'4\cresc mib' mib'8~ |
mib' mib'4 mib'8 <mib' do'>8. q16\! |
<sib re'>8.\f\noBeam <fa' re''>16 q4 r |
r8 fa'4-\sug\fp sol'8( la' la') |
sib'-\sug\mf( do'' re''4.) do''16 sib' |
la'8.-\sug\f fa'16 do'4 r |
fa'8-\sug\p fa' fa' fa' fa' fa' |
\rt#6 fa' |
\rt#4 sol' sol' sol' |
sol'-\sug\mf sib'4 sol'8 sol' sol' |
fa'-\sug\f do'([ la do' fa' la']) |
re'4.-\sug\p( dod'16 re' \grace mi'8 re'8 do'?16 sib) |
sib8 la la2 |
sib8 re' fa'4 \grace sol'8 fa'8 mi'16 re' |
re'8 do' do'8. fa'16-\sug\f fa'8. fa'16 |
fa'8. fa'16 fa'8. fa'16 mi'8. mi'16 |
fa'2.-\sug\cresc~ |
fa'2\!-\sug\f mi'4 |
fa'8-\sug\f fa'4 mib'!8 re' do' |
si8-\sug\p si'4 si' si'8 |
si' do''4 sol' sol'8 |
do'4 r r |
R2. |
r4 re'8-\sug\mf( la sib do') |
re'8-\sug\f re'4 mib'16 re' sib8 re' |
do'8.\noBeam <fa' do''>16 q4 r |
R2.*3 |
r4 r8 fa'4-\sug\f fa'8 |
sib'2.-\sug\p |
la'8.-\sug\cresc sib'32 do'' sib'8 sib'4 sib'8-\!~ |
sib'8-\sug\f sib'4 sib'8 \grace la'16 sol'8 fa'16 mib' |
re'8 fa''4 re''16 sib' la'8. do''16 |
sib'8. sib16 sib4 r\fermata |
