\clef "treble" \transposition mib
r8 |
R1 |
r2 r4 r8 sol'-\sug\mf |
do''4 sol' r2 |
R1 |
r4 r8 do''16.\f do''32 do''8 do''16. do''32 do''8 do''16. do''32 |
do''2~ do''8 r r4 |
r <>-\sug\p <<
  \tag #'(corno1 corni) \new Voice {
    \tag #'corni \voiceOne
    mi''8. mi''16 re''4 re''8. re''16 | do''2
  }
  \tag #'(corno2 corni) \new Voice {
    \tag #'corni \voiceTwo
    do''8. do''16 do''4 sol'8. sol'16 | mi'2
  }
>> r2\fermata |
\twoVoices #'(corno1 corno2 corni) <<
  { sol''2 re''4 re'' | sol'1 | }
  { sol'2 re''4 re'' | sol1 | }
  { s1-\sug\ff }
>>
R1 |
r4 <>-\sug\p <<
  \tag #'(corno1 corni) \new Voice {
    \tag #'corni \voiceOne
    re''8. re''16 re''4 re'' |
  }
  \tag #'(corno2 corni) \new Voice {
    \tag #'corni \voiceTwo
    sol'8. sol'16 sol'4 re'' |
  }
>>
re''4 re''8.\f re''16 re''4 re'' |
re''2 r |
re''1~ |
re''~ |
\tag #'corni \once\tieUp re'' -\tag #'(corno1 corni) ~ |
<<
  \tag #'(corno1 corni) { \tag #'corni \voiceOne re''2  \oneVoice }
  \tag #'(corno2 corni) \new Voice { \tag #'corni \voiceTwo sol'2 }
>> r2 |
r r4 re''-\sug\p |
sol''1 |
R1*2 |
r4 re''8.-\sug\f re''16 re''4 re'' |
sol' <<
  \tag #'(corno1 corni) \new Voice {
    \tag #'corni \voiceOne
    re''8. re''16 re''4 re'' |
  }
  \tag #'(corno2 corni) \new Voice {
    \tag #'corni \voiceTwo
    sol'8. sol'16 sol'4 sol' |
  }
>>
do''2 sol'' |
mi'' do'' |
sol'1 |
R1 |
\twoVoices #'(corno1 corno2 corni) <<
  { do''1~ | do''~ | do''~ | do'' | }
  { do'1~ | do'~ | do'~ | do' | }
  { s1-\sug\fp | s | s-\sug\f }
>>
R1*3 |
r2 \twoVoices #'(corno1 corno2 corni) <<
  { re''2 | mi''1 | fa''2 la'' | mi''1~ |
    mi''2 re'' | do''1\fermata | }
  { sol'2 | do''1~ | do'' | do''~ |
    do''2 sol' | mi'1\fermata | }
  { s2 | s1-\sug\f | s | s-\sug\ff }
>>
R1*3 |
r2 \twoVoices #'(corno1 corno2 corni) <<
  { re''2 | mi''1 | fa''2 la'' | mi''1~ |
    mi''2 re'' | do''1~ | do''~ | do''~ |
    do''~ | do''~ | do'' | }
  { sol'2 | do''1~ | do'' | do''~ |
    do''2 sol' | do'1~ | do'~ | do'~ |
    do'~ | do'~ | do' | }
  { s2 | s1-\sug\f | s1*3 | s1-\sug\ff }
>>
R1*3 |
r2 \twoVoices #'(corno1 corno2 corni) <<
  { re''2 | mi''1 | fa''2 la'' | mi''1~ |
    mi''2 re'' | do''1\fermata | }
  { sol'2 | do''1 | do'' | do''~ |
    do''2 sol' | mi'1\fermata | }
  { s2 | s1-\sug\ff }
>>
R1*3 |
r2 \twoVoices #'(corno1 corno2 corni) <<
  { re''2 | mi''1 | fa''2 la'' | mi''1~ | mi''2 re'' | }
  { sol'2 | do''1 | do'' | do''~ | do''2 sol' | }
  { s2 | s1-\sug\ff }
>> <>\p
<<
  { s1*4 r4 }
  \tag #'(corno1 corni) \new Voice {
    \tag #'corni \voiceOne
    do''4 do''8. do''16 do''4 do'' | do''1~ |
    do''4 sol'' mi'' do'' | sol'1 |
    s4 do''8. do''16 do''4 do'' | do''1~ |
    do''4 sol'' mi'' do'' | sol'1 |
    do''~ | do''~ | do''~ | do''~ | do''~ | do'' |
    do''2 do'' | do''2
  }
  \tag #'(corno2 corni) \new Voice {
    \tag #'corni \voiceTwo
    do'4 do'8. do'16 do'4 do' | do'1~ |
    do'4 sol' mi' do' | sol1 |
    s4 do'8. do'16 do'4 do' | do'1~ |
    do'4 sol' mi' do' | sol1 |
    do'~ | do'~ | do'~ | do'~ | do'~ | do' |
    do'2 do' | do'
  }
>> r2 |
R1*8 | <>\f
<<
  \tag #'(corno1 corni) \new Voice {
    \tag #'corni \voiceOne
    re''2 re''4. re''8 | do''4
  }
  \tag #'(corno2 corni) \new Voice {
    \tag #'corni \voiceTwo
    sol'2 sol'4. sol'8 | mi'4
  }
>> r4 r2 |
