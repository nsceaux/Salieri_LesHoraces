\clef "treble" sol16\ff la32 si |
do'8. re'16 re'8.\trill do'32 re' mib'4. si16 do'32 re' |
mib'4. mib'32 fa' sol' lab' sib'4. sib'16\mf do''32 re'' |
mib''4 sib' r8 sol'( lab' do'') |
re'4( mib'8.) sol'16 sib4.\f lab16\trill sol32 lab |
sol4 r r r8 mib' |
<lab mib' do''>4 r r8 mib'-. lab'-. do''-. |
sib'4 mib''8.\p sol'16 lab'8.\trill sol'32 lab' sib'8. sib16 |
<sol mib'>2 r\fermata |
<re' sib' sib''>\ff fa''4 re'' |
sib8 do'16 re' mib' fa' sol' la' sib'8 do''16 re'' mib'' fa'' sol'' la'' |
<<
  \tag #'violino1 {
    sib''4. fa''8\p fa''4. fa''8 |
    re'''2.\fp sib''4 |
    la''\f <la'' do'' la>8. <la'' do''>16 <la'' do'' la>4 q |
    <la fa'>2 fa''4\p la'' |
    do'''2 re'''4. re'''8 |
    mib'''2 do'''4. la''8 |
    fa''2 mib''4. mib''8 |
    re''2 r4 fa'8.\ff fa'16 |
    re'4 sib re' fa' |
    sib'4.\p sib''8 sib''8. reb'16 \grace mib'8 reb' do'16 reb' |
    do'4. do''8 mi''4. sol''8 |
    do'''2. sib''4 |
    lab''4\f <<
      { lab''8. lab''16 lab''4 lab'' | } \\
      { do''8. do''16 do''4 do'' | }
    >>
    sib4 sib''8. sib''16 sib''4 sib'' |
    mib'''4. mib'''8 sib''4. sib''8 |
    sol''4. sol''8 mib''4. mib''8 |
    sib'1 |
  }
  \tag #'violino2 {
    sib''16 <sib' re'>\p q q \rt#4 q \rt#4 q \rt#4 q |
    <>-\sug\fp \rt#4 q \rt#8 <fa' re''> \rt#4 q |
    <do'' fa'>\ff <fa' la> q q \rt#4 q \rt#8 q |
    << { s4 s-\sug\p } { \rt#8 q16 } >> \rt#8 q |
    \rt#8 <do' mib'> \rt#8 <sib re'> |
    \rt#8 <do' la'?> \rt#8 <la' do'> |
    \rt#16 <do' la'?> |
    <sib' re'> <sib re'>-\sug\ff q q \rt#12 q |
    \rt#16 q |
    <>-\sug\p \rt#12 q \rt#4 sib' |
    \rt#16 <sol' sib'> |
    \rt#16 <mi' sol> |
    <>-\sug\f \rt#8 <lab fa'> \rt#8 <do' lab'> |
    \rt#8 <lab' sib> \rt#8 q |
    \rt#8 <sol' sib> \rt#8 q |
    \rt#8 <sol' sib> \rt#8 q |
    <sib fa'>1 |
  }
>>
r4 r8 sib' sib'4. sib'8 |
<>\fp \rt#8 <sol mib'>16 \rt#8 q |
\rt#8 <sol mib'>16 \rt#8 q |
<>\ff <<
  \tag #'violino1 {
    <lab mib'>2~ q4. <lab lab'>8 |
    q2 lab |
    do''8\p do''4 do'' do'' do''8~ |
    do'' do''4 do'' do'' do''8~ |
    do'' do''4 do''\cresc do'' do''8~ |
    do'' do''4 do''8 re''4. re''8\! |
    <<
      { s16 sol'' sol'' sol'' \rt#4 sol'' \rt#8 sol'' | } \\
      { mib''16\f mib'' mib'' mib'' \rt#4 mib'' \rt#8 mib'' }
    >>
    \rt#8 lab'' \rt#8 do''' |
  }
  \tag #'violino2 {
    \rt#8 <lab mib'>16 \rt#8 q |
    \rt#8 q \rt#8 q |
    sol'8-\sug\p sol'4 sol' sol' sol'8~ |
    sol' sol'4 sol' sol' sol'8 |
    lab' lab'4 lab'-\sug\cresc lab' lab'8~ |
    lab' lab'4 lab' lab' lab'8\! |
    sib'16-\sug\f <mib'' mib'> q q \rt#4 q \rt#8 q |
    \rt#8 q \rt#8 q |
  }
>>
<>\ff <<
  { \rt#8 sol''16 \rt#8 sol'' |
    \rt#8 sol'' \rt#8 fa'' |
    mib''1\fermata | } \\
  { \rt#8 mib''16 \rt#8 mib'' |
    \rt#8 mib'' \rt#8 re'' |
    sol'1 | }
>>
<<
  \tag #'violino1 {
    do''8\p do''4 do'' do'' do''8~ |
    do'' do''4 do'' do'' do''8~ |
    do'' do''4 do''\cresc do'' do''8~ |
    do'' do''4 do''8 re''4. re''8\! |
    <<
      { s16 sol'' sol'' sol'' \rt#4 sol'' \rt#8 sol'' | } \\
      { mib''16\f mib'' mib'' mib'' \rt#4 mib'' \rt#8 mib'' }
    >>
    \rt#8 lab'' \rt#8 do''' |
  }
  \tag #'violino2 {
    sol'8-\sug\p sol'4 sol' sol' sol'8~ |
    sol' sol'4 sol' sol' sol'8 |
    lab' lab'4 lab'-\sug\cresc lab' lab'8~ |
    lab' lab'4 lab' lab' lab'8\! |
    sib'16-\sug\f <mib'' mib'> q q \rt#4 q \rt#8 q |
    \rt#8 q \rt#8 q |
  }
>>
<<
  { \rt#8 sol''16 \rt#8 sol'' |
    \rt#8 sol'' \rt#8 fa'' | } \\
  { \rt#8 mib''16 \rt#8 mib'' |
    \rt#8 mib'' \rt#8 re'' | }
>>
mib''16\ff <mib' sol> q q \rt#4 q \rt#8 q |
\rt#8 q \rt#8 q |
\rt#8 q \rt#8 q |
\rt#8 q \rt#8 q |
<<
  \tag #'violino1 {
    <lab mib'>2~ q4. <lab lab'>8 |
    q2 lab |
    sol''8\mf sol''4 sol'' sol'' sol''8~ |
    sol'' sol''4 sol'' sol'' sol''8 |
    lab'' lab''4 lab''\cresc lab'' lab''8~ |
    lab''8 lab''4 lab'' lab'' lab''8\! |
    <>\ff \rt#8 sol''16 \rt#8 sol'' |
    \rt#8 lab'' \rt#8 do''' |
  }
  \tag #'violino2 {
    \rt#8 <lab mib'>16 \rt#8 q |
    \rt#8 q \rt#8 q |
    do''8-\sug\mf do''4 do'' do'' do''8~ |
    do'' do''4 do'' do'' do''8~ |
    do'' do''4 do''-\sug\cresc do'' do''8~ |
    do''8 do''4 do''8 re''4. re''8\! |
    <>-\sug\ff \rt#8 <mib'' mib'>16 \rt#8 q |
    \rt#8 q \rt#8 q |
  }
>>
<<
  { \rt#8 sol''16 \rt#8 sol'' |
    \rt#8 sol'' \rt#8 fa'' |
    mib''1\fermata | } \\
  { \rt#8 mib''16 \rt#8 mib'' |
    \rt#8 mib'' \rt#8 re'' |
    sol'1 | }
>>
<<
  \tag #'violino1 {
    do'''8\mf do'''4 do''' do''' do'''8~ |
    do''' do'''4 do''' do''' do'''8 |
    do''' do'''4\cresc do''' do''' do'''8~ |
    do'''8 do'''4 do'''8 re'''4. re'''8\! |
    <>\ff <<
      { \rt#8 mib'''16 \rt#8 mib''' |
        \rt#8 mib''' \rt#8 mib''' |
        \rt#8 mib''' \rt#8 mib''' |
        \rt#8 mib''' \rt#8 re''' |
        mib'''4 } \\
      { \rt#16 mib''16 |
        \rt#16 mib'' |
        \rt#16 sol'' |
        \rt#8 sol'' \rt#8 fa'' |
        mib''4 }
    >> r4 r2 |
    mib'2.\p( re'8) do' |
    sib2. sol'8. fa'16 |
    fa'4( sib'2\sf lab'4) |
    sol'4 r r2 |
    mib'2.\p( re'8) do' |
    sib2. sol'8. fa'16 |
    fa'4( sib'2\sf re'4) |
    sol'2 lab'4. sol'8 |
    sol'2 fa'4.\sf mib'8 |
    mib'2 lab'4. sol'8 |
    sol'2 fa'4.(\sf mib'8) |
    mib'1~ |
    mib' |
    mib'2 mib' |
    mib'2 r |
    R1*2 |
    mib'1\fp |
    re'4 r r2 |
    la''1\f |
    sib''2\p lab''!~ |
    lab'' sol''~ |
    sol'' lab''4 r |
    fa''2\f fa''4. fa''8 |
    mib''4 r r2 |
  }
  \tag #'violino2 {
    sol''8-\sug\mf sol''4 sol'' sol'' sol''8~ |
    sol'' sol''4 sol'' sol'' sol''8 |
    lab'' lab''4-\sug\cresc lab'' lab'' lab''8~ |
    lab'' lab''4 lab'' lab'' lab''8\! |
    <>-\sug\ff \rt#8 sol''16 \rt#8 sol'' |
    \rt#8 lab'' \rt#8 do''' |
    <<
      { \rt#8 sol'' \rt#8 sol'' |
        \rt#8 sol'' \rt#8 fa'' | } \\
      { \rt#8 mib'' \rt#8 mib'' |
        \rt#8 mib'' \rt#8 re'' | }
    >>
    mib''4 r r2 |
    do'2.-\sug\p( sib8) lab |
    sol2. mib'8. re'16 |
    re'2. re'4 |
    mib' r r2 |
    do'2.-\sug\p( sib8) lab |
    sol2. mib'8. re'16 |
    re'2. sib4 |
    mib'1~ |
    mib'2 re'4.-\sug\sf mib'8 |
    mib'1~ |
    mib'2 re'4.-\sug\sf mib'8 |
    mib'2 do'4. sib8 |
    sib2 do'4. sib8 |
    sib2 sib |
    sib2 r |
    R1*2 |
    do'1\fp |
    sib4 r r2 |
    re''1\f |
    re''\p~ |
    re''2 mib''~ |
    mib'' mib''4 r |
    re''2\f re''4. re''8 |
    mib''4 r r2 |
  }
>>
