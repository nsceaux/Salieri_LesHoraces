\clef "bass" r8 |
R1 |
r2 r4 r8 sib |
mib'4 sib r8 sol lab do' |
re4 mib r2 |
r r4 r8 mib |
do'2 r8 mib lab do' |
sib4 mib'8. sol16 lab4 sib8. sib16 |
mib2 r\fermata |
R1 |
r2 r4 r8 fa |
sib4. fa8 fa4. fa8 |
re'2. sib4 |
la la r2 |
r fa4 la |
do'2 re'4. re'8 |
mib'2 do'4. la8 |
fa2 mib4. mib8 |
re2 r |
r r4 fa |
sib4. sib8 sib4 sib |
do4. do8 mi4 sol |
do'2. sib4 |
lab2 lab |
r4 re' re' re' |
mib'2 sib4. sib8 |
sol2 mib4. mib8 |
sib,2 r |
r4 r8 sib sib4. sib8 |
mib2~ mib4. sib8 |
sib4. mib'8 mib'4. do'8 |
lab1 |
R1 |
do'2 do'4. do'8 |
do'2 do' |
do'1 |
r2 sib4. sib8 |
mib'1 |
do'2 lab |
sib1~ |
sib |
do\fermata |
do'2 do'4. do'8 |
do'2 do' |
do'1 |
r2 sib4. sib8 |
mib'1 |
do'2 lab |
sib1~ |
sib |
mib2 r |
R1*13 |
R1^\fermataMarkup |
R1*24 |
sib2 r4 sib |
sol2 sol8 r r do' |
la4^! r8 fa fa sol la!8. sib16 |
fa4^! r8 sib16 sib sib4 do'8 re' |
la4^! r8 la16 sib do'4 la8 sib |
sol8^! sol r16 sib sib do' lab!4^! r8 fa |
lab8 lab lab sib sol4^! r8 sib |
sib sib do' reb' do'4^! r8 do'16 do' |
fa2^! sib4. sib8 |
sol mib r4 r2 |
