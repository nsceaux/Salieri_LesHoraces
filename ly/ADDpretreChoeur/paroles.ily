\tag #'pretre {
  O Ro -- me ! ô ma pa -- tri -- e !
  Choi -- sis, on te pré -- sente ou le sceptre, ou des fers.
  E -- veil -- le ton puis -- sant gé -- ni -- e,
  Sou -- viens- toi que les Dieux t’ont pro -- mis l’u -- ni -- vers,
  L’es -- poir de tes en -- fans sur leurs dé -- crets se fon -- de,
  Ju -- pi -- ter même a ré -- glé ton des -- tin :
  Tes en -- ne -- mis __ t’at -- ta -- que -- ront en vain,
  Ro -- me doit être un jour la maî -- tres -- se du mon -- de.
  Ro -- me doit être un jour la maî -- tres -- se du mon -- de.
}
\tag #'(vsoprano valto vtenor vbasso) {
  Tes en -- ne -- mis __ t’at -- ta -- que -- ront en vain,
  Ro -- me doit être un jour la maî -- tres -- se du mon -- de.
  Ro -- me doit être un jour la maî -- tres -- se du mon -- de.
}

\tag #'pretre {
  Roi, Pon -- ti -- fes, Sé -- nat, ré -- u -- nis -- sez- vous tous :
  Que nos trois dé -- fen -- seurs soient nom -- més ce jour mê -- me !
  Sur le grand choix que Rome at -- tend de vous,
  Je vous pro -- mets des Dieux l’as -- sis -- tan -- ce su -- prê -- me.
}
