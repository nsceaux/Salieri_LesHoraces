\clef "vtaille" r8 |
R1*7 |
r2 r\fermata |
R1*32 |
R1^\fermataMarkup |
R1*9 |
r4 r8\f sib sib4. sib8 |
mib2~ mib4. sib8 |
sib4. mib'8 mib'4. do'8 |
lab2 r |
R1 |
do'2^\mf do'4. do'8 |
do'2 do' |
do'1^\markup\italic cresc |
r2 sib4. sib8 |
mib'1^\f |
do'2 lab |
sib1~ |
sib |
do\fermata |
do'2^\mf do'4. do'8 |
do'2 do' |
do'1 |
r2 sib4. sib8 |
mib'1^\f |
do'2 lab |
sib1~ |
sib |
mib4 r r2 |
R1*25 |
