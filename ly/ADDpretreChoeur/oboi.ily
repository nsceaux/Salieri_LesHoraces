\clef "treble" r8 |
R1 |
r2 r4 r8 sib'-\sug\mf |
mib''4 sib' r2 |
R1*2 |
<<
  \tag #'(oboe1 oboi) \new Voice { \tag #'oboi \voiceOne do''2~ do''8 }
  \tag #'(oboe2 oboi) \new Voice { \tag #'oboi \voiceTwo mib'2~ mib'8 }
>> r8 r4 |
r4 <>-\sug\p <<
  \tag #'(oboe1 oboi) \new Voice {
    \tag #'oboi \voiceOne
    sol''8. sol''16 fa''4 fa''8. fa''16 | mib''2
  }
  \tag #'(oboe2 oboi) \new Voice {
    \tag #'oboi \voiceTwo
    mib''8. mib''16 mib''4 re''8. re''16 | mib''2
  }
>> r2\fermata |
sib''2-\sug\ff fa''4 re'' |
sib'2~ sib'8 do''16 re'' mib'' fa'' sol'' la'' |
sib''4 r r2 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''8. fa''16 fa''4 sib'' |
    la'' fa''8. fa''16 fa''4 fa'' | fa''2 }
  { re''8. re''16 re''4 re'' |
    do''4 la'8. la'16 la'4 la' | la'2 }
  { s2.-\sug\p | s4 s2.-\sug\f | }
>> r2 |
<<
  \tag #'(oboe1 oboi) \new Voice {
    \tag #'oboi \voiceOne
    mib''2 re'' | do''1~ | do'' | re''2
  }
  \tag #'(oboe2 oboi) \new Voice {
    \tag #'oboi \voiceTwo
    do''2 sib' | la'1~ | la' | sib'2
  }
>> r2 |
R1*2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''1~ | sol'' | lab''4 lab''8. lab''16 lab''4 lab'' |
    lab''1 | sol''1~ | sol'' | fa'' | }
  { mi''1~ | mi'' | fa''4 do''8. do''16 do''4 do'' |
    fa''1 | mib''1~ | mib'' | re'' | }
  { s1-\sug\p | s | s-\sug\f }
>>
R1*5 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''1~ |
    do'' |
    do''~ |
    do''2 re'' |
    mib'' sol'' |
    lab'' do''' |
    sol''1~ |
    sol''2 fa'' |
    mib''1\fermata | }
  { sol'1~ |
    sol' lab'~ |
    lab'2 lab' |
    sol' mib''~ |
    mib'' mib'' |
    mib''1~ |
    mib''2 re'' |
    sol'1\fermata | }
  { s1-\sug\p | s | s4. s8-\sug\cresc s2 | s1 | s\!-\sug\f | s | s-\sug\ff }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''1~ | do'' | do''~ | do''2 re'' |
    mib'' sol'' | lab'' do''' | sol''1~ | sol''2 fa'' | }
  { sol'1~ | sol' | lab'~ | lab'1 |
    sol'2 mib''~ | mib''1 | mib''1~ | mib''2 re'' | }
  { s1-\sug\p | s | s4. s8-\sug\cresc s2 | s1 | s-\sug\f }
>>
mib''1-\sug\ff~ |
mib''2 sib' |
mib'1~ |
mib'~ |
mib' |
mib' |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''1~ | sol'' | lab''~ | lab'' |
    sol'' | lab''2 do''' | sol''1~ | sol''2 fa'' |
    mib''1\fermata | sol''1~ | sol'' | lab''~ |
    lab'' | sol'' | lab''2 lab'' | sol''1~ |
    sol''2 fa'' | mib''4 }
  { do''1~ | do'' | do''~ | do''2 re'' |
    mib''1 | mib'' | mib''~ | mib''2 re'' |
    sol'1\fermata | do''1~ | do'' | do''~ |
    do''2 re'' | mib''1~ | mib'' | mib''~ |
    mib''2 re'' | mib''4 }
  { s1-\sug\mf | s1 | s-\sug\cresc | s | s\!-\sug\ff | s1*4 |
    s1-\sug\mf | s | s-\sug\cresc | s | s-\sug\ff }
>> r4 r2 |
R1 |
r2 r4 <>\p
<<
  \tag #'(oboe1 oboi) \new Voice {
    \tag #'oboi \voiceOne
    sol''8. fa''16 | fa''4 sib''2\sf lab''4 | sol''
  }
  \tag #'(oboe2 oboi) \new Voice {
    \tag #'oboi \voiceTwo
    mib''8. re''16 | re''2. re''4 | mib''4
  }
>> r4 r2 |
R1 |
r2 r4 <<
  \tag #'(oboe1 oboi) \new Voice {
    \tag #'oboi \voiceOne
    sol''8. fa''16 | fa''4 sib''2-\sug\sf re''4 | sol''4
  }
  \tag #'(oboe2 oboi) \new Voice {
    \tag #'oboi \voiceTwo
    mib''8. re''16 | re''2. sib'4 | mib''4
  }
>> r4 r2 |
r <>-\sug\sf <<
  \tag #'(oboe1 oboi) \new Voice {
    \tag #'oboi \voiceOne
    fa''4. mib''8 |
  }
  \tag #'(oboe2 oboi) \new Voice {
    \tag #'oboi \voiceTwo
    re''4. mib''8 |
  }
>>
mib''1~ |
mib''2 <>-\sug\sf <<
  \tag #'(oboe1 oboi) \new Voice {
    \tag #'oboi \voiceOne
    fa''4. mib''8 | mib''2 do''4. sib'8 |
    sib'2 do''4. sib'8 | sib'2 sib' | sib'
  }
  \tag #'(oboe2 oboi) \new Voice {
    \tag #'oboi \voiceTwo
    re''4. mib''8 | mib''2 lab'4. sol'8 |
    sol'2 lab'4. sol'8 | sol'2 sol' | sol'
  }
>> r2 |
R1*8 | <>\f
<<
  \tag #'(oboe1 oboi) \new Voice {
    \tag #'oboi \voiceOne
    fa''2 fa''4. fa''8 | mib''4
  }
  \tag #'(oboe2 oboi) \new Voice {
    \tag #'oboi \voiceTwo
    re''2 re''4. re''8 | mib''4
  }
>> r4 r2 |
