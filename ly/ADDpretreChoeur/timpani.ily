\clef "bass" r8 |
R1*7 |
r2 r\fermata |
R1*32 |
R1^\fermataMarkup |
R1*8 |
<>\ff \rt#8 mib16 \rt#8 mib |
mib2 r |
r2 r4 r8 sib, |
sib,4. sib,8 sib,4. sib,8 |
mib4 mib8. mib16 mib4 mib |
mib2 r |
R1*3 |
r2 sib,4. sib,8 |
\rt#16 mib16 |
\rt#16 mib16 |
\rt#16 sib, |
\rt#16 sib, |
mib1\fermata |
R1*3 |
r2 sib,4. sib,8\! |
\rt#16 mib16 |
\rt#16 mib16 |
\rt#16 sib, |
\rt#16 sib, |
mib4 r r2 |
R1*25 |
