\clef "bass" sol,16\f la,32 si, |
do8. re16 re8.\prall do32 re mib4. si,16 do32 re |
mib4. mib32 fa sol lab sib4. sib16\mf do'32 re' |
mib'4 sib r8 sol( lab do') |
re4( mib) sib,4.\f lab,8 |
sol,4 r r2 |
r8 lab,16. lab,32 do8-. mib-. lab2 |
sol4 r8. sol16-\sug\p lab4 sib8. sib,16 |
mib2 r\fermata |
sib2\ff fa4 re |
sib,2 <<
  \tag #'(fagotto1 fagotto2 fagotti) {
    sib,8 do16 re mib fa sol la |
    sib2 r |
    r4 sib8.-\sug\p sib16 sib4 sib, |
    fa fa8.-\sug\f fa16 fa4 fa |
    fa2 r |
    R1*3 |
    r2 r4
  }
  \tag #'basso {
    r2 |
    \rt#4 sib,8 <>\p \rt#4 sib, |
    <>\fp \rt#4 sib, \rt#4 sib, |
    <>\ff \rt#4 fa \rt#4 fa |
    << { s4 s\p } { \rt#4 fa8 } >> \rt#4 fa |
    \rt#4 fa \rt#4 fa |
    \rt#4 fa \rt#4 fa |
    \rt#4 fa \rt#4 fa |
    sib,2.
  }
>> fa8.\ff fa16 |
re4 sib, re fa |
sib2.\p reb4 |
do1~ |
do4 do8. do16 do4 do |
fa\f fa8. fa16 fa4 fa |
sib, re' re' re' |
mib'2 sib4. sib8 |
sol2 mib4. mib8 |
sib,1 |
r2 r4 r8 sib,16\ff do |
reb1~ |
reb\p |
do\f~ |
do |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    mi1-\sug\p~ | mi | << fa1~ { s4. s8-\sug\cresc s2 } >> | fa2 sib, |
  }
  \tag #'basso {
    mi4\p mi mi mi |
    mi mi mi mi |
    fa fa fa\cresc fa |
    fa fa sib, sib, |
  }
>>
mib2\f mib' |
do' lab |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    sib1-\sug\ff~ | sib | do,\fermata |
    mi1-\sug\p~ | mi | << fa1~ { s4. s8-\sug\cresc } >> | fa2 sib, |
  }
  \tag #'basso {
    sib4\ff sib sib sib |
    sib sib sib, sib, |
    do1\fermata |
    mi4\p mi mi mi |
    mi mi mi mi |
    fa fa fa\cresc fa |
    fa fa sib, sib, |
  }
>>
mib2\f mib' |
do' lab |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    sib1~ | sib2 sib, |
  }
  \tag #'basso {
    sib4 sib sib sib | sib sib sib, sib, |
  }
>>
mib2 r |
r2 r4 r8 sib,16\ff do |
reb1~ |
reb |
do~ |
do |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    mi1-\sug\mf~ | mi | fa-\sug\cresc | fa |
  }
  \tag #'basso {
    mi4\mf mi mi mi |
    mi mi mi mi |
    fa fa\cresc fa fa |
    fa4 fa sib,4. sib,8 |
  }
>>
mib2\ff mib' |
do' lab |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    sib1~ | sib | do,\fermata |
    mi1-\sug\mf~ | mi | fa-\sug\cresc | fa2
  }
  \tag #'basso {
    \rt#4 sib8 \rt#4 sib |
    \rt#4 sib8 \rt#4 sib |
    do1\fermata |
    mi4-\sug\mf mi mi mi |
    mi mi mi mi |
    fa fa\cresc fa fa |
    fa4 fa
  }
>> sib,4. sib,8 |
mib2\ff mib' |
do' lab |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    sib1~ | sib2 sib, |
  }
  \tag #'basso {
    \rt#4 sib8 \rt#4 sib |
    \rt#4 sib \rt#4 sib, |
  }
>>
mib4 r r2 |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    R1 |
    \clef "tenor" r2 r4 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { sol'8. fa'16 | fa'1 | sol'4 }
      { mib'8. re'16 | re'1 | mib'4 }
      { s4-\sug\p | }
    >> r4 r2 |
    R1 |
    r2 r4 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { sol'8. fa'16 | fa'1 | sol2 }
      { mib'8. re'16 | re'1 | mib2 }
    >> r2 |
    r2 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { sib2 | sib }
      { lab4.-\sug\sf sol8 | sol2 }
    >> r2 |
    r \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { sib2 | sib do'4. sib8 | sib2 do'4. sib8 | sib2 sib | sol2 }
      { lab4.-\sug\sf sol8 | sol2 lab4. sol8 | sol2 lab4. sol8 |
        sol2 sol | mib2 }
    >> r2 |
    R1*8 |
    \clef "bass" sib,2\f sib,4. sib,8 |
    mib4 r r2 |
  }
  \tag #'basso {
    r4 lab,8.\p( lab,16 lab,4 lab,) |
    mib1 |
    r4 sib,\mf sib, sib, |
    mib r r2 |
    r4 lab,8.\p lab,16 lab,4 lab, |
    mib2 r |
    r4 sib,\mf sib, sib, |
    mib1~ | mib~ | mib~ | mib~ |
    mib~ | mib~ | mib~ |
    mib2 r |
    R1*2 |
    fa1\fp |
    sib,4 r r2 |
    fad1\f |
    sol2\p fa!~ |
    fa mib~ |
    mib lab,4 r |
    sib,2\f sib,4. sib,8 |
    mib4 r r2 |
  }
>>
