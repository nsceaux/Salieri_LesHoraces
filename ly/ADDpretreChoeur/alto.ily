\clef "alto" sol16\ff la32 si |
do'8. re'16 re'8.\prall do'32 re' mib'4. si16 do'32 re' |
mib'4. mib'32 fa' sol' lab' sib'4. sib'16\mf do''32 re'' |
mib''4 sib' r8 sol'( lab' do'') |
re'4( mib') sib4.\f lab8 |
sol4 r r2 |
r8 lab16. lab32 do'8-. mib'-. lab'2 |
sol'4 r8. sol'16-\sug\p lab'4 sib'8. sib16 |
mib'2 r\fermata |
sib'2-\sug\ff fa'4 re' |
sib2 sib8 do'16 re' mib' fa' sol' la' |
sib'16 fa'-\sug\p fa' fa' \rt#4 fa' \rt#4 fa' \rt#4 fa' |
<>-\sug\fp \rt#4 fa' \rt#8 sib' \rt#4 sib |
fa'16-\sug\ff <do' la'?> <do' la'> q \rt#4 q \rt#8 q |
<< { s4 s-\sug\p } { \rt#8 <do' la'?>16 } >> \rt#8 <do' la'> |
\rt#8 <do' mib'> \rt#8 <sib re'> |
\rt#16 <la fa'> |
\rt#16 <la fa'> |
sib <sib fa'>-\sug\ff q q \rt#4 q \rt#8 q |
\rt#16 q |
<>-\sug\p \rt#16 q |
\rt#16 mi' |
\rt#16 do' |
<>-\sug\f \rt#16 do' |
\rt#16 <re' fa'> |
\rt#8 mib' \rt#8 mib' |
\rt#8 mib' \rt#8 mib' |
re'1 |
R1 |
<>-\sug\fp \rt#8 <sib mib>16 \rt#8 q |
\rt#8 q \rt#8 q |
<>-\sug\ff \rt#8 <mib do'> \rt#8 q |
\rt#8 q \rt#8 q |
do'8-\sug\p do'4 do' do' do'8~ |
do' do'4 do' do' do'8~ |
do' do'4 do'-\sug\cresc do' \once\tieDashed do'8~ |
do' fa'4 fa' fa' fa'8\! |
mib'1-\sug\f |
do'2 lab |
<>-\sug\ff \rt#4 sib8 \rt#4 sib |
\rt#4 sib8 \rt#4 sib |
do1\fermata |
do'8-\sug\p do'4 do' do' do'8~ |
do' do'4 do' do' do'8~ |
do' do'4 do'-\sug\cresc do' do'8~ |
do' fa'4 fa' fa' fa'8\! |
mib2-\sug\f mib' |
do'2 lab |
sib4 sib sib sib |
sib sib sib sib |
<>-\sug\ff \rt#8 <mib sib>16 \rt#8 q |
\rt#8 q \rt#8 q |
\rt#8 q \rt#8 q |
\rt#8 q \rt#8 q |
\rt#8 <mib do'> \rt#8 q |
\rt#8 q \rt#8 q |
sol'8-\sug\mf sol'4 sol' sol' sol'8~ |
sol' sol'4 sol' sol' sol'8 |
fa' fa'4 fa'-\sug\cresc fa' fa'8~ |
fa' fa'4 fa' fa' fa'8\! |
mib2-\sug\ff mib' |
do' lab |
sib16 sib' sib' sib' \rt#4 sib' \rt#8 sib' |
\rt#8 sib' \rt#8 sib' |
do'1\fermata |
do''8-\sug\mf do''4 do'' do'' do''8~ |
do'' do''4 do'' do'' do''8 |
do'' do''4-\sug\cresc do'' do'' do''8~ |
do'' do''4 do''8 sib'4. sib'8\! |
mib2-\sug\ff mib' |
do' lab |
\rt#8 sib16 \rt#8 sib |
\rt#8 sib \rt#8 sib |
mib'4 r r2 |
r4 lab8.\p( lab16 lab4 lab) |
mib'1 |
r4 sib\mf sib sib |
mib' r r2 |
r4 lab8.\p lab16 lab4 lab |
mib'2 r |
r4 sib\mf sib sib |
<< sol2 \\ mib >> do'4. sib8 |
sib2 lab4.-\sug\sf sol8 |
sol2 do'4. sib8 |
sib2 lab4.-\sug\sf sol8 |
sol2 lab4. sol8 |
sol2 lab4. sol8 |
sol2 sol |
sol2 r |
R1*2 |
la1\fp |
sib4 r r2 |
re'1\f |
sib'-\sug\p~ |
sib'2 sib'~ |
sib' do''4 r |
sib2\f sib'4. sib'8 |
sol'4 r r2 |

