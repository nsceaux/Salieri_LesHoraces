\clef "vbas-dessus" r8 |
R1*7 |
r2 r\fermata |
R1*32 |
R1^\fermataMarkup |
R1*9 |
r4 r8\f sib' sib'4. sib'8 |
mib'2~ mib'4. sib'8 |
sib'4. mib''8 mib''4. do''8 |
lab'2 r |
R1 |
do''2^\mf do''4. do''8 |
do''2 do'' |
do''1^\markup\italic cresc |
r2 sib'4. sib'8 |
mib''1^\f |
do''2 lab' |
sib'1~ |
sib' |
do'1\fermata |
do''2^\mf do''4. do''8 |
do''2 do'' |
do''1 |
r2 sib'4. sib'8 |
mib''1^\f |
do''2 lab' |
sib'1~ |
sib' |
mib'4 r r2 |
R1*25 |
