\tag #'all \key mib \major \tempo "Andante maestoso"
\midiTempo#108 \time 4/4 \partial 8 s8 s1*8 \bar "|."
\tempo "Allegro assai" \midiTempo#160 s1*64
\tempo "Ralentissez le double" \bar "||" \time 4/4
\midiTempo#80 s1*26 \bar "|."