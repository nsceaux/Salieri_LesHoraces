\clef "bass" do4 r r2 |
re1 |
mib4 r r2 |
R1 |
r2 sol\fp~ |
sol sol~ |
sol lab4 r |
r2 fa\f~ |
fa r4 sol |
