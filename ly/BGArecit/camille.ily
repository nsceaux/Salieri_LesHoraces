\clef "vbas-dessus" do''8^! do'' r4 r2 |
R1*2 |
r2 r4 r8 sib' |
sol'4 sol'8 r mib''4. sib'8 |
sib'?4 sib'8 do'' reb'' reb'' r reb''16 sib' |
mib''4 mib''8 sib'? do''4 r |
R1*2 |
