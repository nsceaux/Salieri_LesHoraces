\clef "treble"
<<
  \tag #'violino1 {
    mib''4 r r2 |
    sib'1~ |
    sib'4 r r2 |
    R1 |
    r2 sib''\fp~ |
    sib''1~ |
    sib''2 do'''4 r |
  }
  \tag #'violino2 {
    sol''4\repeatTie r r2 |
    fa'1( |
    sol'4) r r2 |
    R1 |
    r2 <>-\sug\fp mib''~ |
    mib''1~ |
    mib''2~ mib''4 r4 |
  }
>>
r2 <>\f <re' si'!>2~ |
q <<
  \tag #'violino1 { r4 <sol re' si'> | }
  \tag #'violino2 \new CueVoice { r4 <sol re' si'> | }
>>
