\clef "alto" do''4 r r2 |
sib1~ |
sib4 r r2 |
R1 |
r2 <>-\sug\fp sib'?~ |
sib'1~ |
sib'2 la'4 r4 |
r2 <<
  \tag #'conducteur \new CueVoice { fa'2\f~ | fa' r4 sol' | }
  \tag #'part { fa'2\f~ | fa' r4 sol' | }
>>

