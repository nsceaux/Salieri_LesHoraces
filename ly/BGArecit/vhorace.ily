\clef "vbasse" r2 r4 r8 do'16 do' |
sib4^! r8 fa16 sol lab4 lab8 sib |
sol4 r sib4. sib8 |
sol4 sol8 sib mib4 r |
R1*2 |
r2 r4 r8 lab16 sib |
do'4 do'8 re' si!4^! r8 sol |
si si si do' sol4 r |
