\tag #'camille {
 l’au -- tre.
}
\tag #'jhorace {
  Mon pè -- re !…
}
\tag #'vhorace {
  Mes en -- fans, il est temps de par -- tir.
  Vous, Ca -- mil -- le, ren -- trez.
}
\tag #'camille {
  Mon pè -- re !
  Quoi ! sur vous la na -- tu -- re a si peu de pou -- voir ?
}
\tag #'vhorace {
  La na -- tu -- re se taît où par -- le le de -- voir.
}
