\clef "bass" sib4\f sib,8. re16 fa4. fad8 |
sol8( sib sol re) mib2\fermata |
mib'4 mib8. sol16 sib4. <<
  \twoVoices #'(fagotto1 fagotto2 fagotti) <<
    { sib16. si32 | si8( si si si) do'2\fermata | }
    { sol16. fa32 | fa8( fa fa fa) mib2\fermata | }
    { <>-\sug\p }
  >>
  \tag #'basso {
    mib16.\p re32 |
    re8( re re re) do2\fermata |
  }
>>
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    do'2-\sug\f sib4 mib' |
    lab8\p( sib do' la) sib2 |
    sib?1~ |
    sib2 sib-\sug\fp~ |
    sib1~ |
    sib2 lab4\f
    
  }
  \tag #'basso {
    do2\f sib,4 mib |
    lab,8\p( sib, do la,) sib,2~ |
    sib,1~ |
    sib,2 mib-\sug\fp~ |
    mib1~ |
    mib2 lab,4\f
  }
>> lab,8. do16 |
mib4. mib8 lab do'4 lab16. fa32 |
mi!1~ |
mi~ |
mi~ |
mi |
fa8 r32 do\f re mi! fa16. sol32 lab16. do32 <<
  \tag #'(fagotto1 fagotto2 fagotti) {
    si,!2 |
    R1*8 |
    r4 la-.(\p la-. la-.) |
    fa2.\cresc dod4 |
    re\! fa2 fad4\p |
    sol sol,8. sol,16 sol,4 sol, |
    sol,2
  }
  \tag #'basso {
    si,!2~ |
    si,1~ |
    \footnoteHere #'(0 . 0) \markup\wordwrap {
      Basses, mes. 18 : la source contient un \italic do.
    } si, |
    do |
    fad |
    sol |
    << \modVersion dod1 \origVersion { dod2~ dod } >> |
    re1~ |
    re2 r4 mi |
    la, r r2 |
    R1*3 |
    r2
  }
>>