\clef "treble" \transposition mib
<>-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { sol''4 }
  { sol'4 }
>> sol'8. sol'16 re''4. r8 |
r2 \twoVoices #'(corno1 corno2 corni) <<
  { do''2\fermata |
    do''4 do''8. mi''16 sol''4. mi''16. mi''32 |
    mi''8( mi'' mi'' mi'') mi''2\fermata |
    do''2 re''4. mi''8 |
    re''8 re''4 re''8 re''2 | }
  { mi'2\fermata |
    do'4 do'8. mi'16 sol'4. do''16. re''32 |
    re''8( re'' re'' re'') do''2\fermata |
    do''2 sol'4. do''8 |
    do''8 do''4 do''8 sol'2 | }
  { s2 s2. s8 s-\sug\p s1 s1-\sug\f <>\p }
>>
R1*22 |
r2
