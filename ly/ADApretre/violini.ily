\clef "treble" sib'4\f sib8. re'16 fa'4~ fa'16. fad'32 fad'16[\trill mi'32 fad'] |
sol'8( sib' sol' re') <sol mib'>2\fermata |
<mib' mib''>4 mib'8. sol'16 sib'4. <<
  \tag #'violino1 {
    sib'16.\p si'32 |
    si'8-.( si'-. si'-. si'-.) do''2 |
    lab'4\f lab8. lab'16 lab'4. sol'8 |
    fa'8\p fa'4 fa'8 fa'2~ |
    fa'1~ |
    fa'2 sol'\fp~ |
    sol'1~ |
    sol'2 lab'4\f
  }
  \tag #'violino2 {
    <sol sol'>16.-\sug\p <sol fa'>32 |
    <sol fa'>8( q q q) <sol mib'>2 |
    mib'2-\sug\f re'4. mib'8 |
    mib'8-\sug\p mib'4 mib'8 re'2~ |
    re'1~ |
    re'2 reb'-\sug\fp~ |
    reb'1~ |
    reb'2 do'4\f
  }
>> lab8. do'16 |
mib'4~ mib'16. mib'32 \grace fa'32 mib'16[ re'32 mib'] lab'8 do''4 lab'16. fa'32 |
<<
  \tag #'violino1 {
    do''1~ | do'' | <sol' sib'>~ | q | lab'8
  }
  \tag #'violino2 {
    sol'1 | sol' | reb'1 | do' | do'8
  }
>> r32 do'\f re' mi'! fa'16. sol'32 lab'16. do'32
<<
  \tag #'violino1 {
    < \tweak duration-log #2 re'
    \tweak duration-log #2 si'! sol''~ >2 |
    sol''1~ |
    sol''~ |
    sol'' |
    la'' |
    sib'' |
    << \modVersion la''1 \origVersion { la''2~ la'' } >> |
    la''1 |
    sold''2 r4
  }
  \tag #'violino2 {
    re''2~ |
    re''1~ |
    re'' |
    mib'' |
    re'' |
    re'' |
    << \modVersion mi'' \origVersion { mi''2~ mi'' } >> |
    fa''1 |
    mi''2 r4
  }
>> <sold''! si' mi'>4 |
<mi' do'' la''>4 r r2 |
R1*3 |
r2
