\clef "alto" sib'4\f sib8. re'16 fa'4. fad'8 |
sol'( sib' sol' re') mib'2\fermata |
mib''4 mib'8. sol'16 sib'4. mib'16.\p re'32 |
re'8( re' re' re') do'2\fermata |
do'2\f sib4 mib' |
lab8(\p sib do' la) sib2~ |
sib1~ |
sib2 mib'-\sug\fp~ |
mib'1~ |
mib'2 lab4\f lab8. do'16 |
mib'4. mib'8 lab' do''4 lab'16. fa'32 |
mi'!1~ |
mi'~ |
mi'~ |
mi'1 |
fa'8 r32 do'\f re' mi'! fa'16. sol'32 lab'16. do'32 sol'2~ |
sol'1~ |
sol' |
do'~ |
do'1 |
sib |
<< \modVersion mi'1 \origVersion { mi'2~ mi' } >> |
re'1 |
si'!2 r4 mi' |
la r r2 |
R1*3 |
r2
