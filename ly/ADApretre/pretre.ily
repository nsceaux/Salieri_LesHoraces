\clef "bass" R1*5 |
r2 r4 r8 fa16 fa |
sib4. sib16 sib fa4 fa8 sol |
lab4 lab8 sib sol^! sol r sib16 sib |
reb'4 sib?8 lab sol4 r8 sol |
sib sib sib do' lab4^! r |
R1 |
r2 sol4 sol8 sol |
do'4. do'16 do' do'4 sol8 lab |
sib^! sib r sib sib sib do' reb' |
sol4^! r8 sol sib sol sol lab |
fa4^! r r2 |
sol4. sol8 si4 r8 re'16 re' |
si4 sol8 la si4 si8 do' |
sol^! sol r sol16 sol do'4 do'8 do' |
la4^! r8 la do' la la sib |
sol4^! r r8 sib sib sib |
la4^! la8 la dod'4 dod'8 la |
re'4^! r re'8 re' re' si! |
sold4^! sold8 la mi mi^! r4 |
R1*4 |
r2
