Le Sé -- nat, ras -- sem -- blé sous ces vou -- tes sa -- cré -- es,
Va choi -- sir trois Hé -- ros pour ê -- tre nos ven -- geurs :
Puis -- sent nos voix, par les Dieux ins -- pi -- ré -- es,
Nom -- mer à cet é -- tat de di -- gnes dé -- fen -- seurs !
Vous, Ro -- mains, à nos vœux u -- nis -- sez vos pri -- è -- res,
Le sa -- lut de l’E -- tat dé -- pend de ce grand choix :
Pri -- ez les Dieux pro -- tec -- teurs de nos loix,
De ver -- ser sur nous leurs lu -- miè -- res.
