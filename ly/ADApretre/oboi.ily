\clef "treble" R1*24 |
r4 <<
  \tag #'(oboe1 oboi) \new Voice {
    \tag #'oboi \voiceOne
    do''4-.(\p do''-. do''-.) |
    re''2.(\cresc mi''4 |
    fa''\! re''2) do''4\p |
    do''1( |
    si'2)
  }
  \tag #'(oboe2 oboi) \new Voice {
    \tag #'oboi \voiceTwo
    mi'4-.(-\tag #'oboe2 -\sug\p mi' mi') |
    la'2.-\tag #'oboe2 -\sug\cresc la'4~ |
    la'-\tag #'oboe2 \! la'2 la'4 |
    re'1~ |
    re'2
  }
>>
