\clef "soprano/treble" R1*8 |
<>^\markup\character Camille
mib''2 mib''8 mib'' mib'' mib'' |
do''4. do''8 do'' do'' reb'' mib'' |
lab'4^! r r2 |
R1*27 |
\stopStaff