\clef "alto/G_8" R1*5 |
<>^\markup\character { Le jeune Horace }
r4 r8 sib mib'4 mib'8 r |
R1*10 |
r4 r8 sib sib sib sib mib' |
re'4^! r8 re'16 re' re'4 re'8 la |
sib8^! sib r4 r2 |
R1*6 |
r4 r8 do' lab lab r4 |
do'8 do'16 do' do'8 re' si4^! r16 re' si sol |
do'8^! do' r4 r r8 sol |
do' do' re' mi' si^! si r si16 do' |
re'4 si8 do' la4^! r |
R1*8 |
\stopStaff
