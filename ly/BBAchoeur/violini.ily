\clef "treble" mib''8\p sib' sol' sib' mib'' sib' sol' sib' |
mib'' sib' sol' sib' mib'' sib' sol' sib' |
fa'' re'' sib' re'' fa'' re'' sib' re'' |
sol'' sib' mib'' sol'' sib'' sol'' mib'' sib' |
sib'' sol'' mib'' sib' sol'' mib'' sib' sol' |
<mib' sol>2 r |
<<
  \tag #'violino1 {
    \grace sib4 <<
      { mib''1~ | mib''2 do''4 } \\
      { sol'1\f~ | sol'2 mib'4 }
    >>
  }
  \tag #'violino2 <<
    { sib'1~ | sib'2 do''4 } \\
    { sol'1-\sug\f~ | sol'2 lab'4 }
  >>
>> r4 |
R1*3 |
<<
  \tag #'violino1 {
    <do' lab' mib''>4\f mib''8 mib'' fa''4 sol'' |
    <mib' do'' lab''>2 do'''4. reb'''8 |
    mib'''2 mib'''4. mib'''8 |
    lab''2
  }
  \tag #'violino2 {
    <lab mib' do''>4-\sug\f do''8 do'' reb''4 reb'' |
    <lab mib' do''>2 do''4. reb''8 |
    mib''2 mib''4. mib''8 |
    lab'2
  }
>> r2 |
r8 r16 <>_\markup\italic { Sempre forte } <<
  { fa''16 fa''4 } \\ { sib'16 sib'4 }
>> r2 |
r4 <mib' sib' sol''>4
<<
  \tag #'violino1 <mib' sib' sib''>4 
  \tag #'violino2 <mib' sol>4
>> r4 |
r8 r16 << { la''16 la''4 } \\ { do''16 do''4 } >> r2 |
r4 <<
  \tag #'violino1 {
    re''8. re''16 re''4 re'' |
    re''4. re''8 re''4 re'' |
    mib''4 sib'8. sib'16 sib'4 mib'' |
    fa''4 fa''8. fa''16 <<
      { sib''4 sib''4 } \\ { sib'4 sib' }
    >>
  }
  \tag #'violino2 {
    sib'8. sib'16 sib'4 sib' |
    sib'4. sib'8 lab'!4 lab' |
    sol'4 sol'8. sol'16 sol'4 sol' |
    sib'4 sib'8. sib'16 <<
      { fa''4 fa'' } \\ { sib'4 sib' }
    >>
  }
>>
<< sol''4 \\ sib' >> <mib' sol>8. q16 q4 q |
q2 r |
r8. << { sol''16 sol''4 } \\ { do''16 do''4 } >> r2 |
r8. << { lab''16 lab''8. lab''16 lab''4 }
  \\ { do''16 do''8. do''16 do''4 } >> r4 |
r2 r8. << { sol''16 sol''4 } \\ { si'16 si'4 } >> |
r8. << { sol''16 sol''8. sol''16 sol''4 }
  \\ { do''16 do''8. do''16 do''4 } >> r4 |
r2 r8. <<
  \tag #'violino1 << { si''16 si''4 } \\ { si'16 si'4 } >>
  
  \tag #'violino2 << { mi''16 mi''4 } \\ { si'16 si'4 } >>
>>
r2 r4 <mi' do'' la''>4 |
la! r r2 |
r8 r16 <<
  \tag #'violino1 << { dod'''16 dod'''4 } \\ { mi''16 mi''4 } >>
  \tag #'violino2 << { la''16 la''4 } \\ { dod''16 dod''4 } >>
>> r2 |
<<
  \tag #'violino1 {
    re'8 re'' re'' re'' \rt#4 re'' |
    mi''2 mi'4. mi'8 |
    mi'4. sold'16 si' mi''4 mi'' |
    do'' la' do'' mi'' |
    fa''4. fa''8 re''4. re''8 |
    <re' si' sol''>2 q4 q |
  }
  \tag #'violino2 {
    re'8 la' la' la' \rt#4 la' |
    <<
      { \rt#4 si' \rt#4 si' |
        \rt#4 si' \rt#4 si' |
        \rt#4 do'' \rt#4 do'' | } \\
      { \rt#4 sold' \rt#4 sold' |
        \rt#4 mi' \rt#4 mi' |
        \rt#4 mi' \rt#4 mi' | }
    >>
    \rt#4 si' \rt#4 si' |
    \rt#4 <re' si'> \rt#4 q |
  }
>>
\stopStaff
