<<
  \tag #'fagotti {
    \clef "tenor"
    mib'2-\sug\p~ mib' |
    mib'~ mib' |
    re'8 sib re' sib re' sib re' sib |
    mib'2~ mib' |
    mib'~ mib' |
    mib'2
  }
  \tag #'basso {
    \clef "bass"
    <>\p \rt#4 mib8 \rt#4 mib |
    \rt#4 mib8 \rt#4 mib |
    re8 sib, re sib, re sib, re sib, |
    \rt#4 mib8 \rt#4 mib |
    \rt#4 mib8 \rt#4 mib |
    mib2
  }
>> r2 |
mib1\f~ |
mib2 lab4 r |
R1*3 |
lab4\f lab8 lab lab4 lab |
lab2 <<
  \tag #'fagotti {
    do'4. reb'8 |
    mib'2 mib'4. mib'8 |
    lab2 r |
    R1*3 |
  }
  \tag #'basso {
    do4. reb8 |
    mib2 mib4. mib8 |
    lab,2 r |
    r8 r16 re!_\markup\italic { Sempre forte } re4 r2 |
    r4 mib mib r |
    r8 r16 fad fad4 r2 |
  }
>>
r4 sol8. sol16 sol4 sol |
sol4. sol8 fa4 fa |
mib mib8. mib16 mib4 mib |
re re re re |
mib4 mib8. mib16 mib4 mib |
mib2 r |
<<
  \tag #'fagotti { R1*8 }
  \tag #'basso {
    r8. mi16 mi4 r2 |
    r8. fa16 fa8. fa16 fa4 r |
    r2 r8. fa16 fa4 |
    r8. mi16 mi8. mi16 mi4 r |
    r2 r8. sold16 sold4 |
    r2 r4 la4 |
    la,! r r2 |
    r8. sol!16 sol4 r2 |
  }
>>
\rt#4 fa8 \rt#4 fa8 |
\rt#4 mi \rt#4 mi |
\rt#4 sold \rt#4 sold |
\rt#4 la, \rt#4 la |
\rt#4 sol! \rt#4 sol |
\rt#4 sol \rt#4 sol |
\stopStaff
