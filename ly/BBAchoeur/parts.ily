\piecePartSpecs
#`((corno1 #:tag-global () #:instrument "Corno I en mi♭")
   (corno2 #:tag-global () #:instrument "Corno II en mi♭")
   (flauto1)
   (flauto2)
   (oboe1)
   (oboe2)
   (clarinetto1 #:score-template "score-clarinette-sib"
                #:notes "oboi" #:tag-notes oboe1)
   (clarinetto2 #:score-template "score-clarinette-sib"
                #:notes "oboi" #:tag-notes oboe2)
   (fagotto1 #:tag-notes fagotti)
   (fagotto2 #:tag-notes fagotti)
   (violino1)
   (violino2)
   (alto)
   (basso #:instrument "Basso")
   (silence #:on-the-fly-markup , #{ \markup\tacet#38 #}))
