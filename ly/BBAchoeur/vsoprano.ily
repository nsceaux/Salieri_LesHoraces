\clef "vbas-dessus" <>^\p mib''2 mib''4. mib''8 |
mib''2. mib''4 |
fa''2. fa''4 |
sol''1~ |
sol'' |
sol''2 r |
R1*32 |
\stopStaff
