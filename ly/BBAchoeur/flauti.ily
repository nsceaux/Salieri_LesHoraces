\clef "treble" R1*6 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { mib'''1\f~ | mib'''2 do'''4 }
  { sol''1\f~ | sol''2 lab''4 }
>> r4 |
R1*3 |
mib''4-\sug\f mib''8 mib'' fa''4 sol'' |
lab''2 do'''4. reb'''8 |
mib'''2 mib'''4. mib'''8 |
lab''2 r |
R1*3 |
r4 \twoVoices #'(flauto1 flauto2 flauti) <<
  { re'''8. re'''16 re'''4 re''' |
    re'''1 |
    mib''' |
    re'''4 sib''8. sib''16 sib''4 sib'' |
    sib''4 mib''8. mib''16 mib''4 mib'' |
    mib''2 }
  { sib''8. sib''16 sib''4 sib'' |
    sib''2 lab'' |
    sol''1 |
    sib''4 fa''8. fa''16 fa''4 fa'' |
    sol''4 sol'8. sol'16 sol'4 sol' |
    sol'2 }
>> r2 |
R1*14 |
