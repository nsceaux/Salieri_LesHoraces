\clef "treble" \transposition mib
\twoVoices #'(corno1 corno2 corni) <<
  { do''2 do''4. do''8 |
    do''4 do''8 do'' do''4 do'' |
    re''2. re''4 |
    mi''4 sol''2 sol''4~ |
    sol'' sol''2 sol''4 |
    mi''2 }
  { mi'2 mi'4. mi'8 |
    mi'4 mi'8 mi' mi'4 mi' |
    sol'2. sol'4 |
    do''4 mi''2 mi''4~ |
    mi'' mi''2 mi''4 |
    do''2 }
  { s1-\sug\p }
>> r2 | <>\f
\twoVoices #'(corno1 corno2 corni) <<
  { sol''1~ | sol''2 fa''4 }
  { mi''1~ | mi''2 fa''4 }
>> r4 |
R1*10 |
r4 \twoVoices #'(corno1 corno2 corni) <<
  { sol''8. sol''16 sol''4 sol'' |
    sol''2 fa'' |
    mi''4 mi''8. mi''16 mi''4 sol'' |
    sol''1 |
    sol''4 do''8. do''16 do''4 do'' |
    do''2 }
  { mi''8. mi''16 mi''4 mi'' |
    mi''2 re'' |
    do''4 do''8. do''16 do''4 mi'' |
    re''1 |
    mi''4 mi'8. mi'16 mi'4 mi' |
    mi'2 }
>> r2 |
R1*14 |
