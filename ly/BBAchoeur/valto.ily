\clef "vhaute-contre" <>^\p sib'2 sib'4. sib'8 |
sib'2. sol'4 |
fa'2. sib'4 |
sib'1~ |
sib' |
sol'2 r |
R1*5 |
<>^\markup\character { Chœur de chevaliers Romains }
<>^\f mib'4 mib'8 mib' fa'4 sol' |
lab'2 do'4. reb'8 |
mib'2 mib'4. mib'8 |
lab2 r |
R1*3 |
r2 r4 re' |
re'4. re'8 re'4 re' |
mib'2 r |
sib'4 sib' sib' sib'8 sib' |
sol'2 r |
R1*9 |
r2 r4 re' |
mi'2 mi'4. mi'8 |
mi'2 mi'4. mi'8 |
mi'2 r4 mi' |
fa'2 re'4. re'8 |
sol'2 sol'4. sol'8 |
mi'2 \bar "|."
