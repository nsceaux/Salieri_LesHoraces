\clef "alto" <>-\sug\p \rt#4 <sib sol'>8 \rt#4 q |
\rt#4 q \rt#4 q |
\rt#4 <sib fa'> \rt#4 q |
\rt#4 <sib sol'> \rt#4 q |
\rt#4 <sib sol'> \rt#4 q |
q2 r |
mib'1-\sug\f~ |
mib'2 mib'4 r |
R1*3 |
lab'4-\sug\f lab'8 lab' lab'4 lab' |
lab'2 do'4. reb'8 |
mib'2 mib'4. mib'8 |
lab2 r |
r8. re'!16_\markup\italic\bracket Sempre forte re'4 r2 |
r4 mib' mib' r |
r8 r16 << { la'16 la'4 } \\ { re'16 re'4 } >> r2 |
r4 sol'8. sol'16 sol'4 sol' |
sol'4. sib'8 sib'4 sib' |
sib' sib8. sib16 sib4 mib' |
re' re'8. re'16 re'4 re' |
mib'4 sib8. sib16 sib4 sib |
sib2 r |
r8. mi'16 mi'4 r2 |
r8. fa'16 fa'8. fa'16 fa'4 r |
r2 r8. fa'16 fa'4 |
r8. mi'16 mi'8. mi'16 mi'4 r4 |
r2 r8. sold'16 sold'4 |
r2 r4 la' |
la! r r2 |
r8 r16 sol' sol'4 r2 |
\rt#4 fa'8 \rt#4 fa' |
\rt#4 mi' \rt#4 mi' |
\rt#4 sold' \rt#4 sold' |
\rt#4 la \rt#4 la' |
\rt#4 sol'! \rt#4 sol' |
\rt#4 sol' \rt#4 sol' |
\stopStaff
