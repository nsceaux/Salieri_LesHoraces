\clef "vbasse" <>^\p mib'2 mib'4. mib'8 |
mib'2. mib'4 |
sib2. sib4 |
mib'1~ |
mib' |
mib2 r |
R1*5 |
<>^\f lab4 lab8 lab lab4 lab |
lab2 do'4. reb'8 |
mib'2 mib'4. mib'8 |
lab2 r2 |
R1*3 |
r2 r4 sol |
sol4. sol8 fa4 fa |
mib2 r |
re'4 re' re' re'8 re' |
mib'2 r |
R1*9 |
r2 r4 fa |
mi2 mi4. mi8 |
sold2 sold4. sold8 |
la2 r4 la |
sol!2 sol4. sol8 |
sol2 sol4. sol8 |
do'2
