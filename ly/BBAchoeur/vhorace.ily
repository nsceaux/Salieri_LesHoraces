\clef "bass" R1*6 |
<>^\markup\character { Le vieil Horace }
mib'4. sib8 sol4 sib8 do' |
reb'4 reb'8 mib' do'^! do' r4 |
R1*6 |
r4 lab8 sib do'4 do'8 do' |
sib4^! r8 sib re'! re' re' mib' |
sib4^! r r2 |
R1*6 |
r4 sib sib8 sib do' reb' |
do'^! do' sol sol16 lab sib8 sib sib do' |
lab4^! r r2 |
R1*4 |
r4 r8 la! la la la si! |
dod'4^! r8 mi' dod'4 dod'8 dod'16 re' |
la4^! la r2 |
R1*5 |
\stopStaff
