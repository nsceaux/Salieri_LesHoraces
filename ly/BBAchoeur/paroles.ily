\tag #'(vsoprano valto vtenor vbasso) {
  Vive à ja -- mais le nom d’Ho -- ra -- ce
}
\tag #'jhorace {
  Qu’en -- tends- je ?
}
\tag #'vhorace {
  Viens, mon fils, que ton pè -- re t’em -- bras -- se.
}
\tag #'camille {
  Ah ! Ro -- me l’a choi -- si pour no -- tre dé -- fen -- seur !
}
\tag #'(valto vtenor vbasso) {
  Rome a ren -- du jus -- tice à sa hau -- te va -- leur.
}
\tag #'vhorace {
  Tes deux frè -- res & toi par -- ta -- gent cet hon -- neur.
}
\tag #'jhorace {
  A ce su -- prême hon -- neur eus -- sions- nous pu pré -- ten -- dre ?
}
\tag #'(valto vtenor vbasso) {
  Les Dieux ont fait ce choix : seuls ils l’ont pu dic -- ter.
}
\tag #'vhorace {
  Moins nous de -- vions l’at -- ten -- dre,
  Plus il fau -- dra le mé -- ri -- ter.
}
\tag #'jhorace {
  Mon pè -- re ! et je le jure à vous, à ma pa -- tri -- e
  Ou Ro -- me se -- ra li -- bre, ou je ne se -- rai plus.
}
\tag #'vhorace {
  Mou -- rant pour son pa -- ys, on meurt di -- gne d’en -- vi -- e.
}
\tag #'(valto vtenor vbasso) {
  Nos vœux ne se -- ront point dé -- çus.
  Nos vœux ne se -- ront point dé -- çus.
}
