\clef "vtaille" <>^\p sol'2 sol'4. sol'8 |
sol'2. mib'4 |
re'2. re'4 |
mib'1~ |
mib' |
mib'2 r |
R1*5 |
<>^\f do'4 do'8 do' reb'4 reb' |
mib'2 do'4. reb'8 |
mib'2 mib'4. mib'8 |
lab2 r |
R1*3 |
r2 r4 sib |
sib4. sib8 lab4 lab |
sol2 r |
fa'4 fa' fa' fa'8 fa' |
mib'2 r |
R1*9 |
r2 r4 la |
sold2 sold4. sold8 |
si2 si4. si8 |
do'2 r4 do' |
si2 si4. si8 |
si2 si4. si8 |
do'2
