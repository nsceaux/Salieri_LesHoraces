\clef "vhaute-contre" sol'8^\f |
sol'4 sol'8. sol'16 sol'4 sol'8. mib'16 |
re'2 r4 re'8. re'16 |
mib'2 mib'4. mib'8 |
lab'2 r4 lab'8^\p lab' |
fa'2 fa'4 fa' |
fa'2 r4 fa'8.^\f fa'16 |
fa'2 fa'4. sib'8 |
sib'2 r4 lab'8^\p lab' |
fa'2 fa'4 fa' |
mib'2 r |
R1*6 |
