\clef "vbas-dessus" sol'8^\f |
do''4 do''8. do''16 do''4 do''8. do''16 |
re''2 r4 sib'8. sib'16 |
mib''2 mib''4. mib''8 |
do''2 r4 mib''8^\p do'' |
sib'2 sib'4 sib' |
do''2 r4 fa''8.^\f fa''16 |
fa''2 re''4. sib'8 |
mib''2 r4 mib''8^\p do'' |
sib'2 sib'4 sib' |
sol'2 r |
R1*6 |
