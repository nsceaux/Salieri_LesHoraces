\clef "alto" r8 |
do'4\f do'8. do'16 do'4 do'8. do'16 |
sib2 r4 sib' |
sol'2 sol'4. sol'8 |
lab'2 r |
R1 |
r2 r4 la8.-\sug\f la16 |
lab!2 sib'4. sib'8 |
sib'2 r |
R1*2 |
mib'2 sib-\sug\f |
mib'1~ |
mib'2 fa-\sug\p |
mib2 mib4 mib |
mib2-\sug\f sib |
sib1 |
