\piecePartSpecs
#`((corno1 #:tag-global () #:instrument "Corno I en mi♭")
   (corno2 #:tag-global () #:instrument "Corno II en mi♭")
   (flauto1 #:notes "oboi")
   (flauto2 #:notes "oboi")
   (oboe1)
   (oboe2)
   (clarinetto1 #:score-template "score-clarinette-sib"
                #:tag-notes oboe1 #:notes "oboi")
   (clarinetto2 #:score-template "score-clarinette-sib"
                #:tag-notes oboe2 #:notes "oboi")
   (fagotto1 #:notes "fagotti")
   (fagotto2 #:notes "fagotti")
   (violino1 #:notes "violino1")
   (violino2 #:notes "violino2")
   (alto)
   (basso #:instrument "Basso")
   (silence #:on-the-fly-markup , #{ \markup\tacet#16 #}))
