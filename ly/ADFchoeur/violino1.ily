\clef "treble" sol''8\f |
do'''4 do'''8. do'''16 do'''4 do'''8. do'''16 |
re'''2 r4 re''8. re''16 |
mib''2 mib''4. mib''8 |
do''2 r |
R1 |
r2 r4 do'''8.\f do'''16 |
re'''2 re'''4. sib''8 |
mib'''2 r |
R1*2 |
r2 <fa' sib>2\f |
<sib sol'>4 r r2 |
r re'2\p |
mib'4. sol'8 mib'4 mib' |
mib'2\f mib' |
mib'1 |
