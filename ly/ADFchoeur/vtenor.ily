\clef "vtaille" sol8^\f |
mib'4 mib'8. mib'16 mib'4 mib'8. mib'16 |
fa'2 r4 fa'8. fa'16 |
sib2 sib4. mib'8 |
mib'2 r4 mib'8^\p mib' |
re'2 re'4 re' |
do'2 r4 do'8.^\f do'16 |
re'2 sib4. re'8 |
mib'2 r4 mib'8^\p mib' |
re'2 re'4 re' |
mib'2 r |
R1*6 |
