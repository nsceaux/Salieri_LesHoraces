\clef "treble" sol'8\f |
<<
  { mib''4 mib''8. mib''16 mib''4 mib''8. mib''16 |
    fa''2 } \\
  { sol'1 | sib'2 }
>> r4 fa'8. fa'16 |
<mib' sib'>2 q4. q8 |
<do'' mib'>2 r |
R1 |
r2 r4 fa''8.-\sug\f fa''16 |
<<
  { fa''2 fa''4. fa''8 | sib''2 } \\
  { sib'2 sib'4. sib'8 | sib'2 }
>> r2 |
R1*2 |
r2 re'2-\sug\f |
mib'4 r r2 |
r lab-\sug\p |
sol4. sib8 sol4 sol |
sol2-\sug\f sol |
sol1 |
