\clef "bass" sol8^\f |
do'4 do'8. do'16 do'4 do'8. do'16 |
sib2 r4 sib8. sib16 |
sol2 sol4. sol8 |
lab2 r4 lab8^\p lab |
sib2 sib4 sib |
la2 r4 la8^\f la |
lab!2 lab4. lab8 |
sol2 r4 lab8^\p lab |
sib2 sib,4 sib, |
mib2 r |
R1*6 |
