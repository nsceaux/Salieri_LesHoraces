\clef "bass" r8 |
do'1-\sug\f |
sib |
sol |
lab2. \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { lab4 | re'2 re'4 re' | do'2. la?4 | }
  { lab,4 | sib,2 sib,4 sib, | la,2. la,4 | }
  { s4-\sug\p | s1 | s2. s4-\sug\f }
>>
lab!1 |
sol2 r4 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { lab4 | re'2 re'4 re' | mib2 }
  { lab,4 | sib,2 sib,4 sib, | mib,2 }
  { s4-\sug\p }
>> do'4. sib8 |
lab4 sol \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { fa2 | }
  { sib,2 | }
  { s2-\sug\f }
>>
mib2 do'4. sib8 |
lab4 sol \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { fa2 | }
  { sib,2 | }
  { s2-\sug\p }
>>
mib2 mib4 mib |
mib2-\sug\f mib |
mib1 |
