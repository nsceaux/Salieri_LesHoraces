\clef "treble" \transposition mib
r8 |
\twoVoices #'(corno1 corno2 corni) <<
  { mi''4 mi''8. mi''16 mi''4 mi''8. mi''16 |
    sol''2 s4 re''8. re''16 | }
  { do''4 do''8. do''16 do''4 do''8. do''16 |
    sol'2 s4 sol'8. sol'16 | }
  { s1-\sug\f | s2 r4 }
>>
do''1~ |
do''2. \twoVoices #'(corno1 corno2 corni) <<
  { la''8 fa'' | re''2 re''4 re'' | }
  { do''4 | sol'2 sol'4 sol' | }
  { s4-\sug\p }
>>
re''2. re''4-\sug\f |
\twoVoices #'(corno1 corno2 corni) <<
  { re''2 sol''4. sol''8 | sol''4 do''2 la''8(-\sug\p fa'') |
    re''2 re''4 re'' | do''1 | do''2 re'' | do''1~ | do''2 sol' | }
  { sol'2 re''4. re''8 | mi''2 r |
    sol'2-\tag #'corno2 -\sug\p sol'4 sol' | do'1 |
    do'2 sol' | do'1~ | do'2 sol' | }
  { s1*4 | s2 s-\sug\f | s1 | s2 s-\sug\p | }
>>
do'1 |
do'2-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { do''2 | do''1 | }
  { mi'2 | mi'1 | }
>>
