\clef "bass" r8 |
do4\f do8. do16 do4 do8. do16 |
sib,2 r4 sib |
sol2 sol4. sol8 |
lab2 r |
R1 |
r2 r4 la8.\f la16 |
lab!2 lab4. lab8 |
sol2 r |
R1 |
mib1\p~ |
mib2 sib,\f |
mib1~ |
mib2 sib,\p |
mib2 mib4 mib |
mib2\f mib |
mib1 |
