\clef "bass" r2 do\p( |
si, sol,) |
la,4-. la,( si, la,) |
sold,2( la,) |
r4 re\sf( red mi) |
r4 mi\sf( fad sol) |
do2\p( la,4 re) |
sol,1 |
R1 |
r4 dod re2 |
r4 si,( do2) |
r4 la,( si, la,) |
sold,\f sol,2.\fermata |
r2 mi,\p |
fa, r |
r4 sol,2 sol,4 |
do2 do,4 r |
