\clef "treble"
<<
  \setMusic #'violino {
    do'\p( mi'2 la'8. sol'16) |
    sol'4 sol2 sol'4 |
    fa' fa'2 fa'4 |
    fa'2( mi') |
    fad'2.\sf sol'4 |
    do''2.\sf si'4\p |
    do''8( la') mi'2 fad'4 |
    fad'2( sol'4) r |
    sib'2( sol'4 mi') |
    la'2~ la'8 re''( do'' si'! |
    la' sol' sol'4.) do''8( si' la' |
    sol' fa' fa'2) fa'4 |
    fa'8\f si' si'2.\fermata |
    do''4\p do'2 dod'4 |
    re'( la' sol'8 fa' mi' re') |
    sol'2.( si4) |
    si2( do'4) r |
  }
  \tag #'violino1 \keepWithTag #'() \violino
  \tag #'flauto1 \transpose do do' \violino
>>
