\clef "alto" do4\p( mi2 la8. sol16) |
sol4 sol2 sol4 |
fa fa2 fa4 |
fa2( mi) |
fad2.-\sug\sf sol4 |
do'2.-\sug\sf si4-\sug\p |
do'8( la) mi2 fad4 |
fad2( sol4) r |
sib2( sol4 mi) |
la2~ la8 re'( do' si |
la sol sol4.) do'8( si la |
sol fa fa2 fa4) |
fa8-\sug\f si si2.\fermata |
do'4-\sug\p do2 dod4 |
re( la sol8 fa mi re) |
sol4 mi fa2~ |
fa mi4 r |
