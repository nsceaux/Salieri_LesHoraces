\clef "treble" r2 mi'(\p |
re' si) |
do'4-. do'( re' do') |
si2( do') |
r4 la2 sol4 |
r sol'( la' re') |
mi'-\sug\p do'2 do'4 |
do'2( si4) r |
R1 |
r4 mi'( fa'2) |
r4 re'( mi'2) |
r4 do'( re' do') |
si8-\sug\f fa' fa'2.\fermata |
mi'4-\sug\p do'2 sib4 |
la2 r |
r4 sol2 si4 |
si2( do'4) r |
