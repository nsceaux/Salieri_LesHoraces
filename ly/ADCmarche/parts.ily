\piecePartSpecs
#`((flauto1 #:instrument "Flauto solo" #:notes "violino1")
   (fagotto1 #:notes "fagotti" #:instrument "Fagotto solo")
   (violino1 #:notes "violino1")
   (violino2 #:notes "violino2")
   (alto)
   (basso #:instrument "Basso")
   (silence #:on-the-fly-markup , #{ \markup\tacet#34 #}))
