\clef "soprano/treble" R1*9 |
r2 r4 r8 lab' |
re''4. fa''8 re'' re'' do'' sib' |
mib''-! mib'' r4 r2 |
R1*2 |
r2 r4 r8 sol'16 sol' |
do''8 sol'16 lab' sib'8. sib'16 sib'8 sib' do'' sol' |
lab'!4-! r r2 |
R1 |
r4 r8 sib' mib''8. sib'16 sib'8 sib' |
sol'4 sib'8 do'' reb''4 reb''8 mib'' |
do''8-! do'' r mib'' mib'' mib'' mib'' mib'' |
do''4. do''8 do'' do'' reb'' mib'' |
lab'4-! r r2 |
R1*12 |
r2 mi''!4. mi''16 mi'' |
dod''4 r r2 |
r4 r8 la'16 la' re''4 mi''8 fa'' |
si'-! si' r si' si' do'' re'' mi'' |
do''4-! r r8 sol'16 sol' do''8 do''16 si' |
do''4 r r2 |
R1*13 |
r2 r8 la' la' si' |
do'' r16 mi'' do''8. do''16 do''8 do'' re'' mi'' |
la'4 r r2 |
R1*24 |
