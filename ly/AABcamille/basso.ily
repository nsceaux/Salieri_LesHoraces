\clef "bass/bass" r4 mib\f( fa sol) |
lab sol2.\fp |
fa1\p |
r4 sib,\f( re do) |
sib,1\f |
si,1 |
R1*2 |
sib,!4..\f sib,16 sib,4.. sib,16 |
sib,1~ |
sib, |
r4 mib-\sug\p\cresc( fa sol) |
lab sol2.\f~ |
sol8\p sol sol sol sol4. sol8\f |
mi8. mi16 mi8. mi16 mi2\p~ |
mi1 |
fa4 r r2 |
R1 |
mib2\fp~ mib~ |
mib1 |
%%
lab,4 r r2 |
R1 |
r4 lab\<( sib do')\! |
re1\p~ |
re |
r4 mib4\<( fa sol)\! |
si,1~ |
si, |
r4 do\<( re mib)\! |
fad,1~ |
fad, |
sol4\fermata r8 sol,\p( sib,4) r8\fermata sib, |
re4\fermata r8 re\f dod do sib, la, |
sol,4 r r2 |
R1 |
r4 sol2.\fp~ |
sol1 |
fa4 r r2 |
r8. fa16\f fa4 r2 |
r8. mi!16 mi8. mi16 mi4 r |
r2 mi\p |
fa1~ |
\once\tieDashed fa~ |
fa~ |
fa |
sib,4 r sib-\sug\f( la) |
sol2 sol,4 sol |
la1\p~ |
la |
fa8\p r fa r fa r fa r |
fa r fa r fa r fa r |
mi r mi r mi r mi r |
mi1\fp~ |
mi2 sold\f |
la4 r r2 |
R1*2 |
sol!1\fp |
fad1~ |
fad |
si,8 si\pp[ si si] \rt#4 si |
\rt#4 dod'8 \rt#4 lad |
si4 r r2 |
fad2\p r |
<>\f \rt#4 sol8 \rt#4 sol |
\rt#4 la \rt#4 fad |
sol4 r r2 |
R1 |
r4 do-\sug\f~ do8 re16 mi fa sol la si |
do'4 r r2 |
r8. do16 do2. |
r2 r8. fa16\ff fa8. fa16 |
fa4 r r2 |
r8. fa16 fa4 r2 |
r8. mi16 mi8. mi16 mi4 r |
r r8. sold16 sold2~ |
sold r2 |
la,4\ff sold, la, r |
la,8 si,16 do re mi fad sold la4 r |
R1 |
la2 r4 si |
