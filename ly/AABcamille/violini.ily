\clef "treble/treble"
<<
  \tag #'violino1 {
    mib'8\f mib'4 mib' mib' mib'8 |
    mib'4 sib'2\sf lab'16\p( sol' fa' mib') |
    mib'4.( do'8) re'2 |
    lab'8\f lab'4 lab' lab' lab'8 |
    lab'\sf re''8([ fa'' re''] lab'' fa'' re'' lab') |
    lab'4.( fa'8) sol'2 |
    r8 mib''4\fp( re''16 do'' si'8) sol'( lab' sol') |
    sol' mib''4\sf( re''16 do'' si'8) sol'( sol' lab') |
    lab'4..\f re''16 re''4.. fa''16 |
    fa''1~ |
    fa'' |
    mib'8\p mib'4\cresc mib' mib' mib'8 |
    mib'4 sib'2\f lab'16\p sol' fa' mib' |
    mib'8 reb' reb' reb' reb'4. reb'8\f |
    do'8. do'16 do'8. do'16 do'2\p~ |
    do'1 |
    r8 lab'4\sf sol'16 fa' mi'8 do'( reb' do') |
    do'8 lab'4\sf sol'16 fa' mi'8 do' do' reb' |
    reb'2\fp~ reb'~ |
    reb'1 |
    %%
    do'4 r r2 |
  }
  \tag #'violino2 {
    r4 sol-\sug\f( lab sib) |
    do' sib2-\sug\fp sib4 |
    lab1 |
    r4 re'-\sug\f( fa' mib') |
    re'1~ |
    re' |
    <>\fp do'8-.( do'-. do'-. do'-.) re'8-.( re'-. re'-. re'-.) |
    <>\fp do'8-.( do'-. do'-. do'-.) re'8-.( re'-. re'-. re'-.) |
    re'4..-\sug\f lab'16 lab'4.. re''16 |
    re''1~ |
    re'' |
    r4 sol(-\sug\p\cresc lab sib) |
    do' sib2-\sug\f sib4-\sug\p~ |
    sib8 sib sib sib sib4. sib8-\sug\f |
    sib8. sib16 sib8. sib16 sib2\p~ |
    sib1 |
    <>\fp lab8-.( lab-. lab-. lab-.) sib-.( sib-. sib-. sib-.) |
    <>\sf lab8-.( lab-. lab-. lab-.) sib-.( sib-. sib-. sib-.) |
    sib2\fp~ sib~ |
    sib1 |
    %%
    lab4 r r2 |
  }
>>
R1 |
r4 lab'4(\< sib' do'')\! |
re'1\p ~ |
re' |
r4 mib'(\< fa' sol')\! |
si!1~ |
si |
r4 do''(\< re'' mib'')\! |
fad'1~ |
fad' |
sol'4\fermata r8 sol\p( sib4) r8\fermata \grace do'16 sib la32 sib |
re'4\fermata r8 <<
  \tag #'violino1 {
    re'16\f re' mi'! mi' fad' fad' sol' sol' la' la' |
    sib'4 r r2 |
    R1 |
    r4 mi''!2.\fp~ |
    mi''1 |
  }
  \tag #'violino2 {
    re'4-\sug\f re' re'8 |
    re'4 r r2 |
    R1 |
    r4 dod''2.\fp~ |
    dod''1 |
  }
>>
re''4 r r2 |
r8. << { sol''16 sol''4 } \\ { si'!16\f si'4 } >> r2 |
r8. << { sol''16 sol''8. sol''16 sol''4 } \\ { do''16 do''8. do''16 do''4 } >> r4 |
<<
  \tag #'violino1 {
    r2 sib'\p |
    la'1~ |
    la'~ |
    la'~ |
    la' |
    sib'4 r8 r16 fa''\f fa''8. fa''16 fad''8. fad''16 |
    sol''4 <sib'' sib'>2 la''16 sol'' fa'' mi'' |
    \grace re''2 dod''1\p~ |
    dod'' |
    r8 re''\p r re'' r re'' r re'' |
    r re'' r re'' r re'' r re'' |
    r re'' r re'' r re'' r re'' |
    re''2\fp~ re''~ |
    re'' <mi'' mi'>2\f |
    do''4 r r2 |
    R1*2 |
    dod''1\fp~ |
    dod''~ |
    dod'' |
    r4 fad''2\pp re''8 si' |
    si' lad' lad' dod'' \grace fad'' mi'' re''16 mi'' fad''8 mi'' |
    re''4 r r2 |
    re''2\p r |
    r4 re'''2\f si''8 sol'' |
    sol'' fad'' fad'' la'' do''' si''16 do''' re'''8 do''' |
    si''4 r r2 |
  }
  \tag #'violino2 {
    r2 sol'-\sug\p |
    fa'1~ |
    <fa' mib''>~ |
    q~ |
    q |
    <fa' re''>4 r re''-\sug\f( do'') |
    sib'4 <sib' re'>2 sib'4 |
    mi'!1\p~ |
    mi' |
    r8 la'\p r la' r la' r la' |
    r la' r la' r si'! r la' |
    r <<si' \\ sold'>> r <<si' \\ sold'>> r <<si' \\ sold'>> r <<si' \\ sold'>> |
    << { si'2~ si'~ | si' <si' mi'>-\sug_\f | mi''4 } \\ { sold'2-\sug\fp~ sold'~ | sold' s | do''4 } >> r r2 |
    R1*2 |
    la'1\fp |
    lad'1~ |
    lad' |
    si'8 fad'-\sug\pp[ fad' fad'] \rt#4 fad'8 |
    \rt#4 fad'8 \rt#4 fad'8 |
    fad'4 r r2 |
    la'2\p r |
    <>\f \rt#4 <si' re'>8 \rt#4 q |
    \rt#4 <do'' re'>8 \rt#4 <la' re'> |
    <si' re'>4 r r2 |
  }
>>
R1 |
r4 do'\f~ do'8 re'16 mi' fa' sol' la' si' |
do''4 r r2 |
<<
  \tag #'violino1 {
    r8. mi''16 mi''2. |
  }
  \tag #'violino2 {
    r8. << { sib'16 sib'2. } \\ { sol'16 sol'2. } >> |
  }
>>
r2 r8. <fa'' la'>16\ff q8. q16 |
q4 r r2 |
r8. << { sol''16 sol''4 } \\ { si'!16 si'4 } >> r2 |
r8. << { sol''16 sol''8. sol''16 sol''4 } \\ { do''16 do''8. do''16 do''4 } >> r |
<<
  \tag #'violino1 {
    r4 r8. << { si''16 si''2 si''2 } \\ { si'16 si'2~ si'2 } >> r2 |
  }
  \tag #'violino2 {
    r4 r8. << { mi''16 mi''2~ | mi'' } \\ { si'16 si'2 | si' } >> r |
  }
>>
la16\ff do' si la si re' do' si do' la si do' re' mi' fad' sold' |
la'8 si'16 do'' re'' mi'' fad'' sold'' la''4 r |
R1 |
<red'' fad'>2 r4 <red'' fad' si>4 |
