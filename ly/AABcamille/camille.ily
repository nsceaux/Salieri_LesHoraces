\clef "soprano/treble" R1*23 |
r2 fa'8 fa' fa' fa' |
sib'2. lab'8 sib' |
sol'^! sol' r4 r2 |
r4 r8 sol' sol' sol' sol' la' |
si'4. sol'8 re'' re'' re'' mib'' |
do''-! do'' r4 r2 |
r r4 la' |
re''4. la'8 la' sib' do'' re'' |
sib'4-! r <>^\markup\italic { elle fait quelques pas timidement vers l'autel } r2 |
R1 |
r4 r8 sol'8 re''4 r16 re'' re'' re'' |
sib'4 sib'8 sib' sib'4 la'8 sib' |
sol' r16 sol' dod''4-! r2 |
r4 r8 dod''16 re'' mi''4 dod''8 la' |
re''-! re'' r4 r2 |
R1*2 |
r4 r8 re'' sib'-! sib' sib' do'' |
la'4-! r16 la' la' sib' do''4 r8 do''16 do'' |
mib''2-! do''4. sib'8 |
la' la' r do''16 do'' la'4 sib'8 do'' |
fa'4 r8 la'16 sib' do''4 do''8 re'' |
sib'4-! r r2 |
R1 |
r2 r4 mi''8 mi'' |
dod''4. mi''8 dod''4 dod''8 la' |
re''4 r r re''8 re'' |
re''2 si'!4. la'8 |
sold' sold' r sold' sold' sold' la' si' |
<>^\markup\italic { sans vigueur, mais vite } mi' mi' r8 mi'16 fad' sold'4 sold'8 la' |
si'4 si'8 si' mi''4-! mi''8 si' |
do''-! do'' r4 r2 |
R1 |
r2 la'4 r8 la' |
dod''4-! dod''16 dod'' dod'' re'' mi''8. dod''16 dod''8 dod'' |
lad'4-! r8 dod'' dod'' dod'' dod'' dod'' |
fad''4 r8 dod''16 re'' mi''4 dod''8 re'' |
si'-! si' r4 r2 |
R1 |
r4 r8 si' si' si' dod'' re'' |
la'4-! la'8 si' do''!4 do''8 re'' |
si'4-! r4 r2 |
R1 |
r4 r8 re''16 re'' si'4 si'8 do'' |
re''4 re''8 mi'' fa''4 re''8 mi'' |
do''8-! do'' r4 r2 |
r4 r8 sol' do''4 r16 do'' do'' re'' |
sib'4-! r16 sib' sib' sib' mi''4. sol''8 |
mi'' mi'' mi'' fa'' do''-! do'' r4 |
r4 r8 fa' do'' do'' do'' re'' |
si'!4-! r si'8 si'16 do'' re''8 mi'' |
do''4-! r r16 sol' do'' si' do''8. mi''16 |
mi''4. si'8 re''2~ |
re''4 re'' re'' do'' |
la'4 la' r2 |
r r4 r8 mi''16 mi'' |
do''4 si'8 do'' la'4 r8 la' |
red''8.-! red''16 red''8 mi'' si' si'-! r4 |
