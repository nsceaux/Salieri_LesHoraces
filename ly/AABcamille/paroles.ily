\tag #'suivante {
  D’où naît le trou -- ble qui vous pres -- se ?
  Vous trem -- blez à l’as -- pect de ces au -- gus -- tes lieux !
  Un mot peut dis -- si -- per cet -- te som -- bre tris -- tes -- se,
  O -- sez sur vos des -- tins in -- ter -- ro -- ger les Dieux.
}
\tag #'camille {
  J’ai dé -- ja pré -- vu leur ré -- pon -- se,
  Un noir pres -- sen -- ti -- ment d’a -- van -- ce me l’an -- non -- ce :
  Un mot peut me don -- ner la mort,
  Hé -- las ! fuy -- ons plu -- tôt sans con -- noî -- tre mon sort,
  Fuy -- ons.
}
\tag #'suivante {
  Non, de -- meu -- rez.
}
\tag #'camille {
  Que faut- il que j’es -- pè -- re ?
}
\tag #'suivante {
  Qu’Albe ou Ro -- me tri -- om -- phe, en ce mo -- ment fa -- tal,
  Vo -- tre pè -- re pour -- rait…
}
\tag #'camille {
  Que tu le con -- nois mal !
  Ces noms si doux & de fille & de pè -- re
  Dans son cœur tout Ro -- main sont des noms sans pou -- voir.
  Et sur quoi fon -- der mon es -- poir ?
  Pen -- sez- vous que pour gen -- dre il ac -- cep -- tât un hom -- me
  Que se -- roit ou l’es -- clave ou le maî -- tre de Ro -- me ?
}
\tag #'suivante {
  Lui- même en -- fin, lui- même a -- voit for -- mé ces nœuds.
}
\tag #'camille {
  Ah ! ce jour à la fois heu -- reux & mal -- heu -- reux
  Fit naître & dé -- trui -- sit ma plus chère es -- pé -- ran -- ce.
  Mon pè -- re me don -- noit à l’ob -- jet de mes vœux,
  Albe et Rome ap -- prou -- voient cette il -- lustre al -- li -- an -- ce.
  Sou -- dain, le sort ja -- loux
  Des deux é -- tats dé -- truit l’in -- tel -- li -- gen -- ce,
  La guerre en un mo -- ment bri -- se des nœuds si doux.
  Un mê -- me jour me donne & m’ôte à ce que j’ai -- me,
  Le mal -- heur naît pour moi du sein du bon -- heur mê -- me.
}
