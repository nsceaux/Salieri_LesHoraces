\clef "tenor/tenor" r4 mib\f( fa sol) |
lab sol2.-\sug\fp |
fa1 |
r4 \once\slurDashed sib,(\f re do) |
sib,1 |
si, |
<<
  \tag #'fagotto1 {
    mib'2\fp( fa') |
    mib'\fp( fa')~ |
    fa'1\f~ |
    fa'4 r r2
  }
  \tag #'fagotto2 {
    do'2\fp( re') |
    do'-\sug\fp( re')~ |
    re'1-\sug\f~ |
    re'4 r r2 |
  }
>>
R1 |
r4 mib-\sug\p(\cresc fa sol) |
lab! sol2.\f~ |
sol8-\sug\p sol sol sol sol4. sol8-\sug\f |
mi8. mi16 mi8. mi16 mi4 r |
R1 |
<<
  \tag #'fagotto1 {
    lab2\fp sib |
    lab sib |
    sib\fp
  }
  \tag #'fagotto2 {
    fa2-\sug\fp sol |
    fa sol |
    sol\fp
  }
>> r |
R1*62 |
