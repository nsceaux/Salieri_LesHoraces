\key mib \major \midiTempo#120
\time 4/4 \tempo "Andante con moto" s1*20
\key do \major s1*2
\tempo "Allegro" s1*10 s4.
\tempo "Allegro" s8 s2 s1*12 s4..
\tempo "Allegro" s16 s2 s1*3
\tempo "a tempo" s1*11
\tempo "Allegretto" s1*8 s4
\tempo "Allegro assai" s2. s1*12 \bar "|."
