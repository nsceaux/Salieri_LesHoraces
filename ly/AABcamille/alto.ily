\clef "alto/alto" r4 mib\f( fa sol) |
lab sol2.-\sug\fp |
fa1 |
r4 sib-\sug\f( re' do') |
fa'1 |
fa' |
<>\fp mib'8-.( mib'-. mib'-. mib'-.) fa'-.( fa'-. fa'-. fa'-.) |
<>-\sug\fp mib'8-.( mib'-. mib'-. mib'-.) fa'-.( fa'-. fa'-. fa'-.) |
fa'4..\f fa'16 fa'4.. lab'16 |
lab'1~ |
lab'1 |
r4 mib(-\sug\p\cresc fa sol) |
lab sol2-\sug\f sol4-\sug\p~ |
sol8 sol sol sol sol4. sol8-\sug\f |
sol8. sol16 sol8. sol16 sol2\p~ |
sol1 |
fa8-\sug\fp -.( fa-. fa-. fa-.) sol-.( sol-. sol-. sol-.) |
<>-\sug\sf fa8-.( fa-. fa-. fa-.) sol-.( sol-. sol-. sol-.) |
sol2\fp~ sol~ |
sol1 |
%%
lab4 r r2 |
R1 |
r4 lab(-\sug\< sib do'\!) |
re1\p~ |
re |
r4 mib'(\< fa' sol')\! |
si!1~ |
si |
r4 do'(\< re' mib')\! |
fad1~ |
fad |
sol4\fermata r8 sol\p( sib4) r8\fermata \grace do'16 sib la32 sib |
re'4\fermata r8 re'4-\sug\f la8( sol fad) |
sol4 r r2 |
R1 |
r4 la'2.\fp~ la'1 |
la'4 r r2 |
r8. fa'16\f fa'4 r2 |
r8. mi'!16 mi'8. mi'16 mi'4 r4 |
r2 do'-\sug\p |
do'1~ |
do'~ |
do'~ |
do' |
sib4 r sib'-\sug\f( la') |
sol'2 sol4 sol |
la1-\sug\p~ |
la |
r8 fa'\p r fa' r fa' r fa' |
r fa' r fa' r fa' r fa' |
r mi' r mi' r mi' r mi' |
mi'2-\sug\fp~ mi'~ |
mi' sold'2-\sug\f |
mi'4 r r2 |
R1*2 |
mi'1\fp |
fad'1~ |
fad' |
re'8 re'-\sug\pp[ re' re'] \rt#4 re' |
\rt#4 mi' \rt#4 dod' |
re'4 r r2 |
re'2-\sug\p r |
<>\f \rt#4 sol'8 \rt#4 sol' |
\rt#4 la' \rt#4 fad' |
sol'4 r r2 |
R1 |
r4 do'\f~ do'8 re'16 mi' fa' sol' la' si' |
do''4 r r2 |
r8. do'16 do'2. |
r2 r8. do'16-\sug\ff do'8. do'16 |
do'4 r r2 |
r8. re'16 re'4 r2 |
r8. mi'16 mi'8. mi'16 mi'4 r |
r4 r8. sold'16 sold'2~ |
sold' r |
la4\ff sold la r |
la8 si16 do' re' mi' fad' sold' la'4 r |
R1 |
la'2 r4 si' |
