\piecePartSpecs
#`((clarinetto1)
   (clarinetto2 #:instrument "Clarinette I")
   (fagotto1 #:notes "fagotti" #:clef "tenor")
   (fagotto2 #:notes "fagotti" #:clef "tenor")
   (violino1)
   (violino2)
   (alto)
   (basso #:notes "basso"
          #:instrument , #{ \markup\center-column { Basso Contrabasso } #})
   (silence #:on-the-fly-markup , #{ \markup\tacet#81 #}))
