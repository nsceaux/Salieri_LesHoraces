\clef "treble/treble" R1 |
r4 <>^"Solo" sib'2 lab'16( sol' fa' mib') |
mib'4.( do'8) re'2 |
R1 |
r8 re''( fa'' re'' lab'' fa'' re'' lab') |
lab'4.( fa'8) sol'2 |
r8 mib''4\fp( re''16 do'' si'8) sol'( lab' sol') |
sol' mib''4\sf( re''16 do'' si'8) sol'( sol' lab') |
lab'1\f ~ |
lab'4 r r2 |
R1*2 |
r4 sib'2\f lab'16\p sol' fa' mib' |
mib'8 reb'' reb'' reb'' reb''4. reb''8-\sug\f |
do''8. do''16 do''8. do''16 do''4 r |
R1 |
r8 lab''4\sf sol''16 fa'' mi''8 do''( reb'' do'') |
do''8 lab''4\sf sol''16 fa'' mi''8 do'' do'' reb'' |
reb''2\fp r |
R1*62 |
