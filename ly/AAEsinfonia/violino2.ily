\clef "treble" <>^\markup\italic { con sordini }
sol'16(\> mi' re' do' si do')\! |
la8( si do') |
sol'16(\> mi' re' do' si do')\! |
la8[ si] do'16 dod' |
re'8\cresc re' re'\! |
re'16(\f si' la' sol' fad' mi') |
re'16(\p red' mi' do' la la) |
la4( si8) |
re'16(\> si la\! sol la sol) |
fa'(\> re' do'\! si la si) |
la'(\sf sol' fa' mi' re' do') |
do'4( si8) |
sol'16(\> mi' re'\! do' si do') |
la8([ si] do'16 dod') |
re'16(\f mi' fa' fa' mi' re') |
re'4( mi'8) |
