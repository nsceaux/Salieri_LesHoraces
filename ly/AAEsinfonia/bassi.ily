\clef "bass" <>^\markup\italic { [con sordini] }
do4. |
fa8( re do) |
do4. |
fa8 re mi |
re16-\sug\cresc( fad la do' si la\!) |
sol4.-\sug\f |
si,8-\sug\p( do re) |
sol re sol, |
sol4. |
sol |
fa!\f |
sol |
do |
fa8( re mi) |
fa\f re sol |
do4. |
