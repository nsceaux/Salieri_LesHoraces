\clef "treble" <>^\markup\italic { con sordini }
sol''16(\> mi'' re'' do'' si' do'')\! |
la'8( si' do'') |
sol''16\>( mi'' re'' do'' si' do'')\! |
la'8[ si'] do''16 dod'' |
re''8\cresc re'' re''\! |
re''16\f ( si'' la'' sol'' fad'' mi'') |
re''(\p red'' mi'' do'' la' fad') |
fad'4( sol'8) |
re''16(\> si' la'\! sol' fad' sol') |
fa''(\> re'' do''\! si' la' si') |
la''(\sf sol'' fa'' mi'' re'' do'') |
do''4( si'8) |
sol''16(\> mi'' re''\! do'' si' do'') |
la'8([ si'] do''16 dod'') |
re''16(\f mi'' fa'' re'' do'' si') |
si'4( do''8) |
