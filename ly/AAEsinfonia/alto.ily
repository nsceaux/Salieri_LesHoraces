\clef "alto" <>^\markup\italic { [con sordini] }
mi4. |
r8 fa( mi) |
mi4. |
r8 fa( sol) |
fad16(-\sug\cresc fad la do' si la\!) |
sol4.-\sug\f |
si8-\sug\p( do' re') |
sol re sol |
sol4. |
re'16(\> si la\! sol fad sol) |
fa'-\sug\sf( mi' re' do' si la) |
re'4. |
mi |
r8 fa( sol) |
la4-\sug\f sol8 |
sol4. |
