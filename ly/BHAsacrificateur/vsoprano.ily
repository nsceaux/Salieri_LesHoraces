\clef "vbas-dessus" R1*62 |
r2 sol'4. sol'8 |
do''2 do''4. do''8 |
mi''2 do''4^\p do'' |
sol'2 sol'4 sol' |
mi'2 r\fermata |
