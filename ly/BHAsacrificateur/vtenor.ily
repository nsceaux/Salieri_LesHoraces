\clef "vtaille" R1*62 |
r2 sol4. sol8 |
mi'2 mi'4. mi'8 |
sol'2 do'4^\p do' |
do'2 si4 si |
do'2 r\fermata |
