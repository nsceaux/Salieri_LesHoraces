\tag #'sacrificateur {
  Ro -- mains, Al -- bains ! ce jour pré -- vient vo -- tre ru -- i -- ne.
  Ce pa -- ys in -- culte & dé -- sert,
  De vos longs dif -- fé -- rens a trop long- temps souf -- fert ;
  Le Ciel pour ja -- mais les ter -- mi -- ne.
  Six guer -- riers choi -- sis par -- mi vous,
  Vont dé -- ci -- der du sort de l’un & l’autre em -- pi -- re :
  Ce jour mê -- me la guerre ex -- pi -- re,
  Et ce der -- nier com -- bat nous ré -- u -- ni -- ra tous.
}
