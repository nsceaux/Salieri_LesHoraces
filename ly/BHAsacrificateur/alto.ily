\clef "alto" do'2 r |
do' r |
do' r |
do'4. do'8 do'4.\prall si16 do' |
la4 si8. si16 do'4 dod'8. dod'16 |
re'4. mi'8 mi'4.\prall re'16 mi' |
fa'4 mi'8. mi'16 re'4 do'8. do'16 |
si4 r r2 |
r4 do'8. do'16 do'4 do' |
sol2 r |
r4 do8.-\sug\p re16 mi4 fa |
sol4. sol'8-\sug\f sol'4. sol8 |
<< sol1~ { s2. s4-\sug\p } >> |
sol1~ |
sol~ |
sol |
sol4.-\sug\f do'8 do'4.\trill si16 do' |
la4 si8. si16 do'4 dod'8. dod'16 |
re'4. re'8 re'4 re' |
<< re'1~ { s2. s4-\sug\p } >> |
re'1~ |
re' |
red' |
mi'~ |
mi'2 r4 fad'\f |
si4. si8 si4 si |
si1~ |
si~ |
si2 do''!~ |
\smallNotes do'' \smallNotes si'2~ |
\smallNotes si'1~ |
\once\tieDashed si'~ |
si'2 r4 sol' |
