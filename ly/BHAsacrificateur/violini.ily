\clef "treble" <sol mi' do''>2 r |
<sol mi' do'' mi''> r |
<sol mi' do'' sol''> r |
<do' sol' mi'' do'''>4. do'8 do'4.\prall si16 do' |
la4 si8. si16 do'4 dod'8. dod'16 |
re'4. mi'8 mi'4.\prall re'16 mi' |
fa'4 mi'8. mi'16 re'4 do'8. do'16 |
si4 r r2 |
<<
  \tag #'violino1 {
    r4 << { sol''8. sol''16 sol''4 do''' | si''2 }
      \\ { do''8. do''16 do''4 do'' | re''2 }
    >> r2 |
    r4 mi'8.\p mi'16 mi'4 mi' |
    re'4. <>\f << { si''8 si''4. } \\ { si'8 si'4. }
    >> <sol' si>8 |
    << q1~ { s2. s4\p } >> |
    q1~ |
    q~ |
    q |
    <sol mi' do'' mi''>4.\f
  }
  \tag #'violino2 {
    r4 << { mi''8. mi''16 mi''4 mi'' | sol''2 }
      \\ { do''8. do''16 do''4 do'' | si'2 }
    >> r2 |
    r4 do'8.-\sug\p do'16 do'4 do' |
    si4. <re' si'>8-\sug\f q4. <re' si>8 |
    << q1~ { s2. s4\p } >> |
    q1~ |
    q~ |
    q |
    <do' mi'>4.-\sug\f
  }
>> do'8 do'4.\trill si16 do' |
la4 si8. si16 do'4 dod'8. dod'16 |
re'4. <<
  \tag #'violino1 {
    re'''8 re'''4 re''' |
    << re'''1~ { s2. s4\p } >> |
    re'''1~ |
    re''' |
    si''~ |
    si'' |
    lad''2 r4 <fad' dod'' lad''>\f |
    <fad' re'' si''>4. fad'8 si'4 re'' |
    fad''1~ |
    fad''~ |
    fad''2 la''~ |
    la'' si''~ |
    si''1~ |
    si''~ |
    si''2 r4 << <si'' re''>4 \\ sol'4 >> |
  }
  \tag #'violino2 {
    << { fad''8 fad''4 fad'' | fad''1~ | fad''~ |
        \once\tieDashed fad''~ | fad'' | sol'' | mi''2
      } \\
      { la'8 la'4 la' | << la'1~ { s2. s4-\sug\p } >> |
        la'1~ | la' | \once\tieDashed si'~ | si' | dod''2 }  
    >> r4 << mi'' \\ dod''-\sug\f >> |
    re''4. re'8 <si fad'>4 <re' si'> |
    << { \voiceOne re''1~ | re''~ | re''2~ \oneVoice re''~ |
        re'' re''~ | re''1~ | re''~ | re''2 }
      \new Voice { \voiceTwo fad'1~ | fad'~ | fad'2 }
    >> r4 <re' si' sol''>4 |
  }
>>
