\piecePartSpecs
#`((tromba1 #:notes "corni" #:tag-notes corno1
            #:instrument "Tromba I in C")
   (tromba2 #:notes "corni" #:tag-notes corno2
            #:instrument "Tromba II in C")
   (corno1 #:tag-global () #:instrument "Corno I in C")
   (corno2 #:tag-global () #:instrument "Corno II in C")
   (oboe1)
   (oboe2)
   (clarinetto1 #:notes "oboi" #:tag-notes oboe1)
   (clarinetto2 #:notes "oboi" #:tag-notes oboe2)
   (fagotto1)
   (fagotto2)
   (violino1)
   (violino2)
   (alto)
   (basso #:instrument "Basso")
   (timpani)
   (silence #:on-the-fly-markup , #{ \markup\tacet#33 #}))
