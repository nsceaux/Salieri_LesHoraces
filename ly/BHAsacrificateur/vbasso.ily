\clef "vbasse" R1*62 |
r2 sol4. sol8 |
do'2 do'4. do'8 |
do'2 do4^\p mi |
sol2 sol4 sol |
do2 r\fermata |
