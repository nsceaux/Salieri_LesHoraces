\clef "bass" do2 r |
do r |
do r |
do4. do8 do4.\prall si,16 do |
la,4 si,8. si,16 do4 dod8. dod16 |
re4. mi8 mi4.\prall re16 mi |
fa4 mi8. mi16 re4 do8. do16 |
si,4 r r2 |
r4 do8. do16 do4 do |
sol2 r |

<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    \clef "tenor" r4
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { mi'8. mi'16 mi'4 mi' | re'4. }
      { do'8. do'16 do'4 do' | si4. }
      { s4-\sug\p }
    >> \clef "bass"
  }
  \tag #'basso { r4 do8.\p re16 mi4 fa | sol4. }
>> sol8\f sol4. sol8 |
<< sol1~ { s2. s4\p } >> |
sol1~ |
sol~ |
sol |
do4.\f do8 do4.\trill si,16 do |
la,4 si,8. si,16 do4 dod8. dod16 |
re4. re8 re4 re |
<< re1~ { s2. s4-\sug\p } >> |
re1~ |
re |
red |
mi~ |
mi2 r4 fad\f |
si,4. si,8 si,4 si, |
si,1~ |
si,~ |
si,2 fad~ |
fad sol~ |
sol1~ |
sol2 fa!~ |
fa r4 sol |
