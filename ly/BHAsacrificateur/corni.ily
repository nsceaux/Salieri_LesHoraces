\clef "treble"
\twoVoices #'(corno1 corno2 corni) <<
  { do''4. do''8 do''4 do'' |
    mi''4. mi''8 mi''4 mi'' |
    sol''4. sol''8 sol''4 sol'' |
    sol''2 }
  { mi'4. do''8 do''4 do'' |
    do''4. do''8 do''4 do'' |
    mi''4. mi''8 mi''4 mi'' |
    mi''2 }
>> r2 |
R1*3 |
r4 r8 \twoVoices #'(corno1 corno2 corni) <<
  { re''8 re''4 re'' |
    mi'' sol''8. sol''16 sol''4 mi'' |
    re''4. re''8 re''4 re'' |
    mi''4 }
  { sol'8 sol'4 sol' |
    do'' mi''8. mi''16 mi''4 do'' |
    sol'4. sol'8 sol'4 sol' |
    do''4 }
>> r4 r2 |
r4 r8 \twoVoices #'(corno1 corno2 corni) <<
  { re''8 re''4. sol'8 | sol'1~ | sol'~ | sol'~ |
    sol' | do''4 }
  { sol'8 sol'4. sol8 | sol1~ | sol~ | sol~ |
    sol | do'4 }
  { s8-\sug\f s2 | s2. s4-\sug\p | }
>> r4 r2 |
R1 |
r4 r8 re''8-\sug\f re''4 re'' |
<< re''1~ { s2. s4-\sug\p } >> |
re''1~ |
re'' |
R1*10 |
r2 r4 \twoVoices #'(corno1 corno2 corni) <<
  { re''4 | }
  { sol'4 | }
>>
