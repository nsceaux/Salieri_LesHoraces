\clef "vbasse" R1*12 |
r2 r4 r8 si |
re'2 r4 re' |
si2 r4 sol |
si4. re'8 si4 si8 si16 do' |
sol8^! sol r4 r2 |
R1*2 |
r2 r4 re'8 re' |
re'4. la8 la4 la8 la |
fad4 r8 la16 la la4 si8 do' |
si4.^! si8 si si la si |
sol4^! r8 si mi'4 si8 dod' |
lad4^! lad8 si fad fad r4 |
R1 |
r4 r8 fad16 fad si4. si8 |
si4 si8 re' si4 r8 si |
si si si re' la4^! r8 la |
do' do' do' si sol^! sol r8 re'16 re' |
si8 si16 si si8 re' sol sol r sol |
sol sol sol la si4^! si8 re' |
si4 si8 do' sol4^! r4 |
