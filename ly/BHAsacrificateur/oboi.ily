\clef "treble"
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''4. do''8 do''4 do'' |
    mi''4. mi''8 mi''4 mi'' |
    sol''4. sol''8 sol''4 sol'' |
    do'''2 }
  { mi'4. do''8 do''4 do'' |
    do''4. do''8 do''4 do'' |
    mi''4. mi''8 mi''4 mi'' |
    mi''2 }
>> r2 |
R1 |
r4 r8 mi' mi'4.\prall re'16 mi' |
fa'4 mi'8. mi'16 re'4 do'8. do'16 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol'4. re''8 re''4 re'' |
    mi'' sol''8. sol''16 sol''4 do''' |
    si''2 s |
    s4 mi''8. mi''16 mi''4 mi'' |
    re''4. sol''8 sol''4. sol''8 |
    sol''1~ | sol''~ | sol''~ | sol'' | mi''4 }
  { sol'4. sol'8 sol'4 sol' |
    do'' mi''8. mi''16 mi''4 mi'' |
    re''2 s |
    s4 do''8. do''16 do''4 do'' |
    si'4. si'8 si'4. si'8 |
    si'1~ | si'~ | si'~ | si' | do''4 }
  { s1*2 | s2 r | r4 s2.-\sug\p | s4. s8\f s2 | s2. s4\p | }
>> r4 r2 |
R1 |
r4 r8 \twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''8 fad''4 fad'' | fad''1~ | fad''~ | fad''~ |
    fad'' | sol'' | lad''2 s4 lad'' | si''4. }
  { la'8 la'4 la' | la'1~ | la'~ | la' |
    si'~ | si' | dod''2 s4 dod'' | re''4. }
  { s8-\sug\f s2 | s2. s4\p | s1*4 | s2 r4 s4-\sug\f }
>> fad'8 si'4 re'' |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''1~ | fad''~ | fad''2 la''~ | la'' si''~ |
    si''1~ | si''1~ | si''2 s4 si'' | }
  { re''1~ | re''~ | re''2~ re''~ | re'' re''~ |
    re''1~ | re''~ | re''2 s4 re'' | }
  { s1*6 | s2 r4 }
>>
