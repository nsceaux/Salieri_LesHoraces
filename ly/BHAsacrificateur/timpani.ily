\clef "bass" do2 r |
do r |
do r |
do r |
R1*4 |
r4 do8. do16 do4 do |
sol,2 r |
r4 sol,8.\p sol,16 sol,4 sol, |
sol,4. sol,8\f sol,4. sol,8 |
sol,2 r |
R1*20 |
