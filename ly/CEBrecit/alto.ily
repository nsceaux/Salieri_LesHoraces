\clef "alto" <mi do'>8\p q4 q q q8 |
<mi si>8 q4 q q q8 |
<mi do'>8 q4 q q q8 |
<< { la8 la4 la la la8 | } \\ { fa8 fa4 fa fa fa8 | } >>
sold8 <mi si>4 q q q8 |
<mi do'>8 q4 q q q8 |
<si re'>1-\sug\fp |
<< mi'4 \\ do' >> r4 << mi' \\ sol >> r |
<< mi' \\ sol >> r << mi' \\ sol >> r |
<< mi' \\ sol >> r mi16 fa sol la si do' re' mi' |
re'4 r re' r |
re' r re'-\sug\p r |
do'16 si la sol fad mi re do re'4 r |
r2 si'16 la' sol' fad' mi' re' do' si |
fad'4 r red'16 mi' fad' sold' lad' si' dod'' red'' |
mi''4 r r fa'!-\sug\mf |
sol'2 << sib' \\ sol'-\sug\ff >> |
<mi' sol>1 |
r4 << <do'' fa'>4 \\ la >> r << mi' \\ sol >> |
<< { fa'4 la'2 } \\ { la4 fa'2 } >> la4-\sug\ff |
sib8 do'16 re' mib' fa' sol' la' sib'8 sib' sib' sib' |
do''8 do' do' do' \rt#4 la |
si4 r si' r |
la' r sold' r |
sol'! r fa' r |
mi''1-\sug\ff |
re''4 r r2\fermata |
<fa'! sold>1-\sug\f\fermata |
r2 mi' |
