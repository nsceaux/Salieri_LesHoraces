\clef "vbasse" R1 |
r2 r4 mi8 mi |
la4 r r2 |
r4 fa8 fa fa4. fa8 |
mi4. mi8 mi4. mi8 |
do2 r |
r4 r8 sol si4. si8 |
do'2 r4 sol8 sol16 sol |
do'4 do' r do'8 do' |
mi'4 do'8 do' do'4. do'8 |
sol4 r r2 |
r4 si8 si re'4 re'8 re' |
do'4 la8 la re'4 re'8 re' |
si4 sol r2 |
r4 si8 si si4 si8 si |
sol4 r r la8 si |
do'4 do' r2 |
r4 do'8 do' do'4 do'8 do' |
fa'4 la8 sib do'4. do'8 |
la4 fa r fa8 fa |
re'2. re'4 |
mib'2 mib'4 do'8 fa' |
re'2 r4 re'8 re' |
dod'4 dod'8 dod' re'4. re'8 |
dod'4 dod' r re'8 re' |
mi'4 sol8 sol sol4. sol8 |
fa4\fermata la8 la re'4 re'8 fa' |
sold8 sold r re sold sold sold la |
mi mi r4 r2 |
