\clef "treble" la16\p( si do' si la si do' si la si do' si la si do' re') |
mi' si mi' si mi' si mi' si mi' si mi' si mi' fad' sold' mi' |
la' mi' la' mi' la' mi' la' mi' la' si' do'' si' la' sol' fa' mi' |
fa' re' fa' re' fa' re' fa' re' fa' re' fa' re' fa' re' fa' re' |
mi' si mi' si mi' si mi' si sold' la' si' la' sold' fa'! mi' re' |
do' la si do' re' mi' fad' sold' la' mi' la' mi' la' mi' la' mi' |
si'!\fp do'' re'' mi'' fa'' mi'' re'' do'' si' la' sol' fa' mi' re' do' si |
<<
  \tag #'violino1 {
    do'8 re'16 mi' fa' sol' la' si' do'' si' la' sol' fa' mi' re' do' |
    mi'' re'' do'' si' la' sol' fa' mi' sol'' fa'' mi'' re'' do'' si' la' sol' |
    do'''16 si'' la'' sol'' fa'' mi'' re'' do'' do'''4 r |
    sol''16 fad'' mi'' re'' do'' si' la' sol' si'' la'' sol'' fad'' mi'' re'' do'' si' |
    re''' do''' si'' la'' sol'' fad'' mi'' re'' sol''\p fad'' mi'' re'' do'' si' la' sol' |
    do''4 r la'16 si' do'' re'' mi'' fad'' sol'' la'' |
    si''8 la''16 sol'' fad'' mi'' re'' do'' si'4 r |
    si'16 dod'' red'' mi'' fad'' sold'' lad'' si'' si'4 r |
    sol'16 si' si' si' \rt#4 si' \rt#4 si' \rt#4 si'\mf |
    do''2
  }
  \tag #'violino2 {
    do'4 r r2 |
    do''16 si' la' sol' fa' mi' re' do' mi'' re'' do'' si' la' sol' fa' mi' |
    mi'' re'' do'' si' la' sol' fa' mi' mi''4 r |
    r2 sol'16 fad' mi' re' do' si la sol |
    si' la' sol' fad' mi' re' do' si si'-\sug\p la' sol' fad' mi' re' do' si |
    fad'4 r re' r |
    sol'8 fad'16 mi' re' do' si la sol4 r |
    si'16 lad' sold' fad' mi' red' dod' si fad'4 r |
    mi'16 sol' sol' sol' \rt#4 sol' \rt#4 sol' \rt#4 fa''-\sug\mf |
    mi''2
  }
>> <mi' do'' sib''>4.\ff la''32*4/6( sol'' fa'' mi'' re'' do'') |
<sol' sib'>1 |
r4 << <fa'' la'> \\ fa' >> r <<
  \tag #'violino1 { <mi' sol>4 | }
  \tag #'violino2 { sib4 | }
>>
<la fa'>4 << <la'' do''>2 \\ fa'\f >> do'4\ff |
re'8 mib'16 fa' sol' la' sib' do'' re''8 re'' re'' re'' |
mib''8 mib' mib' mib' \grace fa' mib' re'16 mib' \grace fa'8 mib' re'16 mib' |
re'4 <si fa' re''>4 r q |
r <la mi' dod''> r <fa' re''> |
r <mi' dod''> r <<
  \tag #'violino1 {
    re'''4 | mi'''!1\ff | re'''4
  }
  \tag #'violino2 {
    la''4 | <sib' sol''>1-\sug\ff | << fa''4 \\ la' >>
  }
>> r4 r2\fermata |
<si sold'>1\f\fermata |
r2 <mi'! si'! sold''>2 |
