\tag #'valere {
  Res -- té seul, n’o -- sant plus comp -- ter sur sa va -- leur,
  Il feint de fuir ; les Cu -- ri -- a -- ces
  Le pour -- sui -- vent a -- vec fu -- reur,
  Mais d’un pas i -- né -- gal cha -- cun d’eux suit ses tra -- ces :
  Il les voit di -- vi -- sés, se re -- tour -- ne, & d’a -- bord
  Sous ses coups vo -- tre gendre ex -- pi -- re.
}
\tag #'camille { Ciel ! }
\tag #'valere {
  Le se -- cond ac -- court, dou -- ble d’ef -- fort,
  Son cou -- ra -- ge n’y peut suf -- fi -- re,
  Il é -- prou -- ve le mê -- me sort,
  Et la mort du troi -- siè -- me as -- su -- re notre em -- pi -- re.
}
