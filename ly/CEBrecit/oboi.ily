\clef "treble" R1*6 |
<>\fp \twoVoices #'(oboe1 oboe2 oboi) <<
  { si'1 | do''4 s do''2 | mi'' sol'' | do''' mi'' |
    sol'' si'' | re''' re'' | do''1 | si' |
    si' | si'2. fa''!4 | mi''2 sib'' | sib'1 |
    s4 la' s mi'' | fa'' la''2. | }
  { re'1 | mi'4 s mi'2 | do'' mi'' | mi'' do'' |
    re'' re'' | si' si' | fad' la' | sol'1 |
    fad'1 | sol'2. si'4 | do''2 sib'' | sol'1 |
    s4 fa' s sib' | la' do''2. | }
  { s1 | s4 r s2 | s1*3 | s2 s-\sug\p |
    s1*3 | s2. s4-\sug\mf | s2 s\f | s1 | r4 s r s | }
>>
R1*5 | <>-\sug\ff
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sib''1 | la''4 }
  { sol''1 | fa''4 }
>> r4 r2\fermata |
R1^\fermataMarkup |
R1 |
