\clef "treble" mib''8 sol''16[-\sug\p sol''] fa'' fa'' mib'' mib'' |
re'' re'' si' si' do'' do'' re'' re'' |
mib'' mib'' sol' sol' fa' fa' mib' mib' |
\rt#4 re' \rt#4 re' |
\rt#8 re' |
fa'2~ |
fa' |
mib'4. do''8 |
do''4 sib' |
lab' re''~ |
re'' sol'~ |
sol'2 |
sol16 mib''-\sug\f[ mib'' mib''] \rt#4 mib'' |
\rt#4 re'' \rt#4 re'' |
\rt#4 re'' \rt#4 re'' |
\rt#8 sol' |
sol'8 sol4\p sol8 |
la8 la4 la8 |
la la4 la8 |
sib4 r\fermata |
R2*3 | \allowPageTurn
r4 r8 sib'16-\sug\f sib' |
mib''8 sib' sol' mib' |
re'2 |
<fa' sib> |
<sib sol'>4 sib'-\sug\p |
mib' do''~ |
<do'' mib'>16-\sug\f <mib' mib''>8 q q q16 |
q16 q8 q q q16 |
<re'' fa'>2\ff\fermata |
r4 re'-\sug\p( |
mib') sib'~ |
sib' lab' |
sol' mib'~ |
mib' re' |
mib'4. sib8 |
sib4 sib |
sib2*7/8~ \hideNotes sib16 |
