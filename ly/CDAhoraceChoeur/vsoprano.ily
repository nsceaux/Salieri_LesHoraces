\clef "vbas-dessus" r8 <>^\markup\italic { Derrière le théâtre }
sol'' sol'' sol'' |
sol''4. sol''8 |
sol''4. sol''8 |
sol''2 |
sol''4 r |
R2*7 |
<>^\markup\italic\override #'(baseline-skip . 0) \column {
  Plusieurs femmes entrent effrayées sur la scène
}
r8 sol'' sol'' sol'' |
<< { \voiceOne lab''4. lab''8 | lab''4. fa''8 | mib''2 | mib''4 }
  \new Voice \with { autoBeaming = ##f } {
    \voiceTwo re''4. re''8 | re''4. re''8 | do''2 | do''4
  }
>> \oneVoice r4 |
R2*2 |
R2\fermataMarkup |
<< { \voiceOne sol''4 sol''8. sol''16 |
    mib''8 mib'' \oneVoice r \voiceOne mib'' |
    re''8. re''16 re''8. re''16 |
    sib'8 sol'
  }
  \new Voice \with { autoBeaming = ##f } {
    \voiceTwo sib'4 sib'8. sib'16 |
    do''8 do'' s sol' |
    fad'8. fad'16 fad'8. fad'16 |
    sol'8 sol'
  }
>> \oneVoice r4 |
R2*3 |
r4 mib''8. re''16 |
do''4 fa''8. fa''16 |
solb''2~ |
solb'' |
fa''\fermata |
r4 sib'8. sib'16 |
mib''2 |
do''4 do'' |
sib'2~ |
sib' |
mib'4 r |
R2*2 |
