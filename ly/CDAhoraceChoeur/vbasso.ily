\clef "vbasse" mib4 r |
R2*6 |
r4 r8 do' |
do'4 sib |
lab si~ |
si si8 si |
do'2 |
do'4 r |
R2*3 |
do4 do8 do |
fad4. fad8 |
fad4. fad8 |
sol4 r\fermata |
R2*3 |
r4 r8 sib16 sib |
mib'8[ sib] sol mib |
re2 |
R2 |
\sugNotes { r4 sol8. sol16 } |
lab4 lab8. lab16 |
la2~ |
la |
lab!\fermata |
r4 lab8. lab16 |
sol2 |
lab4 lab, |
sib,2~ |
sib, |
mib4 r |
R2*2 |
