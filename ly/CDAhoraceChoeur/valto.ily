\clef "vhaute-contre" mib'4 r |
R2*6 |
r4 r8 mib'^\p |
mib'4 mi' |
fa' fa'~ |
fa' mib'8 re' |
mib'2 |
mib'4 r |
R2*3 |
mib'4 mib'8 mib' |
re'4. re'8 |
re'4. re'8 |
re'4 r\fermata |
R2*5 |
r4 r8 sib16 sib |
lab'8[ fa'] re' lab |
sol4 mib'8. mib'16 |
mib'4 fa'8. fa'16 |
mib'2~ |
mib' |
re'\fermata |
r4 re'8. re'16 |
mib'2 mib'4 mib' |
mib'2~ |
mib'4\melisma re'\melismaEnd |
mib'4 r |
R2*2 |
