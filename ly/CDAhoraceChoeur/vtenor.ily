\clef "vtaille" sol4 r |
R2*6 |
r4 r8 do' |
do'4 do' |
do' re'~ |
re' sol8 sol |
sol2 |
sol4 r |
R2*3 |
sol4 sol8 sol |
la4. la8 |
la4. la8 |
sib4 r\fermata |
R2*5 |
r4 r8 sib16 sib |
lab'8[ fa'] re' lab |
sol4 sib8. sib16 |
do'4 do'8. do'16 |
do'2~ |
do' |
sib\fermata |
r4 fa'8. fa'16 |
sib2 |
do'4 lab |
sol2~ |
sol4\melisma fa\melismaEnd |
sol r |
R2*2 |
