\tag #'all \key mib \major
\tempo "Allegro" \midiTempo#112
\time 2/4 s2*20
\tempo "Poco lento" s2*3 s4
\tempo "Allegro" s4 s2*16 \bar "|."