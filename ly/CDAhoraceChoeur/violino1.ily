\clef "treble" mib''8 sol''[\p sol'' sol''] |
sol'' sol''4 sol''8 |
sol'' sol''4 sol''8 |
sol''16 sol' sol' sol' \rt#4 sol' |
\rt#8 sol' |
si'2 |
si' |
do''4. mib''8 |
mib''4( mi'' |
fa'') fa''~\sf |
fa'' mib''8 re'' |
mib''2 |
mib'16 sol''[\f sol'' sol''] \rt#4 sol'' |
\rt#4 lab'' \rt#4 lab'' |
\rt#4 lab'' lab'' lab'' fa'' fa'' |
\rt#8 mib'' |
mib''8 mib'4\p mib'8 |
re' re'4 re'8 |
re' re'4 re'8 |
re'4 r\fermata |
R2*3 | \allowPageTurn
r4 r8 sib'16\f sib' |
mib''8 sib' sol' mib' |
re'4. sib'16. sib'32 |
lab''8-. fa''-. re''-. lab'-. |
<sol' sib>4 mib''8.\p re''16 |
do''4 fa''8. fa''16 |
solb''16\f solb''8 solb'' solb'' solb''16 |
solb''16 solb''8 solb'' solb'' solb''16 |
<fa'' sib'>2\ff\fermata |
r4 sib'\p~ |
sib' mib''~ |
mib'' do'' |
sib' sol'~ |
sol' fa' |
mib'4. mib'8 |
mib'4 mib' |
mib'2*7/8~ \hideNotes mib'16 |
