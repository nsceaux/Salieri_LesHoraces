\clef "treble" \transposition mib
\twoVoices #'(corno1 corno2 corni) <<
  { do''4 }
  { mi'4 }
>> r4 |
R2*2 |
\twoVoices #'(corno1 corno2 corni) <<
  { mi''2~ | mi''~ | mi''~ | mi'' |
    mi'' | mi''4 sol'' | fa'' fa''~ | fa'' mi'' |
    mi''2~ | mi''8 }
  { mi'2~ | mi'~ | mi'~ | mi' |
    do'' | do''4 mi'' | re'' re'' | re''2 |
    do''2~ | do''8 }
>> r8 r4 |
R2*6 |
R2^\fermataMarkup |
R2*6 |
\twoVoices #'(corno1 corno2 corni) <<
  { re''2 | mi''4 }
  { sol'2 | do''4 }
>> r4 |
R2 |
do''2\f |
do'' |
\twoVoices #'(corno1 corno2 corni) <<
  { re''2\fermata }
  { sol'-\tag #'corno2 ^\fermata }
  { s\ff }
>>
R2*3 |
r4 \twoVoices #'(corno1 corno2 corni) <<
  { mi''4~ | mi'' re'' | do''4. do''8 do''4 do'' | do''2 | }
  { do''4~ | do''4 sol' | mi'4. mi'8 | mi'4 mi' | mi'2 | }
>> 
