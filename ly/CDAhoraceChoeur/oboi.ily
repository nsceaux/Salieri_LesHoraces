\clef "treble"
mib''8 sol''4-\sug\p sol''8 |
sol'' sol''4 sol''8 |
sol'' sol''4 sol''8 |
sol''2~ |
sol''4 r |
R2*7 |
r8 \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''4 sol''8 | lab''2~ | lab''4. fa''8 | mib''2~ | mib''4 }
  { mib''4 mib''8 | fa''2~ | fa''4. re''8 | do''2~ | do''4 }
  { s4-\sug\f }
>> r4 |
R2*2 |
R2^\fermataMarkup |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''4 sol''8. sol''16 |
    mib''8 mib'' s mib'' |
    re''8. re''16 re''8. re''16 |
    sib'8 sol' }
  { sib'4 sib'8. sib'16 |
    do''8 do'' s do'' |
    fad'8. fad'16 fad'8. fad'16 |
    sol'8 sol' }
  { s2\f | s4 r8 s }
>> r4 |
R2 |
r4 r8 sib' |
lab'' fa'' re'' lab' |
sol'4 r |
R2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { solb''2 | solb'' | fa''\fermata | }
  { mib'' | mib'' | re''-\tag #'oboe2 ^\fermata | }
  { s2*2-\sug\f | s2\ff }
>>
R2*3 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''4~ | sol'' fa'' | mib'' }
  { mib''4~ | mib'' re'' | mib'' }
>> r4 |
R2*2 |
