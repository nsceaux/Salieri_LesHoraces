\clef "vbasse" mib4 r |
R2*3 |
r4 r8 sol |
si4 si8 do' |
re'4 si8. sol16 |
do'4 r |
R2*8 |
do4 do8 do |
fad4. fad8 |
fad4. fad8 |
sol4 r\fermata |
R2*3 |
r4 r8 sib16 sib |
mib'8[ sib] sol mib |
re2 |
R |
r4 sol8. sol16 |
lab4 lab8. lab16 |
la2~ |
la |
lab!\fermata |
r4 lab8. lab16 |
sol2 |
lab4 lab, |
sib,2~ |
sib, |
mib4 r |
R2*2 |
