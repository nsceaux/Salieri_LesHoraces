\tag #'camille {
  - res !
}
\tag #'vhorace {
  - res !
}
\tag #'(valto vtenor vbasso) {
  - res !
}
%%
\tag #'vsoprano {
  O sort cru -- el ! des -- tins con -- trai -- res !
}
\tag #'vhorace {
  D’où vien -- nent ces tris -- tes cla -- meurs ?
}
\tag #'(valto vtenor vbasso) {
  Veil -- lez sur nous, Dieux tu -- té -- lai -- res !
}
\tag #'vsoprano {
  O sort cru -- el ! des -- tins con -- trai -- res !
}
\tag #'(vhorace valto vtenor vbasso) {
  Ciel que pré -- sa -- gent ces dou -- leurs ?
}
\tag #'vsoprano {
  Al -- be tri -- om -- phe et Rome est as -- ser -- vi -- e.
}
\tag #'(vhorace valto vtenor vbasso) {
  Ô fu -- nes -- te com -- bat, mal -- heu -- reu -- se pa -- tri -- e,
  mal -- heu -- reu -- se pa -- tri -- e.
}
\tag #'vsoprano {
  mal -- heu -- reu -- se pa -- tri -- e,
  mal -- heu -- reu -- se pa -- tri -- e.
}
