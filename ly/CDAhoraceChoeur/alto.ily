\clef "alto" mib8 sol'16[-\sug\p sol'] \rt#4 sol' |
\rt#8 sol' |
\rt#8 sol' |
sol' si si si \rt#4 si |
\rt#8 si |
re'2~ |
re' |
mib'4. fa'8 |
sol'4 sol'( |
fa') si~ |
si si |
do'2 |
do16 do'-\sug\f[ do' do'] \rt#4 do' |
\rt#4 fa' \rt#4 fa' |
\rt#4 fa' fa'16 fa' sol sol |
do' do'' do'' do'' \rt#4 do'' |
do''8 do'4-\sug\p do'8~ |
do' do'4 do'8 |
do' do'4 do'8 |
sib4 r\fermata |
sol4-\sug\f sol8. sol16 |
do'8 do' r do' |
re'4 re |
sol r8 sib16 sib |
mib'8 sib sol mib |
re2~ |
re |
mib4 mib'-\sug\p~ |
mib' do'~ |
do'16-\sug\f do''8 do'' do'' do''16 |
do'' do''8 do'' do'' do''16 |
sib'2\ff\fermata |
r4 fa'\p( |
sib) sol'( |
lab') mib'~ |
mib' sol' |
sib'2 |
sol4. sol8 |
sol4 sol |
sol2*7/8~ \hideNotes sol16 |
