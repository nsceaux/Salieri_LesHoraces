<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    \clef "tenor" sol8
  }
  \tag #'basso {
    \clef "bass" mib8 \clef "tenor" <>^"Violoncelli"
  }
>> mib'8\p[ re' do'] |
si sol la si |
do' mib' re' do' |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    si2~ |
    si4 r |
    R2*3 |
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { mib'4 mi' | fa'2~ | fa'4 mib'8 re' | mib'2 | }
      { do'4 sib | lab si | si4 si | do'2 | }
    >>
    r8 sol'4-\sug\f mib'8 |
    re'2~ |
    re' |
    sol~ |
    sol4 r |
    R2*2 |
    R2^\fermataMarkup |
    sol4-\sug\f sol8. sol16 |
    do'8 do' r do' |
    re'2 |
    sol4 r8 sib | \clef "bass"
  }
  \tag #'basso {
    si16 \clef "bass" <>^"Tutti" sol16 sol sol \rt#4 sol |
    \rt#8 sol |
    \rt#8 sol |
    \rt#8 sol |
    \rt#8 do |
    \rt#8 do |
    \rt#8 do |
    \rt#8 do |
    \rt#8 do |
    do16\f do' do' do' \rt#4 do' |
    \rt#8 si |
    \rt#8 si |
    \rt#8 do' |
    <>\p \rt#8 do |
    \rt#8 fad |
    \rt#8 fad |
    sol4 r\fermata |
    <>^"Violoncelli" sol4\f sol8. sol16 |
    do'8 do' r do' |
    re'4 re |
    sol4 r8 sib^"Tutti" |
  }
>>
mib'8 sib sol mib |
re2~ |
re |
mib4 sol\p |
lab lab |
la8\f la,16 la, la,8 la, |
la, la, la, la, |
lab,!2\ff\fermata |
r4 lab\p |
sol sol, |
lab, lab, |
sib,2~ |
sib,4 sib, |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    sib4. sib8 |
    sib4 sib |
    sib2 |
  }
  \tag #'basso {
    mib4. mib8 |
    mib4 mib |
    mib2*7/8~ \hideNotes mib16 |
  }
>>
