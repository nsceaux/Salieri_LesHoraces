\clef "alto" fa'16-\sug\f |
fa'8. do''16 do''8. la'16 |
la'8. fa'16 fa'8. do'16 |
do' do'( la do' la-\sug\p do' la do') |
sib!( do' sib do' sib do' sib do') |
la do' la do' la do' la-\sug\f do' |
mi do' mi do' mi do' mi do' |
fa do'( la do' la-\sug\p do' la do') |
sib( do' sib do' sib do' sib do') |
la do' la do' la do' la-\sug\f do' |
\rt#8 fa'16 |
si8. sol16 sol8. sol16 |
sol8 sol'16. sol'32 sol'8 sol' |
sol'1~ |
sol' |
<< \modVersion { sol'1~ } \origVersion { sol'2~ sol'~ } >> |
sol'4 r r2 |
fa16-\sug\f do'-\sug\p( la do' la do' la do') |
sib( do' sib do' sib do' sib do') |
la-\sug\< ( do' la do' la do' la do'\!) |
la8-\sug\f la'16. la'32 la'8 la' |
la'1 |
sib'4 r r2 |
r2 fa' |
mib'1~ |
mib'2 fad'~ |
fad' |
sol'8.-\sug\f sol16 sol8. sol16 |
sol8 sol'16. sol'32 sol'8 sib' |
la'2~ la'-\sug\p~ |
la'1~ |
la' |
la'8. fa16-\sug\f fa16. fa32 fa16. fa32 fa4 r |
r2 la'2-\sug\f~ |
la'1 |
