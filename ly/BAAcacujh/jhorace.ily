\clef "tenor/G_8" r16 |
R2*12 R1*2 |
r2 <>^\markup\character { Le Jeune Horace } do'8 do'16 do' do'8 re' |
sib4^! r8 sol sib sib sib do' |
la4^! r |
R2*3 |
R1*3 |
\clef "alto/G_8" r4 r16 sol sol lab sib4 sib8 sib |
sib4 sib8 mib' re'^! re' r la16 sib |
do'8 do' r16 do' do' re' |
sib4^! r |
R2 R1*6 |
