\clef "treble" <la' do'''>16\f |
q8. <la'' la'>16 q8. <fa'' la'>16 |
q8. <do'' fa'>16 q8. la'16 |
fa'4. fa''8\p |
mi''4.( sol''16 fa'') |
fa''4. re'''16\f( do''' |
do'''4.\grace do'''16 sib'' la''32 sib'') |
la''4. fa''8\p |
mi''4.( sol''16 fa'') |
fa''4. fa'8\f |
si'8 re''16 do'' si' do'' re'' mi'' |
fa''8. re'''16 re'''8. si''16 |
si''8 fa''16. fa''32 fa''8 fa'' |
fa''1~ |
fa'' |
<< \modVersion { mi''1~ } \origVersion { mi''2~ mi''~ } >> |
mi''4 r r2 |
fa'4.\f fa''8\p |
mi''4.( sol''16 fa'') |
fa''4~ fa''16\< sol'' la'' sib''\! |
do'''8\f do'''16. do'''32 do'''8 do''' |
do'''1 |
sib''4 r r2 |
r2 re'' |
mib''1~ |
mib''2 re''~ |
re'' |
sib'8.\f re''16 re''8. sol''16 |
sol''8 sib''16. sib''32 sib''8 sib'' |
dod''2~ dod''\p~ |
dod''1~ |
dod''1 |
re''8. re''16\f re''16. fa''32 fa''16. la''32 la''4 r |
r2 fa''\f~ |
fa''1*15/16~ \hideNotes fa''16 |
