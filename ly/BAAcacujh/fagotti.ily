\clef "tenor" fa16-\sug\f |
fa8. fa16 fa8. fa16 |
fa8. fa16 fa8. fa16 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { fa8 do' do' do' |
    do'2 | do'2 | do' |
    do'8 do' do' do' |
    do'2 | do' | si~ | si8. }
  { fa8 la la la |
    sib2 | la2 | mi |
    fa8 la la la |
    sib2 | la | fa~ | fa8. }
  { s4 s-\sug\p | s2 | s2\cresc | s2\! -\sug\f | s8 s4.-\sug\p |
    s2*2 | <>-\sug\f }
>> \clef "bass" sol,16 sol,8. sol16 |
sol8 sol16. sol32 sol8 sol |
sol2 r |
R1*3 |
\clef "tenor" r8 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { do'8 do' do' | do'2 | do'~ | do' | do'4 }
  { la8 la la | sib2 | la~ | la | la4 }
  { s4.-\sug\p | s2 | s2\< | s2\! -\sug\f }
>> r4 r2 |
R1*4 R2*3 R1*6 |
