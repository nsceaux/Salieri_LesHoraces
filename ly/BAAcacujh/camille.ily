\clef "soprano/treble" r16 |
R2*12 |
r4 <>^\markup\character Camille r8 re'' fa''4. re''8 |
si'4 si'8 do'' re''4 re''8 mi'' |
do''^! do'' r4 r2 |
R1 |
R2*4 R1*5 R2*3 R1*3 |
r2 r4 r8 re''16 re'' |
re''4 mi''8 fa'' do''4^! do''8 re'' |
mib''2 do''4. re''8 |
