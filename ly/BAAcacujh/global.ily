\tempo "Allegretto" \midiTempo#108
\tag #'all \key fa \major
\time 2/4 \partial 16 s16 s2*12
\time 4/4 s1*4
\time 2/4 s2*4
\tag #'all \key do \major \time 4/4 s1*5
\time 2/4 s2*3
\time 4/4 s1*6
\time 2/2
