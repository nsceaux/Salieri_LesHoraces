\clef "bass" fa16\f |
fa8. fa16 fa8. fa16 |
fa8. fa16 fa8. fa16 |
<<
  \tag #'cello {
    fa16 do'( la do' la\p do' la do') |
    sib( do' sib do' sib do' sib do') |
    la do' la do' la do' la\f do' |
  }
  \tag #'cb {
    fa4 r |
    R2 |
    r4 r8 la16\f do' |
  }
>>
mi do' mi do' mi do' mi do' |
<<
  \tag #'cello {
    fa16 do'( la do' la\p do' la do') |
    sib( do' sib do' sib do' sib do') |
    la do' la do' la do' la\f fa |
  }
  \tag #'cb {
    fa4 r |
    R2 |
    r4 r8 la16\f fa |
  }
>>
\rt#8 re16 |
re8. sol,16 sol,8. sol16 |
sol8 sol16. sol32 sol8 sol |
sol1~ |
sol |
<< \modVersion { do1~ } \origVersion { do2~ do~ } >> |
do4 r r2 |
<<
  \tag #'cello {
    fa16\f do'(-\sug\p la do' la do' la do') |
    sib( do' sib do' sib do' sib do') |
    la\< do' la do' la do' la do'\! |
    la\f do' la do' la do' la do' |
    do'1 |
    sib4
  }
  \tag #'cb {
    fa4 r |
    R2*2 |
    fa8\f fa16. fa32 fa8 fa |
    fa1 |
    sib,4
  }
>> r4 r2 |
r2 sib, |
mib1~ |
mib2 fad~ |
fad |
sol8.\f sol16 sol8. sol16 |
sol8 sol16. sol32 sol8 sol |
sol2~ sol\p~ |
sol1~ |
sol |
fa8. fa16-\sug\f fa8 fa fa4 r |
r2 la,\f~ |
la,1 |
