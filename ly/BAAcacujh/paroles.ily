\tag #'camille {
  Ain -- si le Ciel pour ja -- mais nous ras -- sem -- ble !
}
\tag #'jhorace {
  Sur le bon -- heur pu -- blic le nôtre est af -- fer -- mi.
}
\tag #'curiace {
  Que mon sort est heu -- reux ! Ce jour me rend en -- sem -- ble
  Et ma maî -- tres -- se & mon a -- mi.
}
\tag #'jhorace {
  Il u -- ni -- ra d’u -- ne chaine é -- ter -- nel -- le
  Nos fa -- mil -- les & nos é -- tats.
}
\tag #'curiace {
  Quel Dieu pro -- pi -- ce, a -- près tant de dé -- bats,
  A pu for -- mer une u -- ni -- on si bel -- le ?
}
\tag #'camille {
  Que ce Dieu bien -- fai -- sant en bé -- nis -- se le
}
