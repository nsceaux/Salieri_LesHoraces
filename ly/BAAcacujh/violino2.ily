\clef "treble" <do'' la''>16-\sug\f |
q8. q16 q8. <do'' fa'>16 |
q8. <la' fa'>16 q8. do'16 |
la4. fa'8-\sug\p |
sol'4.( mi'16 fa') |
fa'4. \slurDashed re''16-\sug\f( do'' |
do''4. \grace do''16 sib' la'32 sib' |
la'4.) fa'8-\sug\p |
sol'4.( mi'16 fa') | \slurSolid
fa'4. fa'8\f |
si'8 re''16 do'' si' do'' re'' mi'' |
fa''8. fa''16 fa''8. <fa'' re''>16 |
q8 <si' re'>16. q32 q8 q |
q1~ |
q |
<< \modVersion { <mi' do''>1~ } \origVersion { <mi' do''>2~ q~ } >> |
q4 r r2 |
fa'4.-\sug\f fa'8-\sug\p |
sol'4.( mi'16 fa') |
fa'4~ fa'16-\sug\< sol' la' sib'\! |
do''8-\sug\f mib''16. mib''32 mib''8 mib'' |
mib''1 |
re''4 r r2 |
r2 <lab' sib!> |
<sib sol'>1~ |
sol'2 la'~ |
la' |
re'8.-\sug\f <re' sib'>16 q8. q16 |
q8 re''16. re''32 re''8 re'' |
<mi' la>2~ q-\sug\p~ |
q1~ |
q |
<la re'>8. <re' la'>16-\sug\f q16. q32 q16. q32 q4 r |
r2 do''-\sug\f~ |
do''1 |
