\clef "tenor/G_8" r16 | R2*12 R1*4 R2*4 |
<>^\markup\character Curiace r4 do'8 re' mib'4 mib'8 fa' |
re'4^! r8 sib re' re' do' re' |
sib8 sib r16 fa' fa' fa' re'8^! re' r16 re' re' mib' |
sib4^! r r2 |
R1 R2*3 |
r8 mi' mi' mi' dod' dod' r dod'16 dod' |
dod'4 si8 dod' la4 r16 dod' dod' re' |
mi'4 mi'8 mi'16 mi' dod'4. la8 |
re'^! re' r4 r2 |
R1*2 |
