\clef "treble" \twoVoices #'(oboe1 oboe2 oboi) <<
  { do'''16 |
    do'''8. la''16 la''8. fa''16 |
    fa''8. do''16 do''8. la'16 |
    fa'4. fa''8-\sug\p |
    mi''4.( sol''16 fa'') |
    fa''4. fa''8 |
    sol''2 |
    la''4 r8 fa''-\sug\p |
    mi''4.( sol''16 fa'') |
    fa''4. }
  { la''16 |
    la''8. fa''16 fa''8. do''16 |
    do''8. la'16 la'8. fa'16 |
    fa'4. r8 |
    R2 |
    r4 r8 fa' |
    do''2 |
    fa''4 r |
    R2 |
    r4 r8 }
  { s16-\sug\f | s2*4 | s4. s8-\sug\f }
>> fa'8-\sug\f |
si' re''16 do'' si' do'' re'' mi'' |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''8. fa''16 fa''8. fa''16 |
    fa''8 fa''16. fa''32 fa''8 fa'' |
    fa''2 }
  { fa''8. si'16 si'8. si'16 |
    si'8 si'16. si'32 si'8 si' |
    si'2 }
>> r2 |
R1*3 |
r4 r8 \twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''8 |
    mi''4. sol''16 fa'' |
    fa''4~ fa''16 sol'' la'' sib'' |
    do'''8 mib''16. mib''32 mib''8 mib'' |
    mib''2 }
  { fa'8 |
    sol'4. mi'16 fa' |
    fa'4~ fa'16 sol' la' sib' |
    do''8 do''16. do''32 do''8 do'' |
    do''2 }
  { s8-\sug\p | s2 | s4 s-\sug\< | s2\!-\sug\f }
>> r2 |
R1*4 R2*3 R1*6 |
