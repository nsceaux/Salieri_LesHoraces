\clef "treble" \transposition fa
\twoVoices #'(corno1 corno2 corni) <<
  { sol''16 |
    sol''8. mi''16 mi''8. do''16 |
    do''8. sol'16 sol'8. do''16 |
    do''8 sol''4 sol''8-\sug\p |
    sol''2~ | sol''\cresc~ | sol''\!\f~ |
    << sol''~ { s4 s\p } >> |
    sol''2~ | sol'' | <>-\tag #'corno1 -\sug\f }
  { mi''16 |
    mi''8. do''16 do''8. sol'16 |
    sol'8. mi'16 mi'8. mi'16 |
    mi'4 r |
    R2 |
    r4 r8 do''-\tag #'corno2 -\sug\f |
    re''2 |
    mi''4 r |
    R2 |
    r4 r8 do'-\tag #'corno2 -\sug\f | }
  { s16-\sug\f }
>>
do''2 |
do''4 r8 r16 re'' |
re''8 re''16. re''32 re''8 re'' |
re''2 r |
R1*3 |
\twoVoices #'(corno1 corno2 corni) <<
  { r8 sol''-\sug\p sol'' sol'' | sol''2\<~ | sol''2\!\f~ |
    sol''8 do''16. do''32 do''8 do'' | do''2 }
  { do'4-\sug\f r | R2*2 | r8 do'16. do'32 do'8 do' | do'2 }
>> r2 |
R1*4 R2*3 R1*6 |
