\clef "treble" \transposition mib
re''1-\sug\fp~ |
re'' |
mi''-\sug\f |
re''-\sug\p~ |
re''4.-\sug\cresc re''8 re''4. re''8 |
re''1\!-\sug\fp |
mi''4 r r2 |
re''1-\sug\cresc~ |
re'' |
re''4\! re''8. re''16 re''4 re'' |
re''2 r |
R1*6 |
\twoVoices #'(corno1 corno2 corni) <<
  { sol''1~ | sol''~ | sol'' | mi''4 mi''8. mi''16 mi''2 | }
  { sol'1~ | sol'~ | sol' | do''4 do''8. do''16 do''2 | }
  { s1-\sug\fp | s-\sug\cresc | s\!-\sug\f | }
>>
R1 |
r4 r8 \twoVoices #'(corno1 corno2 corni) <<
  { re''8 re''4. re''8 | mi''1~ | mi''~ | mi'' | }
  { sol'8 sol'4. sol'8 | do''1~ | do''~ | do'' | }
  { s8-\sug\f }
>>
re''1-\sug\fp~ |
re''-\sug\cresc |
re''\!-\sug\f |
re''4 re''8. re''16 re''4 re'' |
re''2 r |
\twoVoices #'(corno1 corno2 corni) <<
  { re''1 | mi''4 do'' mi'' sol'' | do'' }
  { sol'1 | do''4 do' mi' sol' | do'' }
  { s2-\sug\f s-\sug\p | s4 s-\sug\f }
>> r4 r2 |
R1 |
\twoVoices #'(corno1 corno2 corni) <<
  { re''1 | mi'' | re'' | mi'' | fa'' |
    mi''~ | mi''~ | mi'' | }
  { sol'1 | do'' | sol' | do'' | re''1 |
    mi''2. re''4 | do''1~ | do''2. mi''4 | }
  { s1*2-\sug\fp | s1*3-\sug\fp | s1-\sug\fp | s-\sug\ff } 
>>
re''1 |
R1*3 |
r4 sol''8. sol''16 sol''4 sol'' |
re'' re''8. re''16 re''4 re'' |
re''2.\fermata r4 |
R1*9 |
R1^\fermataMarkup |
\twoVoices #'(corno1 corno2 corni) <<
  { re''1 | mi''4 do'' mi'' sol'' | do'' }
  { sol'1 | do''4 do' mi' sol' | do'' }
  { s1-\sug\fp | s4 s2.-\sug\f }
>> r4 r2 |
sol''1~ |
sol''2. fa''4 |
mi''1~ |
mi'' |
re'' |
R1*4 |
re''1~ |
re'' |
sol''~ |
sol''2.-\sug\ff fa''4 |
mi''1-\sug\p |
mi'' |
re'' |
R1*10 |
r2 re''-\sug\< ~ |
<< re''1\! { s2 s-\sug\p } >> |
sol'4 r r2 |
R1*3 |
r2 re''~ |
re'' re'' |
\twoVoices #'(corno1 corno2 corni) <<
  { sol'1~ | sol'~ | sol'~ | sol' | sol'2 }
  { sol1~ | sol~ | sol~ | sol | sol2 }
  { s1 | s-\sug\f | s | s-\sug\p }
>> r2 |
R1*3 |
