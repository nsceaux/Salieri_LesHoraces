\clef "vbas-dessus" fa''2 do''4 la' |
fa' fa' r fa'' |
sol''2. sib'4 |
la' la' r do'' |
si'1 |
re''4. mib''8 fa''4 fa''8 si' |
do''2 r4 do'' |
la'2 r |
r4 do'' re'' mi'' |
fa''2 r |
r r4 fa' |
sib' do'' reb'' mib'' |
fa''2. reb''8[ sib'] |
\appoggiatura sib'4 la'2 la'4 r |
r2 do''4. mib''8 |
reb''2 reb''4. solb''8 |
fa''2. la'4 |
sib'2 r |
R1*17 |
lab''2 fa''4 re'' |
mib'' sib' r r8 sib' |
lab''2. re''4 |
mib'' mib'' r2 |
R1 |
sol''1~ |
sol''2. sol''4 |
sol''4( mib'') do'' sib' |
la'! r r2 |
r4 reb'' do'' sib' |
la'2 r |
r4 sib' reb''2~ |
reb''4 reb'' reb'' sib' |
la'2.\fermata r4 |
r2\fermata r4 fa' |
sib' do'' reb'' mib'' |
fa''2. reb''8[ sib'] |
\appoggiatura sib'4 la'2 la' |
r do''4. mib''8 |
reb''2 mib''4. solb''8 |
fa''2. la'4 |
sib'4 r r2 |
R1*2 |
R1^\fermataMarkup |
R1*3 |
r2 r4 fa'' |
fa''2. fa''4 |
fa''2. fa''4 |
mi''2. mi''4 |
fa''( mib''!) mib''2 |
R1 |
r2 re''4 re'' |
do''1 |
r2 reb''4 reb'' |
re''!1~ |
re''2 mib''4( do'') |
sib'2. fa''4 |
fa''2. fa''4 |
fa''2. fa''4 |
mi''2. mi''4 |
fa''( mib''!) mib''2 |
R1 |
r2 re''4 re'' |
do''1 |
r2 reb''4 reb'' |
re''!1~ |
re''2 mib''4( do'') |
sib'2 r |
R1*2 |
r2 fa''4 mi'' |
fa''1~ |
fa''2 la' |
sib' r |
R1*2 |
r2 fa''4 mi'' |
fa''1~ |
fa''2 la' |
sib' r |
R1*7 |
\stopStaff
