\clef "treble" <>-\sug\fp \rt#8 <la fa'>16 \rt#8 q |
\rt#12 q <>-\sug\f \rt#4 <do' la'> |
\rt#8 <do' sol'> <>-\sug\p \rt#8 q |
\rt#8 <do' la'> \rt#8 q |
<>-\sug\cresc \rt#8 <re' si'> \rt#8 q |
<>\!-\sug\fp \rt#8 q \rt#8 q |
\rt#4 <mi' do''> \rt#8 <mi' sol> \rt#4 q |
\rt#4 <la fa'>-\sug\cresc \rt#4 fa' \rt#4 sol' \rt#4 la' |
\rt#4 sib' \rt#4 la' \rt#4 sib' \rt#4 sol' |
<>\! <la' fa'>4 <fa' la>8. q16 q4 q |
q2. r4 |
R1*6 |
<>-\sug\fp \rt#4 sib'16 \rt#4 re' \rt#4 fa' \rt#4 la' |
<>-\sug\cresc \rt#4 sib' \rt#4 do'' \rt#4 re'' \rt#4 sib' |
<>\!-\sug\f \rt#4 fa'' \rt#4 re'' \rt#4 sib' \rt#4 fa' |
sol'4 <sol' sib>8. q16 q2 |
r4 r8. lab'16 lab'4. lab'8 |
fa'4. fa'8 fa'4. sib'8 |
sol' r sol'4( sib' sol') |
mib'8 r mib'4( sol' mib') |
do'8 r do'4( mib' do') |
<>-\sug\fp do'16 la' la' la' \rt#4 la' \rt#8 la' |
<>-\sug\cresc \rt#8 sib' \rt#8 sib' |
<>\!-\sug\f \rt#8 <reb'' sib''> \rt#8 q |
<< la''4 \\ do'' >> <fa' la>8. q16 q4 q |
q2 r |
<sib fa' re''!>2\f re'4.-\sug\p re'8 |
mib'4 mib'8.\f mib'16 sol'4 sib' |
mib'' mib'' re'' do'' |
sib'16 fa' fa' fa' \rt#4 fa' \rt#8 fa' |
<>-\sug\fp \rt#8 fa' \rt#8 fa' |
\rt#8 sol' \rt#8 sol' |
<>-\sug\fp \rt#8 fa' \rt#8 fa' |
\rt#8 sol' \rt#8 sol' |
lab'8 lab'4 lab' lab' lab'8 |
<>-\sug\fp sol' do''4 <>-\sug\fp do'' re'' re''8 |
<>-\sug\ff mib'' mib''4 mib'' mib'' mib''8 |
\rt#4 mib''16 \rt#4 sol'' \rt#4 mib'' \rt#4 re'' |
do''4 la'(\sf do'' fa'') |
mi''1 |
fa''8 r la'4\sf( do'' fa'') |
mi''1~ |
mi''4 mi''8. mi''16 mi''4 mi'' |
mib''!4 << { la''8. la''16 la''4 la'' | la''2.\fermata }
  \\ { do''8. do''16 do''4 do'' | do''2. }
>> r4 |
R1*9 |
R1^\fermataMarkup |
<sib fa' re''!>2-\sug\f re'4.-\sug\p re'8 |
mib'4 mib'8.\f mib'16 sol'4 sib' |
mib'' mib'' re'' do'' |
sib'2.( <>-\sug\ff re''4) |
re''2.( do''4) |
si'1 |
sib'!4 \once\slurDashed reb'''( do''' sib'') |
la''( do''') do'''2 |
r4 sib'(-\sug\p la' sib') |
r sib' sib' sib' |
r sib' sib' sib' |
r sib' sib' sib' |
r sib' sib' sib' |
sib' sib'( do'' la') |
sib'2.( <>-\sug\ff re''4) |
re''2. do''4 |
<>-\sug\p si'1 |
sib'!4 reb''' do''' sib'' |
la''( do''') do'''2 |
r4 <>-\sug\p sib'( la' sib') |
r sib' sib' sib' |
r sib' sib' sib' |
r sib' sib' sib' |
r sib' sib' sib' |
sib'-. \once\slurDashed sib'( do'' la') |
sib' r <>-\sug\p mib'2~ |
mib'( fa'4 sol') |
sol'1 |
fa'4-\sug\p( mib' re' dod') |
re' r re'2\< |
mib'2\! do'\p |
re'4 r <>-\sug\p mib'2~ |
mib'(\< fa'4 sol'\!) |
sol'1 |
\once\slurDashed fa'4( mib' re' dod') |
re' r re'2( |
mib') do' |
sib r |
<>-\sug\f mib''2.( re''4) |
re''1 |
<>-\sug\p <mib' do'>2.( <re' sib>4) |
q sib2\ff do'8 re' |
mib' do' re' mib' fa' sol' mib' fa' |
re'4 sib'8 sib' sib'4 do'' |
re'' sib' la' sol' |
\custosNote <la fad' re''>4
