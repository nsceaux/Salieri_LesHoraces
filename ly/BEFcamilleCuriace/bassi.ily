<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    \clef "tenor" fa1-\sug\fp | fa | mi-\sug\f | fa-\sug\p |
    si4.-\sug\cresc do'8 re'4. mi'8 |
    fa'1\!-\sug\fp | mi' |
    fa'4-\sug\cresc \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { la4 sib do' | re' do' re' mi' | }
      { fa4 sol la | sib la sib sol | }
    >>
    fa'4\! fa8. fa16 fa4 fa |
    fa2. r4 |
    R1 |
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { fa'1 | solb'~ | solb'2 do' | reb' }
      { reb'1 | mib'~ | mib'2 la | sib2 }
      { s1\f | s\p }
    >> solb4 mib |
    fa2 fa |
    sib1-\sug\fp~ |
    sib-\sug\cresc |
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { fa'1 | }
      { re'1 | }
      { s1\!-\sug\f }
    >>
    mib'4 \clef "bass"
  }
  \tag #'basso {
    \clef "bass" <>\fp \rt#4 fa8 \rt#4 fa |
    \rt#4 fa <>\f \rt#4 fa |
    \rt#4 mi <>\p \rt#4 mi |
    \rt#4 fa \rt#4 fa |
    <>\cresc \rt#4 re \rt#4 re |
    <>\!\fp \rt#4 re \rt#4 re |
    \rt#4 do \rt#4 do |
    <>\cresc \rt#4 fa \rt#4 fa |
    \rt#4 fa \rt#4 fa |
    <>\! fa4 fa8. fa16 fa4 fa |
    fa2. r4 |
    R1*6 |
    <>\fp \rt#4 sib,8 \rt#4 sib, |
    <>\cresc \rt#4 sib, \rt#4 sib, |
    <>\!-\sug\f \rt#4 sib, \rt#4 sib, |
    mib4
  }
>> mib8. mib16 mib2 |
lab\f lab, |
sib,4. sib,8 sib,4. sib,8 |
mib2 r |
mib2 r |
mib r |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    \clef "tenor"
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { fa4 do'2 do'4 | reb'1 | sib | la1 | la2 }
      { fa4 la2 la4 | sib1 | sol | fa1 | fa2 }
      { s1-\sug\fp | s-\sug\cresc | s-\sug\f }
    >>
    \clef "bass"
  }
  \tag #'basso {
    <>\fp \rt#4 fa8 \rt#4 fa |
    <>\cresc \rt#4 fa \rt#4 fa |
    <>\!\f \rt#4 fa \rt#4 fa |
    fa4 fa fa fa |
    fa2
  }
>> r2 |
<< sib,1 { s2\f s2\p } >> |
mib4 mib-.\f sol-. sib-. |
mib' mib fa fa, |
\rt#4 sib,8 \rt#4 sib, |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    \clef "tenor"
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { re'1 | sol | lab | sol | lab1 | }
      { lab1 | mib | fa | mib | fa | }
      { s1-\sug\fp | s1 | s1-\sug\fp }
    >>
    sol1-\sug\fp | \clef "bass"
  }
  \tag #'basso {
    <>\fp \rt#4 sib,8 \rt#4 sib, |
    \rt#4 sib,8 \rt#4 sib, |
    <>\fp \rt#4 sib,8 \rt#4 sib, |
    \rt#4 mib \rt#4 mib |
    \rt#4 re \rt#4 re |
    <>\fp \rt#4 mib <>\fp \rt#4 si, |
  }
>>
do8\ff do mib sol do' sol mib sol |
\rt#4 do8 mib mib mi mi |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    \clef "tenor" fa4 la-\sug\sf( do' fa') |
    mi'1 |
    fa'4 la-\sug\sf( do' fa') |
    mi'1~ |
    mi' |
    mib'!~ |
    mib'2.\fermata r4 |
    R1 | <>\fp
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { fa'1 | solb'~ | solb'2 do' | }
      { reb'1 | mib'~ | mib'2 la | }
    >>
    sib4 r mib r |
    fa r fa, r |
    R1 | <>\fp
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { fa'1 | solb' | }
      { reb'1 | mib' | }
    >>
    \clef "bass"
  }
  \tag #'basso {
    fa1 |
    solb\sf |
    fa |
    solb2~ solb4. solb8 |
    solb4 solb8. solb16 solb4 solb |
    fa4 fa8. fa16 fa4 fa |
    fa2.\fermata r4 |
    R1*9 |
  }
>>
R1^\fermataMarkup |
sib,\fp |
mib4 mib\f sol sib |
mib' mib fa fa, |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    \clef "tenor" sib1 |
    sib2.-\sug\ff lab4 |
    sol1~ |
    sol1 |
    fa |
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { s4 re'( dod' re') |
        s re' re' re' |
        s do' do' do' |
        s reb' reb' reb' |
        s re'! re' re' |
        re' re'( mib' do') | }
      { s4 sib( la sib) |
        s sib sib sib |
        s sib sib sib |
        s sib sib sib |
        s sib sib sib |
        sib \once\slurDashed sib( do' la) | }
      { r4 s2.\p | r4 s2. | r4 s2. | r4 s2. | r4 s2. | }
    >>
    sib2 sib |
    sib2.-\sug\ff lab4 |
    sol1-\sug\p |
    sol |
    fa |
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { s4 re'( dod' re') |
        s re' re' re' |
        s do' do' do' |
        s reb' reb' reb' |
        s re'! re' re' |
        re'-. re'( mib' do') | }
      { s4 sib( la sib) |
        s sib sib sib |
        s sib sib sib |
        s sib sib sib |
        s sib sib sib |
        sib-. sib( do' la) | }
      { r4 s2.\p | r4 s2. | r4 s2. | r4 s2. | r4 s2. | }
    >>
    sib4 r mib'2\p~ |
    mib'2(-\sug\< fa'4 sol')\! |
    sol'1 |
    fa'4-\sug\p( mib' re' dod') |
    re'4 r \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { re'2 | mib' do' | re'4 }
      { sib2 | do' la | sib4 }
      { s2\< | s\! s\p }
    >> r4 mib'2~ |
    mib'( fa'4 sol') |
    sol'1 |
    fa'4 mib' re' dod' |
    re'2 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { re'2 | mib' do' | }
      { sib2 | do' la | }
    >>
    sib1 |
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { sol'2.( fa'4) | fa'1 | mib'2.( re'4) | re' }
      { mib'2.( re'4) | re'1 | do'2.( sib4) | sib4 }
      { s1*2\f | s1\p }
    >>
  }
  \tag #'basso {
    sib,8 sib sib sib \rt#4 sib |
    <>\ff \rt#6 sib lab8 lab |
    <>\p \rt#4 sol \rt#4 sol |
    \rt#4 sol \rt#4 sol |
    fa1 | sol\p | re | mib | mi | fa | fa, |
    sib,8 sib sib sib \rt#4 sib |
    <>\ff \rt#6 sib8 lab lab |
    \rt#4 sol \rt#4 sol |
    \rt#4 sol \rt#4 sol |
    fa1 | sol\p | re | mib | mi | fa~ | fa2 fad | sol r |
    R1*4 |
    fa,1 |
    sib,4 r r2 |
    R1*4 |
    fa,1 | sib,~ | sib,-\sug\f~ | sib,~ | sib,-\sug\p | sib,4
  }
>> sib,2-\sug\ff do8 re |
mib do re mib fa4 mib |
re sib8 sib sib4 do' |
re' sib la sol |
\custosNote fad
