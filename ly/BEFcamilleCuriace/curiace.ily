\clef "vhaute-contre" R1*16 |
r2 r4 fa |
sib2. do'4 |
re'2. fa'4 |
lab'2. lab4 |
sol sol r mib' |
\appoggiatura re' do'2. do'4 |
sib4. sib8 sib4. fa8 |
sol2 r4 sol' |
\appoggiatura fa'4 mib'2 mib' |
r4 do' mib' do' |
la2 r4 do' |
reb'2 reb'4. reb'8 |
mi'2 mi'4. mi'8 |
fa'4 fa r2 |
R1 |
re'!2 sib4. sib8 |
sol2 r |
r4 sol' fa' la |
sib2 r |
R1 |
r4 mib' re' do' |
sib2 r |
r4 mib' re' do' |
si si r si |
do'2 r4 re' |
mib'1~ |
mib'4 sol' mib' re' |
do' la do' fa' |
mi'1 |
r4 la do' fa' |
mi'2 mi'4. mi'8 |
mi'4. mi'8 mi'4. mi'8 |
mib'!2.\fermata r4 |
r2\fermata r |
R1*5 |
r2 r4 fa |
sib( do') reb' mib' |
fa'2 reb'4 sib |
\appoggiatura sib la2 la |
R1^\fermataMarkup |
re'!2 sib4. sib8 |
sol2 r |
r4 sol' fa' la |
sib2 r4 re' |
re'2. do'4 |
si2. si4 |
sib!2. sib4 |
la( do') do'2 |
R1*3 |
r2 r4 sib |
sib1~ |
sib4 sib do' la |
sib2. re'4 |
re'2. do'4 |
si2. si4 |
sib!2. sib4 |
la( do') do'2 |
R1*3 |
r2 r4 sib |
sib1~ |
sib4 sib do' la |
sib2 r |
R1*3 |
r2 re' |
mib'2. do'4 |
sib2 r |
R1*3 |
r2 re' |
mib'2. do'4 |
sib2 r |
R1*7 |
\stopStaff
