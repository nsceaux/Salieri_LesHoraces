\clef "treble"
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''1~ |
    fa''2. la''4 |
    sib''1 |
    la''2. la''4 |
    si''4. do'''8 re'''4. do'''8 |
    si''1 |
    do''' |
    la''4 la' sib' do'' |
    re'' do'' re'' mi'' |
    fa''4 }
  { la'1~ |
    la'2. do''4 |
    do''1 |
    do''2. do''4 |
    si'4. do''8 re''4. mi''8 |
    fa''1 |
    mi'' |
    fa''4 fa' sol' la' |
    sib' la' sib' sol' |
    la'4 }
  { s1-\sug\fp | s2. s4-\sug\f | s1 | s1-\sug\p | s1-\sug\cresc |
    s-\sug\fp | s | s-\sug\cresc | s | s4\! }
>> fa'8. fa'16 fa'4 fa' |
fa'2 r4 r |
R1*6 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sib'4 sib'2 do''4 | re'' mib'' fa'' re'' | lab''1 | sol''2~ sol''4 }
  { sib'4 sib'2 la'4 | sib' do'' re'' sib' | fa''1 | mib''2~ mib''4 }
  { s1-\sug\fp | s-\sug\cresc | s\!-\sug\f | }
>> r4 |
r4 r8. \twoVoices #'(oboe1 oboe2 oboi) <<
  { do'''16 do'''4. do'''8 | fa''4. fa''8 fa''4. fa''8 | sol''2 }
  { mib''16 mib''4. mib''8 | re''4. re''8 re''4. re''8 | mib''2 }
  { s8-\sug\f }
>> r2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { r4 \once\slurDashed mib''(-\sug\sf sol'' mib'') |
    do''8 r do''4(-\sug\sf mib'' do'') |
    do''1 | reb'' | mi'' |
    fa''4 fa''8. fa''16 fa''4 fa'' | fa''2 }
  { R1*2 |
    la'1 | sib' | sib' |
    la'4 la'8. la'16 la'4 la' | la'2 }
  { s1*2 | s1-\sug\fp | s-\sug\cresc | s\!-\sug\f }
>> r2 |
R1*3 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''2 fa''4 | lab''1 | sol'' | lab'' | sol'' | si''1 |
    do'''4 sol''2 sol''4~ | sol'' do'''2 do'''4~ | do'''2. sib''4 | la'' }
  { sib'2 re''4 | re''1 | mib'' | re'' | mib'' | fa''1 |
    mib''4 do''2 re''4 | mib''1~ | mib''2. re''4 | do''4 }
  { s2.-\sug\f | s1-\sug\fp | s | s-\sug\fp | s1*2 | s1-\sug\fp | s-\sug\ff }
>> r4 r2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sib''1 | la''4 }
  { sib'1 | la'4 }
  { s1-\sug\sf }
>> r4 r2 |
<>\sf \twoVoices #'(oboe1 oboe2 oboi) <<
  { sib''1~ |
    sib'' |
    do'''4 do'''8. do'''16 do'''4 do''' |
    do'''2.\fermata }
  { sib'1~ |
    sib' |
    mib''4 mib''8. mib''16 mib''4 mib'' |
    mib''2.\fermata }
>> r4 |
R1*9 |
R1^\fermataMarkup |
R1*4 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''1~ | fa''4 \once\slurDashed lab''-\sug\sf( sol'' fa'') |
    mi''1 | \once\slurDashed fa''4( mib''!) mib''2 | }
  { re''2. do''4 | si'1 |
    sib'!4 \once\slurDashed reb''-\sug\sf( do'' sib') |
    \once\slurDashed la'( do'') do''2 | }
  { s1-\sug\ff }
>>
R1*7 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''1~ | fa''4 lab''-\sug\sf sol'' fa'' |
    mi''1 | fa''4( mib''!) mib''2 | }
  { re''2. do''4 | si'1 |
    sib'!4 reb'' do'' sib' | la'( do'') do''2 | }
  { s1-\sug\ff | s-\sug\p | }
>>
R1*10 |
r2 \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''2 | mib'' do'' | sib'4 }
  { sib'2 | do'' la' | sib'4 }
  { s2\< | s\! s\p }
>> r4 r2 |
R1*3 |
r2 \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''2 | mib'' do'' | re'' }
  { sib'2 | do'' la' | sib' }
>> r2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''2.( fa''4) | fa''1 | la'2.( sib'4) | sib'2 }
  { mib''2.( re''4) | re''1 | mib'2.( re'4) | re'2 }
  { s1*2\f | s1\p }
>> r2 |
R1*3 |
