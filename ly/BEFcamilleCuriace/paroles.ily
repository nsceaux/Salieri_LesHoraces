\tag #'camille {
  Cœur in -- sen -- si -- ble ! a -- mant bar -- ba -- re !
  Ain -- si rien ne peut te flé -- chir !
  Cru -- el, peux- tu me fuir !
  Je sens que ma rai -- son s’é -- ga -- re,
  C’en est fait : je me sens mou -- rir.

  Cœur in -- sen -- si -- ble ! a -- mant bar -- ba -- re !
  Rien __ ne peut te flé -- chir !
  Peux- tu me fuir, Cru -- el, peux- tu me fuir !

  Je sens que ma rai -- son s’é -- ga -- re,
  C’en est fait : je me sens mou -- rir.
  O sort cru -- el ! a -- mant bar -- ba -- re !
  C’en est fait : je me sens __ mou -- rir.
  O sort cru -- el ! a -- mant bar -- ba -- re !
  C’en est fait : je me sens __ mou -- rir.
  Je me sens __ mou -- rir.
  Je me sens __ mou -- rir.
}
\tag #'curiace {
  O sort cru -- el ! de -- voir bar -- ba -- re
  Hé -- las ! faut- il vous o -- bé -- ïr ?
  Ca -- mil -- le, il faut me fuir,
  Le Ciel pour ja -- mais nous sé -- pa -- re :
  Oui, c’en est fait il faut par -- tir.

  O sort cru -- el ! de -- voir bar -- ba -- re
  Hé -- las ! faut- il __ vous o -- bé -- ïr ?
  O sort cru -- el ! de -- voir bar -- ba -- re
  faut- il vous o -- bé -- ïr ?

  Le Ciel pour ja -- mais nous sé -- pa -- re :
  Oui, c’en est fait il faut par -- tir.
  O sort cru -- el ! de -- voir bar -- ba -- re
  faut- il __ vous o -- bé -- ïr ?
  O sort cru -- el ! de -- voir bar -- ba -- re
  faut- il __ vous o -- bé -- ïr ?
  Oui, c’en est fait il faut par -- tir.
}
