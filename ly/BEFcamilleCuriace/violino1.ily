\clef "treble" << <fa'' la'>2 \\ fa'\f >> do''4 la' |
<fa' la>2. \grace sol''16 fa''8\f mi''16 fa'' |
sol''2. sib'4\p |
la'2. do''4 |
si'4.\cresc do''8 re''4. mi''8 |
fa''4\!\fp fa''2 fa'4 |
mi' mi'( sol' do'') |
la'4\cresc \rt#4 la'16 \rt#4 sib' \rt#4 do'' |
\rt#4 re'' \rt#4 do'' \rt#4 re'' \rt#4 mi'' |
<>\! fa''4 <fa' la>8. q16 q4 q |
q2. r4 |
R1*6 |
<>\fp \rt#4 sib'16 \rt#4 fa' \rt#4 sib' \rt#4 do'' |
<>\cresc \rt#4 re'' \rt#4 mib'' \rt#4 fa'' \rt#4 re'' |
<>\!\f \rt#4 lab'' \rt#4 fa'' \rt#4 re'' \rt#4 sib' |
sol'4 <sol' sib>8. q16 q8( mib'' mib'' mib'') |
r4 r8. do''16 do''4. do''8 |
sib'4. sib'8 sib'4. fa'8 |
sol' r sol''4\sf( sib'' sol'') |
mib''8 r mib''4\sf( sol'' mib'') |
do''8 r do''4\sf( mib'' do'') |
la'16\fp do'' do'' do'' \rt#4 do'' \rt#8 do'' |
<>\cresc \rt#8 reb''16 \rt#8 reb'' |
<>\!\f << { \rt#8 mi''16 \rt#8 mi'' | }
  \\ { \rt#8 sib' \rt#8 sib' | } >>
<fa'' la'>4 <fa' la>8. q16 q4 q |
q2 r |
<sib fa' re''!>2\f sib'4.\p sib'8 |
sol'4 mib'8.\f mib'16 sol'4 sib' |
mib'' << { sol''4 fa'' la' } \\ { sib'4 sib' mib' } >> |
<re' sib'>2. sib'8( lab'') |
lab''2\fp fa''4 re'' |
mib''8 r mib''4( re'' do'') |
sib'\fp( lab''2 re''4) |
mib''8 r mib''4( re'' do'') |
si'4 si'2 si'8 lab'' |
sol''\fp sol''4 sol''\fp sol'' sol''8~ |
sol''\ff << { sol''4 sol'' sol'' sol''8 | \rt#4 sol''16 }
  \\ { mib''4 mib'' mib'' mib''8 | \rt#4 mib''16 }
>> \rt#4 mib''16 \rt#4 do'' \rt#4 sib' |
la'4 r r2 |
r4 reb''\p( do'' sib') |
la'4 r r2 |
r4 r8 sib'\f reb''2~ |
reb''4 <<
  { sib''8. sib''16 sib''4 sib'' |
    do''' do'''8. do'''16 do'''4 do''' |
    do'''2.\fermata } \\
  { reb''8. reb''16 reb''4 sib' |
    la' mib''8. mib''16 mib''4 mib'' |
    mib''2. }
>> r4 |
R1*9 |
R1^\fermataMarkup |
<sib fa' re''!>2\f sib'4.\p sib'8 |
sol'4 mib'8.\f mib'16 sol'4 sib' |
mib'' << { sol''4 fa'' la' } \\ { sib' sib' mib' } >> |
<re' sib'>2. fa''4\ff |
fa''1~ |
fa''4 lab''\sf( sol'' fa'') |
mi''2. mi''4 |
fa''( mib''!) mib''2 |
r4 re''\p( dod'' re'') |
r4 re'' re'' re'' |
r do'' do'' do'' |
r4 reb'' reb'' reb'' |
r re''! re'' re'' |
re'' re''( mib'' do'') |
sib'2.( fa''4)\ff |
fa''1~ |
fa''4\p lab''\sf sol'' fa'' |
mi''2. mi''4 |
fa''( mib''!) mib''2 |
r4 re''\p( dod'' re'') |
r4 re'' re'' re'' |
r4 do'' do'' do'' |
r4 reb'' reb'' reb'' |
r re''! re'' re'' |
re''-. re''( mib'' do'') |
sib' r sol''2\p~ |
sol''\<( la''4 sib'')\! |
sib''1 |
la''4\p( sol'' fa'' mi'') |
fa''1~ |
fa'' |
sib'4 r sol''2\p~ |
sol''\<( la''4 sib'')\! |
sib''1 |
la''4( sol'' fa'' mi'') |
fa''1~ |
fa'' |
sib'4 r r2 |
sol''2.(\f fa''4) |
fa''1 |
la'2.(\p sib'4) |
sib' sib2\ff do'8 re' |
mib' do' re' mib' fa' sol' mib' fa' |
re'4 sib'8 sib' sib'4 do'' |
re'' sib' la' sol' |
\custosNote fad'4
