\clef "alto" <>-\sug\fp \rt#8 do'16 \rt#8 do' |
\rt#12 do' <>-\sug\f \rt#4 fa' |
\rt#8 mi' <>-\sug\p \rt#8 mi' |
\rt#8 fa' \rt#8 fa' |
<>-\sug\cresc \rt#8 fa' \rt#8 fa' |
<>\!-\sug\fp \rt#8 fa' \rt#8 fa' |
\rt#4 mi' \rt#8 do' \rt#4 do' |
\rt#4 do'-\sug\cresc \rt#4 la \rt#4 sib \rt#4 do' |
\rt#4 re' \rt#4 do' \rt#4 re' \rt#4 mi' |
<>\! fa'4 do'8. do'16 do'4 do' |
do'2. r4 |
R1*6 |
<>-\sug\fp \rt#4 re'16 \rt#4 fa' \rt#4 re' \rt#4 do' |
<>-\sug\cresc \rt#8 sib \rt#8 sib |
<>\!-\sug\f \rt#12 sib \rt#4 re' |
mib'4 mib8. mib16 mib2 |
r4 r8. mib'16 mib'4. mib'8 |
re'4. re'8 re'4. re'8 |
mib'2 r |
sol'1~ |
sol' |
<>-\sug\fp \rt#4 fa'8 \rt#4 fa' |
<>-\sug\cresc \rt#4 fa' \rt#4 fa' |
<>\!-\sug\f \rt#4 fa' \rt#4 fa' |
fa'4 fa fa fa |
fa2 r |
<< sib1 { s2\f s2\p } >> |
mib'4 mib-\sug\f sol sib |
mib' mib fa fa |
sib16 re' re' re' \rt#4 re' \rt#8 re' |
<>-\sug\fp \rt#8 re' \rt#8 re' |
\rt#8 mib' \rt#8 mib' |
<>-\sug\fp \rt#8 re' \rt#8 re' |
\rt#8 mib' \rt#8 mib' |
fa'8 fa'4 fa' fa' fa'8 |
<>\fp \rt#4 mib'8 <>\fp \rt#4 si |
do'8\ff do' mib' sol' do'' sol' mib' sol' |
\rt#4 do' mib' mib' mi' mi' |
fa'1 |
<>-\sug\sf solb' |
fa' |
solb'2~ solb'4. solb'8 |
solb'4 solb'8. solb'16 solb'4 solb' |
fa'4 fa'8. fa'16 fa'4 fa' |
fa'2.\fermata r4 |
R1*9 |
R1^\fermataMarkup |
<>-\sug\fp sib1 |
mib'4 mib-\sug\f sol sib |
mib' mib' fa' fa |
sib1 |
re'-\sug\ff~ |
re' |
reb'4 sib( do' reb') |
do'( la') la'2 |
R1 |
<>-\sug\p fa'1 |
sol'~ |
sol' |
fa'~ |
fa'2. mib'4 |
re'1~ |
re'-\sug\ff~ |
re'-\sug\p |
reb'4 sib do' reb' |
do'( la') la'2 |
R1 |
<>-\sug\p fa'1 |
sol'~ |
sol' |
fa'~ |
fa'2 mib' |
re'4 r <>-\sug\p sol'2~ |
sol'(-\sug\< la'4 sib')\! |
sib'1 |
la'4-\sug\p( sol' fa' mi') |
fa'4 r sib2\< |
do'\! la-\sug\p |
sib4 r <>-\sug\p sol'2~ |
sol'(\< la'4 sib'\!) |
sib'1 |
la'4( sol' fa' mi') |
fa'4 r sib2 |
do' la |
sib r |
<>\f << { sol'2.( fa'4) | fa'1 | mib'2.( re'4) | re' }
  \\ { mib'2.( re'4) | re'1 | do'2.(-\p sib4) | sib }
>> sib2-\sug\ff do'8 re' |
mib' do' re' mib' fa'4 mib' |
re' sib'8 sib' sib'4 do'' |
re'' sib' la' sol' |
\custosNote fad'4
