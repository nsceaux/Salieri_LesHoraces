\clef "treble" R1*10 |
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { r2 r4 fa'-\sug\f |
    sib'( do'' reb'' mib'') |
    fa''2. reb''8 sib' |
    \grace sib'4 la'2\p la'4 la' |
    la'2 mib'' |
    r4 reb'' r solb'' |
    r fa'' r la' |
    sib'4 }
  { R1*2 |
    fa'1\f |
    solb'1-\sug\p ~ |
    solb'2 fa' |
    r4 fa' r mib'' |
    r reb'' r do'' |
    sib' }
>> r4 r2 |
R1*31 |
r2\fermata r4
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { fa'4 |
    sib'( do'' reb'' mib'') |
    fa''2.\fp reb''8 sib' |
    \grace sib'4 la'2 la'4 la' |
    la'2 mib'' |
    r4 reb'' r solb'' |
    r fa'' r do'' |
    sib'4 do'' reb'' mib'' |
    fa''2.\fp reb''8 sib' |
    \grace sib'4 la'2 la' | }
  { r4 |
    R1 |
    fa'1-\sug\fp |
    solb' |
    solb'2 fa' |
    r4 sib' r mib'' |
    r reb'' r la' |
    sib'4 r r2 |
    fa'1-\sug\fp |
    solb' | }
>>
R1^\fermataMarkup |
R1*4 |
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { fa''1~ | fa''4 \once\slurDashed lab''-\sug\sf( sol'' fa'') |
    mi''1 | \once\slurDashed fa''4( mib''!) mib''2 | }
  { re''2. do''4 | si'1 |
    sib'!4 \once\slurDashed reb''-\sug\sf( do'' sib') |
    \once\slurDashed la'( do'') do''2 | }
  { s1-\sug\ff }
>>
R1*7 |
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { fa''1~ | fa''4 lab''-\sug\sf sol'' fa'' |
    mi''1 | fa''4( mib''!) mib''2 | }
  { re''2. do''4 | si'1 |
    sib'!4 reb'' do'' sib' | la'( do'') do''2 | }
  { s1-\sug\ff | s-\sug\p }
>>
R1*6 |
r2 \twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { sol''2~ | sol''2( la''4 sib'') | sib''1 | la''4( sol'' fa'' mi'') |
    fa''1~ | fa''~ | fa''4 r sol''2~ | sol''( la''4 sib'') |
    sib''1 | la''4 sol'' fa'' mi'' | fa''1~ | fa'' |
    fa''4 r r2 | sol''2.(\f fa''4) | fa''1 | la'2.(\p sib'4) | sib'2 }
  { sol'2~ | sol'2( la'4 sib') | sib'1 | la'4( sol' fa' mi') |
    fa'1~ | fa' | sib'4 r sol'2~ | sol'( la'4 sib') |
    sib'1 | la'4 sol' fa' mi' | fa'1~ | fa'1 |
    fa'4 r r2 | mib''2.(-\sug\f re''4) | re''1 | mib'2.(-\sug\p re'4) | re'2 }
  { s2\p | s2.-\sug\< s4\! | s1 | s-\sug\p | s1*2 |
    s2 s-\sug\p | s2.-\sug\< s4\! }
>> r2 |
R1*3 |
