\clef "vhaute-contre" R1*29 |
r2 sol'4. sol'8 |
sol'2 sol'4. sol'8 |
mi'2 mi'4^\p mi' |
re'2 re'4 re' |
do'2 r\fermata |
R1*4 |
R1^\fermataMarkup |
r2 r4\fermata sol8. sol16 |
do'4 do' r2\fermata r2 r4\fermata mi'8. do'16 |
fa'2 r\fermata |
la'2.\fermata la'4 |
la'2. la'4 |
la'2 la'4 la' |
sol' sol' r8 sol' sol' sol' |
sol'4. sol'8 sol' sol' sol' sol' |
sol'4 sol'8 sol' sol'4 sol' |
sol'2. sol'4 |
sol'2 r |
R1*2 |
r4 sol'8 sol' sol'4 mi'8 do' |
mi' mi' mi' mi' mi'4 mi' |
la'2 r |
R1 |
r4 sol'8 sol' sol'4 sol'8 sol' |
sol'4 sol'8 sol' sol'4 sol'8 sol' |
sol'2 r |
r4 sol'8 sol' mi'2 |
r4 sol'8 sol' sol'4 sol'8 sol' |
sol'1 |
R1*6 |
r4 re' re' re' |
sol'2 sol' |
sol' sol' |
sol' r |
R1 |
r4 la'!8 la' la'4 la'8 la' |
la' la' la' la' la'4 la' |
la'2~ la'4 r |
re'2. la4 |
re'4 la re' fa'8 fa' |
re'2 re'4 la' |
sib'4. sib'8 la'4 la' |
la'2 r |
r4 la' la' la' |
sib'1~ |
sib'4 re' re' do'' |
sib'2 sib' |
sib' sib' |
sib' r4 sol8^\p sol |
lab4 la sib si |
do'2 sib4 la8 la |
sib4 si do' re' |
mib' sol' sol' sol' |
sol'1~ |
sol'4 sol' sol' sol' |
sol'2 sol' |
sol' sol' |
sol' sol' |
si'! do'' |
si'1~ |
si'~ |
si'2 r |
R1*15 |
