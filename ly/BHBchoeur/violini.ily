\clef "treble"
<<
  \tag #'violino1 {
    << <do''' mi''>4. \\ sol' >>
  }
  \tag #'violino2 {
    << mi''4. \\ do'' >>
  }
>> do'8 mi'4 sol' |
<<
  \tag #'violino1 { do''2 }
  \tag #'violino2 { mi'2 }
>> r2 |
R1*2 |
r2 r4 la'8.\f la'16 |
<<
  \tag #'violino1 {
    si'!4 si'8. si'16 do''4 do''8. do''16 |
    re''4. re''8 si'4. si'8 |
    do''2
  }
  \tag #'violino2 {
    sold'4 sold'8. sold'16 la'4 la'8. la'16 |
    si'4. si'8 sold'4. sold'8 |
    la'2
  }
>> r2 |
R1*4 |
<<
  \tag #'violino1 {
    \once\tieUp <sol'' si' re'>1~ |
    << { \once\stemUp \smallNotes sol''2 }
      \new Voice { \voiceTwo do''2 }
    >> mi'4\p mi' |
    re'2 re'4 re' |
  }
  \tag #'violino2 {
    <si re'>1 |
    <do' mi'>2 do'-\sug\p~ |
    do' si |
  }
>>
do'4. do'8\f mi'4 sol' |
<sol mi' do''>4 r r2 |
q4 r r <<
  \tag #'violino1 {
    do''4~ |
    do'' do''2 do''4 |
    la'4 la'8. la'16 la'4 la'8. la'16 |
    si'!4 si'8. si'16 do''4 do''8. do''16 |
    re''4. re''8 si'4. mi''8 |
    do''2
  }
  \tag #'violino2 {
    do'4~ |
    do' do'2 do'4 |
    do'4 do'8. do'16 do'4 la'8. la'16 |
    sold'4 sold'8. sold'16 la'4 la'8. la'16 |
    si'4. si'8 sold'4. sold'8 |
    la'2
  }
>> r4 do''8. la'16 |
<la fad' re''>4 r r2 |
q4 r r <<
  \tag #'violino1 {
    re''4~ |
    re'' re''2 re''4 |
    <si' re'>4 q8. q16 q4 re''8. re''16 |
  }
  \tag #'violino2 {
    re'4~ |
    re' re'2 re'4~ |
    re' <re' si>8. q16 q4 r |
  }
>>
%%%
<<
  \tag #'violino1 {
    re''2 si'4. sol'8 |
    do''2 mi'4\p mi' |
    re'2 sol'4\f sol' |
  }
  \tag #'violino2 {
    <re' si>1 |
    << mi'2 \\ do' >> do'4-\sug\p do' |
    do'2 si4-\sug\f si |
  }
>>
<sol mi' do''>2 <do'' mi'>4. q8 |
<sol mi' do'' mi''>2 <mi' do'>4-\sug\p q |
<< re'2 \\ do' >> <re' si>4 q |
do'4.\fermata r8 r2 |
R1*4 |
R1^\fermataMarkup | \allowPageTurn
<<
  \tag #'violino1 {
    r8. <sol sol'>16\p q8. q16 q4\fermata r4 |
    r8. <mi' do''>16 q8. q16 q4\fermata r4 |
    r8. mi''16 mi''8. mi''16 mi''4\fermata r4 |
    r8. fa''16 fa''8. fa''16 fa''4 r4\fermata |
    <mib'' mib'>2.\fermata\ff mib''4 |
    re''4. fad''16 la'' re'''8 re''' re''' re''' |
    re'''2 do'4. do'8 |
    si8 << { sol''4 sol''8 } \\ { si'4 si'8 }
    >> <sol'' si'>8 si'[ si' do''] |
    re''8 si''4 si''8 do''' mi''4 mi''8 |
    re'' si16[ do'] re' do' si la sol8 re'' mi'' mi'' |
    fa'' si''4 re'''8 re'''4
  }
  \tag #'violino2 {
    r8. <sol mi'>16-\sug\p q8. q16 q4\fermata r4 |
    r8. <la mi'>16 q8. q16 q4\fermata r4 |
    r8. sib'16 sib'8. sib'16 sib'4\fermata r4 |
    r8. la'16 la'8. la'16 la'4 r4\fermata |
    do''2.\fermata\ff <mib' do'>4 |
    << { re'8 re'4 re' re' re'8~ |
        re' re'4 re' re' re'8 |
        re' } \\
      { do' do'4 do' do' do'8~ |
        do' do'4 do' do' do'8 |
        si }
    >> <re' si'>4 q8 q sol'[ sol' la'] |
    si' re''4 re''8 mi'' do''4 do''8 |
    si' si16[ do'] re' do' si la sol8 si' do'' do'' |
    re'' << { fa''4 fa''8 fa''4 } \\ { re''4 re''8 re''4 }
    >>
  }
>> <sol re' si'>4 |
<sol mi' do''>4. sol'16 mi' do'8 do' do' do' |
<sol mi' do'' mi''>4. do''16 sol' mi'4
<<
  \tag #'violino1 {
    <sol mi' do'' mi''>4 | re''16 sol sol sol
  }
  \tag #'violino2 {
    <sol mi' do''>4 | <sol re' si'>16 sol sol sol
  }
>> \rt#4 sol16 sol4 <<
  \tag #'violino1 {
    <re' si' sol''>4 |
    mi''4. do'''16 re''' mi'''8 mi'''4 mi'''8~ |
    mi''' << { mi'''4 mi''' mi''' mi'''8 | mi'''8 }
      \\ { mi''4 mi'' mi'' mi''8 | mi'' }
    >>
  }
  \tag #'violino2 {
    <sol re' si'>4 |
    <sol mi' do''>4. do''16 re'' mi''8 do'' sol' mi' |
    <mi' si'>8 q[ q <mi' do''>] <mi' re''> si''4 si'8 |
    do''8
  }
>> do'16 re' mi' re' do' si la4 << la''4 \\ la' >> |
<re' la' fad''>16 re' re' re' \rt#4 re' re'4 <re' la' fad''> |
<re' si' sol''>8
<<
  \tag #'violino1 {
     re'''8[ re''' re'''] re''' re''' re''' re''' |
     mib''' mib'''4 mib''' mib''' mib'''8 |
     re'''
  }
  \tag #'violino2 {
    << { si''8 si'' si'' si'' si'' si'' si'' |
        do''' do'''4 do''' do''' do'''8 |
        si'' }
      \\ { re''8 re'' re'' re'' re'' re'' re'' |
        mib'' mib''4 mib'' mib'' mib''8 |
        re'' }
    >>
  }
>> sol16 sol sol8 sol sol sol sol sol |
do'4 <sol mi' do'' mi''!>8 <do'' mi''> <sol mi' do''>4 r |
r4 <sol mi' do'' sol''>8 <do'' sol''> <sol mi' do'' mi''>4
<< { sol''8 sol'' } \\ { do''8 do'' } >> |
sol''16 do''' si'' la'' sol'' la'' sol'' fa'' mi'' la'' sol'' fa'' mi'' fa'' mi'' re'' |
do'' sol' la' si' do'' sol' la' si' do'' sol' la' si' do'' sol' la' si' |
do'' sol' la' si' do'' re'' mi'' fa'' sol'' fa'' mi'' re'' do'' sib' la' sol' |
fa' do' re' mi' fa' sol' la' sib' do'' fa' sol' la' sib' do'' re'' mi'' |
fa'' la'' sol'' fa'' mi'' re'' do'' sib' la' fa'' mi'' re'' do'' sib' la' sol' |
<<
  \tag #'violino1 {
    \rt#8 fa'16 \rt#8 mib' |
  }
  \tag #'violino2 {
    fa'16 do'' do'' do'' \rt#4 do'' \rt#8 do'' |
  }
>>
re'16 re' fad' fad' la' la' re'' re'' fad'' fad'' la'' la'' re''' re''' la'' la'' |
fad'' fad'' la'' la'' re''' re''' la'' la'' fad'' fad'' re'' re'' la' la' fad' fad' |
<<
  \tag #'violino1 {
    sol'16 sol'' sol'' sol'' \rt#4 sol'' \rt#8 sol'' |
    \rt#8 sol'' \rt#8 sol'' |
    sol''16 sol sol sol \rt#4 sol \rt#8 sol |
    do'4 sol''8 sol'' sol''4 sol''8 sol'' |
    la''8 << { la''4 la'' la'' la''8 |
        la'' la''4 la'' la'' la''8~ |
        la'' la''4 la'' la'' la''8 |
        \rt#4 re'''16 \rt#4 re'''16 \rt#4 re'''16 }
      \\ { la'4 la' la' la'8 |
        la' la'4 la'8 la'2 |
        la'1 |
        <>\ff \rt#4 re''16 \rt#4 re'' \rt#4 re'' }
    >> \rt#4 la''16 |
    \rt#4 re''' \rt#4 la'' \rt#4 re''' \rt#4 fa''' |
    \rt#8 re''' re'''4 la'' |
    sib''4.*11/12 la''32 sol'' fa'' mi'' re'' dod''4 dod''' |
    re'''16 re' re' re' \rt#4 re' \rt#8 re' |
    re'4 <re' la' fa''> q q |
    <re' sib' fa''>16 re' re' re' \rt#4 re' \rt#8 re' |
    re'4 <re' sib' fa''> q <re' la' fad''> |
    << { \rt#8 sol''16 \rt#8 sol'' | \rt#8 lab'' \rt#8 lab'' | }
      \\ { \rt#16 sib'16 | \rt#16 sib' } >>
  }
  \tag #'violino2 {
    sol'16 <re' si!> <re' si> q \rt#4 q <<
      { \rt#8 mib'16 |
        \rt#8 fa' \rt#8 mib' | } \\
      { \rt#8 do' |
        \rt#8 re' \rt#8 do' | }
    >>
    \rt#8 <re' si>16 \rt#8 <re' si>16 |
    <do' mi'!>4 <<
      { mi''!8 mi'' mi''4 } \\ { sol'8 sol' sol'4 }
    >> re''8 re'' |
    dod''8 <<
      { dod'''4 dod''' dod''' dod'''8 |
        re''' re'''4 re''' re''' re'''8 |
        dod''' dod'''4 dod''' dod''' dod'''8 |
        \rt#4 re''16 \rt#4 re'' \rt#4 re'' } \\
      { mi''4 mi'' mi'' mi''8 |
        fa'' fa''4 fa'' fa'' fa''8 |
        mi'' mi''4 mi'' mi'' mi''8 |
        <>\ff \rt#4 re'16 \rt#4 re' \rt#4 re' }
    >> \rt#4 la'16 |
    \rt#4 re'' \rt#4 la' \rt#4 re'' \rt#4 fa'' |
    \rt#8 re'' <<
      { s16 fa'' fa'' fa'' \rt#4 fa'' |
        \rt#8 mi'' \rt#8 mi'' | } \\
      { re''16 re'' re'' re'' \rt#4 re'' |
        \rt#8 re'' \rt#8 dod'' | }
    >>
    <re'' fa''> re' re' re' \rt#4 re' \rt#8 re' |
    re'4 <la fa' re''>4 q q |
    <sib fa' re''>16 sib sib sib \rt#4 sib \rt#8 sib |
    sib4 <sib fa' re''> q do''4 |
    << { \rt#8 sib'16 \rt#8 sib' | \rt#8 re'' \rt#8 re'' }
      \\ { \rt#16 re' | \rt#16 re' } >>
  }
>>
<<
  { \rt#8 sol''16 \rt#8 sol'' | \rt#8 sol''16 \rt#8 sol'' | } \\
  { \rt#8 mib''16 <>\p \rt#8 mib'' |
    <>\< \rt#8 mib''16 \rt#8 mib''16 | <>\! }
>>
<>\ff \rt#8 <sol'' mib''>16 <>\p \rt#8 q |
<>\< \rt#8 q \rt#8 q | <>\!
<< sol''4 \\ mib'' >> <mib' sib' sol''>4\ff q q |
q16 <mib' sol> q q \rt#4 q \rt#8 q |
q4 <mib' sib' sol''>4 q q |
<< \rt#8 sol''16 \\ \rt#8 mib'' >> \rt#8 sol'' |
<<
  \tag #'violino1 {
    \rt#8 si''! \rt#8 do''' |
    \rt#8 re''' \rt#8 mib''' |
    \rt#8 fa''' \rt#8 mib''' |
    re'''8 << { sol'''4 sol''' sol''' sol'''8~ |
        sol''' sol'''4 sol''' sol''' sol'''8 | } \\
      { sol''4 sol'' sol'' sol''8~ |
        sol'' sol''4 sol'' sol'' sol''8 | }
    >>
    <sol'' sol'''>2~ sol''~ |
    sol''1~ |
    sol''~ |
    sol''~ |
    sol'' |
    sol'' |
    << la''2 \\ do'' >> r2 |
  }
  \tag #'violino2 {
    \rt#8 sol''16 \rt#8 sol'' |
    \rt#8 sol''16 \rt#8 sol'' |
    \rt#8 sol''16 \rt#8 sol'' |
    sol''8 << { si''!4 si'' si'' \once\tieDashed si''8~ |
        si'' si''4 si'' si'' si''8 | } \\
      { re''4 re'' re'' \once\tieDashed re''8~ |
        re'' re''4 re'' re'' re''8 | }
    >>
    <re'' si''>2~ q~ |
    q1~ |
    q~ |
    q~ |
    q |
    <mi''! do'''> |
    la''2 r |
  }
>>
r4 << { <re'' si''>4 <mi'' do'''>4. } \\ { sol'4 sol'4. } >>
do'16 re' |
mi'8. mi'16 mi'8. mi'16 mi'4 sol'( |
sib' re') <mi' sol'>2~ |
q1~ |
q |
<< { la'1~ | la'2 si'~ | si'1 | do''2 } \\
  { re'1~ | re'2 sold'~ | sold'1 | la'2 }
>> r2 |
