\clef "vtaille" R1*15 |
r2 r4 <>^\markup\smallCaps "[Les chefs des deux armées]"
sol8. sol16 |
do'2 do'4. do'8 |
do'2. do'8 do' |
do'2 do'4 do' |
la2 la8 r la8. la16 |
si!4 si8 si do'4 do'8 do' |
re'2 si4. mi'8 |
do'2 r4 do'8. la16 |
re'2 re'4. re'8 |
re'2. re'4 |
re' re' re' re' |
si2 si8 r re'8. re'16 |
re'2 si4. sol8 |
do'2 do'4 do' |
sol2 sol4 sol |
do2 r |
R1*7 |
R1^\fermataMarkup |
R1*4 |
R1^\fermataMarkup |
R1*5 |
<>^\markup\smallCaps [Les Horaces & les Curiaces]
r2 r4 sol |
do'2 r4 r8 do' |
mi'2 r4 mi' |
re' re'8 re' sol'4 sol'8 sol' |
mi'2 mi'4 r |
r2 r4 mi' |
mi'2 r4 r8 la' |
fad'4 fad'8 fad' fad'4 fad'8 fad' |
sol'1~ |
sol'~ |
sol'4 sol r sol8 sol |
do'2 r4 r8 do' |
mi'2 sol'4 sol' |
mi'2 r |
R1*2 |
r2 r4 do'8 do' |
fa'2 r |
R1*5 |
r2 sol'4. sol'8 |
mi'!2 r |
R1*6 |
r2 r4 la8 la |
re'2 re'4. re'8 |
fa'2 r |
r fa'4. fa'8 |
re'2 r |
R1*7 |
r2 sol'4. sol'8 |
mib'2 r |
R1*22 |
