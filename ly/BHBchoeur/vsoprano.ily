\clef "vbas-dessus" R1*29 |
r2 <>^\markup\smallCaps [Tous] sol'4. sol'8 |
do''2 do''4. do''8 |
mi''2 do''4^\p do'' |
sol'2 sol'4 sol' |
do'2 r\fermata |
R1*4 |
R1^\fermataMarkup |
R1*4 |
\once\override TextScript.outside-staff-priority = #9999
<>^\markup\smallCaps [Les deux armées]
do''2.\fermata mib''4 |
re''2. re''4 |
re''2 re''4 re'' |
re'' re'' r8 si' si' do'' |
re''4. re''8 mi'' mi'' mi'' mi'' |
re''4 re''8 re'' re''4 mi'' |
fa''2. fa''4 |
mi''2 r |
R1*2 |
r4 mi''8 mi'' mi''4 mi''8 mi'' |
si' si' si' do'' re''4 mi'' |
do''2 r |
R1 |
r4 re''8 re'' re''4 re''8 re'' |
mib''4 mib''8 mib'' mib''4 mib''8 mib'' |
re''2 r |
r4 mi''8 mi'' do''2 |
r4 sol''8 sol'' mi''4 sol''8 sol'' |
sol''1 |
R1*6 |
r4 re'' re'' re'' |
sol''2 sol'' |
sol'' sol'' |
sol'' r |
r4 mi''!8 mi'' mi''4 re''8 re'' |
dod''2. mi''4 |
fa'' fa'' fa'' fa'' |
mi''2 mi''4 r |
re''2. la'4 |
re''4 la' re'' fa''8 fa'' |
re''2 re''4 fa'' |
mi''4. mi''8 mi''4 la'' |
fa''2 r |
r4 fa'' fa'' fa'' |
fa''1~ |
fa''4 fa'' fa'' fad'' |
sol''2 sol'' |
lab'' lab'' |
sol''2 r4 sol'8^\p sol' |
lab'4 la' sib' si' |
do''2^\ff sib'4^\p la'8 la' |
sib'4^\< si' do'' re''\! |
mib'' sol''^\ff sol'' sol'' |
sol''1~ |
sol''4 sol'' sol'' sol'' |
sol''2 sol'' |
sol'' sol'' |
sol'' sol'' |
sol'' sol'' |
sol''1~ |
sol''~ |
sol''2 r |
R1*15 |
