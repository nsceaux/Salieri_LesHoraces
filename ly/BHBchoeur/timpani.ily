\clef "bass" R1*33 |
r4 r8\fermata sol,16\ff sol, do8 do do do |
do8 do16 do do8 do do do[ do do] |
do4 r8 do16 do do4 r8 do16 do |
do4 r8 do16 do do4 r8 do16 do |
do4 r8 do do4 do |
do2 r\fermata |
R1*10 |
r2 r4 sol, |
do4. do16 do \rt#4 do8 |
do4. do16 do \rt#4 do8 |
sol,4 sol,8 sol, sol,4 sol, |
do r r2 |
R1 |
r8 do16 do do8 do do4 r |
R1*2 |
r2 \rt#8 sol,16 |
\rt#8 sol, sol,4 sol,8. sol,16 |
do2 r4 do8. do16 |
do2 r4 do8. do16 |
\rt#16 do16 |
\rt#16 do16 |
\rt#16 do16 |
\rt#16 do16 |
\rt#16 do16 |
do2 r |
R1*4 |
r2 \rt#8 sol,16 |
do4 r r2 |
R1*20 |
r2 \rt#8 sol,16 |
\rt#16 sol, |
\rt#16 sol, |
\rt#16 sol, |
\rt#16 sol, |
\rt#8 sol,16 \rt#8 sol,16 |
sol,2 r |
R1*15 |
