\clef "treble"
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do'''4 s2. |
    s4 mi'8. mi'16 mi'4 fa' |
    sol' sol'8. sol'16 sol'4 la' |
    sib' la' sol'4. sol'8 |
    la'4 la'8. la'16 la'4 s |
    sold''2 la'' |
    si'' sold'' |
    la'' s |
    s4 fad'8. fad'16 fad'4 sol' |
    la' la'8. la'16 la'4 si' |
    do'' si' la'4. la'8 |
    si'4 si'8. si'16 si'4 s |
    si'1 |
    do''2 do''4 do'' |
    do''2 si' | }
  { mi''4 s2. |
    s4 do'8. do'16 do'4 re' |
    mi' mi'8. mi'16 mi'4 fa' |
    sol' fa' mi'4. mi'8 |
    fa'4 fa'8. fa'16 fa'4 s |
    si'!2 do'' |
    re'' si' |
    do'' s |
    s4 re'8. re'16 re'4 mi' |
    fad' fad'8. fad'16 fad'4 sol' |
    la' sol' fad'4. fad'8 |
    sol'4 re'8. re'16 re'4 s |
    re'1 |
    do'2 mi'4 mi' |
    re'1 | }
  { s4 r4 r2 | r4 s2. | s1*2 | s2. r4 | s1-\sug\f | s1 | s2 r | r4 s2. |
    s1*2 | s2. r4 | s1 | s2 s-\sug\p }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''4 s2. |
    s4 mi'8. mi'16 mi'4 fa' |
    sol' sol'8. sol'16 sol'4 la' |
    sib' la' sol'4. sol'8 |
    la'4 la'8. la'16 la'4 s |
    sold''2 la'' |
    si'' sold'' |
    la'' s |
    s4 fad'8. fad'16 fad'4 sol' |
    la' la'8. la'16 la'4 si' |
    do'' si' la'4. la'8 |
    si'4 si'8. si'16 si'4 s | }
  { do'4 s2. |
    s4 do'8. do'16 do'4 re' |
    mi' mi'8. mi'16 mi'4 fa' |
    sol' fa' mi'4. mi'8 |
    fa'4 fa'8. fa'16 fa'4 s |
    si'!2 do'' |
    re'' si' |
    do'' s |
    s4 re'8. re'16 re'4 mi' |
    fad' fad'8. fad'16 fad'4 sol' |
    la' sol' fad'4. fad'8 |
    sol'4 re'8. re'16 re'4 s | }
  { s4 r r2 | r4 s2.-\sug\f | s1*2 | s2. r4 | s1*2 |
    s2 r | r4 s2. | s1*2 | s2. r4 | }
>>
%%%
\twoVoices #'(oboe1 oboe2 oboi) <<
  { si'1 | do''2 }
  { re'1 | do'2 }
>> r2 |
r sol'4.-\sug\f sol'8 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''2 do''4. do''8 |
    mi''2 do''4 do'' |
    do''2 si' |
    do''4 }
  { mi'2 mi'4. mi'8 |
    sol'2 mi'4 mi' |
    re'1 |
    do'4 }
  { s1 | s2 s\p }
>> r4\fermata r2 |
R1*4 |
R1^\fermataMarkup |
R1*4 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { mib''2.\fermata }
  { la'2.\fermata }
  { s-\sug\ff }
>> r4 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''1 | la'' | sol''2~ sol''8 si''[ si'' do'''] |
    re'''4 si'' do'''4. mi''8 | re''2. mi''4 | fa''1 | mi''4 }
  { la'1 | do'' | si'2~ si'8 re''[ re'' do''] |
    si'4 re'' mi''4. do''8 | si'2. do''4 | si'1 | do''4 }
>> r4 r2 |
R1*2 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''2 mi''4 | si''1 | la''2~ la''4 }
  { do''2 do''4 | si' mi''2 re''4 | do''2~ do''4 }
>> r4 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''1 |
    sol''4 si''2 si''4 |
    do''' do'''2 do'''4 |
    si''2~ si''4 }
  { la'1 |
    si'4 re''2 re''4 |
    mib'' mib''2 mib''4 |
    re''2~ re''4 }
>> r4 |
R1*2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do'''1~ | do''' | sol'' | la''~ |
    la'' | la''4 do'''2 do'''4 | la''1~ | la'' | }
  { mi''1~ | mi'' | mi'' | fa''~ |
    fa'' | fa''4 la''2 sol''4 | fad''1~ | fad'' | }
>>
sol''1~ |
sol''~ |
sol''~ |
sol'' |
la''~ |
la''~ |
la'' |
re''2.-\sug\ff la'4 |
re''4 la' re'' fa'' |
re''2. \twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''4 | mi''1 | re''2 re''4. re''8 | fa''1 |
    s2 fa''4. fa''8 | re''4 fa''2 fad''4 | sol''1 | lab'' |
    sol''~ | sol''~ | sol''~ | sol''~ |
    sol''4 sol'' sol'' sol'' | sol''1~ |
    sol''4 sol'' sol'' sol'' | sol''2 }
  { re''4~ | re''2 dod'' | re'' re''4. re''8 | re''1 |
    s2 re''4. re''8 | sib'4 re''2 do''4 | sib'1 | sib' |
    mib''~ | mib''~ | mib''~ | mib''~ |
    mib''4 mib'' mib'' mib'' | mib''1~ |
    mib''4 mib'' mib'' mib'' | mib''2 }
  { s4 | s1*3 | r2 s | s1 |
    s1*2 |
    s2. s4\p |
    s1\< |
    s2.\!-\sug\ff s4\p |
    s1\< | s4\! s2.-\sug\ff }
>> sol''2 |
fa'' mib'' |
re'' do'' |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''2 mib'' | re''4 si''2 si''4~ | si''1~ | si''~ |
    si''~ | si''~ | si''~ | si'' | do''' | la''2 }
  { si'2 do'' | si'4 re''2 re''4~ | re''1~ | re''~ |
    re''~ | re''~ | re''~ | re'' | mi''! | fa''2 }
>> r2 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { si''4 do''' }
  { re''4 mi'' }
>> r4 |
R1*8 |
