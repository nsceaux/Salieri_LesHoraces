\clef "vbasse" R1*15 |
r2 r4 sol8. sol16 |
do'2 do'4. do'8 |
do'2. do'8 do' |
do'2 do'4 do' |
la2 la8 r la8. la16 |
sold4 sold8 sold la4 la8 la |
si2 sold4. sold8 |
la2 r4 do'8. la16 |
re'2 re'4. re'8 |
re'2. re'4 |
re' re' re' re' |
si2 si8 r re'8. re'16 |
re'2 si4. sol8 |
do'2 do'4 do' |
sol2 sol4 sol |
do2 r |
R1*7 |
r2 r4^\fermata mi8. fa16 |
sol4 sol r\fermata r |
r2 r4\fermata r8 do' |
sib4 sib r2\fermata |
R1 |
R1^\fermataMarkup |
R1*6 |
r2 r4 r8 sol |
do'2 r4 do' |
si si8 si si4 si8 si |
do'2 do'4 r |
r2 r4 mi |
do'2 r4 r8 do' |
la4 la8 la re'4 re'8 re' |
sol1~ |
sol~ |
sol4 sol r2 |
r2 r4 r8 sol |
do'2 mi'4 mi' |
do'2 r |
R1*2 |
r2 r4 do'8 do' |
la2 r |
R1*5 |
r2 si4. si8 |
do'2 r |
R1*6 |
r2 r4 la8 la |
re'2 re'4. re'8 |
re'2 r |
r re'4. re'8 |
sib2 r |
R1*7 |
r2 sib4. sib8 |
mib'2 r |
R1*22 |
