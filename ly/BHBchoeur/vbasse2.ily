\clef "vbasse" <>^\markup\smallCaps "[Le Grand Sacrificateur]"
r2 r4 r8 sol |
do'4. do'8 do'4 do' |
do'2. do'8 do' |
do'2 do'4 do' |
la2 la4 la8 la |
si!4 si8 si do'4 do'8 do' |
re'2 si4. mi'8 |
do'2 r4 do'8. la16 |
re'2 re'4. re'8 |
re'2. re'4 |
re' re' re' re' |
si2 si4 re'8. re'16 |
re'2 si4. sol8 |
do'2 do'4 do' |
sol2 sol4 sol |
do2 r2 |
R1*22 |
r2 r4\fermata do8. re16 |
mi4 mi r\fermata r4 |
r2 r4\fermata r8 mi |
sol4 sol r2\fermata |
R1 |
R1^\fermataMarkup |
R1*19 |
<>^\markup\smallCaps [Les chefs de l'armée]
r2 r4 sol |
do' do' do' do' |
la2 la4 r |
R1 |
r2 mib'4 mib' |
re'2. re'4 |
re' re' re' re' |
re'2 r |
R1*28 |
<>^\markup\smallCaps [Le grand Prêtre]
r2 r4 re' |
re'1 |
re'2 re'4. re'8 |
si2 si8 r re' re' |
si2 si4 re' |
sol2 r4 re'8 re' |
mi'!2.^! do'8 do' |
la4^! la8 la si4 si8 do' |
sol4 r r2 |
R1 |
r2 r4 r8 mi16 mi |
la8 la16 la la8 si dod' dod' r mi'16 mi' |
dod'2 dod'8 dod' dod' re' |
la4^! r16 la la la re'4. re'8 |
re'8 re' re' si sold4^! r8 mi16 fad |
sold8 sold16 la si4 r8 si16 si mi'8 mi'16 si |
do'4^! do'8 r r2 |

