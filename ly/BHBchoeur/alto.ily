\clef "alto" do'4. do8 mi4 sol |
do'2 r |
R1*2 |
r2 r4 fa'-\sug\f |
mi' mi'2 mi'4~ |
mi' mi'2 mi'4 |
mi'2 r |
R1*4 |
fa'!1 |
mi'2 do'4-\sug\p mi' |
sol'2 sol |
do'4. do'8\f mi'4 sol' |
do' <<
  { mi'8. mi'16 mi'4 fa' |
    sol' sol'8. sol'16 sol'4 la' |
    sib' la' sol'4. sol'8 |
    la'4 la'8. la'16 la'4 } \\
  { do'8. do'16 do'4 re' |
    mi'4 mi'8. mi'16 mi'4 fa' |
    sol' fa' mi'4. mi'8 |
    fa'4 fa'8. fa'16 fa'4 }
>> fa'4 |
mi'2 mi' |
mi'4. mi'8 mi'4. mi'8 |
la2 r4 la' |
re'4 <<
  { fad'8. fad'16 fad'4 sol' |
    la' la'8. la'16 la'4 si' |
    do''4 si' la'4. la'8 | } \\
  { re'8. re'16 re'4 mi' |
    fad' fad'8. fad'16 fad'4 sol' |
    la' sol' fad'4. fad'8 | }
>>
sol'4. sol8 sol4 r |
%%%
fa'1 |
mi'2 do'4\p mi' |
sol'2 sol'4\f sol' |
do'2 do'4. do'8 |
do'2 do'4\p mi' |
sol'2 sol |
do'4.\fermata r8 r2 |
R1*4 |
R1^\fermataMarkup | \allowPageTurn
r8. do'16-\sug\p do'8. do'16 do'4\fermata r |
r8. la16 la8. la16 la4\fermata r |
r8. do'16 do'8. do'16 do'4\fermata r |
r8. do'16 do'8. do'16 do'4 r\fermata |
la'2.\fermata\ff la4 |
la'8 la'4 la' la' la'8~ |
la' la'4 la' la' la'8 |
sol' sol'4 sol'8 sol' re'[ re' si] |
si sol'4 sol' sol' sol'8 |
sol' si16[ do'] re' do' si la \rt#4 sol8 |
sol4 sol8. sol16 sol4 <sol re' si'> |
<sol mi' do''>4. sol'16 mi' do'8 do' do' do' |
do''4. sol'16 mi' do'8 do' do' do' |
si4 si8. si16 si4 sol |
do'8 sol'4 sol' sol' sol'8 |
\rt#4 sold'8 sold' mi'4 mi'8 |
mi' do'16[ re'] mi' re' do' si la4 r |
re'16 re re re \rt#4 re re4 re' |
sol8 sol'[ sol' sol'] sol' sol' sol' sol' |
sol' sol'4 sol' sol' sol'8 |
sol' sol16 sol sol8 sol sol4 sol8 sol |
do'4 << { do''8 do'' } \\ { mi'! mi' } >> do4 r |
r4 << { do''8 do'' } \\ { mi' mi' } >> do4 << { do''8 do'' } \\ { mi' mi' } >> |
<< { \rt#8 mi'16 \rt#8 mi' |
    \rt#8 mi'16 \rt#8 mi' |
    \rt#8 mi'16 \rt#8 mi' |
    \rt#8 fa' \rt#8 fa' |
    \rt#8 fa' \rt#8 fa' |
    fa'4
  } \\
  { \rt#16 sol16 |
    \rt#16 sol |
    \rt#16 sol |
    \rt#16 la |
    \rt#16 la |
    la4 }
>> la'2 sol'4 |
<< { \rt#8 la'16 \rt#8 la' | \rt#8 la'16 \rt#8 la' | }
  \\ { \rt#8 fad'16 \rt#8 fad' | \rt#8 fad'16 \rt#8 fad' | } >>
\rt#8 sol'16 \rt#8 sol' |
\rt#8 sol'16 \rt#8 sol' |
sol'16 sol sol sol \rt#4 sol \rt#8 sol |
do4 do''8 do'' do''4 sib'8 sib' |
la'16 la' mi' mi' dod' dod' la la la' la' mi' mi' dod' dod' la la |
la' la' fa' fa' re' re' la la la' la' fa' fa' re' re' la la |
la'16 la' mi' mi' dod' dod' la la la' la' mi' mi' dod' dod' la la |
<>\ff \rt#12 re'16 \rt#4 la |
\rt#4 re'16 \rt#4 la \rt#4 re' \rt#4 fa' |
\rt#8 re' re'16 la' la' la' \rt#4 la' |
\rt#8 sib' \rt#8 la' |
la'2 r |
r4 re' re' re' |
\rt#4 sib16 \rt#4 sib16 \rt#4 sib16 \rt#4 sib16 |
sib4 sib sib la |
sol16 sol' sol' sol' \rt#4 sol' \rt#8 sol' |
\rt#8 fa' \rt#8 fa' |
mib'2. <>-\sug\p \rt#4 sol16 |
<>\< \rt#4 lab \rt#4 la \rt#4 sib \rt#4 si |
<>\!-\sug\ff \rt#8 do' \rt#4 sib <>-\sug\p \rt#4 la |
<>\< \rt#4 sib \rt#4 si \rt#4 do' \rt#4 re' |
<>\! mib'4 <sol mib' sib'>4-\sug\ff q q |
q2 r |
r4 q q q |
q2 \rt#8 sol'16 |
\rt#8 fa' \rt#8 mib' |
\rt#8 re' \rt#8 do' |
\rt#8 si! \rt#8 do' |
sol16 sol si si re' re' sol' sol' sol sol si si re' re' sol' sol' |
sol sol si si re' re' sol' sol' sol sol si si re' re' sol' sol' |
<sol sol'>2~ sol'~ |
sol'1~ |
sol'~ |
sol'~ |
sol' |
sol' |
fa'2 r |
r4 sol' do'4. do'16 re' |
mi'8. mi'16 mi'8. mi'16 mi'4 sol'( |
sib' re') la2~ |
la1~ |
la~ |
\once\tieDashed la~ |
la2 mi' |
mi'1 |
mi'2 r |
