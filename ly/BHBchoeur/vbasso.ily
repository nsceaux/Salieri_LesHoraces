\clef "vbasse" R1*29 |
r2 sol4. sol8 |
do'2 do'4. do'8 |
do'2 do4^\p mi |
sol2 sol4 sol |
do2 r\fermata |
R1*4 |
R1^\fermataMarkup |
r2 r4\fermata sol8. sol16 |
mi4 mi r2\fermata |
r2 r4\fermata sib8. sib16 |
la2 r\fermata |
fad2.\fermata fad4 |
fad2. fad4 |
do'!2 do'4 do' |
si si r8 sol sol la |
si4. si8 do' do' do' do' |
si4 si8 si si4 do' |
re'2. re'4 |
do'2 r |
R1*2 |
r4 do'8 do' do'4 do'8 do' |
sold sold sold sold sold4. sold8 |
la2 r |
R1 |
r4 si8 si si4 si8 si |
do'4 do'8 do' do'4 do'8 do' |
si2 r |
r4 do'8 do' do2 |
r4 do'8 do' do'4 do'8 do' |
do'1 |
R1*7 |
si!2 do' |
re'4 re' do' do' |
si2 r |
r4 do'8 do' do'4 sib8 sib |
la2. la4 |
la la la la |
la2 la4 r |
re'2. la4 |
re'4 la re' re'8 re' |
re'2 re'4 re' |
sol4. sol8 la4 la |
re'2 r |
r4 re' re' re' |
sib1~ |
sib4 sib sib la |
sol2 sol |
re' re' |
mib' r4 sol8^\sug\p sol |
lab4 la sib si |
do'2 sib4 la8 la |
sib4 si do' re' |
mib' mib' mib' mib' |
mib'1~ |
mib'4 mib' mib' mib' |
mib'2 mib' |
re' do' |
si! do' |
re' do' |
sol1~ |
sol~ |
sol2 r |
R1*15 |
