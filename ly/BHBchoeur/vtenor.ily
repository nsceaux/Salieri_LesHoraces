\clef "vtaille" R1*29 |
r2 sol4. sol8 |
mi'2 mi'4. mi'8 |
sol'2 do'4^\p do' |
do'2 si4 si |
do'2 r\fermata |
R1*4 |
R1^\fermataMarkup |
r2 r4\fermata sol8. sol16 |
mi4 mi r2\fermata |
r2 r4\fermata sib8. sib16 |
la2 r\fermata |
mib'2.\fermata do'4 |
re'2. la4 |
fad'2 fad'4 la' |
re' re' r8 re' re' do' |
si4. sol'8 mi' do' sol sol |
sol4 sol8 sol' sol'4 sol' |
si2. si4 |
do'2 r |
R1*2 |
r4 mi'8 mi' mi'4 sol'8 fa' |
mi' mi' mi' mi' si4 mi' |
mi'2 r |
R1 |
r4 re'8 re' re'4 re'8 re' |
do'4 do'8 mib'? sol'4 sol'8 sol' |
sol2 r |
r4 mi'8 mi' sol'2 |
r4 mi'8 mi' sol'4 mi'8 mi' |
mi'1 |
R1*7 |
re'2 mib' |
fa'4 fa' mib' mib' |
re'2 r |
r4 sol'8 sol' sol'4 sol'8 sol' |
mi'2. mi'4 |
re' re' re' re' |
dod'2 dod'4 r |
re'2. la4 |
re'4 la re' fa'8 fa' |
re'2 re'4 re' |
re'4. re'8 dod'4 dod' |
re'2 r |
r4 fa' fa' fa' |
re'1~ |
re'4 re' re' re' |
re'2 re' |
fa' fa' |
sib r4 sol8^\p sol |
lab4 la sib si |
do'2 sib4 la8 la |
sib4 si do' re' |
mib' mib' mib' mib' |
mib'1~ |
mib'4 mib' mib' mib' |
mib'2 sol' |
fa' mib' |
re' mib' |
fa' mib' |
re'1~ |
re'~ |
re'2 r |
R1*15 |
