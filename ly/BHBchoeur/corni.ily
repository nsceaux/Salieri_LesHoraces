\clef "treble"
<<
  \tag #'(corno1 tromba1 tutti) \new Voice {
    \tag #'tutti \voiceOne
    mi''4.
  }
  \tag #'(corno2 tromba2 tutti) \new Voice {
    \tag #'tutti \voiceTwo
    do''4.
  }
>> <>^"Il corni soli" do'8 mi'4 sol' |
do'' do''8. do''16 do''4 do'' |
do'' do''8. do''16 do''4 do''~ |
do'' do''2 do''4~ |
do'' do'8. do'16 do'4 r |
mi'2-\sug\f mi' |
mi' mi' |
mi' r4 do'' |
re'' re''8. re''16 re''4 re'' |
re'' re''8. re''16 re''4 re'' |
re'' re''2 re''4~ |
re'' sol'8. sol'16 sol'4 r |
sol'1~ |
sol'4 r r2 |
R1 |
r4 r8 do'-\sug\f mi'4 sol' |
do'' do''8. do''16 do''4 do'' |
do'' do''8. do''16 do''4 do''~ |
do'' do''2 do''4~ |
do'' do'8. do'16 do'4 r |
mi'2 mi' |
mi' mi' |
mi' r4 do'' |
re'' re''8. re''16 re''4 re'' |
re'' re''8. re''16 re''4 re''~ |
re''4 re''2 re''4 |
re'' sol'8. sol'16 sol'4 r |
%%%
sol'1 |
sol'2 r |
r sol'4.\f sol'8 |
<<
  \tag #'(corno1 tromba1 tutti) \new Voice {
    \tag #'tutti \voiceOne
    do''2 do''4. do''8 | mi''2
  }
  \tag #'(corno2 tromba2 tutti) \new Voice {
    \tag #'tutti \voiceTwo
    mi'2 mi'4. mi'8 | sol'2
  }
>> do'4\p mi' |
<<
  \tag #'(corno1 tromba1 tutti) \new Voice {
    \tag #'tutti \voiceOne
    sol'1 |
  }
  \tag #'(corno2 tromba2 tutti) \new Voice {
    \tag #'tutti \voiceTwo
    sol1 |
  }
>>
do'4 <<
  \tag #'(tromba1 tromba2 tutti) {
    r8\fermata <>^"Cors tacet" _"Fièrement"
    sol'16 sol' mi'8 do' mi' sol' |
    do''8 sol'16 sol' sol'8 sol'
    <<
      \tag #'(tromba1 tutti) \new Voice {
        \tag #'tutti \voiceOne
        sol'8 mi' sol' do'' |
        mi''4 s8 sol''16 sol'' sol''4 s8 mi''16 mi'' |
        mi''4 s8 do''16 do'' do''4 s8 sol'16 sol' |
        sol'4
      }
      \tag #'(tromba2 tutti) \new Voice {
        \tag #'tutti \voiceTwo
        sol'8 do' mi' sol' |
        do''4 s8 mi''16 mi'' mi''4 s8 do''16 do'' |
        do''4 s8 sol'16 sol' sol'4 s8 mi'16 mi' |
        mi'4
      }
      { s2 | s4 r8 s4. r8 s | s4 r8 s4. r8 s | }
    >> r8 do' do'4 do' |
    do'2 r\fermata |
  }
  \tag #'(corno1 corno2) {
     r4\fermata r2 | R1*4 | R1\fermataMarkup |
  }
>>
R1*4 |
do'2.-\sug\ff\fermata r4 |
<<
  \tag #'(corno1 corno2 tutti) {
    <>^"Corni soli" re''1~ |
    re'' |
    sol' |
    R1*2 |
  }
  \tag #'(tromba1 tromba2) R1*5
>>
r2 r4 <>^"Tutti" sol'4 |
<<
  \tag #'(corno1 tromba1 tutti) \new Voice {
    \tag #'tutti \voiceOne
    do''2 s4 do''8. do''16 |
    mi''2 s4 mi''8. mi''16 |
    re''4 re''8. re''16 re''4 re'' |
    mi''2 mi''4 s |
    s2. mi''8. mi''16 |
    mi''2 s4 mi''8. mi''16 |
  }
  \tag #'(corno2 tromba2 tutti) \new Voice {
    \tag #'tutti \voiceTwo
    mi'2 s4 mi'8. mi'16 |
    do''2 s4 do''8. do''16 |
    sol'4 sol'8. sol'16 sol'4 sol' |
    do''2 do''4 s |
    s2. mi'8. mi'16 |
    do''2 s4 do''8. do''16 |
  }
  { s2 r4 s | s2 r4 s | s1 | s2. r4 |
    r2 r4 s | s2 r4 s | }
>>
re''4 re''8. re''16 re''4 re''8. re''16 |
<<
  \tag #'(corno1 tromba1 tutti) \new Voice {
    \tag #'tutti \voiceOne
    sol''1~ | sol'' | sol''8
  }
  \tag #'(corno2 tromba2 tutti) \new Voice {
    \tag #'tutti \voiceTwo
    sol'1~ | sol' | sol'8
  }
>> sol'16 sol' sol'8 sol' sol'4 sol'8. sol'16 |
<<
  \tag #'(corno1 tromba1 tutti) \new Voice {
    \tag #'tutti \voiceOne
    do''2. do''8. do''16 | mi''2. mi''8. mi''16 |
    sol''1~ | sol''4 sol'8. sol'16 sol'4 sol' |
    do''1 | do''~ | do''~ | do''4
  }
  \tag #'(corno2 tromba2 tutti) \new Voice {
    \tag #'tutti \voiceTwo
    mi'2. mi'8. mi'16 | sol'2. do''8. do''16 |
    mi''1~ | mi''4 mi'8. mi'16 mi'4 mi' |
    do'1 | do'~ | do'~ | do'4
  }
>> do''2 do''4 |
re''2. re''4 |
re'' re'' re'' re'' |
<<
  \tag #'(corno1 tromba1 tutti) \new Voice {
    \tag #'tutti \voiceOne
    sol''2 sol'' |
    sol'' sol'' |
    sol'' re''4. re''8 |
    mi''2
  }
  \tag #'(corno2 tromba2 tutti) \new Voice {
    \tag #'tutti \voiceTwo
    sol'2 sol' |
    sol' sol' |
    sol' sol'4. sol'8 |
    do''2
  }
>> r2 |
mi''1 |
R1*2 |
re''2.-\sug\ff r4 |
R1*3 |
r2 re''4. re''8 |
re''1 |
r2 re''4. re''8 |
re''1 |
re''~ |
re'' |
R1*7 |
r2
<<
  \tag #'(corno1 tromba1 tutti) \new Voice {
    \tag #'tutti \voiceOne
    sol''2 | sol'' sol'' | sol'' sol'' | sol'' sol'' |
    sol''1 | sol'' | re''~ | re''~ |
    re''~ | re''~ | re'' | mi'' | do''2
  }
  \tag #'(corno2 tromba2 tutti) \new Voice {
    \tag #'tutti \voiceTwo
    sol'2 | sol' sol' | sol' sol' | sol' sol' |
    sol'1 | sol' | sol'~ | sol'~ |
    sol'~ | sol'~ | sol' | do'' | do''2
  }
>> r2 |
r4
<<
  \tag #'(corno1 tromba1 tutti) \new Voice {
    \tag #'tutti \voiceOne
    re''4 mi''
  }
  \tag #'(corno2 tromba2 tutti) \new Voice {
    \tag #'tutti \voiceTwo
    sol'4 do''
  }
>> r |
R1*8 |
