\clef "treble"
\twoVoices #'(flauto1 flauto2 flauti) <<
  { do'''4 s2. |
    s4 mi''8. mi''16 mi''4 fa'' |
    sol'' sol''8. sol''16 sol''4 la'' |
    sib'' la'' sol''4. sol''8 |
    la''4 la''8. la''16 la''4 s |
    sold''2 la'' |
    si'' sold'' |
    la'' s |
    s4 fad''8. fad''16 fad''4 sol'' |
    la'' la''8. la''16 la''4 si'' |
    do''' si'' la''4. la''8 |
    si''4 si''8. si''16 si''4 s | }
  { mi''4 s2. |
    s4 do''8. do''16 do''4 re'' |
    mi'' mi''8. mi''16 mi''4 fa'' |
    sol''4 fa'' mi''4. mi''8 |
    fa''4 fa''8. fa''16 fa''4 s |
    si'!2 do'' |
    re'' si' |
    do'' s |
    s4 re''8. re''16 re''4 mi'' |
    fad'' fad''8. fad''16 fad''4 sol'' |
    la'' sol'' fad''4. fad''8 |
    sol''4 re''8. re''16 re''4 s | }
  { s4 r4 r2 | r4 s2. | s1*2 | s2. r4 | s1-\sug\f | s | s2 r | r4 s2. |
    s1*2 | s2. r4 | }
>>
R1*4 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { s4 mi''8. mi''16 mi''4 fa'' |
    sol'' sol''8. sol''16 sol''4 la'' |
    sib'' la'' sol''4. sol''8 |
    la''4 la''8. la''16 la''4 s |
    sold''2 la'' |
    si'' sold'' |
    la'' s |
    s4 fad''8. fad''16 fad''4 sol'' |
    la''4 la''8. la''16 la''4 si'' |
    do'''4 si'' la''4. la''8 |
    si''4 si''8. si''16 si''4 s | }
  { s4 do''8. do''16 do''4 re'' |
    mi'' mi''8. mi''16 mi''4 fa'' |
    sol'' fa'' mi''4. mi''8 |
    fa''4 fa''8. fa''16 fa''4 s |
    si'!2 do'' |
    re'' si' |
    do'' s |
    s4 re''8. re''16 re''4 mi'' |
    fad'' fad''8. fad''16 fad''4 sol'' |
    la'' sol'' fad''4. fad''8 |
    sol''4 re''8. re''16 re''4 s | }
  { r4 s2.-\sug\f | s1*2 | s2. r4 |
    s1*2 | s2 r | r4 s2. | s1*2 | s2. r4 | }
>>
%%%
R1*2 |
r2 sol''4.\f sol''8 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { do'''2 do'''4. do'''8 | mi'''2 mi''4 mi'' | re''1 | do''4 }
  { mi''2 mi''4. mi''8 | do''2 do''4 do'' | do''2 si' | do''4 }
  { s1 | s2 s-\sug\p }
>> r4\fermata r2 |
R1*4 |
R1^\fermataMarkup |
R1*4 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { do'''2.\fermata }
  { mib''2.\fermata }
  { s-\sug\ff }
>> r4 |
re''4. fad''16 la'' re'''8 re''' re''' re''' |
re'''1 |
si''2~ si''8 r r4 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { re'''2 mi''' | re'''2. mi'''4 | fa'''1 | mi'''4 }
  { si''2 do''' | si''2. do'''4 | re'''1 | do'''4 }
>> r4 r2 |
R1*2 |
r4 mi'''2 mi'''4 |
mi'''1~ |
mi'''2~ mi'''4 r |
R1 |
r4 \twoVoices #'(flauto1 flauto2 flauti) <<
  { re'''2 re'''4 | mib'''1 | re'''2~ re'''4 }
  { si''2 si''4 | do'''1 | si''2~ si''4 }
>> r4 |
R1*2 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { sol''1 }
  { mi'' }
>>
do'''16 sol'' la'' si'' do''' sol'' la'' si'' do''' sol'' la'' si'' do''' sol'' la'' si'' |
do''' sol'' la'' si'' do''' re''' mi''' fa''' sol''' fa''' mi''' re''' do''' sib'' la'' sol'' |
fa'' do'' re'' mi'' fa'' sol'' la'' sib'' do''' fa'' sol'' la'' sib'' do''' re''' mi''' |
fa''' la''' sol''' fa''' mi''' re''' do''' sib'' la'' fa''' mi''' re''' do''' sib'' la'' sol'' |
fa''4 \twoVoices #'(flauto1 flauto2 flauti) <<
  { do'''2 do'''4 | la''1~ | la'' | si''!2 do''' |
    re''' do''' | si''1 | }
  { la''2 sol''4 | fad''1~ | fad'' | re''2 mib'' |
    fa'' mib'' | re''1 | }
>>
r4 mi'''2 re'''4 |
dod'''1 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { re'''1 | dod''' | }
  { fa''1 | mi'' | }
>>
re'''2.-\sug\ff la''4 |
re''' la'' re''' fa''' |
re'''2. la''4 |
sib''2 dod''4 la'' |
re'''2 r |
R1*3 |
r2 \twoVoices #'(flauto1 flauto2 flauti) <<
  { re'''2 | re'''1 | mib'''1~ | mib'''~ |
    mib'''~ | mib''' | mib'''4 sol''' sol''' sol''' |
    sol'''1~ | sol'''4 sol''' sol''' sol''' | sol'''2 }
  { sib''?2 | lab''1 | sol''~ | sol''~ |
    sol''~ | sol'' | sol''4 mib''' mib''' mib''' |
    mib'''1~ | mib'''4 mib''' mib''' mib''' | mib'''2 }
  { s2 | s1 | s2. s4-\sug\p | s1\< | s2.\!-\sug\ff s4\p |
    s1\< | s4\! s2.-\sug\ff }
>> sol''2 |
si''! do''' |
re''' mib''' |
fa''' mib''' |
re'''4 sol'''2 sol'''4~ |
sol'''1 |
re'''~ |
re'''~ |
re'''~ |
re'''~ |
re''' |
mi'''! |
fa'''2 r |
r4 \twoVoices #'(flauto1 flauto2 flauti) <<
  { si''4 do''' }
  { re'' mi'' }
>> r4 |
R1*8 |
