\key do \major \tempo "Maestoso" \midiTempo#160
\time 2/2 s1*33 s4.
\tempo "Allegro" s8 s2 s1*9 s2.
\tempo "Allegro" s4 s1*63
\tempo "Allegro maestoso" s1*9 \bar "|."
