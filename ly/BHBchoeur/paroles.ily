\tag #'vbasse2 {
  Ju -- rez au nom des Dieux, par l’hon -- neur & la gloi -- re,
  D’é -- touf -- fer tout es -- prit de ven -- geance & d’ai -- greur :
  Qu’Albe ou Rome en ce jour ob -- tien -- ne la vic -- toi -- re,
  Ju -- rez tous d’o -- bé -- ir au par -- ti du vain -- queur.
}
\tag #'(vtenor1 vbasse1) {
  Nous ju -- rons tous aux Dieux, par l’hon -- neur & la gloi -- re
  D’é -- touf -- fer tout es -- prit de ven -- geance & d’ai -- greur.
  Qu’Albe ou Rome en ce jour em -- por -- te la vic -- toi -- re,
  Nous ju -- rons d’o -- bé -- ir au par -- ti du vain -- queur.
}
\tag #'(vsoprano valto vtenor vbasso) {
  Nous ju -- rons d’o -- bé -- ir au par -- ti du vain -- queur.
}
\tag #'(vbasse1 vbasse2) {
  Les Ho -- ra -- ces, des fre -- res.
}
\tag #'(valto vtenor vbasso) {
  Les Cu -- ria -- ces, des a -- mis.
}

\tag #'(vsoprano valto vtenor vbasso) {
  Ciel !
  O crime ! ô honte é -- ter -- nel -- le !
  La guer -- re même é -- toit moins cri -- mi -- nel -- le
  Que cet hor -- ri -- ble choix.
}
\tag #'(vtenor1 vbasse1) {
  Al -- lons, vo -- lons, \tag #'vtenor1 { vo -- lons } où l’hon -- neur nous ap -- pel -- le.
}
\tag #'(vsoprano valto vtenor vbasso) {
  De la terre & du Ciel c’est ou -- tra -- ger les loix.
}
\tag #'(vtenor1 vbasse1) {
  Al -- lons, vo -- lons où l’hon -- neur nous ap -- pel -- le.
}
\tag #'(vsoprano valto vtenor vbasso) {
  Nous ne souf -- fri -- rons pas ce com -- bat plein d’hor -- reur.
}
\tag #'vtenor1 {
  A -- van -- çons…
}
\tag #'(vsoprano valto vtenor vbasso) {
  Ar -- rê -- tez !
}
\tag #'(vtenor1 vbasse1) {
  Al -- lons, a -- van -- çons…
}
\tag #'(vsoprano valto vtenor vbasso) {
  Ar -- rê -- tez, Ar -- rê -- tez !
}
\tag #'vbasse2 {
  Ré -- vol -- te pu -- nis -- sa -- ble !

  De vos chefs re -- con -- nois -- sez la voix.
}  
\tag #'(vtenor1 vbasse1) {
  Lais -- sez- nous.
}
\tag #'(vsoprano valto) {
  que Ro -- me fasse un au -- tre choix !
}
\tag #'(vtenor vbasso) {
  qu’Al -- be fasse un au -- tre choix !
}
\tag #'(vtenor1 vbasse1) {
  Lais -- sez- nous.
}
\tag #'(vsoprano vtenor vbasso) {
  Nous ne souf -- fri -- rons pas ce meurtre a -- bo -- mi -- na -- ble
}
\tag #'(valto) {
  De la terre & du Ciel c’est ou -- tra -- ger les loix. __
}
\tag #'(vsoprano valto vtenor vbasso) {
  Oui, la guerre é -- toit moins cou -- pa -- ble
  Que cet hor -- ri -- ble choix.
}
\tag #'(vtenor1 vbasse1) {
  A -- van -- çons, a -- van -- çons…
  Lais -- sez- nous.
  Lais -- sez- nous.
}
\tag #'(vsoprano valto vtenor vbasso) {
  Un au -- tre choix, __ un au -- tre choix, un au -- tre choix.
  C’est un meurtre a -- bo -- mi -- na -- ble,
  C’est un meurtre a -- bo -- mi -- nable,
  un au -- tre choix, __ un au -- tre choix,
  un au -- tre choix, un au -- tre choix. __
}
\tag #'vbasse2 {
  Hé -- ros d’Albe & de Ro -- me, & vous chefs & sol -- dats,
  E -- cou -- tez !… & ces -- sez d’i -- nu -- ti -- les dé -- bats.
  Si le choix du Sé -- nat vous bles -- se,
  Al -- lez tous con -- sul -- ter vos Dieux :
  Qu’ils soit ou non dé -- sa -- prou -- vé par eux ;
  Quel pro -- fane o -- se -- ra con -- dam -- ner leur sa -- ges -- se !
}
