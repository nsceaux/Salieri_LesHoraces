\clef "treble" R1 |
r2 <<
  \tag #'violino1 {
    sib'2~ |
    sib' do''~ |
    do''1 |
    fa''~ |
    fa'' |
    mib'' |
    re''~ |
    re''4. mi''!8 \grace re''4*1/2 dod''2 |
    R1 |
    re''4 r r2 |
    r sold'\fp~ |
    sold' la' |
    sib'~ sib'4. sol''8\f |
    sol''2 <>\p \rt#8 do'16 |
    \rt#16 do' |
    do'4 r r8. sol''16[\f sol''8. sol''16] |
    sol''1\p~ |
    sol''~ |
    sol'' |
    r8. do''16[\f mi''8. sol''16] do'''2~ |
    do'''4 r r2 |
    si''4\f
  }
  \tag #'violino2 {
    sol'2~ |
    sol' lab'~ |
    lab'1 |
    do''2 si'!~ |
    si'1 |
    sol' |
    la' |
    sib'2 mi'! |
    R1 |
    la'4 r r2 |
    r <re' si!>-\sug\fp~ |
    <re' si> do' |
    sib~ sib4. sib'8-\sug\f |
    sib'2 <>-\sug\p \rt#8 la16 |
    \rt#16 la |
    la4 r r8. <re' si'!>16[-\sug\f <re' si'>8. q16] |
    <>-\sug\p q1~ |
    q~ |
    q |
    r8. sol'16-\sug\f[ do''8. mi''16] sol''2 |
    la''4 r r2 |
    re''4-\sug\f
  }
>>
r8 r16 sol' sol'4 \grace sol'8 fa'! mi'16 fa' |
mi'1 |
fa'4 r8 fa'16 sol' la'8. la'16 la'8. la'16 |
sib'4 sib'16 la' sol' fa' mi'4 sol'16 fa' mi' re' |
dod'4 r r2 |
R1 |
<<
  \tag #'violino1 {
    dod''1 |
    re''4 r mib''2~ |
    mib''2 re''8
  }
  \tag #'violino2 {
    la'1 |
    la'4 r la'2~ |
    la' sib'8
  }
>> r8\fermata sol16 la32 sib do' re' mi'! fad' |
sol'2 r |
r r4 <la mi' dod''> |
<la fa' re''>2
