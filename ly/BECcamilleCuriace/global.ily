\midiTempo#80
\key do \major
\time 4/4 s1*16 s2 s8.
\tempo "Allegro" s16 s4 s1*3 s8.
\tempo "Allegro maestoso" s16 s2. s1 s4
\tempo "Presto" s2. s1*10 s2 \bar "|."
