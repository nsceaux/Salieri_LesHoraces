\clef "alto" R1 |
r2 mib'~ |
mib'~ mib'~ |
mib'1 |
lab'2 re'~ |
re'1 |
do'~ |
do' |
sib2 la! |
R1 |
fa'4 r r2 |
r2 mi!-\sug\fp~ |
mi mi'~ |
mi'~ mi'4. mi'8-\sug\f |
mi'2 <>-\sug\p \rt#8 fa'16 |
\rt#16 fa' |
fa'4 r r8. sol'16[-\sug\f sol'8. sol'16] |
sol'1-\sug\p~ |
sol'~ |
sol' |
r8. mi'16-\sug\f[ mi'8. mi'16] mi'2 |
re'4 r r2 |
sol'4-\sug\f r8 r16 sol sol4 \grace sol8 fa! mi16 fa |
mi1 |
fa'4. fa'16 sol' la'4. la'8 |
sib'4 sib'16 la' sol' fa' mi'4 sol'16 fa' mi' re' |
dod'4 r r2 |
R1 |
mi'1 |
re'4 r do''2~ |
do'' sib'8 r\fermata sol16 la32 sib do' re' mi'! fad' |
sol'2 r |
r r4 la' |
re'2
