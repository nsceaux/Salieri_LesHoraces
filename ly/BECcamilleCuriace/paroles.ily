\tag #'camille {
  Non, je te con -- nois mieux ; non, tu n’es point bar -- ba -- re.
  L’a -- mi -- tié, la na -- ture, & l’hy -- men & l’a -- mour,
  Rap -- pel -- le -- ront ta rai -- son qui s’é -- ga -- re :
  Tu ne tra -- hi - ras point tant de droits en ce jour.
}
\tag #'curiace {
  Hé -- las !
}
\tag #'camille {
  Et que pré -- tends- tu fai -- re ?
  As- tu con -- çu l’hor -- reur de cet ordre in -- hu -- main ?
  Tu vien -- dras donc m’of -- frir ta main
  Fu -- man -- te du sang de mon frè -- re ?
}
\tag #'curiace {
  Ah ! lais -- sez- moi, Ca -- mil -- le !
}
\tag #'camille {
  Eh ! quoi !
  Ton nom con -- sa -- cré par la gloi -- re,
  N’est- il donc pas fa -- meux par plus d’u -- ne vic -- toi -- re ?
  Al -- be n’a- t-elle en -- fin d’au -- tres guer -- riers que toi ?
}
\tag #'curiace {
  Ciel ! que pro -- po -- ses- tu ? tu veux que Cu -- ri -- a -- ce
  D’un op -- probre é -- ter -- nel se souil -- le lâ -- che -- ment,
  Qu’il dés -- ho -- no -- re & lui- même & sa ra -- ce ?…
  Ah ! tu l’es -- pè -- re vai -- ne -- ment.
}
