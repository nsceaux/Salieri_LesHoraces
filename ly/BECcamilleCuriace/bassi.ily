\clef "bass" R1 |
r2 mib~ |
mib lab~ |
lab1~ |
lab2 sol~ |
sol1 |
do |
fad |
sol2 la! |
R1 |
fa4 r r2 |
r mi\fp~ |
mi la |
sol!2~ sol4. do8\f |
do2 <>\p \rt#8 fa16 |
\rt#16 fa |
fa4 r\fermata r8. sol16[\f sol8. sol16] |
sol1\p~ |
sol~ |
sol |
r8. mi16[\f mi8. mi16] mi2 |
fad4 r r2 |
sol4-\sug\f r8 r16 sol sol4 fa! |
mi1 |
fa4. fa16 sol la4. la8 |
sib4 sib16 la sol fa mi4 sol16 fa mi re |
dod4 r r2 |
R1 |
sol1 |
fa4 r fad2~ |
fad sol8 r\fermata sol,16 la,32 sib, do re mi! fad |
sol2 r |
r r4 la |
re2
