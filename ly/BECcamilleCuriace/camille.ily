\clef "soprano/treble" mib''4. sib'8 sib'4 lab'8 sib' |
sol'4 r sib'^! r |
reb''8 reb'' reb'' mib'' do''^! do'' r do''16 do'' |
lab'4 r8 lab'16 sib' do''4 do''8 do'' |
fa''4^! re''8 do'' si'!4^! r16 si' si' do'' |
re''4 re''8 mib'' fa''4 re''8 mib'' |
do''^! do'' r4 do''8^! do'' do'' mib'' |
re''4^! la'8 sib' do''4 do''8 re'' |
sib'4^! r r2 |
r4 r8 dod'' dod'' dod'' mi'' la' |
re''^! re'' r4 r r8 re'' |
re'' re'' mi'' fa'' sold'4^! si'8 do'' |
re''4 mi''8 si' do''4^! r16 do'' do'' re'' |
sib'8.^! sib'16 sib'8 sib' mi''4. sol''8 |
sol''8. sib'16 sib' sib' do'' sol' la'8^! la' r4 |
R1 |
r4 r8\fermata re'' si'!4^! r8 si' |
sol'4 sol'8 la' si'4 si'8 do'' |
re'' re'' r re'' re'' re'' fa'' re'' |
si'4 r8 re'' si' si' si' sol' |
do''^! do'' r4 do''8 do''16 do'' do''8 mi'' |
re''4^! r la'8 la'16 la' re''8 la' |
si'4^! r4 r2 |
R1*10 |
\new CueVoice { r2 }
