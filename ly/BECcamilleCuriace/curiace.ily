\clef "alto/G_8" R1*8 |
r4 mi'! \grace re'8 dod'2^! |
R1*6 |
do'4 do'8 do' fa'4. do'8 |
la^! la r4\fermata r2 |
R1*6 |
do'2 do'8 do' do' sol |
la4^! r r2 |
R1 |
r4 r8 la la4 la8 la16 si |
dod'8 dod' r8*3 dod'16 dod' mi'8 re'16 mi' |
dod'4^! r8 mi' dod' dod' si la |
re'4^! r16 re' re' re' mib'8^! mib' r8 la16 sib |
do'4 re'8 la sib^! sib r4 |
sol'2. mi'8 re' |
dod' dod' dod' re' la4^! r4 |
\new CueVoice { r2 }
