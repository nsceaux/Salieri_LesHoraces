\clef "treble" r4 fa'' fa'' fa'' |
fa''1~ |
fa''4 fa'' fa'' fa'' |
fa''1~ |
fa''4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { sib''4 sib'' sib'' |
    sib''1~ |
    sib''4 sol''2 fa''4 |
    fa'' mib''2 re''8 do'' |
    s4 sib'' sib'' sib'' |
    sib''1~ |
    sib''4 sol''2 fa''4 |
    fa'' mib''2 re''8 do'' |
    sib'4 sib'8. sib'16 sib'4 sib' |
    sib'2 }
  { sib'4 sib' sib' |
    sib'1~ |
    sib'4 mib''2 re''4 |
    re'' do''2 sib'8 la' |
    s4 sib' sib' sib' |
    sib'1~ |
    sib'4 mib''2 re''4 |
    re'' do''2 sib'8 la' |
    sib'4 re'8. re'16 re'4 re' |
    re'2 }
  { s2. | s1 | s4 s2-\sug\f s4-\sug\p | s1 | r4 s2. | s1 |
    s4 s2-\sug\f s4-\sug\p | s1 | s-\sug\f }
>> r2 |
R1*4 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sib'1 | la'2 do'' | mi''1 | }
  { sol'1 | fa'2 la' | sib'1 | }
  { s4-\sug\fp s2.-\sug\cresc | s1 | s\!-\sug\f }
>>
r4 fa''-\sug\p fa'' fa'' |
fa''1~ |
fa''4 fa'' fa'' fa'' |
fa''1~ |
fa''4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { sib''4 sib'' sib'' |
    sib''1~ |
    sib''4 sol''2 fa''4 |
    fa'' mib''2 re''8 do'' |
    s4 sib'' sib'' sib'' |
    sib''1~ |
    sib''4 sol''2 fa''4 |
    fa'' mib''2 re''8 do'' | }
  { sib'4 sib' sib' |
    sib'1~ |
    sib'4 mib''2 re''4 |
    re'' do''2 sib'8 la' |
    s4 sib' sib' sib' |
    sib'1~ |
    sib'4 mib''2 re''4 |
    re'' do''2 sib'8 la' | }
  { s2. | s1 | s4 s2-\sug\f s4-\sug\p | s1 | r4 s2. | s1 |
    s4 s2-\sug\f s4-\sug\p }
>>
sib'1 |
