\clef "bass" sib,4 r r2 |
r4 sib2 la8 sib |
fa2 r |
r4 fa fa fa |
sib2 sib, |
\clef "tenor" <>^"Violoncelli" sol'2 sol'4. fa'8 |
mib'4 \clef "bass" <>^"Tutti" mib2\f sib,4\p |
fa2 fa, |
sib,1 |
\clef "tenor" <>^"Violoncelli" sol'2 sol'4. fa'8 |
mib'4 \clef "bass" <>^"Tutti" mib2\f sib,4\p |
fa2 fa, |
sib,4\f sib,8. sib,16 sib,4 sib, |
sib,2 r |
lab1\fp |
sol |
fad2 re |
sol1 |
<>\fp \rt#4 do8 <>\cresc \rt#4 do |
\rt#4 do8 \rt#4 do8 |
<>\!\f \rt#4 do8 \rt#4 do8 |
fa2 r |
r4 sib2\p la8 sib |
fa2 r |
r4 fa fa fa |
sib2 sib, |
\clef "tenor" <>^"Violoncelli" sol'2 sol'4. fa'8 |
mib'4 \clef "bass" <>^"Tutti" mib2\f
\footnoteHere #'(0 . 0) \markup\wordwrap { Source : \italic mi♭ }
sib,4\p |
fa2 fa, |
sib,1 |
\clef "tenor" <>^"Violoncelli" sol'2 sol'4. fa'8 |
mib'4 \clef "bass" <>^"Tutti" mib2\f sib,4\p |
fa2 fa, |
sib,1 |
