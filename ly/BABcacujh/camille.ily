\clef "soprano/treble" sib'4 r r2 |
r4 re''2^\markup\italic dolce mib''8[ re''] |
do''2 r4 do'' |
mib'' mib''2 fa''8[ mib''] |
\grace mib''4 re''2 re'' |
mib''2 mib''4. re''8 |
sol''2. fa''4 |
fa''( mib''2) re''8[ do''] |
\grace do''4 re''1 |
mib''2 mib''4. re''8 |
sol''2. fa''4 |
fa''( mib''2) re''8[ do''] |
sib'2 r |
r fa''4. mib''8 |
\grace mib''4 re''2 do''4 sib' |
mib''2. mib''4 |
mib'' re'' re'' do'' |
do''( sib') sib' r8 sib' |
mi''2 mi''4. mi''8 |
fa''2 fa''4 fad'' |
sol''( mi'') do'' sib' |
la'2 r |
r4 re''2 mib''8[ re''] |
do''2 r4 do'' |
mib'' mib''2 fa''8[ mib''] |
mib''4( re'') re''2 |
mib''2 mib''4. re''8 |
sol''2. fa''4 |
fa''( mib''2) re''8[ do''] |
\grace do''4 re''1 |
mib''2 mib''4. re''8 |
sol''2. fa''4 |
fa''( mib''2) re''8[ do''] |
sib'2 r |
