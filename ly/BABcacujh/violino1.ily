\clef "treble" fa''4\repeatTie r r2 |
r4 re''2(_\markup\italic Dolce mib''8 re'') |
do''2 r |
r4 mib''2 fa''8 mib'' |
\grace mib''4 re''2 re'' |
mib'' mib''4. re''8 |
sol''2.\f fa''4\p |
fa'' mib''2 re''8 do'' |
\grace do''2*1/2 re''1 |
mib''2 mib''4. re''8 |
sol''2.\f fa''4\p |
fa'' mib''2 re''8 do'' |
<sib' re'>4\f q8. q16 q4 q |
q2 fa''4.\p mib''8 |
\grace mib''4 re''2\fp do''4 sib' |
mib'' mib'2 mib''4~ |
mib''8( re'' re'' la'' la'' fad''? re'' do'')~ |
do''4 sib'2 r8 sib' |
mi''4\fp sib''2\cresc \grace la''16 sol''8 fa''16 mi'' |
fa''4 fa''2 fad''4 |
sol''4\!\f( mi'' do'' sib') |
la'2 r |
r4 re''2(\p mib''8 re'') |
do''2 r |
r4 mib''2 fa''8 mib'' |
mib''4( re'') re''2 |
mib''2 mib''4. re''8 |
sol''2.\f fa''4\p |
fa'' mib''2 re''8 do'' |
\grace do''2*1/2 re''1 |
mib''2 mib''4. re''8 |
sol''2.\f fa''4\p |
fa''4 mib''2 re''8 do'' |
<sib' re'>1 |
