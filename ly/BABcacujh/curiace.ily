\clef "alto/G_8" R1 |
r4 sib2^\markup\italic dolce do'8[ sib] |
la2 r4 la |
do' do'2 do'4 |
\grace do'4 sib2 sib |
sol'2 sol'4. fa'8 |
mib'2. re'4 |
re'( do'2) sib8[ la] |
\grace la4 sib1 |
sol'2 sol'4. fa'8 |
mib'2. re'4 |
re'( do'2) sib8[ la] |
sib2 r |
R1*9 |
r4 sib2 do'8[ sib] |
la2 r4 la |
do' do'2 do'4 |
do'( sib) sib2 |
sol'2 sol'4. fa'8 |
mib'2. re'4 |
re'( do'2) sib8[ la] |
\grace la4 sib1 |
sol'2 sol'4. fa'8 |
mib'2. re'4 |
re'( do'2) sib8[ la] |
sib2 r |
