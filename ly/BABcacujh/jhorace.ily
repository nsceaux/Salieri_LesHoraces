\clef "tenor/G_8" R1 |
r4 <>^\markup\italic dolce sib2 la8[ sib] |
fa2 r4 fa |
fa la2 la4 |
\grace la4 sib2 sib |
R1 |
r4 sib2 sib4 |
fa'4. fa8 fa4. fa8 |
\grace fa4 sib1 |
R1 |
r4 sib2 sib4 |
fa'4. fa'8 fa'4. fa'8 |
sib2 r |
R1*9 |
r4 sib2 la8[ sib] |
fa2 r4 fa |
fa la2 la4 |
la( sib) sib2 |
R1 |
r4 sib2 sib4 |
fa4. fa8 fa4. fa8 |
\grace fa2*1/2 sib1 |
R1 |
r4 sib2 sib4 |
fa'4. fa'8 fa'4. fa'8 |
sib2 r |
