\clef "alto" sib'4 r r2 |
r4 fa'2 fa'4 |
fa'2 r |
r4 do'2 do'4 |
\grace do'4 sib2 sib |
sol'2 sol'4. fa'8 |
mib'2.-\sug\f sib4-\sug\p |
fa'1~ |
fa' |
sol'2 sol'4. fa'8 |
mib'2.-\sug\f sib4-\sug\p |
fa'1~ |
fa'4-\sug\f fa'8. fa'16 fa'4 fa' |
fa'2 r |
lab1-\sug\fp |
sol2 sol' |
fad' re' |
re'1 |
sol'8-. sib'-. sol'-.-\sug\cresc mi'-. do'-. mi'-. sol'-. sib'-. |
la' do'' la' fa' do' la do' la |
mi'\!-\sug\f sol' do'' sol' mi' sol' mi' sol' |
fa'2 r |
r4 fa'2-\sug\p fa'4 |
fa'2 r |
r4 do'2 do'4 |
do'( sib) sib2 |
sol'2 sol'4. fa'8 |
mib'2.-\sug\f sib4-\sug\p |
fa'1~ |
fa' |
sol'2 sol'4. fa'8 |
mib'2.-\sug\f sib4-\sug\p |
fa'1~ |
fa' |
