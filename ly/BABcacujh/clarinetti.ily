\clef "treble" R1 |
r4 \twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { re''2( mib''8 re'') | do''2 }
  { sib'2( do''8 sib') | la'2 }
>> r2 |
r4 \twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { mib''2 fa''8 mib'' |
    \grace mib''4 re''2 re'' |
    mib''2 mib''4. re''8 |
    sol''2. fa''4 |
    fa'' mib''2 re''8 do'' |
    \grace do''4 re''1 |
    mib''2 mib''4. re''8 |
    sol''2. fa''4 |
    fa'' mib''2 re''8 do'' | }
  { la'2 la'4 |
    \grace la'4 sib'2 sib' |
    sib'1 |
    sib'2. re''4 |
    re'' do''2 sib'8 la' |
    \grace la'4 sib'1 |
    sib'1~ |
    sib'2. re''4 |
    re'' do''2 sib'8 la' | }
  { s2. | s1*2 | s2.-\sug\f s4-\sug\p | s1*3 | s2.-\sug\f s4-\sug\p }
>>
sib'4-\sug\f sib'8. sib'16 sib'4 sib' |
sib'2 r |
R1*8 |
r4 \twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { re''2( mib''8 re'') | do''2 }
  { sib'2( do''8 sib') | la'2 }
  { <>-\sug\p }
>> r2 |
r4 \twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { mib''2 fa''8 mib'' |
    mib''4( re'') re''2 |
    mib''2 mib''4. re''8 |
    sol''2.\f fa''4\p |
    fa''4 mib''2 re''8 do'' |
    \grace do''2*1/2 re''1 |
    mib''2 mib''4. re''8 |
    sol''2. fa''4 |
    fa'' mib''2 re''8 do'' | }
  { la'2 la'4 |
    la'4( sib') sib'2 |
    sib'1~ |
    sib'2.-\tag #'clarinetto2 -\sug\f re''4-\tag #'clarinetto2 -\sug\p |
    re'' do''2 sib'8 la' |
    \grace la'4 sib'1 |
    sib'~ |
    sib'2. re''4 |
    re'' do''2 sib'8 la' | }
  { s2. | s1*6 | s2.-\sug\f s4-\sug\p | }
>>
sib'1 |
