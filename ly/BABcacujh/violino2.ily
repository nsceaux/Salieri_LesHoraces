\clef "treble" re''4 r r2 |
r4 sib'2( do''8 sib') |
la'2 r |
r4 la'2 la'4 |
\grace la'4 sib'2 sib' |
sib'1 |
sib'2.-\sug\f re''4-\sug\p |
re'' do''2 sib'8 la' |
\grace la'4 sib'1 |
sib'~ |
sib'2.-\sug\f re''4-\sug\p |
re'' do''2 sib'8 la' |
<sib' re'>4\f q8. q16 q4 q |
q2 r |
r8 sib(-\sug\p re' fa' sib' fa' re' sib) sib( mib' sol' sib' sib' sol' mib' sib) |
<<
  { la re' re' re' fad' la'! la' la' |
    la'4( sib') sib' } \\
  { s2 s8 fad' fad' fad' |
    fad'4( sol') sol' }
>> r4 |
sol'8-. sib'-. sol'-.-\sug\cresc mi'-. do'-. mi'-. sol'-. sib'-. |
la' do'' la' fa' do' la do' la |
mi'-\!-\sug\f sol' sib' sol' mi' sol' mi' sol' |
fa'2 r |
r4 \once\slurDashed sib'2\p( do''8 sib') |
la'2 r |
r4 la'2 la'4 |
la'( sib') sib'2 |
sib'1~ |
sib'2.-\sug\f re''4-\sug\p |
re'' do''2 sib'8 la' |
\grace la'4 sib'1 |
sib'~ |
sib'2.-\sug\f re''4-\sug\p |
re'' do''2 sib'8 la' |
<sib' re'>1 |
