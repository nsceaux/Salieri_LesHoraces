\clef "vhaute-contre" R1*43 |
r2 mi'4 mi' |
fad'2 fad'4 fad' |
fad'2. fad'4 |
fad'1 |
re'2 fad'4. fad'8 |
si1 |
mi'2 mi' |
dod'1 |
la2 la |
mi'1 |
mi'2 mi' |
la r\fermata |
