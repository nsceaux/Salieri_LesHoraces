\clef "vtaille" R1*43 |
r2 dod'4 dod' |
re'2 re'4 re' |
re'2. re'4 |
re'1 |
re2 re'4. re'8 |
sold1 |
si2 si |
la1 |
la2 la |
mi'1 |
mi'2 mi' |
la r\fermata |
