\clef "treble" r4 <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''4 mi'' mi'' | mi''1 | sold'' | la'' |
    mi''~ | mi''2 la'' | fad'' si'' | \grace la''4 sold''2 sold'' |
    s4 sold''2 sold''4 | s la''2 la''4 | s si''2 si''4 | dod'''1 |
    si''4 si''8 si'' si''4 si'' | si''2. }
  { dod''4 dod'' dod'' | dod''1 | re'' | dod''4 mi''2 mi''4 |
    la'1 | dod''2 dod'' | re''1 | si' |
    s4 si'2 si'4 | s dod''2 dod''4 | s4 sold''2 sold''4 | la''1 |
    mi''4 si'8 si' si'4 si' | mi''2. }
  { s2. | s1*3 | s1-\sug\ff | s2 s2-\sug\p | s1*2 | r4 s2\f s4\p |
    r4 s2\f s4\p | r4 s2\f s4\p | s1 | s-\sug\f }
>> r4 |
R1*2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sold''1~ | sold'' | la''2 }
  { si'1~ | si' | dod''2 }
  { s1-\sug\f | s | s2-\sug\p }
>> r2 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { sold''4 fad'' fad'' | }
  { mi''4 red'' red'' | }
>>
mi''2-\sug\fp~ mi''~ |
mi''4 mi'' sold'' mi'' |
si'2-\sug\fp~ si'~ |
si'4 si' red'' si' |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { si'8 si' si'4 si' | si'1 | sold''~ | sold'' |
    la'' | sold''4 la'' si'' sold'' | la''2 }
  { sold'8 sold' sold'4 sold' | sold'1 | si'~ | si' |
    dod'' | si'4 dod'' re'' si' | dod''2 }
>> r2 |
R1 |
r4 re''8 re'' fad''4 re'' |
la' re'' fad' la' |
re' mi'8 fad' sol'! la' si' dod'' |
re''2 r |
r \twoVoices #'(oboe1 oboe2 oboi) <<
  { sold''2~ | sold'' sold'' | la''1 | dod''' |
    si''1~ | si'' | la''4 }
  { si'2~ | si' si' | dod''1 | la'' |
    sold''1~ | sold'' | la''4 }
  { s2-\sug\p | s1*2 | s1*3-\sug\cresc | s4-\sug\ff }
>> r4 r2 |
r2 \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''2 | fad''1 | fad''~ | fad'' |
    fad'' | si'2 sold''~ | sold'' sold'' | la''1~ |
    la'' | si''~ | si'' | la''2 }
  { dod''2 | re''1 | re''~ | re'' |
    re'' | sold'2 si'~ | si' si' | dod''1~ |
    dod'' | sold''~ | sold'' | la''2 }
>> r2\fermata |
