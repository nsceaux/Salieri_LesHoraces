\clef "vbasse" r4 dod' dod' dod' |
dod'2. dod'4 |
re'2. si4 |
la2 r |
mi'1~ |
mi'2 dod'4 la |
fad si re' si |
sold sold r2 |
si2. si4 |
dod'2 la4 la |
re'2. re'4 |
dod'4 dod' r dod' |
si1 |
r2 r4 si |
fad sold la si |
sold2 sold4 si |
mi'1~ |
mi'4 si sold mi |
dod'2 la4 dod' |
si2 si4 si |
mi2 r |
r sold4 mi |
si2. si4 |
si2 si4 si |
sold2 sold |
r si4 dod' |
re'1 |
si2. sold4 |
la2. la4 |
si dod' re'4. mi'8 |
dod'2 r |
r la4 la |
re'2 re'4 re' |
re'2. re'4 |
re'1 |
re2 re'4. re'8 |
mi1 |
si2 mi' |
dod'1 |
la2 la |
mi'1 |
mi'2 mi' |
la1 |
R1*11 |
R1^\fermataMarkup |
