\clef "vbasse" R1*43 |
r2 la4 la |
re'2 re'4 re' |
re'2. re'4 |
re'1 |
re2 re'4. re'8 |
mi1 |
sold2 sold |
la1 |
la2 la |
mi'1 |
mi'2 mi' |
la r\fermata |
