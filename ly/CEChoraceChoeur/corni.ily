\clef "treble" \transposition la
r4 <>-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { mi''4 mi'' mi'' | mi''1 | fa''2. re''4 | do''1 |
    sol''~ | sol''2 }
  { do''4 do'' do'' | do''1 | re''2. sol'4 | sol'1 |
    mi''~ | mi''2 }
  { s2. | s1*3 | s1-\sug\ff }
>> r2 |
R1 |
\twoVoices #'(corno1 corno2 corni) <<
  { s4 re''8 re'' re''4 re'' | s4 re''2 re''4 |
    s mi''2 mi''4 | s fa''2 fa''4 |
    mi''2. mi''4 | re''4 re''8 re'' re''4 re'' | re''2. }
  { s4 sol'8 sol' sol'4 sol' | s sol'2 sol'4 |
    s do''2 do''4 | s re''2 re''4 |
    do''2. do''4 | sol'4 sol'8 sol' sol'4 sol' | sol'2. }
  { r4 s2. | r4 s2\f s4\p | r4 s2\f s4\p | r4 s2\f s4\p |
    s1 | s1-\sug\f | }
>> r4 |
R1*5 |
r4 re''2 re''4 |
sol'2-\sug\fp~ sol'~ |
sol'4 sol' sol' sol' |
re''2-\sug\fp re''~ |
re''4 re'' re'' re'' |
r4 sol'8 sol' sol'4 sol' |
sol'1~ |
sol'~ |
sol'~ |
sol'~ |
sol' |
do''2 r |
R1*5 |
r2 \twoVoices #'(corno1 corno2 corni) <<
  { re''2~ | re'' re'' | mi''1~ | mi'' |
    re''4 re''8 re'' re''4 re'' | re'' re'' re'' re'' | do''2 }
  { sol'2~ | sol' sol' | do''1~ | do'' |
    sol'4 sol'8 sol' sol'4 sol' | sol' sol' sol' sol' | do''2 }
  { s2-\sug\p | s1*2 | s1*3-\sug\cresc | s2-\sug\ff }
>> r2 |
R1*5 |
r2 \twoVoices #'(corno1 corno2 corni) <<
  { re''2~ | re'' re'' | mi''1~ | mi'' |
    re''4 re''8 re'' re''4 re'' | re'' re'' re'' re'' | do''2 }
  { sol'2~ | sol' sol' | do''1~ | do'' |
    sol'4 sol'8 sol' sol'4 sol' | sol' sol' sol' sol' | mi'2 }
>> r2\fermata |
