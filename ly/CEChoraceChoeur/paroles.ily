\tag #'vhorace {
  O noble ap -- pui de ton pay -- ys !
  Gloire __ é -- ter -- nel -- le de ta ra -- ce !
  O mon fils, ô mon cher Ho -- ra -- ce !
  Re -- viens, que ma ten -- dresse ef -- fa -- ce
  La hon -- te du soup -- çon qui trom -- pa nos es -- prits !
  Les hon -- neurs que Ro -- me t’ap -- prê -- te
  De ta hau -- te va -- leur se -- ront le di -- gne prix :
  Nous al -- lons cou -- ron -- ner ta tê -- te
  Des lau -- riers im -- mor -- tels que ta main a con -- quis.
}
\tag #'vchoeur {
  Nous al -- lons cou -- ron -- ner sa tê -- te
  Des lau -- riers im -- mor -- tels que sa main a con -- quis.
}
