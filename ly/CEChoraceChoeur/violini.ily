\clef "treble"
<<
  \tag #'violino1 {
    <mi' dod'' la''>4\f dod''' dod''' dod''' |
    dod'''2. dod'''4 |
    re'''2. si''4 |
    la''1 |
    mi'''\ff~ |
    mi'''2 dod'''4\p la'' |
    fad'' si'' re''' si'' |
    \grace la''4 sold''2 sold'' |
    mi''8(\fp sold'') si'' si'' \rt#4 si'' |
    mi''(\fp la'') dod''' dod''' \rt#4 dod''' |
    mi''(\fp si'') re''' re''' \rt#4 re''' |
    mi''(\fp la'') dod''' dod''' \rt#4 dod''' |
    si''4\f <<
      { si''8 si'' si''4 si'' | si''2. si''4 | } \\
      { si'8 si' si'4 si' | si'2.\f si'4\p | }
    >>
  }
  \tag #'violino2 {
    <mi' dod'' la''>8-\sug\f <<
      { mi''8 mi'' mi'' \rt#4 mi''  | mi'' } \\
      { dod'' dod'' dod'' \rt#4 dod'' | dod'' }
    >> la'8 dod'' mi'' la'' mi'' dod'' la' |
    si' sold'' sold'' sold'' \rt#4 sold'' |
    la'' mi' la' dod'' mi'' dod'' la' mi' |
    mi''-\sug\ff dod'' la' mi' mi'' dod'' la' mi' |
    dod' mi' la' dod'' mi''-\sug\p la'' mi'' dod'' |
    re'' fad'' re'' si' fad' si' re'' fad'' |
    si' mi'' si' sold' mi' sold' si' mi'' |
    mi''(-\sug\fp si') sold' sold' \rt#4 sold' |
    mi''8(-\sug\fp dod'') la' la' \rt#4 la' |
    mi''(-\sug\fp si') sold' sold' \rt#4 sold' |
    la'8(-\sug\fp dod'') mi'' mi'' \rt#4 mi'' |
    mi''4-\sug\f <<
      { mi''8 mi'' mi''4 mi'' | mi''2. mi''4 | } \\
      { si'8 si' si'4 si' | si'2. si'4-\sug\p | }
    >>
  }
>>
fad''8 si' sold'' si' la'' si' si'' si' |
sold''4 <<
  \tag #'violino1 {
    mi''8\f mi'' sold''4 si'' |
    mi'''1~ |
    mi'''4 si''\p sold'' mi'' |
    dod'''2 la''4 dod''' |
    si''2 si''4 si'' |
    mi''4\fp sold''8 sold'' si''4 sold'' |
    mi'' mi'' sold'' mi'' |
    si'\fp red''8 red'' fad''4 red'' |
    si'4 si' red'' si' |
    mi''4 mi''8 mi'' mi''4 mi'' |
    mi''^! sold''( si'' dod''') |
    re'''1 |
    re'''2. sold''4 |
    la''2. la''4 |
    si''8 mi'' dod''' mi'' re''' mi'' mi''' mi'' |
    dod'''\f la' si' dod''
  }
  \tag #'violino2 {
    mi'8-\sug\f mi' sold'4 si' |
    mi''1~ |
    mi''4 sold'-\sug\p si' sold |
    la mi''8 mi'' dod''4 la' |
    sold'8 si' sold' mi' red' fad' si' red' |
    mi'4-\sug\fp sold'8 sold' si'4 sold' |
    mi' mi' sold' mi' |
    si-\sug\fp red'8 red' fad'4 red' |
    si si red' si |
    <<
      { mi''8 si' si' si' \rt#4 si' | } \\
      { s8 sold' sold' sold' \rt#4 sold' | }
    >>
    \rt#4 <sold' si'>8 \rt#4 q |
    \rt#4 q \rt#4 q |
    \rt#4 <re'' mi'> \rt#4 q |
    \rt#4 <dod'' mi'> \rt#4 q |
    sold'8 mi' la' mi' si' mi' sold' mi' |
    la'4-\sug\f si'8 dod''
  }
>> re''8 mi'' fad'' sold'' |
la'' sold'' fad'' mi'' re'' dod'' si' la' |
re'4 re''8 re'' fad''4 re'' |
la' re'' fad' la' |
re' mi'8 fad' sol' la' si' dod'' |
re''2 <re' la' fad''> |
<<
  \tag #'violino1 {
    mi'2 sold''(\p |
    si'' mi''') |
    dod'''4 <<
      { dod'''8 dod''' dod'''4 dod''' | } \\
      { la'8 la' la'4 la' | }
    >>
    \rt#4 dod'''8\cresc \rt#4 dod''' |
    \rt#4 si'' \rt#4 si'' |
    \rt#4 si'' \rt#4 si'' |
    <>\ff
  }
  \tag #'violino2 {
    mi'8 sold' si' sold' mi'-\sug\p sold' si' sold' |
    mi' sold' si' sold' mi' sold' si' sold' |
    la' mi' fad' sold' la' si' dod'' si' |
    la'-\sug\cresc la'' la'' la'' \rt#4 la'' |
    \rt#4 sold'' \rt#4 sold'' |
    \rt#4 sold'' \rt#4 sold'' |
    <>-\sug\ff
  }
>>
la''8 la' si' dod'' re'' mi'' fad'' sold'' |
la'' sold'' fad'' mi'' re'' dod'' si' la' |
re'4 re''8 re'' fad''4 re'' |
la' re'' fad' la' |
re' mi'8 fad' sol' la' si' dod'' |
re''2 <re' la' fad''> |
<<
  \tag #'violino1 {
    mi'2 sold''( |
    si'' mi''') |
    dod'''4 <<
      { dod'''8 dod''' dod'''4 dod''' | } \\
      { la'8 la' la'4 la' | }
    >>
    \rt#4 dod'''8 \rt#4 dod''' |
    \rt#4 si'' \rt#4 si'' |
    \rt#4 si'' \rt#4 si'' |
    la''2
  }
  \tag #'violino2 {
    mi'8 sold' si' sold' mi' sold' si' sold' |
    mi' sold' si' sold' mi' sold' si' sold' |
    la' mi' fad' sold' la' si' dod'' si' |
    la' la'' la'' la'' \rt#4 la'' |
    <<
      { \rt#4 sold'' \rt#4 sold'' | \rt#4 sold'' \rt#4 sold'' | la''2 }
      \\ { \rt#4 si'8 \rt#4 si' | \rt#4 si' \rt#4 si' | la'2 }
    >>
  }
>> r2\fermata |
