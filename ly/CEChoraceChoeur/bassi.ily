\clef "bass"
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    la,4-\sug\f la la la | la1 | si | dod'1~ |
    dod'4-\sug\ff dod' mi dod | la,2 la-\sug\p | re re' | mi' mi |
  }
  \tag #'basso {
    la,8\f la la la \rt#4 la |
    la la, la, la, \rt#4 la, |
    \rt#4 si,\p \rt#4 si, |
    \rt#4 dod \rt#4 dod |
    dod4\ff dod8 dod mi4 dod |
    \rt#4 la,8 \rt#4 la,8\p |
    \rt#4 re \rt#4 re |
    mi4 mi8 mi mi4 mi |
  }
>>
r4 re'(\sf si sold) |
r4 mi'(\sf dod' la) |
r4 si(\sf sold mi) |
r4 la\sf la, la |
sold4\f sold8 sold sold4 sold |
sold1 |
red\p |
mi2 r |
r4 mi\f sold si |
mi'1 |
la4\p la,8 la, la,4 la, |
si,2 si,4 si, |
mi4\fp sold8 sold si4 sold |
mi4 mi sold mi |
si,\fp red8 red fad4 red |
si, si, red si, |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    \clef "tenor" r4 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { si4 si si | si1 | si | re'1 |
        dod' | si4 dod' re' si | }
      { sold4 sold sold | sold1 | sold | si1 |
        la | sold4 la si sold }
    >>
  }
  \tag #'basso {
    \rt#4 mi8 \rt#4 mi |
    \rt#4 mi8 \rt#4 mi |
    \rt#4 mi8 \rt#4 mi |
    \rt#4 mi8 \rt#4 mi |
    \rt#4 mi8 \rt#4 mi |
    \rt#4 mi8 \rt#4 mi |
  }
>>
la8\f la, si, dod re mi fad sold |
la sold fad mi re dod si, la, |
re4 re'8 re' fad'4 re' |
la re' fad la |
re mi8 fad sol la si dod' |
re'2 re |
mi8 sold si sold mi\p sold si sold |
mi sold si sold mi sold si sold |
la mi fad sold la si dod' si la\cresc sold fad mi re dod si, la, |
mi4 fad8 sold la si dod' re' |
mi' re' dod' si la sold fad mi |
la8\ff la, si, dod re mi fad sold |
la sold fad mi re dod si, la, |
re4 re'8 re' fad'4 re' |
la re' fad la |
re mi8 fad sol la si dod' |
re'2 re |
mi8 sold si sold mi sold si sold |
mi8 sold si sold mi sold si sold |
la mi fad sold la si dod' si |
la sold fad mi re dod si, la, |
mi4 fad8 sold la si dod' re' |
mi' re' dod' si la sold fad mi |
la2 r\fermata |
