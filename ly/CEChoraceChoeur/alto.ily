\clef "alto" la'8-\sug\f <<
  { la'8 la' la' \rt#4 la' |
    la' mi'[ mi' mi'] \rt#4 mi' |
    \rt#4 mi' \rt#4 mi' |
    \rt#4 mi' \rt#4 mi' |
    \rt#4 mi'-\sug\ff \rt#4 mi' |
    \rt#4 mi' \rt#4 mi'-\sug\p |
    \rt#4 fad' \rt#4 fad' |
    \rt#4 mi' \rt#4 mi' | } \\
  { mi'8 mi' mi' \rt#4 mi' |
    mi' dod'[ dod' dod'] \rt#4 dod' |
    \rt#4 re' \rt#4 re' |
    \rt#4 dod' \rt#4 dod' |
    \rt#4 dod' \rt#4 dod' |
    \rt#4 dod' \rt#4 dod' |
    \rt#4 re' \rt#4 re' |
    \rt#4 si \rt#4 si | }
>>
sold'8-\sug\fp( si') mi' mi' \rt#4 mi' |
dod''-\sug\fp( la') mi' mi' \rt#4 mi' |
sold'-\sug\fp( mi') si' si' \rt#4 si' |
dod''(-\sug\fp mi'') la' la' \rt#4 la' |
sold'4-\sug\f sold'8 sold' sold'4 sold' |
sold'1 |
red'-\sug\p |
mi'2 r |
r4 mi-\sug\f sold si |
mi'1~ |
mi'4-\sug\p mi'8 mi' mi'4 la |
si2 si4 si |
mi'4\fp sold'8 sold' si'4 sold' |
mi' mi' sold' mi' |
si\fp red'8 red' fad'4 red' |
si si red' si |
\rt#4 mi'8 \rt#4 mi' |
\rt#4 mi'8 \rt#4 mi' |
\rt#4 mi'8 \rt#4 mi' |
\rt#4 si' \rt#4 si' |
\rt#4 la' \rt#4 la' |
mi''4 mi'8 mi' mi'4 mi' |
la'8\f la si dod' re' mi' fad' sold' |
la' sold' fad' mi' re' dod' si la |
re'4 re''8 re'' fad''4 re'' |
la' re'' fad' la' |
re' mi'8 fad' sol' la' si' dod'' |
re''2 re' |
mi'8 sold' si' sold' mi'\p sold' si' sold' |
mi' sold' si' sold' mi' sold' si' sold' |
la' mi' fad' sold' la' si' dod'' si' |
la'\cresc sold' fad' mi' re' dod' si la |
mi'4 fad'8 sold' la' si' dod'' re'' |
mi'' re'' dod'' si' la' sold' fad' mi' |
la'\ff la si dod' re' mi' fad' sold' |
la' sold' fad' mi' re' dod' si la |
re'4 re''8 re'' fad''4 re'' |
la' re'' fad' la' |
re' mi'8 fad' sol' la' si' dod'' |
re''2 re' |
mi'8 sold' si' sold' mi' sold' si' sold' |
mi' sold' si' sold' mi' sold' si' sold' |
la' mi' fad' sold' la' si' dod'' si' |
la' sold' fad' mi' re' dod' si la |
mi'4 fad'8 sold' la' si' dod'' re'' |
mi'' re'' dod'' si' la' sold' fad' mi' |
la'2 r\fermata |
