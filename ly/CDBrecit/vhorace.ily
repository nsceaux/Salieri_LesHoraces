\clef "vbasse" r4 r8 sol sib sib lab sib |
sol4 r r2 |
R1*2 |
r2 lab4. lab8 |
lab4 lab8 sib do' do' r do' |
do' do' sib lab reb'^! reb' r16 lab lab lab |
fa8 fa sol lab reb4 r8 lab |
reb' reb' sib lab sol4^! r |
sol8 sol16 sol sol8 lab mib4^! r |
R1*8 |
r2 r4 r8 re' |
mib'2 do'8 r sib sib |
sol8. sol16 sol sol lab sib mib4 r8 sib |
reb'4^! reb'8 sib16 lab sol8 sol r sib |
reb'8. reb'16 reb'8 do' lab8^! lab r4 |
r4 r8 do'16 re'! si!4^! r4 |
si8 si16 si si8 do' sol4^! r |
do'4. do'8 do'4 re'8 mib' |
re'4.^! la8 do' do' do' sib |
sol^! sol r4 r2 |
R1 |
