\tag #'vhorace {
  Mes fils ne sont donc plus ?
}
\tag #'femme {
  Un seul vous reste, hé -- las !
  Ses frè -- res, à nos yeux, ont re -- çu le tré -- pas.
}
\tag #'vhorace {
  Quoi ? lors -- qu’Al -- be tri -- om -- phe un de mes fils res -- pi -- re !
  On vous a fait un faux rap -- port,
  Si Rome a suc -- com -- bé, mon der -- nier fils est mort.
}
\tag #'femme {
  Ain -- si que moi, tous pour -- ront vous le di -- re,
  Long- temps, a -- vec cou -- ra -- ge, il a -- voit com -- bat -- tu ;
  Mais res -- té seul con -- tre trois ad -- ver -- sai -- res,
  Et n’es -- pé -- rant plus rien de sa hau -- te ver -- tu,
  Sa fui -- te l’a sau -- vé du des -- tin de ses frè -- res.
}
\tag #'vhorace {
  O cri -- me, dont la honte en re -- jail -- lit sur nous !
  O fils lâche & per -- fi -- de ; op -- pro -- bre de ma vi -- e !
}
\tag #'camille {
  Mes frè -- res !
}
\tag #'vhorace {
  Ar -- rê -- tez, ne les pleu -- rez pas tous.
  Deux jou -- is -- sent d’un sort trop di -- gne qu’on l’en -- vi -- e.
}
