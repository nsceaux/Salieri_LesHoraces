\clef "bass" mib1\repeatTie~ |
mib2 mi |
fa1~ |
fa |
r4 sol\f lab,2~ |
lab,1~ |
lab,2 r8. fa16 fa4 |
R1 |
r2 mib4 r |
r2 r4 mib |
lab, r r2 |
R1 | \allowPageTurn
re1 |
mib~ |
mib2 re~ |
re1~ |
re2 do~ |
do1~ |
do2 r4 re\f |
\rt#8 mib16 <>\p \rt#8 mib |
\rt#16 mib |
\rt#8 mi! \rt#8 mi |
\rt#8 mi! \rt#8 fa |
\rt#8 fa fa4\f r4 |
r2 r8. mib16 mib8. mib16 |
mib4 r r2 |
fad4 r r2 |
sol4.\f sol,8 sib,4 re |
sol sol,8. sol,16 sol,4 la, |
