\clef "treble"
<<
  \tag #'violino1 {
    mib'1\repeatTie~ |
    mib'2 reb'' |
    do''1 |
    si'! |
  }
  \tag #'violino2 {
    sib1\repeatTie~ |
    sib2 sib' |
    lab'1 |
    lab' |
  }
>>
r4 <sol re' si'!>\f lab2~ |
lab1~ |
lab2 r8. <<
  \tag #'violino1 { lab''16 lab''4 | }
  \tag #'violino2 { reb''16 reb''4 | }
>>
R1 |
r2 <<
  \tag #'violino1 { sol''4 }
  \tag #'violino2 { reb''4 }
>> r4 |
r2 r4 <mib' sib' sol''> |
<mib' do'' lab''> r r2 |
R1 |
<<
  \tag #'violino1 {
    fa''1\p |
    sol''~ |
    sol''2 fa''~ |
    fa''1~ |
    fa''2 mib''~ |
    mib''1~ |
    mib''2 r4 <re' la' fad''>\f |
    <mib' sib' sol''>16 sib''[ sib'' sib''] \rt#4 sib'' <>\p \rt#4 sib'' \rt#4 sib'' |
    \rt#16 sib'' |
    \rt#8 reb''' \rt#8 reb''' |
    \rt#8 reb''' \rt#8 do''' |
    \rt#8 do''' si''!4\f r4 |
  }
  \tag #'violino2 {
    sib'1-\sug\p~ |
    sib'~ |
    sib'2 si'~ |
    si'1~ |
    si'2 do''~ |
    do''1~ |
    do''2 r4 <re' do''>4-\sug\f |
    <mib' sib'>16 <<
      { sol''16 sol'' sol'' \rt#4 sol'' \rt#4 sol'' \rt#4 sol'' } \\
      { sib' sib' sib' \rt#4 sib' <>-\sug\p \rt#4 sib' \rt#4 sib' }
    >>
    \rt#16 <sib' sol''>16 |
    \rt#8 <reb'' sib''> \rt#8 q |
    \rt#8 q \rt#8 <do'' lab''> |
    \rt#8 q <re' si'! sol''>4-\sug\f r |
  }
>>
r2 r8. <<
  \tag #'violino1 { do'''16 do'''8. do'''16 | do'''4 }
  \tag #'violino2 <<
    { sol''16 sol''8. sol''16 | sol''4 } \\
    { do''16 do''8. do''16 | do''4 }
  >>
>> r4 r2 |
<<
  \tag #'violino1 { re'''4 }
  \tag #'violino2 << la''!4 \\ la' >>
>> r4 r2 |
<re' sib' sol''>4.\f sol8 sib4 re' |
sol' sol'8. sol'16 sol'4 la'8 sol'16 la' |
