\clef "alto" sol1\repeatTie~ |
sol2 sol' |
do'1 |
re' |
r4 <sol re' si'!>4-\sug\f lab2~ |
lab1~ |
lab2 r8. lab'16 lab'4 |
R1 |
r2 sib'4 r |
r2 r4 reb'4 |
do'4 r r2 |
R1 |
re'1-\sug\p |
mib'~ |
mib'2 fa'~ |
fa'1~ |
fa'2 sol'~ |
sol'1~ |
sol'2 r4 re'-\sug\f |
\rt#8 mib'16 <>-\sug\p \rt#8 mib' |
\rt#16 mib' |
\rt#8 mi'! \rt#8 mi' |
\rt#8 mi'! \rt#8 fa' |
\rt#8 fa' sol'4-\sug\f r |
r2 r8. mib'16 mib'8. mib'16 |
mib'4 r r2 |
fad'4 r r2 |
sol'4.\f sol8 sib4 re' |
sol' sol8. sol16 sol4 la |
