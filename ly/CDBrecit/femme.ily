\clef "vbas-dessus" R1 |
r4 r8 sib' reb''8.^! sib'16 sib'8. do''16 |
lab'4^! r8 fa'' fa'' do'' do'' re'' |
si'!4.^! re''16 do'' si'4 si'8 do'' |
sol'4^! r4 r2 |
R1*5 |
r16 lab' lab' sib' do''8 mib''16 mib'' do''4 reb''8 mib'' |
lab' lab' r lab' do'' do'' do'' do'' |
sib'^! sib' r sib'16 sib' sib'4 lab'?8 sib' |
sol'4^! r4 sib'4. sib'16 sib' |
mib''8 mib''16 mib'' mib''8 re''16 do'' si'!8^! si' r si' |
si' si' si' do'' re''4 re''8 re'' |
re''4 si'8 sol' do''4^! r8 sol' |
do'' do'' do'' mib'' do''4 r8 do''16 do'' |
do''4 do''8 sib' sol'^! sol' r4 |
R1*10 |
