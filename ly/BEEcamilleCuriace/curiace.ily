\clef "alto/G_8" R2*8 |
r4 r8 re |
sol4 sol8 sol |
lab4 lab8. lab16 |
sol4 r |
r r8 sol |
re'4 re'8 re' |
re'4 re'8 re' |
sib8 sol r4 |
R2*3 |
r4 r8 la^\markup\italic { più adagio } |
re'8. la16 la8. la16 |
sib8.\fermata sol16 sib8. sol16 |
la4. la8 |
re4 r |
