\clef "treble" r8. la16[( do'8-\sug\sf la)] |
r8. \slurDashed sol'16([ sol'8-\sug\sf sol']) |
r8. fa'16[( fa'8-\sug\sf fa')] |
r8. sol'16[( sol'8-\sug\sf sol')] | \slurSolid
r8. <>^\markup\italic { [plus serré] } fa'16[ la'8-. do'']-. |
la'4 la'-\sug\p |
re'8. re'16 re'8( sib) |
sol'4-\sug\mf sol'8 sol' |
la4 r |
re'16-\sug\p re' re' re' \rt#4 re' |
<>-\sug\fp \rt#8 re' |
\rt#8 mib' |
<>-\sug\fp \rt#8 mi' |
<>-\sug\f la8. la16 sib8. sib16 |
do'8. do'16 la8. la16 |
sib4 r |
R2*4 |
re'2 |
re'8\fermata r r4 |
re'4( dod') |
re'4 r |
