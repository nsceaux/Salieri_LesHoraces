\clef "soprano/treble" r4 r8 fa' |
do''4 r |
sib' r |
r8 sol'16. sol'32 do''8. sol'16 |
la'8 la' r4 |
r4 re''8. do''16 |
sib'4. sib'8 |
do''4 do''8 mib'' |
fad'4 r |
R2*6 |
r4 r8 re'' |
dod'' dod'' mi'' la' |
re''4 re''8 mi'' |
fa''4 re''8 sold' |
la'4 r |
R2*4 |
