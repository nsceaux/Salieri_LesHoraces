\clef "alto" fa4 r |
r8. \slurDashed mi'16[( mi'8-\sug\sf mi')] |
r8. re'16[( re'8-\sug\sf re')] |
r8. do'16[( do'8-\sug\sf do')] | \slurSolid
r8. <>^\markup\italic { plus serré } fa'16[ fa'8 la] |
re'4 re'-\sug\p |
sol'4. sol8 |
mib4-\sug\mf mib'8 do' |
re'4 r |
sib16-\sug\p sib sib sib \rt#4 sib |
<>-\sug\fp \rt#8 fa' |
mib' do' do' do' \rt#4 do' |
<>-\sug\fp \rt#8 dod' |
<>-\sug\f re'8. re'16 re'8. re'16 |
re'8. re'16 re'8. re'16 |
sol4 r |
R2*4 |
fa2 |
sol8\fermata r r4 |
la2 |
re4 r |
