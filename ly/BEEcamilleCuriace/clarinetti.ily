\clef "treble" R2*15 |
r4 r8 \twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { re'' | dod''2 | re''4 re''8 mi'' | fa''4. sold''8 | la''4 }
  { re'8 | sol'2 | fa'4 fa'8 mi' | re'4 re'' | dod'' }
>> r4 |
R2*4 |
