\clef "treble" r8 r16 fa'[( la'8\sf fa')] |
r8. do''16[( mi''8\sf do'')] |
r8. sib'16[( re''8\sf sib')] |
r8. sol'16[( do''8\sf sol')] |
r8. <>^\markup\italic { plus serré } la'16\f[ <fa' do''>8-. <fa' mib''!>]-. |
<re'' re'>4 re''8.\p do''16 |
sib'8. sib'16 sib'8( sol') |
do''4\mf do''8-. mib''-. |
fad'4 r |
sol'16\p sol' sol' sol' \rt#4 sol' |
<>\fp \rt#4 lab'16 \rt#4 lab' |
<sol' sol>4. r16 sol'32 la' |
sib'8.\fp sib'16 la'!8. sol'16 |
fad'8.\f fad'16 sol'8. sol'16 |
la'8. la'16 fad'8. fad'16 |
sol'4 r |
R2*4 |
la'2 |
sib'8\fermata r r4 |
fa'4( mi') |
re'4 r |
