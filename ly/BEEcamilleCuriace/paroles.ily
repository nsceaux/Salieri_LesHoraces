\tag #'camille {
  O Ciel ! quoi, ma pri -- ère est vai -- ne ;
  Je n’ai plus sur toi de pou -- voir !
}
\tag #'curiace {
  Tu sais qu’un bar -- ba -- re de -- voir
  Com -- mande à mon cœur & l’en -- traî -- ne.
}
\tag #'camille {
  Tu ne te sou -- viens plus que ton cœur est à moi ?
}
\tag #'curiace {
  J’é -- tois à mon pa -- ys a -- vant que d’être à toi.
}
