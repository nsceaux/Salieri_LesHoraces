\clef "treble"
\twoVoices #'(oboe1 oboe2 oboi) <<
  { s4 la' | s do'' | s sib' | s sib' | }
  { s4 fa' | s sol' | s fa' | s sol' | }
  { r4 s | r s | r s | r s | }
>>
la'8 r16 <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''16[ fa''8 mib''!] | re''4 }
  { la'16[ la'8 la'] | la'4 }
>> r4 |
R2*3 |
sol'2-\sug\p |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { lab'2 | sol' | sib'! | la'!4 sib' | do'' la' | }
  { fa'2 | mib' | mi' | fad'4 sol' | la' fad' | }
  { s2-\sug\fp | s | s-\sug\fp | s-\sug\f }
>>
sol'4 r |
R2*8 |
