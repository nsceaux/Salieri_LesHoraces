\clef "tenor" r4 fa' |
r mi'? |
r re' |
r do' |
fa'8.\noBeam fa16[\f fa8 fa] |
fad4 r |
R2*10 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { mi'2 | re'4 fa'8 dod' | re'2 | mi'4 }
  { la2 | sib~ | sib | la4 }
>> r4 |
R2*4 |
