\clef "bass" fa2\fp |
mi\fp |
re\fp |
mi\sf |
fa8.\f fa16[ fa8 fa] |
fad4 fad\p |
sol4. sol8 |
mib4\mf mib8 do |
re4 r |
sib,8\p sib, sib, sib, |
<>\fp \rt#4 si,8 |
\rt#4 do |
<>\fp \rt#4 dod |
re8.\f re16 re8. re16 |
re4 re8. re16 |
sol,4 r |
R2*4 |
fa2 |
sol8\fermata r r4 |
la4 la, |
re r |
