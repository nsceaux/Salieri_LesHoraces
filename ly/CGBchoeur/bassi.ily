\clef "bass" sol,4 |
do2 r4 do' |
fa2 r4 fa |
sol4 sol8 sol sol4 sol, |
do4 do' fa fa |
sol sol8 sol sol,4 sol, |
do4. do8 do4 do |
do1 |
