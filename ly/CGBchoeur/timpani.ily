\clef "bass" sol,4 |
do2 do4 do8. do16 |
do2 r4 do |
\rt#6 sol,8*2/3 \rt#6 sol, |
\rt#6 do \rt#6 do |
\rt#6 sol, \rt#6 sol, |
do4. do8 do4 do |
do1 |
