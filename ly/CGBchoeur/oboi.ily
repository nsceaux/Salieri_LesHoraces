\clef "treble"
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''4 | mi''8*2/3 do'' do'' }
  { si'4 | do''8*2/3 do'' do'' }
>> \rt#3 do'' \rt#6 do'' |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { la''2. la''4 |
    mi''2 re''4. re''8 |
    mi''2 la''4 la'' |
    mi''2 re''4. re''8 | }
  { do''2. do''4 |
    do''2 si'4. si'8 |
    do''2 do''4 do'' |
    do''2 si'4. si'8 | }
>>
do''4. do'8 do'4 do' |
do'1 |
