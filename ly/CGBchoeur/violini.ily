\clef "treble"
<<
  \tag #'violino1 {
    <re' si' sol''>4 | mi''8*2/3 do'' do''
  }
  \tag #'violino2 {
    <sol re' si'>4 | <sol mi' do''>8*2/3 do'' do''
  }
>> \rt#3 do'' \rt#6 do'' |
<<
  \tag #'violino1 {
    <fa' do'' la''>2 r4 q | sol''8*2/3
  }
  \tag #'violino2 {
    \rt#6 do''8*2/3 \rt#6 do'' | do''
  }
>> <mi'' do'''>8*2/3 q \rt#3 q \rt#6 <re'' si''> |
\rt#3 <mi'' do'''> \rt#3 q do'' fa'' la'' la'' la'' la'' |
sol'' <mi'' do'''> q \rt#3 q \rt#3 <re'' si''> \rt#3 q |
<<
  \tag #'violino1 {
    <mi'' do'''>4.
  }
  \tag #'violino2 {
    <do'' do'''>4.
  }
>> do'8 do'4 do' |
do'1 |
