\clef "treble"
\twoVoices #'(corno1 corno2 corni) <<
  { re''4 | mi''8*2/3 do'' do'' }
  { sol'4 | sol'8*2/3 do'' do'' }
>> \rt#3 do'' \rt#6 do'' |
\rt#6 do'' \rt#6 do'' |
\twoVoices #'(corno1 corno2 corni) <<
  { mi''2 re''4. re''8 |
    mi''4. do''8 do''4 do'' |
    mi''2 re''4. re''8 |
    do''4. }
  { do''2 sol'4. sol'8 |
    do''4. do''8 do''4 do'' |
    do''2 sol'4. sol'8 |
    mi'4. }
>> do'8 do'4 do' |
do'1 |
