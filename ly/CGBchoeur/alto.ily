\clef "alto" sol |
do'8*2/3 do'' do'' \rt#3 do'' \rt#6 do'' |
\rt#6 do'' \rt#6 do'' |
\rt#6 sol' \rt#6 sol' |
\rt#3 sol' \rt#3 sol' la' do'' do'' do'' do'' do'' |
\rt#3 sol' \rt#3 sol' \rt#3 sol' \rt#3 sol' |
do'4. do'8 do'4 do' |
do'1 |
