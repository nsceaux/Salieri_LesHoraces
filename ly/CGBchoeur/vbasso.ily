\clef "vbasse" si8. si16 |
do'4 do' r do'8 do' |
fa'2 r4 do'8. do'16 |
do'2 si4. si8 |
do'2 do'4 do' |
do'2 si4. si8 |
do'2 r |
R1 |
