\clef "vbasse" r8 |
r2\fermata r4 r8\fermata la8 |
re'2 r8. re'16 re'8. re'16 |
si2 r4 r8 si |
mi4. sol8 sol4. sold8 |
la la r4 r2\fermata |
r2 r8 la la la |
re'4 re'8 re' la4 la8 la |
re'2 r8 si si si |
sol2 re4 re |
la2 la4. la8 |
re4 r r2 |
R1*4 |
r2 r4 mi |
la mi' dod' la |
mi mi r mi |
la mi' dod' la |
re' re'8 re' re'4 re' |
si si si si |
sold2 r4 r8 mi |
la4. la8 la4. la8 |
la4 la, r4 r8 la |
la4. la8 la4. la8 |
lad4 lad r2 |
r4 si si sold |
la2 re |
mi2. mi4 |
fad fad mi re |
dod2 re |
mi2. mi4 |
la4 r r2 |
R1 |
r2 r4 r8 la, |
re4. re8 fad4. re8 |
la2 r4 r8 la |
re'4. re8 fad4. re8 |
la4 la r2 |
R1^\fermata |
r2 r8 fad fad fad |
si4 si8 si fad4 fad8 fad |
si2 r8 si si si |
sol2 re4. re8 |
la2 la4. la8 |
re2 r4 r8 la |
re'4. re'8 re'4. re'8 |
sol4 sol r r8 fad |
sol4. sol8 mi4. mi8 |
la4 la r2 |
r2 r4 mi |
la mi' dod' la |
mi mi r mi |
la mi' dod' la |
re' re'8 re' re'4 re' |
re'2 sol |
la2. la4 |
re r r2 |
r2 r4 la |
re fad la re' |
la la r la |
re fad la re' |
la la r2 |
re'2 re'4 re' |
re'2 sol |
la2. la4 |
si si si dod' |
re'2 si |
sol la |
re4 r r2 |
R1*7 |
