\clef "treble" la'16 si'32 dod'' |
re''4.\fermata r16 re'32*1/2 mi' fad' sold' la'4.\fermata r8 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { s8. re''16 re''8. re''16 re''2 |
    s8. fad''16 fad''8. fad''16 fad''4. fad''8 |
    sol''4. mi''8 mi''4. re''8 |
    dod''8 la'16. la'32 }
  { s8. fad'16 fad'8. fad'16 fad'2 |
    s8. re''16 re''8. re''16 re''4. re''8 |
    si'4. si'8 si'4. si'8 |
    mi'8 la'16. la'32 }
  { r8. s16 s2. | r8. s16 s2. | }
>> la'8 la' la'2\mordent\fermata |
r2 r8 \twoVoices #'(oboe1 oboe2 oboi) <<
  { dod''16 dod'' dod''8 dod'' |
    re'' la' fad' re' s mi''16 mi'' mi''8 mi'' |
    fad'' re'' la' fad' s8 fad''16 fad'' fad''8 fad'' |
    sol''2 fad'' | mi''1 | re''4 }
  { mi'16 mi' mi'8 mi' |
    fad' la' fad' re' s dod''16 dod'' dod''8 dod'' |
    re'' la' fad' re' s re''16 re'' re''8 re'' |
    si'2 la' | dod''1 | re''4 }
  { s4. | s2 r8 s4. | s2 r8 s4. | }
>> r4 r2 |
re''2 la'' |
fad''8 re''[ re'' re''] dod'' si' la' sol' |
fad'4 re'8. mi'16 fad'4.\trill mi'16 re' |
la'2 r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { dod''4 | si'4. dod''8 re''4 re'' |
    dod''2 s4 dod'' | si'4. dod''8 re''4 re'' |
    dod''2 s | fad''1 | fad'' | }
  { la'4 | sold'4. la'8 si'4 si' |
    la'2 s4 la' | sold'4. la'8 si'4 si' |
    la'2 s | la'1 | re'' | }
  { s4 | s1 | s2 r4 s | s1 | s2 r | }
>>
mi''8 si''[ si'' si''] si''2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { dod''2~ dod''4. mi''8 | mi''1~ | mi''2~ mi''4. sol''8 | fad''4 }
  { la'2~ la'4. dod''8 | dod''1~ | dod''2~ dod''4. mi''8 | re''4 }
>> r4 r2 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''2 fad''4 | mi''2 fad'' | si'2. dod''4 | re'' re'' mi'' fad'' |
    mi''2 fad'' | si'4 sold''2 sold''4 | la'' }
  { re''2 re''4 | dod''2 re'' | sold'1 | la' |
    la' | sold'4 si'2 si'4 | la' }
>> r4 r2 |
la'2 mi'' |
dod''4 r r r8 la' |
re'4. re'8 fad'4.\trill mi'16 re' |
la'4. la'8 dod''4.\prall si'16 la' |
re''4. re''8 fad''4.\prall mi''16 re'' |
la''4 la'8. la'16 la'4 la' |
la'1\mordent\fermata |
r2 r8 \twoVoices #'(oboe1 oboe2 oboi) <<
  { lad''16 lad'' lad''8 lad'' |
    si'' s4. s8 dod'''16 dod''' dod'''8 dod''' |
    re''' s4. s8 fad''16 fad'' fad''8 fad'' |
    sol''2 fad'' | mi''1 |
    s8 fad'' fad'' fad'' mi''16 fad'' re'' mi'' dod''8 mi'' |
    re''4 }
  { dod''16 dod'' dod''8 dod'' |
    re'' s4. s8 lad''16 lad'' lad''8 lad'' |
    re''' s4. s8 re''16 re'' re''8 re'' |
    si'2 la' | dod''1 |
    s8 re'' re'' re'' dod''16 re'' si' dod'' la'8 sol' |
    fad'4 }
  { s4. | s8 fad''[ re'' si'] r8 s4. | s8 si'' fad'' re'' r8 s4. | s1*2 | r8 }
>> r4 r2 |
r8 \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''8 sol'' sol'' fad''16 sol'' mi'' fad'' re'' mi'' do'' re'' |
    si'4 }
  { si'8 si' si' la' sol' fad' la' | sol'4 }
>> r4 r2 |
r2 r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { dod''4 | si'4. dod''8 re''4 re'' |
    dod''2 s4 dod'' | si'4. dod''8 re''4 re'' |
    dod''2 s | fad''1 | fad''2 sol'' | mi''1 |
    re''2 s4 fad'' | mi''4. fad''8 sol''4 sol'' |
    fad''2 s4 fad'' | mi''4. fad''8 sol''4 sol'' | fad''2 }
  { la'4 | sold'4. la'8 si'4 si' |
    la'2 s4 la' | sold'4. la'8 si'4 si' |
    la'2 s | la'1 | la'2 si' | dod''1 |
    re''2 s4 re'' | dod''4. re''8 mi''4 mi'' |
    re''2 s4 re'' | dod''4. re''8 mi''4 mi'' | re''2 }
  { s4 | s1 | s2 r4 s | s1 | s2 r | s1*3 | s2 r4 s | s1 | s2 r4 s | }
>> r2 |
R1 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''1 | fad''2 sol'' | mi''2. fad''4 | sol'' sol''2 sol''4 |
    fad''1 | mi'' | re''4 }
  { la'1 | la'2 si' | dod''2. dod''4 | re''2. mi''4 |
    re''1~ | re''2 dod'' | re''4 }
>> r4 r2 |
re''2 la'' |
fad''8 r r4 r2 |
R1 |
r2 r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''4 | fad'' }
  { la'4 | la' }
>> r4 r2 |
r2 r4 la' |
re' r r2 |
