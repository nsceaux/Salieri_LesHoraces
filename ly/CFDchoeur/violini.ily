\clef "treble" la16 si32 dod' |
re'4.\fermata r32*1/2 la64 si dod' re' mi' fad' sold' la'4.\fermata r8 |
<<
  \tag #'violino1 {
    r8. <fad' re''>16 q8. q16 q2 |
    r8. << { fad''16 fad''8. fad''16 fad''4. }
      \\ { re''16 re''8. re''16 re''4. } >> fad''8 |
    sol''4. mi''8 mi''4. re''8 |
    dod''
  }
  \tag #'violino2 {
    r8. <fad' la'>16 q8. q16 q2 |
    r8. <fad' re''>16 q8. q16 q4. re''8 |
    si'4. si'8 si'4. si'8 |
    mi'8
  }
>> la16. la32 la8 la la2\mordent\fermata |
re'16 mi' fad' sol' la' si' dod'' re'' la8 <mi' dod''>16 q q8 q |
<la fad' re''>8 la' fad' re' la <<
  { mi''16 mi'' mi''8 mi'' | } \\
  { dod''16 dod'' dod''8 dod'' | }
>>
<re' la' fad''>8 re'' la' fad' <<
  \tag #'violino1 {
    si8 <<
      { fad''16 fad'' fad''8 fad'' | } \\
      { re''16 re'' re''8 re'' | }
    >>
    <si' sol''>8 <re' si'>4 <si' sol''>8 <la' fad''> <la fad'>4 <la' fad''>8 |
    <la' mi''>8 <<
      { dod'''16 dod''' dod'''8 dod''' \rt#4 dod'''16 \rt#4 dod''' | } \\
      { mi''16 mi'' mi''8 mi'' \rt#4 mi''16 \rt#4 mi'' | }
    >> re'8 re'''[ re''' re'''] dod'''16 re''' si'' dod''' la'' si'' sol'' la'' |
    fad''8 fad''-. mi''-. re''-. dod''16 re'' mi'' re'' dod'' la' si' dod'' |
    re''8
  }
  \tag #'violino2 {
    si16 dod' re' mi' fad' sold' lad' si' |
    sol la si do' re' mi' fad' sol' re' mi' fad' sol' la' si' dod''! re'' |
    la si dod' re' mi' fad' sold' la' la si dod' re' mi' fad' sold' la' |
    re'2 la' |
    si' la' |
    fad'8
  }
>> re''8[ re'' re''] dod''16 re'' si' dod'' la' si' sol' la' |
fad'4 re'8. mi'16 fad'4.\prall mi'16 re' |
la'4 r <<
  \tag #'violino1 {
    r2 | r2 r4 r8 mi''16 fad''32 sold'' |
    la''4 mi'' dod'' la' | mi'2 r4 r8 mi''16 fad''32 sold'' |
    la''4 mi'' dod'' la' |
  }
  \tag #'violino2 {
    r4
    <<
      { dod''4 | si'4. dod''8 re''4 re''( | dod''4) } \\
      { \sugNotes { mi'4 } | mi'2. mi'4 | mi' }
    >> r4 r <<
      { dod''4 | si'4. dod''8 re''4 re''( | dod'') } \\
      { mi'4 | mi'2. mi'4 | mi'4 }
    >> r r2 |
  }
>>
re'16( la' fad'' la' fad'' la' fad'' la' fad'' la' fad'' la' fad'' la' fad'' la') |
fad''( re'' si' re'' fad'' re'' fad'' re'' fad'' re'' fad'' re'' fad'' re'' fad'' re'') |
<<
  \tag #'violino1 {
    mi''8 << { si'' si'' si'' } \\ { si' si' si' } >> si''16 la'' sold'' fad'' mi'' re'' dod'' si' |
    <<
      { dod''4.. la'16 la'4.. mi''16 |
        mi''4.. dod''16 dod''4.. mi''16 |
        mi''4.. dod''16 dod''4. } \\
      { la'4.. mi'16 mi'4.. dod''16 |
        dod''4.. mi'16 mi'4.. dod''16 |
        dod''4.. mi'16 mi'4. }
    >> sol''!8 |
    fad''8
  }
  \tag #'violino2 {
    mi''16 si' mi'' si' mi'' si' mi'' si' mi'' mi' mi' mi' \rt#4 mi' |
    la' <mi' dod'> q q \rt#4 q \rt#8 q |
    \rt#8 q \rt#8 q |
    \rt#8 q \rt#8 q |
    <re' fad'>8
  }
>> fad''8[ fad'' fad''] fad''16 sol'' mi'' fad'' re'' mi'' dod'' mi'' |
re''8 <<
  { fad''4 fad'' fad'' fad''8 | } \\ { re''4 re'' re'' re''8 | }
>>
<<
  \tag #'violino1 {
    <dod'' mi''>16-. la''( sold'' la'' si'' la'' sold'' la'')
    fad''-. la''( sold'' la'' si'' la'' sold'' la'') |
    si'8 <mi' si'>4 q q <mi' dod''>8 | <re' re''>8
  }
  \tag #'violino2 {
    <<
      { mi''8 dod''4 mi''8 fad'' fad'4 fad''8 | } \\
      { dod''8 mi'4 dod''8 re'' la4 re''8 | }
    >>
    sold'8 sold'4 sold' sold' sold'8 | la'
  }
>> re''16( la') re''( la' re'' la' mi'' la' mi'' la' fad'' la' fad'' la') |
<<
  \tag #'violino1 {
    mi''16-. la''( sold'' la'' si'' la'' sold'' la'') fad''-. la''( sold'' la'' si'' la'' sold'' la'') |
    mi''8 sold''16 sold'' sold''8 si''16 si'' si''8 mi'''16 mi''' mi'''8 sold' |
  }
  \tag #'violino2 {
    mi''8 <mi' la>4 mi''8 fad'' <la fad'>4 fad''8 |
    sold' << { sold''16 sold'' \rt#4 sold'' \rt#8 sold'' }
      \\ { si'16 si' \rt#4 si' \rt#8 si' }
    >>
  }
>>
la'8 la''[ la'' la''] sold''16 la'' fad'' sold'' mi'' fad'' re'' mi'' |
dod''8 dod''[ si' la'] sold'16 la' si' la' sold' mi' fad' sold' |
la'8 la la' la' sol'!16 fad' mi' re' dod' la si dod' |
re'4. re'8 fad'4.\trill mi'16 re' |
la'4. la'8 dod''4.\prall si'16 la' |
re''4. re''8 fad''4.\prall mi''16 re'' |
la''4 la8. la16 la4 la |
la1\mordent\fermata |
si16 dod' re' mi' fad' sold' lad' si' fad'8 <<
  { lad''16 lad'' lad''8 lad'' | si'' } \\
  { dod''16 dod'' dod''8 dod'' | si' }
>> fad''8 re'' si' fad' <<
  \tag #'violino1 {
    dod'''16[ dod'''] dod'''8 dod''' |
    re''' si''[ fad'' re''] re' <<
      { fad''16 fad'' fad''8 fad'' |
        sol'' si'4 sol''8 fad'' fad'4 fad''8 |
        mi'' dod'''16[ dod'''] dod'''8 dod''' \rt#4 dod'''16  \rt#4 dod''' | } \\
      { re''16 re'' re''8 re'' |
        si' re'4 si'8 la' la4 la'8 |
        la' mi''16[ mi''] mi''8 mi'' \rt#4 mi''16 \rt#4 mi'' | }
    >>
    re'8 re'''[ re''' re'''] dod'''16 re''' si'' dod''' la'' si'' sol'' la'' |
    fad''4
  }
  \tag #'violino2 {
    <<
      { lad''16 lad'' lad''8 lad'' | } \\
      { dod''16 dod'' dod''8 dod'' | }
    >>
    << si''8 \\ si' >> fad''[ re'' si'] si16 dod' re' mi' fad' sold' lad' si' |
    sol la si do' re' mi' fad' sol' re' mi' fad' sol' la' si' dod''! re'' |
    la si dod' re' mi' fad' sold' la' la si dod' re' mi' fad' sold' la' |
    re'8 fad''[ fad'' fad''] mi''16 fad'' re'' mi'' dod''8 mi'' |
    re''4
  }
>> r4 r2 |
r8 <<
  \tag #'violino1 {
    sol''8 sol'' sol'' fad''16 sol'' mi'' fad'' re'' mi'' do'' re'' |
    si'4
  }
  \tag #'violino2 {
    si'8 si' si' la'16 si' sol' la' fad'8 la' |
    sol'4
  }
>> r4 r2 |
<<
  \tag #'violino1 {
    R1 |
    r2 r4 r8 mi''16 fad''32 sold'' |
    la''4 mi'' dod'' la' |
    mi'2 r4 r8 mi''16 fad''32 sold'' |
    la''4 mi'' dod'' la' |
  }
  \tag #'violino2 {
    r2 r4 <<
      { dod''4 | si'4. dod''8 re''4 re''( | dod''4) } \\
      { mi'4 | mi'2. mi'4 | mi' }
    >> r4 r <<
      { dod''4 | si'4. dod''8 re''4 re''( | dod''4) } \\
      { mi'4 | mi'2. mi'4 | mi' }
    >> r4 r2 |
  }
>>
re'16 la' fad'' la' fad'' la' fad'' la' fad'' la' fad'' la' fad'' la' fad'' la' |
<<
  \tag #'violino1 {
    fad''16 re'' dod'' re'' mi'' re'' dod'' re'' si' dod'' re'' mi'' fad'' sol'' la'' si'' |
    la' dod'' mi'' dod'' la'' mi'' dod'' la' la'' mi'' dod'' la' la'' mi'' dod'' mi'' |
    re''4 r r2 |
    r r4 r8 la'16 si'32 dod'' |
    re''4 fad'' la'' re''' |
    la''2 r4 r8 la16 si32 dod' |
    re'4 fad' la' re'' |
  }
  \tag #'violino2 {
    fad''8 <<
      { la'4 la'8 si' si'4 si'8 | } \\
      { fad'4 fad'8 mi' mi'4 mi'8 | }
    >>
    mi'8 <mi' dod''>4 q q q8 |
    <fad' re''>4 r r <<
      { fad''4 | mi''4. fad''8 sol''4 sol''( | fad''4) } \\
      { la' | la'2. la'4 | la' }
    >> r4 r <<
      { fad''4 | mi''4. fad''8 sol''4 sol''( | fad''4) } \\
      { la' | la'2. la'4 | la' }
    >> r4 r2 |
  }
>>
la8 la''[ la'' la''] la''16 si'' sol'' la'' fad'' sol'' mi'' sol'' |
<<
  \tag #'violino1 {
    <re' la' fad''>2 q4 q |
    fad''16 re'' dod'' re'' mi'' re'' dod'' re'' si' sol'' fad'' sol'' si'' sol'' fad'' sol'' |
    mi''8 <<
      { mi''4 mi'' mi'' fad''8 | } \\
      { la'4 la' la' la'8 | }
    >>
    sol''16 sol'' fad'' sol'' la'' sol'' fad'' sol'' si'' sol'' fad'' sol'' mi'' sol'' fad'' sol'' |
    fad''8
  }
  \tag #'violino2 {
    <re' la' fad''>8 <fad' la'>4 q q q8 |
    <fad' la'>8 q4 q8 <re' si'>8 q4 q8 |
    dod''8 dod''4 dod'' dod'' dod''8 |
    re''16 re' re' re' \rt#4 re' re'8[ re']( mi')[ la] |
    re'8
  }
>> <<
  { re'''16 re''' re'''8 re''' \rt#4 re'''16 \rt#4 re''' |
    \rt#4 re'''16 \rt#4 re''' \rt#4 dod''' \rt#4 dod''' | } \\
  { fad''16 fad'' fad''8 fad'' \rt#4 fad''16 \rt#4 fad'' |
    \rt#4 mi'' \rt#4 mi'' \rt#4 mi'' \rt#4 mi'' | }
>>
<<
  \tag #'violino1 {
    re'8 re'''[ re''' re'''] dod'''16 re''' si'' dod''' la'' si'' sol'' la'' |
    fad''8 fad''[-. mi''-. re''-.] dod''16 re'' mi'' re'' dod'' la' si' dod'' |
    re''8
  }
  \tag #'violino2 {
    re'2 la' | si' la' | fad'8
  }
>> re''8[ re'' re''] dod''16 re'' si' dod'' la' si' sol' la' |
fad' sol' mi' fad' re' mi' dod' re' si4 sib |
la4~ la16 si32 dod' re' mi' fad' sold' la'4 <la' mi''> |
<re' la' fad''>4. r16 re' fad'4.\mordent mi'16 re' |
si'4.\prall la'16 sold' la'4 la |
re'4 r r2 |
