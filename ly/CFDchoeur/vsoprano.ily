\clef "vbas-dessus" r8 |
r2\fermata r4 r8\fermata la'8 |
re''2 r8. re''16 re''8. re''16 |
fad''2 r4 r8 fad'' |
sol''4. mi''8 mi''4. re''8 |
dod'' la' r4 r2\fermata |
r2 r8 dod'' dod'' dod'' |
re''4 re''8 re'' mi''4 mi''8 mi'' |
fad''2 r8 fad'' fad'' fad'' |
sol''2 fad''4 fad'' |
mi''2 la''4. la''8 |
re''4 r r2 |
R1*3 |
r2 r4 dod'' |
si'4. dod''8 re''4 re'' |
dod'' la' r dod'' |
si'4. dod''8 re''4 re'' |
dod'' la' r2 |
fad''2 fad''4 fad'' |
fad'' fad'' fad'' fad'' |
mi''2 r4 r8 si' |
dod''4. la'8 la'4. mi''8 |
mi''4 dod'' r r8 mi'' |
mi''4. dod''8 dod''4. sol''!8 |
fad''4 fad'' r2 |
r4 fad'' fad'' fad'' |
mi''2 fad'' |
si'2. dod''4 |
re'' re'' mi'' fad'' |
mi''2 fad'' |
si'2. mi''4 |
la' r r2 |
R1 |
r2 r4 r8 la' |
re'4. re'8 fad'4. re'8 |
la'2 r4 r8 la' |
re''4. re''8 fad''4. re''8 |
la'4 la' r2 |
R1^\fermataMarkup |
r2 r8 lad' lad' lad' |
si'4 si'8 si' dod''4 dod''8 dod'' |
re''2 r8 fad'' fad'' fad'' |
sol''2 fad''4. fad''8 |
mi''2 la''4. la''8 |
re''2 r4 r8 dod'' |
re''4. re''8 re''4. re''8 |
si'4 si' r4 r8 la' |
si'4. si'8 sol''4. mi''8 |
dod''4 la' r dod'' |
si'4. dod''8 re''4 re'' |
dod'' la' r dod'' |
si'4. dod''8 re''4 re'' |
dod'' la' r2 |
fad''2 fad''4 fad'' |
fad''2 sol'' |
mi''2. mi''4 |
re'' r r re'' |
dod''4. re''8 mi''4 mi'' |
re'' la' r re'' |
dod''4. re''8 mi''4 mi'' |
re'' la' r2 |
R1 |
fad''2 fad''4 fad'' |
fad''2 sol'' |
mi''2. fad''4 |
sol'' sol'' sol'' sol'' |
fad''2 fad'' |
mi'' mi'' |
re''4 r r2 |
R1*7 |
