\clef "vhaute-contre" r8 |
r2\fermata r4 r8\fermata la'8 |
la'2 r8. la'16 la'8. la'16 |
re'2 r4 r8 re' |
si'4. si'8 si'4. si'8 |
la' la' r4 r2\fermata |
r2 r8 la' la' la' |
la'4 la'8 la' la'4 la'8 la' |
la'2 r8 si' si' si' |
si'2 la'4 la' |
la'2 la'4. la'8 |
fad'4 r r2 |
R1*3 |
r2 r4 la' |
sold'4. la'8 si'4 si' |
la'4 mi' r la' |
sold'4. la'8 si'4 si' |
la'4 mi' r2 |
la'2 la'4 la' |
si'4 si' si' si' |
si'2 r4 r8 mi' mi'4. la'8 la'4. la'8 |
la'4 mi' r r8 la' |
la'4. mi'8 mi'4. mi'8 |
fad'4 fad' r2 |
r4 fad' fad' re' |
la'2 la' |
sold'2. sold'4 |
la' la' la' la' |
la'2 la' |
sold'2. sold'4 |
la'4 r r2 |
R1 |
r2 r4 r8 \sugNotes { la8 } |
re'4. re'8 fad'4. re'8 |
la'2 r4 r8 la' |
re'4. re'8 fad'4. re'8 |
la4 la r2 |
R1^\fermataMarkup |
r2 r8 fad' fad' fad' |
fad'4 fad'8 fad' lad'4 lad'8 lad' |
si'2 r8 si' si' si' |
si'2 la'4. la'8 |
la'2 la'4. la'8 |
la'2 r4 r8 la' |
la'4. la'8 la'4. la'8 |
si'4 sol' r r8 re' |
re'4. si'8 si'4. sol'8 |
mi'4 dod' r la' |
sold'4. la'8 si'4 si' |
la' mi' r la' |
sold'4. la'8 si'4 si' |
la' mi' r2 |
la'2 la'4 la' |
la'2 si' |
la'2. la'4 |
fad'4 la' la'2~ |
la'4. la'8 la'4 la' |
la' la' la'2 |
la'4. la'8 la'4 la' |
la' fad' r2 |
R1 |
la'2 la'4 la' |
la'2 si' |
la'2. la'4 |
la' si' si' la' |
la'2 si' |
si' la' |
la'4 r r2 |
R1*7 |
