\clef "treble" \transposition re
\twoVoices #'(corno1 corno2 corni) <<
  { sol'8 | do''4.\fermata s16 sol' sol'4.\fermata s8 |
    s8. do''16 do''8. do''16 do''2 |
    s8. mi''16 mi''8. mi''16 mi''4. mi''8 | }
  { sol8 | do'4.-\tag #'corno2 -\fermata s16 sol
    sol4.-\tag #'corno2 -\fermata s8 |
    s8. mi'16 mi'8. mi'16 mi'2 |
    s8. do''16 do''8. do''16 do''4. do''8 | }
  { s8 | s4. r16 s s4. r8 | r8. s16 s2. | r8. s16 s2. | }
>>
re''4. re''8 re''4. re''8 |
re''8 sol'16. sol'32 sol'8 sol' sol'2\fermata |
r2 r8 sol'16 sol' sol'8 sol' |
do'' sol' mi' do' r8 \twoVoices #'(corno1 corno2 corni) <<
  { re''16 re'' re''8 re'' |
    mi'' do'' sol' mi' mi'2 | }
  { sol'16 sol' sol'8 sol' |
    do'' sol' mi' do' do'2 | }
>>
r2 r8 \twoVoices #'(corno1 corno2 corni) <<
  { mi''16 mi'' mi''8 mi'' |
    re'' re''16 re'' re''8 re'' re'' re'' re'' re'' |
    do''4 }
  { do''16 do'' do''8 do'' |
    sol' sol'16 sol' sol'8 sol' sol' sol' sol' sol' |
    mi'4 }
>> r4 r2 |
\twoVoices #'(corno1 corno2 corni) <<
  { do''2 sol'' | mi''8 do''[ do'' do''] sol'2 | }
  { R1 | do'2 sol'2 | }
>>
mi'4 r r2 |
r4 re'' re''2~ |
re''1~ |
re''~ |
re''~ |
re'' |
\twoVoices #'(corno1 corno2 corni) <<
  { mi''1~ | mi'' | }
  { sol'1 | do'' | }
>>
re''8 re''16 re'' re''8 re'' \rt#4 re'' |
sol'1 |
sol'2~ sol'4. \twoVoices #'(corno1 corno2 corni) <<
  { re''8 | re''4. re''8 re''4. re''8 | mi'' }
  { sol'8 | sol'4. sol'8 sol'4. sol'8 | mi'8 }
>> mi'8[ mi' mi'] \rt#4 mi' |
mi'4 \twoVoices #'(corno1 corno2 corni) <<
  { mi''4 mi'' mi'' | re''2 mi'' | }
  { do''4 do'' do'' | sol'2 do'' | }
>>
re''1 |
mi''4 mi'' re'' do'' |
\twoVoices #'(corno1 corno2 corni) <<
  { re''2 mi'' | }
  { sol'2 do'' | }
>>
re''4. re''8 re''4 re'' |
sol'2 r |
sol'2 re'' |
sol'' r |
r4 r8 do' mi'4. do'8 |
sol'2 r4 r8 sol' |
do''4. do''8 mi''4. do''8 |
sol''4 sol'8. sol'16 sol'4 sol' |
sol'1\fermata |
R1*2 |
r2 r8 \twoVoices #'(corno1 corno2 corni) <<
  { mi''16 mi'' mi''8 mi'' | mi'' do''16 do'' }
  { do''16 do'' do''8 do'' | do'' do''16 do'' }
>> do''8 do'' \rt#4 do'' |
\twoVoices #'(corno1 corno2 corni) <<
  { re''2. }
  { sol'2. }
>> sol4 |
do'2 sol' |
do''4 r r2 |
R1*2 |
r4 re'' re''2~ |
re''1~ |
re'' |
re'' |
\twoVoices #'(corno1 corno2 corni) <<
  { re''1 | mi''4 }
  { sol'1 | do''4 }
>> do''8. do''16 do''4 do'' |
\twoVoices #'(corno1 corno2 corni) <<
  { mi''4 }
  { do''4 }
>> do''8. do''16 do''4 do'' |
sol'2 r |
r4 \twoVoices #'(corno1 corno2 corni) <<
  { sol''4 sol''2~ | sol''1~ | sol''~ | sol''~ | sol'' | sol''4 }
  { sol'4 sol'2~ | sol'1~ | sol'~ | sol'~ | sol' | sol'4 }
>> r4 r2 |
do'8 do''16 do'' do''8 do'' do'' sol' mi' sol' |
do'4 do''2 do''4 |
sol'2. sol'4 |
do''4 do''2 \twoVoices #'(corno1 corno2 corni) <<
  { re''4 | mi''1 | re''2 re'' | do'' r | do'' sol'' | mi''4 r r2 | }
  { sol'4 | do''1 | do''2 sol' | do' sol' | do'' r | R1 | }
>>
R1 |
r2 r4 \twoVoices #'(corno1 corno2 corni) <<
  { re''4 | mi'' }
  { sol'4 | do'' }
>> r4 r2 |
r r4 sol |
do'4 r r2 |
