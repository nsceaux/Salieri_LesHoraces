\tag #'all \key re \major
\time 4/4 \partial 8
\tempo "Maestoso" \midiTempo#60 s8 s1*5
\tempo "Allegro spiritoso" \midiTempo#120 s1*72 \bar "|."