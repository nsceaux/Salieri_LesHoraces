\clef "vtaille" r8 |
r2\fermata r4 r8\fermata fad'8 |
fad'2 r8. fad'16 fad'8. fad'16 |
fad'2 r4 r8 fad' |
mi'4. mi'8 mi'4. mi'8 |
mi' mi' r4 r2\fermata |
r2 r8 mi' mi' mi' |
fad'4 fad'8 fad' dod'4 dod'8 dod' |
re'2 r8 re' re' re' |
re'2 re'4 re' |
dod'2 dod'4. dod'8 |
re'4 r r2 |
R1*3 |
r4 mi' mi'2~ |
mi'4. mi'8 mi'4 mi' |
mi' mi' mi'2 |
mi'4. re'8 mi'4 mi' |
mi' dod' r2 |
fad'2 fad'4 fad' |
re' re' re' re' |
re'2 r4 r8 re' |
dod'4. dod'8 dod'4. dod'8 |
dod'4 la r r8 dod' |
dod'4. la8 la4. dod'8 |
dod'4 dod' r2 |
r4 re' re' re' |
dod'2 fad' |
mi'2. mi'4 |
re' re' dod' re' |
mi'2 la |
mi'2. mi'4 |
dod' r r2 |
R1 |
r2 r4 r8 la |
re'4. re8 fad4. re8 |
la2 r4 r8 la |
re'4. re'8 fad'4. re'8 |
la4 la r2 |
R1^\fermataMarkup |
r2 r8 dod' dod' dod' |
re'4 re'8 re' dod'4 fad'8 fad' |
fad'2 r8 re' re' re' |
re'2 re'4. re'8 |
dod'2 dod'4. dod'8 |
re'2 r4 r8 mi' |
fad'4. fad'8 fad'4. fad'8 |
sol'4 re' r2 |
R1 |
r4 mi'4 mi'2~ |
mi'4. mi'8 mi'4 mi' |
mi' mi' mi'2 |
mi'4. mi'8 mi'4 mi' |
mi'4 dod' r2 |
fad'2 fad'4 fad' |
re'2 re' |
dod'2. dod'4 |
re'4 r r fad' |
mi'4. fad'8 sol'4 sol' |
fad' re' r fad' |
mi'4. fad'8 sol'4 sol' |
fad'4 re' r2 |
R1 |
re'2 re'4 re' |
re'2 re' |
dod'2. dod'4 |
re'4 re' re' mi' |
re'2 re' |
re' dod' |
re'4 r r2 |
R1*7 |
