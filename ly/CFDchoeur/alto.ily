\clef "alto" la16 si32 dod' |
re'4.\fermata r32*1/2 la64 si dod' re' mi' fad' sold' la'4.\fermata r8 |
r8. re'16 re'8. re'16 re'2 |
r8. si16 si8. si16 si4. si'8 |
mi'4. mi'8 mi'4. mi'8 |
\sugNotes { la'8 la16. la32 la8 la la2\fermata } |
re'16 mi' fad' sol' la' si' dod'' re'' la8 la'16 la' la'8 la' |
re'16 mi' fad' sol' la' si' dod'' re'' la8 la'16 la' la'8 la' |
re'16 mi' fad' sol' la' si' dod'' re'' si dod' re' mi' fad' sold' lad' si' |
sol16 la si do' re' mi' fad' sol' re' mi' fad' sol' la' si' dod''! re'' |
la si dod' re' mi' fad' sold'? la' la si dod' re' mi' fad' sold' la' |
re'2 la' |
si' la' |
fad'8 fad'[ fad' fad'] mi' re' dod' mi' |
re'4 re'8. mi'16 fad'4.\prall mi'16 re' |
la'4 r r la' |
sold'4. la'8 si'4 si' |
la' r r la' |
sold'4. la'8 si'4 si' |
la' r r2 |
re'8 re'' re'' re'' \rt#4 re'' |
\rt#4 si' \rt#4 si' |
\rt#4 sold' \rt#4 sold' |
la16 <dod' mi'> q q \rt#4 q \rt#8 q |
\rt#8 q \rt#8 q |
\rt#8 q \rt#8 q |
<< fad'8 \\ lad >> dod' dod' dod' dod' fad' fad' fad' |
fad' fad'4 fad' re' re'8 |
\rt#4 la'8 \rt#4 re' |
\rt#4 mi' \rt#4 mi' |
\rt#4 fad' mi'8 mi' re' re' |
\rt#4 dod' \rt#4 re' |
\rt#4 mi' \rt#4 mi' |
la'2 mi' |
fad' mi' |
dod'8 dod'[ dod' dod'] dod'4. la8 |
fad4. re'8 fad'4.\trill mi'16 re' |
la'4. la8 dod'4.\prall si16 la |
re'4. re'8 fad'4.\prall mi'16 re' |
la'4 la8. la16 la4 la |
la1\mordent\fermata |
si16 dod' re' mi' fad' sold' lad' si' fad8 fad'16[ fad'] fad'8 fad' |
si16 dod' re' mi' fad' sold' lad' si' fad8 fad'16[ fad'] fad'8 fad' |
si16 dod' re' mi' fad' sold' lad' si' si dod' re' mi' fad' sold' lad' si' |
sol la si do' re' mi' fad' sol' re' mi' fad' sol' la' si' dod''! re'' |
la si dod' re' mi' fad' sold' la' la si dod' re' mi' fad' sold' la' |
re2 la |
re'4 r r2 |
sol2 re' |
sol4. sol8 mi4. mi8 |
la2 r4 la' |
sold'4. la'8 si'4 si' |
la' r r la' |
sold'4. la'8 si'4 si' |
la' mi' dod' la |
re'8 re' re' re' \rt#4 re' |
\rt#4 re' \rt#4 sol' |
\rt#4 la' \rt#4 la |
re'2 r4 re'' |
dod''4. re''8 mi''4 mi'' |
re'' r r re'' |
dod''4. re''8 mi''4 mi'' |
re'' r r2 |
la8 si16 dod' re' mi' fad' sold' la'8 la la la |
re' mi'16 fad' sol' la' si' dod'' re''8 la' fad' la' |
\rt#4 re'8 \rt#4 sol' |
\rt#4 la' \rt#4 la' |
\rt#4 si' si' si' dod'' dod'' |
re'' re' re' re' \rt#4 si' |
\rt#4 sol' la' la la la |
re'2 la' |
si' la' |
re'8 re'[ re' re'] dod' si la sol |
fad' mi' re' dod' si4 sib |
la4~ la16 si32 dod' re' mi' fad' sold' la'4 la |
re'4 r8 r16 re' fad'4.\prall mi'16 re' |
si'4.\prall la'16 sold' la'4 la |
re' r r2 |
