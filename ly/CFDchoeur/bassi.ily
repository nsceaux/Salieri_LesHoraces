\clef "bass" la,16 si,32 dod |
re4.\fermata r32*1/2 la,64 si, dod re mi fad sold la4.\fermata r8 |
r8. re16 re8. re16 re2 |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    r8. si,16 si,8. si,16 si,4. si,8 |
    mi4. sol8 sol4. sold8 |
    \sugNotes { la8 la,16. la,32 la,8 la, la,2\fermata } |
  }
  \tag #'basso {
    R1*2 |
    \sugNotes { r8 la,16. la,32 la,8 la, la,2\fermata } |
  }
>>
<<
  \new Voice {
    \tag #'basso { \voiceOne <>^"[Violoncelli]" }
    re16 mi fad sol la si dod' re'
  }
  \tag #'basso \new Voice {
    \voiceTwo <>_"[C.b.]" re8 fad la re'
  }
>> la,8 la16 la la8 la |
<<
  \new Voice {
    \tag #'basso { \voiceOne <>^"[Violoncelli]" }
    re16 mi fad sol la si dod' re'
  }
  \tag #'basso \new Voice {
    \voiceTwo <>_"[C.b.]" re8 fad la re'
  }
>> la,8 la16 la la8 la |
<<
  \new Voice {
    \tag #'basso { \voiceOne <>^"[Violoncelli]" }
    re16 mi fad sol la si dod' re' si, dod re mi fad sold lad si |
    sol, la, si, do re mi fad sol re mi fad sol la si dod'! re' |
    la, si, dod re mi fad sold? la la, si, dod re mi fad sold la |
  }
  \tag #'basso \new Voice {
    \voiceTwo <>_"[C.b.]"
    re8 fad la re' si, re fad si |
    sol,8 si, re sol re fad la re' |
    la, dod mi la la, dod mi la |
  }
>>
re4 r r2 |
R1 |
re2\ff la |
re'4 re8. mi16 fad4. mi16 re |
la4 r <<
  \tag #'(fagotto1 fagotto2 fagotti) {
    r4 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { dod'4 | si4. dod'8 re'4 re' |
        dod'2 s4 dod' | si4. dod'8 re'4 re' | dod'2
      }
      { la4 | sold4. la8 si4 si |
        la2 s4 la | sold4. la8 si4 si | la2 }
      { s4 | s1 | s2 r4 s | }
    >> r2 |
  }
  \tag #'basso {
    r2 |
    r2 r4 r8 mi16 fad32 sold |
    la4 mi dod la, |
    mi2 r4 r8 mi16 fad32 sold |
    la4 mi dod la, |
  }
>>
re8 re' re' re' \rt#4 re' |
\rt#4 si \rt#4 si |
\rt#4 sold \rt#4 sold |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { dod'4.. la16 la4.. mi'16 |
        mi'4.. dod'16 dod'4.. mi'16 |
        mi'2~ mi'4. mi'8 | re' }
      { la4.. mi16 mi4.. dod'16 |
        dod'4.. la16 la4.. dod'16 |
        dod'2~ dod'4. dod'8 | re' }
    >> fad8[ fad fad] \rt#4 fad |
  }
  \tag #'basso {
    la16\ff la, la, la, \rt#4 la, \rt#8 la, |
    \rt#8 la, \rt#8 la, |
    \rt#8 la, \rt#8 la, |
    lad,8 lad lad lad \rt#4 lad |
  }
>>
si,8 re16 fad si8 si si re' si sold |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    la2 re' | mi'1 | fad'4 fad' mi' re' |
    dod'2 re' | mi'1 |
  }
  \tag #'basso {
    \rt#4 la8 \rt#4 re | \rt#4 mi \rt#4 mi | \rt#4 fad mi mi re re |
    \rt#4 dod \rt#4 re | \rt#4 mi \rt#4 mi |
  }
>>
<<
  \new Voice {
    \tag #'basso { \voiceOne <>^"[Violoncelli]" }
    la2 mi' |
    fad' mi' |
  }
  \tag #'basso \new Voice {
    \voiceTwo <>_"[C.b.]"
    la,2 r | R1 |
  }
>>
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    la,2 dod' | re'4.
  }
  \tag #'basso { <>^"[Tutti]" la,2\ff mi | re4. }
>> re8 fad4.\prall mi16 re |
la4. la,8 dod4.\prall si,16 la, |
re4. re8 fad4.\prall mi16 re |
la4 la,8. la,16 la,4 la, |
la,1\mordent\fermata |
<<
  \new Voice {
    \tag #'basso { \voiceOne <>^"[Violoncelli]" }
    si,16 dod re mi fad sold lad si
  }
  \tag #'basso \new Voice {
    \voiceTwo <>_"[C.b.]"
    si,8 re fad si
  }
>> fad,8 fad16[ fad] fad8 fad |
<<
  \new Voice {
    \tag #'basso { \voiceOne <>^"[Violoncelli]" }
    si,16 dod re mi fad sold lad si
  }
  \tag #'basso \new Voice {
    \voiceTwo <>_"[C.b.]"
    si,8 re fad si
  }
>> fad,8 fad16[ fad] fad8 fad |
<<
  \new Voice {
    \tag #'basso { \voiceOne <>^"[Violoncelli]" }
    si,16 dod re mi fad sold lad si si, dod re mi fad sold lad si |
    sol, la, si, do re mi fad sol re mi fad sol la si dod'! re' |
    la, si, dod re mi fad sold la la, si, dod re mi fad sold la |
  }
  \tag #'basso \new Voice {
    \voiceTwo <>_"[C.b.]"
    si,8 re fad si si, re fad si |
    sol, si, re sol re fad la re' |
    la, dod mi la la, dod mi la |
  }
>>
re2\f la |
re' r |
sol,\f re |
sol4. sol8 mi4. mi8 |
la2 <<
  \tag #'(fagotto1 fagotto2 fagotti) {
    r4 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { dod'4 | si4. dod'8 re'4 re' | dod'2 s4 dod' |
        si4. dod'8 re'4 re' | dod'2 }
      { la4 | sold4. la8 si4 si | la2 s4 la |
        sold4. la8 si4 si | la2 }
      { s4 | s1 | s2 r4 s | }
    >> r2 |
    r4 re' la fad |
    re2 sol |
    la2. la,4 |
    re4 r r \clef "tenor" \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { fad'4 | mi'4. fad'8 sol'4 sol' |
        fad'2 s4 fad' | mi'4. fad'8 sol'4 sol' | fad'2 }
      { re'4 | dod'4. re'8 mi'4 mi' |
        re'2 s4 re' | dod'4. re'8 mi'4 mi' | re'2 }
      { s4 | s1 | s2 r4 s | s1 | }
    >> r2 |
    \clef "bass" la,8 si,16 dod re mi fad sold la8 la, la, la, |
  }
  \tag #'basso {
    r2 |
    r r4 r8 mi16 fad32 sold |
    la4 mi dod la, |
    mi,2 r4 r8 mi16 fad32 sold |
    la4 mi dod la, |
    re8 re re re \rt#4 re |
    \rt#4 re \rt#4 sol 
    \rt#4 la \rt#4 la, |
    re2 r |
    r r4 r8 la,16 si,32 dod |
    re4 fad la re' |
    la la r r8 la,16 si,32 dod |
    re4 fad la re' |
    la,2 r |
  }
>>
re8 mi16 fad sol la si dod' re'8 la fad la |
\rt#4 re8 \rt#4 sol |
\rt#4 la \rt#4 la |
\rt#4 si si8 si dod' dod' |
re' re re re \rt#4 si |
\rt#4 sol la la, la, la, |
re2 r |
R1 |
r8 re' re' re' dod' si la sol |
fad mi re dod si,4 sib, |
la,4~ la,16 si,32 dod re mi fad sold la4 la, |
re r8 r16 re fad4.\prall mi16 re |
si4.\prall la16 sold la4 la, |
re r r2 |
