\clef "bass" r8 |
R1^\fermataMarkup |
R1 |
r8. re16 re8. re16 re4 r |
R1 |
r8 la,16. la,32 la,8 la, la,2\fermata |
\rt#8 re16 la,4 r |
\rt#8 re16 la,4 r |
\rt#8 re16 \rt#8 re |
\rt#8 re \rt#8 re |
\rt#8 la, \rt#8 la, |
re4 r r2 |
R1 |
re4. re8 la,4. la,8 |
re4 re8. re16 re4. re16 re |
la,4 r r2 |
R1*4 |
re2 re4 re |
re re8. re16 re4 re |
re2 r |
\rt#8 la,16 \rt#8 la, |
\rt#16 la, |
\rt#16 la, |
re4 r r2 |
r4 re re re |
la,8 la,16 la, la,8 la, re re16 re re8 re |
re4 r r2 |
R1 |
la,4 r la, r |
R1*8 |
R1^\fermataMarkup |
re2\ff r |
re\ff r |
\rt#8 re16 \rt#8 re |
\rt#8 re16 \rt#8 re |
\rt#8 la, \rt#8 la, |
re4. re8 la,4. la,8 |
re4 r r2 |
R1*7 |
re8 re16. re32 re8 re \rt#4 re |
re4 re8. re16 re4 re |
la,8 la,16 la, la,8 la, \rt#4 la, |
re4 r r2 |
R1*5 |
re8 re16 re re8 re \rt#4 re |
re4. re8 re4. re8 |
la,4 la,8. la,16 la,4 la, |
re4. re8 re4 la, |
re8 re16 re re8 re \rt#4 re |
re8 re16 re re8 re la,4 la, |
re4 r r2 |
R1 |
r8 re re re la,4 r |
R1 |
r2 r4 la, |
re r r2 |
r2 r4 la, |
re r r2 |
