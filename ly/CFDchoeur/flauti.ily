\clef "treble" la'16 si'32 dod'' |
re''4.\fermata r16 la' la'4.\fermata r8 |
R1*3 |
R1^\fermataMarkup |
r2 r8 dod''16 dod'' dod''8 dod'' |
re'' la' fad' re' r8 mi''16 mi'' mi''8 mi'' |
fad'' re'' la' fad' r fad''16 fad'' fad''8 fad'' |
sol''2 fad'' |
mi''1 |
re''8 re'''[ re''' re'''] dod'''16 re''' si'' dod''' la'' si'' sol'' la'' |
fad''8 fad''-. mi''-. re''-. dod''16 re'' mi'' re'' dod'' la' si' dod'' |
re''8 re''[ re'' re''] dod'' si' la' sol' |
fad'4 r r2 |
r r4 \twoVoices #'(flauto1 flauto2 flauti) <<
  { dod'''4 | si''4. dod'''8 re'''4 re''' |
    dod'''2 s4 dod''' | si''4. dod'''8 re'''4 re''' |
    dod'''2 s | fad'''1 | fad''' | }
  { la''4 | sold''4. la''8 si''4 si'' |
    la''2 s4 la'' | sold''4. la''8 si''4 si'' |
    la''2 s | la''1 | re''' | }
  { s4 | s1 | s2 r4 s | s1 | s2 r | }
>>
mi'''8 si''[ si'' si''] si''16 la'' sold'' fad'' mi'' re'' dod'' si' |
dod''2~ dod''4. mi''8 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { mi''1 | mi''2~ mi''4. sol''8 | fad''8 }
  { dod''1 | dod''2~ dod''4. mi''8 | fad'' }
>> fad''[ fad'' fad''] fad''16 sol'' mi'' fad'' re'' mi'' dod'' mi'' |
re''4 fad''2 fad''4 |
mi''16 la''( sold'' la'' si'' la'' sold'' la'') fad''-. la''( sold'' la'' si'' la'' sold'' la'') |
sold'4 si''2 dod'''4 |
re''' re''' mi''' fad''' |
mi''16 la''( sold'' la'' si'' la'' sold'' la'') fad''-. la''( sold'' la'' si'' la'' sold'' la'') |
si'4 \twoVoices #'(flauto1 flauto2 flauti) <<
  { sold''2 sold''4 | }
  { si'2 si'4 | }
>>
la'4 r r2 |
la''2 mi''' |
dod'''8 la''[ la'' la''] sol''!16 fad'' mi'' re'' dod'' la' si' dod'' |
re''4 r r2 |
R1*3 |
R1^\fermataMarkup |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { s2 s8 lad''16 lad'' lad''8 lad'' |
    si'' s4. s8 dod'''16 dod''' dod'''8 dod''' |
    re''' s4. s8 fad''16 fad'' fad''8 fad'' |
    sol''2 fad'' | mi''1 | }
  { s2 s8 dod''16 dod'' dod''8 dod'' |
    re'' s4. s8 lad''16 lad'' lad''8 lad'' |
    re''' s4. s8 re''16 re'' re''8 re'' |
    si'2 la' | dod''1 | }
  { r2 r8 s4. | s8 fad''[ re'' si'] r8 s4. | s8 si'' fad'' re'' r8 s4. | }
>>
r8 re'''[ re''' re'''] dod'''16 re''' si'' dod''' la'' si'' sol'' la'' |
fad''4 r r2 |
r8 sol'' sol'' sol'' fad''16 sol'' mi'' fad'' re'' mi'' do'' re'' |
si'4 r r2 |
r r4 \twoVoices #'(flauto1 flauto2 flauti) <<
  { dod'''4 | si''4. dod'''8 re'''4 re''' |
    dod'''2 s4 dod''' | si''4. dod'''8 re'''4 re''' |
    dod'''2 s | fad'''1 | fad'''2 sol''' | mi'''1 |
    re'''2 s4 fad'' | mi''4. fad''8 sol''4 sol'' |
    fad''2 s4 fad'' | mi''4. fad''8 sol''4 sol'' | fad''2 }
  { la''4 | sold''4. la''8 si''4 si'' |
    la''2 s4 la'' | sold''4. la''8 si''4 si'' |
    la''2 s | la''1 | la''2 si'' | dod'''1 |
    re'''2 s4 re'' | dod''4. re''8 mi''4 mi'' |
    re''2 s4 re'' | dod''4. re''8 mi''4 mi'' | re''2 }
  { s4 | s1 | s2 r4 s | s1 | s2 r | s1*3 | s2 r4 s | s1 | s2 r4 s | }
>> r2 |
R1 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { fad''1 | fad''2 sol'' | mi''2. fad''4 | sol''4 sol''2 sol''4 |
    fad''1 | mi'' | }
  { la'1 | la'2 si' | dod''2. dod''4 | re''2. mi''4 |
    re''1~ | re''2 dod'' | }
>>
re''8 re'''[ re''' re'''] dod'''16 re''' si'' dod''' la'' si'' sol'' la'' |
fad''8 fad''[-. mi''-. re''-.] dod''16 re'' mi'' re'' dod'' la' si' dod'' |
re''4 r r2 |
R1 |
r2 r4 \twoVoices #'(flauto1 flauto2 flauti) <<
  { mi''4 | fad'' }
  { la' | la' }
>> r4 r2 |
r r4 la' |
re' r r2 |
