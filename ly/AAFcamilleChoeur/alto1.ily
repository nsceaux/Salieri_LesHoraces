\clef "alto" <>^\markup\whiteout\italic "[con sordini]" r8 |
do'4. re'8( mi' fa') |
mi' re' re'2 |
do'~ do'8 sib |
sib8.( la16) la4 r |
si2. |
do'2( sol'8 mi' |
re' mi'16 fa' do'4. si8) |
sol'2( mi'8 dod') |
dod'16-\sug\p( re' mi' fa') do'4. si8 |
do'8.-\sug\sf do16 do4 r |
do'4.-\sug\p do'8( do' mib') |
mib'8 re' re'2 |
re' mib'8 do' |
do'8. sib!16 sib4 r |
sib2-\sug\fp~ sib8. sib16 |
sib4( la8) la-\sug\rinf([ sib do']) |
re' re'4 fa'8([ re' sib]) |
la2 sol4 |
fa8[ la] la([ sib!]) sib([ do']) |
do'2 do'8( mib') |
mib' re' re'2 |
re' mib'8( do') |
do'4( sib) r |
do'4.\f( re'8 mi' re') |
do'2. |
r8 fa'4-\sug\p fa'8( mi'16 re' do' sib) |
la2 sol8 do' |
\grace sib8 la2 r8 do'-\sug\f |
mi' mi'4( fa'8 sol' fa') |
fa'4( mi'2) |
fa'8 fa'4 fa'8 mi'16 re' do' sib |
la2 sol4 |
fa2 r4 |
