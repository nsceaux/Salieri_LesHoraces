\piecePartSpecs
#`((clarinetto1 #:notes "fagotto1" #:clef "tenor")
   (clarinetto2 #:notes "fagotto2" #:clef "tenor")
   (fagotto1 #:notes "fagotto1" #:clef "tenor")
   (fagotto2 #:notes "fagotto2" #:clef "tenor")
   (violino1 #:notes "violino1")
   (violino2 #:notes "violino2")
   (alto #:score "score-viola")
   (basso #:tag-notes bassi
          #:instrument , #{\markup\center-column { Violoncelli C.b. }#})
   (silence #:on-the-fly-markup , #{ \markup\tacet#33 #}))
