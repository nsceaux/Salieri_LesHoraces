\clef "soprano/treble" r8 |
R2.*18 |
r4 r r8 do'' |
do''4. do''8 do'' mib'' |
mib''([ re'']) re''2 |
re''4 re''8 re'' mib'' do'' |
\grace do''4 sib'2 r4 |
R2.*4 |
r4 r r8 do'' |
mi''4. fa''8 sol'' fa'' |
\grace fa''8 mi''2 r8 do''8 |
fa''4. fa''8 mi''16[ re''] do''[ sib'] |
la'2( sol'4) |
fa'2 r4 |

