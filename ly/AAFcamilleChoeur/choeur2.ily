\clef "soprano/treble" r8 |
R2.*18 |
r4 r r8 la' |
la'4. la'8 la' la' |
la'4 la'2 |
fad'4 fad'8 fad' la' la' |
\grace la'4 sol'2 r4 |
R2.*4 |
r4 r r8 la' |
sib'4. sib'8 sib' sib' |
sib'4 r r8 sib' |
la'4. la'8\grace do''8 sib' la'16[ sol'] |
fa'2( mi'4) |
fa'2 r4 |
