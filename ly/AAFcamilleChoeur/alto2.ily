\clef "alto" <>^\markup\whiteout\italic "[con sordini]" r8 |
la4. sib8 sol la |
sib4 sib2 |
sol8 sol4 mi sol8 |
sol8.( fa16) fa4 r |
r8 fa\sf( sol lab sol fa) |
mi2 sol4 |
la mi4. re8 |
sol8\sf( la) sib!2 |
la4-\sug\p mi4. re8 |
mi8.-\sug\sf do16 do4 r |
do'8-\sug\p( mib' do' sib! la sol) |
fad4 fad8( la fad la) |
fad( la re fad) la[ la] |
la8. sol16 sol4 r |
sol2-\sug\fp~ sol8. sol16 |
sol4( fa8) fa-\sug\rinf([ sol la]) |
sib8 sib4 re'8( sib sol) |
fa2 mi4 |
fa8[ fa] fa([ sol]) sol([ la]) |
la2 la4~ |
la8 la fad[ la fad la] |
fad( la re fad la[ la]) |
la4 sol r |
R2. |
r4 r8 \footnoteHere #'(0 . 0) \markup\wordwrap {
  Source : \score {
    { \tinyQuote \clef "alto" \key fa \major \time 3/4
      r4 r8 sib( mi' re') |
      do'8 do'4 do'8 \grace mi'16 sib8 la16 sol | }
    \layout { \quoteLayout }
  }
}
sol8( do' sib) |
la8 la4-\sug\p la8 \grace do'16 sib8 la16 sol |
fa2 mi4 |
\grace sol8 la2 r8 do'-\sug\f |
\once\tieDashed sib2.~ |
sib4. sol8( do' sib) |
la8 la4 la8 \grace do'8 sib8 la16 sol |
fa2 mi4 |
fa2 r4 |
