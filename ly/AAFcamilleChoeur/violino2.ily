\clef "treble" <>^\markup\whiteout\italic "con sordini" r8 |
la'4. sib'8( sol' la') |
sib'4 sib'2 |
sol'8 sol'4 mi' sol'8 |
sol'8.( fa'16) fa'4 r |
r8 fa'(\sf sol' lab' sol' fa') |
mi'( do' mi' sol') sol'[ sol'] |
la'4 mi'4. re'8 |
sol'8\sf( la') sib'2 |
la'4-\sug\p mi'4. re'8 |
mi'8.-\sug\sf do'16 do'4 r |
do''8-\sug\p( mib'' do'' sib' la' sol') |
fad'4 fad'8( la' fad' la') |
fad'( la' re' fad') la'8[ la'] |
re'4. re'8 re' re' |
sib'2-\sug\fp~ sib'8. sib'16 |
sib'4( la'8) la'-\sug\rinf([ sib' do'']) |
sib' sib'4 re''8( sib' sol') |
fa'2 mi'4 |
fa'8[ fa'] fa'([ sol']) sol'([ la']) |
la'2 la'4~ |
la'8 la' fad'([ la' fad' la']) |
fad'([ la' re' fad'] la'[ la']) |
re'2 r4 |
sib'2.-\sug\f~ |
sib'4. sol'8( do'' sib') |
la'8 la'4-\sug\p la'8 \grace do''16 sib'8 la'16 sol' |
fa'2 mi'4 |
\grace mi'8 fa'2 r8 do''8-\sug\f |
sib'2.~ |
sib'4. sol'8( do'' sib') |
la' la'4 la'8 \grace do''8 sib' la'16 sol' |
fa'2 mi'4 |
fa'2 r4 |
