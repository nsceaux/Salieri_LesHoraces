\clef "soprano/treble" do''8 |
do''4. re''8 mi'' fa'' |
\grace mi''4 re'' re'' fa''8 re'' |
do''4 do''4. sib'8 |
la'2 r8 do'' |
si'4. si'8 si' si' |
do''4 do'' sol''8 mi'' |
re''[ mi''16 fa''] do''4. si'8 |
sol''2 \grace fa''16 mi''8 re''16[ dod''] |
dod''[ re'' mi'' fa''] do''4. si'8 |
do''2 r8 do'' |
do''4. do''8 do'' mib'' |
re''4 re''2 |
re''4 re''8 re'' mib'' do'' |
sib'2 r8 sib' |
mi''!4. fa''8 sol''8. sib'16 |
\grace sib'4 la'2 r8 do'' |
re''4. fa''8 \grace mi''4 re''8 do''16[ sib'] |
la'2( sol'4) |
fa'2 r4 |
R2.*3 |
r4 r r8 sib' |
mi''4. fa''8 sol'' fa'' |
\grace fa''16 mi''2 r8 do'' |
fa''4. fa''8 mi''16[ re''] do''[ sib'] |
la'2( sol'8)[ do''] |
\grace sib'8 la'2 r4 |
R2.*5 |
