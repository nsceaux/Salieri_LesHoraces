\clef "bass" r8 |
fa4 fa fa |
fa fa fa |
mi do mi |
fa4. fa8 fa, fa |
re2. |
do2 mi4 |
fa sol sol, |
mi2.\f |
fa4\p sol sol, |
do8.\f do16 do4 r |
r r do\p |
re4 re8( fad re fad) |
re2 fad4 |
sol4. sol8 sib sol |
do8.\fp do16 do2 |
fa4 fa,8 fa([\rinf sol la]) |
sib4 sib, sib, |
\rt#6 do8 |
fa,4 r r |
\tag #'bassi <>^\markup\right-align [Vc.] _\markup\right-align [Cb.]
r8 <<
  \tag #'(cello bassi) \new Voice {
    \tag #'bassi \voiceOne
    do'8( mib' do' sib do') | la re' re' re' re' re' |
  }
  \tag #'(cb bassi) \new Voice {
    \tag #'bassi \voiceTwo
    fa8 fa fa fa fa | fad fad re fad re fad |
  }
>>
\tag #'bassi <>^"[Tutti]"
re2 fad4 |
sol sol,
\tag #'bassi <>^\markup [Vc.] _\markup [Cb.] r |
<<
  \tag #'(cello bassi) \new Voice {
    \tag #'bassi \voiceOne
    do'4.-\sug\f re'8 mi' re' | do'2 do'4
  }
  \tag #'(cb bassi) \new Voice {
    \tag #'bassi \voiceTwo R2.*2
  }
>>
\tag #'bassi <>^"[Tutti]"
fa4\p fa sib, |
\rt#6 do8 |
fa4 fa, r |
do\f do8( re mi re) |
do2. |
fa,8 fa fa fa sib,[ sib,] |
\rt#6 do8 |
fa2 r4 |
