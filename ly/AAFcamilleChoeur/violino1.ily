\clef "treble" <>^\markup\whiteout\italic "con sordini" r8 |
do''4. re''8( mi'' fa'') |
mi'' re'' re''2 |
do''~ do''8 sib' |
sib'8.( la'16) la'4 r |
si'2. |
do''2( sol''8 mi'' |
re'' mi''16 fa'' do''4. si'8) |
sol''2( mi''8 dod'') |
dod''16\p( re'' mi'' fa'') do''4. si'8 |
do''8.\sf do'16 do'4 r |
do''4.\p do''8( do'' mib'') |
mib'' re'' re''2 |
re'' mib''8 do'' |
do''( sib'!) sib'[ sib' sib' sib'] |
mi''!2\fp~ mi''8. mi''16 |
mi''4( fa''8) la''([\rinf mi'' fa'']) |
re''8 re''4 fa''8( re'' sib') |
la'2 sol'4 |
fa'8\f([ la']) la'([ sib']) sib'([ do'']) |
do''2 do''8( mib'') |
mib''( re'') re''2 |
re'' mib''8( do'') |
do''4( sib') r |
mi''!4.-\sug\f( fa''8 sol'' fa'') |
fa''4 mi''2 |
r8 fa''4\p la''16 fa'' mi'' re'' do'' sib' |
la'2 sol'8 do'' |
\grace sib'8 la'2 r8 do''\f |
mi'' mi''4( fa''8 sol'' fa'') |
fa''4( mi''2) |
r8 fa''4 la''16 fa'' mi'' re'' do'' sib' |
la'2 sol'4 |
fa'2 r4 |
