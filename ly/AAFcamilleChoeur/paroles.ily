\tag #'camille {
  Dé -- es -- se se -- cou -- ra -- ble,
  Je t’in -- voque en trem -- blant ;
  Du dou -- te qui m’ac -- ca -- ble
  Fais ces -- ser le tour -- ment
  Fais ces -- ser __ le tour -- ment.
  Faut- il que je re -- non -- ce
  À la plus tendre ar -- deur ?
  Hé -- las ! tout mon bon -- heur
  Dé -- pend de ta ré -- pon -- se.
}
\tag #'choeur {
  Faut- il qu’el -- le re -- non -- ce
  À la plus tendre ar -- deur ?
}
\tag #'camille {
  Hé -- las ! tout mon bon -- heur
  Dé -- pend de ta ré -- pon -- se.
}
\tag #'choeur {
  Hé -- las ! tout son bon -- heur
  Dé -- pend de ta ré -- pon -- se.
}
