\clef "treble" \transposition fa
R1*21 |
<>-\sug\f <<
  \tag #'(corno1 corni) \new Voice {
    \tag #'corni \voiceOne do''4. sol'8
  }
  \tag #'(corno2 corni) \new Voice {
    \tag #'corni \voiceTwo mi'4. sol'8
  }
>> mi'4 do' |
<<
  \tag #'(corno1 corni) \new Voice {
    \tag #'corni \voiceOne re''1 |
  }
  \tag #'(corno2 corni) \new Voice {
    \tag #'corni \voiceTwo sol'1 |
  }
>>
do''1-\sug\fp |
sol'2. r4 |
re''1-\sug\f |
do''2. sol'4 |
re''1-\sug\p |
do''4.-\sug\fp do''8 do''4. do''8 |
<<
  \tag #'(corno1 corni) \new Voice {
    \tag #'corni \voiceOne re''4. re''8 re''4. re''8 |
  }
  \tag #'(corno2 corni) \new Voice {
    \tag #'corni \voiceTwo do''4. do''8 sol'4. sol'8 |
  }
>>
do''1\ff |
<<
  \tag #'(corno1 corni) \new Voice {
    \tag #'corni \voiceOne re''1 | mi'' | re''1 |
  }
  \tag #'(corno2 corni) \new Voice {
    \tag #'corni \voiceTwo sol'1 | do'' | do''2 sol' |
  }
>>
r4 sol'2-\sug\p sol'4 |
sol'1~ |
sol'~ |
sol'~ |
sol'~ |
sol'~ |
sol'~ |
sol'-\sug\p |
<<
  \tag #'(corno1 corni) \new Voice {
    \tag #'corni \voiceOne do''2. mi''4 |
  }
  \tag #'(corno2 corni) \new Voice {
    \tag #'corni \voiceTwo do'2. mi''4 |
  }
>>
re''1-\sug\cresc~ |
re''~ |
re''~ |
<< re''1 { s2. <>\! } >> |
R1*11 |
r4 <>-\sug\f <<
  \tag #'(corno1 corni) \new Voice {
    \tag #'corni \voiceOne re''8 re'' re''4 re'' | do''2
  }
  \tag #'(corno2 corni) \new Voice {
    \tag #'corni \voiceTwo sol'8 sol' sol'4 sol' | sol'2
  }
>> r2 |
r4 sol'8. sol'16 sol'4 sol' |
mi''1 |
mi'' |
re''2 r\fermata |
r <>\p <<
  \tag #'(corno1 corni) \new Voice {
    \tag #'corni \voiceOne re''2 | re'' re'' | mi''
  }
  \tag #'(corno2 corni) \new Voice {
    \tag #'corni \voiceTwo sol'2 | sol' sol' | do''2
  }
>> r4 do'' |
r2 r4 <>-\sug\f <<
  \tag #'(corno1 corni) \new Voice {
    \tag #'corni \voiceOne re''4 | mi'' mi''2 mi''4 |
  }
  \tag #'(corno2 corni) \new Voice {
    \tag #'corni \voiceTwo sol' | do'' do''2 do''4 |
  }
>>
do''1~ |
do'' |
<<
  \tag #'(corno1 corni) \new Voice {
    \tag #'corni \voiceOne re''1 | re'' | mi'' | re'' | mi''2
  }
  \tag #'(corno2 corni) \new Voice {
    \tag #'corni \voiceTwo sol'1 | sol' | do'' | sol' | do''2
  }
>> do''4. do''8 |
do''2 r4 sol' |
do''1 |
do''2 sol'4 sol' |
<<
  \tag #'(corno1 corni) \new Voice {
    \tag #'corni \voiceOne do''2
  }
  \tag #'(corno2 corni) \new Voice {
    \tag #'corni \voiceTwo mi'2
  }
>> r2 |
R1*41 |