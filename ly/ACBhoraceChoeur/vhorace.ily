\clef "bass" R1*3 |
r4 r8 sol sol sol sol sol |
do' do' r do'16 do' do'4 do'8 do' |
sol4 sol8 sol mi4 fa8 sol |
do8^! do r4 r2 |
R1*2 |
r4 r8 la16 la la8 la16 la sol8 la |
fa4^! r r2 |
r la8 la16 la la8 la |
re'4 r8 la16 la la4 la8 si |
sold8^! sold r4 r r8 mi16 fad |
sold8^! sold r4 r8 sold16 la si8 si |
R1 |
r4 r8 mi16 mi si4 si8 do' |
la8^! la r4 r2 |
r4 r8 la do'4. do'8 |
la4 r8 do' sib2^! |
r8 sol sol la sib4. do'8 |
la2 r |
do' r |
si4 si8 do' re'4 fa8 sol |
mi4 mi r2 |
R1 |
r2 r4 do' |
reb'4. do'8 sol4 mi |
fa2 lab4. lab8 |
sol2 do'4. do'8 |
si2. r8 sol |
sib!4. sol8 do4 do' |
la2 re'4. re'8 |
sol2 do'4. do'8 |
fa2 r |
R1 |
r2 r4 fa |
sol4. mi8 do4 sol |
la2 r |
R1 |
r2 r4 fa |
sol4. la8 sib4. do'8 |
la4 la r do' |
si4. re'8 si4 sol |
do'2. do'8 mi' |
re'2 sol4. fa8 |
mi2 r |
R1 |
r2 r4 sol |
do'4 mib'2 re'8 re' |
re'2( do'4.) do'8 |
mib'4. do'8 do'4. fad8 |
sol4 sol r2\fermata |
R1 |
r2 sol4. sol8 |
la2 si4. si8 |
do'2 la4. la8 |
fa2 sol4. sol8 |
do2 r |
r fa4 la |
do'2 do'4. do'8 |
re1 |
re4 fad8 fad la4 do' |
sib1\fermata |
<>^\markup\italic\bold { un poco lento }
sib2 sib |
sib sib4 sib |
la2 r4 do' |
re'4. sib8 sol4 do' |
la fa r2 |
R1*13 |
r2 la8 la16 la la8. la16 |
re'4 r8 la la la sol la |
fad fad r la do'8. do'16 si!8 do' |
la la r la16 si do'4 do'8 re' |
si4^! r r2 |
R1 |
r4 r8 fad16 fad fad4 fad8 fad |
si4 fad8 sol la4 la8 si |
sol^! sol r si sol sol fad mi |
lad^! lad16 dod' lad8 lad16 si fad4^! r |
r2 r4 r8 sol |
si4 r8 re'16 re' re'4 si8 si |
sol sol r sol16 sol sol4 sol8 la |
si4^! si8 re' si4 si8 do' |
sol^! sol^! r4 r2 |
R1*2 |
r4 do'4^! do'8 do' sib do' |
la4^! r8 do' la4 r |
la8 la16 la sib8 do' fa8 fa r fa |
fa fa fa sol la r16 la la sib do' re' |
sib4^! r4 r2 |
sib4. fa8 lab lab16 lab lab8 sib |
sol8^! sol r sib16 sib sib4 sib8 sib |
sol4 r8 sol16 sol sol4 lab8 sib |
mib4 r16 sib reb' sib sol4^! r8 sol |
sol sol sol lab mib4^! r4 |
r2 r4 r8 do'16 do' |
sol4.^! sol8 sol sol sib la! |
fa8^! fa r la16 la la4 la8 sib |
sol16^! sol r8 r sol dod' dod' dod' dod'16 la |
re'8^! re' r la16 la re'4 la8 si |
sold4^! r8 sold sold sold la si |
mi4 r8 mi si si r4 |
si8 si si do' la4^! r4 |
r8 la si do' si8.^! si16 la8 si |
sol4^! r8 si16 si sol4 la8 si |
mi mi r sol16 mi lad4^! lad8 si |
fad8 fad^! r4 r2 |
