\tag #'vhorace {
  Dé -- jà les deux ar -- mé -- es
  D’une é -- ga -- le fu -- reur pa -- rois -- soient a -- ni -- mé -- es ;
  On al -- loit en ve -- nir aux mains…
  En -- tre les deux par -- tis sou -- dain Tul -- le s’a -- van -- ce :
  On s’ar -- rê -- te, on l’en -- tou -- re, on l’é -- coute en si -- len -- ce :
  Al -- bains, dit- il, & vous, é -- cou -- tez- moi Ro -- mains !

  Dieux ! quel -- les fu -- reurs sont les nô -- tres.
  Je vois à notre as -- pect la na -- tu -- re fré -- mir !
  Je vois à notre as -- pect la na -- tu -- re fré -- mir !
  Vos fils sont nos ne -- veux, nos fil -- les sont les vô -- tres,
  Le sang de mil -- le nœuds a vou -- lu nous u -- nir.
  D’un sang si pré -- ci -- eux __ pour -- quoi souil -- ler la ter -- re ?
  Qu’en -- tre vous, qu’en -- tre nous trois guer -- riers soient choi -- sis !
  Et que notre in -- té -- rêt, en -- tre leurs mains re -- mis,
  Fasse un sim -- ple com -- bat d’une ef -- froy -- a -- ble guer -- re.
}
\tag #'choeur {
  O roi, le mo -- dè -- le des rois !
  Oui, les Dieux, les Dieux t’ins -- pi -- roient,
  ils par -- loient par ta voix,
  ils par -- loient par ta voix !
}
\tag #'vhorace {
  Vous eus -- siez vu sou -- dain dans l’une & l’autre ar -- mé -- e
  La joie & la con -- cor -- de en -- flam -- mer tous les cœurs.
  D’un é -- gal in -- té -- rêt l’une & l’autre a -- ni -- mé -- e
  Ne son -- ge plus qu’au choix de ses trois dé -- fen -- seurs.
  Ro -- mains, quel -- le gloire à pré -- ten -- dre !
  Trop heu -- reux les hé -- ros qui sau -- ront nous dé -- fen -- dre !
}
\tag #'jhorace {
  Que je leur porte en -- vi -- e !
}
\tag #'vhorace {
  O noble & cher trans -- port !
  Les Dieux veil -- lent sur ma pa -- tri -- e,
  Et dans des di -- gnes mains ils re -- met -- tront son sort.
  
  Toi dont Al -- be se glo -- ri -- fi -- e,
  Par tes hau -- tes ver -- tus di -- gnes d’ê -- tre Ro -- main
  De -- viens mon fils en re -- ce -- vant sa main.
}
\tag #'(camille curiace) {
  Mon pè -- re !
}
\tag #'vhorace {
  Tu le peux, sans ha -- sar -- der ma gloi -- re
  Soit que Ro -- me tri -- om -- phe ou qu’Al -- be ait la vic -- toi -- re
  Le vain -- cu du vain -- queur re -- con -- noî -- tra les loix,
  Sans hon -- te, sans tri -- buts ser -- vils ;
  Et nos é -- tats, u -- nis par choix,
  Ne se -- ront qu’un em -- pi -- re & qu’un peuple en deux vil -- les.
}
