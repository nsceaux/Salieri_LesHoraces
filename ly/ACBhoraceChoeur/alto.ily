\clef "alto" do'16 si do' si do' si do' si do' si do' si do' si do' si |
do' re' mi' fa' sol' fa' mi' re' do' re' mi' fa' sol' fa' mi' re' |
do'8 re'16 mi' fa' sol' la' si' do''8 sol' sol' sol' |
mi'4 r r2 |
R1*2 |
do'16 si do' si do' si do' si do' si do' si do' si do' si |
do' re' mi' fa' sol' fa' mi' re' do' re' mi' fa' sol' fa' mi' re' |
do'8 do'16 re' mi' fa' sol' la' sib'8 sol' mi' sol' |
dod'2~ dod' |
re8 mi16 fa sol la si dod' re'8 mi'16 fa' sol' la' si' dod'' |
re'' la' la' la' \rt#4 la' <>-\sug\p \rt#4 la' \rt#4 la' |
\rt#8 la' \rt#8 la' |
mi8\f fad16 sold la si dod' red' mi'4 r |
r8. << { re'16 re'4 } \\ { si16 si4 } >> r2 |
<< { \rt#8 re'16 \rt#8 re' | sold'1 | la'4. } \\
  { \rt#8 si16 \rt#8 si | si1 | la4. }
>> la8-\sug\f la4 la |
la1~ |
la2-\sug\p sib~ |
sib1 |
la4.-\sug\f do'8 la4 fa |
do'4. sol'8 sol'4. fa'32 mi' re' do' |
si1\fp |
do'2. r4 |
sol8-\sug\f sol4 sol sol sol8 |
fa fa4 fa do' do'8 |
sol-\sug\p sol4 sol sol sol8 |
do'4.-\sug\fp do'8 do'4. do'8 |
reb'4. reb'8 do'4. do'8 |
re'!1-\sug\fp |
mi'-\sug\ff |
\rt#16 fa'32 \rt#16 re' |
\rt#16 re' \rt#16 do' |
fa8 la-\sug\p la la \rt#4 la |
<>-\sug\f \rt#4 sib \rt#4 sib |
\rt#4 la \rt#4 la |
<>-\sug\p \rt#4 sol \rt#4 sol |
fa la la la \rt#4 la |
\rt#4 sib \rt#4 sib |
\rt#4 la \rt#4 la |
<>-\sug\p \rt#4 sol \rt#4 sol |
fa8 fa4 fa fa' fa'8 |
re'8-\sug\cresc si4 si si re'8 |
do' do'4 do' do' do'8 |
sol re'4 re' re' re'8 |
do' mi'4 mi' mi' mi'8\! |
mib'4-\sug\f mib'2 re'4 |
re'4. si8 do'4 re' |
mib' mib'2 re'4-\sug\p |
re'4. si8 do'2 |
lab8 lab4 lab lab lab8 |
sol2 r |
do'4\ff do'8 do' mi'4 mi' |
sol'2 sol'4.\p sol'8 |
fad'2 fa' |
mi' la'4. la'8 |
fa'4.\prall mi'16 fa' sol'4 sol |
do'4 do'8\f do' do'4 sib! |
la2 r |
r4 la la sol |
fad fad8-\sug\f fad fad4 fad |
fad1 |
sol2 r\fermata |
r2 sol-\sug\p |
sol' do'4 do' |
do'2 r4 la |
sib2-\sug\f do'4 do |
fa8 do'[-\sug\ff do' do'] \rt#4 do' |
\rt#4 re' \rt#4 re' |
\rt#4 re' \rt#4 re' |
\rt#4 sol' \rt#4 sol' |
\rt#4 <sol' sib'> \rt#4 q |
<< { \rt#4 la' \rt#4 la' | \rt#4 sib' \rt#4 sib' |
    \rt#4 la' \rt#4 la' | } \\
  { \rt#4 fa' \rt#4 fa' | \rt#4 sol' \rt#4 sol' |
    \rt#4 fa' \rt#4 fa' | }
>>
\rt#4 sib \rt#4 do' |
\rt#4 fa'\rt#4 la |
\rt#4 sib \rt#4 do' |
fa4 fa'16 sol' la' sib' do''4 re'' |
mib''8. re''16 do''8. sib'16 la'8. do''16 la'8. sol'16 |
fad'4 re'8. re'16 re'4 re' |
%%%
<<
  \tag #'alto { <fad' la'>1~ | }
  \tag #'alto-conducteur { <fad' la'>2~ q~ | }
>>
<fad' la'>1~ |
q~ |
q |
r8. sol16 la8. si!16 do'8. re'16 mi'8. fad'16 |
sol'4 si'8. sol'16 fad'8. sol'16 fad'8. mi'16 |
red'1~ |
red' |
mi' |
mi'4 r r fad' |
sol'4. sol8 sol2 |
R1*2 |
fa'4 r r2 |
sol8 <sol sol'> q q \rt#4 q |
\rt#4 q \rt#4 <sol re'> |
<< mi'4 \\ sol >> r r2 |
r4 mi'2.\f |
fa'4 r r2 |
R1*2 |
r8. sib16 do'8. re'16 mib'8. fa'16 sol'8. la'16 |
sib'4 r r2 |
mib'4 r r2 |
R1 |
r2 reb'4 r |
r2 r4 mib' |
lab2~ lab4 r |
mi'!4 r r2 |
r8. fa'16 fa'4 r2 |
r8. mi'16 mi'4 r2 |
r8. re'16 re'4 r2 |
r8. re'16 re'4 r2 |
R1 |
r2 r8. do'16 do'8. do'16 |
do'4 r red' r |
r8. mi'16 mi'4 r2 |
r mi'4 r |
r fad'4 si2 |

