\clef "treble" do'1 |
do'8 mi' sol' mi' do' mi' sol' mi' |
do'2 r8 sol'16 sol' sol'8 sol' |
mi'4 r r2 |
R1*2 |
do'1~ |
do'8 mi' sol' mi' do' mi' sol' mi' |
do'2 r8 sol'16 sol' mi'8 sol' |
mi''2 r |
re''4. re''8 re''4. re''8 |
re''2 r |
R1 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''2~ mi''4 s4 |
    s8. mi''16 mi''4 s2 |
    s8 mi''16 mi'' mi''8 mi'' \rt#4 mi'' |
    mi''2 s |
    s4. mi'8 mi'4 mi' |
    mi'2 s | }
  { mi'2~ mi'4 s |
    s8. mi'16 mi'4 s2 |
    s8 mi'16 mi' mi'8 mi' \rt#4 mi' |
    mi'2 s |
    s4. do'8 do'4 do' |
    do'2 s | }
  { s2.-\sug\f r4 | r8. s16 s4 r2 | r8 s s2. |
    s2 r | r4 r8 s-\sug\f s2 | s r | }
>>
R1*34 |
do'4-\sug\ff do'8 do' mi'4 mi' |
sol'2 r |
R1 |
r2 <>\p \twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''4. mi''8 | re''2 re''4. re''8 | do''4 }
  { do''4. do''8 | do''2 sol'4. sol'8 | mi'4 }
>> r4 r2 |
R1*8 |
r2 r4 do''-\sug\f |
do'' do''2 do''4 |
re''1~ |
re'' |
do''~ |
do'' |
do'' |
do'' |
do''2 do''4. do''8 |
re''2 do''4 do'' |
do''2 do''4 do'' |
re''2 do''4 do'' |
do''4 r r2 |
R1 |
r4 re''8. re''16 re''4 re'' |
re'' r r2 |
R1*38 |
