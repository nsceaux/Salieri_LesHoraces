\clef "vtaille" R1*68 |
r4 do'2. |
fa'2 fa'4. fa'8 |
fa'2 fa'4 fa' |
sol'1 |
mi'2. sol'4 |
fa'2. fa'4 |
mi'2 mi'4. mi'8 |
fa'2 do'4 fa' |
fa'2 do'4 do' |
do'2 fa'4 fa' |
fa'2 mi'4 mi' |
fa'2 r |
R1*41 |
