\midiTempo#120
\tag #'(all part) \key do \major \tempo "Allegro" \time 4/4 s1*17 s4.
\tempo "Allegro" s8 s2 s1*3
\tag #'(all part) \key fa \major \tempo "Andante maestoso" \time 4/4 s1*47
\tempo "Allegro assai" s1*14
\tag #'(all part) \key do \major \midiTempo#96 \time 4/4
<<
  \tag #'(all cuivres) {
    s1*4
    \tempo "Allegro assai" s1*10
    \tempo "Allegro" s1*7
    \tempo "Presto" s1*18 \bar "|."
  }
  \tag #'(part part-cuivres) {
    s1*39 \bar "|."
  }
>>
