\clef "vbas-dessus" R1*68 |
r4 fa''2. |
fa''2 fa''4. fa''8 |
fa''2 fa''4 fa'' |
mi''1 |
sol''2. mi''4 |
fa''2. fa''4 |
sol''2 sib'4. sib'8 |
la'2 fa''4 fa'' |
re''2 mi''4 mi'' |
fa''2 fa''4 fa'' |
re''2 do''4 do'' |
do''2 r |
R1*41 |


