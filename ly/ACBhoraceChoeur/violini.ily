\clef "treble" do'16 si do' si do' si do' si do' si do' si do' si do' si |
do' re' mi' fa' sol' fa' mi' re' do' re' mi' fa' sol' fa' mi' re' |
do'8 re'16 mi' fa' sol' la' si' do''8 sol'16 sol' sol'8 sol' |
mi'4 r r2 |
R1*2 |
do'16 si do' si do' si do' si do' si do' si do' si do' si |
do' re' mi' fa' sol' fa' mi' re' do' re' mi' fa' sol' fa' mi' re' |
do'8 do'16 re' mi' fa' sol' la' sib'8 sol'16 sol' mi'8 sol' |
dod'2~ dod' |
re'8 mi'16 fa' sol' la' si' dod'' re''8 mi''16 fa'' sol'' la'' si'' dod''' |
<<
  \tag #'(violino1 violino1-conducteur) {
    \rt#4 re'''16 \rt#4 re''' <>\p \rt#4 re''' \rt#4 re''' |
    \rt#8 re''' \rt#8 re''' |
  }
  \tag #'(violino2 violino2-conducteur) {
    re'''16 fa'' fa'' fa'' \rt#4 fa'' <>\p \rt#4 fa'' \rt#4 fa'' |
    \rt#8 fa'' \rt#8 fa'' |
  }
>>
<>\f mi'8 fad'16 sold' la' si' dod'' red'' mi''4 r |
r8. << { sold''16 sold''4 } \\ { si'16 si'4 } >> r2 |
r8 mi''16 mi'' sold'' sold'' si'' si'' re''' re''' si'' si'' sold'' sold'' mi'' mi'' |
re''1 |
<<
  \tag #'(violino1 violino1-conducteur) {
    do''4. mi'8\f mi'4 mi' |
    mi'1~ |
    mi'2\p mi'2~ |
    mi'1 |
    <la fa'>1\f |
    <sol mi' do''>4. sol''8 sol''4. fa''32 mi''! re'' do'' |
    si'1\fp |
    do''2. do''4\f |
    reb''4.*11/12_"serré" do''32 sib' la' sol' fa' mi'!8. mi'16 mi'8. mi'16 |
    fa'8.
  }
  \tag #'(violino2 violino2-conducteur) {
    do''4. do'8\f do'4 do' |
    do'1~ |
    do'2\p do'~ |
    do'1 |
    do'1\f |
    <sol' sol>1 |
    <sol fa'>\fp |
    mi'2. r4 |
    reb'8-\sug\f reb'4 reb' reb' reb'8 |
    do'8.
  }
>> do'16 reb'8. do'16 fa'8. fa'16 lab'8. do''16 |
<<
  \tag #'(violino1 violino1-conducteur) {
    reb''4.*11/12\p do''32 sib' la' sol' fa' mi'!8. mi'16 mi'8. mi'16 |
    fa'16\fp lab' lab' lab' \rt#4 lab' \rt#8 lab' |
    \rt#8 sol'16 \rt#8 sol' |
    sol'1\fp |
  }
  \tag #'(violino2 violino2-conducteur) {
    reb'8-\sug\p reb'4 reb' reb' reb'8 |
    <fa' lab>16-\sug\fp fa' fa' fa' \rt#4 fa' \rt#8 fa' |
    \rt#8 fa'16 \rt#8 <mi' do''> |
    <fa' si'>1\fp |
  }
>>
<>\ff \rt#16 <sol' sib'!>32 \rt#16 <sol' sib'> |
<>\ff \rt#16 <la'! fa''> \rt#16 <la' fa''> |
<< { \rt#16 fa''32 \rt#16 mi'' } \\ { \rt#16 sol' \rt#16 sol' } >> |
<<
  \tag #'(violino1 violino1-conducteur) {
    << fa''2 \\ la' >> r4 fa' |
    mi'8\sf( sol' re'' do'') do''4 do'' |
    do''2 sib'8( la' sol' fa') |
    sol'1 |
    la'2 r4 fa' |
    mi'8\sf( sol' re'' do'') do''4 do'' |
    do''2 sib'8( la' sol' fa') |
    sol'4.-\sug\p la'8 sib'4. do''8 |
    la'8 do''4 do'' do'' do''8 |
    si'8\cresc re''4 sol'' re'' si'8 |
    do'' mi''4 sol'' mi'' do''8 |
    re''8 re'''4 si'' sol'' fa''8 |
    mi'' sol''4 mi'' do'' sol'8\! |
    do''8\f mib'' mib''2 re''4 |
    re''4.( si'8) do''4 sol' |
    do''8(\sf mib'') mib''2 re''4\p |
    re''4. si'8 do''4 do'' |
    mib''4. do''8 do''4. fad''8 |
    sol''2 r |
    <>^"Retenir"
  }
  \tag #'(violino2 violino2-conducteur) {
    <fa' la>8 do'\p do' do' \rt#4 do' |
    <>-\sug\f \rt#4 do' \rt#4 do' |
    \rt#4 do' \rt#4 do'8 |
    <>-\sug\p \rt#4 do' \rt#4 do' |
    \rt#4 do' \rt#4 do' |
    \rt#4 do' \rt#4 do' |
    \rt#4 do' \rt#4 do' |
    <>-\sug\p \rt#4 do' \rt#4 do' |
    do'8 la'4 la' la' la'8 |
    sol'8-\sug\cresc sol'4 sol' sol' sol'8~ |
    sol' sol'4 sol' sol' sol'8~ |
    sol' sol'4 sol' sol' sol'8 |
    sol' sol'4 sol' sol' sol'8\! |
    mib'-\sug\f sol' sol' sol' \rt#4 sol' |
    lab'8 mib' mib' mib' mib' do' si si |
    do' sol' sol' sol' <>-\sug\p \rt#4 sol' |
    lab' mib' mib' mib' \rt#4 mib' |
    \rt#4 do' mib' mib' mib' do' |
    si2 r |
  }
>>
do'4\ff do'8 do' mi'4 mi' |
sol'2
<<
  \tag #'(violino1 violino1-conducteur) {
    sol''4.\p sol''8 |
    la''2 si''4. si''8 |
    do'''2 <<
      { do'''4. do'''8 | do'''4. do'''8 si''4. si''8 | do'''4 } \\
      { mi''4. mi''8 | re''4. re''8 re''4. re''8 | mi''4 }
    >>
  }
  \tag #'(violino2 violino2-conducteur) {
    si'4.\p si'8 |
    re''2 re''4. re''8 |
    sol'2 la'4. la'8 |
    fa'4.\prall mi'16 fa' sol'4. sol'8 |
    do'4
  }
>> do'8\f do' do'4 sib! |
la2 <<
  \tag #'(violino1 violino1-conducteur) {
    fa''4 la'' |
    do'''1~ |
    do'''4 re''2 re''4~ |
    re'' fad''8 fad'' la''4 do''' |
    sib''2 r\fermata |
    sib''2\p sib'' |
    sib'' sib'' |
    la''2
  }
  \tag #'(violino2 violino2-conducteur) {
    r2 |
    r4 do' fa' mib' |
    re'4 <re' la>8\f q q4 q |
    q1 |
    <sib re'>2 r\fermata |
    re''2\p do'' |
    mi'' sol''4 sol'' |
    do''2
  }
>> r4 << do''4 \\ fa' >> |
<sib fa' re''>4.*11/12\f do''32 sib' la' sol' fa' mi'4 <<
  { mi''4 | fa''8 } \\ { <sol' sib'>4 | <fa' la'>8 }
>> <la' fa''>8\ff q q \rt#4 q |
<<
  \tag #'(violino1 violino1-conducteur) {
    fa''8 si''[ si'' si''] \rt#4 si'' |
    \rt#4 si'' \rt#4 si'' |
    \rt#4 do'''8 \rt#4 do''' |
    do'''4 sol''2 fa''16 mi'' re'' do'' |
    do''4 la''2 fa''8 mi''16 fa'' |
    sol''8 do'' mi'' sol'' sib'' sol'' mi'' sib' |
    la' fa'' mi'' fa'' la'' fa'' mi'' fa'' |
    re'' fa'' \grace sol''8 fa'' mi''16 fa'' mi''8 do'' mi'' sol'' |
    fa'' la'' do''' do''' \rt#4 do''' |
    re'''2 << { mi''4 mi'' } \\ { <sib' sol'>4 q } >> |
    << fa''4 \\ <fa' la'> >>
  }
  \tag #'(violino2 violino2-conducteur) {
    si'8 fa''[ fa'' fa''] \rt#4 fa'' |
    \rt#4 fa'' \rt#4 fa'' |
    \rt#4 <do'' mi''>8 \rt#4 q |
    \rt#4 <sib'! mi''>8 \rt#4 <sib' mi''> |
    << { \rt#4 fa''8 \rt#4 fa'' |
        \rt#4 mi'' \rt#4 mi'' |
        \rt#4 fa'' \rt#4 fa'' | } \\
      { \rt#4 la' \rt#4 la' |
        \rt#4 sib' \rt#4 sib' |
        \rt#4 la' \rt#4 la' | } >>
    fa'8 sib' re'' sib' sol' mi' sol' mi' |
    fa' <la' fa''> q q \rt#4 q |
    \rt#4 <fa'' sib'> \rt#4 <sib' mi''> |
    << fa''4 \\ la' >>
  }
>> fa'16 sol' la' sib' do''4 re'' |
mib''8. re''16 do''8. sib'16 la'8. do''16 la'8. sol'16 |
fad'4
<<
  \tag #'(violino1 violino1-conducteur) {
    re'''8. re'''16 re'''4 re''' |
  }
  \tag #'(violino2 violino2-conducteur) {
    << { fad''8. fad''16 fad''4 fad'' } \\
      { la'8. la'16 la'4 la' } >> |
  }
>>
%%%
<<
  \tag #'violino1 { re'''1~ | }
  \tag #'violino1-conducteur { re'''2~ re'''~ | }
  \tag #'violino2 { <la' fad''>1~ | }
  \tag #'violino2-conducteur { <la' fad''>2~ q~ | }
>>
<<
  \tag #'(violino1 violino1-conducteur) {
    re'''1~ | re'''~ | re''' |
  }
  \tag #'(violino2 violino2-conducteur) {
    <la' fad''>1~ | q~ | q |
  }
>>
r8. sol'16 la'8. si'!16 do''8. re''16 mi''8. fad''16 |
sol''8. sol''16 si''8. sol''16 fad''8. sol''16 fad''8. mi''16 |
<<
  \tag #'(violino1 violino1-conducteur) {
    si''1~ | si'' | si'' | lad''4
  }
  \tag #'(violino2 violino2-conducteur) {
    <si' fad''>1~ | q | <sol'' si'>1 | << dod''4 \\ fad' >>
  }
>> r4 r8. fad'16 fad'8.\prall mi'32 fad' |
sol'4. 
<<
  \tag #'(violino1 violino1-conducteur) {
    << { re'''8 re'''2 } \\ { re''8 re''2 } >> |
  }
  \tag #'(violino2 violino2-conducteur) {
    << { si''8 si''2 } \\ { re''8 re''2 } >> |
  }
>>
R1*2 |
<<
  \tag #'(violino1 violino1-conducteur) {
    <re' si' si''>4
  }
  \tag #'(violino2 violino2-conducteur) {
    <re' si' sol''>4
  }
>> r4 r2 |
r4 r16 do' mi' sol' do''8. do''16 do''8. do''16 |
re''4. do''16 si'32 la' sol'8. si'16 re''8. sol''16 |
mi''4 r r2 |
r4 
<<
  \tag #'(violino1 violino1-conducteur) {
    do'''2.\f | la''4
  }
  \tag #'(violino2 violino2-conducteur) {
    <mi' do'' sol''>2.\f | <fa' do'' la''>4
  }
>> r4 r2 |
R1*2 |
r8. sib'16 do''8. re''16 mib''8. fa''16 sol''8. la''16 |
sib''4 r r2 |
<mib' sib' sol''>4 r r2 |
R1 |
r2 << sol''4 \\ sib' >> r4 |
r2 r4 <mib' sib' sol''> |
<<
  \tag #'(violino1 violino1-conducteur) {
    << { lab''2~ lab''4 } \\ { do''2~ do''4 } >> r4 | do'''
  }
  \tag #'(violino2 violino2-conducteur) {
    <mib'' mib'>2~ q4 r | <do'' sol''>4
  }
>> r4 r2 |
r8. << { la''!16 la''4 } \\ { do''16 do''4 } >> r2 |
<<
  \tag #'(violino1 violino1-conducteur) {
    r8. dod'''16 dod'''4 r2 |
    r8. re'''16 re'''4 r2 |
    r8. << { sold''16 sold''4 } \\ { si'16 si'4 } >> r2 |
  }
  \tag #'(violino2 violino2-conducteur) {
    r8. sol''16 sol''4 r2 |
    r8. << { fa''16 fa''4 } \\ { la'16 la'4 } >> r2 |
    r8. << { mi''16 mi''4 } \\ { si'16 si'4 } >> r2 |
  }
>>
R1 |
<<
  \tag #'(violino1 violino1-conducteur) {
    r2 r8. << { la''16 la''8. la''16 | la''4 }
      \\ { la'16 la'8. la'16 | la'4 }
    >> r4 << si''4 \\ si' >> r4 |
    r8. << { si''16 si''4 } \\ { si'16 si'4 } >> r2 |
  }
  \tag #'(violino2 violino2-conducteur) {
    r2 r8. << { mi''16 mi''8. mi''16 | mi''4 }
      \\ { la'16 la'8. \parenthesize la'16 | la'4 }
    >> r4 fad'' r |
    r8. << { sol''16 sol''4 } \\ { si'16 si'4 } >> r2 |
  }
>>
r2 << lad''4 \\ dod'' >> r4 |
r4 << <lad'' dod''>4 \\ fad' >> <fad' re'' si''>2 |
