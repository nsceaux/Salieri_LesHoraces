\clef "treble" do'1 |
do'16 re' mi' fa' sol' fa' mi' re' do' re' mi' fa' sol' fa' mi' re' |
do'8 re'16 mi' fa' sol' la' si' do''8 sol'16 sol' sol'8 sol' |
mi'4 r r2 |
R1*2 |
do'1 |
do'16 re' mi' fa' sol' fa' mi' re' do' re' mi' fa' sol' fa' mi' re' |
do'8 do'16 re' mi' fa' sol' la' sib'8 sol'16 sol' mi'8 sol' |
dod'2 r |
re'8 mi'16 fa' sol' la' si' dod'' re''8 mi''16 fa'' sol'' la'' si'' dod''' |
re'''4 r r2 |
R1 |
mi'8-\sug\f fad'16 sold' la' si' dod'' red'' mi''4 r |
<<
  { r8. s16 s4 r2 |
    r8 s s2. |
    s2 r |
    r4 r8 s-\sug\f s2 |
    s r |
  }
  \tag #'(oboe1 oboi) \new Voice {
    \tag #'oboi \voiceOne s8. sold''16 sold''4 s2 |
    s8 re''16 re'' re''8 re'' \rt#4 re'' |
    re''2 s |
    s4. mi'8 mi'4 mi' |
    mi'2 s |
  }
  \tag #'(oboe2 oboi) \new Voice {
    \tag #'oboi \voiceTwo s8. si'16 si'4 s2 |
    s8 si'16 si' si'8 si' \rt#4 si' |
    si'2 s |
    s4. do'8 do'4 do' |
    do'2 s |
  }
>>
R1*2 |
fa'1-\sug\f |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''1 | fa'' | mi''2 }
  { sol'1 | si' | do''2 }
  { s1 | <>-\sug\fp }
>> r4 do''-\sug\f |
reb''2. mi''!4 |
fa''8. do''16 reb''8. do''16 do''4 do'' |
reb''2.-\sug\p mi''!4 |
fa''2 \twoVoices #'(oboe1 oboe2 oboi) <<
  { lab'4. lab'8 | sol'4. sol'8 do''4. do''8 |
    si'1 | sib'! | la'!2 fa'' | fa''2 mi'' | fa'' }
  { fa'4. fa'8 | fa'4. fa'8 mi'!4. mi'8 |
    fa'1 | sol' | fa'2 la' | sol'1 | fa'2 }
  { s2 | s1 | s1-\sug\fp | s1-\sug\ff }
>> r2 |
R1*7 |
r2 r4 <>-\sug\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { la''4 | sol''1~ | sol'' | sol'' | sol'' | }
  { do''4 | si'1 | do'' | re'' | mi'' | }
  { s4 | <>-\sug\cresc s1*3 | s2. s4\! }
>>
R1*6 |
do'4-\sug\ff do'8 do' mi'4 mi' |
sol'2 r |
R1 |
r2 <>-\sug\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''4. mi''8 | re''2 re''4. re''8 |
    do''4 do''8 do'' do''4 do'' | do''2 }
  { do''4. do''8 | do''2 si'4. si'8 |
    do''4 mi'8 mi' mi'4 mi' | fa'2 }
  { s2 | s1 | s4 <>-\sug\f }
>> r2 |
r4 do''4 fa'' mib'' |
<<
  { s1*2 |
    s2 r2\fermata |
    <>\p s1*2 |
    s2 r4 s |
    r2 r4 s-\sug\f |
  }
  \tag #'(oboe1 oboi) \new Voice {
    \tag #'oboi \voiceOne re''1~ | re''1 | re''2 s | re''2 do'' |
    mi'' sol'' | do''2 s4 do'' | s2. mi''4 | fa'' fa''2 fa''4 |
    fa'' si''2 si''4 | si''1 | do'''1 | sol'' | la'' | sib'' |
    la''2 la'' | sib'' sol'' | la'' do''' | re''' do'''4 do''' | la''2
  }
  \tag #'(oboe2 oboi) \new Voice {
    << \tag #'oboi { \voiceTwo s1 } \tag #'oboe2 re''1 >> |
    la'1 | sib'2 s | sib'2 sib' |
    sib' sib' | la' s4 fa' | s2. sol'4 | la' la'2 la'4 |
    si' fa''2 fa''4 | fa''1 | mi'' | mi'' | fa'' | sol'' |
    fa''2 fa'' | re'' mi'' | fa'' fa'' | fa'' mi''4 mi'' | fa''2
  }
>> r2 |
R1 |
r4
<<
  \tag #'(oboe1 oboi) \new Voice {
    \tag #'oboi \voiceOne la''8. la''16 la''4 la'' | la''
  }
  \tag #'(oboe2 oboi) \new Voice {
    \tag #'oboi \voiceTwo fad''8. fad''16 fad''4 fad'' | fad''
  }
>> r4 r2 |
R1*38 |
