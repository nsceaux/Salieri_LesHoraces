\clef "bass" R1*68 |
r4 fa2. |
re'2 re'4. re'8 |
re'2 re'4 re' |
do'1 |
do'2. do'4 |
do'2. do'4 |
do'2 do'4. do'8 |
fa2 fa4 fa |
sib2 do'4 do' |
fa2 fa4 fa |
sib2 do'4 do' |
fa2 r |
R1*41 |

