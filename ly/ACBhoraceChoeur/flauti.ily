\clef "treble" R1*34 |
r2 r4 fa'' |
mi''8(\sf sol'' re''' do''') do'''4 do''' |
do'''2 sib''8( la'' sol'' fa'') |
sol''1 |
la''2 r4 fa'' |
mi''8(\sf sol'' re''' do''') do'''4 do''' |
do'''2 sib''8( la'' sol'' fa'') |
sol''4.( la''8 sib''4. do'''8) |
la''2. r4 |
R1*3 |
r2 r4 r8 sol'' |
do'''8-\sug\f mib''' mib'''2 re'''4 |
re'''4. si''8 do'''4 r8 sol'' |
do'''8-\sug\sf( mib''') mib'''2 re'''4-\sug\p |
re'''4. si''8 do'''4. do'''8 |
mib'''4. do'''8 do'''4. fad''8 |
sol''2 r |
R1*14 |
r2 r4 <>-\sug\f <<
  \tag #'(flauto1 flauti) \new Voice {
    \tag #'flauti \voiceOne mi''4 | fa'' fa''2 fa''4 | fa'' si''2 si''4 |
    si''1 | do''' | sol'' | la'' | sib'' |
    la''2 la'' | sib'' sol'' | la'' do''' | re''' do'''4 do''' | la''2
  }
  \tag #'(flauto2 flauti) \new Voice {
    \tag #'flauti \voiceTwo sol'4 | la' la'2 la'4 | si' fa''2 fa''4 |
    fa''1 | mi''1 | mi'' | fa'' | sol'' |
    fa''2 fa'' | re'' mi'' | fa'' fa'' | fa'' mi''4 mi'' | fa''2
  }
>> r2 |
R1 |
r4 <<
  \tag #'(flauto1 flauti) \new Voice {
    \tag #'flauti \voiceOne la''8. la''16 la''4 la'' | la''4
  }
  \tag #'(flauto2 flauti) \new Voice {
    \tag #'flauti \voiceTwo fad''8. fad''16 fad''4 fad'' | fad''4
  }
>> r4 r2 |
R1*38 |
