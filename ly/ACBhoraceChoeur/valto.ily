\clef "vhaute-contre" R1*68 |
r4 la'2. |
si'2 si'4. si'8 |
si'2 si'4 si' |
do''1 |
sib'!2. sib'4 |
la'2. la'4 |
sol'2 sol'4. sol'8 |
fa'2 la'4 la' |
sib'2 sol'4 sol' |
la'2 la'4 la' |
sib'2 sol'4 sol' |
la'2 r |
R1*41 |
