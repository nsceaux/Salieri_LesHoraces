\clef "bass" \rt#8 do16 \rt#8 do |
\rt#8 do \rt#8 do |
do4 r do8 sol,16 sol, sol,8 sol, |
sol,4 r r2 |
R1*2 |
\rt#8 do16 \rt#8 do |
\rt#8 do \rt#8 do |
do2 r8 do16 do do8 do |
sol,2 r |
R1*111 |
