\clef "treble"
<< fad''2 \\ la'2 >> r2 |
r r8. <si' re'>16 q4 |
R1 |
r4 << { <mi'' dod''>4 <mi'' si'>2 | } \\ { la'4 sold'2 | } >>
R1 |
r8. la'16 la'2.~ |
la'1~ |
la'~ |
la'2 do''~ |
do''~ do'' |
re'' mib''!-\sug\f~ |
mib''1 |
r8 re'4-\sug\f re' re' re'8 |
lab'8 lab'4 lab' lab' lab'8~ |
<lab' sib>8 q4 q q q8 |
<sib sol'>2 r8. sib'16 sib'8. sib'16 |
fa''2 r |
r r4 <si' re' sol> |
