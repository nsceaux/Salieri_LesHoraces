\tag #'vieilhorace {
  Peu -- ples, dis -- si -- pez vos a -- lar -- mes,
  Les Dieux nous ont don -- né la paix.
}
\tag #'(vsoprano valto vtenor vbasso) {
  _ Ciel !
}
\tag #'vieilhorace {
  Albe & Ro -- me ont dé -- po -- sé leurs ar -- mes.
}
\tag #'camille {
  Grands Dieux ! Ah ! __ par -- don -- nez mes trans -- ports in -- dis -- crets !
}
\tag #'curiace {
  Che -- re Ca -- mil -- le, en -- fin je puis re -- voir vos char -- mes.
}
\tag #'camille {
  Ah ! __ tu m’as coû -- té bien des lar -- mes !
}
\tag #'(vsoprano valto vtenor vbasso) {
  Quel mi -- ra -- cle a pro -- duit ces é -- ton -- nans ef -- fets ?
}
\tag #'vieilhorace {
  Un Dieux, qui parle aux cœurs.
}
