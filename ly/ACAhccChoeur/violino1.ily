\clef "treble" re''2 r |
r r8. << { sol''16 sol''4 } \\ { si'16 si'4 } >> |
R1 |
r4 << <mi'' dod''>4 \\ la' >> <mi' si' si''>2 |
R1 |
r8. << { mi''16 mi''2.~ | mi''1~ | mi''2 }
  \\ { dod''16 dod''2.~ | dod''1~ | dod''2 }
>> re''2~ |
re'' fa''!~ |
fa''~ fa''~ |
fa'' <do'' la''>2\f~ |
q1 |
r8 sib'4\f sib' sib' sib'8 |
re'' re''4 re'' re'' re''8~ |
re'' << { re''4 re'' re'' re''8 | mib''2 }
  \\ { fa'4 fa' fa' fa'8 | mib'2 }
>> r8. sol''16 sol''8. sol''16 |
si''2 r |
r r4 <si' re' sol> |


