\clef "vhaute-contre" la'2\repeatTie r |
R1*2 |
r2 mi' |
R1*8 |
r2 r4 sol'8 sol' |
lab'4 lab' r lab'8 lab' |
lab'4. lab'8 lab' lab' lab' lab' |
sol'4 r r2 |
R1*2 |
