\clef "soprano/treble" R1*5 |
r4 r8 mi'' dod''4 sol''~ |
sol'' mi''8 re'' dod''4^! la'8 si' |
dod''4 dod''8 re'' la'4^! r4 |
R1*2 |
r2 mib''!~ |
mib''8 la' la' sib' do''4 do''8 re'' |
sib'^! sib' r4 r2 |
R1*5 |
