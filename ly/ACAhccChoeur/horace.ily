\clef "bass" re'2 re'8 r re' re' |
re'4 do'8 re' si^! si r sol |
dod'4. mi'8 dod' dod' dod' re' |
la4^! r r r8 si16 mi' |
mi'?8 si? r si si dod' re' dod' |
la8^! la r4 r2 |
R1*10 |
r4 r8 re' si2~ |
si8 si si do' sol4 r |
