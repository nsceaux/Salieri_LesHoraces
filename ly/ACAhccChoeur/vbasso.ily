\clef "bass" re'2\repeatTie r |
R1*2 |
r2 sold |
R1*8 |
r2 r4 sol8 sol |
fa4 fa r fa8 fa |
sib4. sib8 sib sib sib sib |
mib4 r r2 |
R1*2 |
