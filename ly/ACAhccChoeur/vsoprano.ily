\clef "vbas-dessus" fad''2\repeatTie r |
R1*2 |
r2 mi'' |
R1*8 |
r2 r4 sib'8 sib' |
re''4 re'' r fa''8 fa'' |
re''4. re''8 re'' re'' re'' sib' |
mib''4 r r2 |
R1*2 |
