\clef "tenor" re'2 r |
R1*11 |
\clef "bass" sol4\f sol sol sol |
fa! fa fa fa |
sib, sib, sib, sib |
mib2 r8. mib16 mib8. mib16 |
re2 r |
R1 |
