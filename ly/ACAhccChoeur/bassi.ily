\clef "bass" re2 r |
r r8. sol16 sol4 |
R1 |
r4 la sold2 |
R1 |
r8. la16 la2.~ |
la2 sol!~ |
sol fa!~ |
fa la~ |
la~ la |
sib fad\f~ |
fad1 |
sol4\f sol sol sol |
fa! fa fa fa |
sib, sib, sib, sib |
mib2 r8. mib16 mib8. mib16 |
re2 r |
r r4 sol |
