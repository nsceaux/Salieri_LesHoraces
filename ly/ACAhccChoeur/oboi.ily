\clef "treble"
<<
  \tag #'(oboe1 oboi) \new Voice {
    \tag #'oboi \voiceOne fad''2
  }
  \tag #'(oboe2 oboi) \new Voice {
    \tag #'oboi \voiceTwo la'2
  }
>> r2 |
R1*11 |
r8 <>-\sug\f <<
  \tag #'(oboe1 oboi) \new Voice {
    \tag #'oboi \voiceOne sib'4 sib' sib' sib'8 |
    re''8 re''4 re'' re'' re''8 |
    lab''8 lab''4 lab'' lab'' lab''8 |
    sol''2
  }
  \tag #'(oboe2 oboi) \new Voice {
    \tag #'oboi \voiceTwo re'4 re' re' re'8 |
    lab'8 lab'4 lab' lab' lab'8 |
    re''8 re''4 re'' re'' re''8 |
    mib''2
  }
>> r8. <<
  \tag #'(oboe1 oboi) \new Voice {
    \tag #'oboi \voiceOne sol''16 sol''8. sol''16 | si''2
  }
  \tag #'(oboe2 oboi) \new Voice {
    \tag #'oboi \voiceTwo sib'16 sib'8. sib'16 | fa''2
  }
>> r2 |
R1 |
