\clef "alto" re'2 r |
r r8. sol'16 sol'4 |
R1 |
r4 la' sold'2 |
R1 |
r8. la'16 la'2.~ |
la'2 sol'!~ |
sol' fa'!~ |
fa' la'~ |
la'~ la' |
sib' fad'-\sug\f~ |
fad'1 r8 sib'4-\sug\f sib' sib' sib'8~ |
sib' sib'4 sib' sib' sib'8~ |
sib' fa'4 re' sib sib8 |
sib2 r8. mib'16 mib'8. mib'16 |
re'2 r |
r r4 sol |
