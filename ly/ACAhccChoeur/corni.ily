\clef "treble" \transposition sib
<<
  \tag #'(corno1 corni) \new Voice {
    \tag #'corni \voiceOne mi''2
  }
  \tag #'(corno2 corni) \new Voice {
    \tag #'corni \voiceTwo mi'2
  }
>> r2 |
R1*11 |
do''1-\sug\f |
<<
  \tag #'(corno1 corni) \new Voice {
    \tag #'corni \voiceOne mi''1~ | mi'' | fa''4
  }
  \tag #'(corno2 corni) \new Voice {
    \tag #'corni \voiceTwo do''1~ | do'' | do''4
  }
>> r4 r8. do''16 do''8. do''16 |
<<
  \tag #'(corno1 corni) \new Voice {
    \tag #'corni \voiceOne mi''2
  }
  \tag #'(corno2 corni) \new Voice {
    \tag #'corni \voiceTwo sol'2
  }
>> r2 |
R1 |
