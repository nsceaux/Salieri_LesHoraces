\clef "treble"
<<
  \tag #'(flauto1 flauti) \new Voice {
    \tag #'flauti \voiceOne re''2
  }
  \tag #'(flauto2 flauti) \new Voice {
    \tag #'flauti \voiceTwo fad''2
  }
>> r2 |
R1*11 |
r8 sib'4-\sug\f sib' sib' sib'8 |
<<
  \tag #'(flauto1 flauti) \new Voice {
    \tag #'flauti \voiceOne re''8 re''4 re'' re'' re''8 |
    lab''8 lab''4 lab'' lab'' lab''8 | sol''2
  }
  \tag #'(flauto2 flauti) \new Voice {
    \tag #'flauti \voiceTwo lab'8 lab'4 lab' lab' lab'8 |
    re''8 re''4 re'' re'' re''8 | mib''2
  }
>> r8. <<
  \tag #'(flauto1 flauti) \new Voice {
    \tag #'flauti \voiceOne sol''16 sol''8. sol''16 | si''2
  }
  \tag #'(flauto2 flauti) \new Voice {
    \tag #'flauti \voiceTwo sib'16 sib'8. sib'16 | fa''2
  }
>> r2 |
R1 |
