\clef "tenor/G_8" R1*8 |
re'4 mi'8 fa' fa'^! do' r do' |
la4. do'8 mib'!8. mib'16 mib'8 fa' |
re'8^! re' r4 r2 |
R1*7 |
