\clef "tenor" do'2.\p |
re' |
re' |
do'4 r r |
mib'-\sug\f( fa' mib') |
re'2.-\sug\p |
re'4-\sug\f( mib' re') |
do'2.-\sug\p |
do'4 re'4. do'8 |
\grace re'4 do' si r |
R2. |
re'2.-\sug\p |
re' |
do'8( re' mib' re' do' sib!) |
lab2.-\sug\cresc |
mib'\!-\sug\f~ |
mib'8-\sug\p sol'( fa' mib' re') fa' |
fa'2( mib'4) |
fa'2. |
mib'4 r r |
