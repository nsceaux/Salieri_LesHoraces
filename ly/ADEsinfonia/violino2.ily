\clef "treble" R2. |
re'8\p re'4 re' re'8 |
re' re'4 re' re'8 |
do' r r4 r |
mib'4-\sug\f( fa' mib') |
re'2.-\sug\p |
re'4(-\sug\f mib' re') |
do'2.-\sug\p |
do'4 re'4. do'8 |
\grace re'4 do' si r |
R2. |
re'8\p re'4 re' re'8 |
re' re'4 re' re'8 |
do'2. |
mib'8-\sug\cresc mib'4 mib' mib'8\! |
mib'2.-\sug\f |
mib'8-\sug\p sol'( fa' mib' re') fa' |
fa'2( mib'4) |
fa'2. |
mib'4 r r |
