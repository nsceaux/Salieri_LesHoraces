\clef "bass" do4\p( mib lab fa re fa) |
sol sol,8 fa( mib re) |
do4 r r |
do4\f( re do) |
sib,2.\p |
sib,4\f( do sib,) |
lab,2.\p |
lab,4 fa, fad, |
sol, sol r |
do4\p( mib lab |
fa re fa) |
sol4 sol,8 fa( mib re) |
do( re mib re do sib,!) |
lab,\cresc( do mib lab mib do\!) |
lab,2.\f |
sol,\p |
do |
sol,2. |
do4 r r |
