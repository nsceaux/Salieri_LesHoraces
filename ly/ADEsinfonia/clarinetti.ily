\clef "treble" R2.*3 |
<>^"Solo" r8 do''( re'' mib'' fa'' sol'') |
lab''2.\f |
lab''8(\p fa'' re'' sib' la' sib') |
sol''2.-\sug\f~ |
sol''8-\sug\p( mib'' do'' lab' sol' lab') |
fa''2~ fa''8 mib'' |
\grace fa''4 mib''4 re'' r |
R2.*4 |
do''2.-\sug\p-\sug\cresc |
mib''2\!-\sug\f ~ mib''8 fad'' |
sol''2-\sug\p~ sol''8 si' |
si'2( do''4) |
si'2. |
do''4 r r |
