\clef "treble" R2.*2 |
sol'8\p sol'4 lab'8( sol' fa') |
mib' do'([\rinf re' mib' fa' sol']) |
lab'8\f lab'4 lab' lab'8 |
lab'8\p( fa' re' sib la sib) |
sol'8\f sol'4 sol' sol'8 |
sol'\p( mib' do' lab sol lab) |
fa'8 fa'4 fa' mib'8 |
\grace fa'4 mib' re' r |
R2.*2 |
sol'8\p sol'4 lab'8( sol' fa') |
mib'2. |
do''8\p do''4\cresc do'' do''8\! |
mib''\f( do'' sib' lab' sol' fad') |
sol'2\p~ sol'8 si |
si2( do'4) |
si2. |
do'4 r r |
