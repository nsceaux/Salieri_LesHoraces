\clef "alto" do'8\p do'4 do' do'8~ |
do' do'4 do' do'8 |
si8 si4 si si8 |
do'4 r r |
do'4-\sug\f( re' do') |
sib2.-\sug\p |
sib4(-\sug\f do' sib) |
lab2.-\sug\p |
fa4 fa fad |
sol sol' r |
do'8\p do'4 do' do'8 |
do' do'4 do' do'8 |
si si4 si si8 |
do'8( re' mib' re' do' sib!) |
lab-\sug\cresc( do' mib' lab' mib' do')\! |
lab2.-\sug\f |
sol8-\sug\p mib'( re' do' si) re' |
re'2( do'4) |
re'2. |
do'4 r r |
