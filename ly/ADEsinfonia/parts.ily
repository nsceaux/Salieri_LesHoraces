\piecePartSpecs
#`((clarinetto1 #:score-template "score-clarinette-sib")
   (clarinetto2 #:score-template "score-clarinette-sib"
                #:instrument ,#{\markup\center-column { Clarinette I en si♭}#})
   (fagotto1 #:notes "fagotto1" #:clef "tenor")
   (fagotto2 #:notes "fagotto2" #:clef "tenor")
   (violino1 #:notes "violino1")
   (violino2 #:notes "violino2")
   (alto)
   (basso #:instrument , #{ \markup\center-column { Basso Contrabasso } #})
   (silence #:on-the-fly-markup , #{ \markup\tacet#37 #}))
