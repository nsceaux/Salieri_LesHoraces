\clef "tenor" do'2.-\sug\p |
do' |
si |
do'4 r r |
do'-\sug\f( re' do') |
sib2.-\sug\p |
sib4-\sug\f( do' sib) |
lab2.-\sug\p |
lab4 fa fad |
sol2 r4 |
do'2.-\sug\p |
do' |
si |
do'8( re' mib' re' do' sib!) |
lab2.-\sug\cresc |
do'-\sug\f~ |
do'8-\sug\p mib'( re' do' si) re' |
re'2( do'4) |
re'2. |
do'4 r r |
