\clef "vtaille" r4 re' re' re' |
re'2 re'4 re' |
re'2 re'4 re' |
fad'1~ |
fad' |
fad'4 r mib'! do' |
do'2 la4 do' |
re'2. re'4 |
do'2. sol'4 |
fad'2. fad'4 |
sol'1 |
R1*5 |
r2 mib'4^\f mib' |
mib'1 |
mib'2. mib'4 |
re'1 |
re'4 re' re' re' |
fad'2 fad'4 fad' |
fad'2 fad'4 fad' |
fad'1~ |
fad' |
fad'2 mib'!4 do' |
do'2 la4 re' |
re'2. re'4 |
do'2. sol'4 |
fad'2. fad'4 |
sol'1 |
R1 |
r2\fermata r |
R1*10 |
r2\fermata r4 la |
fa'2. fa'4 |
sol'2. sol'4 |
fa'1 |
fa'4 fa' fa' fa' |
fad'2. fad'4 |
fad' fad' fad' fad' |
sol'1 |
R1 |
sol' |
sol'2 sol' |
re'1 |
r2 re' |
sol' sol' |
sol' sol' |
re'1~ |
re'~ |
\custosNote re'4
