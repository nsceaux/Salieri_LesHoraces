\clef "treble" sol8 sib'' sib'' sib'' \rt#4 sib'' |
\rt#4 sib'' \rt#4 sib'' |
\rt#4 sib'' \rt#4 sib'' |
\rt#4 do''' \rt#4 do''' |
\rt#4 do''' \rt#4 do''' |
\rt#4 do''' \rt#4 do''' |
do''' la'' la'' la'' \rt#4 la'' |
\rt#4 sol'' \rt#4 sol'' |
\rt#4 sol'' \rt#4 sol'' |
\rt#4 fad'' \rt#4 fad'' |
sol'' sib-\sug\p sib sib \rt#4 sib |
\rt#4 sib \rt#4 sib |
sib8 sib'[\f sib' sib'] \rt#4 sib' |
mib'' mib'' sol'' mib'' \rt#4 sib' |
sib' sib sib' sib' \rt#4 sib' |
<>\p \rt#4 mib' \rt#4 mib' |
mib' sib'-\sug\ff sib' sib' \rt#4 sib' |
\rt#4 <sib' sol''> \rt#4 q |
sib'8 sol'' sol'' sol'' \rt#4 sol'' |
\rt#4 fad'' \rt#4 fad'' |
\rt#4 fad'' \rt#4 fad'' |
\rt#4 <fad'' la'> \rt#4 q |
\rt#4 q \rt#4 q |
\rt#4 q \rt#4 q |
\rt#4 q \rt#4 q |
\rt#4 q \rt#4 q |
la' la'' la'' la'' \rt#4 la'' |
\rt#4 sol'' \rt#4 sol'' |
\rt#4 sol'' \rt#4 sol'' |
\rt#4 fad'' \rt#4 fad'' |
<>-\sug\ff \rt#4 <sib' sol''> \rt#4 q |
\rt#4 q \rt#4 q |
q2 r\fermata |
R1 |
r8. sib16-\sug\f sib4 r2 |
sol'1_\markup\dynamic mfp ~ |
sol'2 la'!~ |
la'1~ |
la'~ |
la'~ |
la'2 sol'~ |
sol' dod''~ |
dod'' re''~ |
re''2.\fermata la'4-\sug\f |
\rt#4 la'8 \rt#4 la' |
\rt#4 la'8 \rt#4 la' |
\rt#4 <re' la'>8 \rt#4 q |
\rt#4 q \rt#4 q |
\rt#4 <fad' mib''!> \rt#4 <fad' mib''> |
\rt#4 q \rt#4 q |
\rt#4 <sol' re''> \rt#4 q |
\rt#4 q \rt#4 q |
\rt#4 <sib' sol''> \rt#4 q |
q sol'[ sol' sol'] \rt#4 sol' |
la' << { fad''[ fad'' fad''] \rt#4 fad'' | \rt#4 fad'' \rt#4 fad'' | }
  \\ { la'[ la' la'] \rt#4 la' | \rt#4 la'  \rt#4 la' | } >>
sol sib' sib' sib' \rt#4 sib' |
\rt#4 sol' \rt#4 sol' |
la' <<
  { fad''[ fad'' fad''] \rt#4 fad'' |
    \rt#4 fad'' \rt#4 fad'' |
    \custosNote fad''4 } \\
  { la'8[ la' la'] \rt#4 la' |
    \rt#4 la'  \rt#4 la' |
    \custosNote la'4 }
>>
