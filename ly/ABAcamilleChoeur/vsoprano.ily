\clef "vbas-dessus" r4 re'' re'' re'' |
re''2 re''4 re'' |
re''2 re''4 re'' |
mib''1~ |
mib'' |
mib''4 r mib'' mib'' |
mib''2 do''4 do'' |
sib'2. re''4 |
mib''2. mib''4 |
re''2. re''4 |
re''1 |
R1*5 |
r2 sol''4^\f sol'' |
sol''1 |
sol''2. sol''4 |
fad''1 |
fad''4 re'' re'' re'' |
mib''2 mib''4 mib'' |
mib''2 mib''4 mib'' |
mib''1~ |
mib'' |
mib''2 mib''4 mib'' |
mib''2 do''4 do'' |
sib'2. re''4 |
mib''2. mib''4 |
re''2. re''4 |
re''1 |
R1 |
r2\fermata r |
R1*10 |
r2\fermata r4 la' |
re''2. re''4 |
mi''2. mi''4 |
fa''1 |
fa''4 fa'' fa'' fa'' |
mib''!2. mib''4 |
mib'' mib'' mib'' mib'' |
re''1 |
R1 |
sol'' |
sol''2 sol'' |
fad''1 |
r2 fad'' |
sol'' sol'' |
sol'' sol'' |
fad''1~ |
fad''~ |
\custosNote fad''4
