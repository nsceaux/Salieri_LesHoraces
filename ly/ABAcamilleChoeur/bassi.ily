\clef "bass" sol,8 sol sol sol \rt#4 sol |
\rt#4 sol \rt#4 sol |
\rt#4 sol \rt#4 sol |
\rt#4 fad \rt#4 fad |
\rt#4 fad \rt#4 fad |
\rt#4 fad \rt#4 fad |
\rt#4 fad \rt#4 fad |
\rt#4 sol \rt#4 sib |
\rt#4 do' \rt#4 do' |
\rt#4 re' \rt#4 re |
<>\p \rt#4 sol \rt#4 sol |
\rt#4 sol \rt#4 sol |
sol sib[\f sib sib] \rt#4 sib |
mib'2 sib4 sib |
sib2 sib,4 sib |
<>\p \rt#4 mib8 \rt#4 mib |
mib mib'\ff mib' mib' \rt#4 mib' |
\rt#4 mib' \rt#4 mib' |
\rt#4 mib' \rt#4 mib' |
\rt#4 re' \rt#4 re' |
re' re re re \rt#4 re |
\rt#4 do \rt#4 do |
\rt#4 do \rt#4 do |
\rt#4 do \rt#4 do |
\rt#4 do \rt#4 do |
do do' do' do' do'4 la |
\rt#4 fad8 \rt#4 fad |
\rt#4 sol \rt#4 sib |
\rt#4 do' \rt#4 do' |
\rt#4 re' \rt#4 re |
<>\ff \rt#4 sol \rt#4 sol |
\rt#4 sol \rt#4 sol |
sol2 r\fermata |
R1 |
r8. sol16\f sol4 r2 |
fa!1_\markup\dynamic mfp |
mib2 fad~ |
fad1~ |
fad~ |
fad~ |
fad2 sol~ |
sol1~ |
sol2 fa!~ |
fa2.\fermata la4\f |
\rt#4 fa8 \rt#4 fa |
\rt#4 dod \rt#4 dod |
re4 re' la fa |
re re' re' re' |
\rt#4 do'8 \rt#4 do' |
\rt#4 do'8 \rt#4 do' |
\rt#4 sib \rt#4 sib |
\rt#4 sib \rt#4 sib |
\rt#4 mib! \rt#4 mib |
\rt#4 mib \rt#4 mib |
\rt#4 re \rt#4 re |
\rt#4 re \rt#4 re |
\rt#4 mib \rt#4 mib |
\rt#4 mib \rt#4 mib |
\rt#4 re \rt#4 re |
\rt#4 re \rt#4 re |
\custosNote re4
