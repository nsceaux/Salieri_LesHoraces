\clef "vhaute-contre" r4 sib' sib' sib' |
sib'2 sib'4 sib' |
sib'2 sib'4 sib' |
la'1~ |
la' |
la'4 r la' la' |
la'2 la'4 la' |
sol'2. sol'4 |
sol'2. do''4 |
la'2. la'4 |
sib'1 |
R1*5 |
r2 sol'4^\f sol' |
sib'1 |
sol'2. sol'4 |
la'1 |
la'4 re' re' re' |
la'2 la'4 la' |
la'2 la'4 la' |
la'1~ |
la' |
la'2 la'4 la' |
la'2 la'4 la' |
sol'2. sol'4 |
sol'2. do''4 |
la'2. la'4 |
sib'1 |
R1 |
r2\fermata r |
R1*10 |
r2\fermata r4 la' |
la'2. la'4 |
la'2. la'4 |
la'1 |
la'4 la' la' la' |
la'2. la'4 |
la' la' la' la' |
sib'1 |
R1 |
sib' |
sib'2 sol' |
la'1 |
r2 la' |
sol' sib' |
sib' sol' |
la'1~ |
la'~ |
\custosNote la'4
