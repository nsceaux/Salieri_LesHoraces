\clef "tenor" r4 sol'2 sol'4 |
sol'1~ |
sol' |
fad'~ |
fad'~ |
fad'~ |
fad' |
sol'~ |
sol' |
re' |
<>\p sol'~ |
sol' |
sol'4 r r2 |
R1*3 |
r4 mib'-\sug\ff mib' mib' |
mib'1~ |
mib' |
re'~ |
re' |
do' |
do'~ |
do'~ |
do' |
do'2 la |
fad'1 |
sol' |
do' |
re' |
sol'-\sug\ff~ |
sol'~ |
sol'2 r\fermata |
R1*10 |
r2\fermata r4 la-\sug\f |
fa'!2. mi'8 re' |
dod'1 |
re' |
re' |
do'~ |
do' |
sib~ |
sib |
mib'~ |
mib' |
re'~ |
re' |
mib'~ |
mib' |
re'~ |
re' |
\custosNote re'4
