\clef "alto" sol8 sol' sol' sol' \rt#4 sol' |
\rt#4 sol' \rt#4 sol' |
\rt#4 sol' \rt#4 sol' |
do' la' la' la' \rt#4 la' |
\rt#4 la' \rt#4 la' |
\rt#4 la' \rt#4 la' |
\rt#4 la' la' re'' re'' re'' |
\rt#4 re'' \rt#4 sib' |
\rt#4 do'' \rt#4 do'' |
\rt#4 la' \rt#4 la' |
sol' sol-\sug\p sol sol \rt#4 sol |
\rt#4 sol \rt#4 sol |
sol8 sib-\sug\f[ sib sib] \rt#4 sib |
mib' mib' sol' mib' \rt#4 sib |
\rt#4 sib \rt#4 sib |
<>\p \rt#4 sib \rt#4 sib |
sib mib'-\sug\ff mib' mib' \rt#4 mib' |
\rt#4 mib' \rt#4 mib' |
\rt#4 mib' \rt#4 mib' |
re' <fad' la'> q q \rt#4 q |
\rt#4 q \rt#4 q |
\rt#4 q \rt#4 q |
\rt#4 q \rt#4 q |
\rt#4 q \rt#4 q |
\rt#4 q \rt#4 q |
\rt#4 q \rt#4 q |
\rt#4 q \rt#4 <re' la'> |
\rt#4 <re' sib'> \rt#4 sib' |
\rt#4 do'' \rt#4 do'' |
\rt#4 la' \rt#4 la' |
<>-\sug\ff \rt#4 <sib' re'> \rt#4 q |
\rt#4 <sib' re'> \rt#4 q |
q2 r\fermata |
R1 |
r8. sol16-\sug\f sol4 r2 |
re'1_\markup\bracket\dynamic mfp |
mib'2 re'~ |
re'1~ |
re'~ |
re'~ |
re'~ |
re'2 mi'~ |
mi' re'~ |
re'2.\fermata la'4-\sug\f |
\rt#4 fa'8 \rt#4 fa' |
\rt#4 sol' \rt#4 sol' |
\rt#4 fa' \rt#4 fa' |
\rt#4 fa' \rt#4 re' |
\rt#4 <fad' la'> \rt#4 q |
\rt#4 q \rt#4 q |
\rt#4 <sol' sib'> \rt#4 q |
\rt#4 q \rt#4 q |
\rt#4 mib' \rt#4 mib' |
\rt#4 mib' \rt#4 mib' |
\rt#4 re' \rt#4 re' |
\rt#4 re' \rt#4 re' |
\rt#4 mib' \rt#4 mib' |
\rt#4 mib' \rt#4 mib' |
\rt#4 re' \rt#4 re' |
\rt#4 re' \rt#4 re' |
\custosNote re'4
