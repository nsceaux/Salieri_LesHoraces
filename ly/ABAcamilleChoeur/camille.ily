\clef "soprano/treble" R1*11 |
<>^\markup\character Camille re''2 r4 re'' |
sib'2 r |
R1*2 |
r2 r4 mib'' |
sib'2 r |
R1*15 |
r2\fermata <>^\markup\character { \simple #"Récit" Camille } r8 sol' sol' la' |
sib'8. re''16 sib'8 sib'16 sib' sib'4 la'8 sib' |
sol'^! sol' r sol' sol' sol' sol' la' |
si'4.^! si'16 do'' re''!4 si'8 sol' |
do''4^! r8 do'' la'4^! r8 sib' |
re''4 mib'' \grace re'' do'' r8 do'' |
do'' do'' do'' do'' \grace sib'4 la'8 la' r la'16 sib' |
do''4 re''8 mib'' la' la' r la'16 sib' |
do''4 do''8 re'' sib'4^! r8 re''16 re'' |
re''4 re''8 mi'' dod''4^! r8 dod'' |
dod'' dod'' dod'' re'' la'4^! fa'' |
\grace mi''?4 re''2.\fermata r4 |
R1*16 |
