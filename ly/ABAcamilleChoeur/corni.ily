\clef "treble" \transposition sib
r4 <<
  \tag #'(corno1 corni) \new Voice {
    \tag #'corni \voiceOne
    mi''2 mi''4 | mi''1~ | mi''
  }
  \tag #'(corno2 corni) \new Voice {
    \tag #'corni \voiceTwo
    do''2 do''4 | do''1~ | do''
  }
>>
re''1~ |
re''~ |
re''2 re''4 re'' |
re''2 mi''4 mi'' |
<<
  \tag #'(corno1 corni) \new Voice {
    \tag #'corni \voiceOne mi''1 |
  }
  \tag #'(corno2 corni) \new Voice {
    \tag #'corni \voiceTwo do''1 |
  }
>>
re''1 |
mi'' |
<<
  \tag #'(corno1 corni) \new Voice {
    \tag #'corni \voiceOne \tag #'corno1 <>\p mi''1~ | mi'' | mi''4
  }
  \tag #'(corno2 corni) \new Voice {
    \tag #'corni \voiceTwo <>\p do''1~ | do'' | do''4
  }
>> r4 r2 |
R1*3 |
r4 do''-\sug\ff do'' do'' |
do''1~ |
do''4 do''2 do''4 |
mi''1~ |
mi'' |
re'' |
re''~ |
re''~ |
re'' |
re''2 re''4 re'' |
re''2 <<
  \tag #'(corno1 corni) \new Voice {
    \tag #'corni \voiceOne mi''2~ | mi''1 |
  }
  \tag #'(corno2 corni) \new Voice {
    \tag #'corni \voiceTwo mi''2 | do''1 |
  }
>>
re''1 |
mi'' |
<>-\sug\ff <<
  \tag #'(corno1 corni) \new Voice {
    \tag #'corni \voiceOne mi''1~ | mi'' | mi''2
  }
  \tag #'(corno2 corni) \new Voice {
    \tag #'corni \voiceTwo do''1~ | do'' | do''2
  }
>> r2\fermata |
R1*10 |
r2\fermata r4 r |
mi''2-\sug\f r |
R1 |
r4 mi''2 mi''4 |
mi''1 |
re''1~ |
re'' |
do''4 <<
  \tag #'(corno1 corni) \new Voice {
    \tag #'corni \voiceOne mi''2 mi''4~ | mi''4 mi''2 mi''4 |
  }
  \tag #'(corno2 corni) \new Voice {
    \tag #'corni \voiceTwo do''2 do''4~ | do''4 do''2 do''4 |
  }
>>
r2 do''4. do''8 |
do''2 do'' |
mi''4 mi''2 mi''4~ |
mi''4 mi''2 mi''4 |
do''1 |
do''2. do''4 |
mi''4 <<
  \tag #'(corno1 corni) \new Voice {
    \tag #'corni \voiceOne mi''2 mi''4~ | mi''4 mi''2 mi''4 |
    \custosNote mi''
  }
  \tag #'(corno2 corni) \new Voice {
    \tag #'corni \voiceTwo mi'2 mi'4~ | mi'4 mi'2 mi'4 |
    \custosNote mi'
  }
>>
