\tag #'(vsoprano valto vtenor vbasso) {
  Se -- cou -- rez- nous, ô puis -- sante E -- gé -- ri -- e,
  Pro -- tè -- gez de Nu -- ma le peuple in -- for -- tu -- né.
}
\tag #'camille {
  Ciel ! & quoi ?
}
\tag #'vbasso {
  Du com -- bat le si -- gnal est don -- né.
}
\tag #'camille {
  Je meurs !
}
\tag #'(vsoprano valto vtenor) {
  Mal -- heu -- reu -- se pa -- tri -- e !
}
\tag #'(vsoprano valto vtenor vbasso) {
  Se -- cou -- re- nous, ô puis -- sante E -- gé -- ri -- e,
  Pro -- tè -- ge de Nu -- ma le peuple in -- for -- tu -- né.
}
\tag #'camille {
  Et voi -- là donc la foi que l’on doit aux o -- ra -- cles !
  Pour -- quoi d’un faux bon -- heur m’an -- non -- cer les ap -- pas,
  Dieux vains ! trem -- blante, hé -- las ! de -- vant vos ta -- ber -- na -- cles,
  J’au -- rois cru faire un cri -- me en ne vous croy -- ant pas.
  Voi -- là donc le bon -- heur dont je m’é -- tois flat -- tée !
  Hé -- las !
}
\tag #'(vsoprano valto vtenor vbasso) {
  Dé -- es -- se re -- dou -- té -- e,
  Dans ce mo -- ment peut- être on est aux mains
  Veil -- le sur nous, com -- bats pour les Ro -- mains. __
}
