\clef "bass" r4 sol sol sol |
sol2 sol4 sol |
sol2 sol4 sol |
do'1~ |
do' |
do'4 r do' la |
fad2 fad4 fad |
sol2. sib4 |
do'2. do'4 |
re'2. re'4 |
sol1 |
R1 |
r2 sib4 sib |
mib'2 sib4 sib |
sib2 sib4 sib |
sol2 r |
R1*4 |
r4 re'4^\f re' re' |
do'2 do'4 do' |
do'2 do'4 do' |
do'1~ |
do' |
do'2 do'4 la |
fad2 fad4 fad |
sol2. sib4 |
do'2. do'4 |
re'2. re'4 |
sol1 |
R1 |
r2\fermata r |
R1*10 |
r2\fermata r4 la |
fa2. fa4 |
dod2 dod' |
re'1 |
re'4 re' re' re' |
do'!2. do'4 |
do' do' do' do' |
sib1 |
R1 |
mib'! |
mib'2 mib' |
re'1 |
r2 re' |
mib' mib' |
mib' mib' |
re'1~ |
re'~ |
\custosNote re'4
