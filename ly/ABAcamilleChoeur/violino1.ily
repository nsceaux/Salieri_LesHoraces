\clef "treble" sol8 re''' re''' re''' \rt#4 re''' |
\rt#4 re''' \rt#4 re''' |
\rt#4 re''' \rt#4 re''' |
\rt#4 mib''' \rt#4 mib''' |
\rt#4 mib''' \rt#4 mib''' |
\rt#4 mib''' \rt#4 mib''' |
mib''' do''' do''' do''' \rt#4 do''' |
\rt#4 sib'' \rt#4 re''' |
\rt#4 mib''' \rt#4 mib''' |
\rt#4 re''' \rt#4 re''' |
re''' re'\p re' re' \rt#4 re' |
\rt#4 re' \rt#4 re' |
re' sib'[\f sib' sib'] \rt#4 sib' |
mib'' mib'' sol'' mib'' \rt#4 sib' |
sib' sib sib' sib' \rt#4 sib' |
<>\p \rt#4 sol' \rt#4 sol' |
sol' sol''\ff sol'' sol'' \rt#4 sol'' |
sol''4 << sib''2 \\ sib' >> sol''4~ |
sol'' dod'''2 dod'''4 |
\rt#4 re'''8 \rt#4 re''' |
\rt#4 re''' \rt#4 re''' |
mib'''4 mib'''2 mib'''4~ |
mib'''4 mib'''2 mib'''4 |
\rt#4 mib'''8 \rt#4 mib''' |
\rt#4 mib''' \rt#4 mib''' |
\rt#4 mib''' \rt#4 mib''' |
mib''' do''' do''' do''' \rt#4 do''' |
\rt#4 sib'' \rt#4 re''' |
\rt#4 mib''' \rt#4 mib''' |
\rt#4 re''' \rt#4 re''' |
<>\ff \rt#4 re''' \rt#4 re''' |
\rt#4 re''' \rt#4 re''' |
re'''2 r2\fermata |
R1 |
r8. re'16\f re'4 r2 |
si'1_\markup\dynamic mfp |
do''~ |
do''~ |
do''~ |
do''~ |
do''2 sib'~ |
sib'1~ |
sib'2 la'~ |
la'2.\fermata la'4\f |
\rt#4 re''8 \rt#4 re'' |
\rt#4 mi'' \rt#4 mi'' |
\rt#4 fa'' \rt#4 fa'' |
\rt#4 fa'' \rt#4 fa'' |
fad''4 la''2 do'''4~ |
do''' mib'''2 mib'''4 |
re''' re'''2 sol''4~ |
sol'' re''2 sol''4 |
\rt#4 <sib' sol''>8 \rt#4 q |
\rt#4 sib'' \rt#4 sol'' |
fad''4 la''2 re'''4~ |
re''' la''2 fad''4 |
\rt#4 sol''8 \rt#4 sol'' |
\rt#4 sib'' \rt#4 sol'' |
fad''4 la''2 re'''4~ |
re''' la''2 fad''4 |
\custosNote re''4
