\clef "treble"
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sib''4 sib''8. sib''16 sib''8. sib''16 |
    re'''2 sib''8. sol''16 |
    fa''2. | fa'' | sol'' | la''~ | la'' |
    sib''8 sib''16 sib'' sib''8 sib'' sol'' sol'' |
    fa''2 }
  { re''4 re''8. re''16 re''8. re''16 |
    fa''2 re''8. sib'16 |
    do''2. | re'' | mib'' | do''~ | do'' |
    re''8 re''16 re'' re''8 re'' mib'' mib'' |
    re''2 }
  { s2.\f | s2 s4\p | s2.*5 | s2.\ff }
>> r4 |
R2. |
r8 \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''16 mi'' mi''8 mi'' mi'' mi'' |
    sol''2. |
    la''8 fa' la' do'' fa'' la'' | }
  { sol'16 sol' sol'8 sol' sol' sol' |
    do''2. |
    do''8 fa' la' do'' fa'' la'' | }
  { s8 s2 | s2. | s\f }
>>
do'''2\p la'4 |
re'''2-\sug\f do'''4-\sug\p |
sib'' sol'' la''8. fa''16 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''8 sol''16 sol'' sol''8 sol'' la'' la'' | sol''4 }
  { mi''8 mi''16 mi'' mi''8 mi'' fa'' fa'' | mi''4 }
  { s2.-\sug\ff }
>> r4 r\fermata |
%%
fa'1\fp |
sol' |
sol'-\sug\fp |
la'2. la'4 |
sib'( sol' mi' dod') |
re' fa'-\sug\sf( sol' la' |
sib'-\sug\p sol' mi' dod') |
re' fa'(-\sug\f la' re'' |
mib''!-\sug\p re'' do'' sib') |
\once\slurDashed la'( fa'-\sug\cresc la' sib') |
do''2-\sug\f mib' |
re'2. r4 |
R1 |
r2\fermata r |
R1 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re'''2 sib'' | fa'' re'' | sib' }
  { fa''2 re'' | sib' fa' | re' }
  { s1-\sug\ff }
>> sib'4 sib' |
sib'2 r |
