Que des plus no -- bles fleurs leurs tom -- beaux soient cou -- verts !
Que Ro -- me leur con -- sa -- cre un é -- ter -- nel hom -- ma -- ge !
Que leur noms, ré -- vé -- rés en cent cli -- mats di -- vers,
A nos ne -- veux sur -- pris soient ci -- tés d’âge en â -- ge !

Mais leur in -- di -- gne frè -- re, a -- près sa lâ -- che -- té,
Qu’il traîne, a -- vec mé -- pris, sa honte & sa mi -- sè -- re !
Qu’il soit par -- tout er -- rant, & par -- tout re -- bu -- té,
Mau -- dit du monde en -- tier comme il l’est de son pè -- re !
