\clef "bass" R2.*7 |
<>-\sug\ff \rp#3 { sib,16*4/6 sib, sib, sib, sib, sib, } |
sib,4 r r |
R2.*7 |
<>\ff \rp#3 { do16*4/6 do do do do do } |
do4 r r |
R1*19 |
