\clef "vbasse" sib4 sib8. sib16 sib8. sib16 |
re'2 sib8. sol16 |
fa2 fa8. fa16 |
re2 r8 sib |
sol4. mib8 sol sib |
la4 la r8 fa |
mib'4. do'8 la fa |
sib2. |
sib4 r fa8. fa16 |
sib2 re'8. re'16 |
mi2. |
do4 do8. mi?16 sol8. do'16 |
la2 r4 |
do'4 do'8. do'16 la8. fa16 |
re'2 do'8. do'16 |
sib2 la8. fa16 |
do'2. |
do'4 r r\fermata |
%%
fa4 fa8 fa fa4. fa8 |
sol4 do r r8 do |
sol4. sol8 sol4. sol8 |
la2 r4 la |
sib sol mi dod |
re2. la4 |
sib sol mi dod |
re re r re' |
mib' re' do' sib |
la2 la4 sib |
do'2 mib4. mib8 |
re2 r4 r8 fa |
sib4. sib8 sib4 sib |
re'2\fermata mi'4 mi' |
fa'2 fa'4 fa' |
re' sib r2 |
R1*3 |
