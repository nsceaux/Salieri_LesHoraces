\piecePartSpecs
#`((corno1 #:instrument "Corno I mi♭" #:tag-global ())
   (corno2 #:instrument "Corno II mi♭" #:tag-global ())
   (tromba1 #:instrument "Tromba I si♭" #:tag-global ())
   (tromba2 #:instrument "Tromba II si♭" #:tag-global ())
   (oboe1)
   (oboe2)
   (fagotto1)
   (fagotto2)
   (violino1)
   (violino2)
   (alto #:notes "violini" #:tag-notes alto)
   (basso #:instrument "Basso")
   (timpani)
   (silence #:on-the-fly-markup , #{ \markup\tacet#37 #}))
