\clef "treble" \transposition sib
\twoVoices #'(tromba1 tromba2 trombe) <<
  { do''4 do''8. do''16 do''8. do''16 |
    mi''2 mi''8. do''16 |
    re''2. | mi''4 }
  { mi'4 mi'8. mi'16 mi'8. mi'16 |
    sol'2 do''8. do''16 |
    sol'2. | do''4 }
  { s2.-\sug\f | s2 s4-\sug\p | }
>> do''8. do''16 do''8 r |
R2.*3 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { do''8 do''16 do'' do''8 do'' do'' do'' | do''2 }
  { do'8 do'16 do' do'8 do' do' do' | do'2 }
  { s4-\sug\ff }
>> r4 |
R2. |
r8 re''16 re'' re''8 re'' re'' re'' |
re''2. |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { re''8 re'' re'' re'' re'' re'' | re''2. |
    mi''2 re''4 | }
  { re''8 sol' sol' sol' sol' sol' | sol'2. |
    do''2 re''4 | }
  { s2.-\sug\f | s-\sug\p | s2-\sug\f s4-\sug\p }
>>
r8. re''16 re''4 re'' |
re''8-\sug\ff re''16 re'' re''8 re'' re'' re'' |
re''4 r r\fermata |
%%
R1*13 |
r2\fermata r |
R1 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''2 do'' | sol'' mi'' | do'' }
  { sol'2 mi' | mi'' do'' | mi' }
  { s1-\sug\ff }
>> do'4 do' |
do'2 r |

