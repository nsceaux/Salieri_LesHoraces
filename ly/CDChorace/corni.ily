\clef "treble" \transposition mib
\twoVoices #'(corno1 corno2 corni) <<
  { re''4 re''8. re''16 re''8. re''16 |
    re''2 sol''8. mi''16 |
    re''2. |
    re''4 re''8. re''16 re''8. re''16 |
    mi''2. | }
  { sol'4 sol'8. sol'16 sol'8. sol'16 |
    sol'2 sol'4 |
    re''2. |
    sol'4 sol'8. sol'16 sol'8. sol'16 |
    do''2. | }
  { s2.\f | s2 s4\p }
>>
re''8. re''16 re''8. re''16 re''8. re''16 |
re''2.\fp |
\twoVoices #'(corno1 corno2 corni) <<
  { re''8 re''16 re'' re''8 re'' mi'' mi'' | re''2 }
  { sol'8 sol'16 sol' sol'8 sol' do'' do'' | sol'2 }
  { s2.\ff }
>> r4 |
R2.*3 |
r8. re''16\f re''8 re'' re'' re'' |
re''2.\p |
sol''8.-\sug\f re''16 re''2 |
r4 \twoVoices #'(corno1 corno2 corni) <<
  { sol''4 fad'' | mi''4 }
  { mi''4 re'' | mi'' }
  { s2 | s4 }
>> r4 r |
R2.^\fermataMarkup |
%%
re''1\fp |
mi''4 r r2 |
mi''1\fp |
re''4 r r2 |
R1*9 |
r2\fermata r |
re''2-\sug\ff re''4 re'' |
\twoVoices #'(corno1 corno2 corni) <<
  { re''2 re'' | re'' re'' | }
  { sol'2 sol' | sol' sol' | }
>>
sol'2 sol'4 sol' |
sol'2 r |
