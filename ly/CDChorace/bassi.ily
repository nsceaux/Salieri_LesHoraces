\clef "bass" sib,8.\f sib,16 sib,8. sib,16 sib,8. sib,16 |
sib,8. sib,16 sib,8. sib,16 sib,8.\p sib,16 |
la,8. la,16 la,8. la,16 la,8. la,16 |
sib,8. sib,16 sib,8. sib,16 sib,8. sib,16 |
mib8. mib16 mib8. mib16 mib8. mib16 |
fa8. fa16 fa8. fa16 fa8. fa16 |
fa8.\f fa16 fa8.\p fa16 fa8. fa16 |
sib,8\ff sib16 sib sib8 sib sib sib |
sib,8. sib,16 sib,8. sib,16 sib,8.\p sib,16 |
sib,8. sib,16 sib,8. sib,16 sib,8. sib,16 |
do8. do16 do8. do16 do8. do16 |
mi8. mi16 mi8. mi16 mi8. mi16 |
fa4\f fa fa |
la8.\p la16 la8. la16 la8. la16 |
sib8.\f sib,16 sib8. sib16 la8.\p la16 |
sol8. mi16 mi8. mi16 fa8. fa16 |
do8\ff do'16. do'32 do'8 do' do' do' |
do4 r r\fermata |
%%
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    fa1-\sug\fp | sol | sol-\sug\fp | la |
  }
  \tag #'basso {
    fa8\fp fa fa fa \rt#4 fa |
    \rt#4 mi \rt#4 mi |
    <>\fp \rt#4 mi \rt#4 mi |
    fa2 r4 la |
  }
>>
sib( sol mi dod) |
re fa(\sf sol la |
sib\p sol mi dod) |
re fa\f( la re' |
mib'\p re' do' sib) |
la( fa\cresc la sib) |
do'\f mib' la,2 |
sib,2. sib,16\f do32 re mib fa sol la |
sib4. sib,8 sib,4 sib, |
sib,2\fermata <<
  \tag #'(fagotto1 fagotto2 fagotti) {
    r2 |
    R1 |
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { re'2 re' | re' re' | }
      { fa2 fa | fa fa | }
      { s1-\sug\ff }
    >>
  }
  \tag #'basso {
    mi4\ff mi |
    fa2 fa4 fa |
    sib,2\ff sib, |
    sib, sib, |
  }
>>
sib,8 do16 re mib fa sol la sib4 sib, |
<<
  \tag #'(fagotto1 fagotto2 fagotti) { sib,2 r | }
  \tag #'basso { sib,4 sib2 sol4 | }
>>
