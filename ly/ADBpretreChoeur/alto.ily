\clef "alto" r8. sol16[ la8. si16] |
do'8-\sug\fp do'4 do' do' do'8 |
si si4 si si si8 |
sol8 sol4 sol sol sol8 |
sol sol4 sol sol sol8 |
sold-\sug\cresc sold4 sold sold sold8\! |
la-\sug\f la4 la la8 fa'8.[-\sug\mf fa'16] |
re'8 si4 si si re'8 |
sol-\sug\p do'4 do' fa'8[ re' re'] |
mi'8 mi'4 mi'8 mi'8. sol16[-\sug\f la8. si16] |
do'8 do'4 do' do' do'8 |
si si4 si si si8 |
sol8 sol4 sol sol sol8 |
sol sol4 sol sol sol8 |
sold sold4 sold mi' mi'8 |
mi'8 la4 la la8 fa'8.[-\sug\mf fa'16] |
re'8 re'4 re' si re'8 |
sol-\sug\p do'4 do' fa'8 re' re' |
mi' sol'4 mi' do' sol8 |
sol2 r |
<mi' do'>1-\sug\fp |
<fad' la> |
sol'2-\sug\cresc la' |
re'1\!-\sug\f |
sol'2-\sug\p sold' |
la'2.-\sug\f la'4-\sug\p |
sol' la' sol' fad' |
sol' re'2-\sug\f r4 |
R1*3 |
r2 r8. sol16-\sug\f[ la8. si16] |
do'8 do'4 do' do' do'8 |
si si4 si si si8 |
sol8 sol4 sol sol sol8 |
sol sol4 sol sol sol8 |
mi'8 mi'4 mi' mi' mi'8 |
mi' la4 la la8 fa'[-\sug\mf fa'] |
re' re'4 re' si re'8 |
sol8-\sug\p do'4 do' fa'8 re' re' |
mi' sol'4 mi' do' sol8 |
do'8.\f do'16 do'8. do'16 do'8 do'[ do' do'] |
do'4 r r2 |
r r8 sib16-\sug\ff sib sib8 sib |
\rt#16 <mib' mib>32 q8 r r4 |
r2 r8 <<
  { sol'8\p sol' sol' |
    <>\cresc \rt#16 mib'32 \rt#16 mib' |
    <>\!\f \rt#16 mib'32 \rt#16 mib' |
  } \\
  { si8 si si |
    \rt#16 do'32 \rt#16 do'32 |
    \rt#16 do'32 \rt#16 do'32 |
  }
>>
<si sol'>4 r r <>-\sug\p <<
  { re'4 | mib'2 fa'4 mib' | re'4. re'8 re'4 sol' | } \\
  { si4 | do'2 re'4 do' | si4. si8 si4 si | }
>>
\rt#8 <do' mib'>16 \rt#8 q |
\rt#8 <do' mib'>16 \rt#8 q |
\rt#8 <si re'> q8. sol16-\sug\ff[ la8. si16] |
do'8 do'4 do' do' do'8 |
si si4 si si si8 |
sol8 sol4 sol sol sol8 |
sol sol4 sol sol sol8 |
sold sold4 << \modVersion sold4 \origVersion { sold8~ sold } >> mi'4 mi'8 |
mi'8 la4 la la8 fa'[-\sug\mf fa'] |
re' re'4 re' si re'8 |
sol8 do'4 do' fa'8 re' re' |
mi' sol'4 mi' do' sol8 |
sol1 |
