\clef "bass" r2 |
R1*8 |
r2 r8. sol16 la8. si16 |
do'4. do8 do4. do8 |
si,2 r4 r8 sol |
sol4 sol8. sol16 sol4 sol8. sol16 |
sol4 sol r8. sol16 sol8. sol16 |
sold4. sold8 sold4. sold8 |
la4 la r fa8. fa16 |
fa2~ fa4. fa8 |
mi2 fa4 sol |
do1 |
R1*12 |
r2 r8. sol16 la8. si16 |
do'4. do8 do4. do8 |
si,2 r4 r8 sol |
sol4 sol8. sol16 sol4 sol8. sol16 |
sol4 sol r8. sol16 sol8. sol16 |
sold4. sold8 sold4. sold8 |
la4 la r fa8. fa16 |
fa2~ fa4. fa8 |
mi2^\p fa4 sol |
do2 r |
R1*7 |
r2 r8. <<
  { \voiceOne re16 re8. re16 |
    mib4. mib8 fa4. mib8 |
    re2 r4 re8. sol16 |
    mib2~ mib4. mib8 |
    mib2 mib4. do8 |
    si,2 \oneVoice }
  \new Voice \with { autoBeaming = ##f } {
    \voiceTwo si,16 si,8. si,16 |
    do4. do8 re4. do8 |
    si,2 r4 si,8. si,16 |
    do2~ do4. fad8 |
    fad2 fad4. fad8 |
    sol2 }
>> r8. sol16 la8. si16 |
do'4. do8 do4. do8 |
si,2 r4 r8 sol |
sol4 sol8. sol16 sol4 sol8. sol16 |
sol4 sol r8 sol sol8. sol16 |
sold4. sold8 sold4. sold8 |
la4 la r fa8. fa16 |
fa2~ fa4. fa8 |
mi2 fa4 sol |
do1~ |
do |
