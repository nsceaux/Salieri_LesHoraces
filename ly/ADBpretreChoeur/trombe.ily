\clef "treble" r2 |
R1*8 |
r2 r8. sol'16-\sug\f[ sol'8. sol'16] |
<<
  \tag #'(tromba1 trombe) \new Voice {
    \tag #'trombe \voiceOne
    do''1 | sol'~ | sol'4 sol'8. sol'16 sol'4 sol' | sol'1 |
  }
  \tag #'(tromba2 trombe) \new Voice {
    \tag #'trombe \voiceTwo
    do' | sol~ | sol4 sol8. sol16 sol4 sol | sol1 |
  }
>>
mi'1 |
<<
  \tag #'(tromba1 trombe) \new Voice {
    \tag #'trombe \voiceOne mi'2~ mi'4
  }
  \tag #'(tromba2 trombe) \new Voice {
    \tag #'trombe \voiceTwo do'2~ do'4
  }
>> r4 |
R1*2 |
<<
  \tag #'(tromba1 trombe) \new Voice {
    \tag #'trombe \voiceOne
    mi'1~ | mi'2    
  }
  \tag #'(tromba2 trombe) \new Voice {
    \tag #'trombe \voiceTwo |
    do'1~ | do'2
  }
>> r2 |
R1*7 |
r4 re''2-\sug\f r4 |
R1*3 |
r8. sol'16-\sug\f sol'8. sol'16 sol'8. sol'16 sol'8. sol'16 |
<<
  \tag #'(tromba1 trombe) \new Voice {
    \tag #'trombe \voiceOne
    do''1 | sol'4 sol'8. sol'16 sol'4 sol' | sol'1~ | sol' |
  }
  \tag #'(tromba2 trombe) \new Voice {
    \tag #'trombe \voiceTwo
    do'1 | sol4 sol8. sol16 sol4 sol | sol1~ | sol |
  }
>>
mi'1 |
<<
  \tag #'(tromba1 trombe) \new Voice {
    \tag #'trombe \voiceOne
    mi'2~ mi'4
  }
  \tag #'(tromba2 trombe) \new Voice {
    \tag #'trombe \voiceTwo
    do'2~ do'4
  }
>> r4 |
R1*8 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { do''1~ | do'' | sol''2 }
  { do'1~ | do' | sol'2 }
  { s4-\sug\p s2.-\sug\cresc | s1\!-\sug\f }
>> r2 |
R1*4 |
r2 r8. sol'16[\f sol'8. sol'16] |
<<
  \tag #'(tromba1 trombe) \new Voice {
    \tag #'trombe \voiceOne
    do''1 | sol'4 sol'8. sol'16 sol'4 sol' | sol'1~ | sol' |
  }
  \tag #'(tromba2 trombe) \new Voice {
    \tag #'trombe \voiceTwo
    do'1 | sol4 sol8. sol16 sol4 sol | sol1~ | sol |
  }
>>
<< \modVersion mi'1 \origVersion { mi'2~ mi' } >> |
<<
  \tag #'(tromba1 trombe) \new Voice { \tag #'trombe \voiceOne mi'2~ mi'4 }
  \tag #'(tromba2 trombe) \new Voice { \tag #'trombe \voiceTwo do'2~ do'4 }
>> r4 |
R1*2 |
<<
  \tag #'(tromba1 trombe) \new Voice {
    \tag #'trombe \voiceOne
    mi'1~ | mi' |
  }
  \tag #'(tromba2 trombe) \new Voice {
    \tag #'trombe \voiceTwo
    do'1~ | do' |
  }
>>
