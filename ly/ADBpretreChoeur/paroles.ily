\tag #'pretre {
  Puis -- sant mo -- teur de l’u -- ni -- vers,
  O toi dont l’es -- sen -- ce su -- prê -- me
  As -- su -- jet -- tit le Des -- tin mê -- me,
  Que sur nous, tes yeux soient ou -- verts.
}
\tag #'(vsoprano valto vtenor vbasso) {
  Puis -- sant mo -- teur de l’u -- ni -- vers,
  O toi dont l’es -- sen -- ce su -- prê -- me
  As -- su -- jet -- tit le Des -- tin mê -- me,
  Que sur nous, __ tes yeux soient ou -- verts.
}
\tag #'pretre {
  Foi -- bles jou -- ets des des -- ti -- né -- es,
  Que pou -- vons- nous sans ton se -- cours ?
  C’est lui seul qui de nos an -- né -- es
  Ar -- rête ou pro -- lon -- ge le cours.
}
\tag #'(vsoprano valto vtenor) {
  Oui c’est lui seul qui de nos an -- né -- es
  Ar -- rête ou pro -- lon -- ge le cours.
}
\tag #'(vsoprano valto vtenor vbasso) {
  Puis -- sant mo -- teur de l’u -- ni -- vers,
  O toi dont l’es -- sen -- ce su -- prê -- me
  As -- su -- jet -- tit le Des -- tin mê -- me,
  Que sur nous, __ tes yeux soient ou -- verts.
}
\tag #'pretre {
  Les jours tris -- tes, les jours se -- reins,
  La dou -- ce paix, l’af -- freu -- se guer -- re,
  Et la ro -- sé -- e & le ton -- ner -- re,
  Tout part de tes puis -- san -- tes mains.
}
\tag #'(vtenor vbasso) {
  Puis -- sant mo -- teur de l’u -- ni -- vers,
  Que sur nous, __ tes yeux soient ou -- verts.
}
\tag #'(vsoprano valto vtenor vbasso) {
  Puis -- sant mo -- teur de l’u -- ni -- vers,
  O toi dont l’es -- sen -- ce su -- prê -- me
  As -- su -- jet -- tit le Des -- tin mê -- me,
  Que sur nous, __ tes yeux soient ou -- verts.
}
