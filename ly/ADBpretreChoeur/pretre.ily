\clef "bass" r8. sol16 la8. si16 |
do'4. do'8 do'4. do'8 |
sol2 r4 r8 sol |
si4 si8. do'16 re'4 si8 sol |
fa4 fa r8 fa fa8. fa16 |
mi4. mi8 mi'4. re'8 |
do'4 do' r re'8. do'16 |
si2. r8 sol |
do'2 la4. sol8 |
sol2 r |
R1*9 |
r2 do'4 do'8. si16 |
la4. do'8 la4. sol8 |
fad4 fad r8 la la re' |
do'4. dod'8 re'4. la8 |
si2 r4 si8 sol |
mi4 mi8 mi sold4. sold8 |
la4 la r r8 do' |
si[ re'] do'8. la16 sol4 fad8. fad16 |
sol4 r r2 |
R1*12 |
r2 r4 sol8 sol |
do'4 do'8. do'16 mib'4. do'8 |
\grace sib8 lab2 r8 lab lab8. do'16 |
sib4. sib8 sib4. sib8 |
mib'2 mib'8 sol sib sol |
fa4 fa r8 re' si sol |
do'2 do'4 r8 do' |
mib'8. mib'16 mib'8. mib'16 mib'4. do'8 |
sol2 r |
R1*15 |
