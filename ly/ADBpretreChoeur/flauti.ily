\clef "treble" r2 |
R1*8 |
r2 r8. sol''16-\sug\f[ la''8. si''16] |
<<
  \tag #'(flauto1 flauti) \new Voice {
    \tag #'flauti \voiceOne
    do'''1 | sol'' |
    si''4 si''8. do'''16 re'''4 si''8. sol''16 |
    fa''4 re'''2 fa''4 | mi''1 | mi''2~ mi''4
  }
  \tag #'(flauto2 flauti) \new Voice {
    \tag #'flauti \voiceTwo
    mi''1 | re''~ | re'' |
    re''4 fa''2 re''4 | re''1 | do''2~ do''4
  }
>> r4 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { re''1 | do''4 do''' la''4. sol''8 | sol''1~ |
    sol''2 do'''8 mi''' do''' si'' |
    la'' mi''([ mi''' do''']) la'' mi''([ do''' la'']) |
    fad''8 fad''([ la'' fad'']) re''( fad'' la'' re''') |
    do'''8( sol'' do'''_\markup\italic cresc dod''') re'''4. do'''8 |
    si''8-. si''( re''' si'') sol''-. sol''( si'' sol'') |
    r8 sol'-.\p mi''( do'') r8 re''-. si''( sold'') |
    r8 mi''-.\f do'''( la'') mi'''( do''' la'' do'''\p) |
    si''( re''' do''') la''-. sol''( si' la') fad''-. |
    sol''4 re'''2\f re'''4 |
    do'''4 do'''2 do'''4 |
    do'''8( la'' fad'' mi'' red'' fad'' la'' do''') |
    si''\p( mi''' mi'' do''' si'' sol'' la' fad'') |
    sol''2 }
  { sol'1 | do''2 fa''4 re'' | mi''1~ |
    mi''2 r | R1*7 |
    r4 fad''2-\tag #'flauto2 -\sug\f fad''4 |
    sol''4 sol''( la''-\sug\sf sol'') |
    fad''1 |
    sol''4 sol''8.-\sug\p mi''16 re''8( si' do'' la') |
    sol'2 }
  { s1-\sug\mf s1-\sug\p s1*11 <>-\sug\f }
>> r8. sol''16-\sug\f[ la''8. si''16] |
<<
  \tag #'(flauto1 flauti) \new Voice {
    \tag #'flauti \voiceOne
    do'''1 | sol'' | si''4 si''8. do'''16 re'''4 si''8. sol''16 |
    fa''4 re'''2 fa''4 | mi''1~ | mi''2~ mi''4
  }
  \tag #'(flauto2 flauti) \new Voice {
    \tag #'flauti \voiceTwo
    do'''4 mi''2 mi''4 | re''1~ | re'' |
    re''4 fa''2 re''4~ | re''1 | do''2~ do''4
  }
>> r4 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { re''1 | do''4 do''' la''4. sol''8 | sol''2~ sol''4 }
  { si'1 | do''2 fa''4 re'' | mi''2~ mi''4 }
  { s1-\sug\mf s1-\sug\p }
>> r4 |
r2 r8 \twoVoices #'(flauto1 flauto2 flauti) <<
  { sol''8 sol'' sol'' | lab''1 | fa''2~ fa''8. }
  { mib''8 mib'' mib'' | mib''1 | sib''2~ sib''8. }
  { s4.\f | s1\p | }
>> sib''16-\sug\ff[ sib''8. sib''16] |
mib'''8 mib'''4 mib'''8 mib''' \twoVoices #'(flauto1 flauto2 flauti) <<
  { sol''8 sol'' sol'' |
    fa''1 |
    mib''4 do'''2 do'''4 |
    do'''1 |
    si''2 }
  { mib''8 mib'' mib'' |
    re''1 |
    do''4 mib''2 mib''4 |
    mib''2. do''4 |
    re''2 }
  { s4.\p | s1 | s4 s2.\cresc | s1\!-\sug\f }
>> r2 |
R1*2
<<
  \tag #'(flauto1 flauti) \new Voice {
    \tag #'flauti \voiceOne
    r8 do'''\sf( re''' mib''') mib'''( si'' do''') do'''-. |
    r8 do'''-\sug\sf( re''' mib''') mib'''( si'' do''') do'''-. |
    si'' si''([ re''' si'']) sol''
  }
  \tag #'(flauto2 flauti) \new Voice {
    \tag #'flauti \voiceTwo
    R1*2 | r2 r8
  }
>> r16 sol''[\f la''8. si''16] |
<<
  \tag #'(flauto1 flauti) \new Voice {
    \tag #'flauti \voiceOne
    do'''1 |
    sol'' |
    si''4 si''8. do'''16 re'''4 si''8. sol''16 |
    fa''4 re'''2 fa''4 |
    << \modVersion mi''1 \origVersion { mi''2~ mi'' } >> |
    mi''2~ mi''4
  }
  \tag #'(flauto2 flauti) \new Voice {
    \tag #'flauti \voiceTwo
    do'''4 mi''2 mi''4 | re''1 | re''1 | re''4 fa''2 re''4 |
    << \modVersion re''1 \origVersion { re''2~ re'' } >> |
    do''2~ do''4
  }
>> r4 | <>\mf
<<
  \tag #'(flauto1 flauti) \new Voice {
    \tag #'flauti \voiceOne
    re''1 | do''4 do''' la''4. sol''8 | sol''1~ | sol'' |
  }
  \tag #'(flauto2 flauti) \new Voice {
    \tag #'flauti \voiceTwo
    si'1 | do''2 fa''4 re'' | mi''1~ | mi'' |
  }
>>
