\clef "treble" r8. sol'16[ la'8. si'16] |
<sol mi' do''>4.\fp do''8 do''4. do''8 |
sol'8 <sol' sol>4 q q q8 |
<sol re' si'>4 si'8. do''16 re''4 si'8. sol'16 |
fa'8 fa'4 fa' fa' fa'8 |
mi'4.\cresc mi'8 mi''4. sold8\! |
la2.\f re''8.\mf do''16 |
si'8 re''4 sol'' re'' si'8 |
do''4\p( do''' la''4. sol''8) |
sol''2~ sol''8. sol''16\f[ la''8. si''16] |
do'''4. do'''8 do'''4. do'''8 |
sol'' sol''4 sol'' sol'' sol''8 |
<re' si' si''>4 si''8. do'''16 re'''4 si''8. sol''16 |
fa''8 fa''4 fa'' fa' fa'8 |
mi'4. mi''8 mi''4. sold8 |
la2. re'''8.\mf do'''16 |
si''8 sol''4 si'' re''' si''8 |
do'''2\p la''4. sol''8 |
sol'' mi''4 do'' sol' mi'8 |
mi'2 do''8 mi'' do'' si' |
la'\fp mi'([ mi'' do'']) la' mi'([ do'' la']) |
fad' fad'([ la' fad']) re'( fad' la' re'') |
do''8( sol' do''_\markup\italic cresc dod'') re''4. do''8 |
si'8-. si'( re'' si') sol'-. sol'( si' sol') |
r sol-.\p mi'( do') r re'-. si'( sold') |
r8 mi'-.\f do''( la') mi''( do'' la' do''\p) |
si'( re'' do'') la'-. sol'( si la) fad'-. |
sol'4 <la fad' re''>2\f r4 |
R1*3 |
r2 r8. sol''16[\f la''8. si''16] |
do'''4. do'''8 do'''4. do'''8 |
sol'' sol''4 sol'' sol'' sol''8 |
<re' si' si''>4 si''8. do'''16 re'''4 si''8. sol''16 |
fa''8 fa''4 fa'' fa' fa'8 |
mi'4. mi'8 mi''4. sold8 |
la2. re'''8.\mf do'''16 |
si''8 sol''4 si'' re''' si''8 |
do'''2\p la''4. sol''8 |
sol'' mi''4 do'' sol' sol'8 |
<do'' mib' sol>8.\f <mib' sol>16 q8. q16 q do''([ mib'' do''] sol' do'' mib' sol') |
lab'8(\p do'' mib'' lab'') lab'' mib'( lab' do'') |
sib'8. <>\f << { sib''16[ sib''8. sib''16] } \\ { sib'16[ sib'8. sib'16] } >>
\rt#16 sib''32 |
\rt#16 <mib''' mib''> q8 sol''-.[\p sib''-. sol'']-. |
fa''8-. si''16-. si''-. re'''8-. si''-. re''' sol16\p[ sol sol8 sol] |
<>\cresc \rt#24 lab32 lab16 sib32 do' reb' mib' fa' sol'\! |
lab'8.\f <mib' mib''>16 q8. q16 q8. do'16 mib'8. do'16 |
si8( re') sol' sol' r8\p sol'( sol sol') |
lab'-. lab[( lab') lab']-. r lab'( lab lab') |
sol'-. sol( sol') sol'-. r sol'( sol sol') |
mib'8 do''([\sf re'' mib'']) mib''( si' do'') do''-. |
r8 do''(\sf re'' mib'') mib''( si' do'') do''-. |
re'' sol''[( re'' si')] sol'8. sol''16\ff[ la''8. si''16] |
do'''4. do'''8 do'''4. do'''8 |
sol'' sol''4 sol'' sol'' sol''8 |
<re' si' si''>4 si''8. do'''16 re'''4 si''8. sol''16 |
fa''8 fa''4 fa'' fa' fa'8 |
mi'4. mi'8 mi''4. sold8 |
la2. re'''8.\mf do'''16 |
si''8 sol''4 si'' re''' si''8 |
do'''2 la''4. sol''8 |
sol'' mi''4 do'' sol' mi'8 |
mi'1 |
