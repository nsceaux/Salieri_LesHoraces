\clef "treble" r8. sol'16[ la'8. si'16] |
<<
  { mi'8 mi'4 mi' mi' mi'8 | re'8 re'4 re' re' re'8 |
    re'8 re'4 re' re' re'8 |
    re' re'4 re' re' re'8 |
    re'-\sug\cresc re'4 re' re' re'8\! | } \\
  { sol1-\sug\fp | sol | si | si | si | } 
>>
do'8-\sug\f <do' mi'>4 q q8 la'8.[-\sug\mf la'16] |
sol'8 sol'4 sol' sol' sol'8 |
sol'-\sug\p sol'4 do'' do'' si'8 |
do''8 sol'4 sol'8 sol'8. sol'16[-\sug\f la'8. si'16] |
do''8 <<
  { mi''4 mi'' mi'' mi''8 |
    mi'' re''4 re'' re'' re''8 |
    re'' } \\
  { sol'4 sol' sol' sol'8 |
    sol'1 | sol'8 }
>> <re' si'>4 q q q8 |
q q4 q <si re'>4 q8 |
<< { re'8 re'4 re' re' re'8 | } \\ { si1 | } >>
do'8 <do' mi'>4 q q8 la'8.[-\sug\mf la'16] |
sol'8 sol4 sol sol' sol'8 |
sol'-\sug\p sol'4 do'' do'' si'8 |
do'' do''4 sol' mi' do'8 |
do'2 r |
r16 la(-\sug\fp do' mi' la' mi' do' la) r la(\p do' mi' la' mi' do' la) |
r la re' fad' la' fad' re' la r la re' fad' la' fad' re' la |
r16 sol-\sug\cresc do' mi' sol' mi' dod' sol r la re' fad' la' fad' re' la\! |
r si-\sug\f re' sol' si' sol' re' si r sol si re' sol' re' si sol |
r <do' mi'>-\sug\p q q \rt#4 q r <<
  { mi' mi' mi' \rt#4 mi' } \\ { re'16 re' re' \rt#4 re' }
>> |
r16 <do' mi'>-\sug\f q q \rt#4 q \rt#4 q <>-\sug\p << \rt#4 re' \\ \rt#4 do' >> |
r sol' re' si r la do' do' r si re' sol' fad' re' do' la |
si4 <la fad' re''>2\f r4 |
R1*3 |
r2 r8. sol'16-\sug\f[ la'8. si'16] |
do''8 <<
  { mi''4 mi'' mi'' mi''8 | re'' re''4 re'' re'' re''8 | re''8 } \\
  { sol'4 sol' sol' sol'8 | sol'1 | sol'8 }
>> <re' si'>4 q q q8 |
q q4 q <re' si> q8 |
<< { re'8 re'4 re' re' re'8 } \\ si1 >> |
do'8 <do' mi'>4 q q8 la'[\mf la'] |
sol' si4 sol' sol' sol'8 |
sol'8\p sol'4 do'' do'' si'8 |
do'' do''4 sol' mi' mi'8 |
<do'' mib' sol>8.\f <mib' sol>16 q8. q16 q8 q[ q q] |
<lab mib'>4 r r2 |
r8. <sib' sib''>16-\sug\f q8. q16 \rt#16 q32 |
\rt#16 <mib' mib''>32 q8 r r4 |
re'8-\sug\p re' si re' si sol16\p[ sol sol8 sol] |
<>\cresc \rt#24 lab32 lab16 sib32 do' reb' mib' fa' sol'\! |
lab'8.\f <mib' mib''>16 q8. q16 q8. do'16 mib'8. do'16 |
si8( re') sol' sol' sol-\sug\p sol'([ sol sol']) |
lab'-. lab[( lab') lab']-. r lab'( lab lab') |
sol'-. sol( sol') sol'-. r sol'( sol sol') |
\rt#8 fad'16 \rt#8 fad' |
\rt#8 fad'16 \rt#8 fad' |
\rt#8 sol' sol'8. sol'16\ff[ la'8. si'16] |
<<
  { do''8 mi''4 mi'' mi'' mi''8 |
    re'' re''4 re'' re'' re''8 | } \\
  { mi'8 sol'4 sol' sol' sol'8 |
    sol' sol'4 sol' sol' sol'8 | }
>>
<sol' re''>8 <re' si'>4 q q q8 |
q q4 q <si re'> q8 |
q q4 << \modVersion q4 \origVersion { q8~ q } >> q4 q8 |
do'8 <do' mi'>4 q q8 la'[-\sug\mf la'] |
sol' si4 sol' sol' sol'8 |
sol'8 sol'4 do'' do'' si'8 |
do'' do''4 sol' mi' do'8 |
do'1 |
