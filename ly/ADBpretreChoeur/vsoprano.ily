\clef "vbas-dessus" r2 |
R1*8 |
r2 r8. sol'16 la'8. si'16 |
do''4. do''8 do''4. do''8 |
sol'2 r4 r8 sol' |
si'4 si'8. do''16 re''4 si'8. sol'16 |
fa'4 fa' r8. fa'16 fa'8. fa'16 |
mi'4. mi'8 mi''4. re''8 |
do''4 do'' r re''8. do''16 |
si'2~ si'4. sol'8 |
do''2 la'4. sol'8 |
sol'1 |
R1*8 |
r4\f re''2 re''8^\p re'' |
do''4 do''8 do'' do''4. do''8 |
do''4 do'' r r8 do'' |
si'4 mi''8.^\p do''16 si'8[ re''] do'' la' |
sol'2 r8. sol'16 la'8. si'16 |
do''4. do''8 do''4. do''8 |
sol'2 r4 r8 sol' |
si'4 si'8. do''16 re''4 si'8. sol'16 |
fa'4 fa' r8. fa'16 fa'8. fa'16 |
mi'4. mi'8 mi''4. re''8 |
do''4 do'' r re''8. do''16 |
si'2~ si'4. sol'8 |
do''2^\p la'4. sol'8 |
sol'2 r |
R1*12 |
r2 r8. sol'16 la'8. si'16 |
do''4. do''8 do''4. do''8 |
sol'2 r4 r8 sol' |
si'4 si'8. do''16 re''4 si'8. sol'16 |
fa'4 fa' r8 fa' fa'8. fa'16 |
mi'4. mi'8 mi''4. re''8 |
do''4 do'' r re''8. do''16 |
si'2~ si'4. sol'8 |
do''2 la'4. sol'8 |
sol'1~ |
sol' |
