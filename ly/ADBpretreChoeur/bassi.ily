\clef "bass" r2 |
do4\fp do do do |
si, si, si, si, |
sol, sol, sol, sol, |
sol, sol, sol, sol, |
sold, sold\cresc sold sold\! |
la\f la la fa\mf~ |
fa fa fa fa |
mi\p mi fa sol |
do do do8. sol,16\f[ la,8. si,16] |
do4 do do do |
si, si, si, si, |
sol, sol sol sol |
sol sol sol sol |
sold sold sold sold |
la la la fa\mf |
fa fa fa fa |
mi\p mi fa sol |
do do do do |
do2 r |
do\fp do |
re re |
mi2(\cresc fad) |
sol\!\f sol, |
do4\p r si, r |
la,2.\f fad4\p |
sol do re re, |
sol, re,2\f r4 |
R1*3 |
r2 r8. sol,16[\f la,8. si,16] |
do4 do do do |
si, si, si, si, |
sol, sol, sol, sol, |
sol, sol, sol, sol, |
sold, sold sold sold |
la la la fa\mf |
fa fa fa fa |
mi\p mi fa sol |
do do do do |
do8.\f do16 do8. do16 do8 do[ do do] |
do4 r r2 |
r r8 <>\ff <<
  { sib16 sib sib8 sib | \rt#16 mib'32 mib'8 } \\
  { sib,16 sib, sib,8 sib, | \rt#8 mib16 mib8 }
>> r8 r4 |
r2 r16 sol,\p sol, sol, sol, sol, sol, sol, |
<>\cresc \rt#16 lab,32 \rt#16 lab, |
<>\f \rt#16 lab, \rt#16 lab, |
\rt#16 sol, <>\p \rt#16 sol, |
\rt#16 sol, \rt#16 sol, |
\rt#16 sol, \rt#16 sol, |
\rt#16 lab, \rt#16 lab, |
\rt#16 lab, \rt#16 lab, |
\rt#16 sol, sol,8. sol,16[\ff la,8. si,16] |
do4 do do do |
si, si, si, si, |
sol,4 sol sol sol |
sol sol sol sol |
sold sold sold sold |
la la la fa\mf |
fa fa fa fa |
mi mi fa sol |
do do do do |
do1 |
