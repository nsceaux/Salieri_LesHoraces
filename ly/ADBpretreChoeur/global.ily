\midiTempo#92
\tempo "Andante maestoso" \tag #'all \key do \major
\time 4/4 \partial 2 s2 s1*40 \bar "||"
\beginMark "Minore" \tag #'all \key do \minor s1*13 \bar "||"
\beginMark "Majore" \tag #'all \key do \major s1*10 \bar "|."