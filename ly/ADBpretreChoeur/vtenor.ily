\clef "vtaille" r2 |
R1*8 |
r2 r8. sol16 la8. si16 |
do'4. sol8 sol4. sol8 |
sol2 r4 r8 si |
si4 si8. si16 si4 si8. si16 |
si4 si r8. si16 si8. si16 |
si4. si8 si4. si8 |
la4 la r la8. la16 |
re'2~ re'4. re'8 |
do'2 do'4 si |
do'1 |
R1*8 |
r4\f re'2 re'8^\p re' |
mi'4 mi'8 mi' fad'4 mi' |
red' red' r r8 red' |
mi'4 do'8.^\p do'16 re'4 re'8 re' |
sol2 r8. sol16 la8. si16 |
do'4. sol8 sol4. sol8 |
sol2 r4 r8 si |
si4 si8. si16 si4 si8. si16 |
si4 si r8. si16 si8. si16 |
si4. si8 si4. si8 |
la4 la r la8. la16 |
re'2~ re'4. re'8 |
do'2^\p do'4 si |
do'2 r |
R1*7 |
r2 r8.^\markup\italic { Les vieillards } sol16 sol8. sol16 |
lab4. lab8 lab4. lab8 |
sol2 r4 sol8. sol16 |
fad2~ fad4. do'8 |
do'2 do'4. do'8 |
si2 r8. sol16 la8. si16 |
do'4. sol8 sol4. sol8 |
sol2 r4 r8 sol |
si4 si8. si16 si4 si8. si16 |
si4 si r8 si si8. si16 |
si4. si8 si4. si8 |
la4 la r la8. la16 |
re'2~ re'4. re'8 |
do'2 do'4 si |
do'1~ |
do' |
