\clef "vhaute-contre" r2 |
R1*8 |
r2 r8. sol16 la8. si16 |
do'4. mi'8 mi'4. mi'8 |
re'2 r4 r8 re' |
re'4 re'8. re'16 re'4 re'8. re'16 |
re'4 re' r8. re'16 re'8. re'16 |
re'4. re'8 si4. mi'8 |
mi'4 mi' r la'8. la'16 |
sol'2~ sol'4. sol'8 |
sol'2 fa'4 re' |
mi'1 |
R1*8 |
r4\f fad'2 fad'8^\p fad' |
sol'4 sol'8 sol' la'4 sol' |
fad'4 fad' r r8 fad' |
sol'4 sol'8.^\p mi'16 re'8[ si] la fad |
si2 r8. sol16 la8. si16 |
do'4. mi'8 mi'4. mi'8 |
re'2 r4 r8 re' |
re'4 re'8. re'16 re'4 re'8. re'16 |
re'4 re' r8. re'16 re'8. re'16 |
re'4. re'8 si4. mi'8 |
mi'4 mi' r la'8. la'16 |
sol'2~ sol'4. sol'8 |
sol'2^\p fa'4 re' |
mi'2 r |
R1*12 |
r2 r8. sol16 la8. si16 |
do'4. mi'8 mi'4. mi'8 |
re'2 r4 r8 re' |
re'4 re'8. re'16 re'4 re'8. re'16 |
re'4 re' r8 re' re'8. re'16 |
re'4. re'8 si4. mi'8 |
mi'4 mi' r la'8. la'16 |
sol'2~ sol'4. sol'8 |
sol'2 fa'4 re' |
mi'1~ |
mi' |
