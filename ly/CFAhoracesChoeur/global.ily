\tag #'all \key do \major
\tempo "Mouvement de marche" \midiTempo#120
\time 4/4 s1*14
\bar ".|:" s1*15 \alternatives s1 { \midiTempo#80 s1 }
s1*3 \midiTempo#120 \tempo "Piu allegro" s1*9
\midiTempo#80 s1*11 \bar "|."
