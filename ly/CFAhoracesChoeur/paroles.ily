\tag #'vchoeur {
  Du vain -- queur cé -- lé -- brons la gloi -- re,
  Du vain -- queur cé -- lé -- brons la gloi -- re,
  Por -- tons son nom jus -- ques aux Cieux.
  Por -- tons son nom jus -- ques aux Cieux.
  Fai -- sons re -- ten -- tir en tout lieux
  Et sa va -- leur et sa vic -- toi -- re.
  Du vain -- queur cé -- lé -- brons la gloi -- re,
  Du vain -- queur cé -- lé -- brons la gloi -- re,
  Por -- tons son nom jus -- ques aux Cieux.
  Por -- tons son nom son nom jus -- ques aux Cieux.
  Fai -  Cieux.
}
\tag #'vhorace {
  Mon cher fils !
}
\tag #'jhorace {
  Heu -- reu -- se vic -- toi -- re !
  J’en re -- çois dans vos bras un prix bien glo -- ri -- eux.
}
\tag #'vchoeur {
  Du vain -- queur cé -- lé -- brons la gloi -- re,
  Por -- tons son nom jus -- ques aux Cieux.
}
\tag #'jhorace {
  Peu con -- tent des hon -- neurs dont sa bon -- té m’ac -- ca -- ble
  Le roi, chez vous, veut lui- mê -- me ve -- nir ;
  Et peut- être au -- jour -- d’hui…
}
\tag #'vhorace {
  Je cours le pré -- ve -- nir :
  Ja -- mais à ses su -- jets un roi n’est re -- de -- va -- ble ;
  Et l’on est trop pay -- é d’a -- voir pu le ser -- vir.
}
