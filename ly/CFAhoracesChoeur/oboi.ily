\clef "treble" sol'!4 sol'8. sol'16 sol'4 sol' |
sol'1 |
r4 sol'8. sol'16 si'4 si' |
re''2. sol'8. sol'16 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''4 do''8. do''16 do''4. mi''8 |
    mi''4. re''8 do''4 re'' |
    mi'' mi''8. mi''16 mi''4. sol''8 |
    sol''4. fa''8 mi''4 }
  { mi'4 mi'8. mi'16 mi'4. do''8 |
    do''4. sol'8 mi'4 sol' |
    do'' do''8. do''16 do''4. mi''8 |
    mi''4. re''8 do''4 }
>> r4 |
r \twoVoices #'(oboe1 oboe2 oboi) <<
  { do''4 fa'' la'' |
    sol''2 fa''4. sol''8 |
    mi''4 mi''2 fad''4 |
    sol''1 |
    sol''4 si''8. si''16 la''4 la'' |
    sol''4 }
  { do''4 la' fa' |
    mi'2 do''4 re'' |
    do'' do''2 la'4 |
    la'1 |
    si'4 sol''8. sol''16 fad''4 fad'' |
    sol'' }
>> r4 r sol' |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4 re''8. re''16 mi''4 mi''8. mi''16 |
    re''2 sol'8 re'' re'' re'' |
    fad''1 |
    sol''8 sol' sol' sol' }
  { si'4 si'8. si'16 do''4 do''8. do''16 |
    si'2 sol'8 si' si' si' |
    la'1 |
    si'8 sol' sol' sol' }
>> sol'4 sol'8.\p sol'16 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''4 do''8. do''16 do''4. mi''8 |
    mi''4. re''8 do''4 re'' |
    mi''4 mi''8. mi''16 mi''4. sol''8 |
    sol''4. fa''8 mi''4 }
  { mi'4 mi'8. mi'16 mi'4. do''8 |
    do''4. sol'8 mi'4 sol' |
    do'' do''8. do''16 do''4. mi''8 |
    mi''4. re''8 do''4 }
>> r4 | <>\f
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do'4 mi'' mi'' mi'' |
    re''1 |
    sol''2 sol''4. sol''8 |
    mi''4 mi'' mi'' mi'' |
    fa''2. la''4 |
    sol''8 do'''16 do''' do'''8 do''' \rt#4 do''' |
    si''2 si''4. si''8 |
    do'''4 }
  { do'4 do'' do'' do'' |
    do''1 |
    si'2 si'4. si'8 |
    do''4 sol' sol' sol' |
    fa'2. fa''4 |
    mi''8 mi''16 mi'' mi''8 mi'' \rt#4 mi'' |
    re''2 re''4. re''8 |
    do''4 }
>> do'8. do'16 do'4 sol' |
%%
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do'''4 }
  { do'' }
>> r r2 |
R1*3 |
r4 la'8. la'16 la'4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { do''8. re''16 |
    mi''2. fad''4 |
    sol''2 si'' |
    re'''4 re''2 re''4 |
    mi'' mi'' mi'' mi'' |
    re''8 re''16 re'' re''8 re'' \rt#4 re'' |
    sol''2 si'' |
    do'''1~ | do'''~ | do'''2 }
  { la'8. si'16 |
    do''2. la'4 |
    si'2 re'' |
    si'4 si'2 si'4 |
    do'' do'' do'' do'' |
    do''8 do''16 do'' do''8 do'' \rt#4 do'' |
    si'2 re'' |
    mi''1~ | mi''~ | mi''2 }
>> r2 |
R1*10 |
