\clef "vtaille" R1*3 |
r2 r4 sol8. sol16 |
mi4 mi8. mi16 mi4. do'8 |
do'4.( sol8) mi4 sol8 sol |
do'4 do'8 do' do'4. mi'8 |
mi'4.( re'8) do'4 r |
r do' do' do' |
do'2 do'4 do'8 do' |
do'4 do' mi' re' |
re'1 |
re'2 re'4. re'8 |
si2 r4 sol |
si si8 si do'4 do'8 do' |
si2 r8 si si si |
re'4. re'8 re'4. re'8 |
re'2 re'8 r sol8.^\p sol16 |
mi4 mi8. mi16 mi4. do'8 |
do'4.( sol8) mi4 sol8. sol16 |
do'4 do'8. do'16 do'4. mi'8 |
mi'4.( re'8) do'4 r |
r do' do' do' |
do'1 |
si2 si4. si8 |
do'4 do' do' do' |
do'2. do'4 |
do'1 |
si2 si4. si8 |
do'2 r4 sol |
do'2 r |
R1*3 |
r2 r4 la8. si16 |
do'4 do'8 do' do'4 la |
re'1 |
re'4 r r2 |
r4 mi' mi' do' |
do'1 |
si2 re'4. re'8 |
mi'2 r |
R1*12 |
