\clef "vhaute-contre" R1*3 |
r2 r4 sol8. sol16 |
do'4 do'8. do'16 do'4. mi'8 |
mi'4.( re'8) do'4 re'8 re' |
mi'4 mi'8 mi' mi'4. sol'8 |
sol'4.( fa'8) mi'4 r |
r do' la' fa' |
mi'2 la'4 sol'8 sol' |
sol'4 sol' sol' la' |
sol'1 |
re'2 re'4. re'8 |
sol'2 r4 sol' |
sol' sol'8 sol' sol'4 sol'8 sol' |
sol'2 r8 sol' sol' sol' |
la'4. sol'8 fad'4. fad'8 |
sol'2 sol'8 r sol8.^\p sol16 |
do'4 do'8. do'16 do'4. mi'8 |
mi'4.( re'8) do'4 re'8. re'16 |
mi'4 mi'8. mi'16 mi'4. sol'8 |
sol'4.( fa'8) mi'4 r |
r sol' sol' sol' |
la'1 |
sol'2 sol'4. sol'8 |
sol'4 sol' sol' sol' |
fa'2. fa'4 |
mi'1 |
re'2 re'4. re'8 |
do'2 r4 sol |
do'2 r |
R1*3 |
r2 r4 do'8. re'16 |
mi'4 la'8 la' la'4 do'' |
si'1 |
si'4 r r2 |
r4 sol' sol' sol' |
la'1 |
sol'2 si'4. si'8 |
do''2 r |
R1*12 |
