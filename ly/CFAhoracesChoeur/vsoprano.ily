\clef "vbas-dessus" R1*3 |
r2 r4 sol'8. sol'16 |
do''4 do''8. do''16 do''4. mi''8 |
mi''4.( re''8) do''4 re''8 re'' |
mi''4 mi''8 mi'' mi''4. sol''8 |
sol''4.( fa''8) mi''4 r |
r do'' fa'' la'' |
sol''2 fa''4 fa''8 sol'' |
mi''4 mi'' mi'' fad'' |
sol''1 |
re''2 re''4. re''8 |
sol'2 r4 sol' |
re''4 re''8 re'' mi''4 mi''8 mi'' |
re''2 r8 re'' re'' re'' |
fad''4. sol''8 la''4. do''8 |
si'2 si'8 r sol'8.^\p sol'16 |
do''4 do''8. do''16 do''4. mi''8 |
mi''4.( re''8) do''4 re''8. re''16 |
mi''4 mi''8. mi''16 mi''4. sol''8 |
sol''4.( fa''8) mi''4 r |
r mi'' mi'' mi'' |
re''1 |
sol''2 sol''4. sol''8 |
mi''4 mi'' mi'' mi'' |
fa''2. la''4 |
sol''1 |
sol''2 sol''4. sol''8 |
do''2 r4 sol'4 |
do''2 r |
R1*3 |
r2 r4 do''8. re''16 |
mi''4 mi''8 mi'' mi''4 fad'' |
sol''1 |
sol''4 r r2 |
r4 mi'' mi'' mi'' |
re''1 |
sol''2 sol''4. sol''8 |
mi''2 r |
R1*12 |
