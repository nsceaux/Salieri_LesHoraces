\clef "treble" <sol sol'!>4 <sol sol'>8. q16 q4 q |
q2. sol16 la32 si do' re' mi' fad' |
q4 q8. q16 si'4 si' |
re''2. sol'8. sol'16 |
<sol mi' do''>4 <<
  \tag #'violino1 {
    do''8. do''16 do''4. mi''8 |
    mi''4. re''8 do''4 re'' |
    <sol mi' do'' mi''>4 mi''8. mi''16 mi''4. sol''8 |
    sol''4.( fa''8) mi'' do''-.[ sol'-. mi'-.] |
    do'4 <sol mi' do''> <la' fa''> <fa' do'' la''> |
    <mi' do'' sol''>4 mi''8. sol''16 fa''4 fa''8. sol''16 |
    mi''4
  }
  \tag #'violino2 {
    mi'8. mi'16 mi'4. do''8 |
    do''4. sol'8 mi'4 sol' |
    <sol mi' do''>4 do''8. do''16 do''4. mi''8 |
    mi''4.( re''8) do'' do''-.[ sol'-. mi'-.] |
    do'16 do'' si' do'' re'' do'' si' do'' la' do'' si' do'' fa' do'' si' do'' |
    mi' do'' si' do'' re'' do'' si' do'' do'' la' do'' la' re'' sol' re'' sol' |
    do''4
  }
>> <sol mi' do'' mi''>4 q <re' la' fad''> |
sol''16 re'' mi'' fad'' sol'' la'' si'' do''' re'''8 re''' re''' re''' |
re'''16 si'' sol'' re'' re''' si'' sol'' re'' re''' la'' fad'' re'' re''' la'' fad'' re'' |
sol''4 sol16 sol sol sol sol4 <sol sol'> |
<<
  \tag #'violino1 {
    <<
      { re''4 re''8. re''16 mi''4 mi''8. mi''16 | } \\
      { sol'4 sol'8. sol'16 do''4 do''8. do''16 | }
    >>
    <sol' re''>16
  }
  \tag #'violino2 {
    <re' si'>4 q8. q16 <mi' do''>4 q8. q16 |
    <re' si'>16
  }
>> sol''16 fad'' sol'' la'' sol'' fad'' sol'' sol'8 <<
  \tag #'violino1 { re''8 re'' re'' }
  \tag #'violino2 { si'8 si' si' }
>> <re' la' fad''>4 fad''16 sol'' la'' si'' do'''8 <<
  \tag #'violino1 { do'8 do' do' | si }
  \tag #'violino2 { la8 la la | sol }
>> <re' si' sol''>8[ q q] q4 r |
R1 |
r4 sol16\p sol sol sol do'4 r |
R1 |
r4 sol16 sol sol sol do'4 r |
r16 <>\ff <<
  \tag #'violino1 {
    sol''16 la'' si'' do''' si'' re''' do''' mi''' re''' do''' si'' la'' sol'' fa'' mi'' |
    re''8 mi''16 fa'' sol'' la'' si'' do''' re''' mi''' fa''' mi''' re''' do''' si'' la'' |
    sol''4 r <re' si' si''> r |
    r16 sol'' la'' si'' do''' si'' re''' do''' mi''' re''' do''' si'' la'' sol'' fa'' mi'' |
    fa'' fa' sol' la'
  }
  \tag #'violino2 {
    sol' la' si' do'' si' re'' do'' mi'' re'' do'' si' la' sol' fa' mi' |
    re'8 mi'16 fa' sol' la' si' do'' re'' mi'' fa'' mi'' re'' do'' si' la' |
    <<
      { sol'16 sol'' sol'' sol'' \rt#4 sol'' \rt#8 sol'' | } \\
      { s16 si' si' si' \rt#4 si' \rt#8 si' | }
    >>
    mi''16 sol' la' si' do'' si' re'' do'' mi'' re'' do'' si' la' sol' fa' mi' |
    fa'8 sol'16 la'
  }
>> sib'16 do'' re'' mi'' fa'' mi'' fa'' sol'' la''8 la'' |
sol''4 do'''16 sol'' mi'' sol'' do''' sol'' mi'' sol'' do''' sol'' mi'' do'' |
si'4 <si' si''>16 q q q \rt#8 q |
<do'' do'''>4 do'8. do'16 do'4 <sol sol'>4 |
%%%
<do'' do'''>4 r r r8 <<
  \tag #'violino1 {
    do'''8 |
    do'''1\fp~ |
    do''' |
    si'' |
    do'''4
  }
  \tag #'violino2 {
    sol''8 |
    mi''1-\sug\fp~ |
    mi'' |
    <mi'' si'> |
    mi''4
  }
>> la8. la16 la4 <<
  \tag #'violino1 {
    do''8. re''16 | << mi''4 \\ do'' >>
  }
  \tag #'violino2 {
    la'8. si'16 do''4
  }
>> <<
  { mi''8. mi''16 mi''4 } \\ { do''8. do''16 do''4 }
>> <re' la' fad''>4 |
<re' si' sol''>4 <<
  { sol''8. sol''16 si''4 si'' | } \\
  { si'8. si'16 re''4 re'' | }
>>
<re'' re'''>4 <<
  \tag #'violino1 {
    si''16 do''' si'' do''' re''' do''' si'' la'' sol'' fa'' mi'' re'' |
    mi''4 <sol mi' do'' mi''>4 q q |
    re''8 re''16 re'' re''8 re'' \rt#4 re'' |
  }
  \tag #'violino2 {
    \rt#8 <re' si'>16 \rt#4 q |
    <mi' do''>4 <sol mi' do''> q q |
    << { do''8 do''16 do'' do''8 do'' \rt#4 do'' | } \\ fa'1 >>
  }
>>
<re' si' sol''>2 << { sol''4. sol''8 } \\ { si'4. si'8 } >> |
<<
  \tag #'violino1 {
    mi''8. do'''16 do'''8. sol''16 sol''8. mi''16 mi''8. do''16 |
    do''8. mi''16 mi''8. do''16 do''8. sol'16 sol'8. mi'16 |
    do'2
  }
  \tag #'violino2 {
    <<
      { mi''8. mi''16 mi''8. mi''16 mi''8. do''16 do''8. mi'16 |
        mi'8. do''16 do''8. mi'16 mi'8. mi'16 mi'8. mi'16 |
      } \\
      { do''8. do''16 do''8. do''16 do''8. mi'16 mi'8. sol16 |
        sol8. mi'16 mi'8. sol16 sol8. sol16 sol8. sol16 | }
    >>
    <mi' sol>2
  }
>> r2 |
R1 |
<<
  \tag #'violino1 { mi''1~ | mi'' | re'' | }
  \tag #'violino2 { la'1~ | la' | la' | }
>>
r4 <re' la' fad''> r2 |
r4 <re' si' sol''> <re' si' si''> r |
R1 |
r16. <<
  \tag #'violino1 {
    <<
      { sol''32 sol''16. sol''32 sol''4 } \\
      { si'32 si'16. si'32 si'4 }
    >>
  }
  \tag #'violino2 {
    <si' re'>32 q16. q32 q4
  }
>> r2 |
R1 |
r4 << { <re'' si''>4 <do''' mi''> } \\ { sol' sol' } >> r |
