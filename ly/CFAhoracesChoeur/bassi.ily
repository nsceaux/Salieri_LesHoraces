\clef "bass" sol!4 sol8. sol16 sol4 sol |
sol2. sol,16 la,32 si, do re mi fad |
sol4 sol8. sol16 si4 si |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    \clef "tenor" re'2. sol8. sol16 |
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { do'4 do'8. do'16 do'4. mi'8 |
        mi'4. re'8 do'4 re' |
        mi'4 mi'8. mi'16 mi'4. sol'8 |
        sol'4. fa'8 mi'4 }
      { mi4 mi8. mi16 mi4. do'8 |
        do'4. sol8 mi4 sol |
        do'4 do'8. do'16 do'4. mi'8 |
        mi'4. re'8 do'4 }
    >> r4 |
    \clef "bass" do,4 do' la fa |
    mi4 do8. mi16 la,4 si, |
    do do' do' do' |
    si1 |
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { si2 la | }
      { sol2 fad | }
    >>
  }
  \tag #'basso {
    re'2. sol4 |
    do2 r |
    r4 sol, do sol, |
    do,2 r |
    r4 sol, do8 do'[ sol mi] |
    do do' do' do' \rt#4 do' |
    \rt#4 do' la8 la si si |
    \rt#4 do' \rt#4 do' |
    \rt#4 si si re' si sol |
    \rt#4 re \rt#4 re |
  }
>>
sol4 sol, sol, r |
sol4 sol8. sol16 sol4 sol8. sol16 |
sol2~ sol8 sol sol sol |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    re4 re8. re16 re8 re re re |
    sol, sol sol, sol sol,4 r |
    R1 |
    r4 sol, do r |
  }
  \tag #'basso {
    \rt#4 re8 \rt#4 re |
    sol sol, sol, sol, sol,4 r |
    R1 |
    r4 sol,16-\sug\p sol, sol, sol, do4 r |
  }
>>
R1 |
r4 sol, do8 do'[ sol mi] |
\rt#4 do-\sug\f \rt#4 do |
\rt#4 fa \rt#4 fa |
\rt#4 sol \rt#4 sol, |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    do4 do'2 do'4 |
    la2. fa4 |
    sol2 sol4. sol8 |
    sol,4 sol sol, sol |
  }
  \tag #'basso {
    \rt#4 do8 \rt#4 do' |
    \rt#4 la la8 la fa fa |
    \rt#4 sol \rt#4 sol |
    \rt#4 sol, \rt#4 sol, |
  }
>>
do4 do8. do16 do4 r4 |
%%%
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    do4 r r2 | R1*3 | r4
  }
  \tag #'basso {
    do4 r r r8 do | do1\fp~ | do | sold | la4
  }
>> la,8. la,16 la,4 la |
la, la8. la16 la4 la |
sol1 |
sol8 sol, sol, sol, \rt#4 sol, |
do8 do mi sol do' sol mi do |
fa8 fa16 fa fa8 fa fa4 fa |
sol2 sol,4. sol,8 |
\rt#4 do8 \rt#4 do |
\rt#4 do8 \rt#4 do |
do2 r |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    R1*10
  }
  \tag #'basso {
    R1 |
    dod1~ |
    dod |
    fa |
    r4 re r2 |
    r4 sol sol, r |
    R1 |
    r16. sol32 sol16. sol32 fa!4 r2 |
    R1 |
    r4 sol do r |
  }
>>
