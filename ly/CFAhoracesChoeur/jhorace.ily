\clef "vtaille" R1*14 |
R1*16 |
r2 r4 r8 do' |
mi'2 re'4 mi' |
do'8 do' r sol16 sol do'4 re'8 mi' |
si4^! r8 si re'8. re'16 re'8 mi' |
do'4^! r r2 |
R1*8 |
r4 sol8 sol do'4 do'8 si |
do'4 r8 do' sib sib sib do' |
la^! la r la dod'4 r8 mi' |
dod'4 r8 la16 si dod'4 dod'8 re' |
la4^! r8 la16 la la4 la8 la |
re'4^! r r2 |
R1*5 |
