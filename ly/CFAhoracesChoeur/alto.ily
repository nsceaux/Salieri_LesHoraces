\clef "alto" sol'!4 sol'8. sol'16 sol'4 sol' |
sol'2. sol16 la32 si do' re' mi' fad' |
sol'4 sol'8. sol'16 si'4 si' |
re''2. sol8. sol16 |
<<
  { do'4 do'8. do'16 do'4. mi'8 |
    mi'4. re'8 do'4 re' |
    mi' mi'8. mi'16 mi'4. sol'8 |
    sol'4. fa'8 mi'4 } \\
  { mi4 mi8. mi16 mi4. do'8 |
    do'4. sol8 mi4 sol |
    do' do'8. do'16 do'4. mi'8 |
    mi'4. re'8 do'4 }
>> r4 |
do16 do' si do' re' do' si do' la'4 fa' |
mi'4 do'8. mi'16 la4 si |
do' <sol mi' do''> q la' |
sol'16 re' mi' fad' sol' la' si' do'' re''8 re'' re'' re'' |
<<
  { \rt#8 si'16 \rt#8 la' | } \\
  { \rt#8 sol' \rt#8 fad' | }
>>
sol'4 sol16 sol sol sol sol4 sol |
sol' sol8. sol'16 sol'4 sol8. sol'16 |
sol'16 sol' fad' sol' la' sol' fad' sol' sol8 sol' sol sol' |
re'4 re'8. re'16 re'8 fad fad fad |
\rt#4 sol sol4 r |
R1 |
r4 sol16-\sug\p sol sol sol do'4 r |
R1 |
r4 sol4 do'8 do''[ sol' mi'] |
\rt#8 <mi' do''>16\ff \rt#8 q |
\rt#8 <re' do''> \rt#8 q |
\rt#8 <re' si'> \rt#8 q |
\rt#8 <mi' do''> \rt#8 q |
<do'' fa'>16 fa sol la sib do' re' mi' fa' mi' fa' sol' la'8 la' |
sol'4 \rt#4 <mi' do''>16 \rt#8 q |
\rt#8 <re' si'> \rt#8 q |
<mi' do''>4 do8. do16 do4 sol |
%%%
<mi' do''>4 r r r8 sol' |
sol'1-\sug\fp~ |
sol' |
mi' |
la'4 la8. la16 la4 la' |
la la'8. la'16 la'4 la' |
sol'1 |
sol'4 \rt#8 <si sol'>16 \rt#4 q |
<sol mi'>8 do'[ mi' sol'] do'' sol' mi' do' |
la' la'16 la' la'8 la' \rt#4 la' |
\rt#4 sol' \rt#4 sol |
\rt#4 <sol mi'> \rt#4 q |
\rt#4 q \rt#4 q |
q2 r |
R1 |
dod'1~ |
dod' |
fa' |
r4 re' r2 |
r4 sol' sol r |
R1 |
r16. sol'32 sol'16. sol'32 fa'!4 r2 |
R1 |
r4 sol' do' r |
