\clef "bass" R1*3 |
r2 r4 sol,8. sol,16 |
do4 do8. do16 do4. do8 |
do4 sol,16 sol, sol, sol, do4 sol,8. sol,16 |
do4 do8. do16 do4. do8 |
do4 sol,16 sol, sol, sol, do4 r |
do4 \rt#4 do16 \rt#4 do \rt#4 do |
do4 do8 do do4 sol, |
do do8. do16 do4 do |
sol,2 r |
\rt#8 sol,16 \rt#8 do |
sol,4 sol,16 sol, sol, sol, sol,4 r |
sol, r sol, r |
sol, sol,8. sol,16 sol,4 r |
R1 |
sol,8 sol, sol, sol, sol,4 sol,8.\p sol,16 |
do4 do8. do16 do4. do8 |
do4 sol, do sol,8. sol,16 |
do4 do8. do16 do4. do8 |
do4 sol, do r |
\rt#16 do16-\sug\f |
do8 do16 do do8 do do do do do |
sol,2 sol,4. sol,8 |
do4 do8. do16 do4 do |
do2 r |
sol,8 sol,16 sol, sol,8 sol, \rt#4 sol, |
\rt#8 sol,16 \rt#8 sol, |
do4 do8. do16 do4 r4 |
do4 r r2 |
R1*3 |
r4 do8. do16 do4 r |
r do8. do16 do4 do |
sol, sol,8. sol,16 sol,4 sol, |
\rt#16 sol,16 |
do4 do do do |
\rt#16 do16 |
\rt#16 sol, |
\rt#16 do16 |
\rt#16 do16 |
do2 r |
R1*10 |
