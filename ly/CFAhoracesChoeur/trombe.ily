\clef "treble" sol'4 sol'8. sol'16 sol'4 sol' |
sol'1 |
R1 |
r2 r4 sol'8. sol'16 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { do''4 do''8. do''16 do''4. mi''8 |
    mi''4. re''8 do''4 re'' |
    mi''4 mi''8. mi''16 mi''4. sol''8 |
    sol''4. fa''8 mi''4 }
  { mi'4 mi'8. mi'16 mi'4. do''8 |
    do''4. sol'8 mi'4 sol' |
    do''4 do''8. do''16 do''4. mi''8 |
    mi''4. re''8 do''4 }
>> r4 |
do'1~ |
do'4 do''2 re''4 |
do'' \twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''4 mi'' re'' | re''1 | }
  { do''4 do'' re'' | sol'1 | }
>>
re''2 re''4. re''8 |
sol'4 r r sol' |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { re''4 re''8. re''16 mi''4 mi''8. mi''16 |
    re''2 sol'8 re'' re'' re'' | }
  { sol'4 sol'8. sol'16 do''4 do''8. do''16 |
    sol'2 sol'8 sol' sol' sol' | }
>>
re''1 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { re''8 sol' sol' sol' }
  { sol' sol' sol' sol' }
>> sol'4 sol'8.\p sol'16 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { do''4 do''8. do''16 do''4. mi''8 |
    mi''4. re''8 do''4 re'' |
    mi''4 mi''8. mi''16 mi''4. sol''8 |
    sol''4. fa''8 mi''4 }
  { mi'4 mi'8. mi'16 mi'4. do''8 |
    do''4. sol'8 mi'4 sol' |
    do''4 do''8. do''16 do''4. mi''8 |
    mi''4. re''8 do''4 }
>> r4 | <>\f
\twoVoices #'(tromba1 tromba2 trombe) <<
  { do'4 mi'' mi'' mi'' |
    re''8 re''16 re'' re''8 re'' \rt#4 re'' |
    re''2 re''4. re''8 |
    mi''4 mi'' mi'' do'' | }
  { do'4 do'' do'' do'' |
    do''8 do''16 do'' do''8 do'' \rt#4 do'' |
    sol'2 sol'4. sol'8 |
    do''4 do'' do'' do'' | }
>>
do''1 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { do''8 mi''16 mi'' mi''8 mi'' \rt#4 mi'' |
    re''8 re''16 re'' re''8 re'' \rt#4 re'' | }
  { do''8 do''16 do'' do''8 do'' \rt#4 do'' |
    sol'8 sol'16 sol' sol'8 sol' \rt#4 sol' | }
>>
do''4 do'8. do'16 do'4 sol' |
%%
\twoVoices #'(tromba1 tromba2 trombe) <<
  { do''4 }
  { mi' }
>> r4 r2 |
R1*4 |
r4 \twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''8. mi''16 mi''4 do'' |
    sol' re''8. re''16 re''4 re'' |
    re''1 |
    mi''4 mi'' mi'' mi'' |
    re''8 re''16 re'' re''8 re'' \rt#4 re'' |
    re''2 re''4. re''8 |
    do''1~ | do''~ | do''2 }
  { do''8. do''16 do''4 do'' |
    sol' sol'8. sol'16 sol'4 sol' |
    sol'1 |
    do''4 do'' do'' do'' |
    do''8 do''16 do'' do''8 do'' \rt#4 do'' |
    sol'2 sol'4. sol'8 |
    mi'1~ | mi'~ | mi'2 }
>> r2 |
R1*10 |
