\clef "vbasse" R1*14 |
R1*16 |
r4 sol8 sol do'4^! r |
R1*17 |
r4 r8 la do' do' do' re' |
si4^! r8 sol re' re' do' re' |
si4 r8 si si si do' re' |
re'^! sol r sol16 sol sol4 sol8 la |
si4 r8 re'16 re' si4 si8 do' |
sol4^! r r2 |
