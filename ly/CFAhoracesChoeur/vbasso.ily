\clef "vbasse" R1*3 |
r2 r4 sol8. sol16 |
mi4 mi8. mi16 mi4. do'8 |
do'4.( sol8) mi4 sol8 sol |
do'4 do'8 do' do'4. mi'8 |
mi'4.( re'8) do'4 r |
r do' do' do' |
do'2 la4 si8 si |
do'4 do' do' do' |
si1 |
re'2 re'4. re'8 |
sol2 r4 sol |
sol sol8 sol sol4 sol8 sol |
sol2 r8 sol sol sol |
re'4. re'8 re4. re8 |
sol2 sol8 r sol8.^\p sol16 |
mi4 mi8. mi16 mi4. do'8 |
do'4.( sol8) mi4 sol8. sol16 |
do'4 do'8. do'16 do'4. mi'8 |
mi'4.( re'8) do'4 r |
r do' do' do' |
fa1 |
sol2 sol4. sol8 |
do'4 do' do' do' |
la2. fa4 |
sol1 |
sol2 sol4. sol8 |
do'2 r4 sol |
do'2 r |
R1*3 |
r2 r4 la8. si16 |
do'4 do'8 do' do'4 la |
sol1 |
sol4 r r2 |
r4 do' do' do' |
fa1 |
sol2 sol4. sol8 |
do'2 r |
R1*12 |
