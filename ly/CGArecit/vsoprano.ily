\clef "vbas-dessus" R1*9 |
r2 r4 re''8 re'' |
la'4 la' r fad''8 fad'' |
sol''4 r r2 |
R1*70 |
r2 r4^\fermata
