\clef "bass"
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    sib,4-\sug\ff r r16 sib re' do' sib la sol fa |
    mib4 r r16 mib' do' sib la sol fa mib |
    re4 r r16 re' fa' mib' re' do' sib lab |
    sol4
  }
  \tag #'basso {
    r8 sib,\ff re fa sib,4 r |
    r8 do mib do la,4 r |
    r8 sib, re fa sib4 r8 sib, |
    mib4
  }
>> r4 r2 |
mib4 r mib16 mib' re' do' sib lab sol fa |
mib4 r r2 |
r16 fa, sol, la,! sib, do re mi! fa sol la sib do' sib la sol |
fa4 r r2 |
R1 |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    sib,4 r \clef "tenor" r16 re' mib' re' do' sib la sol |
    fad4 r la16 sol fad mib' re' do' sib la |
    sib sol la sib do' re' mi'! fad' sol'4~ sol'16 fa' mi' re' |
    \clef "bass" la4
  }
  \tag #'basso {
    r8 sib, re fa sib,4 r |
    r8 re fad la fad,4 re' |
    sol,8 sol sol sol \rt#4 sol |
    la4
  }
>> r4 r2 |
R1 |
r2 re16 mi fa re mi fa sol mi |
fa4 r r2 |
r8 re fa la re' fa'16 mi' re' do' si la |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    sold1~ |
  }
  \tag #'basso {
    \rt#8 sold16 \rt#8 sold |
  }
>>
sold1~ |
sold |
la,8 si,16 do re mi fad sold la8. mi16 do8. mi16 |
la,4 <<
  \tag #'(fagotto1 fagotto2 fagotti) {
    r4 r2 | R1 |
  }
  \tag #'basso {
    <>^"Violoncelli" \clef "tenor" la'2\sf la'4 |
    sold'1-\sug\sf | \clef "bass" <>^"Tutti"
  }
>>
r4 sol!2\p sol4 |
lab2\sf lab |
lab1\p~ |
lab |
sol~ |
sol |
do4 r r2 |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    \clef "tenor"
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { mi'!1 | fa'8 }
      { reb'1 | do'8 }
    >> \clef "bass"
  }
  \tag #'basso {
    sib1\sf | lab8
  }
>> lab8[\ff do' lab] fa lab re fa |
sib,4 r r2 |
R1 |
mib4.-\sug\ff sol16 sib mib'4 mib |
mi! r r2 |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    R1*2 |
    \clef "tenor" r4 fa'4.\sf mib'8(\p reb' do') |
    reb'1 | reb'~ | reb'2 do'4 r | r2 fa'4 r |
    R1*14 |
    fa'1-\sug\fp~ | fa'~ | fa' | fad' |
    sol'~ | sol'2 do'-\sug\fp~ | do'1 | sib |
    \clef "bass" la1-\sug\fp | sol | si!-\sug\fp | do'~ | do'-\sug\fp |
    fa'1 | re'-\sug\ff~ | re'4 r r2 |
  }
  \tag #'basso {
    R1 |
    fa4 r r2 | fa2\f fa,\p | fa4 fa fa fa | mib1~ |
    mib2 lab4 r | r2 re!4 r | r2 mib4 r | r2 mib4 r |
    r2 r4 fa | fad1\sf ~ | fad~ | fad2~ fad4. dod8\f |
    fad4 mi red2~ | red1~ | red2 r4 r8 mi8\f | mi4 mi mi r |
    R1 | r2 sol!4 r | R1 | r2 r4 la |
    \rt#8 re16\fp \rt#8 re | \rt#8 re \rt#8 re |
    \rt#8 re \rt#8 re | \rt#8 fad \rt#8 fad |
    \rt#8 sol \rt#8 sol | \rt#8 sol \rt#8 fa!\fp |
    \rt#8 fa \rt#8 fa | \rt#8 sib, \rt#8 sib, |
    la,16\fp la la la \rt#4 la \rt#8 la |
    \rt#8 sol \rt#8 sol | \rt#8 si,!\fp \rt#8 si, |
    \rt#8 do \rt8 do | \rt#8 sib\fp \rt#8 sib |
    \rt#8 lab \rt#8 lab | \rt#8 fa\ff \rt#8 fa | fa4 r r2 |
  }
>>
r4 sol do r |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    R1*8 | r2 r4\fermata
  }
  \tag #'basso {
    R1 |
    r2 fad4 r |
    R1 |
    sol4 r r2 |
    R1 |
    r2 sold4 r |
    r2 la4 r |
    r2 r8 r16 fa fa4 |
    r2 r4\fermata
  }
>>
