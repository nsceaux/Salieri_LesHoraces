\clef "vhaute-contre" R1*9 |
r2 r4 re'8 re' |
fad'4 fad' r la'8 la' |
sib'4 r r2 |
R1*70 |
r2 r4^\fermata
