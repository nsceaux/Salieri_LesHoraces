\clef "treble" <re' sib' sib''>8\ff sib16[ sib] re' re' fa' fa' sib sib'[ re'' do''] sib' la' sol' fa' |
mib'8 do'16[ do'] mib' mib' do' do' la mib''[ do'' sib'] la' sol' fa' mib' |
re'8 sib16[ sib] re' re' fa' fa' sib' lab''[ fa'' mib''] re'' do'' sib' lab' |
sol'4 r r2 |
r16 sol'' sib'' lab'' sol'' fa'' mib'' re'' mib'' sol'' fa'' mib'' re'' do'' sib' lab' |
sol'4 r r2 |
r16 fa' sol' la'! sib' do'' re'' mi''! fa'' sol'' la'' sib'' do''' sib'' la'' sol'' |
fa''4 r r2 |
R1 |
<re' sib' sib''>8 sib16[ sib] re' re' fa' fa' sib re''[ mib'' re''] do'' sib' la' sol' |
fad'8 re'16[ re'] fad' fad' la' la' la'' sol'' fad'' mib'' re'' do'' sib' la' |
sib' sol' la' sib' do'' re'' mi'' fad'' sol'' la'' sib'' la'' sol'' fa''? mi'' re'' |
dod''4 r r2 |
R1 |
r2 <<
  \tag #'violino1 {
    fa''8. fa''16 dod''8. dod''16 | re''4
  }
  \tag #'violino2 {
    << { la'8. la'16 sib'8. sib'16 | la'4 }
      \\ { fa'8. fa'16 sol'8. sol'16 | fa'4 } >>
  }
>> r4 r2 |
r8 re'16 re' fa' fa' la' la' re''8 fa''16 mi'' re'' do'' si' la' |
sold'8 si'16[ si'] mi'' mi'' sold'' sold'' si'' sold'' mi'' si' mi'' si' sold' mi' |
si1~ |
si |
la8 si16 do' re' mi' fad' sold' la'8. mi'16 do'8. mi'16 |
la4 <<
  \tag #'violino1 {
    fa''2\sf fa''4 | fa''1\sf |
    r4 fa''2\p fa''4 | mib''( lab''2\sf sol''4) |
    fad''1\p ~ | fad'' | fa''!~ | fa'' |
    mib''8 r do'''4-.(\< do'''-. do'''-.)\! |
    reb'''4.(\sf do'''16 sib'' lab''8 sol'' fa'' mi''!) | fa''8
  }
  \tag #'violino2 {
    r4 r2 | si''1\sf | r4 si'2-\sug\p si'4 | do''2-\sug\sf do'' |
    do''1-\sug\p~ | do'' | si'!~ | si' |
    do''8 r do''4-.( do''-. do''-.) | reb''1 | lab'8
  }
>> lab'16\ff[ lab'] do'' do'' lab' lab' fa' fa' lab' lab' re' re' fa' fa' |
sib4 r r2 |
R1 |
mib'4.\ff sol'16 sib' mib''8. mib''16 <<
  \tag #'violino1 {
    reb''8. reb''16 | do''4
  }
  \tag #'violino2 {
    << { sib'8. sib'16 | sib'4 }
      \\ { sol'8. sol'16 | sol'4 } >>
  }
>> r4 r2 |
R1 |
<<
  \tag #'violino1 {
    do''4 r r2 |
    r4 fa''4.\fp mib''8( reb'' do'') |
    reb''2. reb''4 |
    reb''1~ |
    reb''2 do''4 r |
    r2 sib'4 r |
    r2 sol''4 r |
    r2 la''!4 r |
  }
  \tag #'violino2 {
    lab'4 r r2 |
    r4 fa'4.\fp mib'8( reb' do') |
    reb'4 lab'2 lab'4 |
    sib'1~ |
    sib'2 lab'4 r |
    r2 sib'4 r |
    r2 sib'4 r |
    r2 do''4 r |
  }
>>
r2 r4 <<
  { fa''4 | dod''1~ | dod''~ | dod''2~ dod''4. } \\
  { la'4 | lad'1\sf~ | lad'~ | lad'2~ lad'4. }
>> dod'8\f |
fad'16 fad' fad' fad' mi' mi' mi' mi' <<
  \tag #'violino1 {
    red'2~ |
    red'1~ |
    red'2 r4 r8 mi''\f |
    mi''8. sold''16 sold''8. si''16 si''4 r |
  }
  \tag #'violino2 {
    si'2~ |
    si'1~ |
    si'2 r4 r8 <<
      { si'8 | si'8. mi''16 mi''8. sold''16 sold''4 } \\
      { sold'8-\sug\f | sold'8. si'16 si'8. si'16 si'4 }
    >> r4 |
  }
>>
R1 |
r2 <mi' dod'' la''>4 r |
R1 |
r2 r4 <la mi' dod''>4 |
<<
  \tag #'violino1 {
    \rt#8 <fa' re''>16\fp \rt#8 <fa' re''> |
    \rt#8 q \rt#8 q |
    \rt#8 q \rt#8 q |
    \rt#8 mib'' \rt#8 mib'' |
    \rt#8 re'' \rt#8 re'' |
    \rt#8 mib'' \rt#8 <mib' do'>\fp |
    \rt#8 <do' mib'> \rt#8 q |
    \rt#8 <sib re'> \rt#8 q |
    do'\fp do'' do'' do'' \rt#4 do'' \rt#8 do'' |
    \rt#8 <re' sib'> \rt#8 q |
    \rt#8 <re' re''>\fp \rt#8 <re' re''> |
    \rt#8 <mib' mib''> \rt#8 q |
    \rt#8 <sol' mi''!>\fp \rt#8 <sol' mi''> |
    \rt#8 fa'' \rt#8 fa'' |
    \rt#8 <re'' si''!>\ff \rt#8 <re'' si''> |
    q4 r r2 |
  }
  \tag #'violino2 {
    \rt#8 <fa' la'>16\fp \rt#8 <fa' la'> |
    \rt#8 q \rt#8 q |
    \rt#8 q \rt#8 q |
    \rt#8 <la' do''> \rt#8 q |
    \rt#8 sib' \rt#8 sib' |
    \rt#8 <sol' sib'> \rt#8 la\fp |
    \rt#8 la \rt#8 la |
    \rt#8 <sib re'> \rt#8 q |
    <la fad'>16\fp fad'' fad'' fad'' \rt#4 fad'' \rt#8 fad'' |
    \rt#8 sol'' \rt#8 sol'' |
    \rt#8 sol''\fp \rt#8 sol'' |
    \rt#8 sol'' \rt#8 sol'' |
    \rt#8 sol''\fp \rt#8 sol'' |
    \rt#8 do'' \rt#8 do'' |
    \rt#8 <si'! lab''>\ff \rt#8 <si' lab''> |
    <si'! lab''>4 r r2 |
  }
>>
r4 << { <re'' si''>4 <mi''! do'''> } \\ { sol' sol' } >> r |
R1 |
r2 <<
  \tag #'violino1 { re''4 }
  \tag #'violino2 { <la' re'>4 }
>> r4 |
R1 |
<re' si'>4 r r2 |
R1 |
r2 <<
  \tag #'violino1 { mi''4 }
  \tag #'violino2 { <mi' si'>4 }
>> r4 |
r2 <mi' do''>4 r |
r2 r8 r16 <<
  \tag #'violino1 { <la' fa''>16 q4 | }
  \tag #'violino2 { <fa' do''>16 q4 | }
>>
r2 r4\fermata
