\clef "vtaille" R1*15 |
r4 la8 la re'4 re'8 re' |
la4^! r r2 |
R1 |
r4 si4 si8 si si si |
mi'4. si8 si do' re' mi' |
do'^! do' r4 r2 |
R1*11 |
r4 lab lab8 lab lab lab |
re'4 r8 re'16 fa' fa'4 lab8 sib |
sol^! sol r4 r2 |
r4 r8 sol do' do' do' do' |
sol4 sol8 lab sib4 sib8 do' |
lab4^! r8 do'16 do' lab8 lab16 lab sib8 do' |
fa8 fa r4 r2 |
R1 |
r2 r4 sib8 sib |
mib'4 reb'8 mib' do'4^! r8 lab16 lab |
lab4 sib8 do' sib4^! r8 sib16 sib |
sib4 lab8 sib sol^! sol r sib |
mib' mib' mib' do' la!4^! r |
la8 la16 la la8 sib fa^! fa r4 |
R1*27 |
r2 r8 sol sol sol |
do'4 do'8 do' mi'4 re'8 mi' |
do'4 r r2 |
R1*5 |
r4 r8 do'16 do' fa'4^! r8 fa'16 re' |
si4 si8 do' sol4\fermata
