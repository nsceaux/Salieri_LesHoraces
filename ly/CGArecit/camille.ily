\clef "vbas-dessus" <>^\markup\italic {
  Elle entre avec beaucoup de précipitation et de fureur
}
R1*3 |
r2 r4 r8 sib' |
mib''4^! mib''8 r r2 |
r4 r8 mib'' mib'' mib'' fa'' sol'' |
la'!^! la' r4 r2 |
do''4 r do''8 do'' do'' fa'' |
fa'' do'' r do'' mib'' mib'' fa'' do'' |
re''4^! r4 r2 |
R1*2 |
r4 r8 dod'' mi''4 r8 mi''16 mi'' |
mi''4 dod''8 dod'' la'4 la'8 la' |
mi''4 mi''8 fa'' re''^! re'' r4 |
R1 |
r2 r4 r8 fa'' |
mi''4 mi'' r mi''8 mi'' |
si'4 r r2 |
R1*3 |
fa''4. fa''8 fa''4 re''8 re'' |
si'4^! r4 r2 |
R1 |
r4 mib''8. mib''16 mib''4 re''8 mib'' |
do'' do'' r8 do'' do''8 do'' mib'' do'' |
si'!^! si' r re''16 re'' fa''4 re''8 re'' |
si'4 r8 si'16 do'' re''4 re''8 mib'' |
do''4^! <>^\markup\italic { Elle pleure sur les dépouilles } r4 r2 |
R1*10 |
r4 reb'' \grace do''4 sib'4 r |
R1*5 |
r2 dod''4 dod'' |
r8 dod'' dod'' fad'' fad'' dod'' r dod'' |
lad' lad' si' dod'' fad'4^! r4 |
r2 r4 r8 si'16 si' |
si'4 si'8 dod'' red''4 r8 red''16 fad'' |
red''4 red''8 mi'' si' si'^! r4 |
r2 r4 r8 si'16 si' |
si'4 si'8 si' mi'' mi'' r si'16 dod'' |
re''4 re''8 mi'' dod''^! dod'' r8 mi'' |
mi'' mi'' re'' mi'' dod''4 sol'' |
dod''8 dod'' dod'' re'' la'4^! r4 |
r2 la'4 la'8 la' |
re''4. re''8 fa'' fa'' mi'' fa'' |
re'' re'' r la'16 la' re''4 re''8 re'' |
mib''4.^! la'16 sib' do''4 do''8 re'' |
sib'4^! r sib' sib'8 sib' |
mib''8.^! mib''16 fa''8 sol'' la'^! la' r8 do''16 do'' |
do''8 do''16 do'' fa''4 r8 do''16 re'' mib''8 mib''16 fa'' |
re''4^! r8 re''16 re'' re''4 re''8 re'' |
fad''4^! r8 fad''16 la'' la''4 do''8 re'' |
sib'^! sib' r re''16 re'' re''4 re''8 re'' |
sol''4^! r4 sol''8. re''16 fa''8 re''16 mib'' |
do''4^! r8 do'' sol'' sol'' fa'' sol'' |
mi''!4^! r sol''8 mi''16 mi'' re''8 do'' |
fa''4^! r8 do''16 do'' do''8 do''16 do'' fa''8. lab''16 |
si'!2^! r2 |
r4 si'! si' do'' |
sol' sol'8 r r2 |
R1*8 |
r2 r4\fermata
