\tag #'camille {
  Ou suis- je ? & quel trans -- port cou -- pa -- ble !
  Quoi ? Rome au fra -- tri -- ci -- de é -- lè -- ve des au -- tels !
}
\tag #'choeur {
  C’est Ca -- mil -- le !
  Ar -- rê -- tez !
}
\tag #'camille {
  Cru -- els !
  Lais -- sez- moi con -- tem -- pler cet -- te fête ex -- é -- cra -- ble.
}
\tag #'jhorace {
  Qu’on l’é -- loi -- gne, Ro -- mains !
}
\tag #'camille {
  Per -- fi -- des lais -- sez- moi.
}
\tag #'jhorace {
  O, d’une in -- di -- gne sœur, in -- su -- por -- table au -- da -- ce !
}
\tag #'camille {
  Ciel ! qu’est- ce que je voi ?
  O dé -- pouil -- les sa -- cré -- es ! ô mon cher Cu -- ri -- a -- ce !
  Voi -- là donc au -- jour -- d’hui ce qui res -- te de toi !
}
\tag #'jhorace {
  O hon -- te de mon sang ! ô cou -- pable in -- so -- len -- ce !
  Ban -- nis d’un lâche a -- mour le hon -- teux sou -- ve -- nir,
  Et sois di -- gne de ta nais -- san -- ce.
}
\tag #'camille {
  Hé -- las !
}
\tag #'jhorace {
  Ne nous fais plus rou -- gir ;
  Et pré -- fè -- re du moins à l’a -- mour d’un seul hom -- me
  La gloi -- re de ton sang & l’in -- té -- rêt de Ro -- me.
}
\tag #'camille {
  Ro -- me ! Je la dé -- tes -- te, ain -- si que ta va -- leur ;
  Plus tu blâ -- mes mes pleurs, plus j’y trou -- ve de char -- mes !
  Rome é -- lève un tro -- phé -- e au suc -- cès de tes ar -- mes,
  Plus el -- le t’ap -- plau -- dit, plus tu me fais hor -- reur.
  Puis -- sent les Dieux, lan -- çant sur vous la fou -- dre,
  Et sur elle & sur toi, me ven -- ger au -- jour -- d’hui !
  Puis -- sé- je voir ré -- duire en cen -- dre %pou -- dre
  Ces fé -- ro -- ces Ro -- mains dont ton bras fut l’ap -- pui !
  Qu’à vos jus -- tes tour -- mens l’u -- ni -- vers ap -- plau -- dis -- se !
  Qu’on ou -- blie à ja -- mais Rome & son dé -- fen -- seur !
  Qu’en -- fin de tant de maux seu -- le je sois l’au -- teur
  Pour ac -- croître à la fois ma joie & ton sup -- pli -- ce !
}
\tag #'jhorace {
  C’est trop souf -- frir un mor -- tel dés -- hon -- neur !
}
\tag #'valere {
  Ah sei -- gneur, d’une a -- mante ex -- cu -- sés la fai -- bles -- se,
  N’é -- cou -- tés point ses trans -- ports fu -- ri -- eux,
  Que rien de ce grand jour ne trou -- ble l’al -- lé -- gre -- se.
  Rome est li -- bre.
}
\tag #'jhorace {
  Il suf -- fit, ren -- dons gra -- ces aux Dieux.
}
