\clef "vbasse" R1*75 |
r4 r8 do'16 mi' re'4^! r8 re'16 re' |
re'4 la8 si do'4 do'8 re' |
si^! si r16 re' re' re' si4 si8 si |
si4 do'8 re' sol4 r8 si |
si si si si mi'4^! r8 si |
re' re' re' mi' do'^! do' r do'16 mi' |
mi'8 la r4 r2 |
r2 r4\fermata
