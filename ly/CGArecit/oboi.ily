\clef "treble" <>-\sug\ff
<<
  \tag #'(oboe1 oboi flauto1 flauti) \new Voice {
    \tag #'(oboi flauti) \voiceOne
    sib''4
  }
  \tag #'(oboe2 oboi flauto2 flauti) \new Voice {
    \tag #'(oboi flauti) \voiceTwo
    re''
  }
>> r4 r16 sib' re'' do'' sib' la' sol' fa' |
mib'4 r4 r16 mib'' do'' sib' la' sol' fa' mib' |
re'4 r r16 lab'' fa'' mib'' re'' do'' sib' lab' |
sol'4 r r2 |
r r16 sol'' fa'' mib'' re'' do'' sib' lab' |
sol'4 r r2 |
r16 fa' sol' la'! sib' do'' re'' mi''! fa'' sol'' la'' sib'' do''' sib'' la'' sol'' |
fa''4 r r2 |
R1 |
<<
  \tag #'(oboe1 oboi flauto1 flauti) \new Voice {
    \tag #'(oboi flauti) \voiceOne
    sib''4
  }
  \tag #'(oboe2 oboi flauto2 flauti) \new Voice {
    \tag #'(oboi flauti) \voiceTwo
    re''
  }
>> r r16 re'' mib'' re'' do'' sib' la' sol' |
fad'4 r la''16 sol'' fad'' mib'' re'' do'' sib' la' |
sib' sol' la' sib' do'' re'' mi''! fad'' sol'' la'' sib'' la'' sol'' fa''? mi'' re'' |
dod''4 r r2 |
R1 |
r2 <<
  \tag #'(oboe1 oboi flauto1 flauti) \new Voice {
    \tag #'(oboi flauti) \voiceOne
    fa''8. fa''16 dod''8. dod''16 | re''4
  }
  \tag #'(oboe2 oboi flauto2 flauti) \new Voice {
    \tag #'(oboi flauti) \voiceTwo
    la'8. la'16 sol'8. sol'16 | fa'4
  }
>> r4 r2 |
r8 re' fa' la' re'' fa''16 mi'' re'' do'' si' la' |
sold'4 r r2 |
<<
  \tag #'(oboe1 oboe2 oboi) {
    R1*3 |
    r4 \twoVoices #'(oboe1 oboe2 oboi) <<
      { fa''2-\tag #'oboe1 -\sug-\sf fa''4 |
        fa''1-\tag #'oboe1 -\sug-\sf | }
      { do''2-\sug\sf do''4 | re''1-\sug\sf | }
    >>
  }
  \tag #'(flauto1 flauto2 flauti) {
    R1*4
    \twoVoices #'(flauto1 flauto2 flauti) <<
      { re'''1-\tag #'flauto1 -\sf }
      { fa''1\sf }
    >>
  }
>>
r4 <<
  \tag #'(oboe1 oboi flauto1 flauti) \new Voice {
    \tag #'(oboi flauti) \voiceOne
    fa''2 fa''4 | mib''4 lab''2\sf sol''4 |
    fad''1~ | fad'' | fa''!~ | fa'' |
  }
  \tag #'(oboe2 oboi flauto2 flauti) \new Voice {
    \tag #'(oboi flauti) \voiceTwo
    si'2 si'4 | do''2 do'' |
    do''1~ | do'' | si'!~ | si' |
  }
  { s2.\p | s1 | s\p }
>>
<<
  \tag #'(oboe1 oboe2 oboi) {
    \twoVoices #'(oboe1 oboe2 oboi) <<
      { mib''4 }
      { do'' }
    >> r4 r2 |
    \twoVoices #'(oboe1 oboe2 oboi) <<
      { mi''!1 | fa''4 }
      { sol'1 | fa'4 }
    >>
  }
  \tag #'(flauto1 flauto2 flauti) {
    r4 \twoVoices #'(flauto1 flauto2 flauti) <<
      { do'''4( do''' do''') |
        reb'''4.( do'''16 sib'' lab''8 sol'' fa'' mi''!) |
        fa''4 }
      { do''4-\tag #'flauto2 -( do'' do''-\tag #'flauto2 -) |
        reb''1 | do''4 }
    >>
  }
>> r4 r2 |
R1*2 |
mib'1-\sug\ff |
mi'!4 r r2 |
R1*3 |
r4 lab''2\sf lab''4 |
sol''1~ |
sol''2 lab''4 r |
<<
  \tag #'(oboe1 oboe2 oboi) {
    R1*4 |
    <>^"Oboi soli" -\sug\sf
    \twoVoices #'(oboe1 oboe2 oboi) <<
      { dod''1~ | dod''~ | dod''2~ dod''4 }
      { lad'1~ | lad'~ | lad'2~ lad'4 }
    >> r4 |
    fad'4-\sug\f mi' red'2~ |
    red'1~ |
    red'2 r4 r8 <>\f \twoVoices #'(oboe1 oboe2 oboi) <<
      { mi''8 | mi''8. si'16 si'8. si'16 si'4 }
      { sold'8 | sold'8. sold'16 sold'8. sold'16 sold'4 }
    >> r4 |
    R1*4 | <>^"Tutti"
  }
  \tag #'(flauto1 flauto2 flauti) {
    R1*15 |
  }
>>
<>\fp <<
  \tag #'(oboe1 oboi flauto1 flauti) \new Voice {
    \tag #'(oboi flauti) \voiceOne
    re''1~ | re''~ | re'' | mib'' |
    re'' | mib''2 mib''-\tag #'(oboe1 flauto1) -\sug\fp |
    mib''1 | re'' |
    fad''1-\tag #'(oboe1 flauto1) -\sug\fp | sol'' |
    sol''-\tag #'(oboe1 flauto1) -\sug\fp | sol'' |
    sol''-\tag #'(oboe1 flauto1) -\sug\fp | fa'' |
    lab''1-\tag #'(oboe1 flauto1) -\ff ~ | lab''4
  }
  \tag #'(oboe2 oboi flauto2 flauti) \new Voice {
    \tag #'(oboi flauti) \voiceTwo
    la'1~ | la'~ | la' | la' |
    sib'~ | sib'2 la'-\sug\fp | la'1 | sib' |
    do''1-\sug\fp | sib' | re''-\sug\fp | mib'' |
    mi''!-\sug\fp | do''1 | si'!\ff~ | si'4
  }
>> r4 r2 |
r4 <<
  \tag #'(oboe1 oboi flauto1 flauti) \new Voice {
    \tag #'(oboi flauti) \voiceOne
    si''4 do'''
  }
  \tag #'(oboe2 oboi flauto2 flauti) \new Voice {
    \tag #'(oboi flauti) \voiceTwo
    re''4 mi''!
  }
>> r4 |
R1*8 |
r2 r4\fermata
