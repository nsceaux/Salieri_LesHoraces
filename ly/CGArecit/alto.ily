\clef "alto" r8 sib-\sug\ff re' fa' sib4 r |
r8 do' mib' do' la4 r |
r8 sib re' fa' sib'4 r8 sib |
<sol mib'>4 r r2 |
mib'4 r mib'16 mib'' re'' do'' sib' lab' sol' fa' |
mib'4 r r2 |
r16 fa sol la! sib do' re' mi'! fa' sol' la' sib' do'' sib' la' sol' |
fa'4 r r2 |
R1 |
r8 sib re' fa' sib4 r |
r8 re' fad' la' fad4 re'' |
re'8 <re' sib'> q q \rt#4 q |
<< mi'!4 \\ dod' >> r r2 |
R1 |
r2 re'16 mi' fa' re' mi' fa' sol' mi' |
fa'4 r r2 |
r8 re fa la re' fa'16 mi' re' do' si la |
\rt#8 <si mi'>16 \rt#8 q |
q1~ |
q |
la8 si16 do' re' mi' fad' sold' la'8. mi'16 do'8. mi'16 |
la4 do''2-\sug\sf do''4 |
re''1-\sug\sf |
r4 re'2-\sug\p re'4 |
do'2-\sug\sf do' |
mib''1-\sug\p~ |
mib'' |
re''~ |
re'' |
do''4 r r2 |
<sol mi'!>1 |
<< fa'8 \\ lab >> lab'-\sug\ff do'' lab' fa' lab' re' fa' |
sib4 r r2 |
R1 |
mib'4.\ff sol'16 sib' mib''4 <sol sol'>4 |
q r r2 |
R1 |
fa'4 r r2 |
fa'2\f fa\p |
fa4 fa'2 fa'4 |
sol'1~ |
sol'2 lab'4 r |
r2 re'!4 r |
r2 mib'4 r |
r2 mib'4 r |
r2 r4 fa' |
fad'1\sf~ |
fad'~ |
fad'2~ fad'4. dod'8\f |
fad16 fad fad fad mi mi mi mi fad'2~ |
fad'1~ |
fad'2 r4 r8 mi'\f |
mi'4 mi' mi' r |
R1 |
r2 sol'!4 r |
R1 |
r2 r4 la' |
\rt#8 re'16\fp \rt#8 re' |
\rt#8 re' \rt#8 re' |
\rt#8 re' \rt#8 re' |
\rt#8 fad' \rt#8 fad' |
\rt#8 sol' \rt#8 sol' |
\rt#8 sol' \rt#8 fa'!\fp |
\rt#8 fa' \rt#8 fa' |
\rt#8 sib \rt#8 sib |
la16\fp la' la' la' \rt#4 la' \rt#8 la' |
\rt#8 sol' \rt#8 sol' |
\rt#8 si!\fp \rt#8 si |
\rt#8 do' \rt#8 do' |
\rt#8 sib'-\sug\fp \rt#8 sib' |
\rt#8 lab' \rt#8 lab' |
\rt#8 fa'\ff \rt#8 fa' |
fa'4 r r2 |
r4 sol' do' r |
R1 |
r2 fad'4 r |
R1 |
sol'4 r r2 |
R1 |
r2 sold'4 r |
r2 la'4 r |
r2 r8 r16 fa' fa'4 |
r2 r4\fermata
