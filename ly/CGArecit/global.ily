\key do \major \midiTempo#112
\tempo\markup { Allegro assai \smaller { et toujours le meme mouvement } }
\time 4/4 s1*16
\tempo "Allegro assai" s1*4
\tempo "Allegro assai" s1*3
\tempo "Allegro assai" s1*6
\tempo "Allegro assai" s1*9
\tempo "Allegro assai" s1*10 s2
\tempo "Allegro assai" s2 s1*2 s2.
\tempo "Allegro assai" s4 s1*30 s2. \bar "|."
