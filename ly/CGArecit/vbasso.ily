\clef "vbasse" R1*9 |
r2 r4 sib8 sib |
re'4 re' r re'8 re' |
sol4 r r2 |
R1*70 |
r2 r4^\fermata
