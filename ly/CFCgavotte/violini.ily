\clef "treble" la'8\p( sib' la' sib') |
do'' r fa'4( fa''\sf mi'') |
\grace mi''4 re''2 sib''8\mf( la'') sol''-. fa''-. |
fa''( mi'') re''-. do''-. do''( sib') la'-. sol'-. |
fa'4 do' la'8\p( sib' la' sib') |
do''8 r fa'4( fa''\sf mi'') |
<<
  \tag #'violino1 {
    re''4 re'''2\f sib''8 sol'' |
    fa''2 mi''4. fa''16 sol'' |
    fa''2
  }
  \tag #'violino2 {
    re''8-\sug\f re'' re'' re'' re'' re'' re'' sib' |
    \rt#4 la' sol' mi' sol' mi' |
    fa'2
  }
>>
%%%
la''8( fa'') mi''-. re''-. |
sol''( mi'') re''-. do''-. si'-. re''( mi'' fa'') |
\grace fa''4 mi''2 r |
<si re'>4^"Pizzicato" r q r |
<do' mi'>4 r r2 |
<si re'>4 r q r |
<do' mi'>4 r << <do''' mi''>^"Arco" \\ sol'\ff >> do'''4 |
\grace do'''8 si''4 la''8 sol'' do'''4 do''' |
<re'' si''>2 sol''8 si'' re''' si'' |
do''' sol'' la'' fa'' mi'' sol'' si' re'' |
do''2 la'8(\p sib'! la' sib') |
do'' r fa'4( fa''\sf mi'') |
\grace mi''4 re''2 sib''8\mf( la'') sol''-. fa''-. |
fa''( mi'') re''-. do''-. do''( sib') la'-. sol'-. |
fa'4 do' la'8(\p sib' la' sib') |
do'' r fa'4( fa''\sf mi'') |
<<
  \tag #'violino1 {
    re''2.\f mi''8 fa'' |
    sol''4-. sol''( la'' sib'') |
    do''2. re''8 mi'' |
    fa''4-. fa''( sol'' la'') |
    sol''4. la''8 sib'' la'' sib'' sol'' |
    fa''2 mi''4. fa''16 sol'' |
    fa''2
  }
  \tag #'violino2 {
    re''8\f sib' fa' fa' \rt#4 fa' |
    \rt#4 re' \rt#4 re' |
    \rt#4 do' \rt#4 do' |
    \rt#4 do' \rt#4 do' |
    re'8 re'' re'' re'' re''4 sib' |
    la'8 do'' la' do'' <<
      { sib' sib' sib' sib' } \\
      { sol' sol' sol' sol' } >> |
    << la'2 \\ fa' >>
  }
>>
%%%
<<
  \tag #'violino1 {
    re''8\p re'' \grace mi'' re'' dod''16 re'' |
    dod''8 dod'' \grace re'' dod'' si'16 dod'' re''8 re'' \grace mi'' re'' dod''16 re'' |
    mi''8 mi'' \grace fa'' mi'' re''16 mi'' fa''8 fa'' \grace sol'' fa'' mi''16 fa'' |
    sol''8 sol'' \grace la'' sol'' fa''16 sol'' fa''8 fa'' \grace sol'' fa'' mi''16 fa'' |
    mi''2 re''8 re'' \grace mi'' re'' dod''16 re'' |
    dod''8 dod'' \grace re'' dod'' si'16 dod'' re''8 re'' \grace mi'' re'' dod''16 re'' |
    <sib'! mi''>1~\f\> |
    mi''4 sib''\! la''8( sol'' fa'' mi'') |
    fa''4 fa'2 sol'4 |
    fa'2
  }
  \tag #'violino2 {
    fa'8-\sug\p( la') fa'-. re'-. |
    mi'( la') sol'-. mi'-. fa'( la') fa'-. re'-. |
    dod'( la') mi'-. dod'-. re'( la') fa'-. re'-. |
    mi'( la') sol'-. mi'-. re'( la') fa'-. re'-. |
    dod'( mi') la'-. sol'-. fa'( la') fa'-. re'-. |
    mi'( la') sol'-. mi'-. fa'( la') fa'-. fa'-. |
    <sol' sib'!>1-\sug\f\>~ |
    << q { s4 s2.\! } >> |
    << la'4 \\ fa' >> la2 sib4 |
    la2
  }
>>
%%%
<<
  \tag #'violino1 {
    do''8\f do'' \grace re''8 do'' si'16 do'' |
    do'4 do'2 do'4 |
    do' do'2-\sug\sf do'4 |
    do' do'2 do'4 |
    do' fa''8( mi'' \grace sol'' fa''4\sf mi''8 re'') |
    dod''8\p dod'' \grace re'' dod'' si'?16 dod'' re''8 re'' \grace mi'' re'' dod''16 re'' |
    mi''8 mi'' \grace fa'' mi'' re''16 mi'' fad''8 fad'' \grace sol'' fad'' mi''16 fad'' |
    sol''8 sol'' \grace la'' sol'' fad''16 sol'' sold''8 sold'' \grace la'' sold'' fad''16 sold'' |
    la''8 la'' \grace si'' la'' sold''16 la'' dod'''8 dod''' \grace re''' dod''' si''!16 dod''' |
    re'''8 la'' fa'' re'' re''4 \grace fa''4 mi'' |
    re''2
  }
  \tag #'violino2 {
    r4 la\f |
    sol8. la16 la8 sol16 la sib4 sol |
    la do'2\sf sib8 la |
    sol8. la16 la8 sol16 la sib4 sol |
    la r r2 |
    sol'8\p la' sol' sol' fa' la' fa' re' |
    la( dod') mi'-. la'-. la'( fad') la'-. re''-. |
    re''( si') sol'-. re'-. re'( re'') mi''-. re''-. |
    dod''( mi'') dod''-. la'-. mi'( la') dod''-. mi''-. |
    la'4 fa'2 dod'4 |
    re'2
  }
>>
%%%
la'8(\p sib' la' sib') |
do'' r fa'4( fa''\sf mi'') |
\grace mi''4 re''2 sib''8\mf( la'') sol''-. fa''-. |
fa''( mi'') re''-. do''-. do''( sib') la'-. sol'-. |
fa'4 do' la'8\p( sib' la' sib') |
do'' r fa'4( fa''\sf mi'') |
<<
  \tag #'violino1 {
    re''2.\f mi''8 fa'' |
    sol''4-. sol''( la'' sib'') |
    do''2. re''8 mi'' |
    fa''4 fa'' sol'' la'' |
    sol''4. la''8 sib'' la'' sib'' sol'' |
    fa''2 mi''4. fa''16 sol'' |
    fa''2
  }
  \tag #'violino2 {
    re''8-\sug\f sib' fa' fa' \rt#4 fa' |
    \rt#4 re' \rt#4 re' |
    \rt#4 do' \rt#4 do' |
    \rt#4 do' \rt#4 do' |
    re'8 re'' re'' re'' re''4 sib' |
    la'8 do'' la' do'' << \rt#4 sib' \\ \rt#4 sol' >> |
    << la'2 \\ fa' >>
  }
>>
%%%
la''8( fa'') mi''-. re''-. |
sol''( mi'') re''-. do''-. si' re'' mi'' fa'' |
\grace fa''4 mi''2 r |
<si re'>4^"Pizzicato" r q r |
<do' mi'> r r2 |
<si re'>4 r q r |
<do' mi'> r << <do''' mi''>4^"Arco" \\ sol'\ff >> do'''4 |
\grace do'''8 si''4 la''8 sol'' do'''4 do''' |
<si'' re''>2 sol''8 si'' re''' si'' |
do''' sol'' la'' fa'' mi'' sol'' si' re'' |
do''2 la'8(\p sib'! la' sib') |
do'' r fa'4( fa''\sf mi'') |
\grace mi''4 re''2 sib''8(\mf la'' sol'' fa'') |
fa''( mi'') re''-. do''-. do''( sib') la'-. sol'-. |
fa'4 do' la'8\p( sib') la'( sib') |
do''8 r fa'4( fa''\sf mi'') |
<<
  \tag #'violino1 {
    re''2.\f mi''8 fa'' |
    sol''4-! sol''( la'' sib'') |
    do''2. re''8 mi'' |
    fa''4^! fa''( sol'' la'') |
    sol''4. la''8 sib'' la'' sib'' sol'' |
    fa''2 mi''4. fa''16 sol'' |
    fa''4.\ff la''8 do'''4 la'' |
    fa''8 fa'' do'' do'' la' la' do'' do'' |
    fa''4
  }
  \tag #'violino2 {
    re''8-\sug\f sib' fa' fa' \rt#4 fa' |
    \rt#4 re' \rt#4 re' |
    \rt#4 do' \rt#4 do' |
    \rt#4 do' \rt#4 do' |
    re'8 re'' re'' re'' re''4 sib' |
    la'8 do'' la' do'' << \rt#4 sib' \\ \rt#4 sol' >> |
    \rt#4 <do' la'>8-\sug\ff \rt#4 q |
    \rt#4 <do' la'>8 \rt#4 q |
    q4
  }
>> <fa' la' fa''>4 q q |
q2 q |
q1 |

