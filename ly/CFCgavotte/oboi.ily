\clef "treble"
r2 |
R1 |
r2 <>-\sug\mf \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4 s | sol' s mi'' s | fa'' }
  { sib'4 s | mi' s sol' s | la' }
  { s4 r | s r s r | }
>> r4 r2 |
R1 |
r4 <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''2 sib''8 sol'' | fa''2 mi''4. fa''16 sol'' | fa''2 }
  { sib'2 re''8 sib' | la'2 sol'4. fa'16 mi' | fa'2 }
>>
%%%
r2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { R1 |
    r2 sol''4^"Solo" sol'' |
    sol''1\prall~ |
    sol''\prall~ |
    sol''\prall~ |
    sol''2 do'''4-\tag #'oboe1 -\sug\ff do''' |
    \grace do'''8 si''4 la''8 sol'' do'''4 do''' |
    si''2 si'' |
    do'''4 la''8 fa'' mi''4 re'' |
    do''2 }
  { R1*5 |
    r2 mi''4-\sug\ff mi'' |
    re''2 mi''4 mi'' |
    re''2 re'' |
    do''4 do'' do'' si' |
    do''2 }
>> r2 |
R1 |
r2 <>-\sug\mf \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4 s | sol' s mi'' s | fa'' }
  { sib'4 s | mi' s sol' s | la' }
  { s4 r | s r s r | }
>> r4 r2 |
R1 |
re''2.-\sug\f mi''8 fa'' |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''1~ | sol'' | fa'' |
    sol''4. la''8 sib''4 sol'' | fa''2 mi'' | fa''2 }
  { sol''4 re''2 re''4 | do''1~ | do'' |
    re''2. sib'4 | la'2 sol' | la' }
>>
%%%
r2 |
R1*5 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''1~ | mi'' | fa''4 fa''2 \grace la''4 sol'' | fa''2 }
  { sib'1~ | sib' | la'4 la'2 sib'4 | la'2 }
  { s1\f\> | s4 s2.\! }
>>
%%%
r2 |
do''1-\sug\f~ |
do''4 <>-\sug\sf \twoVoices #'(oboe1 oboe2 oboi) <<
  { do'''2 sib''8 la'' | sol''4 la''8 sol''16 la'' sib''4 sol'' | la'' }
  { do''2 sib'8 la' | sol'4 la'8 sol'16 la' sib'4 sol' | la' }
>> r r2 |
R1*5 |
r2
%%%
r2 |
R1 |
r2 <>-\sug\mf \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4 s | sol' s mi'' s | fa'' }
  { sib' s | mi' s sol' s | la' }
  { s4 r | s r s r | }
>> r4 r2 |
R1 |
re''2.-\sug\f mi''8 fa'' |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''1~ | sol'' | fa'' |
    sol''4. la''8 sib''4 sol'' | fa''2 mi'' | fa'' }
  { sol''4 re''2 re''4 | do''1~ | do'' |
    re''2. sib'4 | la'2 sol' | fa' }
>>
%%%
r2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { R1 |
    r2 sol''4^"Solo" sol'' |
    sol''1\prall~ |
    sol''\prall~ |
    sol''\prall~ |
    sol''2 do'''4-\tag #'oboe1 -\sug\ff do''' |
    \grace do'''8 si''4 la''8 sol'' do'''4 do''' |
    si''2 si'' |
    do'''4 la''8 fa'' mi''4 re'' |
    do''2 }
  { R1*5 |
    r2 mi''4-\sug\ff mi'' |
    re''2 mi''4 mi'' |
    re''2 re'' |
    do''4 do'' do'' si' |
    do''2 }
>> r2 |
R1 |
r2 <>-\sug\mf \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4 s | sol' s mi'' s | fa'' }
  { sib'4 s | mi' s sol' s | la' }
  { s4 r | s r s r | }
>> r4 r2 |
R1 |
re''2.-\sug\f mi''8 fa'' |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''1~ | sol'' | fa'' |
    sol''4. la''8 sib''4 sol'' | fa''2 mi'' |
    fa''4.-\tag #'oboe1 -\sug\ff la''8 do'''4 la'' |
    fa'' do'' la' do'' |
    fa'' fa'' fa'' fa'' | fa''2 fa'' | fa''1 | }
  { sol''4 re''2 re''4 | do''1~ | do'' |
    re''2. sib'4 | la'2 sol' |
    fa'4.-\sug\ff la'8 do''4 la' | fa' do'' la' do'' |
    fa'' la' la' la' | la'2 la' | la'1 | }
>>
%%%
