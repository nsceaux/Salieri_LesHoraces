\clef "alto" r2 |
la4\p r do'2-\sug\sf |
fa2 sol4-\sug\mf r |
do' r mi'2 |
fa' r |
la4-\sug\p r do'2 |
fa8-\sug\f fa' fa' fa' fa' fa' sib sib |
\rt#4 do' \rt#4 do |
fa2
%%%
fa4 fa' |
mi' mi re si |
do'2 r |
sol4^"Pizzicato" r sol r |
do' r r2 |
sol4 r sol r |
do' r do8\ff^"Arco" re mi fad |
sol sol la si do' re' mi' fad' |
sol'2 fa'4.\prall mi'16 fa' |
mi'4 fa' sol' sol |
do'2 r |
la4-\sug\p r do'2-\sug\sf |
fa sol4-\sug\mf r |
do' r mi'2 |
fa'4 r r2 |
la4-\sug\p r do'2-\sug\f |
re'8\f re' re' re' \rt#4 re' |
\rt#4 sol' \rt#4 sol' |
\rt#4 sol' \rt#4 sol' |
\rt#4 fa' \rt#4 fa' |
\rt#4 sib \rt#4 sib |
\rt#4 do' \rt#4 do |
fa2
%%%
re'4^"Pizzicato" r |
la r re' r |
la r re' r |
dod' r re' r |
la r re' r |
la r re' r |
<sol mi'>1\f^"Arco"\>~ 
<<q { s4 s2.\! } >> |
<< fa'4 \\ la >> la8. si16 do'4 <do' do> |
fa2
%%%
r4 fa\f |
mi8. fa16 fa8 mi16 fa sol4 mi |
fa la2-\sug\sf sol8 fa |
mi8. fa16 fa8 mi16 fa sol4 mi |
fa r r2 |
mi'4^"Pizzicato" r re' r |
dod' r do' r |
si r sib r |
la r sol' r |
fa'4 fa'8. sol'16 la'4 la |
re'2
%%%
r2 |
la4\p r do'2(-\sug\sf |
fa2) sol4-\sug\mf r |
do' r mi'2 |
fa' r |
la4-\sug\p r do'2 |
\rt#4 re'8-\sug\f \rt#4 re' |
\rt#4 sol' \rt#4 sol' |
\rt#4 sol' \rt#4 sol' |
\rt#4 fa' \rt#4 fa' |
\rt#4 sib \rt#4 sib |
\rt#4 do' \rt#4 do |
fa2
%%%
fa4 fa' |
mi' mi re si |
do'2 r |
sol4^"Pizzicato" r sol r |
do' r r2 |
sol4 r sol r |
do' r do8\ff^"Arco" re mi fad? |
sol sol la si do' re' mi' fad' |
sol'2 fa'!4.\prall mi'16 fa' |
mi'4 fa' sol' sol |
do'2 r |
la4-\sug\p r do'2-\sug\sf |
fa sol4-\sug\mf r |
do' r mi'2 |
fa'4 r r2 |
la4-\sug\p r do'2-\sug\sf |
re'8-\sug\f re' re' re' \rt#4 re' |
\rt#4 sol' \rt#4 sol' |
\rt#4 sol' \rt#4 sol' |
\rt#4 fa' \rt#4 fa' |
\rt#4 sib \rt#4 sib |
\rt#4 do' \rt#4 do |
\rt#4 <la fa'>8-\sug\ff \rt#4 q |
\rt#4 q \rt#4 q |
fa'4 fa' fa' fa' |
fa'2 fa' |
fa'1 |
