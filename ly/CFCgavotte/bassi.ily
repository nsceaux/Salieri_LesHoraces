<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    \clef "tenor" la8-\sug\p( sib la sib) | do' r fa4( fa'\sf mi') |
    \grace mi'4 re'2 sol4-\sug\mf r | do' r do r |
    fa r la8-\sug\p( sib la sib) | do'8 r fa4( fa'-\sug\sf mi') |
    re'2. sib4 | do'2 do | fa
  }
  \tag #'basso {
    \clef "bass" r2 | fa4\p r la2\sf( |
    sib) sol4\mf r | do' r do r |
    fa r r2 | fa4-\sug\p r la2 |
    sib8\f sib sib sib \rt#4 sib | \rt#4 do' \rt#4 do | fa2
  }
>>
%%%
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    r2 |
    R1 |
    r2 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { mi'4 mi' |
        fa'8 mi' re' do' si la sol fa |
        mi fa sol la si do' re' mi' |
        fa' mi' re' do' si la sol fa |
        mi4 }
      { do'4 do' |
        re'8 do' si la sol fa mi re |
        do re mi fa sol la si do' |
        re' do' si la sol fa mi re |
        do4 }
    >> r4
  }
  \tag #'basso {
    <>^\markup\whiteout [Violoncelli] fa4 fa' |
    mi' mi re si |
    do'2 r |
    sol4^"Pizzicato" ^"[Tutti]" r sol r |
    do r r2 |
    sol4 r sol r |
    do4 r <>^"Arco"
  }
>> do8\ff re mi fad |
sol sol la si? do' re' mi' fad' |
sol'2 fa'!4.\prall mi'16 fa' |
mi'4 fa' sol' sol |
do'2 <<
  \tag #'(fagotto1 fagotto2 fagotti) {
    la8-\sug\p sib! la sib | do' r fa4( fa'-\sug\sf mi') |
    \grace mi'4 re'2-\sug\mf sol4 r | do'4 r do r |
    fa r la8-\sug\p( sib la sib) | do' r fa4( fa'\sf mi') |
    re'1-\sug\f~ | re' | do'~ | do' | sib | do'2 do | fa
  }
  \tag #'basso {
    r2 |
    fa4\p r la2\sf |
    sib2 sol4\mf r |
    do' r do r |
    fa r r2 |
    fa4-\sug\p r la2\sf |
    sib8\f sib sib sib \rt#4 sib |
    \rt#4 sib \rt#4 sib |
    \rt#4 la \rt#4 la |
    \rt#4 la \rt#4 la |
    \rt#4 sib \rt#4 sib |
    \rt#4 do' \rt#4 do |
    fa2
  }
>>
%%%
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    r2 |
    R1*5 |
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { sib1~ | sib | la4 la2 sib4 | la2 }
      { sol1~ | sol | fa4 fa2 sol4 | fa2 }
      { s1\f\> | s4 s2.\! }
    >>
  }
  \tag #'basso {
    re4^"Pizzicato" r | la, r re r |
    la, r re r | dod r re r |
    la, r re r | la, r re r | do1^"Arco"\f\>~ |
    << do { s4 s2.\! } >> | fa4 la,8. si,16 do4 do | fa,2
  }
>>
%%%
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    r4 <>-\sug\f \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { la4 | sol8. la16 la8 sol16 la sib4 sol |
        la do'2-\tag #'fagotto1 -\sug\sf sib8 la |
        sol8. la16 la8 sol16 la sib4 sol | la4 }
      { fa4 | mi8. fa16 fa8 mi16 fa sol4 mi |
        fa4 la2-\sug\sf sol8 fa |
        mi8. fa16 fa8 mi16 fa sol4 mi | fa }
    >> r4 r2 |
    R1*5 |
    r2
  }
  \tag #'basso {
    r4 fa\f |
    mi fa sol mi |
    fa la2\sf sol8 fa |
    mi4 fa sol mi |
    fa4 r r2 |
    mi4^"Pizzicato" r re r |
    dod r do r |
    si, r sib, r |
    la, r sol r |
    fa fa8. sol16 la4 la, |
    re2
  }
>>
%%%
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    la8-\sug\p( sib la sib) | do' r fa4( fa'-\sug\sf mi') |
    \grace mi'4 re'2 sol4-\sug\mf r | do' r do r |
    fa r la8-\sug\p( sib la sib) | do'8 r fa4( fa'\sf mi') |
    re'1-\sug\f~ | re' | do'~ | do' | sib | do'2 do | fa
  }
  \tag #'basso {
    r2 | fa4\p r la2\sf( |
    sib2) sol4\mf r | do' r do r |
    fa r r2 | fa4-\sug\p r la2\sf |
    sib8\f sib sib sib \rt#4 sib | \rt#4 sib \rt#4 sib |
    \rt#4 la \rt#4 la | \rt#4 la \rt#4 la |
    \rt#4 sib \rt#4 sib | \rt#4 do' \rt#4 do | fa2
  }
>>
%%%
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    r2 |
    R1 |
    r2 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { mi'4 mi' |
        fa'8 mi' re' do' si la sol fa |
        mi fa sol la si do' re' mi' |
        fa' mi' re' do' si la sol fa |
        mi4 }
      { do'4 do' |
        re'8 do' si la sol fa mi re |
        do re mi fa sol la si do' |
        re' do' si la sol fa mi re |
        do4 }
    >> r4
  }
  \tag #'basso {
    fa4 fa' |
    mi' mi re si |
    do'2 r |
    sol4^"Pizzicato" r sol r |
    do r r2 |
    sol4 r sol r |
    do r <>^"Arco"
  }
>> do8\ff re mi fad |
sol sol la si do' re' mi' fad' |
sol'2 fa'!4.\prall mi'16 fa' |
mi'4 fa' sol' sol |
do'2 <<
  \tag #'(fagotto1 fagotto2 fagotti) {
    la8-\sug\p sib la sib | do' r fa4( fa'-\sug\sf mi') |
    \grace mi'4 re'2
  }
  \tag #'basso {
    r2 | fa4\p r la2\sf | sib
  }
>> sol4\mf r |
do'4 r do r |
fa r <<
  \tag #'(fagotto1 fagotto2 fagotti) {
    la8-\sug\p( sib la sib) | do'8 r fa4( fa'\sf mi') |
    re'1-\sug\f~ | re' | do'1~ | do' | sib | do'2 do |
  }
  \tag #'basso {
    r2 | fa4-\sug\p r la2\sf |
    sib8\f sib sib sib \rt#4 sib | \rt#4 sib \rt#4 sib |
    \rt#4 la \rt#4 la | \rt#4 la \rt#4 la |
    \rt#4 sib \rt#4 sib | \rt#4 do' \rt#4 do |
  }
>>
fa4.\ff la8 do'4 la |
fa do la, do |
fa fa fa fa |
fa2 fa |
fa1 |
