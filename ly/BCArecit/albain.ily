\clef "bass" R1*2 |
r4 r8 do' la4 r8 la |
la sib do' fa sib^! sib r4 |
r2 r4 r8 re'16 re' |
re'4 la8 sib sol4 r |
R1 |
r2 r4 r8 sib16 sib |
sol4 lab8 sib mib mib r4 |
sib8 sib16 sib sol8 mib lab4^! r |
R1*3 |
r2 r4 r8 sol16 sol |
do'4^! sol8 lab sib4 do'8 sol |
lab4^! r r2 |
R1*4 |
