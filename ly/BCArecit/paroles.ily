\tag #'curiace {
  Quels sont les trois guer -- riers que le choix d’Albe ho -- no -- re ?
}
\tag #'albain {
  Sei -- gneur ! l’i -- gno -- rez- vous en -- co -- re ?
}
\tag #'curiace {
  A -- che -- vez, qui ?
}
\tag #'albain {
  Vos deux frè -- res u -- nis. %& vous.
}
\tag #'camille {
  Cu -- ri -- a -- ce !
}
\tag #'choeur {
  Grands Dieux !
}
\tag #'albain {
  Vous sem -- blez vous con -- fon -- dre,
  Blâ -- me -- riez- vous ce choix ?
}
\tag #'curiace {
  Il a dû me con -- fon -- dre.
  Je m’es -- ti -- mois trop peu pour un si grand hon -- neur.
  Ah ! Ca -- mil -- le !
}
\tag #'albain {
  Au Sé -- nat que di -- rai- je, sei -- gneur ?
}
\tag #'curiace {
  Que mon cœur, pé -- né -- tré de cet -- te grace in -- si -- gne,
  N’o -- soit pas y comp -- ter… mais qu’il s’en ren -- dra di -- gne.
}
