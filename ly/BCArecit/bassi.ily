\clef "bass" do2 r |
R1 |
fa4\p r r2 |
r re4 r |
r2 fad4 r |
r2 sol4 mib\f~ |
mib1~ |
mib2\p~ mib4 r |
R1 |
r2 do~ |
do1~ |
do2 re~ |
re mib~ |
mib1 |
mi!4 r r2 |
fa1~ |
fa~ |
fa~ |
fa4.\f fa8 fa2 |
r2 r4 sol\f |
