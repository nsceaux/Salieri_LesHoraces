\piecePartSpecs
#`((fagotto1 #:notes "fagotti")
   (fagotto2 #:notes "fagotti")
   (violino1 #:notes "violino1")
   (violino2 #:notes "violino2")
   (alto)
   (basso #:instrument "Basso")
   (silence #:on-the-fly-markup , #{ \markup\tacet#20 #}))
