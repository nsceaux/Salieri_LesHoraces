\clef "tenor" R1*5 |
r2 r4 <>-\sug\f <<
  \tag #'(fagotto1 fagotti) \new Voice {
    \tag #'fagotti \voiceOne
    sib4~ | sib1~ | sib2~ sib4
  }
  \tag #'(fagotto2 fagotti) \new Voice {
    \tag #'fagotti \voiceTwo
    sol4~ | sol1~ | sol2~ sol4
  }
  { s4 s1 <>-\sug\p }
>> r4 |
R1*11 |
r2 r4 sol4-\sug\f |
