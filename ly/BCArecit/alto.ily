\clef "alto" do'2 r |
R1 |
fa'4-\sug\p r r2 |
r re'4 r |
r2 re'4 r |
r2 re'4 mib'-\sug\f~ |
mib'1~ |
mib'2-\sug\p~ mib'4 r |
R1 |
r2 do'2~ |
do'1~ |
do'2 sib~ |
sib1~ |
sib |
do'4 r r2 |
fa'1~ |
fa'~ |
fa' |
re'4.\f sol'8 sol'2 |
r r4 sol-\sug\f |
