\clef "treble" do'2 r |
R1 |
do''4\p r r2 |
r2 sib'4 r |
r2 re''4 r |
r2 sib'4 <sib' sol''>\f~ |
q2 r |
R1*2 |
r2 lab'2~ |
lab'1~ |
lab'~ |
lab'2 sol'~ |
sol'1~ |
sol'4 r r2 |
do''1~ |
do''~ |
do'' |
si'!4.\f << { sol''8 sol''2 } \\ { si'8 si'2 } >> |
r2 r4 <sol re' si'>4\f |
