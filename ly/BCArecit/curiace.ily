\clef "alto/G_8" r4 r8 sol do' do' do' re' |
sib4 r sib8 sib16 sib do'8 sol |
la la r4 r2 |
r r4 r8 re'16 re' |
sib4 r re'^! r |
R1*4 |
r2 r4 r8 lab16 lab |
do'4 sib8 do' lab lab r lab |
lab lab sib do' sib4^! r8 sib |
sib sib lab sib sol4^! r4 |
reb'4.^! sib8 sol sol r4 |
R1 |
r4 r8 do'16 do' do'4 sib8 do' |
lab4. lab8 lab lab sib do' |
lab?8 lab r do'16 do' do'4 do'8 re' |
si!4 r re'2 |
si!8 si si do' sol^! sol r4 |
