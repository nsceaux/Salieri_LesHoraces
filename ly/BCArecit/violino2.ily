\clef "treble" do'2 r |
R1 |
la'4-\sug\p r r2 |
r fa'4 r |
r2 la'4 r |
r2 sol'4 <sol' sib>4-\sug\f~ |
q1~ |
q2-\sug\p~ q4 r |
R1 |
r2 mib'~ |
mib'1~ |
mib'2 fa'~ |
fa'2 mib' |
reb''1\sf |
do''4 r r2 |
lab'1~ |
lab'1~ |
lab' |
sol'4.\f <re' si'!>8 q2 |
r2 r4 sol'-\sug\f |
