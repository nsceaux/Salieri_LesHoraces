\clef "bass" re2\ff fad4. sol8 |
la4 <<
  \tag #'(fagotto1 fagotto2 fagotti) { la8. la16 }
  \tag #'basso { la8. la16 }
>> la4 la |
la2 r |
R1*6 |
r2 r4 la,8 si,16 dod |
re1 |
<<
  \twoVoices #'(fagotto1 fagotto2 fagotti) <<
    { la1 | si~ | si | la~ |
      la | la~ | la | si~ |
      si | la~ | la | }
    { fad1 | sol~ | sol | fad~ |
      fad | fad~ | fad | sol~ |
      sol | sol~ | sol | }
    { <>-\sug\p s1*4 <>-\sug\ff s1*2 <>-\sug\p s1*4 <>-\sug\ff }
  >>
  \tag #'basso {
    <>\p \repeat unfold 8 \rt#4 re8
    <>\f \repeat unfold 4 \rt#4 re8
    <>\p \repeat unfold 8 \rt#4 re8
    <>\f \repeat unfold 2 \rt#4 re8
  }
>>
re2\ff~ re8 mi16 fad sol la si dod' |
re'4 re'8. re'16 re'4 re' |
re'2 si,8 dod16 re mi fad sold lad |
si4 si8. si16 si4 si |
si2 sol,8 la,16 si, do re mi fad |
sol4 sol8. sol16 sol4 sol |
sol4. mi8 mi4. la8 |
la4. la,8 la,4. la,8 |
re2~ re8 mi16 fad sol la si dod' |
re'4 re'8. re'16 re'4 re' |
re'2 si,8 dod16 re mi fad sold lad |
si4 si8. si16 si4 si |
si2 si, |
<<
  \tag #'basso {
    <>\ff \repeat unfold 4 \rt#4 sold8 |
    \repeat unfold 4 \rt#4 la8 |
    \repeat unfold 4 \rt#4 sold8 |
  }
  \tag #'(fagotto1 fagotto2 fagotti) {
    \clef "tenor" sold1~ | sold | la2 mi' | dod' la |
    mi' si | sold mi | \clef "bass"
  }
>>
la,2~ la,8 si,16 dod re mi fad sold |
la8 si dod' si la sold fad mi |
red2 red4. red8 |
<<
  \tag #'basso {
    red2..\prall dod16 red |
    mi4. mi8 mi4. mi8 |
    mi4. mi8 mi4. mi8 |
    mi4. mi8 mi4. mi8 |
    mi4. mi8 mi4. mi8 |
  }
  \tag #'(fagotto1 fagotti) \new Voice {
    \tag #'fagotti \voiceOne
    red2 \clef "tenor" fad'2
    si4. si8 dod'!4. dod'8 |
    re'4. re'8 dod'4. dod'8 |
    si4. si8 re'4. re'8 |
    dod'1 | \clef "bass"
  }
  \tag #'(fagotto2 fagotti) \new Voice {
    \tag #'fagotti \voiceTwo
    red2 \clef "tenor" fad'2
    sold4. sold8 la4. la8 |
    si4. si8 la4. la8 |
    sold4. sold8 si4. si8 |
    la1 | \clef "bass"
  }
>>
mi2~ mi8 fad16 sold la si dod' red' |
mi'4 mi8. mi16 mi4 mi |
mi2 <<
  \tag #'(fagotto1 fagotto2 fagotti) {
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { sold2( | la si | la1) | }
      { mi2( | fad sold | \tag #'fagotti \once\hideNotes la1) | }
      { <>\sf }
    >>
    R1*12 | \clef "tenor"
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { mi'1 | fad'2 fa'~ | fa' mi' | mi'1 |
        re'2 fad'4 re' | r2 dod'~ | dod' re' | dod'4 }
      { do'1 | si | sold | la2 dod' |
        re' re | r mi~ | mi1 | la4 }
      { s1\f | s\p }
    >> \clef "bass"
  }
  \tag #'basso {
    mi2\sf( | fad sold) | la,4-\markup\italic Pizzicato r r2 | R1 |
    si,4 r r2 | R1 | mi4 r r2 | R1 |
    la,4 r r2 | re4 r r2 | la,4 r r2 | sold,4 r r2 |
    la,4 r r2 | re4 r r2 | dod4 r r2 | R1*7 | r4
  }
>> la,2\f si,8 dod |
re2.\trill dod8 re |
si,4. si,8 dod4. re8 |
mi2.\trill re8 mi |
dod4. dod8 re4. mi8 |
fad4. sold8 sold4.\trill fad16 sold |
la2 r |
sold r |
la r |
sold8 la si la sold fad mi re |
dod la, si, dod re si, dod re |
mi dod re mi fad re mi fad |
sol!4 fad mi re |
dod si, la, la, |
re2 fad4. sol8 |
la4 la8. la16 la4 la |
la2 r |
R1*3 |
r2 r4 la,8\ff si, |
do2 do4. do8 |
do2.\mordent si,8 do |
<<
  \tag #'basso {
    si,1\p | R1*5
  }
  \tag #'(fagotto1 fagotto2 fagotti) {
    si,1\p~ | si,~ | si,~ | si,~ | si,~ | si, |
  }
>>
mi2\ff sol4. la8 |
si4 si8. si16 si4 si |
si4 la8 sol la2~ |
la4 sol8 fad sol2~ |
sol4 fad8 mi fad2~ |
fad4 mi8 red? mi4. mi8 |
mi2 <<
  \tag #'basso { r2 | R1*5 | r2 }
  \tag #'(fagotto1 fagotti) \new Voice {
    \tag #'fagotti \voiceOne
    \clef "tenor" sol'4.\f sol'8 |
    sol'1~ |
    sol'2 sol'( |
    fad' mi') |
    fad' re' |
    dod'! si |
    fad'
  }
  \tag #'(fagotto2 fagotti) \new Voice {
    \tag #'fagotti \voiceTwo
    \clef "tenor" mi'4.-\tag #'fagotto2 \f mi'8 |
    mi'1~ |
    mi'2 mi'( |
    re' dod') |
    re' si |
    lad si |
    fad
  }
>>
<<
  \tag #'basso {
    fad4.\f fad8 |
    sol2.\prall fad8 sol |
    fad2 r |
    R1 |
    r2 fad4.\f fad8 |
    sol2.\mordent fad8 sol |
    fad2 r |
    R1*9 | \allowPageTurn
    r2 r4 fad\p |
    sol( mi si, dod) |
    \repeat unfold 10 \rt#4 re8 |
    <>\f \repeat unfold 4 \rt#4 re8 |
    <>\p \repeat unfold 8 \rt#4 re8 |
    <>\f \repeat unfold 2 \rt#4 re8 |
    re2\ff~ re8 mi16 fad sol la si dod' |
  }
  \tag #'(fagotto1 fagotto2 fagotti) {
    r2 |
    R1 |
    r2 fad' |
    sol'1 |
    fad'2 r |
    R1 |
    r2 fad' |
    sol'1 |
    fad'2
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { fad'2~ | fad' mi'4 re' | dod'2 re'~ | re' dod'4 si |
        lad2 fad'~ | fad' mi'4 re' | dod'2 re'~ | re' dod'4 si |
        lad2 }
      { re'2~ | re' dod'4 si | lad2 si~ | si dod'4 re' |
        dod'2 re'~ | re'2 dod'4 si | lad2 si~ | si dod'4 re' |
        dod'2 }
      { s2\f | s s\p | s s\f | s s\p |
        s s\f | s s\p | s s\f | s s\p }
    >> fad'2 |
    sol'!1 |
    fad'4 r r2 | \clef "bass"
    \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { la1 | si~ | si | la~ |
        la | la~ | la | si~ |
        si | la~ | la | la | }
      { fad1 | sol~ | sol | sol~ |
        sol | fad~ | fad | sol~ |
        sol | sol~ | sol | fad }
      { s1*4 <>-\sug\f s1*2 <>-\sug\p s1*4 <>-\sug\f }
    >>
  }
>>
re'4 re'8. re'16 re'4 re' |
do'2 do8 re16 mi fad sol la si |
do'4 do'8. do'16 do'4 do' |
si2 si,8 dod16 red mi fad sold lad |
si4 si8. si16 si4 la! |
sold1\sf |
sol!4 sol8. sol16 sol4 sol |
fad2. re8 mi16 fad |
sol2. mi8 fad16 sol |
<<
  \tag #'basso { \repeat unfold 4 \rt#4 la8 | sold1\sf }
  \tag #'(fagotto1 fagotto2 fagotti) { la1~ | la | sold1 | }
>>
sol!4 sol8. sol16 sol4 sol |
fad2. re8 mi16 fad |
sol2. mi8 fad16 sol |
<<
  \tag #'basso { \rt#4 la8 \rt#4 la | \rt#4 la, \rt#4 la, | }
  \tag #'(fagotto1 fagotto2 fagotti) { la1~ | la | }
>>
re4 re2 mi8 fad |
sol2.\trill fad8 sol |
mi4. mi8 fad4. sol8 |
la2.\prall sol8 la |
fad4. fad8 sol4. la8 |
si4. dod'8 dod'4.\trill si16 dod' |
re'2 r |
dod' r |
re'2 r |
la r |
re'8 dod' si la sol fad mi re |
dod re mi re dod la, si, dod |
re mi fad sol la la, si, dod |
re mi fad sol la la, si, dod |
re2 r4 r8 re |
re2 re |
re1 |
