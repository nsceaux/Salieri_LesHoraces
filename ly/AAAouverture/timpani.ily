\clef "bass" R1*3 |
<>-\sug\ff \repeat unfold 3 \rt#4 re16 la,4 |
re r r2 |
r4 re8. re16 re4 la, |
re4. la,8 re2 |
re4 re8. re16 re4 la, |
\repeat unfold 2 \rt#8 re16 |
re4 re8. re16 re4 la, |
re2 r |
R1*4 |
<>-\sug\ff la,4 la,8. la,16 la,4 la, |
re2 r |
R1*4 |
<>-\sug\ff la,4 la,8. la,16 la,4 la, |
\repeat unfold 2 \rt#8 re16 |
re4 re8. re16 re4 re |
re2 \rt#8 re16 |
re4 re8. re16 re4 re |
re2 \rt#8 re16 |
re4 re8. re16 re4 re |
re4 r r2 |
r4 la,8. la,16 la,4 la, |
\repeat unfold 2 \rt#8 re16 |
re4 re8. re16 re4 re |
\repeat unfold 2 \rt#8 re16 |
re4 re8. re16 re4 re |
re2 r |
R1*6 |
\repeat unfold 4 \rt#16 la,16 |
re4 r r2 |
R1*33 |
la,2-\sug\f r |
re r |
la, r |
re r |
\repeat unfold 4 \rt#16 la,16 |
re2 r |
R1*2 |
\rt#12 re16 la,4 |
re4 re8. re16 re4 re |
re re8. re16 re4 la, |
re4. la,8 re2 |
R1*43 |
la,4-\sug\f la,8. la,16 la,4 la, |
re2 r |
R1*4 |
la,4\f la,8. la,16 la,4 la, |
\rt#16 re16 |
re4 re8. re16 re4 re |
\rt#16 re16 |
re4 re8. re16 re4 re |
la,4 r r2 |
R1*2 |
la,4 la,8 la, la,4 la, |
re4. re8 re4. re8 |
re4. re8 re4. re8 |
\rt#16 la,16 |
\rt#16 la,16 |
re4 r r2 |
la,4 la,8. la,16 la,4 la, |
re4. re8 re4. re8 |
re4. re8 re4. re8 |
\rt#16 la,16 |
\rt#16 la,16 |
re4 r r2 |
R1*5 |
re2-\sug\f r |
la, r |
re r |
la, r |
re4 re8. re16 re4 re |
la, la,8. la,16 la,4 la, |
re4. re8 la,4. la,8 |
re4. re8 la,4. la,8 |
re2 r4 r8 re |
re2 re |
re1 |
