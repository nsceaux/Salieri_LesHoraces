\clef "treble" re'2\ff fad'4. sol'8 |
la'4 la'8. la'16 la'4 la' |
la'1 |
<<
  \tag #'oboe1 {
    fad''2. mi''4 |
    re'' la'8. la'16 la'4 la' |
    re''2. mi''4 |
    fad''4. mi''8 re''2 |
    la''2. sol''4 |
    fad''4. mi''8 re''4. mi''8 |
    fad''2. mi''4 |
    re''2
  }
  \tag #'(oboe2 oboe2-conducteur) {
    re''2. la'4 |
    fad' fad'8. fad'16 fad'4 fad' |
    fad'2. dod''4 |
    re''4. la'8 fad'2 |
    fad''2. mi''4 |
    re''4. la'8 fad'4. dod''8 |
    re''2. dod''4 |
    re''2
  }
>> r2 |
R1*4 |
la'4\ff la'8. la'16 la'4 la' |
re''1 |
R1*4 |
la''4-\sug\ff la''8. la''16 la''4 la'' |
<<
  \tag #'oboe1 {
    re'''1~ | re'''~ | re'''~ | re''' | re'''2
  }
  \tag #'(oboe2 oboe2-conducteur) {
    fad''1~ | fad''~ | fad''~ | fad'' | fad''2
  }
>> si'8 do''16 re'' mi'' fad'' sol'' la'' |
si''4 si''8. si''16 si''4 si'' |
si''4. <<
  \tag #'oboe1 {
    sol''8 sol''4. mi''8 |
    mi''4. dod''8 dod''4. dod'''8 |
    re'''1~ | re'''~ | re'''~ | re'''~ | re''' |
  }
  \tag #'(oboe2 oboe2-conducteur) {
    si'8 si'4. sol'8 |
    sol'4. mi'8 mi'4. mi''8 |
    fad''1~ | fad''~ | fad''~ | fad''~ | fad'' |
  }
>>
mi'1~ |
mi'4 si' sold'4.\trill fad'16 mi' |
la'1~ |
la' |
mi''~ |
mi''4 si'' sold''4. fad''16 mi'' |
la'2~ la'8 si'16 dod'' re'' mi'' fad'' sold'' |
la''8 si'' dod'''! si'' la'' sold'' fad'' mi'' |
red''2 do'''~ |
do''' si''4 la'' |
<<
  \tag #'oboe1 {
    sold''4. sold''8 la''4. la''8 |
    si''4. si''8 la''4. la''8 |
    sold''4. sold''8 si''4. si''8 |
    la''4. la''8 dod''4. dod''8 |
    si'1~ |
    si'4
  }
  \tag #'(oboe2 oboe2-conducteur) {
    si'4. si'8 dod''!4. dod''8 |
    re''4. re''8 dod''4. dod''8 |
    si'4. si'8 re''4. re''8 |
    dod''4. dod''8 la'4. la'8 |
    sold'1~ |
    sold'4
  }
>> mi''8. mi''16 mi''4 mi'' |
mi''2 r |
R1 |
<<
  \setMusic #'solo {
    dod''1~ |
    dod''2( fad''4 mi'' |
    mi'' re'' re''2)~ |
    re''4 fad''4( re'' si' |
    la' sold' mi''2)~ |
    mi''4 mid'' fad'' re'' |
    sid'? dod'' dod''2 |
    fad''2. sold''8 la'' |
    la''4 mi'' mi''2~ |
    mi''4 re''( dod'' re'') |
    sid'? dod'' dod''2 |
    fad''2. sold''8 la'' |
    la''4 mi'' mi''2~ |
    mi''4 la''\sf( si'' do''') |
    red''1 |
    re''! |
    dod''4 la''2 sol''!4 |
    mid''( fad'' re'' si') |
    la'1~ |
    la'2 \grace dod''4 si'2 |
    la' r |
  }
  \tag #'oboe1 { <>^"Solo" \keepWithTag #'() \solo }
  \tag #'oboe2 <<
    \new CueVoice { <>^"Oboe I solo" \solo }
    \\ R1*21
  >>
  \tag #'oboe2-conducteur R1*21
>>
R1*5 |
la'1\f |
mi''\f |
la''\f |
mi''\f |
la'2 r |
R1*3 |
re'2 fad'4. sol'8 |
la'4 la'8. la'16 la'4 la' |
la'2 r |
<<
  \tag #'oboe1 {
    fad''2. mi''4 |
    re'' la'8. la'16 la'4 la' |
    re''2. mi''4 |
    fad''4. mi''8 re''2 |
  }
  \tag #'(oboe2 oboe2-conducteur) {
    re''2. la'4 |
    fad'4 fad'8. fad'16 fad'4 fad' |
    fad'2. la'4 |
    re''4. la'8 fad'2 |
  }
>>
R1*2 |
<<
  \setMusic #'solo {
    r2 si''2~ |
    si''4 la'' sol'' fad'' |
    fad'' sol'' sol''2~ |
    sol''4 si'' sol'' mi'' |
    red'' si'' si''2~ |
    si''4 la'' sol'' fad'' |
    sol''2 r |
  }
  \tag #'oboe1 << { s2 <>^"Solo" } \keepWithTag #'() \solo >>
  \tag #'oboe2 <<
    \new CueVoice << { s2 <>^"Oboe I solo" } \solo >>
    \\ R1*7
  >>
  \tag #'oboe2-conducteur R1*7
>>
R1*5
<<
  \setMusic #'solo {
    do'''1\f~ |
    do'''2 si'' |
    lad''1~ |
    lad'' |
    si''4( fad'') r fad'' |
    fad'' mi''2 re''4 |
    re'' dod'' r lad'' |
    si''( re'''2 dod'''8 si'') |
    si''4( lad'') r lad'' |
    si''( sol'' fad'' mid'') |
    \grace mid''?4 fad''2 r4 lad'' |
    si'' re'''2 dod'''8 si'' |
    si''4( lad'') r lad'' |
    si''( sol'' fad'' mid'') |
    fad''1~ | fad''~ | fad''~ | fad''~ |
    fad''~ | fad''~ | fad''~ | fad''~ |
    fad'' | sol''! | fad''4 r r2 |
  }
  \tag #'oboe1 \keepWithTag #'() \solo
  \tag #'oboe2 <<
    \new CueVoice { <>^"Oboe I" \solo }
    \\ R1*25
  >>
  \tag #'oboe2-conducteur R1*25
>>
R1*4 |  \allowPageTurn
la'4-\sug\f la'8. la'16 la'4 la' |
re''1 |
R1*4 |
la''4\f la''8. la''16 la''4 la'' |
<<
  \tag #'oboe1 {
    re'''1~ |
    re'''~ |
    re''' |
    re'''4 fad''8. fad''16 fad''4 fad'' |
    fad''1~ |
    fad'' |
    mi''-\sug\sf |
    la''-\sug\sf |
    la''4. la''8 la''4. la''8 |
    si''4. si''8 si''4. si''8 |
    fad''1 |
    mi'' |
    mi''\sf |
    la''-\sug\sf |
    la''4. la''8 la''4. la''8 |
    si''4. si''8 si''4. si''8 |
    fad''1 |
    mi'' |
  }
  \tag #'(oboe2 oboe2-conducteur) {
    fad''1~ |
    fad''~ |
    fad'' |
    fad''4 re''8. re''16 re''4 re'' |
    red''1~ |
    red'' |
    si'-\sug\sf |
    dod''-\sug\sf |
    re''4. re''8 re''4. re''8 |
    re''4. re''8 re''4. re''8 |
    re''1 |
    dod'' |
    si'-\sug_\sf |
    dod''-\sug\sf |
    re''4. re''8 re''4. re''8 |
    re''4. re''8 re''4. re''8 |
    re''1 |
    dod'' |
  }
>>
re''4 re'2 mi'8 fad' |
sol'2.\mordent fad'8 sol' |
mi'4. mi'8 fad'4. sol'8 |
la'2.\prall sol'8 la' |
fad'4. fad'8 sol'4. la'8 |
si'4. dod''8 dod''4.\trill si'16 dod'' |
re''1\f |
la''\f |
re'''4 la'' sol''8 fad'' mi'' re'' |
dod'' re'' mi'' re'' dod'' la' si' dod'' |
re'' dod'' si' la' sol' fad' mi' re' |
la'1 |
re'8 mi' fad' sol' la' la' si' dod'' |
re'' mi'' fad'' sol'' la'' la'' si'' dod''' |
re'''2 r4 r8 re' |
re'2 re' |
re'1 |
