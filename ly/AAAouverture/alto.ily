\clef "alto" re2\ff fad4. sol8 |
la4 la8. la16 la4 la |
la2 r |
R1*6 |
r2 r4 la8 si16 dod' |
re'1 |
fad4-\sug\p fad8. fad16 fad4. fad8 |
sol1~ |
sol4 sol8. sol16 sol4. sol8 |
sol1~ |
sol4-\sug\f sol8. sol16 sol4 sol |
fad1 |
fad'4-\sug\p fad'8. fad'16 fad'4. fad'8 |
sol'1~ |
sol'4 sol'8. sol'16 sol'4. sol'8 |
sol'1~ |
sol'4\f sol'8. sol'16 sol'4 sol' |
fad'2\ff~ fad'16 re mi fad sol la si dod' |
re'4 re'8. re'16 re'4 re' |
re'2 re4 mi16 fad sold lad |
si4 si8. si16 si4 si |
si2 sol8 la16 si do' re' mi' fad' |
sol'4 sol'8. sol'16 sol'4 sol' |
sol'4. sol'8 sol'4. dod''8 |
dod''4. mi''8 mi''4. dod'8 |
re'2~ re'8 mi'16 fad' sol' la' si' dod'' |
re''4 re''8. re''16 re''4 re'' |
re''2 si8 dod'16 re' mi' fad' sold' lad' |
si'4 si'8. si'16 si'4 si' |
si'2 si |
<< { \repeat unfold 12 \rt#4 mi'8 } \\
  { \repeat unfold 4 \rt#4 si8
    \repeat unfold 4 \rt#4 dod'
    \repeat unfold 4 \rt#4 si8 } >>
la2~ la8 si16 dod' re' mi' fad' sold' |
la'8 si' dod'' si' la' sold' fad' mi' |
red'2 do''2~ |
do''( si'4 la') |
<< { sold'4. sold'8 la'4. la'8 |
    si'4. si'8 la'4. la'8 |
    sold'4. sold'8 si'4. si'8 |
    la'4. la'8 dod'4. dod'8 | } \\
  { si4. si8 dod'!4. dod'8 |
    re'4. re'8 dod'4. dod'8 |
    si4. si8 re'4. re'8 |
    dod'4. dod'8 la4. la8 | }
>>
mi2~ mi8 fad16 sold la si dod' red' |
mi'4 mi'8. mi'16 mi'4 mi' |
mi'2 mi'-\sug\sf( |
red' re' |
dod') r |
R1*19 |
r4 la2\f si8 dod' |
re'2.\trill dod'8 re' |
si4. si8 dod'4. re'8 |
mi'2.\trill re'8 mi' |
dod'4. dod'8 re'4. mi'8 |
fad'4. sold'8 sold'4.\trill fad'16 sold' |
<< <la' mi'>2 \\ la >> r |
<< <si' mi'> \\ si >> r |
<< <dod'' mi'> \\ la >> r |
<< si' \\ mi' >> r |
<< <dod'' mi'>8 \\ la >> la8 si dod' re' si dod' re' |
mi' dod' re' mi' fad' re' mi' fad' |
sol'!4 fad' mi' re' |
dod' si la la |
re'2 fad'4. sol'8 |
la'4 la'8. la'16 la'4 la' |
la'2 r |
R1*3 |
r2 r4 la8\ff si |
do'2 do'4. do'8 |
do'2.\mordent si8 do' |
si4 si\p si si |
si si si si |
r si si si |
si si si si |
si si si si |
si si si si |
mi2\ff sol4. la8 |
si4 si8. si16 si4 si |
si la8 sol la2~ |
la4 sol8 fad sol2~ |
sol4 fad8 mi fad2~ |
fad4 mi8 red? mi4. mi8 |
mi'2 r |
R1*5 |
r2 r4 lad'-\sug\f |
si' re''2 dod''8 si' |
si'4( lad') r2 |
R1 |
r2 r4 lad'\f |
si' re''2 dod''8 si' |
si'4 lad' r2 |
R1*9 | \allowPageTurn
r2 r4 fad'\p |
sol'( mi' si dod') |
\repeat unfold 2 \rt#4 re'8 |
fad4 fad8. fad16 fad4. fad8 |
sol1~ |
sol4 sol8. sol16 sol4. sol8 |
sol1~ |
sol4-\sug\f sol8. sol16 sol4. sol8 |
fad1 |
fad'4-\sug\p fad'8. fad'16 fad'4. fad'8 |
sol'1~ |
sol'4 sol'8. sol'16 sol'4. sol'8 |
sol'1~ |
sol'4\f sol'8. sol'16 sol'4 sol' |
fad'2\ff~ fad'16 re' mi' fad' sol' la' si' dod'' |
re''4 re''8. re''16 re''4 re'' |
do''2 do'8 re'16 mi' fad' sol' la' si' |
do''4 do''8. do''16 do''4 do'' |
si'2 si8 dod'16 red' mi' fad' sold' lad' |
si'4 si'8. si'16 si'4 la'! |
sold'1\sf |
sol'!4 sol'8. sol'16 sol'4 sol' |
fad'4. <<
  { la'8 la'4. la'8 |
    si'4. si'8 si'4. si'8 | } \\
  { re'8 re'4. re'8 |
    re'4. re'8 re'4. re'8 | }
>>
\repeat unfold 2 \rt#4 la'8 |
\repeat unfold 2 \rt#4 la8 |
sold1\sf |
sol!4 sol8. sol16 sol4. sol8 |
fad4. <<
  { la'8 la'4. la'8 |
    si'4. si'8 si'4. si'8 | } \\
  { re'8 re'4. re'8 |
    re'4. re'8 re'4. re'8 | }
>>
\repeat unfold 2 \rt#4 la'8 |
\repeat unfold 2 \rt#4 la8 |
re'4 re'2 mi'8 fad' |
sol'2.\trill fad'8 sol' |
mi'4. mi'8 fad'4. sol'8 |
la'2.\prall sol'8 la' |
fad'4. fad'8 sol'4. la'8 |
si'4. dod''8 dod''4.\trill si'16 dod'' |
<< <re'' fad'>4 \\ la >> re'8 mi' \grace sol'4 fad'4 mi'8 re' |
<< mi'4 \\ la >> la'8 si' \grace re''4 dod'' si'8 la' |
re''8 dod'' si' la' sol' fad' mi' re' |
dod' re' mi' re' dod' la si dod' |
re' dod' si la sol fad mi re |
la4 mi'8 re' dod' la si dod' |
re' mi' fad' sol' la' la si dod' |
re' mi' fad' sol' la' la si dod' |
re'2 r4 r8 re' |
re'2 re' |
re'1 |
