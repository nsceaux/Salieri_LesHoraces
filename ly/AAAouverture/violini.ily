\clef "treble" re'2\ff fad'4. sol'8 |
la'4 << { la'8. la'16 la'4 la' } \\ { la'8. la'16 la'4 la' } >> |
la'16 <<
  { la''16[ la'' la''] \rt#4 la''16 \rt#8 la''16 |
    \repeat unfold #12 \rt#8 la''16 |
    \rt#8 la''16 la''4
  }
  \\ { la'16[ la' la'] \rt#4 la'16 \rt#8 la'16 |
    \repeat unfold #12 \rt#8 la'16 |
    \rt#8 la'16 la'4
  }
>> la8 si16 dod' |
<<
  \tag #'violino1 {
    re'2~ re'4.*2/3 la8\p si16 dod' |
    re'4 re'8. re'16 re'4 fad'8 re' |
    si2~ si4 dod'16 re' mi' fad' |
    sol'4 sol'8. sol'16 sol'4.*5/6 fad'16 mi' re' |
    dod'2~ dod'4 re'16 mi' fad' sol' |
    la'4\f << { la'8. la'16 la'4 la' | re''2~ re''4.*2/3 }
      \\ { la'8. la'16 la'4 la' | re'2 ~ re'4.*2/3 }
    >> la'8\p si'16 dod'' |
    re''4 re''8. re''16 re''4 fad''8 re'' |
    si'2~ si'4 dod''16 re'' mi'' fad'' |
    sol''4 sol''8. sol''16 sol''4. fad''16 mi''32 re'' |
    dod''2~ dod''4 re''16 mi'' fad'' sol'' |
    la''4\f <<
      { la''8. la''16 la''4 la'' |
        <>_\ff \repeat unfold 16 \rt#4 re'''16 |
        re'''2
      }
      \\ { la'8. la'16 la'4 la'
        \repeat unfold 4 \rt#16 re''16 |
        re''2
      }
    >>
  }
  \tag #'violino2 {
    re'1 |
    la4-\sug\p la8. la16 la4. la8 |
    si1~ |
    si4 si8. si16 si4. si8 |
    la1 |
    dod'4-\sug\f dod'8. dod'16 dod'4 dod' |
    re'1 |
    la'4-\sug\p la'8. la'16 la'4. la'8 |
    si'1~ |
    si'4 si'8. si'16 si'4. si'8 |
    la'1 |
    <>-\sug\f dod''4 dod''8. dod''16 dod''4 dod'' |
    <>-\sug\ff <<
      { \repeat unfold 16 \rt#4 fad''16 | fad''2 }
      \\ { \repeat unfold 4 \rt#16 re''16 | re''2 }
    >>
  }
>> si'8 do''16 re'' mi'' fad'' sol'' la'' |
<<
  { si''4 si''8. si''16 si''4 si'' |
    si''4. sol''8 sol''4. mi''8 |
    mi''4. dod''8 dod''4. sol'8 |
    fad'4 }
  \\ { si'1 |
    si'4. si'8 si'4. sol'8 |
    sol'4. mi'8 mi'4. la8 |
    la4 }
>>
<<
  \tag #'violino1 {
    << { \repeat unfold 19 \rt#4 re'''16 }
      \\ { \rt#12 re''16 \repeat unfold 4 \rt#16 re''16 } >>
    mi'2~ mi'8 fad'16 sold' la' si' dod'' red'' |
    mi''4 si' sold'4.\prall fad'16 mi' |
    la'2 mi' |
    dod' la |
    mi''~ mi''8 fad''16 sold'' la'' si'' dod''' red''' |
    mi'''4 si'' sold''4.\prall fad''16 mi'' |
    la''4 la~ la8 si16 dod' re' mi' fad' sold' |
  }
  \tag #'violino2 {
    << { \repeat unfold 19 \rt#4 fad''16 }
      \\ { \rt#12 re''16 \repeat unfold 4 \rt#16 re''16 } >>
    <mi' si>1~ |
    q |
    <la' la>2~ la'8 si'16 dod'' re'' mi'' fad'' sold'' |
    la''8 sold'' fad'' mi'' re'' dod'' si' la' |
    mi''2 si' |
    sold' mi' |
    la2~ la8 si16 dod' re' mi' fad' sold' |
  }
>>
la'8 si' dod'' si' la' sold' fad' mi' |
red'1\startTrillSpan red'2.\stopTrillSpan dod'8 red' |
mi' sold' si' mi'' la' dod'' mi'' la'' |
sold'' si'' sold'' mi'' la'' mi'' dod'' la' |
mi'' si' sold' mi' re' si mi' re' |
dod' la dod' mi' la' mi' dod' la |
mi'2~ mi'8 fad'16 sold' la' si' dod'' red'' |
mi''4 <mi'' mi'>8. q16 q4 q |
q2 <<
  \tag #'violino1 {
    mi''2\sf( |
    red'' re'') |
    dod''4 la\sf( dod' mi') |
    la'( mi' dod' la) |
    r4 si( re' fad') |
    si'2 si |
    r4 mi'( sold' si') |
    sold2 sold' |
    r4 mi'( dod' la) |
    r la la'( fad') |
    r4 la la'( mi') |
    r si mi'( re') |
    r la mi'( dod') |
    r la la'( fad') |
    r la la'( mi') |
    R1*7 | \allowPageTurn
  }
  \tag #'violino2 {
    sold'2-\sug\sf( |
    la' si') |
    mi'4 r r2 |
    R1*19 | \allowPageTurn
  }
>>
r4 la2\f si8 dod' |
re'2.\trill dod'8 re' |
si4. si8 dod'4. re'8 |
mi'2.\trill re'8 mi' |
dod'4. dod'8 re'4. mi'8 |
fad'4. sold'8 sold'4.\trill fad'16 sold' |
la'4 la'8 si' \grace re''4 dod''4 si'8 la' |
mi''4 mi''8 fad'' \grace la''4 sold''4 fad''8 mi'' |
la''8 sold'' fad'' mi'' re'' dod'' si' la' |
sold' la' si' la' sold' fad' mi' re' |
dod' la si dod' re' si dod' re' |
mi' dod' re' mi' fad' re' mi' fad' |
sol'! la' fad' sol' mi' fad' re' mi' |
dod' re' si dod' la si dod' la |
re'2 fad'4. sol'8 |
la'4 la'8. la'16 la'4 la' |
la'16 <<
  { la''16[ la'' la''] \rt#4 la''16 \rt#8 la''16 |
    \repeat unfold #7 \rt#8 la''16 la''4
  }
  \\ { la'16[ la' la'] \rt#4 la'16 \rt#8 la'16 |
    \repeat unfold #7 \rt#8 la'16 la'4
  }
>> la8\ff si |
do'2 do'4. do'8 |
do'2..\trill si16 do' |
si4 <<
  \tag #'violino1 {
    fad'\p fad' fad' |
    fad' fad' fad' fad' |
    r4 sol' sol' sol' |
    sol' sol' sol' sol' |
    la'1( |
    fad') |
  }
  \tag #'violino2 {
    red'4\p red' red' |
    red' red' red' red' |
    r4 mi' mi' mi' |
    mi'4 mi' mi' mi' |
    fad'1( |
    red') |
  }
>>
mi'2 r |
R1 |
do''2\ff fad''4. la''8 |
si'2 mi''4. sol''8 |
la'2 red''4. fad''8 |
sol'2 si'4. mi''8 |
sol''2 r |
R1*5 |
r2 fad'4.\f fad'8 |
sol'2.^\markup\italic trille _\mordent fad'8 sol' |
fad'2 r |
R1 |
r2 fad'4.\f fad'8 |
sol'2.^\markup\italic trille _\mordent fad'8 sol' |
fad'2 r |
R1*9 | \allowPageTurn
r2 r4 fad'\p |
sol'!( mi' si dod') |
re'2. la8 si16 dod' |
<<
  \tag #'violino1 {
    re'4 re'8. re'16 re'4 fad'8 re' |
    si2. dod'16 re' mi' fad' |
    sol'4 sol'8. sol'16 sol'4~ sol'16 fad' mi' re' |
    dod'2. re'16 mi' fad' sol' |
    la'4\f << { la'8. la'16 la'4 la' } \\ { la'8. la'16 la'4 la' } >> |
    <re' re''>2. la'8\p si'16 dod'' |
    re''4 re''8. re''16 re''4 fad''8 re'' |
    si'2. dod''16 re'' mi'' fad'' |
    sol''4 sol''8. sol''16 sol''4~ sol''16 fad'' mi'' re'' |
    dod''2. re''16 mi'' fad'' sol'' |
    la''4\f <<
      { la''8. la''16 la''4 la'' |
        \repeat unfold 8 \rt#8 re'''16 |
        \repeat unfold 4 \rt#8 red'''16 | } \\
      { la'8. la'16 la'4 la' |
        \repeat unfold 6 \rt#16 fad''16 | }
    >>
    mi'''4 mi'''8. mi'''16 mi'''4 re'''! |
    dod'''2.\mordent si''8 dod''' |
    re'''4.
  }
  \tag #'violino2 {
    la4 la8. la16 la4. la8 |
    si1~ |
    si4 si8. si16 si4. si8 |
    la1 |
    dod'4-\sug\f dod'8. dod'16 dod'4 dod' |
    re'1 |
    la'4-\sug\p la'8. la'16 la'4. la'8 |
    si'1~ |
    si'4 si'8. si'16 si'4. si'8 |
    la'1 |
    dod''4\f dod''8. dod''16 dod''4 dod'' |
     <<
      { \repeat unfold 12 \rt#8 fad''16 |
        mi''1 |
        mi''4 mi''8. mi''16 mi''4 mi'' |
        fad''4. } \\
      { \repeat unfold 4 \rt#16 re''16 |
        \repeat unfold 2 \rt#16 red''16 |
        si'1\sf |
        la'4 la'8. la'16 la'4 la' |
        la'4.
      }
    >>
  }
>> la''8 fad''4. re''8 |
si''4. sol''8 re''4. si'8 |
<<
  \tag #'violino1 {
    la'2.~ la'16 si' dod'' re'' |
    mi''2~ mi''8 fad''16 sold'' la'' si'' dod''' re''' |
    mi'''4 mi'''8. mi'''16 mi'''4 re''' |
    dod'''2.\prall si''8 dod''' |
    re'''4.
  }
  \tag #'violino2 {
    la'8 <<
      { fad''8 fad'' fad'' \rt#4 fad''8 |
        \rt#4 mi'' \rt#4 mi'' |
        mi''1 |
        mi''4 mi''8. mi''16 mi''4. mi''8 |
        fad''4. } \\
      { re''8 re'' re'' \rt#4 re''8 |
        \rt#4 dod'' \rt#4 dod'' |
        si'1\sf |
        la' |
        la'4. }
    >>
  }
>> la''8 fad''4. re''8 |
si''4. sol''8 re''4. si'8 |
<<
  \tag #'violino1 {
    la'2.~ la'16 si' dod'' re'' |
    mi''1\mordent |
    re''4
  }
  \tag #'violino2 {
    la'8 <fad' re''>8[ q q] \rt#4 q |
    \rt#4 <mi' dod''> \rt#4 q |
    <re'' re'>4
  }
>> re'2 mi'8 fad' |
sol'2.\prall fad'8 sol' |
mi'4. mi'8 fad'4. sol'8 |
la'2.\prall sol'8 la' |
fad'4. fad'8 sol'4. la'8 |
si'4. dod''8 dod''4.\trill si'16 dod'' |
<< re''4 \\ re' >> re''8 mi'' \grace sol''4 fad'' mi''8 re'' |
<< la''4 \\ la' >> la''8 si'' \grace re'''4 dod'''4 si''8 la'' |
re'''8 dod''' si'' la'' sol'' fad'' mi'' re'' |
dod'' re'' mi'' re'' dod'' la' si' dod'' |
re'' dod'' si' la' sol' fad' mi' re' |
dod' re' mi' re' dod' la si dod' |
re' mi' fad' sol' la' la' si' dod'' |
re'' mi'' fad'' sol'' la'' la'' si'' dod''' |
re'''2 r4 r8 << re' \\ re' >> |
<< { re'2 re' } \\ { re' re' } >> |
re'1 |
