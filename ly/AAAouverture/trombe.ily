\clef "treble" \transposition re
do'2\ff mi'4. do'8 |
sol'4 sol'8. sol'16 sol'4 sol' |
sol'1 |
<<
  \tag #'(tromba1 trombe) \new Voice {
    \tag #'trombe \voiceOne
    mi''2. re''4 |
    do'' sol'8. sol'16 sol'4 sol' |
    do''2. re''4 |
    mi''4. re''8 do''2 |
    sol''2. fa''4 |
    mi''4. re''8 do''4. re''8 |
    mi''2. re''4 |
    do''2
  }
  \tag #'(tromba2 trombe) \new Voice {
    \tag #'trombe \voiceTwo
    do''2. sol'4 |
    mi' mi'8. mi'16 mi'4 mi' |
    mi'2. sol'4 |
    do''4. sol'8 mi'2 |
    mi''2. re''4 |
    do''4. sol'8 mi'4. sol'8 |
    do''2. sol'4 |
    mi'2
  }
>> r2 |
R1*4 |
sol'4\ff sol'8. sol'16 sol'4 sol' |
do''1 |
R1*4 |
sol'4\ff sol'8. sol'16 sol'4 sol' |
do''1 |
<<
  \tag #'(tromba1 trombe) \new Voice {
    \tag #'trombe \voiceOne
    mi''4 mi''8. mi''16 mi''4 mi'' |
    mi''1~ |
    mi''4 mi''8. mi''16 mi''4 mi'' |
    mi''2
  }
  \tag #'(tromba2 trombe) \new Voice {
    \tag #'trombe \voiceTwo
    do''4 do''8. do''16 do''4 do'' |
    do''1~ |
    do''4 do''8. do''16 do''4 do'' |
    do''2
  }
>> r2 |
r4 do''8. do''16 do''4 do'' |
do''4. re''8 re''4. <<
  \tag #'(tromba1 trombe) \new Voice {
    \tag #'trombe \voiceOne
    re''8 |
    re''4. re''8 re''4. re''8 |
    do''1~ |
    do''4 mi''8. mi''16 mi''4 mi'' |
    mi''1~ |
    mi''4 mi''8. mi''16 mi''4 mi'' |
    mi''1 |
  }
  \tag #'(tromba2 trombe) \new Voice {
    \tag #'trombe \voiceTwo
    sol'8 |
    sol'4. sol'8 sol'4. sol'8 |
    mi'1~ |
    mi'4 do''8. do''16 do''4 do'' |
    do''1~ |
    do''4 do''8. do''16 do''4 do'' |
    do''1 |
  }
>>
R1*2 |
sol'1~ |
sol' |
re''~ |
re'' |
<<
  \tag #'(tromba1 trombe) \new Voice {
    \tag #'trombe \voiceOne sol'1~ | sol' |
  }
  \tag #'(tromba2 trombe) \new Voice {
    \tag #'trombe \voiceTwo sol1~ | sol |
  }
>>
R1*2 |
re''1~ |
re''~ |
re''~ |
re''~ |
re''~ |
re''4 re''8. re''16 re''4 re'' |
re''2 r |
R1*10 |
<>^"[Cors]" re''4\ff re''8. re''16 re''4 re'' |
re''1 |
R1*15 |
<>^"[tous]" sol'1\f |
re''\f |
sol''\f |
re''\f |
<<
  \tag #'(tromba1 trombe) \new Voice {
    \tag #'trombe \voiceOne sol'2
  }
  \tag #'(tromba2 trombe) \new Voice {
    \tag #'trombe \voiceTwo sol2
  }
>> r2 |
R1*3 |
do'2 mi'4. do'8 |
sol'4 sol'8. sol'16 sol'4 sol' |
sol'2 r |
<<
  \tag #'(tromba1 trombe) \new Voice {
    \tag #'trombe \voiceOne
    mi''2. re''4 |
    do'' sol'8. sol'16 sol'4 sol' |
    do''2. re''4 |
    mi''4. re''8 do''2 |
  }
  \tag #'(tromba2 trombe) \new Voice {
    \tag #'trombe \voiceTwo
    do''2. sol'4 |
    mi' mi'8. mi'16 mi'4 mi' |
    mi'2. sol'4 |
    do''4. sol'8 mi'2 |
  }
>>
R1*14 |
<>^"[Cors]" re''2\f re''4. re''8 |
re''1 |
R1*27 |
<>^"[tous]" sol'4-\sug\f sol'8. sol'16 sol'4 sol' |
do''1 |
R1*4 |
sol'4\f sol'8. sol'16 sol'4 sol' |
<<
  \tag #'(tromba1 trombe) \new Voice {
    \tag #'trombe \voiceOne
    do''1~ |
    do''4 do''8. do''16 do''4 do'' |
    do''1~ |
    do''4 do''8. do''16 do''4 do'' |
    mi''1~ |
    mi''4 mi''8. mi''16 mi''4
  }
  \tag #'(tromba2 trombe) \new Voice {
    \tag #'trombe \voiceTwo
    mi'1~ |
    mi'4 mi'8. mi'16 mi'4 mi' |
    mi'1~ |
    mi'4 mi'8. mi'16 mi'4 mi' |
    mi'1~ |
    mi'4 mi'8. mi'16 mi'4
  }
>> mi''4 |
re''1\sf |
re''\sf |
do''4. do''8 do''4. do''8 |
do''4. do''8 do''4. do''8 |
<<
  \tag #'(tromba1 trombe) \new Voice {
    \tag #'trombe \voiceOne mi''1 | re'' |
  }
  \tag #'(tromba2 trombe) \new Voice {
    \tag #'trombe \voiceTwo do''1 | sol' |
  }
>>
re''1\sf |
re''\sf |
do''4. do''8 do''4. do''8 |
do''4. do''8 do''4. do''8 |
<<
  \tag #'(tromba1 trombe) \new Voice {
    \tag #'trombe \voiceOne mi''1 | re'' | do''2
  }
  \tag #'(tromba2 trombe) \new Voice {
    \tag #'trombe \voiceTwo do''1 | sol' | mi'2
  }
>> r2 |
R1*5 |
do'1\f |
sol'\f |
do''\f |
sol' |
do''4 sol' mi' do' |
sol'1 |
do'2 sol' |
do''2 <<
  \tag #'(tromba1 trombe) \new Voice { \tag #'trombe \voiceOne sol''2 }
  \tag #'(tromba2 trombe) \new Voice { \tag #'trombe \voiceTwo sol'2 }
>>
do''2 r4 r8 do' |
do'2 do' |
do'1 |
