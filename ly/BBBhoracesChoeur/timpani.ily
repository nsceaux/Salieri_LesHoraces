\clef "bass" do4-\sug\f r r |
r16 sol,32 sol, sol,16 sol, sol,4 r |
R2. |
r4 r8 sol,16. sol,32 do16 sol, do sol, |
do8. do16 do4 r |
r8. do16\p do8. do16 do8. do16 |
do8. do16 do8. do16 do8. do16 |
do4 r r |
r8. sol,16 sol,8. sol,16 sol,8. sol,16 |
sol,4 r r |
do r r |
r16 do32 do do16 do do4 r |
R2.*7 |
R1*8 |
r2 r4 sol,8.-\sug\ff sol,16 |
\rt#16 do16 |
\rt#16 do16 |
\rt#16 sol, |
\rt#8 do do4 r |
R1 |
r4 do8.-\sug\p do16 do8 do do do |
do4.-\sug\f do16 do \rt#4 do8 |
do4 r r2 |
R1 |
r2 r4 sol,8.-\sug\ff sol,16 |
\rt#16 do16 |
\rt#16 do16 |
\rt#16 sol, |
do4 r r8 do16 do do8 do |
sol,4 r r8 sol,16 sol, sol,8 sol, |
do4 r r2 |
sol,4 r r2 |
R1 |
r2 sol,8-\sug\ff sol,16 sol, sol,8 sol, |
\rt#16 do16 |
sol,4 r sol,8 sol,16 sol, sol,8 sol, |
\rt#16 do16 |
sol,8 sol,16 sol, sol,8 sol, sol,4 do |
sol,8 sol,16 sol, sol,8 sol, sol,4 do |
sol,4 sol,8. sol,16 sol,4 sol, |
sol,2 r |
R1*2 |
