\clef "bass" do4\f sol,8. sol,16 do8. mi16 |
re16 sol,32 sol, sol,16 sol, sol,8. si16 si8. si16 |
do'8 mi fa8. fa16 sol8. sol,16 |
do4 r8 sol16. sol32 mi16-. do-. mi-. sol-. 
do'4 do4 r |
do4\mf do\mf do\mf |
re re re |
re re re |
sol sol sol |
si, si, si, |
do8.\f do16 do'8-. si-. la-. sol-. |
fad8 re16. re32 re4 r |
re-\sug\p re re |
mi mi mi |
la, la, la, |
re re re |
re re re\f |
sol8. re'16 sol'8 re' si sol |
re re16. re32 re8. re16 re8. re16 |
sol,8\fp sol sol sol \rt#4 sol |
\rt#4 sol \rt#4 si |
\rt#4 la \rt#4 fad |
sol4\f r sol,16 sol fad sol re8 si, |
sol,8 la,16 si, do re mi fad
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    sol8 sol, sol sol |
    fad2 fa |
    mi si |
    \clef "tenor" do'4. mi'8 <<
      \tag #'(fagotto1 fagotti) \new Voice {
        \tag #'fagotti \voiceOne fa'4 mi'
      }
      \tag #'(fagotto2 fagotti) \new Voice {
        \tag #'fagotti \voiceTwo re'4 do'
      }
    >> |
    \clef "bass" sol,8-\sug\ff la,16 si, do re mi fad sol la si do' re' mi' fa'! sol' |
    do2 do4 do |
    do2. do'4 |
    sol1 |
    \rt#4 do8 do4 r |
    do1-\sug\p~ |
    do |
    fa-\sug\f |
    fa2 mi |
    re fad4.\prall mi16 fad |
    sol8-\sug\ff sol, sol, sol, sol,4 sol |
    do1 |
    do2. do'4 |
    si1 |
  }
  \tag #'basso {
    sol8 sol, sol, sol, |
    sol, sol sol sol sol, sol sol sol |
    sol, sol sol sol sol, sol sol sol |
    sol,4 sol,8. sol,16 sol,4 sol, |
    sol,8\ff la,16 si, do re mi fad sol4 sol, |
    \rt#4 do8 \rt#4 do |
    \rt#4 do8 \rt#4 do |
    sol,8 sol sol sol \rt#4 sol do16 do' si do' sol8 mi do4 r |
    do\p do do do |
    do do do do |
    <>\f \rt#4 fa8 \rt#4 fa |
    fa4\f r mi\f r |
    re\f r fad\f r |
    sol8\ff sol, sol, sol, sol,16 la, si, do re mi fa! sol |
    \rt#4 do8 \rt#4 do |
    \rt#4 do8 \rt#4 do |
    \rt#4 sol \rt#4 sol, |
  }
>>
do16 do' si do' sol8 mi do4 r |
sol,16 sol fad sol re8 si, sol,4 r |
la4 r fad r |
sol16 sol, la, si, do re mi fad sol8 re si, sol, |
do re16 mi fa sol la si do'8 sol mi do |
sol,4 r r2 |
do2 r |
sol, r |
do r |
sol, r4 do |
sol,2 r4 do |
sol4 sol8. sol16 sol4 sol |
sol2 r |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    R1*2 |
  }
  \tag #'basso {
    R1 |
    r4 r8 sol16 sol fa8-. re-. si,-. sol,-. |
  }
>>
