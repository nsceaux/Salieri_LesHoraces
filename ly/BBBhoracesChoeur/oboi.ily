\clef "treble"
<<
  \tag #'(oboe1 oboi) \new Voice {
    \tag #'oboi \voiceOne do''4
  }
  \tag #'(oboe2 oboi) \new Voice {
    \tag #'oboi \voiceTwo mi'4
  }
  { s4-\sug\f }
>> r r |
r16 sol'32 sol' sol'16 sol' sol'4 r |
r16
<<
  \tag #'(oboe1 oboi) \new Voice {
    \tag #'oboi \voiceOne
    sol''16 \grace la''16 sol''16 fa''32 sol'' la''8. la'16 si'8. si'16 |
    do''4
  }
  \tag #'(oboe2 oboi) \new Voice {
    \tag #'oboi \voiceTwo
    do''16 do''16. do''32 do''8 la'16. fa'32 re'8. re'16 |
    mi'4
  }
>> r16 sol'32 sol' sol'16. sol'32 mi'16 do' mi' sol' |
do''8. do'16 do'4 r |
r8. <<
  \tag #'(oboe1 oboi) \new Voice {
    \tag #'oboi \voiceOne
    mi''16 mi''8. mi''16 mi''8. sol''16 |
    fad''4~ fad''8. fad''16 fad''8. fad''16 |
    fad''2. |
    sol''8. sol''16 sol''8. sol''16 sol''8. sol''16 |
    sol''8. sol'16
  }
  \tag #'(oboe2 oboi) \new Voice {
    \tag #'oboi \voiceTwo
    do''16 do''8. do''16 do''8. mi''16 |
    la'4~ la'8. la'16 la'8. la'16 |
    la'2. |
    si'8. si'16 si'8. si'16 si'8. si'16 |
    re''8. sol'16
  }
  { s16-\sug\p }
>> sol'8. sol'16 sol'8. sol'16 |
mi''8.-\sug\f mi''16 do''8-. si'-. la'-. sol'-. |
fad'16 re'32 re' re'16 re' re'4 r |
<>\p <<
  \tag #'(oboe1 oboi) \new Voice {
    \tag #'oboi \voiceOne
    fad''?2. | sol''~ | sol'' | fad'' |
    la''2 do''4 | si'8. re''16
  }
  \tag #'(oboe2 oboi) \new Voice {
    \tag #'oboi \voiceTwo
    la'2. | dod''~ | dod'' | re'' |
    do''!2 fad'?4 | sol'8. re''16
  }
  { s2.*4 | s2 s4-\sug\f }
>> sol''8 re'' si' sol' |
<<
  \tag #'(oboe1 oboi) \new Voice {
    \tag #'oboi \voiceOne
    re''8 sol''4 sol''8 fad''8. fad''16 |
    sol''2 mi''4 fad'' |
    sol''1 | fad''2 la'' | re''1 | re''2
  }
  \tag #'(oboe2 oboi) \new Voice {
    \tag #'oboi \voiceTwo
    re''8 si'4 si'8 la'8. la'16 |
    si'2 do'' |
    si' re'' | do''1 | si' | si'2
  }
  { s2. | s1-\sug\fp | s1*2 | s2-\sug\f }
>> r |
<<
  \tag #'(oboe1 oboi) \new Voice {
    \tag #'oboi \voiceOne
    la'2 si' | do'' re'' |
    mi''4 mi''8. mi''16 re''4 mi'' | re''
  }
  \tag #'(oboe2 oboi) \new Voice {
    \tag #'oboi \voiceTwo
    fad'2 fa' | mi' si' |
    do''4 do''8. do''16 si'4 do'' | si'
  }
>> r4 r sol'8.-\sug\ff sol'16 |
<<
  \tag #'(oboe1 oboi) \new Voice {
    \tag #'oboi \voiceOne
    do''2 fa'' | mi''2 sol'' | si''1 |
    do'''8 do''' sol'' mi''
  }
  \tag #'(oboe2 oboi) \new Voice {
    \tag #'oboi \voiceTwo
    mi'2 la'4 si' | do''2 mi'' | re''1 |
    do''8 do''' sol'' mi''
  }
>> do''8 do'' sol' mi' |
do'4 <<
  \tag #'(oboe1 oboi) \new Voice {
    \tag #'oboi \voiceOne
    mi''8. mi''16 mi''4 mi'' | sol''1 |
    fa''1 | la''2 sol'' | fa''4
  }
  \tag #'(oboe2 oboi) \new Voice {
    \tag #'oboi \voiceTwo
    sol'8. sol'16 sol'4 sol' | sib'1 |
    la' | do''2 dod'' | re''4
  }
  { s2.-\sug\p | s1 | s1-\sug\f }
>> r4 r2 |
r2 r4 sol'8.-\sug\ff sol'16 |
<<
  \tag #'(oboe1 oboi) \new Voice {
    \tag #'oboi \voiceOne
    do''2 fa'' | mi'' sol'' | si''1 | do'''4
  }
  \tag #'(oboe2 oboi) \new Voice {
    \tag #'oboi \voiceTwo
    mi'2 la'4 si' | do''2 mi'' | re''1 | mi''4
  }
>> r4 r8 <<
  \tag #'(oboe1 oboi) \new Voice {
    \tag #'oboi \voiceOne
    sol''16 sol'' sol''8 do''' | si''4
  }
  \tag #'(oboe2 oboi) \new Voice {
    \tag #'oboi \voiceTwo
    mi''16 mi'' mi''8 mi'' | re''4
  }
>> r4 r8 <<
  \tag #'(oboe1 oboi) \new Voice {
    \tag #'oboi \voiceOne
    re''16 re'' re''8 sol'' | fad''2 la'' | re''4
  }
  \tag #'(oboe2 oboi) \new Voice {
    \tag #'oboi \voiceTwo
    si'16 si' si'8 si' | do''1 | si'4
  }
>> r4 r2 |
r4 r8 sol'' do''' sol'' mi'' do'' |
<<
  \tag #'(oboe1 oboi) \new Voice {
    \tag #'oboi \voiceOne sol''4
  }
  \tag #'(oboe2 oboi) \new Voice {
    \tag #'oboi \voiceTwo si'4
  }
>> r4
<<
  \tag #'(oboe1 oboi) \new Voice {
    \tag #'oboi \voiceOne
    re''8 re''16 re'' re''8 re'' |
    mi'' re'' do'' re'' mi'' fa'' sol'' mi'' |
    re''4
  }
  \tag #'(oboe2 oboi) \new Voice {
    \tag #'oboi \voiceTwo
    sol'8 sol'16 sol' sol'8 sol' |
    do'' sol' mi' sol' do'' re'' mi'' do'' |
    si'4
  }
  { <>-\sug\ff }
>> r4 <<
  \tag #'(oboe1 oboi) \new Voice {
    \tag #'oboi \voiceOne
    re''8 re''16 re'' re''8 re'' |
    mi'' re'' do'' re'' mi'' fa'' sol'' mi'' |
    re'' sol''16 sol'' sol''8 sol'' sol''4 mi'' |
    re''8 sol''16 sol'' sol''8 sol'' sol''4 mi'' |
    re''4 sol''8. sol''16 sol''4 sol'' |
    sol''2
  }
  \tag #'(oboe2 oboi) \new Voice {
    \tag #'oboi \voiceTwo
    sol'8 sol'16 sol' sol'8 sol' |
    do'' sol' mi' sol' do'' re'' mi'' do'' |
    si' si'16 si' si'8 si' si'4 do'' |
    si'8 si'16 si' si'8 si' si'4 do'' |
    si'4 si'8. si'16 si'4 si' |
    si'2
  }
>> r2 |
R1*2 |
