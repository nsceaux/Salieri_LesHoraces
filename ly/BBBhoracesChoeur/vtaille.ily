\clef "vtaille" R2.*19 R1*8 |
r2 r4 sol8 sol |
do'4. do'8 fa'4 fa'8 fa' |
mi'2 sol' |
fa'8 re' re' si sol4 sol8 fa' |
mi'4 do' r2 |
R1*5 |
r2 r4 sol8. sol16 |
do'4. do'8 fa'4 fa'8 fa' |
mi'2 sol' |
fa'4 re'8 si sol4 sol8 fa' |
mi'4 do' r2 |
R1*5 |
mi'2 r |
re'4 si r2 |
mi'8[ re'] do'8 re' mi'4 mi'8 do' |
sol'1~ |
sol'~ |
sol'4 sol r2 |
R1*3 |
