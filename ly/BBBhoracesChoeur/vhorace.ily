\clef "bass" R2.*18 |
r4 r re8. re16 |
sol4. sol8 do'4 do'8 do' |
si2 re' |
do'8 la la fad re4 re8 re'8 |
si4 sol r2 |
r4 r8 re sol4 sol |
la4. la8 si4. si8 |
do'4 do' r re' |
mi' do'8 do' si4 do'8 do' |
sol2 r |
R1*3 |
r2 r4 sol8 sol |
do'2 do'4. re'8 |
sib4. sol8 sol sol do' sol |
la2 r |
la4 la8 la dod'4. dod'8 |
re'4 la8 la la[ si] do'! re' |
si4 sol r sol8. sol16 |
mi4. mi8 la4 si8 si |
do'2 mi' |
re'4 si8 si sol4 sol8 si |
do'4 do r2 |
R1*5 |
do'2 r |
si4 sol r2 |
do'8[ sol] mi8 sol do'4 do'8 do' |
sol1~ |
sol~ |
sol4 sol r2 |
R1*3 |
