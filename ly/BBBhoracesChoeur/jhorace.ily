\clef "tenor/G_8" do'4 sol8. sol16 do'8. mi'16 |
re'8 sol r re' re'8. re'16 |
mi'8 do'16. do'32 la4 si8. si16 |
do'4 r r |
r r sol8. sol16 |
do'2 do'8. mi'16 |
fad4 fad r |
fad4 fad8. la16 re'8. do'16 |
si2 r8 re' |
re'8 sol r4 sol8. sol16 |
mi'8. mi'16 do'8 si la sol |
fad re r4 la8. la16 |
re'2 fad'8. re'16 |
\grace re'8 dod'4 dod'2 |
dod'4 dod'8. re'16 mi'8. sol16 |
fad4 r r8 re' |
do' re r4 do'8. do'16 |
si8. re'16 sol'8 re' si sol |
re'2. |
sol4 r r2 |
R1*21 |
r2 r4 r8 mi' |
re'1 |
do'4 la8 la re'4. do'8 |
si2 re'4. re'8 |
mi' do' do' re' mi'4 mi'8 do' |
<<
  { \voiceOne sol'1~ | sol'~ | sol'~ | sol'~ | sol'~ |
    sol'~ | sol'4 \oneVoice }
  \new Voice {
    \voiceTwo sol1~ | sol~ | sol~ | sol~ | sol~ |
    sol~ | sol4
  }
>> sol4 r2 |
r sol8 sol16 sol sol8 sol |
si4 r8 re' si si do' re' |
sol4 r r2 |
