\clef "treble" <sol mi' do''>4\f sol'8. sol'16 do''8. mi''16 |
re''16 <sol' sol>32 q q16 q q8. re''16 re''8. re''16 |
mi''16. sol''32 \grace la''16 sol''16 fa''32 sol'' la''8. la'16 <sol re' si'>8. si'16 |
<sol mi' do''>4 r16 <sol' sol>32 q q16. q32 mi'16 do' mi' sol' |
<sol mi' do''>8. do'16 do'4 r |
<>\p \repeat percent 3 { sol16*4/6-. do'( re' mi' re' do') }
\repeat percent 3 { la-. fad'( sol' la' sol' fad') }
\repeat percent 3 { la-. fad'( sol' la' sol' fad') }
sol'-. sol'( la' si' la' sol') re''-. sol'( la' si' la' sol') re'-. sol'( la' si' la' sol') |
re''-. sol'( la' si' la' sol') re'-. sol' la' si' la' sol' re' sol' la' si' la' sol' |
mi''8.\f mi''16 do'''16. do'''32 si''16. si''32 la''16. la''32 sol''16. sol''32 |
fad''16 re'32 re' re'16 re' re'4 r |
<>\p \repeat percent 3 { la16*4/6-. re'( mi' fad' mi' re') } |
sol' mi' fad' sol' fad' mi' dod' mi' fad' sol' fad' mi' dod' mi' fad' sol' fad' mi' |
\repeat percent 3 { dod' mi' fad' sol' fad' mi' } |
fad' fad' sol' la' sol' fad' re' fad' sol' la' sol' fad' re' fad' sol' la' sol' fad' |
<<
  \repeat percent 3 { re'-. fad'( sol' la' sol' fad') }
  { s2 s4\f }
>>
sol'8 r16 re'' sol''16. sol''32 re''16. re''32 si'16. si'32 sol'16. sol'32 |
<<
  \tag #'violino1 {
    re''16 sol''32 si'' re'''16 re''' re'''8. re'16 re'8. re'16 |
    si16\fp sol' fad' sol' la' sol' fad' sol' do'' do'' si' do'' re'' do'' si' do'' |
    si'
  }
  \tag #'violino2 {
    si'16 <si' sol''>32 q q16 q q4 <re' la' fad''> |
    <re' si' sol''>16-\sug\fp sol' fad' sol' la' sol' fad' sol' \rt#4 mi'16 \rt#4 fad' |
    sol'
  }
>> re' mi' fad' sol' la' si' do'' re'' si' sol' si' re'' si' re'' si' |
do'' la' si' do'' re'' mi'' fad'' sol'' la'' sol'' fad'' mi'' re'' do'' si' la' si'\f sol'' fad'' sol'' re'' sol'' si' re'' sol' sol' fad' sol' re' sol' si re' |
sol8 la16 si do' re' mi' fad' sol'8 sol sol sol |
la16\fp la' sol' la' si' la' sol' la' si\fp si' la' si' do'' si' la' si' |
do'\fp do'' si' do'' re'' do'' si' do'' re'\fp re'' do'' re'' mi'' re'' do'' re'' |
mi'' do'' si' do'' re'' do'' si' do'' si' sol' la' si' do'' sol' mi' do' |
si\ff sol la si do' re' mi' fad' sol' la' si' do'' re'' mi'' fa''! sol'' |
<<
  \tag #'violino1 {
    mi'16 do'' si' do'' re'' do'' si' do'' fa'' fa'' mi'' fa'' sol'' fa'' mi'' fa'' |
    mi''
  }
  \tag #'violino2 {
    \rt#8 mi'16 \rt#4 la' \rt#4 si' |
    do''
  }
>> sol'16 la' si' do'' re'' mi'' fa'' sol'' mi'' do'' mi'' sol'' mi'' sol'' mi'' |
fa'' re'' mi'' fa'' sol'' la'' si'' do''' re''' do''' si'' la'' sol'' fa'' mi'' re'' |
do'' do'' si' do'' sol' do'' mi' sol' do' do'' si' do'' sol' do'' mi' sol' |
<<
  \tag #'violino1 {
    do'8 do''4\p mi''16 sol'' do'''8. do'''16 do'''8. re'''16 |
    sib''4 sib'8. sib'16 sib'8.\prall la'32 sib' do''8 sol' |
    la'4.\f la'16 do'' \rt#4 fa''8 |
    fa'8
  }
  \tag #'violino2 {
    do'16-\sug\p mi' sol' mi' sol' mi' sol' mi' sol' mi' sol' mi' sol' mi' sol' mi' |
    sol' mi' sol' mi' sol' mi' sol' mi' sol' mi' sol' mi' sol' mi' sol' mi' |
    do'-\sug\f fa' la' fa' la' fa' la' fa' la' fa' la' fa' la' fa' la' fa' |
    <fa' la>8
  }
>>
<<
  { la''16 la'' la''8 la'' } \\ { do''16 do'' do''8 do'' }
>> r8 <<
  \tag #'violino1 {
    <<
      { dod'''16 dod''' dod'''8 dod''' | } \\
      { mi''16 mi'' mi''8 mi'' }
    >>
    r8 <<
      { re'''16 re''' re'''8 re'' } \\
      { re''16 re''16 re''8 re'' }
    >>
    la''16 re'' si'' re'' do''' re'' re''' re'' |
    si''8\ff
  }
  \tag #'violino2 {
    sol''16 sol'' sol''8 sol'' |
    r8 << { fa''16 fa'' fa''8 re'' }
      \\ { la'16 la' la'8 re'' } >> re'2~ |
    re'8-\sug\ff
  }
>> sol8 sol sol sol16 la si do' re' mi' fa'! sol' |
mi' do'' si' do'' re'' do'' si' do'' <<
  \tag #'violino1 {
    fa'' fa'' mi'' fa'' sol'' fa'' mi'' fa'' |
    mi''
  }
  \tag #'violino2 {
    \rt#4 la'16 \rt#4 si' |
    do''
  }
>> sol'16 la' si' do'' re'' mi'' fa'' sol'' mi'' do'' mi'' sol'' mi'' sol'' mi'' |
fa'' re'' mi'' fa'' sol'' la'' si'' do''' re''' do''' si'' la'' sol'' fa'' mi'' re'' |
do'' do'' si' do'' sol' do'' mi' sol' do'4 r |
sol16 sol' fad' sol' re' sol' si re' sol4 r |
do''16 si' la' si' do'' si' la' si' do'' si' la' sol' fad' mi' re' do' |
si sol la si do' re' mi' fad' sol' la' si' do'' re'' mi'' fa''! sol'' |
mi'' do'' re'' mi'' fa'' sol'' la'' si'' do'''8 sol''16 sol'' mi''8 do'' |
<<
  \tag #'violino1 {
    sol'4 r r2 |
    << <do''' mi''>4 \\ sol' >> r r2 |
    << <si'' re''>4 \\ sol' >> r r2 |
    << <do''' mi''>4 \\ sol'>> r r2 |
    <si'' re'' sol'>4 r r << <do''' mi''>4 \\ sol' >> |
    << <si'' re''>4 \\ sol' >> r r << <do''' mi''> \\ sol' >> |
    
  }
  \tag #'violino2 {
    sol'16 sol'' sol'' sol'' \rt#4 sol'' \rt#8 sol'' |
    \rt#8 sol'' \rt#8 sol'' |
    sol''16 sol'' fad'' sol'' re'' sol'' si' re'' sol' sol'' sol'' sol'' \rt#4 sol'' |
    \rt#8 sol'' \rt#8 sol'' |
    \rt#8 sol'' \rt#8 sol'' |
    \rt#8 sol'' \rt#8 sol'' |
  }
>>
<< <si'' re''>4 \\ sol'>> <re' si' sol''>8. <sol'' si'>16 q4 <re' si' sol''> |
q2 r |
R1 |
r4 r8 sol'16 sol' fa'8-. re'-. si-. sol-. |
