\clef "treble"
\twoVoices #'(tromba1 tromba2 trombe) <<
  { do''4 }
  { mi'4 }
  { s4-\sug\f }
>> sol'8. sol'16 do''8. mi''16 |
re''16 sol'32 sol' sol'16 sol'
\twoVoices #'(tromba1 tromba2 trombe) <<
  { sol'8. re''16 re''8. re''16 | mi''4 }
  { sol'8. sol'16 sol'8. sol'16 | do''4 }
>> r4 r |
r4 r16 sol'32 sol' sol'16. sol'32 mi'16 do' mi' sol' |
do''8. do'16 do'4 r |
r8. \twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''16 mi''8. mi''16 mi''8. mi''16 | }
  { do''16 do''8. do''16 do''8. do''16 | }
  { s16-\sug\p }
>>
re''4. re''8 re''8. re''16 |
re''2. |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { re''8. re''16 re''8. re''16 re''8. re''16 |
    re''8. sol'16 sol'8. sol'16 sol'8. sol'16 |
    mi''4 }
  { sol'8. sol'16 sol'8. sol'16 sol'8. sol'16 |
    sol'8. sol16 sol8. sol16 sol8. sol16 |
    do'4 }
>> r4 r |
r16 re''32-\sug\f re'' re''16 re'' re''4 r |
R2.*6 |
r4 r re'' |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { sol'1 | sol'2 re'' | re''1 | }
  { sol1 | sol2 sol' | r2 re''4. re''8 | }
  { s1-\sug\fp }
>>
sol'4-\sug\f r r8 sol' sol' sol' |
sol'4 r r2 |
R1 |
r2 r4 \twoVoices #'(tromba1 tromba2 trombe) <<
  { re''4 | mi'' mi''8. mi''16 re''4 mi'' | re'' }
  { sol'4 | do'' do''8. do''16 sol'4 do'' | sol' }
>> r4 r sol'8.-\sug\ff sol'16 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { do''2 }
  { mi'2 }
>> do''4 re'' |
mi''2 \twoVoices #'(tromba1 tromba2 trombe) <<
  {  mi''2 | re''1 | }
  { do''2 | sol'1 | }
>>
do''8 do''16 do'' sol'8 mi' do'4 r |
r do''8.-\sug\p do''16 do''4 do'' |
do''1 |
do''4.-\sug\f do''16 do'' do''8 do'' do'' do'' |
do''4 r r2 |
R1 |
r2 r4 sol'8.-\sug\ff sol'16 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { do''2 do''4 re'' | mi''2 mi'' | re''1 | do''4 }
  { mi'2 do''4 re'' | mi''2 do'' | sol'1 | mi'4 }
>> r4 r8 \twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''16 mi'' mi''8 mi'' | re''4 }
  { do''16 do'' do''8 do'' | sol'4 }
>> r4 r8 \twoVoices #'(tromba1 tromba2 trombe) <<
  {  re''16 re'' re''8 re'' | }
  { sol'16 sol' sol'8 sol' | }
>>
do''2 re''4. re''8 |
sol'4 r r2 |
r4 r8 sol' do'' sol' mi' do' |
sol'4 r <>\ff
\twoVoices #'(tromba1 tromba2 trombe) <<
  { re''8 re''16 re'' re''8 re'' |
    mi'' re'' do'' re'' mi'' fa'' sol'' mi'' |
    re''4 }
  { sol'8 sol'16 sol' sol'8 sol' |
    do'' sol' mi' sol' do'' re'' mi'' do'' |
    sol'4 }
>> r4 \twoVoices #'(tromba1 tromba2 trombe) <<
  { re''8 re''16 re'' re''8 re'' |
    mi'' re'' do'' re'' mi'' fa'' sol'' mi'' |
    re'' re''16 re'' re''8 re'' re''4 mi'' |
    re''8 re''16 re'' re''8 re'' re''4 mi'' |
    re''4 re''8. re''16 re''4 re'' |
    re''2 }
  { sol'8 sol'16 sol' sol'8 sol' |
    do'' sol' mi' sol' do'' re'' mi'' do'' |
    sol' sol'16 sol' sol'8 sol' sol'4 do'' |
    sol'8 sol'16 sol' sol'8 sol' sol'4 do'' |
    sol' sol'8. sol'16 sol'4 sol' |
    sol'2 }
>> r2 |
R1*2 |
