\clef "vbasse" R2.*19 R1*8 |
r2 r4 sol8 sol |
mi4. mi8 la4 si8 si |
do'2 mi' |
re'8 si si sol sol4 sol8 si |
do'4 do r2 |
R1*5 |
r2 r4 sol8. sol16 |
mi4. mi8 la4 si8 si |
do'2 mi' |
re'4 si8 si sol4 sol8 si |
do'4 do r2 |
R1*5 |
do'2 r |
si4 sol r2 |
do'8[ sol] mi sol do'4 do'8 do' |
sol1~ |
sol~ |
sol4 sol r2 |
R1*3 |
