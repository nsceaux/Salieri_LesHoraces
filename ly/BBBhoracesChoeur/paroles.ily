\tag #'jhorace {
  Dieux, pro -- tec -- teurs du Ti -- bre !
  Tran -- chez mes jours & sau -- vez mon pa -- ys !
  Je re -- çois de ma vi -- e un as -- sez di -- gne prix,
  Si Ro -- me par ma mort est tri -- om -- phante & li -- bre.
  Je re -- çois de ma vi -- e un as -- sez di -- gne prix,
  Si Ro -- me par ma mort est tri -- om -- phante & li -- bre.
}
\tag #'vhorace {
  O de Rome heu -- reux dé -- fen -- seur,
  Cours, vo -- le où la gloi -- re t’ap -- pel -- le !
  Tes frè -- res, pleins du mê -- me zè -- le,
  T’at -- ten -- dent aux champs de l’hon -- neur.
}
\tag #'choeur {
  O de Rome heu -- reux dé -- fen -- seur,
  Cours, vo -- le où la gloi -- re t’ap -- pel -- le !
}
\tag #'vhorace {
  Quel Ro -- main n’en -- vie -- roit cet im -- mor -- tel hon -- neur !
  Tous bri -- gue -- roient en foule u -- ne mort aus -- si bel -- le.
}
\tag #'(vhorace choeur) {
  O de Rome heu -- reux dé -- fen -- seur,
  Cours, vole où la gloi -- re t’ap -- pel -- le !
}
\tag #'jhorace {
  A -- mis, vous em -- bra -- sez mon cœur !
  Oui je vo -- le où la gloi -- re m’ap -- pel -- le…
}
\tag #'(vhorace choeur) {
  Cours, vo -- le, vole où la gloi -- re t’ap -- pel -- le !
}
\tag #'jhorace {
  Mais un sol -- dat Al -- bain s’a -- vance i -- ci vers nous.
}
