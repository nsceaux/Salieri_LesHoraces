\clef "alto" mi'!1~ |
mi' |
re' |
do'' |
sib'~ |
sib'~ |
sib'~ |
sib' |
do''4 r r8. lab16\f lab4 |
R1 |
r2 fa'4 r |
r2 mib'2-\sug\p~ |
mib'1~ |
mib'2 re'~ |
re'4 r r2 |
sol8\f la16 sib do' re' mi' fad' sol'4 fa' |
mib'1~ |
mib'8 fa'16\ff sol' lab' sib' do'' re'' mib''4 mib' |
\rt#16 mi'!16\fp |
\rt#16 mi' |
\rt#16 mi' |
\rt#8 mi' \rt#8 fa' |
\rt#8 fa' re''2 |
r2 r4 sol'4 |
