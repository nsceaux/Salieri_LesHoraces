\clef "vbas-dessus" r4 r8 mi''!16 mi'' mi''4 re''8 mi'' |
dod''4 la'8 si' dod''4 dod''8 re'' |
la'4^! r8 la' re'' re'' re'' re'' |
mib''4.^! la'16 sib' do''4 re''8 la' |
sib'8^! sib' r4 r2 |
R1*19 |
