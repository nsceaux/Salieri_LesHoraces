\tag #'camille {
  Dai -- gnez pren -- dre pour lui de plus doux sen -- ti -- mens
  Vou -- lez- vous rendre, hé -- las ! no -- tre sort plus fu -- nes -- te ?
}
\tag #'romain {
  Crai -- gnez de vous li -- vrer à vos res -- sen -- ti -- mens
  C’est vo -- tre fils en -- fin, c’est le seul qui vous res -- te.
}
\tag #'vhorace {
  Mon fils ! ja -- mais il ne le fut.
  S’il eût é -- té mon sang il l’eût mieux fait con -- noî -- tre.
}
\tag #'romain {
  A tort, vous l’ac -- cu -- sez peut- ê -- tre
  Que vou -- liez- vous qu’il fit con -- tre trois ?
}
\tag #'vhorace {
  Qu’il mou -- rût.
  Vous cher -- chez vai -- ne -- ment à pal -- li -- er son cri -- me :
  J’en at -- tes -- te le Ciel & l’hon -- neur qui m’a -- ni -- me ;
  A -- vant la fin du jour, ces mains, ces pro -- pres mains,
  La -- ve -- ront dans son sang la hon -- te des Ro -- mains.
}
