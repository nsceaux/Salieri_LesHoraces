\clef "vbasse" R1*8 |
r4 r8 lab do'4 r8 mib' |
mib' do' do' do' lab4 r8 lab |
do' do' do' re' si!4^! r8 sol16 la |
si!4 si8 do' sol^! sol r4 |
R1*2 |
r2 r4 re'8. re'16 |
sol4^! r4 r2 |
r8 sib16 sib sib8 sib16 sib sol8. sol16 sol sol lab sib |
mib8^! mib r4 r2 |
r8 sol16 sol sol8 sol16 sol do'4. do'16 do' |
mi'4 re'8 mi' do' do' r sol |
do' do' re' do' sol4 r8 sol |
sib4 r16 sib sib do' lab4^! r8 do'16 do' |
re'4 re'8 do' si!4^! r8 sol |
si si si do' sol4^! r4 |
