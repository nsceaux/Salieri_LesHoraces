\clef "vbasse" R1*4 |
r4 r8 sib sib sib sib do' |
re'4.^! fa'8 re' re' re' mib' |
sib4^! r8 sib sib sib mib' re' |
mib'4 r8 sib16 do' reb'4 reb'8 mib' |
do'8^! do' r4 r2 |
R1*2 |
r2 r4 r8 sol |
do'4. do'8 do' do' do' mib' |
do' do' r16 do' re' mib' re'8.^! la16 do'8 re'16 la |
sib4 r r2 |
R1*9 |
