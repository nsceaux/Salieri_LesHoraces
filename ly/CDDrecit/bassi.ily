\clef "bass" sol1~ |
sol |
fa |
fad |
sol |
fa! |
mib~ |
mib |
lab4 r r8. lab,16\f lab,4 |
R1 |
r2 fa4 r |
r2 mib\p~ |
mib1~ |
mib2 fad |
sol4 r r2 |
sol,8\f la,16 sib, do re mi fad sol4 fa |
mib1~ |
mib8 fa16\ff sol lab sib do' re' mib'4 mib |
\rt#16 mi!16\fp |
\rt#16 mi |
\rt#16 mi |
\rt#8 mi \rt#8 fa |
\rt#8 fa fa2 |
r r4 sol |
