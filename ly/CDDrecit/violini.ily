\clef "treble"
<<
  \tag #'violino1 {
    dod''1~ | dod'' | re'' | la'' |
    sib'' | lab'' | sol''~ | sol'' | lab''4
  }
  \tag #'violino2 {
    la'1~ | la'~ | la' | mib'' |
    re'' | re'' | mib''~ | mib'' | mib''4
  }
>> r4 r8. <mib' do'>16\f q4 |
R1 |
r2 <sol re' si'!>4 r |
r2 <<
  \tag #'violino1 {
    sol''2\p~ | sol''1~ | sol''2 la''! | sib''4
  }
  \tag #'violino2 {
    do''2-\sug\p~ | do''1~ | do''2 do'' | sib'4
  }
>> r4 r2 |
sol8\f la16 sib do' re' mi' fad' sol'4 fa' |
mib'1~ |
mib'8 fa'16\ff sol' lab' sib' do'' re'' mib''4 <<
  \tag #'violino1 {
    <reb'' mib'>4 |
    do''16\fp <<
      { do'''16 do''' do''' \rt#4 do''' \rt#8 do''' } \\
      { do''16 do'' do'' \rt#4 do'' \rt#8 do'' }
    >>
    \rt#16 <do'' do'''> |
    \rt#16 q |
    \rt#8 q <<
      { \rt#8 do'''16 | \rt#8 do'''16 si''!2 | } \\
      { \rt#8 do''16 | \rt#8 do''16 re''2 | }
    >>
  }
  \tag #'violino2 {
    << sib'4 \\ sol' >> |
    <sol sol'>16-\sug\fp <do'' sol''>16[ q q] \rt#4 q \rt#8 q |
    \rt#16 q |
    \rt#16 q |
    \rt#8 q \rt#8 <do'' lab''> |
    \rt#8 q lab''2 |
  }
>>
r2 r4 <sol re' si'>4 |
