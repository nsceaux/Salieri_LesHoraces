\clef "alto" \rt#4 lab8 <>-\sug\p \rt#4 lab |
sib16 sib8 sib sib sib16~ sib sib8 sib sib sib16 |
\rt#4 lab8 \rt#4 solb |
<>-\sug\cresc \rt#4 fa \rt#4 fa |
fa4\! r r2 |
r sol'\f~ |
sol'1~ |
sol'2 lab'8 lab lab lab |
do'16-\sug\p do'8 do' do' do'16~ do' do'8 do' do' do'16 |
fa'16-\sug\mf fa'8-\sug\< fa' re'! re'16\! re'2~ |
re' sol'-\sug\f~ |
sol'~ sol'4 r |
r2 sol4-\sug\mf r |
r2 lab4 r |
r2 lab2-\sug\f~ |
lab sol16-\sug\p sol'8 sol' sol' sol'16 |
sol'16 sol'8 sol' sol' sol'16 mib' mib'8 mib' mib' mib'16 |
do''16 do''8 do'' do'' la'16 sol'4 sol'8-\sug\f sol' |
do'1~ |
do'~ |
do'2 re'~ |
re' r8 do'\f mi' do' |
\grace do'8 si la16 sol \grace sol8 fa mi16 re do4 r |
r2 r8. fad'16 fad'4 |
r2 r8. sol'16 sol'4 |
R1 |
sol'2 r4 sol'-\sug\ff |
fa'!2~ fa' |
fa'8. sib16 sib8. sib16 sib8. sib16 sib8. sib16 |
sib?8 sib sib sib fa'2 |
mib'1~ |
mib' |
lab8 lab'[ do'' lab'] \grace lab'8 sol' fa'16 mib' \grace mib'8 reb' do'16 sib |
lab4 r r2 |
re'1-\sug\fp |
do' |
la' |
sol'4 r r2 |
r8. sol'16-\sug\f sol'4 r2 |
r8. fa'16 fa'4 r2 |
mi'4 r r2 |
r4 mi' la r |
red'1-\sug\p |
mi'2~ mi'~ |
\sugNotes { mi'2 re' } |
fad1-\sug\fp~ |
fad2 sol~ |
sol r8. <sol re'>16-\sug\f q4 |
r2 r4 sol |
do'4.-\sug\f do'8 si16. sol'32 sol'8\mordent la16. fa'32 fa'8\mordent |
sol16. mi'32 mi'8\mordent r16 re' re'8\prall do' do sol sol |
do4 r8 do do4 do |
do1\fermata |
