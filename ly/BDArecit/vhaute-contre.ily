\clef "vhaute-contre" r2 <>^\p do'8 do'16 do' do'8. do'16 |
sib4 r sib8 sib16 sib sib8. mib'16 |
do'4 r r2 |
R1*5 |
<>^\p do'8 do'16 do' do'8. do'16 do'4 r |
re'!8 re'16 re' fa'8. re'16 si!4 r |
R1*6 |
<>^\p re'8 re'16 re' re'8. mib'16 do'4 r |
mib'8 mib'16 mib' mib'8. do'16 sib4 r |
R1*35 |
