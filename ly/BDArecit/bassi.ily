\clef "bass" lab8 lab lab lab <>\p \rt#4 lab |
\rt#4 mib \rt#4 mib |
\rt#4 lab \rt#4 solb |
<>\cresc \rt#4 fa \rt#4 fa |
sib,4\! r r2 |
r2 sol~ |
sol1~ |
sol2 lab8 lab lab lab |
<>\p \rt#4 mi!8 \rt#4 fa |
<>\mf\< \rt#4 fa <>\! sol2~ |
sol si,!\f~ |
si, do4 r |
r2 sol4\mf r |
r2 lab4 r |
r2 lab2\f~ |
lab sol8\p sol sol sol |
\rt#4 sol \rt#4 sol |
fad8 fad fad fad sol8. sol16\ff sol8 fa |
mi!1~ |
mi2 fa~ |
fa1~ |
fa2 r8 do'\f mi'! do' |
\grace do'8 si la16 sol \grace sol8 fa mi16 re do4 r |
r2 r8. fad16 fad4 |
r2 r8. sol16 sol4 |
R1 |
sol2 r4 la\ff |
sib,1 |
sib,8. sib,16 sib,8. sib,16 sib,8. sib,16 sib,8. sib,16 |
sib,8 sib, sib, sib, sib,2 |
mib1~ |
mib |
lab,8 lab[ do' lab] \grace lab8 sol fa16 mib \grace mib8 reb do16 sib, |
lab,4 r r2 |
fa1\fp |
mib |
re |
sol4 r r2 |
r8. sol16-\sug\f sol4 r2 |
r8. fa16 fa4 r2 |
mi4 r r2 |
r4 mi la, r |
red1\p |
mi~ |
mi2 re |
fad1\fp~ |
fad2 sol~ |
sol r8. fa!16\f fa4 |
r2 r4 sol |
do2-\sug\f si,4 la, |
sol, fa, mi,8 do, sol, sol |
do4 r8 do do4 do |
do1\fermata |
