\clef "vtaille" r2 <>^\p lab8 lab16 lab lab8. lab16 |
sol4 r sol8 sol16 sol sol8. sol16 |
lab4 r r2 |
R1*5 |
<>^\p sol8 sol16 sol sol8. sol16 lab4 r |
lab8 lab16 lab lab8. lab16 sol4 r |
R1*6 |
<>^\p sib8 sib16 sib sib8. sib16 la!4 r |
la8 la16 la la8. la16 sol4 r |
R1*35 |
