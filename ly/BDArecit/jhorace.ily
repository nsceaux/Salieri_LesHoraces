\clef "tenor/G_8" R1*11 |
r2 r4 r8 <>^\markup\character { Le jeune Horace } do'16 do' |
do'4 re'8 mib' sib4^! r8 sib16 sib |
reb'4 mib'8 sib do'^! do' r4 |
R1*4 |
r2 r8 <>^\markup\character { Le jeune Horace } sol sol sol |
do'4 do'8 sib16 do' la4^! r8 fa |
do' do' do' re' si!4^! r8 sol16 la |
si4 si8 do' sol sol r4 |
r2 r4 r8 sol |
do'4 do'8 mi'16 mi' re'8^! re' r la16 si |
do'4 do'8 re' si4^! r8 re'16 do' |
si4 do'8 re' sol sol r sol |
dod'8^! dod' dod' re' la^! la r4 |
R1*26 |
