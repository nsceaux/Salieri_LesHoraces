\tag #'choeur {
  O dé -- plo -- ra -- ble choix ! triste & fu -- neste hon -- neur !
}
\tag #'curiace {
  O de -- voir ri -- gou -- reux que l’hon -- neur nous im -- po -- se !
}
\tag #'camille {
  Ciel ! & quoi vo -- tre cœur
  Ne se ré -- vol -- te pas à la loi qu’on pro -- po -- se !
}
\tag #'choeur {
  O dé -- plo -- ra -- ble choix ! triste & fu -- neste hon -- neur !
}
\tag #'camille {
  Ah ! c’est un crime af -- freux qui doit vous faire hor -- reur.
}
\tag #'jhorace {
  Ap -- pel -- lez- vous for -- fait de ser -- vir sa pa -- tri -- e ?
}
\tag #'camille {
  Ap -- pel -- lez- vous ver -- tu, cet at -- ten -- tat im -- pi -- e ?
}
\tag #'choeur {
  O dé -- plo -- ra -- ble choix ! triste & fu -- neste hon -- neur !
}
\tag #'jhorace {
  Je con -- nais tout no -- tre mal -- heur ;
  Il peut nous é -- ton -- ner, mais non pas nous a -- bat -- tre.
  Toi, res -- te, Cu -- ri -- a -- ce, & con -- so -- le ma sœur :
  Je vien -- drai te re -- join -- dre, & nous i -- rons com -- bat -- tre.
}
\tag #'vhorace {
  Ver -- tu di -- gne de Ro -- me ! ô mon fils ! mon cher fils !
  Voi -- là les sen -- ti -- mens que mon cœur t’a trans -- mis !

  Oui, mes en -- fans, votre in -- for -- tune est gran -- de,
  Mon cœur, com -- me le vô -- tre, en a sen -- ti les coups :
  Mais l’ef -- fort est di -- gne de vous,
  Et tout cède à l’hon -- neur a -- lors qu’il nous com -- man -- de.
  Je plains Ca -- mil -- le, & per -- mets sa dou -- leur ;
  Son mal -- heur, sans doute, est ex -- trê -- me :
  C’est à toi, Cu -- ri -- a -- ce, à raf -- fer -- mir son cœur ;
  Rends- la di -- gne de nous & di -- gne de toi- mê -- me.
}