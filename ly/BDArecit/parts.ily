\piecePartSpecs
#`((fagotto1 #:notes "fagotti")
   (fagotto2 #:notes "fagotti")
   (violino1)
   (violino2)
   (alto)
   (basso #:instrument "Basso")
   (silence #:on-the-fly-markup , #{ \markup\tacet#53 #}))
