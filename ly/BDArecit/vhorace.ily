\clef "bass" R1*27 |
r4 <>^\markup\character { Le vieil Horace } r8 fa sib4 re'8 do'16 re' |
sib4^! sib r sib8 do' |
lab2 lab4. sib8 |
sol4. sib8 mib' sib sib sib |
sol4 sol8 lab sib4 sib8 do' |
lab4^! r r2 |
r2 do'4 do'8 re' |
si!4^! r si8 si16 si si8 do' |
sol8^! sol r sol do' do' do' la |
fad^! fad r la la sib do' sib |
sol4^! r sol4. sib8 |
la4.^! la8 dod'4 dod'8 re' |
la4^! r8 la16 la re'4 si8 la |
sold4^! r8 si sold sold sold la |
mi^! mi r4 r8 la la do' |
si^! si r si16 si si4 la8 si |
sol4^! r8 si16 dod' lad4^! r8 fad |
lad4 lad8 si fad8^! fad r si16 dod' |
re'4^! r8 la16 la fad8 fad r la |
la si do' re' si4^! r8 sol16 sol |
sol4 sol8 la si4^! r8 re' |
si si si do' sol sol^! r4 |
R1*3 |
R1^\fermataMarkup |
