\clef "vbasse" r2 <>^\p lab8 lab16 lab lab8. lab16 |
mib4 r mib8 mib16 mib mib8. mib16 |
lab4 r r2 |
R1*5 |
<>^\p mi!8 mi16 mi mi8. mi16 fa4 r |
fa8 fa16 fa fa8. fa16 sol4 r |
R1*6 |
<>^\p sol8 sol16 sol sol8. sol16 sol4 r |
fad8 fad16 fad fad8. fad16 sol4 r |
R1*35 |
