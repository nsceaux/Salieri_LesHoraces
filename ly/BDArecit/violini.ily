\clef "treble"
<<
  \tag #'violino1 {
    <lab mib' do''>16 mib'8 mib' mib' mib'16~ mib'\p mib'8 mib' mib' lab'16 |
    sol'16 sol'8 sol' sol' sol'16~ sol' sol'8 sol' sol' sol'16 |
    lab' lab'8 lab' lab' lab'16~ lab' lab'8 lab' lab' lab'16 |
    la'!\cresc la'8 la' la' la'16~ la' la'8 la' la' la'16\! |
    sib'4 r r2 |
    r2 mib''\f~ |
    mib''1~ |
    mib''2~ mib''16 mib'8\f mib' mib' mib'16 |
    do''16\p do''8 do'' do'' do''16~ do'' do''8 do'' do'' do''16 |
    re''!16\mf re''8\< re'' fa'' re''16\! si'!2~ |
    si' sol''\f~ |
    sol''~ sol''4 r |
    r2 mib''4\mf r4 |
    r2 <mib' do'>4 r |
    r2 fa''\f~ |
    fa'' re''16\p re''8 re'' re'' re''16~ |
    re'' re''8 re'' re'' mib''16 do'' do''8 do'' do'' do''16 |
    mib''16 mib''8 mib'' mib'' do''16 sib'8. sib'16\f sib'8-. si'-. |
    do''1~ |
    do''~ |
    do''2 si'!~ |
    si'2
  }
  \tag #'violino2 {
    mib'16 do'8 do' do' do'16~ do'-\sug\p do'8 do' do' do'16 |
    <<
      { mib'16 mib'8 mib' mib' mib'16~ mib' mib'8 mib' mib' mib'16 } \\
      { reb'16 reb'8 reb' reb' reb'16~ reb' reb'8 reb' reb' reb'16 }
    >>
    <do' mib'>16 q8 q q q16~ q q8 q q q16 |
    q\cresc q8 q q q16~ q q8 q q q16\! |
    reb'4 r r2 |
    r2 sib''\f~ |
    sib''1~ |
    sib''2 do'''16 do'8-\sug\f do' do' do'16 |
    sol'16-\sug\p sol'8 sol' sol' sol'16 lab' lab'8 lab' lab' lab'16~ |
    lab'-\sug\mf lab'8-\sug\< lab' lab' lab'16\! sol'2~ |
    sol' re''-\sug\f~ |
    re'' mib''4 r |
    r2 <sib' mib'>4-\sug\mf r |
    r2 <mib' do''>4 r |
    r2 do''-\sug\f~ |
    do'' si'!16-\sug\p si'8 si' si' si'16 |
    sib' sib'8 sib' sib' sib'16 la'! la'8 la' la' la'16 |
    la'16 la'8 la' la' la'16 re'8. re'16-\sug\f re'8 re' |
    sol'1~ |
    sol'2 la'~ |
    la' sol'~ |
    sol'
  }
>> r8 do''\f mi''! do'' |
\grace do''8 si'8 la'16 sol' \grace sol'8 fa' mi'16 re' do'4 r |
<<
  \tag #'violino1 {
    r2 r8. << { la''16 la''4 } \\ { la'16 la'4 } >> |
    r2 r8. << { sol''16 sol''4 } \\ { si'16 si'4 } >> |
    R1 |
    la''2 r4 << dod'' \\ mi'\ff >> |
    <re' re''>2~ q |
    sib'8. sib''16 sib''8. sib''16 sib''8. sib''16 sib''8. sib''16 |
    lab''8 sib' re'' fa'' lab''16 sol'' fa'' mib'' re'' do'' sib' lab' |
    <sol' sib>1~ |
    q |
    lab'8
  }
  \tag #'violino2 {
    r2 r8. re''16 re''4 |
    r2 r8. re''16 re''4 |
    R1 |
    <dod'' mi'>2 r4 <sol' la>4\ff |
    <sib fa'!>2~ q |
    re'8. <fa' re''>16 q8. q16 q8. q16 q8. q16 |
    q8 <re' lab> q q q2 |
    <sol mib'>1~ |
    q |
    do'8
  }
>> lab'8[ do'' lab'] \grace lab' sol' fa'16 mib' \grace mib'8 reb'8 do'16 sib |
lab4 r r2 |
<<
  \tag #'violino1 {
    sol''1\fp~ |
    sol'' |
    fad'' |
    sol''4 r r2 |
    r8. << { dod'''16 dod'''4 } \\ { la'16\f la'4 } >> r2 |
    r8. << { re'''16 re'''4 } \\ { la'16 la'4 } >> r2 |
    re'''4 r r2 |
  }
  \tag #'violino2 {
    si'!1-\sug\fp |
    do'' |
    do'' |
    sib'4 r r2 |
    r8. << { mi''!16 mi''4 } \\ { dod''16-\sug\f dod''4 } >> r2 |
    r8. << { la''16 la''4 } \\ { la'16 la'4 } >> r2 |
    << sold''4 \\ si'! >> r r2 |
  }
>>
r4 <mi'! si'! sold''>4 <la'' do'' mi'> r |
<<
  \tag #'violino1 {
    si''1\p~ |
    si''2 lad''~ |
    lad'' si'' |
  }
  \tag #'violino2 {
    <fad'' si'>1-\sug\p |
    <sol'' si'>2 <dod'' fad'>~ |
    q~ <si' fad'> |
  }
>>
<re' la>1\fp~ |
q2 <re' si>2~ |
q r8. << { sol''16 sol''4 } \\ { si'16\f si'4 } >> |
r2 r4 <si' re' sol>4 |
<do'' mi' sol>16.\f do'32 do'8\mordent mi'8-. la'-. si16. sol'32 sol'8\mordent la16. fa'32 fa'8\mordent |
sol16. mi'32 mi'8\mordent r16 re' re'8\mordent do'4 <sol re' si'>4 |
<do'' mi' sol>4 r8 do' do'4 do' |
do'1\fermata |
