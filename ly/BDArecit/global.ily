\key do \major \midiTempo#80
\time 4/4 \tempo "Andante" s1*21 s2 s8
\tempo "Allegro" s4. s1*6
\tempo "Presto" s1*4
\tempo "Allegro" s1*17
\tempo "Allegro maestoso" s1*4 \bar "|."
