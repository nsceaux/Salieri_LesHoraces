\clef "tenor"
<<
  \tag #'(fagotto1 fagotti) \new Voice {
    \tag #'fagotti \voiceOne
    do'1 | reb' | do'2~ do' | do'1 |
  }
  \tag #'(fagotto2 fagotti) \new Voice {
    \tag #'fagotti \voiceTwo
    lab1 | sib | lab2~ lab | la!1 |
  }
  { s1*3 | s1-\sug\cresc <>\! }
>>
sib4 r r2 |
R1*2 |
r2 <<
  \tag #'(fagotto1 fagotti) \new Voice {
    \tag #'fagotti \voiceOne
    do'2 | do' do' | re'! si!4
  }
  \tag #'(fagotto2 fagotti) \new Voice {
    \tag #'fagotti \voiceTwo
    lab2 | sol lab | lab sol4
  }
  { s2-\sug\f | s1-\sug\p | s2-\sug\mf\< <>\! }
>> r4 |
R1*5 |
r2 <<
  \tag #'(fagotto1 fagotti) \new Voice {
    \tag #'fagotti \voiceOne
    re'2 | re' do' | do' sib4
  }
  \tag #'(fagotto2 fagotti) \new Voice {
    \tag #'fagotti \voiceTwo
    si!2 | sib la | la sol4
  }
  { s2-\sug\p | }
>> r4 |
R1*35 |
