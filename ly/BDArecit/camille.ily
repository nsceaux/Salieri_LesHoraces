\clef "soprano/treble" R1*4 |
r2 <>^\markup\character Camille fa''4 r8 fa'' |
reb''4 r8 reb''16 reb'' sib'4^! r8 sib' |
sib' sib' sib' sib' mib''4 sib'8 do'' |
reb''4 mib''8 sib' do''^! do'' r4 |
R1 |
r2 r4 re'' |
re''8 re'' re'' re'' sol''4^! r8 re'' |
fa'' re'' re'' mib'' do''4^! r |
R1 |
r2 r4 r8 do''16 do'' |
do''4 do''8 do'' fa''4^! r8 do'' |
do'' do'' do'' si'! re''^! re'' r4 |
R1*37 |
