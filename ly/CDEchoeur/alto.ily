\clef "alto" do'2 r |
R1*3 |
r2 r8 do'-\sug\f mi' sol' |
sol'1\fp~ |
sol' |
la'2 do'8-\sug\p do'16 do' do'8 do' |
re' fa16 fa fa8 fa fa fa' fa' fa' |
do' sol16 sol sol8 sol sol do16 do do8 do' |
<<
  { \rt#8 fa'16 \rt#8 fa' | \rt#8 fa' \rt#8 fa' } \\
  { \rt#16 la | \rt#16 la }
>>
\custosNote do'2
