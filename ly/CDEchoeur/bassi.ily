\clef "bass"
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    do4\p do mi sol |
    do'4 r r2 |
    r4 sol, do sol, |
    do r r2 |
    r4 sol, do r |
    R1*5 |
  }
  \tag #'basso {
    do2\p r |
    R1*3 |
    r2 r8 do\f mi sol |
    do1\fp~ |
    do |
    fa,2 fa4.\p fa8 |
    sib,4 sib, sib, sib, |
    do do do do |
  }
>>
\rt#8 fa16\ff \rt#8 fa |
\rt#8 fa \rt#8 fa |
<<
  \tag #'(fagotto1 fagotto2 fagotti) { fa2 }
  \tag #'basso { \custosNote fa2 }
>>
