\clef "bass" r2 r4 sol,8.\p sol,16 |
do4 do8. do16 do4. do8 |
do4 sol, do sol, |
do do8. do16 do4. do8 |
do4 sol, do r |
R1*7 |
