\clef "treble" <sol mi' do''>2 r |
R1*3 |
r2 r8 do'16\f do' mi'8 sol' |
<<
  \tag #'violino1 {
    do''1\fp~ |
    do'' |
    do''2 fa''8\p fa''16 fa'' fa''8 fa'' |
    re'' re'16 re' re'8 re' re' re'' re'' re'' |
    sol' do''16 do'' do''8 do'' do'' mi''16 mi'' mi''8 mi'' |
    fa''8.\ff do'''16 do'''8. la''16 la''8. fa''16 fa''8. do''16 |
    do''8. fa''16 fa''8. do''16 do''8. la'16 la'8. fa'16 |
    \custosNote fa'2
  }
  \tag #'violino2 {
    mi'1\fp~ |
    mi' |
    fa'2 la'8\p la'16 la' la'8 la' |
    sib' sib16 sib sib8 sib sib sib' sib' sib' |
    mi'! mi'16 mi' mi'8 mi' mi' sol'16 sol' sol'8 sol' |
    <<
      { la'8. la''16 la''8. fa''16 fa''8. do''16 do''8. la'16 |
        la'8. do''16 do''8. la'16 la'8. fa'16 fa'8. fa'16 |
        \custosNote fa'2 } \\
      { s8. do''16 do''8. la'16 la'8. la'16 la'8. fa'16 |
        fa'8. la'16 la'8. fa'16 fa'8. do'16 do'8. la16 |
        \custosNote la2 }
    >>
  }
>>
