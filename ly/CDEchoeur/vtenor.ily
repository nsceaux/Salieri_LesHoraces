\clef "vtaille" r2 r4 sol8.^\p sol16 |
mi4 mi8. mi16 mi4. do'8 |
do'4.( sol8) mi4 sol8. sol16 |
do'4 do'8. do'16 do'4. mi'8 |
mi'4.( re'8) do'4 r |
R1*7 |
