\clef "vbasse" R1*5 |
r4 r8 sol16 sol sol4 sol8 sol |
do' do' r8 sol16 la sib4 do'8 sol |
la4^! r r2 |
R1*4 |
