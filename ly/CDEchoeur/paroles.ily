\tag #'(vsoprano valto vtenor vbasso) {
  Du vain -- queur, cé -- lé -- brons la gloi -- re ;
  Du vain -- queur, cé -- lé -- brons la gloi -- re ;
}
\tag #'vhorace {
  De quels cris d’al -- lé -- gres -- se ont re -- ten -- tit ces lieux ?
}
\tag #'(valto2 vtenor2 vbasso2) {
  L’air re -- ten -- tit des chants de la vic -- toi -- re !
}
