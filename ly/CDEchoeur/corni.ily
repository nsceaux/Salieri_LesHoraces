\clef "treble"
r4 do'8.\p do'16 mi'8. mi'16 sol'8. sol'16 |
\twoVoices #'(corno1 corno2 corni) <<
  { do''4 do''8. do''16 do''4. mi''8 |
    mi''4. re''8 do''4 re'' |
    mi'' mi''8. mi''16 mi''4. sol''8 |
    sol''4. fa''8 mi''4 }
  { mi'4 mi'8. mi'16 mi'4. do''8 |
    do''4. sol'8 mi'4 sol' |
    do'' do''8. do''16 do''4. mi''8 |
    mi''4. re''8 do''4 }
>> r4 |
R1*7 |
