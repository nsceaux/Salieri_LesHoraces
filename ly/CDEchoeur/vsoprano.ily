\clef "vbas-dessus" r2 r4 sol'8.^\p sol'16 |
do''4 do''8. do''16 do''4. mi''8 |
mi''4.( re''8) do''4 re''8. re''16 |
mi''4 mi''8. mi''16 mi''4. sol''8 |
sol''4.( fa''8) mi''4 r |
R1*7 |
