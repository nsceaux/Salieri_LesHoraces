\clef "vtaille" R1*7 |
r2 \footnoteHere #'(0 . 0) \markup\wordwrap {
  Source : \italic ré.
}
\sugNotes { fa'4 fa'8 fa' } | %re'4 re'8 re' |
re'2. re'4 |
do'4. do'8 mi'4. mi'8 |
fa'4 do' r2 |
R1 |
